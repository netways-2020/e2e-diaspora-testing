import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import java.text.DateFormat
import java.text.SimpleDateFormat

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.exception.StepErrorException as StepErrorException

import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By

import internal.GlobalVariable

public class TestHelper {
	static String youtube_url = 'https://www.youtube.com/watch?v=7bXAZfRTZQk';

	static String adminTown_email = 'Adminmayor@gmail.com';
	static String ambassadorTown_email = 'Fullambassador@gmail.com';
	static String mayorTown_email = 'Fullmayor@gmail.com';


	static String lebanedRedCross_OrganizationName = 'Red Cross Club - AUB';
	static String blaybel_CompanyName = "Blaybel Company Don't Remove It";
	static String blaybel_OrganizationName = "Blaybel Organization Don't Remove It";
	static String netways_CompanyName = "Netways";
	static String automationTesting_ProfileName = 'automation_testing';
	static String embassyOfLebanonInAmerica_EmbassyName = 'Embassy of Lebanon- USA';

	static String baseUrl = 'http://localhost:8100/';
	static String homeUrl = baseUrl + 'tabs/home';
	static String myProfileUrl = baseUrl + 'tabs/profile';
	static String profileWithFullDetailsUrl = baseUrl + 'app/profile/542b1ccc-0350-446a-844f-668db59e96a9';
	static String automationTesting_profileUrl = baseUrl + 'app/profile/393c606d-e959-45d1-95c3-1ee2e9d6b90c';
	static String diaspora_profileUrl = baseUrl + 'app/profile/393c606d-e959-45d1-95c3-1ee2e9d6b90c';

	static String blaybel_companyUrl = baseUrl + 'app/company/81ec6c12-deeb-463c-9be5-3ca8a606a2e0';
	static String netways_companyUrl = baseUrl + 'app/company/ec31906c-8772-4c8b-8022-251938455511';

	static String lebanesRedCross_organizationUrl = baseUrl + 'app/organization/9b23ec39-9450-4b0c-8c66-a01bfe7a9c12';
	static String blaybel_organizationUrl = baseUrl + 'app/organization/2230a301-be1d-46f3-ad21-6f0072f620af';

	static String embassyOfOman_EmbassyName = 'Embassy of Lebanon in Oman';
	static String embassyOfAssab_EmbassyName = 'Embassy of Lebanon in Mexico';
	static String embassyOfRiyadh_EmbassyName = 'Lebanese Embassy in Riyadh';

	static String embassyOfLebanonInAmerica_EmbassyUrl = baseUrl + 'app/diplomatic/0557afa6-cf80-4b55-8b40-78e24c1573e5';
	static String embassyOfOman_EmbassyUrl = baseUrl + 'app/diplomatic/d6cb89b9-1f11-424d-b5cf-90f5545d735b';
	static String embassyOfSudan_EmbassyUrl = baseUrl + 'app/diplomatic/b03f30d3-0178-4e0c-af6e-30b1905fc483';
	static String emabssyOfAssab_EmbassyUrl = baseUrl + 'app/diplomatic/14d5d326-b24a-4cf4-aef5-2bc4c5875125';
	static String embassyOfBelgium_EmbassyUrl	= baseUrl + 'app/diplomatic/07cf8234-e794-4f80-8827-32c917db5f9e';
	static String embassyOfRiyadh_EmbassyUrl	= baseUrl + 'app/diplomatic/a0e6192b-598a-48e2-86eb-fb7706c53df1';

	def static TestObject getObjectById(String objectId) {
		TestObject selectedObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, objectId)
		if(selectedObject != null) {
			WebUI.scrollToElement(selectedObject, 3)
		}
		return selectedObject;
	}

	def static boolean isElementPresent(TestObject to) {
		try {
			WebUI.verifyElementPresent(to, 3)
		} catch (Exception e) {
			return false
		}

		return true
	}

	static void thrownException(String message ) {
		throw new StepErrorException(message)
	}

	def static boolean verifyElementValue(String realValue, String excpectedValue) {
		try {
			WebUI.verifyMatch(realValue, excpectedValue, false)
		} catch (Exception e) {
			return false
		}

		return true
	}

	def static void setInputValue(TestObject toObject, value) {
		WebUI.scrollToElement(toObject, 1);
		//		WebUI.clearText(toObject);
		WebUI.delay(0.5);
		WebUI.setText(toObject, value)
	}

	def static void setPasswordValue(TestObject toObject, value) {
		WebUI.scrollToElement(toObject, 1);
		//		WebUI.clearText(toObject);
		WebUI.delay(0.5);
		WebUI.setEncryptedText(toObject, value)
	}

	def static void clearInputValue(TestObject toObject) {
		WebUI.scrollToElement(toObject, 1);
		WebUI.waitForElementClickable(toObject, 5);
		WebUI.clearText(toObject);

		//		wait.until(ExpectedConditions.elementToBeClickable(By.id("myElementId"))).clear();
	}

	def static void clickButton(TestObject toObject) {
		WebUI.scrollToElement(toObject, 1)
		WebUI.click(toObject)
	}

	def static String embassyOmanCounty() {
		return "Oman";
	}

	def static List<String> omanCities() {
		List<String> omanCities = new ArrayList<String>();
		omanCities.add("Muscat");
		omanCities.add("Raysut");
		omanCities.add("Thamarit");
		return omanCities;
	}

	def static List<String> prepareCountries() {
		List<String> countries = new ArrayList<String>();
		countries.add("Lebanon");
		countries.add("Argentina");
		countries.add("Canada");

		return countries;
	}

	def static Map<String, List<String>> prepareCities() {
		Map<String, List<String>> cities = new HashMap<String, List<String>>();

		List<String> lebanonCities = new ArrayList<String>();
		lebanonCities.add("Abboudieh");
		lebanonCities.add("Beirut");
		lebanonCities.add("Hermel");
		cities.put("Lebanon", lebanonCities);

		List<String> argentinaCities = new ArrayList<String>();
		argentinaCities.add("Mercedes");
		argentinaCities.add("Cordoba");
		argentinaCities.add("Tucuman");
		cities.put("Argentina", argentinaCities);

		List<String> canadaCities = new ArrayList<String>();
		canadaCities.add("Ajax");
		canadaCities.add("Jasper");
		canadaCities.add("Cornwall");
		cities.put("Canada", canadaCities);

		return cities;
	}

	def static String getCountryIso(String country) {
		if(country.equals("Lebanon")) {
			return "lb";
		} else if(country.equals("Argentina")) {
			return "ar";
		} else if(country.equals("Canada")) {
			return "ca";
		} else if(country.equals("Oman")) {
			return "om";
		}
	}

	def static String getCountryCode(String country) {
		if(country.equals("Lebanon")) {
			return "+961";
		} else if(country.equals("Argentina")) {
			return "+54";
		} else if(country.equals("Canada")) {
			return "+1";
		}  else if(country.equals("Oman")) {
			return "+968";
		}
	}

	def static getRandomValueFromList(List<String> arrayList) {
		Random random = new Random();
		return arrayList.get(random.nextInt(arrayList.size()))
	}

	def static goToManageEntity() {
		TestObject manageEntityAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Manage_User_entity_action']")
		clickButton(manageEntityAction)
	}

	def static goToCompanyManageEntity() {
		TestObject manageEntityAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Manage_Company_entity_action']")
		clickButton(manageEntityAction)
	}

	def static goToOrganizationManageEntity() {
		TestObject manageEntityAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Manage_Organization_entity_action']")
		clickButton(manageEntityAction)
	}

	def static goToDiplomaticManageEntity() {
		TestObject manageEntityAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Manage_DiplomaticMissions_entity_action']")
		clickButton(manageEntityAction)
	}

	def static countElementByClassName(String className) {
		WebDriver driver = DriverFactory.getWebDriver()
		List<WebElement> elementsCount = driver.findElements(By.className(className))
		return elementsCount.size();
	}

	def static getItem(String className, int index) {
		WebDriver driver = DriverFactory.getWebDriver()
		List<WebElement> mediaCount = driver.findElements(By.className(className))
		return mediaCount.get(index)
	}


	def static registerNewUser(String registeredEmail, String password, String firstName, String lastName, String city, boolean newBrowser) {

		if(newBrowser) {
			WebUI.openBrowser('')
			WebUI.navigateToUrl(TestHelper.getBaseUrl())
		}

		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='registerButton']"))
		WebUI.delay(1)

		// Email
		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='login-with-email-button']"))
		TestObject emailInput = TestHelper.getObjectById("//input[@id='email-input']")
		TestHelper.setInputValue(emailInput, registeredEmail)
		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='login-email-submit-button']"))

		// Password
		TestObject passwordInput = TestHelper.getObjectById("//input[@id='passwordInputId']")
		TestHelper.setPasswordValue(passwordInput, password)
		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='submit-button']"))

		// Profile general details
		TestObject firstNameInput = TestHelper.getObjectById("//input[@id='user-input-first-name']")
		TestHelper.setInputValue(firstNameInput, firstName)

		TestObject lastNameInput = TestHelper.getObjectById("//input[@id='user-input-last-name']")
		TestHelper.setInputValue(lastNameInput, lastName)

		TestObject saveUserGeneralDetails = TestHelper.getObjectById("//button[@id='user_save_general_details']")
		TestHelper.clickButton(saveUserGeneralDetails)


		// Profile location details
		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='user_city_of_origin']"))
		TestObject cityOfOriginSearchInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='city-search-input']")
		TestHelper.setInputValue(cityOfOriginSearchInput, city)
		TestObject cityOfOriginObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+city+"']")
		WebUI.delay(1)
		WebUI.click(cityOfOriginObject)


		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='user_city_of_residence']"))
		TestHelper.setInputValue(cityOfOriginSearchInput, city)
		TestObject cityOfResidenceObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+city+"']")
		WebUI.delay(1)
		WebUI.click(cityOfResidenceObject)

		TestObject saveUserDetails = TestHelper.getObjectById("//button[@id='save_user_info']")
		TestHelper.clickButton(saveUserDetails)
		WebUI.delay(1)

		TestObject manageProfilSection = TestHelper.getObjectById("//div[@id='manageProfileId']")
		WebUI.waitForElementVisible(manageProfilSection, 10)
		WebUI.delay(1);
	}

	def static loginWithAutomationTestingUser() {
		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='loginButton']"))
		WebUI.delay(1)

		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='login-with-email-button']"))

		TestObject emailInput = TestHelper.getObjectById("//input[@id='email-input']")
		TestHelper.setInputValue(emailInput, 'automationTesting@gmail.com')
		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='login-email-submit-button']"))

		TestObject passwordInput = TestHelper.getObjectById("//input[@id='passwordInputId']")
		TestHelper.setPasswordValue(passwordInput, 'cvW8qx4B2o3F4VwP/kNsqA==')


		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='submit-button']"))

		TestObject manageProfilSection = TestHelper.getObjectById("//div[@id='manageProfileId']")
		WebUI.waitForElementVisible(manageProfilSection, 10)
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_tab_id']"))
	}

	def static loginWithSpecificUserNameAndPassword(String email, String password, boolean newBrowser) {
		if(newBrowser) {
			WebUI.openBrowser('')
			WebUI.navigateToUrl(TestHelper.getBaseUrl())
		}

		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='loginButton']"))
		WebUI.delay(1)

		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='login-with-email-button']"))

		TestObject emailInput = TestHelper.getObjectById("//input[@id='email-input']")
		TestHelper.setInputValue(emailInput, email)
		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='login-email-submit-button']"))

		TestObject passwordInput = TestHelper.getObjectById("//input[@id='passwordInputId']")
		TestHelper.setPasswordValue(passwordInput, password)


		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='submit-button']"))

		TestObject manageProfilSection = TestHelper.getObjectById("//div[@id='manageProfileId']")
		WebUI.waitForElementVisible(manageProfilSection, 10)
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_tab_id']"))
	}


	def static loginWithSpecificEmail(String email, String password) {
		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='loginButton']"))
		WebUI.delay(1)

		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='login-with-email-button']"))

		TestObject emailInput = TestHelper.getObjectById("//input[@id='email-input']")
		TestHelper.setInputValue(emailInput, email)
		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='login-email-submit-button']"))

		TestObject passwordInput = TestHelper.getObjectById("//input[@id='passwordInputId']")
		TestHelper.setPasswordValue(passwordInput, password)


		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='submit-button']"))

		TestObject manageProfilSection = TestHelper.getObjectById("//div[@id='manageProfileId']")
		WebUI.waitForElementVisible(manageProfilSection, 10)
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_tab_id']"))
	}

	def static loginWithDiasporaUser() {
		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='loginButton']"))
		WebUI.delay(1)

		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='login-with-email-button']"))

		TestObject emailInput = TestHelper.getObjectById("//input[@id='email-input']")
		TestHelper.setInputValue(emailInput, 'diasporaid2017@gmail.com')
		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='login-email-submit-button']"))

		TestObject passwordInput = TestHelper.getObjectById("//input[@id='passwordInputId']")
		TestHelper.setPasswordValue(passwordInput, 'cvW8qx4B2o3F4VwP/kNsqA==')


		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='submit-button']"))

		TestObject manageProfilSection = TestHelper.getObjectById("//div[@id='manageProfileId']")
		WebUI.waitForElementVisible(manageProfilSection, 10)
	}

	def static verifyCardExist(String targetName, String cardTitleId, String targetType, boolean checkImage) {

		if(checkImage) {
			int cardImageSize = TestHelper.countElementByClassName(targetType + 'card_detail_image_id')

			if (cardImageSize == 0) {
				TestHelper.thrownException("Invalid card details");
			}

			WebElement imageObject = TestHelper.getItem((targetType + 'card_detail_image_id'), 0)

			String text = imageObject.text; // WebUI.getText(imageObject);//.trim();
			if(text.equals('assets/images/placeholder/placeholder.jpg')) {
				TestHelper.thrownException("Invalid card details");
			}
		}

		TestObject cardTargetObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='"+ targetName  + "_id']")
		TestObject cardTitleObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+ cardTitleId  + "_id']")
		TestObject cardTagsObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='card-right-image-tags']")

		boolean cardTargetObjectExist = TestHelper.isElementPresent(cardTargetObject)
		boolean cardTitleObjectExist = TestHelper.isElementPresent(cardTitleObject)
		boolean cardTagsObjectExist =  TestHelper.isElementPresent(cardTagsObject)


		if(cardTargetObjectExist && cardTitleObjectExist && cardTagsObjectExist) {
			print('Card verified Successfully')
		} else {
			TestHelper.thrownException("Invalid card details")
		}
	}
	
	def static verifyHomeCardExist(String targetName, String cardTitleId, String targetType, boolean checkImage) {	
		if(checkImage) {
			int cardImageSize = TestHelper.countElementByClassName('card_detail_image_id')

			if (cardImageSize == 0) {
				TestHelper.thrownException("Invalid card details");
			}

			WebElement imageObject = TestHelper.getItem('card_detail_image_id', 0)

			String text = imageObject.text; // WebUI.getText(imageObject);//.trim();
			if(text.equals('assets/images/placeholder/placeholder.jpg')) {
				TestHelper.thrownException("Invalid card details");
			}
		}

		TestObject cardTargetObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='home_card_title_"+ targetName  + "']")
		TestObject cardTitleObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+ cardTitleId  + "_id']")
		TestObject cardTagsObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='card-right-image-tags']")

		boolean cardTargetObjectExist = TestHelper.isElementPresent(cardTargetObject)
		boolean cardTitleObjectExist = TestHelper.isElementPresent(cardTitleObject)
		boolean cardTagsObjectExist =  TestHelper.isElementPresent(cardTagsObject)


		if(cardTargetObjectExist && cardTitleObjectExist && cardTagsObjectExist) {
			print('Card verified Successfully')
		} else {
			TestHelper.thrownException("Invalid card details")
		}
	}
	
	def static verifyDirectoryEventCardExist(String targetName, String cardTitleId, String targetType, boolean checkImage) {
		if(checkImage) {
			int cardImageSize = TestHelper.countElementByClassName('image_eventId')

			if (cardImageSize == 0) {
				TestHelper.thrownException("Invalid card details");
			}

			WebElement imageObject = TestHelper.getItem('image_eventId', 0)

			String text = imageObject.text; // WebUI.getText(imageObject);//.trim();
			if(text.equals('assets/images/placeholder/placeholder.jpg')) {
				TestHelper.thrownException("Invalid card details");
			}
		}

		TestObject cardTargetObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='home_card_title_"+ targetName  + "']")
		TestObject cardTitleObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+ cardTitleId  + "_id']")
		TestObject cardTagsObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='card-right-image-tags']")

		boolean cardTargetObjectExist = TestHelper.isElementPresent(cardTargetObject)
		boolean cardTitleObjectExist = TestHelper.isElementPresent(cardTitleObject)
		boolean cardTagsObjectExist =  TestHelper.isElementPresent(cardTagsObject)


		if(cardTargetObjectExist && cardTitleObjectExist && cardTagsObjectExist) {
			print('Card verified Successfully')
		} else {
			TestHelper.thrownException("Invalid card details")
		}
	}

	def static savePostCard() {
		TestObject cardDetailssavedObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='card_details_unSaved']")
		boolean cardDetailsSavedObjectExist = TestHelper.isElementPresent(cardDetailssavedObject);

		if(!cardDetailsSavedObjectExist) {
			TestHelper.thrownException("Invalid card details")
		}

		TestHelper.clickButton(cardDetailssavedObject);

		WebUI.delay(1);
		TestObject cardDetailsUnSavedObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='card_details_saved']")
		boolean cardDetailsUnSavedObjectExist = TestHelper.isElementPresent(cardDetailsUnSavedObject);

		if(!cardDetailsUnSavedObjectExist) {
			TestHelper.thrownException("Card not saved successfully");
		}
	}

	def static unSavePostCard() {
		TestObject cardDetailsUnsavedObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='card_details_saved']")
		boolean cardDetailsUnsavedObjectExist = TestHelper.isElementPresent(cardDetailsUnsavedObject);

		if(!cardDetailsUnsavedObjectExist) {
			TestHelper.thrownException("Invalid card details")
		}

		TestHelper.clickButton(cardDetailsUnsavedObject);

		WebUI.delay(1);
		TestObject cardDetailsSavedObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='card_details_unSaved']")
		boolean cardDetailsSavedObjectExist = TestHelper.isElementPresent(cardDetailsSavedObject);

		if(!cardDetailsSavedObjectExist) {
			TestHelper.thrownException("Card not saved successfully");
		}
	}

	def static feartureCard(String postTitle, String postDescription) {
		TestObject cardManageActionObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='card_details_manage_button']")
		boolean cardManageActionObjectExist = TestHelper.isElementPresent(cardManageActionObject);

		if(!cardManageActionObjectExist ) {
			TestHelper.thrownException("Invalid card details")
		}

		// Feature card
		TestHelper.clickButton(cardManageActionObject)
		TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Feature Post_user_action']"))

		WebUI.delay(2);
	}

	def static unFeartureCard(String postTitle, String postDescription) {
		TestObject cardManageActionObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='card_details_manage_button']")
		boolean cardManageActionObjectExist = TestHelper.isElementPresent(cardManageActionObject);

		if(!cardManageActionObjectExist ) {
			TestHelper.thrownException("Invalid card details")
		}

		// Feature card
		TestHelper.clickButton(cardManageActionObject)
		TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Unfeature Post_user_action']"))

		WebUI.delay(2);
	}


	def static checkPostCardFeatures(String postTitle, String postDescription, String newPostTitle, String newPostDescription, String objectName) {
		TestObject cardTargetObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='card-details-"+ objectName  + "']")
		TestObject cardTitleObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+ postTitle  + "']")
		TestObject cardDescriptionObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+ postDescription  + "']")
		TestObject cardManageActionObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='card_details_manage_button']")

		TestObject cardCommentInputObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//textarea[@id='commentInput']")
		TestObject cardCommentMessageActionObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='send_message_action']")

		// Card Feature
		TestObject cardDetailsLikeObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='card_details_like_count']")
		TestObject cardDetailsUnsavedObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='card_details_unSaved']")

		boolean cardTargetObjectExist = TestHelper.isElementPresent(cardTargetObject);
		boolean cardTitleObjectExist = TestHelper.isElementPresent(cardTitleObject);
		boolean cardDescriptionObjectExist = TestHelper.isElementPresent(cardDescriptionObject);
		boolean cardManageActionObjectExist = TestHelper.isElementPresent(cardManageActionObject);

		boolean cardCommentInputObjectExist = TestHelper.isElementPresent(cardCommentInputObject);
		boolean cardCommentMessageActionObjectExist = TestHelper.isElementPresent(cardCommentMessageActionObject);

		boolean cardDetailsLikeObjectExist = TestHelper.isElementPresent(cardDetailsLikeObject);
		boolean cardDetailsUnsavedObjectExist = TestHelper.isElementPresent(cardDetailsUnsavedObject);

		if(!cardTargetObjectExist || !cardTitleObjectExist || !cardDescriptionObjectExist || !cardManageActionObjectExist  ||
		!cardDetailsLikeObjectExist || !cardDetailsUnsavedObjectExist || !cardCommentInputObjectExist || !cardCommentMessageActionObjectExist) {
			TestHelper.thrownException("Invalid card details")
		}

		// Check save action
		TestHelper.clickButton(cardDetailsUnsavedObject);
		WebUI.delay(1);
		TestObject cardDetailsSavedObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='card_details_saved']")
		boolean cardDetailsSavedObjectExist = TestHelper.isElementPresent(cardDetailsSavedObject);

		if(!cardDetailsSavedObjectExist) {
			TestHelper.thrownException("Card not saved successfully");
		}


		// Check unsave action
		TestHelper.clickButton(cardDetailsSavedObject);
		TestObject cardDetailsNewUnsavedObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='card_details_unSaved']")
		boolean cardDetailsNewUnsavedObjectExist = TestHelper.isElementPresent(cardDetailsNewUnsavedObject);

		if(!cardDetailsNewUnsavedObjectExist) {
			TestHelper.thrownException("Card unsaved successfully");
		}


		// Check like action
		String likeString =  WebUI.getText(cardDetailsLikeObject);
		String[] likeSplittedArray = likeString.split(" ");
		int numberofLike = Integer.parseInt(likeSplittedArray[0])

		TestHelper.clickButton(cardDetailsLikeObject);

		String likedString =  WebUI.getText(cardDetailsLikeObject);
		String[] likedSplittedArray = likedString.split(" ");
		int numberofLiked = Integer.parseInt(likedSplittedArray[0])

		if(numberofLiked != (numberofLike + 1)) {
			TestHelper.thrownException("Card not liked successfully");
		}

		// Check unlike action
		TestHelper.clickButton(cardDetailsLikeObject);

		String newLikeString =  WebUI.getText(cardDetailsLikeObject);
		String[] newLikeSplittedArray = newLikeString.split(" ");
		int newLikeNumber = Integer.parseInt(newLikeSplittedArray[0])

		if(newLikeNumber != (numberofLiked - 1)) {
			TestHelper.thrownException("Card not unliked successfully");
		}

		// Comments
		String numberOfCommentsString =  WebUI.getText(TestHelper.getObjectById("//p[@id='headerTitleId']"));
		String[] numberOfCommentsArray = numberOfCommentsString.split(" ");
		int numberOfComments = Integer.parseInt(numberOfCommentsArray[0])

		String commentText = 'Comment added now';
		TestHelper.setInputValue(cardCommentInputObject, commentText);
		TestHelper.clickButton(cardCommentMessageActionObject);

		String commentId = commentText + '_card_comment';
		TestObject commentObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + commentId + "']")
		boolean commentObjectExist = TestHelper.isElementPresent(commentObject);


		if(!commentObjectExist) {
			TestHelper.thrownException("Comment not added successfully");
		}

		String newNumberOfCommentsString =  WebUI.getText(TestHelper.getObjectById("//p[@id='headerTitleId']"));
		String[] newNumberOfCommentsArray = newNumberOfCommentsString.split(" ");
		int newNumberOfComments = Integer.parseInt(newNumberOfCommentsArray[0])

		if(newNumberOfComments != (numberOfComments + 1)) {
			TestHelper.thrownException("Comment not added successfully");
		}

		// Edit comment
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='"+commentText+"_card_comment_action']"))
		WebUI.delay(1);
		TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Edit Comment_user_action']"))

		String newCommentText = 'New Comment added now';
		TestHelper.setInputValue(TestHelper.getObjectById("//textarea[@id='comment-section']"), newCommentText);
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='card_comment_save_button']"));

		String newCommentId = newCommentText + '_card_comment';
		TestObject newCommentObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + newCommentId + "']")
		boolean newCommentObjectExist = TestHelper.isElementPresent(newCommentObject);

		if(!newCommentObjectExist) {
			TestHelper.thrownException("Comment not edited successfully");
		}


		// Delete comment
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='"+newCommentText+"_card_comment_action']"))
		WebUI.delay(1);
		TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Delete Comment_user_action']"))

		WebUI.delay(1);
		WebElement deleteCommentConfirmation = TestHelper.getItem('delete_comment_confirmation', 0)
		deleteCommentConfirmation.click();

		WebUI.delay(2);

		String deletedNumberOfCommentsString =  WebUI.getText(TestHelper.getObjectById("//p[@id='headerTitleId']"));
		String[] deletedNewNumberOfCommentsArray = deletedNumberOfCommentsString.split(" ");
		int deletedNumberOfComments = Integer.parseInt(deletedNewNumberOfCommentsArray[0])

		println('deletedNumberOfComments:  ' + deletedNumberOfComments);
		println('newNumberOfComments:  ' + newNumberOfComments);

		if(deletedNumberOfComments != (newNumberOfComments - 1)) {
			TestHelper.thrownException("Comment not deleted successfully");
		}

		// Edit card
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='card_details_manage_button']"))
		WebUI.delay(1);

		TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Edit Post_user_action']"))
		UserActionsHelper.editPost(newPostTitle, newPostDescription);

		WebUI.delay(2);

		TestObject editCardTitleObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+ newPostTitle  + "']")
		TestObject editCardDescriptionObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+ newPostDescription  + "']")

		boolean editCardTitleObjectExist = TestHelper.isElementPresent(editCardTitleObject);
		boolean editCardDescriptionObjectExist = TestHelper.isElementPresent(editCardDescriptionObject);

		if(!editCardTitleObjectExist || !editCardDescriptionObjectExist) {
			TestHelper.thrownException("Card not edited successfully");
		}

		// Delete Card
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='card_details_manage_button']"))
		WebUI.delay(1);
		TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Delete Post_user_action']"))
		WebUI.delay(1);

		WebElement deleteCardConfirmation = TestHelper.getItem('remove-new-card', 0)
		deleteCardConfirmation.click();

	}

	def static checkEventCardFeatures(String eventTitle, String eventDescription, String eventLocation,
			String newEventTitle, String newEventDescription, String newEventLocation, String objectName) {

		TestObject cardTargetObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='card-details-"+ objectName  + "']")
		TestObject cardTitleObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+ eventTitle  + "']")
		TestObject cardDescriptionObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+ eventDescription  + "']")

		TestObject cardEventStartDateObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='event_card_detail_start_date']")
		TestObject cardEventEndDateObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='event_card_detail_end_date']")
		TestObject cardEventEndLocationObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + eventLocation + "_event_location']")

		TestObject cardManageActionObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='card_details_manage_button']")
		TestObject cardCommentInputObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//textarea[@id='commentInput']")
		TestObject cardCommentMessageActionObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='send_message_action']")

		// Card Feature
		TestObject cardDetailsLikeObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='card_details_like_count']")
		TestObject cardDetailsUnsavedObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='card_details_unSaved']")

		boolean cardTargetObjectExist = TestHelper.isElementPresent(cardTargetObject);
		boolean cardTitleObjectExist = TestHelper.isElementPresent(cardTitleObject);
		boolean cardDescriptionObjectExist = TestHelper.isElementPresent(cardDescriptionObject);
		boolean cardManageActionObjectExist = TestHelper.isElementPresent(cardManageActionObject);

		boolean cardEventStartDateObjectExist = TestHelper.isElementPresent(cardEventStartDateObject);
		boolean cardEventEndDateObjectExist = TestHelper.isElementPresent(cardEventEndDateObject);
		boolean cardEventEndLocationObjectExist = TestHelper.isElementPresent(cardEventEndLocationObject);

		boolean cardCommentInputObjectExist = TestHelper.isElementPresent(cardCommentInputObject);
		boolean cardCommentMessageActionObjectExist = TestHelper.isElementPresent(cardCommentMessageActionObject);

		boolean cardDetailsLikeObjectExist = TestHelper.isElementPresent(cardDetailsLikeObject);
		boolean cardDetailsUnsavedObjectExist = TestHelper.isElementPresent(cardDetailsUnsavedObject);

		if( !cardTargetObjectExist || !cardEventStartDateObjectExist || !cardEventEndDateObjectExist || !cardEventEndLocationObjectExist  ||
		!cardTitleObjectExist || !cardDescriptionObjectExist ||
		!cardManageActionObjectExist  ||
		!cardDetailsLikeObjectExist || !cardDetailsUnsavedObjectExist || !cardCommentInputObjectExist || !cardCommentMessageActionObjectExist) {
			TestHelper.thrownException("Invalid card details")
		}


		String headerText =  WebUI.getText(TestHelper.getObjectById("//p[@id='headerTitleId']"));
		if(headerText != 'Event Details') {
			TestHelper.thrownException("Event header not exist");
		}

		// Check save action
		TestHelper.clickButton(cardDetailsUnsavedObject);
		WebUI.delay(1);
		TestObject cardDetailsSavedObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='card_details_saved']")
		boolean cardDetailsSavedObjectExist = TestHelper.isElementPresent(cardDetailsSavedObject);

		if(!cardDetailsSavedObjectExist) {
			TestHelper.thrownException("Card not saved successfully");
		}

		// Check like action
		String likeString =  WebUI.getText(cardDetailsLikeObject);
		String[] likeSplittedArray = likeString.split(" ");
		int numberofLike = Integer.parseInt(likeSplittedArray[0])

		TestHelper.clickButton(cardDetailsLikeObject);

		String likedString =  WebUI.getText(cardDetailsLikeObject);
		String[] likedSplittedArray = likedString.split(" ");
		int numberofLiked = Integer.parseInt(likedSplittedArray[0])

		if(numberofLiked != (numberofLike + 1)) {
			TestHelper.thrownException("Card not liked successfully");
		}

		// Check unlike action
		TestHelper.clickButton(cardDetailsLikeObject);

		String newLikeString =  WebUI.getText(cardDetailsLikeObject);
		String[] newLikeSplittedArray = newLikeString.split(" ");
		int newLikeNumber = Integer.parseInt(newLikeSplittedArray[0])

		if(newLikeNumber != (numberofLiked - 1)) {
			TestHelper.thrownException("Card not unliked successfully");
		}

		// Comments
		String commentText = 'Comment added now';
		TestHelper.setInputValue(cardCommentInputObject, commentText);
		TestHelper.clickButton(cardCommentMessageActionObject);

		String commentId = commentText + '_card_comment';
		TestObject commentObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + commentId + "']")
		boolean commentObjectExist = TestHelper.isElementPresent(commentObject);


		if(!commentObjectExist) {
			TestHelper.thrownException("Comment not added successfully");
		}


		// Edit comment
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='"+commentText+"_card_comment_action']"))
		WebUI.delay(1);
		TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Edit Comment_user_action']"))

		String newCommentText = 'New Comment added now';
		TestHelper.setInputValue(TestHelper.getObjectById("//textarea[@id='comment-section']"), newCommentText);
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='card_comment_save_button']"));

		String newCommentId = newCommentText + '_card_comment';
		TestObject newCommentObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + newCommentId + "']")
		boolean newCommentObjectExist = TestHelper.isElementPresent(newCommentObject);

		if(!newCommentObjectExist) {
			TestHelper.thrownException("Comment not edited successfully");
		}


		// Delete comment
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='"+newCommentText+"_card_comment_action']"))
		WebUI.delay(1);
		TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Delete Comment_user_action']"))
		WebUI.delay(1);
		WebElement deleteCommentConfirmation = TestHelper.getItem('delete_comment_confirmation', 0)
		deleteCommentConfirmation.click();

		WebUI.delay(1);

		// Edit card
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='card_details_manage_button']"))
		WebUI.delay(1);

		TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Edit Post_user_action']"))
		UserActionsHelper.editEvent(newEventTitle, newEventDescription, newEventLocation);

		WebUI.delay(5);

		TestObject editCardTitleObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+ newEventTitle  + "']")
		//		TestObject editCardDescriptionObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+ newEventDescription  + "']")

		TestObject editCardEventStartDateObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='event_card_detail_start_date']")
		TestObject editCardEventEndDateObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='event_card_detail_end_date']")
		TestObject editCardEventEndLocationObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + newEventLocation + "_event_location']")

		boolean editCardTitleObjectExist = TestHelper.isElementPresent(editCardTitleObject);
		//		boolean editCardDescriptionObjectExist = TestHelper.isElementPresent(editCardDescriptionObject);

		boolean editCardEventStartDateObjectExist = TestHelper.isElementPresent(editCardEventStartDateObject);
		boolean editCardEventEndDateObjectExist = TestHelper.isElementPresent(editCardEventEndDateObject);
		boolean editCardEventEndLocationObjectExist = TestHelper.isElementPresent(editCardEventEndLocationObject);


		if(!editCardEventStartDateObjectExist || !editCardEventEndDateObjectExist || !editCardEventEndLocationObjectExist ||
		!editCardTitleObjectExist //|| !editCardDescriptionObjectExist
		) {
			TestHelper.thrownException("Card not edited successfully");
		}

		// Delete Card
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='card_details_manage_button']"))
		WebUI.delay(1);
		TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Delete Post_user_action']"))
		WebUI.delay(1);

		WebElement deleteCardConfirmation = TestHelper.getItem('remove-new-card', 0)
		deleteCardConfirmation.click();

	}


	def static verifyCardUpdateTownExist(String targetName, String cardDescriptionId) {
		TestObject cardTitleObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='"+ targetName  + "_id']")
		TestObject cardDescriptionObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+ cardDescriptionId  + "_id']")
		TestObject cardTagsObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='card-right-image-tags']")



		boolean cardTitleObjectExist = TestHelper.isElementPresent(cardTitleObject)
		boolean cardDescriptionObjectExist = TestHelper.isElementPresent(cardDescriptionObject)
		boolean cardTagsObjectExist =  TestHelper.isElementPresent(cardTagsObject)

		if(cardTitleObjectExist && cardDescriptionObjectExist && cardTagsObjectExist) {
			print('Card Created Successfully')
		} else {
			TestHelper.thrownException()
		}
	}


	def static verifyOfferCardFeature(String headerTitle) {
		TestObject cardManageActionObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='card_details_manage_button']")
		TestObject cardCommentInputObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//textarea[@id='commentInput']")
		TestObject cardCommentMessageActionObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='send_message_action']")

		// Card Feature
		TestObject cardDetailsLikeObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='card_details_like_count']")
		TestObject cardDetailsUnsavedObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='card_details_unSaved']")

		boolean cardManageActionObjectExist = TestHelper.isElementPresent(cardManageActionObject);
		boolean cardCommentInputObjectExist = TestHelper.isElementPresent(cardCommentInputObject);
		boolean cardCommentMessageActionObjectExist = TestHelper.isElementPresent(cardCommentMessageActionObject);
		boolean cardDetailsLikeObjectExist = TestHelper.isElementPresent(cardDetailsLikeObject);
		boolean cardDetailsUnsavedObjectExist = TestHelper.isElementPresent(cardDetailsUnsavedObject);

		if( !cardManageActionObjectExist  || !cardDetailsLikeObjectExist || !cardDetailsUnsavedObjectExist || !cardCommentInputObjectExist || !cardCommentMessageActionObjectExist) {
			TestHelper.thrownException("Invalid card details")
		}


		String headerText =  WebUI.getText(TestHelper.getObjectById("//p[@id='headerTitleId']"));
		if(headerText != headerTitle + ' Details') {
			TestHelper.thrownException(headerTitle + " header not exist");
		}

		// Check save action
		TestHelper.clickButton(cardDetailsUnsavedObject);
		WebUI.delay(1);
		TestObject cardDetailsSavedObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='card_details_saved']")
		boolean cardDetailsSavedObjectExist = TestHelper.isElementPresent(cardDetailsSavedObject);

		if(!cardDetailsSavedObjectExist) {
			TestHelper.thrownException("Card not saved successfully");
		}

		// Check like action
		String likeString =  WebUI.getText(cardDetailsLikeObject);
		String[] likeSplittedArray = likeString.split(" ");
		int numberofLike = Integer.parseInt(likeSplittedArray[0])

		TestHelper.clickButton(cardDetailsLikeObject);

		String likedString =  WebUI.getText(cardDetailsLikeObject);
		String[] likedSplittedArray = likedString.split(" ");
		int numberofLiked = Integer.parseInt(likedSplittedArray[0])

		if(numberofLiked != (numberofLike + 1)) {
			TestHelper.thrownException("Card not liked successfully");
		}

		// Check unlike action
		TestHelper.clickButton(cardDetailsLikeObject);

		String newLikeString =  WebUI.getText(cardDetailsLikeObject);
		String[] newLikeSplittedArray = newLikeString.split(" ");
		int newLikeNumber = Integer.parseInt(newLikeSplittedArray[0])

		if(newLikeNumber != (numberofLiked - 1)) {
			TestHelper.thrownException("Card not unliked successfully");
		}

		// Comments
		String commentText = 'Comment added now';
		TestHelper.setInputValue(cardCommentInputObject, commentText);
		TestHelper.clickButton(cardCommentMessageActionObject);

		String commentId = commentText + '_card_comment';
		TestObject commentObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + commentId + "']")
		boolean commentObjectExist = TestHelper.isElementPresent(commentObject);


		if(!commentObjectExist) {
			TestHelper.thrownException("Comment not added successfully");
		}


		// Edit comment
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='"+commentText+"_card_comment_action']"))
		WebUI.delay(1);
		TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Edit Comment_user_action']"))

		String newCommentText = 'New Comment added now';
		TestHelper.setInputValue(TestHelper.getObjectById("//textarea[@id='comment-section']"), newCommentText);
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='card_comment_save_button']"));

		String newCommentId = newCommentText + '_card_comment';
		TestObject newCommentObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + newCommentId + "']")
		boolean newCommentObjectExist = TestHelper.isElementPresent(newCommentObject);

		if(!newCommentObjectExist) {
			TestHelper.thrownException("Comment not edited successfully");
		}


		// Delete comment
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='"+newCommentText+"_card_comment_action']"))
		WebUI.delay(1);
		TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Delete Comment_user_action']"))

		WebUI.delay(1);
		WebElement deleteCommentConfirmation = TestHelper.getItem('delete_comment_confirmation', 0)
		deleteCommentConfirmation.click();

		WebUI.delay(1);
	}

	def static verifyOfferCard(String needTitle, int countryCount, int keywordCount, boolean firstCheck) {

		TestObject needTitleObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + needTitle +  "']")
		boolean needTitleObjectExist = TestHelper.isElementPresent(needTitleObject);

		int companyOfferCount = TestHelper.countElementByClassName('company-offer-countries');
		int companyKeywordCount = TestHelper.countElementByClassName('company-offer-keywords');

		if(firstCheck) {
			if(companyOfferCount != 2) {
				TestHelper.thrownException("Need countries not added successfully");
			}

			if(companyKeywordCount != 2) {
				TestHelper.thrownException("Need keywords not added successfully");
			}
		} else {
			if(companyOfferCount == countryCount + 1) {
				TestHelper.thrownException("Need countries not added successfully");
			}

			if(companyKeywordCount == keywordCount + 1) {
				TestHelper.thrownException("Need keywords not added successfully");
			}
		}


		if (!needTitleObjectExist) {
			TestHelper.thrownException("Need item details not added successfully");
		} else {
			println("Added product exist");
		}
	}

	def static checkCards(String cardClass) {
		int cardCount = TestHelper.countElementByClassName(cardClass);
		if(cardCount > 0) {
			return true;
		} else {
			return false;
		}
	}

	def static checkObjectExit(String id, String erroMessage) {
		TestObject object = TestHelper.getObjectById(id);
		if(!TestHelper.isElementPresent(object)) {
			TestHelper.thrownException(erroMessage);
		}
	}

	def static checkObjectText(String id, String message, String erroMessage) {
		TestObject object = TestHelper.getObjectById(id);
		if(!TestHelper.isElementPresent(object)) {
			TestHelper.thrownException(erroMessage);
		}

		String text =  WebUI.getText(object).trim();
		println('Real Text: ' + text + ', Excepected Text: ' + message);
		if(!text.equals(message.trim())) {
			TestHelper.thrownException(erroMessage);
		}
	}

	def static checkObjectImageSrc(String id, String srcText, String erroMessage) {
		TestObject object = TestHelper.getObjectById(id);
		if(!TestHelper.isElementPresent(object)) {
			TestHelper.thrownException(erroMessage);
		}

		String text =  WebUI.getText(object).trim();
		println('Real Text: ' + text + ', Excepected Text: ' + srcText);

		if(!text.equals(srcText.trim())) {
			TestHelper.thrownException(erroMessage);
		}
	}

	// Image Functions
	def static checkIfImageAddedSuccessfully(boolean firstImageAdded, int imageCountBeforAdd) {
		int imageCount = TestHelper.countElementByClassName('gallery-container');

		if(firstImageAdded) {
			if(imageCount == 1) {
				println("Image added successfully");
			} else {
				TestHelper.thrownException("Image not added successfully");
			}
		} else {
			if((imageCountBeforAdd + 1) == imageCount) {
				println("Image added successfully");
			} else {
				TestHelper.thrownException("Image not added successfully");
			}
		}
	}

	def static removeImage() {
		int imageCountBeforAdd = TestHelper.countElementByClassName('gallery-container');
		WebElement element = TestHelper.getItem('gallery-container', 0);
		element.click();

		TestHelper.checkImageLoaded("image_detail_id");

		TestObject imageCaption = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='image-caption-id']")
		boolean imageCaptionExist = TestHelper.isElementPresent(imageCaption);

		if(imageCaptionExist) {
			print("Image caption exist");
		} else {
			TestHelper.thrownException("Image caption not exist");
		}

		TestObject removeAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='galler_image_id']")
		boolean removeActionExist = TestHelper.isElementPresent(removeAction);

		if(removeActionExist) {
			TestHelper.clickButton(removeAction)
			UserActionsHelper.removeMedia();
			this.checkIfImageRemovedSuccessfully(imageCountBeforAdd);
		} else {
			TestHelper.thrownException("Doesn't have the privilege tot delete an image");
		}
	}

	def static checkIfImageRemovedSuccessfully(int imageCountBeforAdd) {
		int imageCount = TestHelper.countElementByClassName('gallery-container');

		if((imageCountBeforAdd - 1) == imageCount) {
			println("Image deleted successfully");
		} else {
			TestHelper.thrownException("Image not deleted successfully");
		}
	}

	def static checkImageLoaded(String imageClass) {
		if(TestHelper.countElementByClassName(imageClass) > 0) {
			WebElement imageElement = TestHelper.getItem(imageClass, 0);
			String imageSrc = imageElement.getAttribute("src");
			if(imageSrc.contains('assets/images/placeholder/placeholder.jpg')) {
				TestHelper.thrownException("Image not loaded");
			}
		} else {
			TestHelper.thrownException("Image not loaded");
		}
	}
}


