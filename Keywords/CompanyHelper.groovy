import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

public class CompanyHelper {

	def static checkCompanyGeneralInfoFieldsEmptyState() {
		boolean successEmptyFields = true;


		TestObject no_short_description_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_company_introduction_id_Add A Short Description']")
		TestObject no_address_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_company_address_id_Add address']")
		//		TestObject no_pobox_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_company_pobox_id_Add P.O. Box']")

		TestObject no_contact_phone_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_company_contact_phone_id_Add Phone Number']")
		TestObject no_contact_email_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_company_contact_email_id_Add Email Address']")
		TestObject no_contact_website_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_company_contact_website_id_Add A Website']")

		TestObject no_social_media_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_social_media_id']")

		// Check Object Exist
		if(!TestHelper.isElementPresent(no_short_description_object)) {
			successEmptyFields = false;
		} else if(!TestHelper.isElementPresent(no_address_object)) {
			successEmptyFields = false;
		}
		//		else if(!TestHelper.isElementPresent(no_pobox_object)) {
		//			successEmptyFields = false;
		//		}
		else if(!TestHelper.isElementPresent(no_contact_phone_object)) {
			successEmptyFields = false;
		} else if(!TestHelper.isElementPresent(no_contact_email_object)) {
			successEmptyFields = false;
		}  else if(!TestHelper.isElementPresent(no_contact_website_object)) {
			successEmptyFields = false;
		} else if(!TestHelper.isElementPresent(no_social_media_object)) {
			successEmptyFields = false;
		}

		if(successEmptyFields) {
			println("Empty state for company general info tested successfully");
		} else {
			TestHelper.thrownException("Error during clear user info")
		}
	}
	def static homeRegistration() {

		String[] firstNameLastNameArray = findTestData('Data Files/firstNameLastname').getAllData()

		int randomFirstNameLastNameNumber = new Random().nextInt(firstNameLastNameArray.length)

		int randomFirstNameLastName = randomFirstNameLastNameNumber == 0 ? 1 : randomFirstNameLastNameNumber

		String firstname = findTestData('Data Files/firstNameLastname').getValue(1, randomFirstNameLastName)

		String lastname = findTestData('Data Files/firstNameLastname').getValue(2, randomFirstNameLastName)

		//cities

		String[] citiesArray = findTestData('Data Files/Cities').getAllData()

		int randomResidenceCity = new Random().nextInt(200)

		int randomOriginCity = new Random().nextInt(citiesArray.length)

		String originCity = findTestData('Data Files/Cities').getValue(1, randomOriginCity)

		String residenceCity = findTestData('Data Files/Cities').getValue(1,randomResidenceCity)

		//generate new email
		String email = ('blaybel@' + new Date().getTime()) + '.com'

		//generate password
		String password = 'Test@123'

		//Cities
		TestObject cityOrigineDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Aaba\']')
		TestObject cityResidenceDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Aaba\']')
		TestObject cityDistrictDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//span[@id=\'district_city\']')
		TestObject profile_tab = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//ion-tab-button[@id='tab-button-profile']")

		//registration button
		TestObject registerByEmailButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='registerButton']")
		TestObject loginWithEmailButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='login-with-email-button']")
		TestObject loginEmailSubmitButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//button[@id=\'login-email-submit-button\']')
		TestObject createAccountButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//button[@id=\'passwordInputId\']')
		TestObject userSaveGeneralDetails = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//button[@id=\'user_save_general_details\']')

		//registration input
		TestObject emailInput = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//input[@id=\'email-input\']')
		TestObject passwordInputId = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//input[@id=\'passwordInputId\']')
		TestObject userInputFirstName = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//input[@id=\'user-input-first-name\']')
		TestObject userInputLastName = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//input[@id=\'user-input-last-name\']')

		//home page
		TestObject homeTitleProfilePage = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'home_title_' + firstname + ' ' + lastname) + '\']')
		TestObject originCityInHomeDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//span[@id=\'country_lb_1_tag_label\']')
		TestObject residenceCityInHomeDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//span[@id=\'country_lb_1_tag_label\']')

		WebUI.openBrowser('')

		WebUI.navigateToUrl('http://localhost:8100/auth/landing')

		WebUI.click(registerByEmailButton)

		TestHelper.clickButton(loginWithEmailButton)

		WebUI.setText(emailInput, email)

		TestHelper.clickButton(loginEmailSubmitButton)

		WebUI.setText(passwordInputId, password)

		TestHelper.clickButton(findTestObject('Page_Ionic App/button_Create Account'))

		WebUI.setText(userInputFirstName,  firstname)

		WebUI.setText(userInputLastName, lastname)

		TestHelper.clickButton(userSaveGeneralDetails)

		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='user_city_of_origin']"))

		WebUI.setText(TestHelper.getObjectById("//input[@id='city-search-input']"), 'Aaba')

		TestHelper.clickButton(cityOrigineDiv)

		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='user_city_of_residence']"))

		WebUI.setText(TestHelper.getObjectById("//input[@id='city-search-input']"), 'Aaba')

		TestHelper.clickButton(cityResidenceDiv)

		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='save_user_info']"))

		TestObject manageProfilSection = TestHelper.getObjectById("//div[@id='manageProfileId']")

		WebUI.waitForElementVisible(manageProfilSection, 5)

		WebUI.delay(20)
	}

}
