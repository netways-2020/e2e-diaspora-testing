import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import org.openqa.selenium.WebElement as WebElement

import internal.GlobalVariable

import java.text.DateFormat
import java.text.SimpleDateFormat

public class UserActionsHelper {

	def static addPost(String postTitle, String postDescription) {
		TestObject postTitleObject = TestHelper.getObjectById("//input[@id='status_title_id']");
		TestObject postDescriptionObject =  TestHelper.getObjectById("//input[@id='status_description_id']");
		TestObject changeImageAction = TestHelper.getObjectById("//div[@id='change_image_action']");

		TestHelper.setInputValue(postTitleObject, postTitle);
		TestHelper.setInputValue(postDescriptionObject, postDescription);
		TestHelper.clickButton(postDescriptionObject)
		TestHelper.clickButton(changeImageAction);

		// Image
		TestObject changeImageGalleryAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='image_gallery_action']")
		String userPath = System.getProperty("user.dir")
		String imagePath = userPath + "//Image//test_image.png"

		WebUI.uploadFile(changeImageGalleryAction, imagePath)

		TestObject selectImageAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='cropper_select_image']")
		TestHelper.clickButton(selectImageAction);

		// Add Status Action
		TestObject addStatusAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='add_status_action']")
		TestHelper.clickButton(addStatusAction);

		WebUI.delay(4)
	}


	def static editPost(String postTitle, String postDescription) {
		TestObject postTitleObject = TestHelper.getObjectById("//input[@id='status_title_id']");
		TestObject postDescriptionObject =  TestHelper.getObjectById("//input[@id='status_description_id']");
		TestObject changeImageAction = TestHelper.getObjectById("//div[@id='change_image_action']");

		TestHelper.setInputValue(postTitleObject, postTitle);
		TestHelper.setInputValue(postDescriptionObject, postDescription);
		TestHelper.clickButton(postDescriptionObject)
		TestHelper.clickButton(changeImageAction);

		// Image
		TestObject changeImageGalleryAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='image_gallery_action']")
		String userPath = System.getProperty("user.dir")
		String imagePath = userPath + "//Image//test_image.png"

		WebUI.uploadFile(changeImageGalleryAction, imagePath)

		TestObject selectImageAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='cropper_select_image']")
		TestHelper.clickButton(selectImageAction);

		// Add Status Action
		TestObject addStatusAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='add_status_action']")
		TestHelper.clickButton(addStatusAction);

		WebUI.delay(4)
	}

	def static addTownPost(String postTitle, String postDescription) {
		TestObject postTitleObject = TestHelper.getObjectById("//input[@id='status_title_id']");
		TestObject postDescriptionObject =  TestHelper.getObjectById("//input[@id='status_description_id']");
		TestObject changeImageAction = TestHelper.getObjectById("//div[@id='change_image_action']");

		TestHelper.setInputValue(postTitleObject, postTitle);
		TestHelper.setInputValue(postDescriptionObject, postDescription);
		TestHelper.clickButton(postDescriptionObject)
		TestHelper.clickButton(changeImageAction);

		// Image
		TestObject changeImageGalleryAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='image_gallery_action']")
		String userPath = System.getProperty("user.dir")
		String imagePath = userPath + "//Image//test_image.png"

		WebUI.uploadFile(changeImageGalleryAction, imagePath)

		TestObject selectImageAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='cropper_select_image']")
		TestHelper.clickButton(selectImageAction);

		// Add Status Action
		TestObject addStatusAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='add_status_action']")
		TestHelper.clickButton(addStatusAction);

		WebUI.delay(4)
	}

	def static addMedia(String caption) {
		TestObject captionObject = TestHelper.getObjectById("//textarea[@id='caption-section']");
		TestObject changeImageAction = TestHelper.getObjectById("//div[@id='change_image_action']");

		TestHelper.setInputValue(captionObject, caption);
		TestHelper.clickButton(changeImageAction);

		// Image
		TestObject changeImageGalleryAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='image_gallery_action']")
		String userPath = System.getProperty("user.dir")
		String imagePath = userPath + "//Image//test_image.png"

		WebUI.uploadFile(changeImageGalleryAction, imagePath)

		TestObject selectImageAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='cropper_select_image']")
		TestHelper.clickButton(selectImageAction);

		// Add Status Action
		TestObject addStatusAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='add_media_action']")
		TestHelper.clickButton(addStatusAction);

		WebUI.delay(4)
	}

	def static addMediaByYoutube(String youtubeUrl) {
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='change_image_action']"));
		TestHelper.clickButton(TestHelper.getObjectById("//a[@id='youtube_camera_action']"));

		TestObject youtubeInput = TestHelper.getObjectById("//input[@id='youtube-url']");
		TestHelper.setInputValue(youtubeInput, youtubeUrl);


		WebElement addYoutubeAction = TestHelper.getItem('addYoutubeAction', 0)
		addYoutubeAction.click();

		WebUI.delay(2);

		// Add Status Action
		TestObject addStatusAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='add_media_action']")
		TestHelper.clickButton(addStatusAction);

		WebUI.delay(4)
	}

	def static addTownMedia(String caption) {
		TestObject captionObject = TestHelper.getObjectById("//textarea[@id='caption-section']");
		TestObject changeImageAction = TestHelper.getObjectById("//div[@id='change_image_action']");

		TestHelper.setInputValue(captionObject, caption);
		TestHelper.clickButton(changeImageAction);

		// Image
		TestObject changeImageGalleryAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='image_gallery_action']")
		String userPath = System.getProperty("user.dir")
		String imagePath = userPath + "//Image//test_image.png"

		WebUI.uploadFile(changeImageGalleryAction, imagePath)

		TestObject selectImageAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='cropper_select_image']")
		TestHelper.clickButton(selectImageAction);

		// Add Status Action
		TestObject addStatusAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='add_media_action']")
		TestHelper.clickButton(addStatusAction);

		WebUI.delay(4)
	}

	def static removeMedia() {
		TestObject deleteMediaAction = TestHelper.getObjectById("//a[@id='Delete_user_action']");
		TestHelper.clickButton(deleteMediaAction);
		WebUI.delay(1);
		WebElement deleteImageConfirmation = TestHelper.getItem('delete_image_confirmation', 0)
		deleteImageConfirmation.click();

		TestObject backButtonAction = TestHelper.getObjectById("//div[@id='galler_images_back_button']");

		boolean backButtonActionExist = TestHelper.isElementPresent(backButtonAction);
		if(backButtonActionExist) {
			TestHelper.clickButton(backButtonAction);
		}
	}

	def static removeEmptYMedia() {
		TestObject deleteMediaAction = TestHelper.getObjectById("//a[@id='Delete_user_action']");
		TestHelper.clickButton(deleteMediaAction);
		WebUI.delay(1);
		WebElement deleteImageConfirmation = TestHelper.getItem('delete_image_confirmation', 0)
		deleteImageConfirmation.click();

		TestObject backButtonAction = TestHelper.getObjectById("//div[@id='back_button_gallery']");

		boolean backButtonActionExist = TestHelper.isElementPresent(backButtonAction);
		if(backButtonActionExist) {
			TestHelper.clickButton(backButtonAction);
		}
	}
	def static addEvent(String eventTitle, String eventDescription, String eventLocation) {
		TestObject eventTitleObject = TestHelper.getObjectById("//input[@id='event_title_id']");
		TestObject eventLocationObject =  TestHelper.getObjectById("//input[@id='event_location_id']");
		TestObject eventDescriptionObject =  TestHelper.getObjectById("//input[@id='event_description_id']");
		TestObject eventStartObject =  TestHelper.getObjectById("//input[@id='event_start_date']");
		TestObject eventEndObject =  TestHelper.getObjectById("//input[@id='event_end_date']");
		TestObject changeImageAction = TestHelper.getObjectById("//div[@id='change_image_action']");

		TestHelper.setInputValue(eventTitleObject, eventTitle);
		TestHelper.setInputValue(eventLocationObject, eventLocation);
		TestHelper.setInputValue(eventDescriptionObject, eventDescription);
		TestHelper.clickButton(eventDescriptionObject)

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("mm/dd/yyyy")

		Date startDate = new SimpleDateFormat("mm/dd/yyyy").parse("01/01/2020");
		Date endDate = new SimpleDateFormat("mm/dd/yyyy").parse("01/01/2030");

		TestHelper.setInputValue(eventStartObject, simpleDateFormat.format(startDate));
		TestHelper.setInputValue(eventEndObject, simpleDateFormat.format(endDate));

		TestHelper.clickButton(changeImageAction);

		// Image
		TestObject changeImageGalleryAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='image_gallery_action']")
		String userPath = System.getProperty("user.dir")
		String imagePath = userPath + "//Image//test_image.png"

		WebUI.uploadFile(changeImageGalleryAction, imagePath)

		TestObject selectImageAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='cropper_select_image']")
		TestHelper.clickButton(selectImageAction);

		// Add Status Action
		TestObject addEventAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='add_event_action']")
		TestHelper.clickButton(addEventAction);

		WebUI.delay(4)
	}



	def static editEvent(String eventTitle, String eventDescription, String eventLocation) {
		TestObject eventTitleObject = TestHelper.getObjectById("//input[@id='event_title_id']");
		TestObject eventLocationObject =  TestHelper.getObjectById("//input[@id='event_location_id']");
		TestObject eventDescriptionObject =  TestHelper.getObjectById("//input[@id='event_description_id']");
		TestObject eventStartObject =  TestHelper.getObjectById("//input[@id='event_start_date']");
		TestObject eventEndObject =  TestHelper.getObjectById("//input[@id='event_end_date']");
		TestObject changeImageAction = TestHelper.getObjectById("//div[@id='change_image_action']");

		TestHelper.setInputValue(eventTitleObject, eventTitle);
		TestHelper.setInputValue(eventLocationObject, eventLocation);
		TestHelper.setInputValue(eventDescriptionObject, eventDescription);
		TestHelper.clickButton(eventDescriptionObject)

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("mm/dd/yyyy")

		Date startDate = new SimpleDateFormat("mm/dd/yyyy").parse("01/01/2020");
		Date endDate = new SimpleDateFormat("mm/dd/yyyy").parse("01/01/2030");

		TestHelper.setInputValue(eventStartObject, simpleDateFormat.format(startDate));
		TestHelper.setInputValue(eventEndObject, simpleDateFormat.format(endDate));

		TestHelper.clickButton(changeImageAction);

		// Image
		TestObject changeImageGalleryAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='image_gallery_action']")
		String userPath = System.getProperty("user.dir")
		String imagePath = userPath + "//Image//test_image.png"

		WebUI.uploadFile(changeImageGalleryAction, imagePath)

		TestObject selectImageAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='cropper_select_image']")
		TestHelper.clickButton(selectImageAction);

		// Add Status Action
		TestObject addEventAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='add_event_action']")
		TestHelper.clickButton(addEventAction);

		WebUI.delay(4)
	}

	def static addTownEvent(String eventTitle, String eventDescription, String eventLocation) {
		TestObject eventTitleObject = TestHelper.getObjectById("//input[@id='event_title_id']");
		TestObject eventLocationObject =  TestHelper.getObjectById("//input[@id='event_location_id']");
		TestObject eventDescriptionObject =  TestHelper.getObjectById("//input[@id='event_description_id']");
		TestObject eventStartObject =  TestHelper.getObjectById("//input[@id='event_start_date']");
		TestObject eventEndObject =  TestHelper.getObjectById("//input[@id='event_end_date']");
		TestObject changeImageAction = TestHelper.getObjectById("//div[@id='change_image_action']");

		TestHelper.setInputValue(eventTitleObject, eventTitle);
		TestHelper.setInputValue(eventLocationObject, eventLocation);
		TestHelper.setInputValue(eventDescriptionObject, eventDescription);
		TestHelper.clickButton(eventDescriptionObject)

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("mm/dd/yyyy")

		Date startDate = new SimpleDateFormat("mm/dd/yyyy").parse("01/01/2020");
		Date endDate = new SimpleDateFormat("mm/dd/yyyy").parse("01/01/2030");

		TestHelper.setInputValue(eventStartObject, simpleDateFormat.format(startDate));
		TestHelper.setInputValue(eventEndObject, simpleDateFormat.format(endDate));

		TestHelper.clickButton(changeImageAction);

		// Image
		TestObject changeImageGalleryAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='image_gallery_action']")
		String userPath = System.getProperty("user.dir")
		String imagePath = userPath + "//Image//test_image.png"

		WebUI.uploadFile(changeImageGalleryAction, imagePath)

		TestObject selectImageAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='cropper_select_image']")
		TestHelper.clickButton(selectImageAction);

		// Add Status Action
		TestObject addEventAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='add_event_action']")
		TestHelper.clickButton(addEventAction);

		WebUI.delay(4)
	}

	def static sendReport(String reportDescription, String reportContact) {
		TestObject reportDescriptionInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//textarea[@id='report-description']")
		TestObject reportDescriptionContact = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//textarea[@id='report-contact']")

		TestHelper.setInputValue(reportDescriptionInput, reportDescription)
		TestHelper.setInputValue(reportDescriptionContact, reportContact)

		TestObject reportProfileAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='report-send-action']")
		TestHelper.clickButton(reportProfileAction);
		WebUI.delay(3);
	}

	def static addNeedOrOffer(String needName, String needDescription) {
		TestObject needNameInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='need_name_id']")
		TestHelper.setInputValue(needNameInput, needName)

		// Sector
		TestObject sector_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='need_sector_id']")
		TestHelper.clickButton(sector_button)
		TestObject agricultureSector = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Agriculture, Forestry and Fishing_sector']")
		TestObject dairySector = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Dairy_sector']")

		TestHelper.clickButton(agricultureSector)
		TestHelper.clickButton(dairySector)


		// Country Availability
		TestObject country_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='need_market_availability_id']")
		TestHelper.clickButton(country_button)
		TestObject afghanstanCountry = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//label[@id='Afghanistan_item']")
		TestObject angolaCountry = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//label[@id='Angola_item']")
		TestObject saveSelectedCountries = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='save_selected_item_action']")

		if(TestHelper.isElementPresent(afghanstanCountry)) {
			WebUI.click(afghanstanCountry)
		}
		if(TestHelper.isElementPresent(angolaCountry)) {
			WebUI.click(angolaCountry)
		}

		WebUI.click(saveSelectedCountries)


		// Keyword
		TestObject keyword_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='need_keywords_id']")

		TestHelper.clickButton(keyword_button)
		TestObject activitiesKeyword = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//label[@id='Activities_item']")
		TestObject adventuresKeyword = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//label[@id='Adventures_item']")
		TestObject saveSelectedKeyword = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='save_selected_item_action']")

		if(TestHelper.isElementPresent(activitiesKeyword)) {
			WebUI.click(activitiesKeyword)
		}
		if(TestHelper.isElementPresent(adventuresKeyword)) {
			WebUI.click(adventuresKeyword)
		}
		WebUI.click(saveSelectedKeyword)


		// Description
		TestObject needDescriptionInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='need_description_id']")
		TestHelper.setInputValue(needDescriptionInput, needDescription)


		// Image
		TestObject changeImageAction = TestHelper.getObjectById("//div[@id='change_image_action']");
		TestHelper.clickButton(changeImageAction);
		TestObject changeImageGalleryAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='image_gallery_action']")
		String userPath = System.getProperty("user.dir")
		String imagePath = userPath + "//Image//test_image.png"

		WebUI.uploadFile(changeImageGalleryAction, imagePath)

		TestObject selectImageAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='cropper_select_image']")
		TestHelper.clickButton(selectImageAction);

		TestObject addNewNeedAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='add_new_need_id']")
		TestHelper.clickButton(addNewNeedAction);
		WebUI.delay(4);
	}

	def static editNeedOrOffer(String needName, String needDescription) {
		TestObject needNameInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='need_name_id']")
		TestHelper.setInputValue(needNameInput, needName)

		// Sector
		TestObject sector_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='need_sector_id']")
		TestHelper.clickButton(sector_button)
		TestObject agricultureSector = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Consumer Goods_sector']")
		TestObject dairySector = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Cosmetics_sector']")

		TestHelper.clickButton(agricultureSector)
		TestHelper.clickButton(dairySector)


		// Country Availability
		TestObject country_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='need_market_availability_id']")
		TestHelper.clickButton(country_button)
		TestObject albaniaCountry = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//label[@id='Albania_item']")

		TestObject saveSelectedCountries = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='save_selected_item_action']")

		if(TestHelper.isElementPresent(albaniaCountry)) {
			WebUI.click(albaniaCountry)
		}
		WebUI.click(saveSelectedCountries)


		// Keyword
		TestObject keyword_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='need_keywords_id']")

		TestHelper.clickButton(keyword_button)
		TestObject printingKeyword = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//label[@id='3D Printing_item']")

		TestObject saveSelectedKeyword = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='save_selected_item_action']")

		if(TestHelper.isElementPresent(printingKeyword)) {
			WebUI.click(printingKeyword)
		}
		WebUI.click(saveSelectedKeyword)


		// Description
		TestObject needDescriptionInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='need_description_id']")
		TestHelper.setInputValue(needDescriptionInput, needDescription)


		TestObject addNewNeedAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='add_new_need_id']")
		TestHelper.clickButton(addNewNeedAction);
		WebUI.delay(3);
	}


}
