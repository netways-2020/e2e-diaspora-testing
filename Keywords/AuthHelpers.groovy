import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

public class AuthHelpers {

//General Login	
def static  generalRegistration(originCity, residenceCity, firstName, LastName, email) {
	String password = 'Test@123'
	TestObject cityOrigineDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\''+ originCity) + '\']')
	TestObject cityResidenceDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\''+ residenceCity) + '\']')
	WebUI.openBrowser('')
	WebUI.navigateToUrl('http://localhost:8100/auth/landing')
	WebUI.click(TestHelper.getObjectById("//button[@id='registerButton']"))
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='login-with-email-button']"))
	WebUI.setText(TestHelper.getObjectById('//input[@id=\'email-input\']'), email)
	TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'login-email-submit-button\']'))
	WebUI.setText(TestHelper.getObjectById('//input[@id=\'passwordInputId\']'), password)
	TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'submit-button\']'))
	WebUI.setText(TestHelper.getObjectById('//input[@id=\'user-input-first-name\']'),  firstName)
	WebUI.setText(TestHelper.getObjectById('//input[@id=\'user-input-last-name\']'), LastName)
	TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'user_save_general_details\']'))
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='user_city_of_origin']"))
	WebUI.setText(TestHelper.getObjectById("//input[@id='city-search-input']"), originCity)
	TestHelper.clickButton(cityOrigineDiv)
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='user_city_of_residence']"))
	WebUI.setText(TestHelper.getObjectById("//input[@id='city-search-input']"), originCity)
	TestHelper.clickButton(cityResidenceDiv)
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='save_user_info']"))
	TestObject manageProfilSection = TestHelper.getObjectById("//div[@id='manageProfileId']")
	}
	
def static generaLogin (email, password, originCity) {
	TestObject cityOrigineDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + originCity) + '\']')
	WebUI.openBrowser('')
	WebUI.navigateToUrl('http://localhost:8100/auth/landing')

	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='loginButton']"))

	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='login-with-email-button']"))

	TestObject emailInput = TestHelper.getObjectById("//input[@id='email-input']")

	TestHelper.setInputValue(emailInput, email)

	TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'login-email-submit-button\']'))

	TestObject passwordInput = TestHelper.getObjectById('//input[@id=\'passwordInputId\']')

	TestHelper.setInputValue(passwordInput, password)

	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='submit-button']"))
}
	
//Town Login
	def static  townRegistration(originCity, residenceCity, firstName, LastName, email) {
		String password = 'Test@123'
		TestObject cityOrigineDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\''+ originCity) + '\']')
		TestObject cityResidenceDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\''+ residenceCity) + '\']')

		WebUI.openBrowser('')
		WebUI.navigateToUrl('http://localhost:8100/auth/landing')
		WebUI.click(TestHelper.getObjectById("//button[@id='registerButton']"))
		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='login-with-email-button']"))
		WebUI.setText(TestHelper.getObjectById('//input[@id=\'email-input\']'), email)
		TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'login-email-submit-button\']'))
		WebUI.setText(TestHelper.getObjectById('//input[@id=\'passwordInputId\']'), password)
		TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'submit-button\']'))
		WebUI.setText(TestHelper.getObjectById('//input[@id=\'user-input-first-name\']'),  firstName)
		WebUI.setText(TestHelper.getObjectById('//input[@id=\'user-input-last-name\']'), LastName)
		TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'user_save_general_details\']'))
		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='user_city_of_origin']"))
		WebUI.setText(TestHelper.getObjectById("//input[@id='city-search-input']"), originCity)
		TestHelper.clickButton(cityOrigineDiv)
		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='user_city_of_residence']"))
		WebUI.setText(TestHelper.getObjectById("//input[@id='city-search-input']"), originCity)
		TestHelper.clickButton(cityResidenceDiv)
		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='save_user_info']"))
		TestObject manageProfilSection = TestHelper.getObjectById("//div[@id='manageProfileId']")
		WebUI.waitForElementVisible(manageProfilSection, 5)
		//go to the profile from bottom tab
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_tab_id']"))
		WebUI.scrollToElement(cityOrigineDiv, 3)
		// go to twon from the profile
		TestHelper.clickButton(cityOrigineDiv)
	}

	def static townLogin (email, password, originCity) {
		TestObject cityOrigineDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + originCity) + '\']')
		WebUI.openBrowser('')
		WebUI.navigateToUrl('http://localhost:8100/auth/landing')

		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='loginButton']"))

		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='login-with-email-button']"))

		TestObject emailInput = TestHelper.getObjectById("//input[@id='email-input']")

		TestHelper.setInputValue(emailInput, email)

		TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'login-email-submit-button\']'))

		TestObject passwordInput = TestHelper.getObjectById('//input[@id=\'passwordInputId\']')

		TestHelper.setInputValue(passwordInput, password)

		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='submit-button']"))
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_tab_id']"))
		WebUI.scrollToElement(cityOrigineDiv, 3)
		TestHelper.clickButton(cityOrigineDiv)
	}
}
