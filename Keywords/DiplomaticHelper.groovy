import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

public class DiplomaticHelper {
	def static checkDiplomaticGeneralInfoFieldsEmptyState() {
		boolean successEmptyFields = true;


		TestObject no_short_description_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_diplomatic_introduction_id_Add A Short Description']")
		TestObject no_address_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_diplomatic_address_id_Add An Address']")

		TestObject no_contact_phone_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_diplomatic_contact_phone_id_Add Phone Number']")
		TestObject no_contact_email_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_diplomatic_contact_email_id_Add Email Address']")
		TestObject no_contact_website_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_diplomatic_contact_website_id_Add A Website']")

		TestObject no_social_media_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_social_media_id']")

		// Check Object Exist
		if(!TestHelper.isElementPresent(no_short_description_object)) {
			successEmptyFields = false;
		} else if(!TestHelper.isElementPresent(no_address_object)) {
			successEmptyFields = false;
		}
		else if(!TestHelper.isElementPresent(no_contact_phone_object)) {
			successEmptyFields = false;
		} else if(!TestHelper.isElementPresent(no_contact_email_object)) {
			successEmptyFields = false;
		}  else if(!TestHelper.isElementPresent(no_contact_website_object)) {
			successEmptyFields = false;
		} else if(!TestHelper.isElementPresent(no_social_media_object)) {
			successEmptyFields = false;
		}

		if(successEmptyFields) {
			println("Empty state for diplomatic general info tested successfully");
		} else {
			TestHelper.thrownException("Error during clear user info")
		}
	}
}
