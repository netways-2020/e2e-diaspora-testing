import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import org.openqa.selenium.WebElement as WebElement

import internal.GlobalVariable

public class UserRoleHelper {

	// Company
	def static createCompanyAsEmployee(String companyName, String roleName) {
		TestObject searchForCompanyInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='searchForCompanyInput']")
		TestHelper.setInputValue(searchForCompanyInput, companyName)

		TestObject createNewCompanyAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='create_new_company_action']")
		TestHelper.clickButton(createNewCompanyAction)

		WebUI.delay(1)

		// Company Type
		TestObject company_type_Button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='company-type']")
		TestHelper.clickButton(company_type_Button)
		WebUI.delay(1);

		TestObject business_company_type = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Business_item']")
		TestHelper.clickButton(business_company_type)

		// Sector
		TestObject sector_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='sector-section']")
		TestHelper.clickButton(sector_button)
		WebUI.delay(1);
		TestObject agricultureSector = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Agriculture, Forestry and Fishing_sector']")
		WebUI.delay(1);
		TestObject dairySector = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Dairy_sector']")

		TestHelper.clickButton(agricultureSector)
		TestHelper.clickButton(dairySector)

		TestObject create_general_info_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='create-company-general-info']")
		TestHelper.clickButton(create_general_info_button)


		// Role Info
		TestObject positionTypeButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='business-role-position-type']")
		WebUI.click(positionTypeButton)

		TestObject positionTypeValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Employee_item']")
		WebUI.click(positionTypeValue)


		TestObject positionNameInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='business-role-position-name']")
		WebUI.setText(positionNameInput, roleName)

		TestObject createCompanyRoleInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='create-company-role-info']")
		WebUI.click(createCompanyRoleInfo)
		WebUI.delay(2);
	}

	def static createCompanyAsMentor(String companyName, String roleName) {
		TestObject searchForCompanyInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='searchForCompanyInput']")
		TestHelper.setInputValue(searchForCompanyInput, companyName)

		TestObject createNewCompanyAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='create_new_company_action']")
		TestHelper.clickButton(createNewCompanyAction)

		WebUI.delay(1)

		// Company Type
		TestObject company_type_Button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='company-type']")
		TestHelper.clickButton(company_type_Button)
		WebUI.delay(1);

		TestObject business_company_type = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Business_item']")
		TestHelper.clickButton(business_company_type)

		// Sector
		TestObject sector_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='sector-section']")
		TestHelper.clickButton(sector_button)
		WebUI.delay(1);
		TestObject agricultureSector = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Agriculture, Forestry and Fishing_sector']")
		WebUI.delay(1);
		TestObject dairySector = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Dairy_sector']")

		TestHelper.clickButton(agricultureSector)
		TestHelper.clickButton(dairySector)

		TestObject create_general_info_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='create-company-general-info']")
		TestHelper.clickButton(create_general_info_button)


		// Role Info
		TestObject positionTypeButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='business-role-position-type']")
		WebUI.click(positionTypeButton)

		TestObject positionTypeValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Mentor_item']")
		WebUI.click(positionTypeValue)


		TestObject positionNameInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='business-role-position-name']")
		WebUI.setText(positionNameInput, roleName)

		TestObject createCompanyRoleInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='create-company-role-info']")
		WebUI.click(createCompanyRoleInfo)
		WebUI.delay(2);
	}

	def static createCompanyAsAdvisor(String companyName, String roleName) {
		TestObject searchForCompanyInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='searchForCompanyInput']")
		TestHelper.setInputValue(searchForCompanyInput, companyName)

		TestObject createNewCompanyAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='create_new_company_action']")
		TestHelper.clickButton(createNewCompanyAction)

		WebUI.delay(1)

		// Company Type
		TestObject company_type_Button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='company-type']")
		TestHelper.clickButton(company_type_Button)
		WebUI.delay(1);

		TestObject business_company_type = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Business_item']")
		TestHelper.clickButton(business_company_type)

		// Sector
		TestObject sector_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='sector-section']")
		TestHelper.clickButton(sector_button)
		WebUI.delay(1);
		TestObject agricultureSector = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Agriculture, Forestry and Fishing_sector']")
		WebUI.delay(1);
		TestObject dairySector = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Dairy_sector']")

		TestHelper.clickButton(agricultureSector)
		TestHelper.clickButton(dairySector)

		TestObject create_general_info_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='create-company-general-info']")
		TestHelper.clickButton(create_general_info_button)


		// Role Info
		TestObject positionTypeButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='business-role-position-type']")
		WebUI.click(positionTypeButton)

		TestObject positionTypeValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Advisor_item']")
		WebUI.click(positionTypeValue)


		TestObject positionNameInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='business-role-position-name']")
		WebUI.setText(positionNameInput, roleName)

		TestObject createCompanyRoleInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='create-company-role-info']")
		WebUI.click(createCompanyRoleInfo)
		WebUI.delay(2);
	}


	def static createCompany(String companyName, String roleName) {

		TestObject searchForCompanyInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='searchForCompanyInput']")
		TestHelper.setInputValue(searchForCompanyInput, companyName)

		TestObject createNewCompanyAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='create_new_company_action']")
		TestHelper.clickButton(createNewCompanyAction)

		WebUI.delay(1)

		// Company Type
		TestObject company_type_Button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='company-type']")
		TestHelper.clickButton(company_type_Button)
		WebUI.delay(1);

		TestObject business_company_type = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Business_item']")
		TestHelper.clickButton(business_company_type)
		WebUI.delay(1);

		// Sector
		TestObject sector_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='sector-section']")
		TestHelper.clickButton(sector_button)
		WebUI.delay(1);
		TestObject agricultureSector = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Agriculture, Forestry and Fishing_sector']")
		WebUI.delay(1);
		TestObject dairySector = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Dairy_sector']")

		TestHelper.clickButton(agricultureSector)
		TestHelper.clickButton(dairySector)

		TestObject create_general_info_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='create-company-general-info']")
		TestHelper.clickButton(create_general_info_button)


		// Role Info
		TestObject positionTypeButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='business-role-position-type']")
		WebUI.click(positionTypeButton)

		TestObject positionTypeValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Person in Charge_item']")
		WebUI.click(positionTypeValue)


		TestObject positionNameInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='business-role-position-name']")
		WebUI.setText(positionNameInput, roleName)

		TestObject createCompanyRoleInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='create-company-role-info']")
		WebUI.click(createCompanyRoleInfo)
		WebUI.delay(2);
	}

	def static createCompanyWithImage(String companyName, String roleName) {
		TestObject searchForCompanyInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='searchForCompanyInput']")
		TestHelper.setInputValue(searchForCompanyInput, companyName)

		TestObject createNewCompanyAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='create_new_company_action']")
		TestHelper.clickButton(createNewCompanyAction)

		WebUI.delay(1)


		// Image
		TestObject changeImageAction = TestHelper.getObjectById("//div[@id='change_image_action']");
		TestHelper.clickButton(changeImageAction);
		TestObject changeImageGalleryAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='image_gallery_action']")
		String userPath = System.getProperty("user.dir")
		String imagePath = userPath + "//Image//test_image.png"
		println(imagePath)
		WebUI.uploadFile(changeImageGalleryAction, imagePath)

		TestObject selectImageAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='cropper_select_image']")
		TestHelper.clickButton(selectImageAction);


		// Company Type
		TestObject company_type_Button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='company-type']")
		TestHelper.clickButton(company_type_Button)

		TestObject business_company_type = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Business_item']")
		TestHelper.clickButton(business_company_type)

		// Sector
		TestObject sector_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='sector-section']")
		TestHelper.clickButton(sector_button)
		TestObject agricultureSector = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Agriculture, Forestry and Fishing_sector']")
		TestObject dairySector = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Dairy_sector']")

		TestHelper.clickButton(agricultureSector)
		TestHelper.clickButton(dairySector)

		TestObject create_general_info_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='create-company-general-info']")
		TestHelper.clickButton(create_general_info_button)


		// Role Info
		TestObject positionTypeButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='business-role-position-type']")
		WebUI.click(positionTypeButton)

		TestObject positionTypeValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Person in Charge_item']")
		WebUI.click(positionTypeValue)


		TestObject positionNameInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='business-role-position-name']")
		WebUI.setText(positionNameInput, roleName)

		TestObject createCompanyRoleInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='create-company-role-info']")
		WebUI.click(createCompanyRoleInfo)
		WebUI.delay(2);
	}

	def static joinCompany(String roleName) {
		TestObject positionTypeButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='business-role-position-type']")
		WebUI.click(positionTypeButton)

		TestObject positionTypeValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Employee_item']")
		WebUI.click(positionTypeValue)


		TestObject positionNameInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='business-role-position-name']")
		WebUI.setText(positionNameInput, roleName)

		TestObject createCompanyRoleInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='join-company-action']")
		WebUI.click(createCompanyRoleInfo)
	}

	def static searchForCompanyAndClickAction(companyName) {
		TestObject searchForCompanyInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='searchForCompanyInput']")
		TestHelper.setInputValue(searchForCompanyInput, companyName);

		WebUI.delay(1)

		String company_action_id = companyName + '_filter_company_action';
		TestObject companyActionObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + company_action_id + "']")
		TestHelper.clickButton(companyActionObject);

		WebElement linkFromParentConfirmation = TestHelper.getItem('link_company_confirmation', 0)
		linkFromParentConfirmation.click();
	}

	def static createCompanyAndGoToTarget() {
		TestObject addBusinessRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'add-new-business-role-action\']')
		WebUI.click(addBusinessRoleAction)
		createNewCompany()
	}

	def static createNewCompany() {
		String companyName = 'Netways' + new Date().getTime()
		String roleName = 'Founder'

		UserRoleHelper.createCompany(companyName, roleName)
		WebUI.delay(2);
		String companyTarget = companyName + '-' + roleName + '-role'
		TestObject selectedCompany = TestHelper.getObjectById("//div[@id='"+companyTarget+"']")
		TestHelper.clickButton(selectedCompany);
	}

	// Organization
	def static createOrganizationAsEmploye(String organizationName, String roleName) {

		TestObject searchForOrganizationInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='searchForOrganizationInput']")
		TestHelper.setInputValue(searchForOrganizationInput, organizationName)

		TestObject createNewOrganizationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='create_new_organization_action']")
		WebUI.click(createNewOrganizationAction)

		WebUI.delay(1)

		TestObject organizationType = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='organization-type']")
		TestHelper.clickButton(organizationType)

		TestObject organizationTypeValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Academy_item']")
		TestHelper.clickButton(organizationTypeValue)

		TestObject organizationCategory = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='organization-category']")
		TestHelper.clickButton(organizationCategory)

		WebUI.delay(1);
		TestObject organizationCategoryValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Artistic_item']")
		TestHelper.clickButton(organizationCategoryValue)
		WebUI.delay(1);

		TestObject create_general_info_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='create-organization-general-info']")
		TestHelper.clickButton(create_general_info_button)

		// Role Info

		TestObject positionTypeButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='network-role-position-type']")
		WebUI.click(positionTypeButton)

		TestObject positionTypeValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Member_item']")
		WebUI.click(positionTypeValue)

//		TestObject positionNameInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='organization-role-position-name']")
//		WebUI.setText(positionNameInput, roleName)

		TestObject createOrganizationRoleInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='create-organization-role-info']")
		WebUI.click(createOrganizationRoleInfo)
		WebUI.delay(2);
	}

	def static createOrganizationAsBoardOfDirectory(String organizationName, String roleName) {
		TestObject searchForOrganizationInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='searchForOrganizationInput']")
		TestHelper.setInputValue(searchForOrganizationInput, organizationName)

		TestObject createNewOrganizationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='create_new_organization_action']")
		WebUI.click(createNewOrganizationAction)

		WebUI.delay(1)

		TestObject organizationType = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='organization-type']")
		TestHelper.clickButton(organizationType)

		TestObject organizationTypeValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Academy_item']")
		TestHelper.clickButton(organizationTypeValue)

		TestObject organizationCategory = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='organization-category']")
		TestHelper.clickButton(organizationCategory)

		WebUI.delay(1);
		TestObject organizationCategoryValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Artistic_item']")
		TestHelper.clickButton(organizationCategoryValue)
		WebUI.delay(1);

		TestObject create_general_info_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='create-organization-general-info']")
		TestHelper.clickButton(create_general_info_button)

		// Role Info

		TestObject positionTypeButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='network-role-position-type']")
		WebUI.click(positionTypeButton)

		TestObject positionTypeValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Board of Directors_item']")
		WebUI.click(positionTypeValue)

		TestObject positionNameInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='organization-role-position-name']")
		WebUI.setText(positionNameInput, roleName)

		TestObject createOrganizationRoleInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='create-organization-role-info']")
		WebUI.click(createOrganizationRoleInfo)
		WebUI.delay(2);
	}

	def static createOrganizationAsCommitteeOrBoardMember(String organizationName, String roleName, String committeeName) {
		TestObject searchForOrganizationInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='searchForOrganizationInput']")
		TestHelper.setInputValue(searchForOrganizationInput, organizationName)

		TestObject createNewOrganizationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='create_new_organization_action']")
		WebUI.click(createNewOrganizationAction)

		WebUI.delay(1)

		TestObject organizationType = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='organization-type']")
		TestHelper.clickButton(organizationType)

		TestObject organizationTypeValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Academy_item']")
		TestHelper.clickButton(organizationTypeValue)

		TestObject organizationCategory = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='organization-category']")
		TestHelper.clickButton(organizationCategory)

		WebUI.delay(1);
		TestObject organizationCategoryValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Artistic_item']")
		TestHelper.clickButton(organizationCategoryValue)
		WebUI.delay(1);

		TestObject create_general_info_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='create-organization-general-info']")
		TestHelper.clickButton(create_general_info_button)

		// Role Info

		TestObject positionTypeButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='network-role-position-type']")
		WebUI.click(positionTypeButton)

		TestObject positionTypeValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Committee or Board Member_item']")
		WebUI.click(positionTypeValue)

		TestObject committeeNameInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='committee-name-id']")
		WebUI.setText(committeeNameInput, committeeName)

		//		TestObject committeePositionTypeButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='committee-position-type']")
		//		WebUI.click(committeePositionTypeButton)
		//
		//		TestObject addCommitteeAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-committee-empty-state-title']")
		//		WebUI.click(addCommitteeAction)
		//
		//		TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='committee-name-input']"), committeeName);
		//		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='add-committee-name-action']"))
		//
		//		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='newCommittee_committee_item']"))


		TestObject positionNameInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='organization-role-position-name']")
		WebUI.setText(positionNameInput, roleName)

		TestObject createOrganizationRoleInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='create-organization-role-info']")
		WebUI.click(createOrganizationRoleInfo)
		WebUI.delay(2);
	}

	def static createOrganizationWithMentorType(String organizationName, String roleName) {

		TestObject searchForOrganizationInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='searchForOrganizationInput']")
		TestHelper.setInputValue(searchForOrganizationInput, organizationName)

		TestObject createNewOrganizationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='create_new_organization_action']")
		WebUI.click(createNewOrganizationAction)

		WebUI.delay(1);

		// Image
		TestObject changeImageAction = TestHelper.getObjectById("//div[@id='change_image_action']");
		TestHelper.clickButton(changeImageAction);
		TestObject changeImageGalleryAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='image_gallery_action']")
		String userPath = System.getProperty("user.dir")
		String imagePath = userPath + "//Image//test_image.png"
		println(imagePath)
		WebUI.uploadFile(changeImageGalleryAction, imagePath)

		TestObject selectImageAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='cropper_select_image']")
		TestHelper.clickButton(selectImageAction);


		TestObject organizationType = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='organization-type']")
		TestHelper.clickButton(organizationType)

		TestObject organizationTypeValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Mentor Hub_item']")
		TestHelper.clickButton(organizationTypeValue)

		TestObject organizationCategory = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='organization-category']")
		TestHelper.clickButton(organizationCategory)
		WebUI.delay(1);

		TestObject organizationCategoryValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Community_item']")
		TestHelper.clickButton(organizationCategoryValue)
		WebUI.delay(1);

		TestObject create_general_info_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='create-organization-general-info']")
		TestHelper.clickButton(create_general_info_button)

		// Role Info

		TestObject positionNameInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='organization-role-position-name']")
		WebUI.setText(positionNameInput, roleName)

		TestObject createOrganizationRoleInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='create-organization-role-info']")
		WebUI.click(createOrganizationRoleInfo)
		WebUI.delay(2);
	}

	def static createOrganization(String organizationName, String roleName) {

		TestObject searchForOrganizationInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='searchForOrganizationInput']")
		TestHelper.setInputValue(searchForOrganizationInput, organizationName)

		TestObject createNewOrganizationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='create_new_organization_action']")
		WebUI.click(createNewOrganizationAction)

		WebUI.delay(1)

		TestObject organizationType = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='organization-type']")
		TestHelper.clickButton(organizationType)

		TestObject organizationTypeValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Academy_item']")
		TestHelper.clickButton(organizationTypeValue)

		TestObject organizationCategory = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='organization-category']")
		TestHelper.clickButton(organizationCategory)
		WebUI.delay(1);

		TestObject organizationCategoryValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Artistic_item']")
		TestHelper.clickButton(organizationCategoryValue)
		WebUI.delay(1);

		TestObject create_general_info_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='create-organization-general-info']")
		TestHelper.clickButton(create_general_info_button)

		// Role Info

		TestObject positionTypeButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='network-role-position-type']")
		WebUI.click(positionTypeButton)

		TestObject positionTypeValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Person in Charge_item']")
		WebUI.click(positionTypeValue)

		TestObject positionNameInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='organization-role-position-name']")
		WebUI.setText(positionNameInput, roleName)

		TestObject createOrganizationRoleInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='create-organization-role-info']")
		WebUI.click(createOrganizationRoleInfo)
		WebUI.delay(2);
	}

	def static createOrganizationWithImage(String organizationName, String roleName) {
		TestObject searchForOrganizationInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='searchForOrganizationInput']")
		TestHelper.setInputValue(searchForOrganizationInput, organizationName)

		TestObject createNewOrganizationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='create_new_organization_action']")
		WebUI.click(createNewOrganizationAction)

		WebUI.delay(1)

		// Image
		TestObject changeImageAction = TestHelper.getObjectById("//div[@id='change_image_action']");
		TestHelper.clickButton(changeImageAction);
		TestObject changeImageGalleryAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='image_gallery_action']")
		String userPath = System.getProperty("user.dir")
		String imagePath = userPath + "//Image//test_image.png"
		println(imagePath)
		WebUI.uploadFile(changeImageGalleryAction, imagePath)

		TestObject selectImageAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='cropper_select_image']")
		TestHelper.clickButton(selectImageAction);

		TestObject organizationType = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='organization-type']")
		TestHelper.clickButton(organizationType)

		TestObject organizationTypeValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Academy_item']")
		TestHelper.clickButton(organizationTypeValue)

		TestObject organizationCategory = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='organization-category']")
		TestHelper.clickButton(organizationCategory)
		WebUI.delay(1);

		TestObject organizationCategoryValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Artistic_item']")
		TestHelper.clickButton(organizationCategoryValue)
		WebUI.delay(1);

		TestObject create_general_info_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='create-organization-general-info']")
		TestHelper.clickButton(create_general_info_button)

		// Role Info

		TestObject positionTypeButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='network-role-position-type']")
		WebUI.click(positionTypeButton)

		TestObject positionTypeValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Person in Charge_item']")
		WebUI.click(positionTypeValue)

		TestObject positionNameInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='organization-role-position-name']")
		WebUI.setText(positionNameInput, roleName)

		TestObject createOrganizationRoleInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='create-organization-role-info']")
		WebUI.click(createOrganizationRoleInfo)
		WebUI.delay(2);
	}

	def static createOrganizationAndGoToTarget() {
		TestObject addNetworkRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'add-new-network-role-action\']')
		WebUI.click(addNetworkRoleAction)
		createNewOrganization()
	}

	def static createNewOrganization() {
		String organizationName = 'Netways_Organization' + new Date().getTime();
		String roleName = 'Founder'

		UserRoleHelper.createOrganization(organizationName, roleName)
		WebUI.delay(2);
		String organizationTarget = organizationName + '-' + roleName + '-role'
		TestObject selectedOrganization = TestHelper.getObjectById("//div[@id='"+organizationTarget+"']")
		TestHelper.clickButton(selectedOrganization);
	}

	def static joinOrganization(String roleName) {
		TestObject positionTypeButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='network-role-position-type']")
		WebUI.click(positionTypeButton)

		TestObject positionTypeValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Member_item']")
		WebUI.click(positionTypeValue)


//		TestObject positionNameInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='organization-role-position-name']")
//		WebUI.setText(positionNameInput, roleName)

		TestObject createOrganizationRoleInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='join-organization-action']")
		WebUI.click(createOrganizationRoleInfo)
	}

	def static joinMentorOrganization() {
		TestObject sector_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='sector-action']")
		TestHelper.clickButton(sector_button)
		WebUI.delay(1);
		TestObject agricultureSector = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Agriculture, Forestry and Fishing_sector']")
		WebUI.delay(1);
		TestObject dairySector = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Dairy_sector']")

		TestHelper.clickButton(agricultureSector)
		TestHelper.clickButton(dairySector)


		TestObject createOrganizationRoleInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='join-organization-action']")
		WebUI.click(createOrganizationRoleInfo)
	}
	
	def static editMentorOrganization() {
		TestObject sector_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='sector-action']")
		TestHelper.clickButton(sector_button)
		WebUI.delay(1);
		TestObject artsSector = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Arts_sector']")
		WebUI.delay(1);
		TestObject animationSector = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Animation_sector']")

		TestHelper.clickButton(artsSector)
		TestHelper.clickButton(animationSector)


		TestObject createOrganizationRoleInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='update_network_role']")
		WebUI.click(createOrganizationRoleInfo)
	}

	def static joinOrganizationCommitee(String roleName) {

		TestObject positionNameInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='organization-role-position-name']")
		WebUI.setText(positionNameInput, roleName)

		TestObject createOrganizationRoleInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='join-organization-action']")
		WebUI.click(createOrganizationRoleInfo)
	}

	def static searchForOrganizationAndClickAction(organizationName) {
		TestObject searchForOrganizationInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='searchForOrganizationInput']")
		TestHelper.setInputValue(searchForOrganizationInput, organizationName);

		WebUI.delay(1)

		String organization_action_id = organizationName + '_filter_organization_action';
		TestObject organizationActionObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + organization_action_id + "']")
		TestHelper.clickButton(organizationActionObject);

		WebElement linkFromParentConfirmation = TestHelper.getItem('link_organization_confirmation', 0)
		linkFromParentConfirmation.click();
	}

	// Diplomatic
	def static joinDiplomatic(String roleName) {
		TestObject positionTypeButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='diplomatic-role-position-type']")
		WebUI.click(positionTypeButton)

		TestObject positionTypeValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Diaspora Member_item']")
		WebUI.click(positionTypeValue)


//		TestObject positionNameInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='diplomatic-position-name']")
//		WebUI.setText(positionNameInput, roleName)

		TestObject createDiplomaticRoleInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='join-diplomatic-action']")
		WebUI.click(createDiplomaticRoleInfo)
	}

	def static joinDiplomaticAsDiplomatOrKeyStaff(String roleName) {
		TestObject positionTypeButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='diplomatic-role-position-type']")
		WebUI.click(positionTypeButton)

		TestObject positionTypeValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Diplomats & Key Staff_item']")
		WebUI.click(positionTypeValue)


		TestObject positionNameInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='diplomatic-position-name']")
		WebUI.setText(positionNameInput, roleName)

		TestObject createDiplomaticRoleInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='join-diplomatic-action']")
		WebUI.click(createDiplomaticRoleInfo)
	}

	def static joinDiplomaticAsDiplomatInCharge(String roleName) {
		TestObject positionTypeButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='diplomatic-role-position-type']")
		WebUI.click(positionTypeButton)

		TestObject positionTypeValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Diplomat in Charge_item']")
		WebUI.click(positionTypeValue)

		TestObject positionNameInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='diplomatic-position-name']")
		WebUI.setText(positionNameInput, roleName)

		TestObject createDiplomaticRoleInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='join-diplomatic-action']")
		WebUI.click(createDiplomaticRoleInfo)
	}

	def static joinDiplomaticAsSupportStaff(String roleName, String officeName) {
		TestObject positionTypeButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='diplomatic-role-position-type']")
		WebUI.click(positionTypeButton)

		TestObject positionTypeValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Support Staff_item']")
		WebUI.click(positionTypeValue)

		TestObject officePositionTypeButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='office-position-type']")
		WebUI.click(officePositionTypeButton)

		TestObject addOfficeAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+officeName+"_office_item']")
		WebUI.click(addOfficeAction)


		TestObject positionNameInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='diplomatic-position-name']")
		WebUI.setText(positionNameInput, roleName)

		TestObject createDiplomaticRoleInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='join-diplomatic-action']")
		WebUI.click(createDiplomaticRoleInfo)
	}


	def static joinDiplomaticOffice(String roleName) {
		TestObject positionNameInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='diplomatic-office-role-position-name']")
		WebUI.setText(positionNameInput, roleName)

		TestObject createOrganizationRoleInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='join-diplomatic-office-action']")
		WebUI.click(createOrganizationRoleInfo)
	}

	// user role
	def static getFeaturedRole() {
		TestObject featuredRoleObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='profile-subTitle']")
		WebUI.scrollToElement(featuredRoleObject, 3)

		String realRoleValue = WebUI.getText(featuredRoleObject)
		String[] targetNameHasRole = realRoleValue.split(",")
		String oldEntityRole = targetNameHasRole[1].trim()
		return oldEntityRole
	}


	def static checkIfSuccessfullySetAsFeature(String targetName) {
		TestObject featuredRoleObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='profile-subTitle']")
		WebUI.scrollToElement(featuredRoleObject, 3)

		String realRoleValue = WebUI.getText(featuredRoleObject)
		String[] targetNameHasRole = realRoleValue.split(",")

		boolean isSuccessfullySetAsFeature = TestHelper.verifyElementValue(targetNameHasRole[1].trim(), targetName);

		if(isSuccessfullySetAsFeature) {
			println("Role Successfully Set As Featured")
		} else {
			println("An expected error has been occurred")
		}
	}

	def static checkIfSuccessfullySetAsUnFeature(String oldTargetName) {

		TestObject featuredRoleObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='profile-subTitle']")
		WebUI.scrollToElement(featuredRoleObject, 3)
		String realRoleValue = WebUI.getText(featuredRoleObject)
		String[] targetNameHasRole = realRoleValue.split(",")

		boolean isSuccessfullySetAsUnfeature = TestHelper.verifyElementValue(targetNameHasRole[1].trim(), oldTargetName);

		if(isSuccessfullySetAsUnfeature) {
			println("Role Successfully Set As Unfeatured")
		} else {
			println("An expected error has been occurred")
		}
	}


	def static checkUserGeneralInfoFieldsEmptyState() {
		boolean successEmptyFields = true;

		TestObject no_shortBio_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_user_shortBio_id_Add a Short Biography']")
		TestObject no_sector_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_user_sector_id_Add Your Sector']")
		TestObject no_profession_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_user_profession_id_Add a Profession']")
		TestObject no_contact_phone_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_user_contact_phone_id_Add Phone Number']")
		TestObject no_contact_email_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_user_contact_email_id_Add Email Address']")
		TestObject no_social_media_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_social_media_id']")

		// Check Object Exist
		if(!TestHelper.isElementPresent(no_shortBio_object)) {
			successEmptyFields = false;
		} else if(!TestHelper.isElementPresent(no_sector_object)) {
			successEmptyFields = false;
		} else if(!TestHelper.isElementPresent(no_profession_object)) {
			successEmptyFields = false;
		} else if(!TestHelper.isElementPresent(no_contact_phone_object)) {
			successEmptyFields = false;
		} else if(!TestHelper.isElementPresent(no_contact_email_object)) {
			successEmptyFields = false;
		} else if(!TestHelper.isElementPresent(no_social_media_object)) {
			successEmptyFields = false;
		}

		if(successEmptyFields) {
			println("Empty state for user general info tested successfully");
		} else {
			TestHelper.thrownException("Error during clear user info")
		}
	}

	def static checkCompanyGeneralInfoFieldsEmptyState() {
		boolean successEmptyFields = true;

		TestObject no_introduction_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_company_introduction_id_Add A Short Description']")
		TestObject no_address_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_company_introduction_id_Add A Short Description']")
		TestObject no_pobox_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_company_pobox_id_Add P.O. Box']")

		TestObject no_contact_phone_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_company_contact_phone_id_Add Phone Number']")
		TestObject no_contact_email_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_company_contact_email_id_Add Email Address']")
		TestObject no_contact_website_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_company_contact_website_id_Add A Website']")

		TestObject no_social_media_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_social_media_id']")

		// Check Object Exist
		if(!TestHelper.isElementPresent(no_introduction_object)) {
			successEmptyFields = false;
		} else if(!TestHelper.isElementPresent(no_address_object)) {
			successEmptyFields = false;
		} else if(!TestHelper.isElementPresent(no_pobox_object)) {
			successEmptyFields = false;
		} else if(!TestHelper.isElementPresent(no_contact_phone_object)) {
			successEmptyFields = false;
		} else if(!TestHelper.isElementPresent(no_contact_email_object)) {
			successEmptyFields = false;
		} else if(!TestHelper.isElementPresent(no_contact_website_object)) {
			successEmptyFields = false;
		} else if(!TestHelper.isElementPresent(no_social_media_object)) {
			successEmptyFields = false;
		}

		if(successEmptyFields) {
			println("Empty state for company general info tested successfully");
		} else {
			TestHelper.thrownException("Error during clear user info")
		}
	}


	def static checkOrganizationGeneralInfoFieldsEmptyState() {
		boolean successEmptyFields = true;

		TestObject no_introduction_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_organization_introduction_id_Add A Short Description']")
		TestObject no_address_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_organization_address_id_Add address']")
		TestObject no_pobox_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_organization_pobox_id_Add P.O. Box']")

		TestObject no_contact_phone_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_organization_contact_phone_id_Add Phone Number']")
		TestObject no_contact_email_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_organization_contact_email_id_Add Email Address']")
		TestObject no_contact_website_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_organization_contact_website_id_Add A Website']")

		TestObject no_social_media_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_social_media_id']")

		// Check Object Exist
		if(!TestHelper.isElementPresent(no_introduction_object)) {
			successEmptyFields = false;
		} else if(!TestHelper.isElementPresent(no_address_object)) {
			successEmptyFields = false;
		} else if(!TestHelper.isElementPresent(no_pobox_object)) {
			successEmptyFields = false;
		} else if(!TestHelper.isElementPresent(no_contact_phone_object)) {
			successEmptyFields = false;
		} else if(!TestHelper.isElementPresent(no_contact_email_object)) {
			successEmptyFields = false;
		} else if(!TestHelper.isElementPresent(no_contact_website_object)) {
			successEmptyFields = false;
		} else if(!TestHelper.isElementPresent(no_social_media_object)) {
			successEmptyFields = false;
		}

		if(successEmptyFields) {
			println("Empty state for organization general info tested successfully");
		} else {
			TestHelper.thrownException("Error during clear user info")
		}
	}

	def static checkDiplomaticGeneralInfoFieldsEmptyState() {
		boolean successEmptyFields = true;

		TestObject no_introduction_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_diplomatic_introduction_id_Add A Short Description']")
		TestObject no_address_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_diplomatic_address_id_Add An Address']")

		TestObject no_contact_phone_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_diplomatic_contact_phone_id_Add Phone Number']")
		TestObject no_contact_email_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_diplomatic_contact_email_id_Add Email Address']")
		TestObject no_contact_website_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_diplomatic_contact_website_id_Add A Website']")

		TestObject no_social_media_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_social_media_id']")

		// Check Object Exist
		if(!TestHelper.isElementPresent(no_introduction_object)) {
			successEmptyFields = false;
		} else if(!TestHelper.isElementPresent(no_address_object)) {
			successEmptyFields = false;
		} else if(!TestHelper.isElementPresent(no_contact_phone_object)) {
			successEmptyFields = false;
		} else if(!TestHelper.isElementPresent(no_contact_email_object)) {
			successEmptyFields = false;
		} else if(!TestHelper.isElementPresent(no_contact_website_object)) {
			successEmptyFields = false;
		} else if(!TestHelper.isElementPresent(no_social_media_object)) {
			successEmptyFields = false;
		}

		if(successEmptyFields) {
			println("Empty state for diplomatic general info tested successfully");
		} else {
			TestHelper.thrownException("Error during clear user info")
		}
	}


}
