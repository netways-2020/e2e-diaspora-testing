import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import org.junit.After

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

public class SearchFilterHelper {

	def static void checkMainFilterAction() {
		TestObject searchEntityFilterCountObject = TestHelper.getObjectById("//div[@id='search-entity-filter-count']");
		boolean searchEntityFilterCountObjectExist = TestHelper.isElementPresent(searchEntityFilterCountObject);

		if(!searchEntityFilterCountObjectExist) {
			TestHelper.thrownException("Filter count icon not exist")
		}

		int resultCount = TestHelper.countElementByClassName('search-result-entity');

		println('resultCount: ' + resultCount)
		if(resultCount == 0) {
			TestHelper.thrownException("No Result Found");
		}

		TestHelper.clickButton(searchEntityFilterCountObject);
	}

	def static void checkFilterEntitiesResult(TestObject moreEntityAction, String entityTitle) {
		TestHelper.clickButton(moreEntityAction);
		WebUI.delay(2);

		int resultCount = TestHelper.countElementByClassName('search-result-entity');

		if(resultCount == 0) {
			TestHelper.thrownException(entityTitle + ": No Result Found");
		}

		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='search-entity-page-back-button']"))
	}

	def static void closeFilterComponents() {
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='close-filters-type']"))
	}

	def static void closeFilterComponentsAndGoBackToSearchPage() {
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='close-filters-type']"))
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='search-entity-page-back-button']"))
	}

	// Entities Filters
	def static void checkMainPeopleFilter() {
		checkMainHomeTownFilter();
		checkMainCountryOfResidenceFilter();
		checkMainSectorFilter();
		checkMainProfessionFilter();
	}

	def static void checkAllPeopleFilter() {
		checkHomeTownFilter();
		checkCountryOfResidenceFilter();
		checkSectorFilter();
		checkProfessionFilter();
	}

	def static void checkMainCompaniesFilter() {
		checkMainCountryFilter();
		checkMainSectorFilter();
		checkMainKeywordsFilter();
		checkMainBrandsFilter();
	}

	def static void checkCompaniesFilter() {
		checkCountryFilter();
		checkSectorFilter();
		checkBrandsFilter();
		checkKeywordsFilter();
	}

	def static void checkMainNeedsFilter() {
		checkMainSectorFilter();
		checkMainMarketOfAvailabilityFilter();
		checkMainKeywordsFilter()
	}

	def static void checkNeedsFilter() {
		checkSectorFilter();
		checkMarketOfAvailabilityFilter();
		checkKeywordsFilter()
	}

	def static void checkMainOrganizationFilter() {
		checkMainCountryFilter();
		checkMainOrganizationTypeFilter();
		checkMainOrganizationCategoryFilter();
	}

	def static void checkOrganizationFilter() {
		checkCountryFilter();
		checkOrganizationTypeFilter();
		checkOrganizationCategoryFilter();
	}

	def static void checkMainHometownsFilter() {
		checkMainGovernateFilter();
	}

	def static void checkHometownsFilter() {
		checkGovernateFilter();
	}

	def static void checkMainDiplomaticFilter() {
		checkMainCountryFilter();
	}

	def static void checkDiplomaticFilter() {
		checkCountryFilter();
	}

	def static void checkMainNewsFilter() {
		checkMainCountryFilter();
	}

	def static void checkNewsFilter() {
		checkCountryFilter();
	}

	def static void checkMainEventsFilter() {
		checkMainDateFilter();
		checkMainCountryFilter();
	}

	def static void checkEventsFilter() {
		checkDateFilter();
		checkCountryFilter();
	}

	def static void checkMainTownOfficialFilter() {
		checkMainHomeTownFilter();
	}

	def static void checkTownOfficialFilter() {
		checkHomeTownFilter();
	}

	def static void checkMainDiplomatsFilter() {
		checkMainHomeTownFilter();
		checkMainCountryOfResidenceFilter();

	}

	def static void checkDiplomatsFilter() {
		checkHomeTownFilter();
		checkCountryOfResidenceFilter();
	}

	def static void checkMainCompanyLeadershipFilter() {
		checkMainCompanyTypeFilter();
		checkMainHomeTownFilter();
		checkMainCountryOfResidenceFilter();
		checkMainSectorFilter();
		checkMainProfessionFilter();
	}

	def static void checkCompanyLeadershipFilter() {
		checkCompanyTypeFilter();
		checkHomeTownFilter();
		checkCountryOfResidenceFilter();
		checkSectorFilter();
		checkProfessionFilter();
	}

	def static void checkMainOrganizationLeadershipFilter() {
		checkMainOrganizationLeadershipTypeFilter();
		checkMainOrganizationLeadershipCategoryFilter();
		checkMainCountryOfResidenceFilter();
		checkMainSectorFilter();
		checkMainProfessionFilter();
	}

	def static void checkOrganizationLeadershipFilter() {
		checkOrganizationLeadershipTypeFilter();
		checkOrganizationLeadershipCategoryFilter();
		checkHomeTownFilter();
		checkCountryOfResidenceFilter();
		checkSectorFilter();
		checkProfessionFilter();
	}


	// Main Filters
	def static void checkMainHomeTownFilter() {
		TestObject mainHometownFilterType = TestHelper.getObjectById("//button[@id='HomeTown-search-entity-filter-type']");
		boolean mainHometownFilterTypeExist = TestHelper.isElementPresent(mainHometownFilterType);

		if(!mainHometownFilterTypeExist) {
			TestHelper.thrownException("Main hometown filter not exist")
		}
	}

	def static void checkMainCountryOfResidenceFilter() {
		TestObject mainCountryOfResidenceType = TestHelper.getObjectById("//button[@id='Country of Residence-search-entity-filter-type']");
		boolean mainCountryOfResidenceTypeExist = TestHelper.isElementPresent(mainCountryOfResidenceType);

		if(!mainCountryOfResidenceTypeExist) {
			TestHelper.thrownException("Main country of residence not exist")
		}
	}

	def static void checkMainSectorFilter() {
		TestObject mainSectorFilterType = TestHelper.getObjectById("//button[@id='Sector-search-entity-filter-type']");
		boolean mainSectorFilterTypeExist = TestHelper.isElementPresent(mainSectorFilterType);

		if(!mainSectorFilterTypeExist) {
			TestHelper.thrownException("Main sector filter not exist")
		}
	}

	def static void checkMainProfessionFilter() {
		TestObject mainProfessionFilterType = TestHelper.getObjectById("//button[@id='Profession-search-entity-filter-type']");
		boolean mainProfessionFilterTypeExist = TestHelper.isElementPresent(mainProfessionFilterType);

		if(!mainProfessionFilterTypeExist) {
			TestHelper.thrownException("Main profession filter not exist")
		}
	}

	def static void checkMainCountryFilter() {
		TestObject mainCountryType = TestHelper.getObjectById("//button[@id='Country-search-entity-filter-type']");
		boolean mainCountryTypeExist = TestHelper.isElementPresent(mainCountryType);

		if(!mainCountryTypeExist) {
			TestHelper.thrownException("Main country not exist")
		}
	}

	def static void checkMainKeywordsFilter() {
		TestObject mainKeywordsType = TestHelper.getObjectById("//button[@id='Keywords-search-entity-filter-type']");
		boolean mainKeywordsTypeExist = TestHelper.isElementPresent(mainKeywordsType);

		if(!mainKeywordsTypeExist) {
			TestHelper.thrownException("Main keywords not exist")
		}
	}

	def static void checkMainBrandsFilter() {
		TestObject mainBrandsType = TestHelper.getObjectById("//button[@id='Brands-search-entity-filter-type']");
		boolean mainBrandsTypeExist = TestHelper.isElementPresent(mainBrandsType);

		if(!mainBrandsTypeExist) {
			TestHelper.thrownException("Main brands not exist")
		}
	}

	def static void checkMainMarketOfAvailabilityFilter() {
		TestObject mainMarketOfAvailabilityType = TestHelper.getObjectById("//button[@id='Market Availability-search-entity-filter-type']");
		boolean mainMarketOfAvailabilityTypeExist = TestHelper.isElementPresent(mainMarketOfAvailabilityType);

		if(!mainMarketOfAvailabilityTypeExist) {
			TestHelper.thrownException("Main market of availability not exist")
		}
	}

	def static void checkMainOrganizationTypeFilter() {
		TestObject mainOrganizationType = TestHelper.getObjectById("//button[@id='Type-search-entity-filter-type']");
		boolean mainOrganizationTypeExist = TestHelper.isElementPresent(mainOrganizationType);

		if(!mainOrganizationTypeExist) {
			TestHelper.thrownException("Main organization type not exist")
		}
	}

	def static void checkMainOrganizationCategoryFilter() {
		TestObject mainOrganizationCategory = TestHelper.getObjectById("//button[@id='Category-search-entity-filter-type']");
		boolean mainOrganizationCategoryExist = TestHelper.isElementPresent(mainOrganizationCategory);

		if(!mainOrganizationCategoryExist) {
			TestHelper.thrownException("Main organization category not exist")
		}
	}

	def static void checkMainGovernateFilter() {
		TestObject mainHometownGovernate = TestHelper.getObjectById("//button[@id='Governorate-search-entity-filter-type']");
		boolean mainHometownGovernateExist = TestHelper.isElementPresent(mainHometownGovernate);

		if(!mainHometownGovernateExist) {
			TestHelper.thrownException("Main hometown governate not exist")
		}
	}

	def static void checkMainDateFilter() {
		TestObject mainEventDateGovernate = TestHelper.getObjectById("//button[@id='Date-search-entity-filter-type']");
		boolean mainEventDateGovernateExist = TestHelper.isElementPresent(mainEventDateGovernate);

		if(!mainEventDateGovernateExist) {
			TestHelper.thrownException("Main event date not exist")
		}
	}

	def static void checkMainCompanyTypeFilter() {
		TestObject mainCompanyType = TestHelper.getObjectById("//button[@id='Company Type-search-entity-filter-type']");
		boolean mainCompanyTypeExist = TestHelper.isElementPresent(mainCompanyType);

		if(!mainCompanyTypeExist) {
			TestHelper.thrownException("Main company type not exist")
		}
	}

	def static void checkMainOrganizationLeadershipTypeFilter() {
		TestObject mainOrganizationType = TestHelper.getObjectById("//button[@id='Organization Type-search-entity-filter-type']");
		boolean mainOrganizationTypeExist = TestHelper.isElementPresent(mainOrganizationType);

		if(!mainOrganizationTypeExist) {
			TestHelper.thrownException("Main organization type not exist")
		}
	}

	def static void checkMainOrganizationLeadershipCategoryFilter() {
		TestObject mainOrganizationCategory = TestHelper.getObjectById("//button[@id='Organization Category-search-entity-filter-type']");
		boolean mainOrganizationCategoryExist = TestHelper.isElementPresent(mainOrganizationCategory);

		if(!mainOrganizationCategoryExist) {
			TestHelper.thrownException("Main organization category not exist")
		}
	}

	// Filters
	def static void checkHomeTownFilter() {
		TestObject hometownFilterType = TestHelper.getObjectById("//div[@id='Hometown_filter_label']");
		TestObject hometownFilterTypeAction = TestHelper.getObjectById("//button[@id='Hometown_filter_values_action']");

		boolean hometownFilterTypeExist = TestHelper.isElementPresent(hometownFilterType);
		boolean hometownFilterTypeActionExist = TestHelper.isElementPresent(hometownFilterTypeAction);

		if(!hometownFilterTypeExist || !hometownFilterTypeActionExist) {
			TestHelper.thrownException("Hometown filter not exist")
		}
	}

	def static void checkCountryOfResidenceFilter() {
		TestObject countryOfResidenceFilterType = TestHelper.getObjectById("//div[@id='Country of Residence_filter_label']")
		TestObject countryOfResidenceFilterTypeAction = TestHelper.getObjectById("//button[@id='Country of Residence_filter_values_action']")
		String country = 'Lebanon';

		boolean countryOfResidenceFilterTypeExist = TestHelper.isElementPresent(countryOfResidenceFilterType);
		boolean countryOfResidenceFilterTypeActionExist = TestHelper.isElementPresent(countryOfResidenceFilterTypeAction);

		if(!countryOfResidenceFilterTypeExist || !countryOfResidenceFilterTypeActionExist) {
			TestHelper.thrownException("Country of residence filter not exist")
		}

		TestHelper.clickButton(countryOfResidenceFilterTypeAction);
		TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='country-search-input']"), country);
		WebUI.delay(1);
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='" + country + "']"))
		WebUI.delay(1);
		checkCityOfResidenceFilter();
	}

	def static void checkCityOfResidenceFilter() {
		TestObject cityOfResidenceFilterType = TestHelper.getObjectById("//div[@id='City of Residence_filter_label']");
		TestObject cityOfResidenceFilterTypeAction  = TestHelper.getObjectById("//button[@id='City of Residence_filter_values_action']");

		boolean cityOfResidenceFilterTypeExist = TestHelper.isElementPresent(cityOfResidenceFilterType);
		boolean cityOfResidenceFilterTypeActionExist = TestHelper.isElementPresent(cityOfResidenceFilterTypeAction);

		if(!cityOfResidenceFilterTypeExist || !cityOfResidenceFilterTypeActionExist) {
			TestHelper.thrownException("City of residence filter not exist")
		}
	}

	def static void checkSectorFilter() {
		TestObject sectorFilterType = TestHelper.getObjectById("//div[@id='Sector_filter_label']")
		TestObject sectorFilterTypeAction = TestHelper.getObjectById("//button[@id='Sector_filter_values_action']")

		boolean sectorFilterTypeExist = TestHelper.isElementPresent(sectorFilterType);
		boolean sectorFilterTypeActionExist = TestHelper.isElementPresent(sectorFilterTypeAction);

		if(!sectorFilterTypeExist || !sectorFilterTypeActionExist) {
			TestHelper.thrownException("Sector filter not exist")
		}
	}

	def static void checkProfessionFilter() {
		TestObject professionFilterType = TestHelper.getObjectById("//div[@id='Profession_filter_label']");
		TestObject professionFilterTypeAction = TestHelper.getObjectById("//button[@id='Profession_filter_values_action']");

		boolean professionFilterTypeExist = TestHelper.isElementPresent(professionFilterType);
		boolean professionFilterTypeActionExist = TestHelper.isElementPresent(professionFilterTypeAction);

		if(!professionFilterTypeExist || !professionFilterTypeActionExist) {
			TestHelper.thrownException("Profession filter not exist")
		}
	}

	def static void checkCountryFilter() {
		String country = 'Lebanon';

		TestObject countryFilterType = TestHelper.getObjectById("//div[@id='Country_filter_label']");
		TestObject countryFilterTypeAction = TestHelper.getObjectById("//button[@id='Country_filter_values_action']");

		boolean countryFilterTypeExist = TestHelper.isElementPresent(countryFilterType);
		boolean countryFilterTypeActionExist = TestHelper.isElementPresent(countryFilterTypeAction);

		if(!countryFilterTypeExist || !countryFilterTypeActionExist) {
			TestHelper.thrownException("Profession filter not exist")
		}

		TestHelper.clickButton(countryFilterTypeAction);
		TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='country-search-input']"), country);
		WebUI.delay(1);
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='" + country + "']"))
		WebUI.delay(1);
		checkCityFilter();
	}

	def static void checkCityFilter() {
		TestObject cityFilterType = TestHelper.getObjectById("//div[@id='City_filter_label']");
		TestObject cityFilterTypeAction  = TestHelper.getObjectById("//button[@id='City_filter_values_action']");

		boolean cityFilterTypeExist = TestHelper.isElementPresent(cityFilterType);
		boolean cityFilterTypeActionExist = TestHelper.isElementPresent(cityFilterTypeAction);

		if(!cityFilterTypeExist || !cityFilterTypeActionExist) {
			TestHelper.thrownException("City filter not exist")
		}
	}

	def static void checkBrandsFilter() {
		TestObject brandsFilterType = TestHelper.getObjectById("//div[@id='Brands_filter_label']");
		TestObject brandsFilterTypeAction = TestHelper.getObjectById("//button[@id='Brands_filter_values_action']");

		boolean brandsFilterTypeExist = TestHelper.isElementPresent(brandsFilterType);
		boolean brandsFilterTypeActionExist = TestHelper.isElementPresent(brandsFilterTypeAction);

		if(!brandsFilterTypeExist || !brandsFilterTypeActionExist) {
			TestHelper.thrownException("Brands filter not exist")
		}
	}


	def static void checkKeywordsFilter() {
		TestObject keywordsFilterType = TestHelper.getObjectById("//div[@id='Keywords_filter_label']");
		TestObject keywordsFilterTypeAction = TestHelper.getObjectById("//button[@id='Keywords_filter_values_action']");

		boolean keywordsFilterTypeExist = TestHelper.isElementPresent(keywordsFilterType);
		boolean keywordsFilterTypeActionExist = TestHelper.isElementPresent(keywordsFilterTypeAction);

		if(!keywordsFilterTypeExist || !keywordsFilterTypeActionExist) {
			TestHelper.thrownException("Keywords filter not exist")
		}
	}

	def static void checkMarketOfAvailabilityFilter() {
		TestObject marketOfAvailabilityFilterType = TestHelper.getObjectById("//div[@id='Market Availability_filter_label']");
		TestObject marketOfAvailabilityFilterTypeAction = TestHelper.getObjectById("//button[@id='Market Availability_filter_values_action']");

		boolean marketOfAvailabilityFilterTypeExist = TestHelper.isElementPresent(marketOfAvailabilityFilterType);
		boolean marketOfAvailabilityFilterTypeActionExist = TestHelper.isElementPresent(marketOfAvailabilityFilterTypeAction);

		if(!marketOfAvailabilityFilterTypeExist || !marketOfAvailabilityFilterTypeActionExist) {
			TestHelper.thrownException("Market of availability not exist")
		}
	}

	def static void checkOrganizationTypeFilter() {
		TestObject organizationTypeFilterType = TestHelper.getObjectById("//div[@id='Type_filter_label']");
		TestObject organizationTypeFilterTypeAction = TestHelper.getObjectById("//button[@id='Type_filter_values_action']");

		boolean organizationTypeFilterTypeExist = TestHelper.isElementPresent(organizationTypeFilterType);
		boolean organizationTypeFilterTypeActionExist = TestHelper.isElementPresent(organizationTypeFilterTypeAction);

		if(!organizationTypeFilterTypeExist || !organizationTypeFilterTypeAction) {
			TestHelper.thrownException("Organization type not exist")
		}
	}

	def static void checkOrganizationCategoryFilter() {
		TestObject organizationCategoryFilterType = TestHelper.getObjectById("//div[@id='Category_filter_label']");
		TestObject organizationCategoryFilterTypeAction = TestHelper.getObjectById("//button[@id='Category_filter_values_action']");

		boolean organizationCategoryFilterTypeExist = TestHelper.isElementPresent(organizationCategoryFilterType);
		boolean organizationCategoryFilterTypeActionExist = TestHelper.isElementPresent(organizationCategoryFilterTypeAction);

		if(!organizationCategoryFilterTypeExist || !organizationCategoryFilterTypeActionExist) {
			TestHelper.thrownException("Organization Category not exist")
		}
	}

	def static void checkGovernateFilter() {
		TestObject governateFilterType = TestHelper.getObjectById("//div[@id='Governate_filter_label']");
		TestObject governateFilterTagsAction = TestHelper.getObjectById("//div[@id='Governate_filter_tags_action']");

		boolean governateFilterTypeExist = TestHelper.isElementPresent(governateFilterType);
		boolean governateFilterTagsActionExist = TestHelper.isElementPresent(governateFilterTagsAction);

		if(!governateFilterTypeExist || !governateFilterTagsActionExist) {
			TestHelper.thrownException("Hometown governate not exist")
		}

		checkDistrictFilter();
	}

	def static void checkDistrictFilter() {
		TestObject governateFilterAction = TestHelper.getObjectById("//button[@id='Baalbek-Hermel_tags_value_action']");
		boolean governateFilterActionExist = TestHelper.isElementPresent(governateFilterAction);

		if(!governateFilterActionExist) {
			TestHelper.thrownException("Baalbeck hermel governate not exist")
		}

		TestHelper.clickButton(governateFilterAction);

		TestObject districtFilterAction = TestHelper.getObjectById("//div[@id='District_filter_label']");
		TestObject hermelDistrictAction = TestHelper.getObjectById("//button[@id='Hermel_tags_value_action']");


		boolean districtFilterActionExist = TestHelper.isElementPresent(districtFilterAction);
		boolean hermelDistrictActionExist = TestHelper.isElementPresent(hermelDistrictAction);

		if(!districtFilterActionExist || !hermelDistrictActionExist) {
			TestHelper.thrownException("hermel district not exist")
		}
	}

	def static void checkDateFilter() {
		TestObject dateFilterType = TestHelper.getObjectById("//div[@id='Date_filter_label']");
		TestObject dateFilterTagsAction = TestHelper.getObjectById("//button[@id='Upcoming Events_tags_value_action']");

		boolean dateFilterTypeExist = TestHelper.isElementPresent(dateFilterType);
		boolean dateFilterTagsActionExist = TestHelper.isElementPresent(dateFilterTagsAction);

		if(!dateFilterTypeExist || !dateFilterTagsActionExist) {
			TestHelper.thrownException("Event Date not exist")
		}

	}

	def static void checkCompanyTypeFilter() {
		TestObject companyTypeFilter = TestHelper.getObjectById("//div[@id='Company Type_filter_label']");
		TestObject companyTypeFilterAction = TestHelper.getObjectById("//button[@id='Business_tags_value_action']");

		boolean companyTypeFilterExist = TestHelper.isElementPresent(companyTypeFilter);
		boolean companyTypeFilterActionExist = TestHelper.isElementPresent(companyTypeFilterAction);

		if(!companyTypeFilterExist || !companyTypeFilterActionExist) {
			TestHelper.thrownException("Company type not exist")
		}
	}

	def static void checkOrganizationLeadershipTypeFilter() {
		TestObject organizationTypeFilterType = TestHelper.getObjectById("//div[@id='Organization Type_filter_label']");
		TestObject organizationTypeFilterTypeAction = TestHelper.getObjectById("//button[@id='Organization Type_filter_values_action']");

		boolean organizationTypeFilterTypeExist = TestHelper.isElementPresent(organizationTypeFilterType);
		boolean organizationTypeFilterTypeActionExist = TestHelper.isElementPresent(organizationTypeFilterTypeAction);

		if(!organizationTypeFilterTypeExist || !organizationTypeFilterTypeAction) {
			TestHelper.thrownException("Organization type not exist")
		}
	}

	def static void checkOrganizationLeadershipCategoryFilter() {
		TestObject organizationCategoryFilterType = TestHelper.getObjectById("//div[@id='Organization Category_filter_label']");
		TestObject organizationCategoryFilterTypeAction = TestHelper.getObjectById("//button[@id='Organization Category_filter_values_action']");

		boolean organizationCategoryFilterTypeExist = TestHelper.isElementPresent(organizationCategoryFilterType);
		boolean organizationCategoryFilterTypeActionExist = TestHelper.isElementPresent(organizationCategoryFilterTypeAction);

		if(!organizationCategoryFilterTypeExist || !organizationCategoryFilterTypeActionExist) {
			TestHelper.thrownException("Organization Category not exist")
		}
	}
}



