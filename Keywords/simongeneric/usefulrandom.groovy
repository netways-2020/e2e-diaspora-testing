package simongeneric

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

// Returns a random character sequence of length 4 out of the provided string of possible characters
@Keyword
public static String GetRandomCharSequence(int length, String possibleChars) {
	length = 4;
	possibleChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
	StringBuilder builder = new StringBuilder();
	Random rnd = new Random();
	while (builder.length() < length) {
		int index = (int)(rnd.nextFloat() * possibleChars.length());
		builder.append(possibleChars.charAt(index));
	}
	return builder.toString();
}


// Returns a random integer from a range of integers (lower and upper bound-inclusive):
@Keyword
public static int getRandomInteger(int lowerBound, int upperBound) {
	return new Random().nextInt(upperBound - lowerBound + 1) + lowerBound;
}


// Returns the current timestamp in nanoseconds (useful for appending a “random” number to other values):
@Keyword
public static String getCurrentTimestamp() {
	return System.nanoTime().toString();
}