<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>html_ion-select-popoverion-selection-select_65e15d</name>
   <tag></tag>
   <elementGuidId>3de45c11-3681-465e-a6f6-4491b2c3be26</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//html[@id='htmlTag']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>html</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>htmlTag</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>lang</name>
      <type>Main</type>
      <value>en</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>direction</name>
      <type>Main</type>
      <value>ltr</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>plt-desktop md hydrated</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mode</name>
      <type>Main</type>
      <value>md</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
  ion-select-popover,ion-select,ion-select-option,ion-menu-button,ion-menu,ion-menu-controller,ion-menu-toggle,ion-action-sheet,ion-fab-button,ion-fab,ion-fab-list,ion-refresher-content,ion-refresher,ion-backdrop,ion-ripple-effect,ion-alert,ion-back-button,ion-loading,ion-toast,ion-card,ion-card-content,ion-card-header,ion-card-subtitle,ion-card-title,ion-item-option,ion-item-options,ion-item-sliding,ion-infinite-scroll-content,ion-infinite-scroll,ion-reorder,ion-reorder-group,ion-segment-button,ion-segment,ion-tab-button,ion-tab-bar,ion-chip,ion-modal,ion-popover,ion-searchbar,ion-action-sheet-controller,ion-alert-controller,ion-anchor,ion-loading-controller,ion-modal-controller,ion-picker-controller,ion-popover-controller,ion-toast-controller,ion-app,ion-buttons,ion-content,ion-footer,ion-header,ion-router-outlet,ion-title,ion-toolbar,ion-nav,ion-nav-link,ion-nav-pop,ion-nav-push,ion-nav-set-root,ion-route,ion-route-redirect,ion-router,ion-router-link,ion-avatar,ion-badge,ion-thumbnail,ion-col,ion-grid,ion-row,ion-slide,ion-slides,ion-checkbox,ion-img,ion-input,ion-progress-bar,ion-range,ion-split-pane,ion-text,ion-textarea,ion-toggle,ion-virtual-scroll,ion-picker,ion-datetime,ion-picker-column,ion-radio,ion-radio-group,ion-spinner,ion-button,ion-icon,ion-item-divider,ion-item-group,ion-note,ion-skeleton-text,ion-item,ion-label,ion-list,ion-list-header{visibility:hidden}.hydrated{visibility:inherit}
  Ionic App

  

    
    
    
  
  
  
  
   html.plt-mobile ion-app{-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}ion-app.force-statusbar-padding{--ion-safe-area-top:20px}.item.sc-ion-label-md-h, .item .sc-ion-label-md-h{--color:initial;display:block;color:var(--color);font-family:var(--ion-font-family,inherit);font-size:inherit;text-overflow:ellipsis;white-space:nowrap;overflow:hidden;-webkit-box-sizing:border-box;box-sizing:border-box}.ion-color.sc-ion-label-md-h{color:var(--ion-color-base)}.ion-text-wrap.sc-ion-label-md-h, [text-wrap].sc-ion-label-md-h{white-space:normal}.item-interactive-disabled.sc-ion-label-md-h:not(.item-multiple-inputs), .item-interactive-disabled:not(.item-multiple-inputs) .sc-ion-label-md-h{cursor:default;opacity:.3;pointer-events:none}.item-input.sc-ion-label-md-h, .item-input .sc-ion-label-md-h{-ms-flex:initial;flex:initial;max-width:200px;pointer-events:none}.item-textarea.sc-ion-label-md-h, .item-textarea .sc-ion-label-md-h{-ms-flex-item-align:baseline;align-self:baseline}.label-fixed.sc-ion-label-md-h{-ms-flex:0 0 100px;flex:0 0 100px;width:100px;min-width:100px;max-width:200px}.label-floating.sc-ion-label-md-h, .label-stacked.sc-ion-label-md-h{-ms-flex-item-align:stretch;align-self:stretch;width:auto;max-width:100%}.label-no-animate.label-floating.sc-ion-label-md-h{-webkit-transition:none;transition:none}.ion-text-wrap.sc-ion-label-md-h, [text-wrap].sc-ion-label-md-h{line-height:1.5}.label-stacked.sc-ion-label-md-h{-webkit-transform-origin:left top;transform-origin:left top;-webkit-transform:translate3d(0,50%,0) scale(.75);transform:translate3d(0,50%,0) scale(.75);-webkit-transition:color .15s cubic-bezier(.4,0,.2,1);transition:color .15s cubic-bezier(.4,0,.2,1)}[dir=rtl].label-stacked.sc-ion-label-md-h, [dir=rtl] .label-stacked.sc-ion-label-md-h, [dir=rtl].sc-ion-label-md-h -no-combinator.label-stacked.sc-ion-label-md-h, [dir=rtl] .sc-ion-label-md-h -no-combinator.label-stacked.sc-ion-label-md-h{-webkit-transform-origin:right top;transform-origin:right top}.label-floating.sc-ion-label-md-h{-webkit-transform:translate3d(0,96%,0);transform:translate3d(0,96%,0);-webkit-transform-origin:left top;transform-origin:left top;-webkit-transition:color .15s cubic-bezier(.4,0,.2,1),-webkit-transform .15s cubic-bezier(.4,0,.2,1);transition:color .15s cubic-bezier(.4,0,.2,1),-webkit-transform .15s cubic-bezier(.4,0,.2,1);transition:color .15s cubic-bezier(.4,0,.2,1),transform .15s cubic-bezier(.4,0,.2,1);transition:color .15s cubic-bezier(.4,0,.2,1),transform .15s cubic-bezier(.4,0,.2,1),-webkit-transform .15s cubic-bezier(.4,0,.2,1)}[dir=rtl].label-floating.sc-ion-label-md-h, [dir=rtl] .label-floating.sc-ion-label-md-h, [dir=rtl].sc-ion-label-md-h -no-combinator.label-floating.sc-ion-label-md-h, [dir=rtl] .sc-ion-label-md-h -no-combinator.label-floating.sc-ion-label-md-h{-webkit-transform-origin:right top;transform-origin:right top}.label-floating.sc-ion-label-md-h, .label-stacked.sc-ion-label-md-h{margin-left:0;margin-right:0;margin-top:0;margin-bottom:0}.item-select.label-floating.sc-ion-label-md-h, .item-select .label-floating.sc-ion-label-md-h{-webkit-transform:translate3d(0,130%,0);transform:translate3d(0,130%,0)}.item-has-focus.label-floating.sc-ion-label-md-h, .item-has-focus .label-floating.sc-ion-label-md-h, .item-has-placeholder.label-floating.sc-ion-label-md-h, .item-has-placeholder .label-floating.sc-ion-label-md-h, .item-has-value.label-floating.sc-ion-label-md-h, .item-has-value .label-floating.sc-ion-label-md-h{-webkit-transform:translate3d(0,50%,0) scale(.75);transform:translate3d(0,50%,0) scale(.75)}.item-has-focus.label-floating.sc-ion-label-md-h, .item-has-focus .label-floating.sc-ion-label-md-h, .item-has-focus.label-stacked.sc-ion-label-md-h, .item-has-focus .label-stacked.sc-ion-label-md-h{color:var(--ion-color-primary,#3880ff)}.sc-ion-label-md-s  h1 {margin-left:0;margin-right:0;margin-top:0;margin-bottom:2px;font-size:24px;font-weight:400}.sc-ion-label-md-s  h2 {margin-left:0;margin-right:0;margin-top:2px;margin-bottom:2px;font-size:16px;font-weight:400}.sc-ion-label-md-s  h3 , .sc-ion-label-md-s  h4 , .sc-ion-label-md-s  h5 , .sc-ion-label-md-s  h6 {margin-left:0;margin-right:0;margin-top:2px;margin-bottom:2px;font-size:14px;font-weight:400;line-height:normal}.sc-ion-label-md-s  p {margin-left:0;margin-right:0;margin-top:0;margin-bottom:2px;font-size:14px;line-height:20px;text-overflow:inherit;overflow:inherit}.sc-ion-label-md-s > p{color:var(--ion-color-step-600,#666)}.sc-ion-label-md-h.ion-color.sc-ion-label-md-s > p, .ion-color .sc-ion-label-md-h.sc-ion-label-md-s > p{color:inherit}ion-infinite-scroll{display:none;width:100%}.infinite-scroll-enabled{display:block}ion-infinite-scroll-content{display:-ms-flexbox;display:flex;-ms-flex-direction:column;flex-direction:column;-ms-flex-pack:center;justify-content:center;min-height:84px;text-align:center;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.infinite-loading{margin-left:0;margin-right:0;margin-top:0;margin-bottom:32px;display:none;width:100%}.infinite-loading-text{margin-left:32px;margin-right:32px;margin-top:4px;margin-bottom:0}@supports ((-webkit-margin-start:0) or (margin-inline-start:0)) or (-webkit-margin-start:0){.infinite-loading-text{margin-left:unset;margin-right:unset;-webkit-margin-start:32px;margin-inline-start:32px;-webkit-margin-end:32px;margin-inline-end:32px}}.infinite-scroll-loading ion-infinite-scroll-content>.infinite-loading{display:block}.infinite-scroll-content-md .infinite-loading-text{color:var(--ion-color-step-600,#666)}.infinite-scroll-content-md .infinite-loading-spinner .spinner-crescent circle,.infinite-scroll-content-md .infinite-loading-spinner .spinner-lines-md line,.infinite-scroll-content-md .infinite-loading-spinner .spinner-lines-small-md line{stroke:var(--ion-color-step-600,#666)}.infinite-scroll-content-md .infinite-loading-spinner .spinner-bubbles circle,.infinite-scroll-content-md .infinite-loading-spinner .spinner-circles circle,.infinite-scroll-content-md .infinite-loading-spinner .spinner-dots circle{fill:var(--ion-color-step-600,#666)}.swiper-container{margin:0 auto;position:relative;overflow:hidden;list-style:none;padding:0;z-index:1}.swiper-container-no-flexbox .swiper-slide{float:left}.swiper-container-vertical>.swiper-wrapper{-webkit-box-orient:vertical;-webkit-box-direction:normal;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column}.swiper-wrapper{position:relative;width:100%;height:100%;z-index:1;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-transition-property:-webkit-transform;transition-property:-webkit-transform;-o-transition-property:transform;transition-property:transform;transition-property:transform,-webkit-transform;-webkit-box-sizing:content-box;box-sizing:content-box}.swiper-container-android .swiper-slide,.swiper-wrapper{-webkit-transform:translateZ(0);transform:translateZ(0)}.swiper-container-multirow>.swiper-wrapper{-webkit-flex-wrap:wrap;-ms-flex-wrap:wrap;flex-wrap:wrap}.swiper-container-free-mode>.swiper-wrapper{-webkit-transition-timing-function:ease-out;-o-transition-timing-function:ease-out;transition-timing-function:ease-out;margin:0 auto}.swiper-slide{-webkit-flex-shrink:0;-ms-flex-negative:0;flex-shrink:0;width:100%;height:100%;position:relative;-webkit-transition-property:-webkit-transform;transition-property:-webkit-transform;-o-transition-property:transform;transition-property:transform;transition-property:transform,-webkit-transform}.swiper-invisible-blank-slide{visibility:hidden}.swiper-container-autoheight,.swiper-container-autoheight .swiper-slide{height:auto}.swiper-container-autoheight .swiper-wrapper{-webkit-box-align:start;-webkit-align-items:flex-start;-ms-flex-align:start;align-items:flex-start;-webkit-transition-property:height,-webkit-transform;transition-property:height,-webkit-transform;-o-transition-property:transform,height;transition-property:transform,height;transition-property:transform,height,-webkit-transform}.swiper-container-3d{-webkit-perspective:1200px;perspective:1200px}.swiper-container-3d .swiper-cube-shadow,.swiper-container-3d .swiper-slide,.swiper-container-3d .swiper-slide-shadow-bottom,.swiper-container-3d .swiper-slide-shadow-left,.swiper-container-3d .swiper-slide-shadow-right,.swiper-container-3d .swiper-slide-shadow-top,.swiper-container-3d .swiper-wrapper{-webkit-transform-style:preserve-3d;transform-style:preserve-3d}.swiper-container-3d .swiper-slide-shadow-bottom,.swiper-container-3d .swiper-slide-shadow-left,.swiper-container-3d .swiper-slide-shadow-right,.swiper-container-3d .swiper-slide-shadow-top{position:absolute;left:0;top:0;width:100%;height:100%;pointer-events:none;z-index:10}.swiper-container-3d .swiper-slide-shadow-left{background-image:-webkit-gradient(linear,right top,left top,from(rgba(0,0,0,.5)),to(transparent));background-image:-webkit-linear-gradient(right,rgba(0,0,0,.5),transparent);background-image:-o-linear-gradient(right,rgba(0,0,0,.5),transparent);background-image:linear-gradient(270deg,rgba(0,0,0,.5),transparent)}.swiper-container-3d .swiper-slide-shadow-right{background-image:-webkit-gradient(linear,left top,right top,from(rgba(0,0,0,.5)),to(transparent));background-image:-webkit-linear-gradient(left,rgba(0,0,0,.5),transparent);background-image:-o-linear-gradient(left,rgba(0,0,0,.5),transparent);background-image:linear-gradient(90deg,rgba(0,0,0,.5),transparent)}.swiper-container-3d .swiper-slide-shadow-top{background-image:-webkit-gradient(linear,left bottom,left top,from(rgba(0,0,0,.5)),to(transparent));background-image:-webkit-linear-gradient(bottom,rgba(0,0,0,.5),transparent);background-image:-o-linear-gradient(bottom,rgba(0,0,0,.5),transparent);background-image:linear-gradient(0deg,rgba(0,0,0,.5),transparent)}.swiper-container-3d .swiper-slide-shadow-bottom{background-image:-webkit-gradient(linear,left top,left bottom,from(rgba(0,0,0,.5)),to(transparent));background-image:-webkit-linear-gradient(top,rgba(0,0,0,.5),transparent);background-image:-o-linear-gradient(top,rgba(0,0,0,.5),transparent);background-image:linear-gradient(180deg,rgba(0,0,0,.5),transparent)}.swiper-container-wp8-horizontal,.swiper-container-wp8-horizontal>.swiper-wrapper{-ms-touch-action:pan-y;touch-action:pan-y}.swiper-container-wp8-vertical,.swiper-container-wp8-vertical>.swiper-wrapper{-ms-touch-action:pan-x;touch-action:pan-x}.swiper-button-next,.swiper-button-prev{position:absolute;top:50%;width:27px;height:44px;margin-top:-22px;z-index:10;cursor:pointer;background-size:27px 44px;background-position:50%;background-repeat:no-repeat}.swiper-button-next.swiper-button-disabled,.swiper-button-prev.swiper-button-disabled{opacity:.35;cursor:auto;pointer-events:none}.swiper-button-prev,.swiper-container-rtl .swiper-button-next{background-image:url(&quot;data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M0%2C22L22%2C0l2.1%2C2.1L4.2%2C22l19.9%2C19.9L22%2C44L0%2C22L0%2C22L0%2C22z'%20fill%3D'%23007aff'%2F%3E%3C%2Fsvg%3E&quot;);left:10px;right:auto}.swiper-button-next,.swiper-container-rtl .swiper-button-prev{background-image:url(&quot;data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M27%2C22L27%2C22L5%2C44l-2.1-2.1L22.8%2C22L2.9%2C2.1L5%2C0L27%2C22L27%2C22z'%20fill%3D'%23007aff'%2F%3E%3C%2Fsvg%3E&quot;);right:10px;left:auto}.swiper-button-prev.swiper-button-white,.swiper-container-rtl .swiper-button-next.swiper-button-white{background-image:url(&quot;data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M0%2C22L22%2C0l2.1%2C2.1L4.2%2C22l19.9%2C19.9L22%2C44L0%2C22L0%2C22L0%2C22z'%20fill%3D'%23ffffff'%2F%3E%3C%2Fsvg%3E&quot;)}.swiper-button-next.swiper-button-white,.swiper-container-rtl .swiper-button-prev.swiper-button-white{background-image:url(&quot;data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M27%2C22L27%2C22L5%2C44l-2.1-2.1L22.8%2C22L2.9%2C2.1L5%2C0L27%2C22L27%2C22z'%20fill%3D'%23ffffff'%2F%3E%3C%2Fsvg%3E&quot;)}.swiper-button-prev.swiper-button-black,.swiper-container-rtl .swiper-button-next.swiper-button-black{background-image:url(&quot;data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M0%2C22L22%2C0l2.1%2C2.1L4.2%2C22l19.9%2C19.9L22%2C44L0%2C22L0%2C22L0%2C22z'%20fill%3D'%23000000'%2F%3E%3C%2Fsvg%3E&quot;)}.swiper-button-next.swiper-button-black,.swiper-container-rtl .swiper-button-prev.swiper-button-black{background-image:url(&quot;data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M27%2C22L27%2C22L5%2C44l-2.1-2.1L22.8%2C22L2.9%2C2.1L5%2C0L27%2C22L27%2C22z'%20fill%3D'%23000000'%2F%3E%3C%2Fsvg%3E&quot;)}.swiper-button-lock{display:none}.swiper-pagination{position:absolute;text-align:center;-webkit-transition:opacity .3s;-o-transition:.3s opacity;transition:opacity .3s;-webkit-transform:translateZ(0);transform:translateZ(0);z-index:10}.swiper-pagination.swiper-pagination-hidden{opacity:0}.swiper-container-horizontal>.swiper-pagination-bullets,.swiper-pagination-custom,.swiper-pagination-fraction{bottom:10px;left:0;width:100%}.swiper-pagination-bullets-dynamic{overflow:hidden;font-size:0}.swiper-pagination-bullets-dynamic .swiper-pagination-bullet{-webkit-transform:scale(.33);-ms-transform:scale(.33);transform:scale(.33);position:relative}.swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active,.swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-main{-webkit-transform:scale(1);-ms-transform:scale(1);transform:scale(1)}.swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-prev{-webkit-transform:scale(.66);-ms-transform:scale(.66);transform:scale(.66)}.swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-prev-prev{-webkit-transform:scale(.33);-ms-transform:scale(.33);transform:scale(.33)}.swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-next{-webkit-transform:scale(.66);-ms-transform:scale(.66);transform:scale(.66)}.swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-next-next{-webkit-transform:scale(.33);-ms-transform:scale(.33);transform:scale(.33)}.swiper-pagination-bullet{width:8px;height:8px;display:inline-block;border-radius:100%;background:#000;opacity:.2}button.swiper-pagination-bullet{border:none;margin:0;padding:0;-webkit-box-shadow:none;box-shadow:none;-webkit-appearance:none;-moz-appearance:none;appearance:none}.swiper-pagination-clickable .swiper-pagination-bullet{cursor:pointer}.swiper-pagination-bullet-active{opacity:1;background:#007aff}.swiper-container-vertical>.swiper-pagination-bullets{right:10px;top:50%;-webkit-transform:translate3d(0,-50%,0);transform:translate3d(0,-50%,0)}.swiper-container-vertical>.swiper-pagination-bullets .swiper-pagination-bullet{margin:6px 0;display:block}.swiper-container-vertical>.swiper-pagination-bullets.swiper-pagination-bullets-dynamic{top:50%;-webkit-transform:translateY(-50%);-ms-transform:translateY(-50%);transform:translateY(-50%);width:8px}.swiper-container-vertical>.swiper-pagination-bullets.swiper-pagination-bullets-dynamic .swiper-pagination-bullet{display:inline-block;-webkit-transition:top .2s,-webkit-transform .2s;transition:top .2s,-webkit-transform .2s;-o-transition:.2s transform,.2s top;transition:transform .2s,top .2s;transition:transform .2s,top .2s,-webkit-transform .2s}.swiper-container-horizontal>.swiper-pagination-bullets .swiper-pagination-bullet{margin:0 4px}.swiper-container-horizontal>.swiper-pagination-bullets.swiper-pagination-bullets-dynamic{left:50%;-webkit-transform:translateX(-50%);-ms-transform:translateX(-50%);transform:translateX(-50%);white-space:nowrap}.swiper-container-horizontal>.swiper-pagination-bullets.swiper-pagination-bullets-dynamic .swiper-pagination-bullet{-webkit-transition:left .2s,-webkit-transform .2s;transition:left .2s,-webkit-transform .2s;-o-transition:.2s transform,.2s left;transition:transform .2s,left .2s;transition:transform .2s,left .2s,-webkit-transform .2s}.swiper-container-horizontal.swiper-container-rtl>.swiper-pagination-bullets-dynamic .swiper-pagination-bullet{-webkit-transition:right .2s,-webkit-transform .2s;transition:right .2s,-webkit-transform .2s;-o-transition:.2s transform,.2s right;transition:transform .2s,right .2s;transition:transform .2s,right .2s,-webkit-transform .2s}.swiper-pagination-progressbar{background:rgba(0,0,0,.25);position:absolute}.swiper-pagination-progressbar .swiper-pagination-progressbar-fill{background:#007aff;position:absolute;left:0;top:0;width:100%;height:100%;-webkit-transform:scale(0);-ms-transform:scale(0);transform:scale(0);-webkit-transform-origin:left top;-ms-transform-origin:left top;transform-origin:left top}.swiper-container-rtl .swiper-pagination-progressbar .swiper-pagination-progressbar-fill{-webkit-transform-origin:right top;-ms-transform-origin:right top;transform-origin:right top}.swiper-container-horizontal>.swiper-pagination-progressbar,.swiper-container-vertical>.swiper-pagination-progressbar.swiper-pagination-progressbar-opposite{width:100%;height:4px;left:0;top:0}.swiper-container-horizontal>.swiper-pagination-progressbar.swiper-pagination-progressbar-opposite,.swiper-container-vertical>.swiper-pagination-progressbar{width:4px;height:100%;left:0;top:0}.swiper-pagination-white .swiper-pagination-bullet-active{background:#fff}.swiper-pagination-progressbar.swiper-pagination-white{background:hsla(0,0%,100%,.25)}.swiper-pagination-progressbar.swiper-pagination-white .swiper-pagination-progressbar-fill{background:#fff}.swiper-pagination-black .swiper-pagination-bullet-active{background:#000}.swiper-pagination-progressbar.swiper-pagination-black{background:rgba(0,0,0,.25)}.swiper-pagination-progressbar.swiper-pagination-black .swiper-pagination-progressbar-fill{background:#000}.swiper-pagination-lock{display:none}.swiper-scrollbar{border-radius:10px;position:relative;-ms-touch-action:none;background:rgba(0,0,0,.1)}.swiper-container-horizontal>.swiper-scrollbar{position:absolute;left:1%;bottom:3px;z-index:50;height:5px;width:98%}.swiper-container-vertical>.swiper-scrollbar{position:absolute;right:3px;top:1%;z-index:50;width:5px;height:98%}.swiper-scrollbar-drag{height:100%;width:100%;position:relative;background:rgba(0,0,0,.5);border-radius:10px;left:0;top:0}.swiper-scrollbar-cursor-drag{cursor:move}.swiper-scrollbar-lock{display:none}.swiper-zoom-container{width:100%;height:100%;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;text-align:center}.swiper-zoom-container>canvas,.swiper-zoom-container>img,.swiper-zoom-container>svg{max-width:100%;max-height:100%;-o-object-fit:contain;object-fit:contain}.swiper-slide-zoomed{cursor:move}.swiper-lazy-preloader{width:42px;height:42px;position:absolute;left:50%;top:50%;margin-left:-21px;margin-top:-21px;z-index:10;-webkit-transform-origin:50%;-ms-transform-origin:50%;transform-origin:50%;-webkit-animation:swiper-preloader-spin 1s steps(12,end) infinite;animation:swiper-preloader-spin 1s steps(12,end) infinite}.swiper-lazy-preloader:after{display:block;content:&quot;&quot;;width:100%;height:100%;background-image:url(&quot;data:image/svg+xml;charset=utf-8,%3Csvg%20viewBox%3D'0%200%20120%20120'%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20xmlns%3Axlink%3D'http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink'%3E%3Cdefs%3E%3Cline%20id%3D'l'%20x1%3D'60'%20x2%3D'60'%20y1%3D'7'%20y2%3D'27'%20stroke%3D'%236c6c6c'%20stroke-width%3D'11'%20stroke-linecap%3D'round'%2F%3E%3C%2Fdefs%3E%3Cg%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(30%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(60%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(90%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(120%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(150%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.37'%20transform%3D'rotate(180%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.46'%20transform%3D'rotate(210%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.56'%20transform%3D'rotate(240%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.66'%20transform%3D'rotate(270%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.75'%20transform%3D'rotate(300%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.85'%20transform%3D'rotate(330%2060%2C60)'%2F%3E%3C%2Fg%3E%3C%2Fsvg%3E&quot;);background-position:50%;background-size:100%;background-repeat:no-repeat}.swiper-lazy-preloader-white:after{background-image:url(&quot;data:image/svg+xml;charset=utf-8,%3Csvg%20viewBox%3D'0%200%20120%20120'%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20xmlns%3Axlink%3D'http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink'%3E%3Cdefs%3E%3Cline%20id%3D'l'%20x1%3D'60'%20x2%3D'60'%20y1%3D'7'%20y2%3D'27'%20stroke%3D'%23fff'%20stroke-width%3D'11'%20stroke-linecap%3D'round'%2F%3E%3C%2Fdefs%3E%3Cg%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(30%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(60%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(90%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(120%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(150%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.37'%20transform%3D'rotate(180%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.46'%20transform%3D'rotate(210%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.56'%20transform%3D'rotate(240%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.66'%20transform%3D'rotate(270%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.75'%20transform%3D'rotate(300%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.85'%20transform%3D'rotate(330%2060%2C60)'%2F%3E%3C%2Fg%3E%3C%2Fsvg%3E&quot;)}@-webkit-keyframes swiper-preloader-spin{to{-webkit-transform:rotate(1turn);transform:rotate(1turn)}}@keyframes swiper-preloader-spin{to{-webkit-transform:rotate(1turn);transform:rotate(1turn)}}.swiper-container .swiper-notification{position:absolute;left:0;top:0;pointer-events:none;opacity:0;z-index:-1000}.swiper-container-fade.swiper-container-free-mode .swiper-slide{-webkit-transition-timing-function:ease-out;-o-transition-timing-function:ease-out;transition-timing-function:ease-out}.swiper-container-fade .swiper-slide{pointer-events:none;-webkit-transition-property:opacity;-o-transition-property:opacity;transition-property:opacity}.swiper-container-fade .swiper-slide .swiper-slide{pointer-events:none}.swiper-container-fade .swiper-slide-active,.swiper-container-fade .swiper-slide-active .swiper-slide-active{pointer-events:auto}.swiper-container-cube{overflow:visible}.swiper-container-cube .swiper-slide{pointer-events:none;-webkit-backface-visibility:hidden;backface-visibility:hidden;z-index:1;visibility:hidden;-webkit-transform-origin:0 0;-ms-transform-origin:0 0;transform-origin:0 0;width:100%;height:100%}.swiper-container-cube .swiper-slide .swiper-slide{pointer-events:none}.swiper-container-cube.swiper-container-rtl .swiper-slide{-webkit-transform-origin:100% 0;-ms-transform-origin:100% 0;transform-origin:100% 0}.swiper-container-cube .swiper-slide-active,.swiper-container-cube .swiper-slide-active .swiper-slide-active{pointer-events:auto}.swiper-container-cube .swiper-slide-active,.swiper-container-cube .swiper-slide-next,.swiper-container-cube .swiper-slide-next+.swiper-slide,.swiper-container-cube .swiper-slide-prev{pointer-events:auto;visibility:visible}.swiper-container-cube .swiper-slide-shadow-bottom,.swiper-container-cube .swiper-slide-shadow-left,.swiper-container-cube .swiper-slide-shadow-right,.swiper-container-cube .swiper-slide-shadow-top{z-index:0;-webkit-backface-visibility:hidden;backface-visibility:hidden}.swiper-container-cube .swiper-cube-shadow{position:absolute;left:0;bottom:0;width:100%;height:100%;background:#000;opacity:.6;-webkit-filter:blur(50px);filter:blur(50px);z-index:0}.swiper-container-flip{overflow:visible}.swiper-container-flip .swiper-slide{pointer-events:none;-webkit-backface-visibility:hidden;backface-visibility:hidden;z-index:1}.swiper-container-flip .swiper-slide .swiper-slide{pointer-events:none}.swiper-container-flip .swiper-slide-active,.swiper-container-flip .swiper-slide-active .swiper-slide-active{pointer-events:auto}.swiper-container-flip .swiper-slide-shadow-bottom,.swiper-container-flip .swiper-slide-shadow-left,.swiper-container-flip .swiper-slide-shadow-right,.swiper-container-flip .swiper-slide-shadow-top{z-index:0;-webkit-backface-visibility:hidden;backface-visibility:hidden}.swiper-container-coverflow .swiper-wrapper{-ms-perspective:1200px}ion-slides{display:block;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.swiper-pagination-bullet{background:var(--bullet-background)}.swiper-pagination-bullet-active{background:var(--bullet-background-active)}.swiper-pagination-progressbar{background:var(--progress-bar-background)}.swiper-pagination-progressbar .swiper-pagination-progressbar-fill{background:var(--progress-bar-background-active)}.swiper-scrollbar{background:var(--scroll-bar-background)}.swiper-scrollbar-drag{background:var(--scroll-bar-background-active)}.slides-md{--bullet-background:var(--ion-color-step-200,#ccc);--bullet-background-active:var(--ion-color-primary,#3880ff);--progress-bar-background:rgba(var(--ion-text-color-rgb,0,0,0),0.25);--progress-bar-background-active:var(--ion-color-primary-shade,#3171e0);--scroll-bar-background:rgba(var(--ion-text-color-rgb,0,0,0),0.1);--scroll-bar-background-active:rgba(var(--ion-text-color-rgb,0,0,0),0.5)}ion-slide{height:100%}.slide-zoom,ion-slide{display:block;width:100%}.slide-zoom,.swiper-slide{text-align:center}.swiper-slide{display:-ms-flexbox;display:flex;position:relative;-ms-flex-negative:0;flex-shrink:0;-ms-flex-align:center;align-items:center;-ms-flex-pack:center;justify-content:center;width:100%;height:100%;font-size:18px;-webkit-box-sizing:border-box;box-sizing:border-box}.swiper-slide img{width:auto;max-width:100%;height:auto;max-height:100%}ion-refresher{left:0;top:0;display:none;position:absolute;width:100%;height:60px;z-index:-1}:host-context([dir=rtl]) ion-refresher,[dir=rtl] ion-refresher{left:unset;right:unset;right:0}ion-refresher.refresher-active{display:block}ion-refresher-content{display:-ms-flexbox;display:flex;-ms-flex-direction:column;flex-direction:column;-ms-flex-pack:center;justify-content:center;height:100%}.refresher-pulling,.refresher-refreshing{display:none;width:100%}.refresher-pulling-icon,.refresher-refreshing-icon{-webkit-transform-origin:center;transform-origin:center;-webkit-transition:.2s;transition:.2s;font-size:30px;text-align:center}:host-context([dir=rtl]) .refresher-pulling-icon,:host-context([dir=rtl]) .refresher-refreshing-icon,[dir=rtl] .refresher-pulling-icon,[dir=rtl] .refresher-refreshing-icon{-webkit-transform-origin:calc(100% - center);transform-origin:calc(100% - center)}.refresher-pulling-text,.refresher-refreshing-text{font-size:16px;text-align:center}.refresher-pulling ion-refresher-content .refresher-pulling,.refresher-ready ion-refresher-content .refresher-pulling{display:block}.refresher-ready ion-refresher-content .refresher-pulling-icon{-webkit-transform:rotate(180deg);transform:rotate(180deg)}.refresher-cancelling ion-refresher-content .refresher-pulling,.refresher-refreshing ion-refresher-content .refresher-refreshing{display:block}.refresher-cancelling ion-refresher-content .refresher-pulling-icon{-webkit-transform:scale(0);transform:scale(0)}.refresher-completing ion-refresher-content .refresher-refreshing{display:block}.refresher-completing ion-refresher-content .refresher-refreshing-icon{-webkit-transform:scale(0);transform:scale(0)}.refresher-md .refresher-pulling-icon,.refresher-md .refresher-pulling-text,.refresher-md .refresher-refreshing-icon,.refresher-md .refresher-refreshing-text{color:var(--ion-text-color,#000)}.refresher-md .refresher-refreshing .spinner-crescent circle,.refresher-md .refresher-refreshing .spinner-lines-md line,.refresher-md .refresher-refreshing .spinner-lines-small-md line{stroke:var(--ion-text-color,#000)}.refresher-md .refresher-refreshing .spinner-bubbles circle,.refresher-md .refresher-refreshing .spinner-circles circle,.refresher-md .refresher-refreshing .spinner-dots circle{fill:var(--ion-text-color,#000)}ion-header{display:block;position:relative;-ms-flex-order:-1;order:-1;width:100%;z-index:10}ion-header ion-toolbar:first-child{padding-top:var(--ion-safe-area-top,0)}.header-md:after{left:0;bottom:-5px;background-position:left 0 top -2px;position:absolute;width:100%;height:5px;background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAHBAMAAADzDtBxAAAAD1BMVEUAAAAAAAAAAAAAAAAAAABPDueNAAAABXRSTlMUCS0gBIh/TXEAAAAaSURBVAjXYxCEAgY4UIICBmMogMsgFLtAAQCNSwXZKOdPxgAAAABJRU5ErkJggg==);background-repeat:repeat-x;content:&quot;&quot;}:host-context([dir=rtl]) .header-md:after,[dir=rtl] .header-md:after{left:unset;right:unset;right:0;background-position:right 0 top -2px}.header-collapse-condense,.header-md[no-border]:after{display:none}.sc-ion-modal-md-h{--width:100%;--min-width:auto;--max-width:auto;--height:100%;--min-height:auto;--max-height:auto;--overflow:hidden;--border-radius:0;--border-width:0;--border-style:none;--border-color:transparent;--background:var(--ion-background-color,#fff);--box-shadow:none;left:0;right:0;top:0;bottom:0;display:-ms-flexbox;display:flex;position:absolute;-ms-flex-align:center;align-items:center;-ms-flex-pack:center;justify-content:center;contain:strict}.overlay-hidden.sc-ion-modal-md-h{display:none}.modal-wrapper.sc-ion-modal-md{border-radius:var(--border-radius);width:var(--width);min-width:var(--min-width);max-width:var(--max-width);height:var(--height);min-height:var(--min-height);max-height:var(--max-height);border-width:var(--border-width);border-style:var(--border-style);border-color:var(--border-color);background:var(--background);-webkit-box-shadow:var(--box-shadow);box-shadow:var(--box-shadow);overflow:var(--overflow);z-index:10}@media only screen and (min-width:768px) and (min-height:600px){.sc-ion-modal-md-h{--width:600px;--height:500px;--ion-safe-area-top:0px;--ion-safe-area-bottom:0px;--ion-safe-area-right:0px;--ion-safe-area-left:0px}}@media only screen and (min-width:768px) and (min-height:768px){.sc-ion-modal-md-h{--width:600px;--height:600px}}@media only screen and (min-width:768px) and (min-height:600px){.sc-ion-modal-md-h{--border-radius:2px;--box-shadow:0 28px 48px rgba(0,0,0,0.4)}}.modal-wrapper.sc-ion-modal-md{-webkit-transform:translate3d(0,40px,0);transform:translate3d(0,40px,0);opacity:.01}

  
  
  
/** Ionic CSS Variables **/
:root {
  /** primary **/
  --ion-color-primary: #3880ff;
  --ion-color-primary-rgb: 56, 128, 255;
  --ion-color-primary-contrast: #ffffff;
  --ion-color-primary-contrast-rgb: 255, 255, 255;
  --ion-color-primary-shade: #3171e0;
  --ion-color-primary-tint: #4c8dff;
  /** secondary **/
  --ion-color-secondary: #0cd1e8;
  --ion-color-secondary-rgb: 12, 209, 232;
  --ion-color-secondary-contrast: #ffffff;
  --ion-color-secondary-contrast-rgb: 255, 255, 255;
  --ion-color-secondary-shade: #0bb8cc;
  --ion-color-secondary-tint: #24d6ea;
  /** tertiary **/
  --ion-color-tertiary: #7044ff;
  --ion-color-tertiary-rgb: 112, 68, 255;
  --ion-color-tertiary-contrast: #ffffff;
  --ion-color-tertiary-contrast-rgb: 255, 255, 255;
  --ion-color-tertiary-shade: #633ce0;
  --ion-color-tertiary-tint: #7e57ff;
  /** success **/
  --ion-color-success: #10dc60;
  --ion-color-success-rgb: 16, 220, 96;
  --ion-color-success-contrast: #ffffff;
  --ion-color-success-contrast-rgb: 255, 255, 255;
  --ion-color-success-shade: #0ec254;
  --ion-color-success-tint: #28e070;
  /** warning **/
  --ion-color-warning: #ffce00;
  --ion-color-warning-rgb: 255, 206, 0;
  --ion-color-warning-contrast: #ffffff;
  --ion-color-warning-contrast-rgb: 255, 255, 255;
  --ion-color-warning-shade: #e0b500;
  --ion-color-warning-tint: #ffd31a;
  /** danger **/
  --ion-color-danger: #f04141;
  --ion-color-danger-rgb: 245, 61, 61;
  --ion-color-danger-contrast: #ffffff;
  --ion-color-danger-contrast-rgb: 255, 255, 255;
  --ion-color-danger-shade: #d33939;
  --ion-color-danger-tint: #f25454;
  /** dark **/
  --ion-color-dark: #222428;
  --ion-color-dark-rgb: 34, 34, 34;
  --ion-color-dark-contrast: #ffffff;
  --ion-color-dark-contrast-rgb: 255, 255, 255;
  --ion-color-dark-shade: #1e2023;
  --ion-color-dark-tint: #383a3e;
  /** medium **/
  --ion-color-medium: #989aa2;
  --ion-color-medium-rgb: 152, 154, 162;
  --ion-color-medium-contrast: #ffffff;
  --ion-color-medium-contrast-rgb: 255, 255, 255;
  --ion-color-medium-shade: #86888f;
  --ion-color-medium-tint: #a2a4ab;
  /** light **/
  --ion-color-light: #f4f5f8;
  --ion-color-light-rgb: 244, 244, 244;
  --ion-color-light-contrast: #000000;
  --ion-color-light-contrast-rgb: 0, 0, 0;
  --ion-color-light-shade: #d7d8da;
  --ion-color-light-tint: #f5f6f9; }

/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy90aGVtZS9DOlxcVXNlcnNcXGFibGF5YmVsXFxEZXNrdG9wXFxEaWFzcG9yYSB2NFxcRGlhc3BvcmEgdjggMjAyMCAtIENvZGUgcHVzaC9zcmNcXHRoZW1lXFx2YXJpYWJsZXMuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFHQSwwQkFBQTtBQUNBO0VBQ0UsY0FBQTtFQUNBLDRCQUFvQjtFQUNwQixxQ0FBd0I7RUFDeEIscUNBQTZCO0VBQzdCLCtDQUFpQztFQUNqQyxrQ0FBMEI7RUFDMUIsaUNBQXlCO0VBRXpCLGdCQUFBO0VBQ0EsOEJBQXNCO0VBQ3RCLHVDQUEwQjtFQUMxQix1Q0FBK0I7RUFDL0IsaURBQW1DO0VBQ25DLG9DQUE0QjtFQUM1QixtQ0FBMkI7RUFFM0IsZUFBQTtFQUNBLDZCQUFxQjtFQUNyQixzQ0FBeUI7RUFDekIsc0NBQThCO0VBQzlCLGdEQUFrQztFQUNsQyxtQ0FBMkI7RUFDM0Isa0NBQTBCO0VBRTFCLGNBQUE7RUFDQSw0QkFBb0I7RUFDcEIsb0NBQXdCO0VBQ3hCLHFDQUE2QjtFQUM3QiwrQ0FBaUM7RUFDakMsa0NBQTBCO0VBQzFCLGlDQUF5QjtFQUV6QixjQUFBO0VBQ0EsNEJBQW9CO0VBQ3BCLG9DQUF3QjtFQUN4QixxQ0FBNkI7RUFDN0IsK0NBQWlDO0VBQ2pDLGtDQUEwQjtFQUMxQixpQ0FBeUI7RUFFekIsYUFBQTtFQUNBLDJCQUFtQjtFQUNuQixtQ0FBdUI7RUFDdkIsb0NBQTRCO0VBQzVCLDhDQUFnQztFQUNoQyxpQ0FBeUI7RUFDekIsZ0NBQXdCO0VBRXhCLFdBQUE7RUFDQSx5QkFBaUI7RUFDakIsZ0NBQXFCO0VBQ3JCLGtDQUEwQjtFQUMxQiw0Q0FBOEI7RUFDOUIsK0JBQXVCO0VBQ3ZCLDhCQUFzQjtFQUV0QixhQUFBO0VBQ0EsMkJBQW1CO0VBQ25CLHFDQUF1QjtFQUN2QixvQ0FBNEI7RUFDNUIsOENBQWdDO0VBQ2hDLGlDQUF5QjtFQUN6QixnQ0FBd0I7RUFFeEIsWUFBQTtFQUNBLDBCQUFrQjtFQUNsQixvQ0FBc0I7RUFDdEIsbUNBQTJCO0VBQzNCLHVDQUErQjtFQUMvQixnQ0FBd0I7RUFDeEIsK0JBQXVCLEVBQUEiLCJmaWxlIjoic3JjL3RoZW1lL3ZhcmlhYmxlcy5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gSW9uaWMgVmFyaWFibGVzIGFuZCBUaGVtaW5nLiBGb3IgbW9yZSBpbmZvLCBwbGVhc2Ugc2VlOlxyXG4vLyBodHRwOi8vaW9uaWNmcmFtZXdvcmsuY29tL2RvY3MvdGhlbWluZy9cclxuXHJcbi8qKiBJb25pYyBDU1MgVmFyaWFibGVzICoqL1xyXG46cm9vdCB7XHJcbiAgLyoqIHByaW1hcnkgKiovXHJcbiAgLS1pb24tY29sb3ItcHJpbWFyeTogIzM4ODBmZjtcclxuICAtLWlvbi1jb2xvci1wcmltYXJ5LXJnYjogNTYsIDEyOCwgMjU1O1xyXG4gIC0taW9uLWNvbG9yLXByaW1hcnktY29udHJhc3Q6ICNmZmZmZmY7XHJcbiAgLS1pb24tY29sb3ItcHJpbWFyeS1jb250cmFzdC1yZ2I6IDI1NSwgMjU1LCAyNTU7XHJcbiAgLS1pb24tY29sb3ItcHJpbWFyeS1zaGFkZTogIzMxNzFlMDtcclxuICAtLWlvbi1jb2xvci1wcmltYXJ5LXRpbnQ6ICM0YzhkZmY7XHJcblxyXG4gIC8qKiBzZWNvbmRhcnkgKiovXHJcbiAgLS1pb24tY29sb3Itc2Vjb25kYXJ5OiAjMGNkMWU4O1xyXG4gIC0taW9uLWNvbG9yLXNlY29uZGFyeS1yZ2I6IDEyLCAyMDksIDIzMjtcclxuICAtLWlvbi1jb2xvci1zZWNvbmRhcnktY29udHJhc3Q6ICNmZmZmZmY7XHJcbiAgLS1pb24tY29sb3Itc2Vjb25kYXJ5LWNvbnRyYXN0LXJnYjogMjU1LCAyNTUsIDI1NTtcclxuICAtLWlvbi1jb2xvci1zZWNvbmRhcnktc2hhZGU6ICMwYmI4Y2M7XHJcbiAgLS1pb24tY29sb3Itc2Vjb25kYXJ5LXRpbnQ6ICMyNGQ2ZWE7XHJcblxyXG4gIC8qKiB0ZXJ0aWFyeSAqKi9cclxuICAtLWlvbi1jb2xvci10ZXJ0aWFyeTogIzcwNDRmZjtcclxuICAtLWlvbi1jb2xvci10ZXJ0aWFyeS1yZ2I6IDExMiwgNjgsIDI1NTtcclxuICAtLWlvbi1jb2xvci10ZXJ0aWFyeS1jb250cmFzdDogI2ZmZmZmZjtcclxuICAtLWlvbi1jb2xvci10ZXJ0aWFyeS1jb250cmFzdC1yZ2I6IDI1NSwgMjU1LCAyNTU7XHJcbiAgLS1pb24tY29sb3ItdGVydGlhcnktc2hhZGU6ICM2MzNjZTA7XHJcbiAgLS1pb24tY29sb3ItdGVydGlhcnktdGludDogIzdlNTdmZjtcclxuXHJcbiAgLyoqIHN1Y2Nlc3MgKiovXHJcbiAgLS1pb24tY29sb3Itc3VjY2VzczogIzEwZGM2MDtcclxuICAtLWlvbi1jb2xvci1zdWNjZXNzLXJnYjogMTYsIDIyMCwgOTY7XHJcbiAgLS1pb24tY29sb3Itc3VjY2Vzcy1jb250cmFzdDogI2ZmZmZmZjtcclxuICAtLWlvbi1jb2xvci1zdWNjZXNzLWNvbnRyYXN0LXJnYjogMjU1LCAyNTUsIDI1NTtcclxuICAtLWlvbi1jb2xvci1zdWNjZXNzLXNoYWRlOiAjMGVjMjU0O1xyXG4gIC0taW9uLWNvbG9yLXN1Y2Nlc3MtdGludDogIzI4ZTA3MDtcclxuXHJcbiAgLyoqIHdhcm5pbmcgKiovXHJcbiAgLS1pb24tY29sb3Itd2FybmluZzogI2ZmY2UwMDtcclxuICAtLWlvbi1jb2xvci13YXJuaW5nLXJnYjogMjU1LCAyMDYsIDA7XHJcbiAgLS1pb24tY29sb3Itd2FybmluZy1jb250cmFzdDogI2ZmZmZmZjtcclxuICAtLWlvbi1jb2xvci13YXJuaW5nLWNvbnRyYXN0LXJnYjogMjU1LCAyNTUsIDI1NTtcclxuICAtLWlvbi1jb2xvci13YXJuaW5nLXNoYWRlOiAjZTBiNTAwO1xyXG4gIC0taW9uLWNvbG9yLXdhcm5pbmctdGludDogI2ZmZDMxYTtcclxuXHJcbiAgLyoqIGRhbmdlciAqKi9cclxuICAtLWlvbi1jb2xvci1kYW5nZXI6ICNmMDQxNDE7XHJcbiAgLS1pb24tY29sb3ItZGFuZ2VyLXJnYjogMjQ1LCA2MSwgNjE7XHJcbiAgLS1pb24tY29sb3ItZGFuZ2VyLWNvbnRyYXN0OiAjZmZmZmZmO1xyXG4gIC0taW9uLWNvbG9yLWRhbmdlci1jb250cmFzdC1yZ2I6IDI1NSwgMjU1LCAyNTU7XHJcbiAgLS1pb24tY29sb3ItZGFuZ2VyLXNoYWRlOiAjZDMzOTM5O1xyXG4gIC0taW9uLWNvbG9yLWRhbmdlci10aW50OiAjZjI1NDU0O1xyXG5cclxuICAvKiogZGFyayAqKi9cclxuICAtLWlvbi1jb2xvci1kYXJrOiAjMjIyNDI4O1xyXG4gIC0taW9uLWNvbG9yLWRhcmstcmdiOiAzNCwgMzQsIDM0O1xyXG4gIC0taW9uLWNvbG9yLWRhcmstY29udHJhc3Q6ICNmZmZmZmY7XHJcbiAgLS1pb24tY29sb3ItZGFyay1jb250cmFzdC1yZ2I6IDI1NSwgMjU1LCAyNTU7XHJcbiAgLS1pb24tY29sb3ItZGFyay1zaGFkZTogIzFlMjAyMztcclxuICAtLWlvbi1jb2xvci1kYXJrLXRpbnQ6ICMzODNhM2U7XHJcblxyXG4gIC8qKiBtZWRpdW0gKiovXHJcbiAgLS1pb24tY29sb3ItbWVkaXVtOiAjOTg5YWEyO1xyXG4gIC0taW9uLWNvbG9yLW1lZGl1bS1yZ2I6IDE1MiwgMTU0LCAxNjI7XHJcbiAgLS1pb24tY29sb3ItbWVkaXVtLWNvbnRyYXN0OiAjZmZmZmZmO1xyXG4gIC0taW9uLWNvbG9yLW1lZGl1bS1jb250cmFzdC1yZ2I6IDI1NSwgMjU1LCAyNTU7XHJcbiAgLS1pb24tY29sb3ItbWVkaXVtLXNoYWRlOiAjODY4ODhmO1xyXG4gIC0taW9uLWNvbG9yLW1lZGl1bS10aW50OiAjYTJhNGFiO1xyXG5cclxuICAvKiogbGlnaHQgKiovXHJcbiAgLS1pb24tY29sb3ItbGlnaHQ6ICNmNGY1Zjg7XHJcbiAgLS1pb24tY29sb3ItbGlnaHQtcmdiOiAyNDQsIDI0NCwgMjQ0O1xyXG4gIC0taW9uLWNvbG9yLWxpZ2h0LWNvbnRyYXN0OiAjMDAwMDAwO1xyXG4gIC0taW9uLWNvbG9yLWxpZ2h0LWNvbnRyYXN0LXJnYjogMCwgMCwgMDtcclxuICAtLWlvbi1jb2xvci1saWdodC1zaGFkZTogI2Q3ZDhkYTtcclxuICAtLWlvbi1jb2xvci1saWdodC10aW50OiAjZjVmNmY5O1xyXG59XHJcbiJdfQ== */html.ios {
  --ion-default-font: -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;Roboto&quot;, sans-serif;
}

html.md {
  --ion-default-font: &quot;Roboto&quot;, &quot;Helvetica Neue&quot;, sans-serif;
}

html {
  --ion-font-family: var(--ion-default-font);
}

body {
  background: var(--ion-background-color);
}

body.backdrop-no-scroll {
  overflow: hidden;
}

.ion-color-primary {
  --ion-color-base: var(--ion-color-primary, #3880ff) !important;
  --ion-color-base-rgb: var(--ion-color-primary-rgb, 56, 128, 255) !important;
  --ion-color-contrast: var(--ion-color-primary-contrast, #fff) !important;
  --ion-color-contrast-rgb: var(--ion-color-primary-contrast-rgb, 255, 255, 255) !important;
  --ion-color-shade: var(--ion-color-primary-shade, #3171e0) !important;
  --ion-color-tint: var(--ion-color-primary-tint, #4c8dff) !important;
}

.ion-color-secondary {
  --ion-color-base: var(--ion-color-secondary, #0cd1e8) !important;
  --ion-color-base-rgb: var(--ion-color-secondary-rgb, 12, 209, 232) !important;
  --ion-color-contrast: var(--ion-color-secondary-contrast, #fff) !important;
  --ion-color-contrast-rgb: var(--ion-color-secondary-contrast-rgb, 255, 255, 255) !important;
  --ion-color-shade: var(--ion-color-secondary-shade, #0bb8cc) !important;
  --ion-color-tint: var(--ion-color-secondary-tint, #24d6ea) !important;
}

.ion-color-tertiary {
  --ion-color-base: var(--ion-color-tertiary, #7044ff) !important;
  --ion-color-base-rgb: var(--ion-color-tertiary-rgb, 112, 68, 255) !important;
  --ion-color-contrast: var(--ion-color-tertiary-contrast, #fff) !important;
  --ion-color-contrast-rgb: var(--ion-color-tertiary-contrast-rgb, 255, 255, 255) !important;
  --ion-color-shade: var(--ion-color-tertiary-shade, #633ce0) !important;
  --ion-color-tint: var(--ion-color-tertiary-tint, #7e57ff) !important;
}

.ion-color-success {
  --ion-color-base: var(--ion-color-success, #10dc60) !important;
  --ion-color-base-rgb: var(--ion-color-success-rgb, 16, 220, 96) !important;
  --ion-color-contrast: var(--ion-color-success-contrast, #fff) !important;
  --ion-color-contrast-rgb: var(--ion-color-success-contrast-rgb, 255, 255, 255) !important;
  --ion-color-shade: var(--ion-color-success-shade, #0ec254) !important;
  --ion-color-tint: var(--ion-color-success-tint, #28e070) !important;
}

.ion-color-warning {
  --ion-color-base: var(--ion-color-warning, #ffce00) !important;
  --ion-color-base-rgb: var(--ion-color-warning-rgb, 255, 206, 0) !important;
  --ion-color-contrast: var(--ion-color-warning-contrast, #fff) !important;
  --ion-color-contrast-rgb: var(--ion-color-warning-contrast-rgb, 255, 255, 255) !important;
  --ion-color-shade: var(--ion-color-warning-shade, #e0b500) !important;
  --ion-color-tint: var(--ion-color-warning-tint, #ffd31a) !important;
}

.ion-color-danger {
  --ion-color-base: var(--ion-color-danger, #f04141) !important;
  --ion-color-base-rgb: var(--ion-color-danger-rgb, 240, 65, 65) !important;
  --ion-color-contrast: var(--ion-color-danger-contrast, #fff) !important;
  --ion-color-contrast-rgb: var(--ion-color-danger-contrast-rgb, 255, 255, 255) !important;
  --ion-color-shade: var(--ion-color-danger-shade, #d33939) !important;
  --ion-color-tint: var(--ion-color-danger-tint, #f25454) !important;
}

.ion-color-light {
  --ion-color-base: var(--ion-color-light, #f4f5f8) !important;
  --ion-color-base-rgb: var(--ion-color-light-rgb, 244, 245, 248) !important;
  --ion-color-contrast: var(--ion-color-light-contrast, #000) !important;
  --ion-color-contrast-rgb: var(--ion-color-light-contrast-rgb, 0, 0, 0) !important;
  --ion-color-shade: var(--ion-color-light-shade, #d7d8da) !important;
  --ion-color-tint: var(--ion-color-light-tint, #f5f6f9) !important;
}

.ion-color-medium {
  --ion-color-base: var(--ion-color-medium, #989aa2) !important;
  --ion-color-base-rgb: var(--ion-color-medium-rgb, 152, 154, 162) !important;
  --ion-color-contrast: var(--ion-color-medium-contrast, #fff) !important;
  --ion-color-contrast-rgb: var(--ion-color-medium-contrast-rgb, 255, 255, 255) !important;
  --ion-color-shade: var(--ion-color-medium-shade, #86888f) !important;
  --ion-color-tint: var(--ion-color-medium-tint, #a2a4ab) !important;
}

.ion-color-dark {
  --ion-color-base: var(--ion-color-dark, #222428) !important;
  --ion-color-base-rgb: var(--ion-color-dark-rgb, 34, 36, 40) !important;
  --ion-color-contrast: var(--ion-color-dark-contrast, #fff) !important;
  --ion-color-contrast-rgb: var(--ion-color-dark-contrast-rgb, 255, 255, 255) !important;
  --ion-color-shade: var(--ion-color-dark-shade, #1e2023) !important;
  --ion-color-tint: var(--ion-color-dark-tint, #383a3e) !important;
}

.ion-page {
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  display: -webkit-box;
  display: flex;
  position: absolute;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
          flex-direction: column;
  -webkit-box-pack: justify;
          justify-content: space-between;
  contain: layout size style;
  overflow: hidden;
  z-index: 0;
}

ion-route,
ion-route-redirect,
ion-router,
ion-select-option,
ion-nav-controller,
ion-menu-controller,
ion-action-sheet-controller,
ion-alert-controller,
ion-loading-controller,
ion-modal-controller,
ion-picker-controller,
ion-popover-controller,
ion-toast-controller,
.ion-page-hidden,
[hidden] {
  /* stylelint-disable-next-line declaration-no-important */
  display: none !important;
}

.ion-page-invisible {
  opacity: 0;
}

html.plt-ios.plt-hybrid, html.plt-ios.plt-pwa {
  --ion-statusbar-padding: 20px;
}

@supports (padding-top: 20px) {
  html {
    --ion-safe-area-top: var(--ion-statusbar-padding);
  }
}

@supports (padding-top: constant(safe-area-inset-top)) {
  html {
    --ion-safe-area-top: constant(safe-area-inset-top);
    --ion-safe-area-bottom: constant(safe-area-inset-bottom);
    --ion-safe-area-left: constant(safe-area-inset-left);
    --ion-safe-area-right: constant(safe-area-inset-right);
  }
}

@supports (padding-top: env(safe-area-inset-top)) {
  html {
    --ion-safe-area-top: env(safe-area-inset-top);
    --ion-safe-area-bottom: env(safe-area-inset-bottom);
    --ion-safe-area-left: env(safe-area-inset-left);
    --ion-safe-area-right: env(safe-area-inset-right);
  }
}

.menu-content {
  -webkit-transform: translate3d(0,  0,  0);
          transform: translate3d(0,  0,  0);
}

.menu-content-open {
  cursor: pointer;
  touch-action: manipulation;
  pointer-events: none;
}

.ios .menu-content-reveal {
  box-shadow: -8px 0 42px rgba(0, 0, 0, 0.08);
}

[dir=rtl].ios .menu-content-reveal {
  box-shadow: 8px 0 42px rgba(0, 0, 0, 0.08);
}

.md .menu-content-reveal {
  box-shadow: 0 2px 22px 0 rgba(0, 0, 0, 0.09), 4px 0 16px 0 rgba(0, 0, 0, 0.18);
}

.md .menu-content-push {
  box-shadow: 0 2px 22px 0 rgba(0, 0, 0, 0.09), 4px 0 16px 0 rgba(0, 0, 0, 0.18);
}

audio,
canvas,
progress,
video {
  vertical-align: baseline;
}

audio:not([controls]) {
  display: none;
  height: 0;
}

b,
strong {
  font-weight: bold;
}

img {
  max-width: 100%;
  border: 0;
}

svg:not(:root) {
  overflow: hidden;
}

figure {
  margin: 1em 40px;
}

hr {
  height: 1px;
  border-width: 0;
  box-sizing: content-box;
}

pre {
  overflow: auto;
}

code,
kbd,
pre,
samp {
  font-family: monospace, monospace;
  font-size: 1em;
}

label,
input,
select,
textarea {
  font-family: inherit;
  line-height: normal;
}

textarea {
  overflow: auto;
  height: auto;
  font: inherit;
  color: inherit;
}

textarea::-webkit-input-placeholder {
  padding-left: 2px;
}

textarea::-moz-placeholder {
  padding-left: 2px;
}

textarea::-ms-input-placeholder {
  padding-left: 2px;
}

textarea::placeholder {
  padding-left: 2px;
}

form,
input,
optgroup,
select {
  margin: 0;
  font: inherit;
  color: inherit;
}

html input[type=button],
input[type=reset],
input[type=submit] {
  cursor: pointer;
  -webkit-appearance: button;
}

a,
a div,
a span,
a ion-icon,
a ion-label,
button,
button div,
button span,
button ion-icon,
button ion-label,
.ion-tappable,
[tappable],
[tappable] div,
[tappable] span,
[tappable] ion-icon,
[tappable] ion-label,
input,
textarea {
  touch-action: manipulation;
}

a ion-label,
button ion-label {
  pointer-events: none;
}

button {
  border: 0;
  border-radius: 0;
  font-family: inherit;
  font-style: inherit;
  font-variant: inherit;
  line-height: 1;
  text-transform: none;
  cursor: pointer;
  -webkit-appearance: button;
}

[tappable] {
  cursor: pointer;
}

a[disabled],
button[disabled],
html input[disabled] {
  cursor: default;
}

button::-moz-focus-inner,
input::-moz-focus-inner {
  padding: 0;
  border: 0;
}

input[type=checkbox],
input[type=radio] {
  padding: 0;
  box-sizing: border-box;
}

input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
  height: auto;
}

input[type=search]::-webkit-search-cancel-button,
input[type=search]::-webkit-search-decoration {
  -webkit-appearance: none;
}

table {
  border-collapse: collapse;
  border-spacing: 0;
}

td,
th {
  padding: 0;
}

* {
  box-sizing: border-box;
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
  -webkit-tap-highlight-color: transparent;
  -webkit-touch-callout: none;
}

html {
  width: 100%;
  height: 100%;
  -webkit-text-size-adjust: 100%;
     -moz-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
          text-size-adjust: 100%;
}

html:not(.hydrated) body {
  display: none;
}

html.plt-pwa {
  height: 100vh;
}

body {
  -moz-osx-font-smoothing: grayscale;
  -webkit-font-smoothing: antialiased;
  margin-left: 0;
  margin-right: 0;
  margin-top: 0;
  margin-bottom: 0;
  padding-left: 0;
  padding-right: 0;
  padding-top: 0;
  padding-bottom: 0;
  position: fixed;
  width: 100%;
  max-width: 100%;
  height: 100%;
  max-height: 100%;
  text-rendering: optimizeLegibility;
  overflow: hidden;
  touch-action: manipulation;
  -webkit-user-drag: none;
  -ms-content-zooming: none;
  word-wrap: break-word;
  overscroll-behavior-y: none;
  -webkit-text-size-adjust: none;
     -moz-text-size-adjust: none;
      -ms-text-size-adjust: none;
          text-size-adjust: none;
}

html {
  font-family: var(--ion-font-family);
}

a {
  background-color: transparent;
  color: var(--ion-color-primary, #3880ff);
}

h1,
h2,
h3,
h4,
h5,
h6 {
  margin-top: 16px;
  margin-bottom: 10px;
  font-weight: 500;
  line-height: 1.2;
}

h1 {
  margin-top: 20px;
  font-size: 26px;
}

h2 {
  margin-top: 18px;
  font-size: 24px;
}

h3 {
  font-size: 22px;
}

h4 {
  font-size: 20px;
}

h5 {
  font-size: 18px;
}

h6 {
  font-size: 16px;
}

small {
  font-size: 75%;
}

sub,
sup {
  position: relative;
  font-size: 75%;
  line-height: 0;
  vertical-align: baseline;
}

sup {
  top: -0.5em;
}

sub {
  bottom: -0.25em;
}

.ion-hide {
  display: none !important;
}

.ion-hide-up {
  display: none !important;
}

@media (max-width: 575px) {
  .ion-hide-down {
    display: none !important;
  }
}

@media (min-width: 576px) {
  .ion-hide-sm-up {
    display: none !important;
  }
}

@media (max-width: 767px) {
  .ion-hide-sm-down {
    display: none !important;
  }
}

@media (min-width: 768px) {
  .ion-hide-md-up {
    display: none !important;
  }
}

@media (max-width: 991px) {
  .ion-hide-md-down {
    display: none !important;
  }
}

@media (min-width: 992px) {
  .ion-hide-lg-up {
    display: none !important;
  }
}

@media (max-width: 1199px) {
  .ion-hide-lg-down {
    display: none !important;
  }
}

@media (min-width: 1200px) {
  .ion-hide-xl-up {
    display: none !important;
  }
}

.ion-hide-xl-down {
  display: none !important;
}

.ion-no-padding,
[no-padding] {
  --padding-start: 0;
  --padding-end: 0;
  --padding-top: 0;
  --padding-bottom: 0;
  padding-left: 0;
  padding-right: 0;
  padding-top: 0;
  padding-bottom: 0;
}

.ion-padding,
[padding] {
  --padding-start: var(--ion-padding, 16px);
  --padding-end: var(--ion-padding, 16px);
  --padding-top: var(--ion-padding, 16px);
  --padding-bottom: var(--ion-padding, 16px);
  padding-left: var(--ion-padding, 16px);
  padding-right: var(--ion-padding, 16px);
  padding-top: var(--ion-padding, 16px);
  padding-bottom: var(--ion-padding, 16px);
}

@supports ((-webkit-margin-start: 0) or (margin-inline-start: 0)) or (-webkit-margin-start: 0) {
  .ion-padding,
[padding] {
    padding-left: unset;
    padding-right: unset;
    -webkit-padding-start: var(--ion-padding, 16px);
    padding-inline-start: var(--ion-padding, 16px);
    -webkit-padding-end: var(--ion-padding, 16px);
    padding-inline-end: var(--ion-padding, 16px);
  }
}

.ion-padding-top,
[padding-top] {
  --padding-top: var(--ion-padding, 16px);
  padding-top: var(--ion-padding, 16px);
}

.ion-padding-start,
[padding-start] {
  --padding-start: var(--ion-padding, 16px);
  padding-left: var(--ion-padding, 16px);
}

@supports ((-webkit-margin-start: 0) or (margin-inline-start: 0)) or (-webkit-margin-start: 0) {
  .ion-padding-start,
[padding-start] {
    padding-left: unset;
    -webkit-padding-start: var(--ion-padding, 16px);
    padding-inline-start: var(--ion-padding, 16px);
  }
}

.ion-padding-end,
[padding-end] {
  --padding-end: var(--ion-padding, 16px);
  padding-right: var(--ion-padding, 16px);
}

@supports ((-webkit-margin-start: 0) or (margin-inline-start: 0)) or (-webkit-margin-start: 0) {
  .ion-padding-end,
[padding-end] {
    padding-right: unset;
    -webkit-padding-end: var(--ion-padding, 16px);
    padding-inline-end: var(--ion-padding, 16px);
  }
}

.ion-padding-bottom,
[padding-bottom] {
  --padding-bottom: var(--ion-padding, 16px);
  padding-bottom: var(--ion-padding, 16px);
}

.ion-padding-vertical,
[padding-vertical] {
  --padding-top: var(--ion-padding, 16px);
  --padding-bottom: var(--ion-padding, 16px);
  padding-top: var(--ion-padding, 16px);
  padding-bottom: var(--ion-padding, 16px);
}

.ion-padding-horizontal,
[padding-horizontal] {
  --padding-start: var(--ion-padding, 16px);
  --padding-end: var(--ion-padding, 16px);
  padding-left: var(--ion-padding, 16px);
  padding-right: var(--ion-padding, 16px);
}

@supports ((-webkit-margin-start: 0) or (margin-inline-start: 0)) or (-webkit-margin-start: 0) {
  .ion-padding-horizontal,
[padding-horizontal] {
    padding-left: unset;
    padding-right: unset;
    -webkit-padding-start: var(--ion-padding, 16px);
    padding-inline-start: var(--ion-padding, 16px);
    -webkit-padding-end: var(--ion-padding, 16px);
    padding-inline-end: var(--ion-padding, 16px);
  }
}

.ion-no-margin,
[no-margin] {
  --margin-start: 0;
  --margin-end: 0;
  --margin-top: 0;
  --margin-bottom: 0;
  margin-left: 0;
  margin-right: 0;
  margin-top: 0;
  margin-bottom: 0;
}

.ion-margin,
[margin] {
  --margin-start: var(--ion-margin, 16px);
  --margin-end: var(--ion-margin, 16px);
  --margin-top: var(--ion-margin, 16px);
  --margin-bottom: var(--ion-margin, 16px);
  margin-left: var(--ion-margin, 16px);
  margin-right: var(--ion-margin, 16px);
  margin-top: var(--ion-margin, 16px);
  margin-bottom: var(--ion-margin, 16px);
}

@supports ((-webkit-margin-start: 0) or (margin-inline-start: 0)) or (-webkit-margin-start: 0) {
  .ion-margin,
[margin] {
    margin-left: unset;
    margin-right: unset;
    -webkit-margin-start: var(--ion-margin, 16px);
    margin-inline-start: var(--ion-margin, 16px);
    -webkit-margin-end: var(--ion-margin, 16px);
    margin-inline-end: var(--ion-margin, 16px);
  }
}

.ion-margin-top,
[margin-top] {
  --margin-top: var(--ion-margin, 16px);
  margin-top: var(--ion-margin, 16px);
}

.ion-margin-start,
[margin-start] {
  --margin-start: var(--ion-margin, 16px);
  margin-left: var(--ion-margin, 16px);
}

@supports ((-webkit-margin-start: 0) or (margin-inline-start: 0)) or (-webkit-margin-start: 0) {
  .ion-margin-start,
[margin-start] {
    margin-left: unset;
    -webkit-margin-start: var(--ion-margin, 16px);
    margin-inline-start: var(--ion-margin, 16px);
  }
}

.ion-margin-end,
[margin-end] {
  --margin-end: var(--ion-margin, 16px);
  margin-right: var(--ion-margin, 16px);
}

@supports ((-webkit-margin-start: 0) or (margin-inline-start: 0)) or (-webkit-margin-start: 0) {
  .ion-margin-end,
[margin-end] {
    margin-right: unset;
    -webkit-margin-end: var(--ion-margin, 16px);
    margin-inline-end: var(--ion-margin, 16px);
  }
}

.ion-margin-bottom,
[margin-bottom] {
  --margin-bottom: var(--ion-margin, 16px);
  margin-bottom: var(--ion-margin, 16px);
}

.ion-margin-vertical,
[margin-vertical] {
  --margin-top: var(--ion-margin, 16px);
  --margin-bottom: var(--ion-margin, 16px);
  margin-top: var(--ion-margin, 16px);
  margin-bottom: var(--ion-margin, 16px);
}

.ion-margin-horizontal,
[margin-horizontal] {
  --margin-start: var(--ion-margin, 16px);
  --margin-end: var(--ion-margin, 16px);
  margin-left: var(--ion-margin, 16px);
  margin-right: var(--ion-margin, 16px);
}

@supports ((-webkit-margin-start: 0) or (margin-inline-start: 0)) or (-webkit-margin-start: 0) {
  .ion-margin-horizontal,
[margin-horizontal] {
    margin-left: unset;
    margin-right: unset;
    -webkit-margin-start: var(--ion-margin, 16px);
    margin-inline-start: var(--ion-margin, 16px);
    -webkit-margin-end: var(--ion-margin, 16px);
    margin-inline-end: var(--ion-margin, 16px);
  }
}

.ion-float-left,
[float-left] {
  float: left !important;
}

.ion-float-right,
[float-right] {
  float: right !important;
}

.ion-float-start,
[float-start] {
  float: left !important;
}

[dir=rtl] .ion-float-start, :host-context([dir=rtl]) .ion-float-start, [dir=rtl] [float-start], :host-context([dir=rtl]) [float-start] {
  float: right !important;
}

.ion-float-end,
[float-end] {
  float: right !important;
}

[dir=rtl] .ion-float-end, :host-context([dir=rtl]) .ion-float-end, [dir=rtl] [float-end], :host-context([dir=rtl]) [float-end] {
  float: left !important;
}

@media (min-width: 576px) {
  .ion-float-sm-left,
[float-sm-left] {
    float: left !important;
  }

  .ion-float-sm-right,
[float-sm-right] {
    float: right !important;
  }

  .ion-float-sm-start,
[float-sm-start] {
    float: left !important;
  }
  [dir=rtl] .ion-float-sm-start, :host-context([dir=rtl]) .ion-float-sm-start, [dir=rtl] [float-sm-start], :host-context([dir=rtl]) [float-sm-start] {
    float: right !important;
  }

  .ion-float-sm-end,
[float-sm-end] {
    float: right !important;
  }
  [dir=rtl] .ion-float-sm-end, :host-context([dir=rtl]) .ion-float-sm-end, [dir=rtl] [float-sm-end], :host-context([dir=rtl]) [float-sm-end] {
    float: left !important;
  }
}

@media (min-width: 768px) {
  .ion-float-md-left,
[float-md-left] {
    float: left !important;
  }

  .ion-float-md-right,
[float-md-right] {
    float: right !important;
  }

  .ion-float-md-start,
[float-md-start] {
    float: left !important;
  }
  [dir=rtl] .ion-float-md-start, :host-context([dir=rtl]) .ion-float-md-start, [dir=rtl] [float-md-start], :host-context([dir=rtl]) [float-md-start] {
    float: right !important;
  }

  .ion-float-md-end,
[float-md-end] {
    float: right !important;
  }
  [dir=rtl] .ion-float-md-end, :host-context([dir=rtl]) .ion-float-md-end, [dir=rtl] [float-md-end], :host-context([dir=rtl]) [float-md-end] {
    float: left !important;
  }
}

@media (min-width: 992px) {
  .ion-float-lg-left,
[float-lg-left] {
    float: left !important;
  }

  .ion-float-lg-right,
[float-lg-right] {
    float: right !important;
  }

  .ion-float-lg-start,
[float-lg-start] {
    float: left !important;
  }
  [dir=rtl] .ion-float-lg-start, :host-context([dir=rtl]) .ion-float-lg-start, [dir=rtl] [float-lg-start], :host-context([dir=rtl]) [float-lg-start] {
    float: right !important;
  }

  .ion-float-lg-end,
[float-lg-end] {
    float: right !important;
  }
  [dir=rtl] .ion-float-lg-end, :host-context([dir=rtl]) .ion-float-lg-end, [dir=rtl] [float-lg-end], :host-context([dir=rtl]) [float-lg-end] {
    float: left !important;
  }
}

@media (min-width: 1200px) {
  .ion-float-xl-left,
[float-xl-left] {
    float: left !important;
  }

  .ion-float-xl-right,
[float-xl-right] {
    float: right !important;
  }

  .ion-float-xl-start,
[float-xl-start] {
    float: left !important;
  }
  [dir=rtl] .ion-float-xl-start, :host-context([dir=rtl]) .ion-float-xl-start, [dir=rtl] [float-xl-start], :host-context([dir=rtl]) [float-xl-start] {
    float: right !important;
  }

  .ion-float-xl-end,
[float-xl-end] {
    float: right !important;
  }
  [dir=rtl] .ion-float-xl-end, :host-context([dir=rtl]) .ion-float-xl-end, [dir=rtl] [float-xl-end], :host-context([dir=rtl]) [float-xl-end] {
    float: left !important;
  }
}

.ion-text-center,
[text-center] {
  text-align: center !important;
}

.ion-text-justify,
[text-justify] {
  text-align: justify !important;
}

.ion-text-start,
[text-start] {
  text-align: start !important;
}

.ion-text-end,
[text-end] {
  text-align: end !important;
}

.ion-text-left,
[text-left] {
  text-align: left !important;
}

.ion-text-right,
[text-right] {
  text-align: right !important;
}

.ion-text-nowrap,
[text-nowrap] {
  white-space: nowrap !important;
}

.ion-text-wrap,
[text-wrap] {
  white-space: normal !important;
}

@media (min-width: 576px) {
  .ion-text-sm-center,
[text-sm-center] {
    text-align: center !important;
  }

  .ion-text-sm-justify,
[text-sm-justify] {
    text-align: justify !important;
  }

  .ion-text-sm-start,
[text-sm-start] {
    text-align: start !important;
  }

  .ion-text-sm-end,
[text-sm-end] {
    text-align: end !important;
  }

  .ion-text-sm-left,
[text-sm-left] {
    text-align: left !important;
  }

  .ion-text-sm-right,
[text-sm-right] {
    text-align: right !important;
  }

  .ion-text-sm-nowrap,
[text-sm-nowrap] {
    white-space: nowrap !important;
  }

  .ion-text-sm-wrap,
[text-sm-wrap] {
    white-space: normal !important;
  }
}

@media (min-width: 768px) {
  .ion-text-md-center,
[text-md-center] {
    text-align: center !important;
  }

  .ion-text-md-justify,
[text-md-justify] {
    text-align: justify !important;
  }

  .ion-text-md-start,
[text-md-start] {
    text-align: start !important;
  }

  .ion-text-md-end,
[text-md-end] {
    text-align: end !important;
  }

  .ion-text-md-left,
[text-md-left] {
    text-align: left !important;
  }

  .ion-text-md-right,
[text-md-right] {
    text-align: right !important;
  }

  .ion-text-md-nowrap,
[text-md-nowrap] {
    white-space: nowrap !important;
  }

  .ion-text-md-wrap,
[text-md-wrap] {
    white-space: normal !important;
  }
}

@media (min-width: 992px) {
  .ion-text-lg-center,
[text-lg-center] {
    text-align: center !important;
  }

  .ion-text-lg-justify,
[text-lg-justify] {
    text-align: justify !important;
  }

  .ion-text-lg-start,
[text-lg-start] {
    text-align: start !important;
  }

  .ion-text-lg-end,
[text-lg-end] {
    text-align: end !important;
  }

  .ion-text-lg-left,
[text-lg-left] {
    text-align: left !important;
  }

  .ion-text-lg-right,
[text-lg-right] {
    text-align: right !important;
  }

  .ion-text-lg-nowrap,
[text-lg-nowrap] {
    white-space: nowrap !important;
  }

  .ion-text-lg-wrap,
[text-lg-wrap] {
    white-space: normal !important;
  }
}

@media (min-width: 1200px) {
  .ion-text-xl-center,
[text-xl-center] {
    text-align: center !important;
  }

  .ion-text-xl-justify,
[text-xl-justify] {
    text-align: justify !important;
  }

  .ion-text-xl-start,
[text-xl-start] {
    text-align: start !important;
  }

  .ion-text-xl-end,
[text-xl-end] {
    text-align: end !important;
  }

  .ion-text-xl-left,
[text-xl-left] {
    text-align: left !important;
  }

  .ion-text-xl-right,
[text-xl-right] {
    text-align: right !important;
  }

  .ion-text-xl-nowrap,
[text-xl-nowrap] {
    white-space: nowrap !important;
  }

  .ion-text-xl-wrap,
[text-xl-wrap] {
    white-space: normal !important;
  }
}

.ion-text-uppercase,
[text-uppercase] {
  /* stylelint-disable-next-line declaration-no-important */
  text-transform: uppercase !important;
}

.ion-text-lowercase,
[text-lowercase] {
  /* stylelint-disable-next-line declaration-no-important */
  text-transform: lowercase !important;
}

.ion-text-capitalize,
[text-capitalize] {
  /* stylelint-disable-next-line declaration-no-important */
  text-transform: capitalize !important;
}

@media (min-width: 576px) {
  .ion-text-sm-uppercase,
[text-sm-uppercase] {
    /* stylelint-disable-next-line declaration-no-important */
    text-transform: uppercase !important;
  }

  .ion-text-sm-lowercase,
[text-sm-lowercase] {
    /* stylelint-disable-next-line declaration-no-important */
    text-transform: lowercase !important;
  }

  .ion-text-sm-capitalize,
[text-sm-capitalize] {
    /* stylelint-disable-next-line declaration-no-important */
    text-transform: capitalize !important;
  }
}

@media (min-width: 768px) {
  .ion-text-md-uppercase,
[text-md-uppercase] {
    /* stylelint-disable-next-line declaration-no-important */
    text-transform: uppercase !important;
  }

  .ion-text-md-lowercase,
[text-md-lowercase] {
    /* stylelint-disable-next-line declaration-no-important */
    text-transform: lowercase !important;
  }

  .ion-text-md-capitalize,
[text-md-capitalize] {
    /* stylelint-disable-next-line declaration-no-important */
    text-transform: capitalize !important;
  }
}

@media (min-width: 992px) {
  .ion-text-lg-uppercase,
[text-lg-uppercase] {
    /* stylelint-disable-next-line declaration-no-important */
    text-transform: uppercase !important;
  }

  .ion-text-lg-lowercase,
[text-lg-lowercase] {
    /* stylelint-disable-next-line declaration-no-important */
    text-transform: lowercase !important;
  }

  .ion-text-lg-capitalize,
[text-lg-capitalize] {
    /* stylelint-disable-next-line declaration-no-important */
    text-transform: capitalize !important;
  }
}

@media (min-width: 1200px) {
  .ion-text-xl-uppercase,
[text-xl-uppercase] {
    /* stylelint-disable-next-line declaration-no-important */
    text-transform: uppercase !important;
  }

  .ion-text-xl-lowercase,
[text-xl-lowercase] {
    /* stylelint-disable-next-line declaration-no-important */
    text-transform: lowercase !important;
  }

  .ion-text-xl-capitalize,
[text-xl-capitalize] {
    /* stylelint-disable-next-line declaration-no-important */
    text-transform: capitalize !important;
  }
}

.ion-align-self-start,
[align-self-start] {
  align-self: flex-start !important;
}

.ion-align-self-end,
[align-self-end] {
  align-self: flex-end !important;
}

.ion-align-self-center,
[align-self-center] {
  align-self: center !important;
}

.ion-align-self-stretch,
[align-self-stretch] {
  align-self: stretch !important;
}

.ion-align-self-baseline,
[align-self-baseline] {
  align-self: baseline !important;
}

.ion-align-self-auto,
[align-self-auto] {
  align-self: auto !important;
}

.ion-wrap,
[wrap] {
  flex-wrap: wrap !important;
}

.ion-nowrap,
[nowrap] {
  flex-wrap: nowrap !important;
}

.ion-wrap-reverse,
[wrap-reverse] {
  flex-wrap: wrap-reverse !important;
}

.ion-justify-content-start,
[justify-content-start] {
  -webkit-box-pack: start !important;
          justify-content: flex-start !important;
}

.ion-justify-content-center,
[justify-content-center] {
  -webkit-box-pack: center !important;
          justify-content: center !important;
}

.ion-justify-content-end,
[justify-content-end] {
  -webkit-box-pack: end !important;
          justify-content: flex-end !important;
}

.ion-justify-content-around,
[justify-content-around] {
  justify-content: space-around !important;
}

.ion-justify-content-between,
[justify-content-between] {
  -webkit-box-pack: justify !important;
          justify-content: space-between !important;
}

.ion-justify-content-evenly,
[justify-content-evenly] {
  -webkit-box-pack: space-evenly !important;
          justify-content: space-evenly !important;
}

.ion-align-items-start,
[align-items-start] {
  -webkit-box-align: start !important;
          align-items: flex-start !important;
}

.ion-align-items-center,
[align-items-center] {
  -webkit-box-align: center !important;
          align-items: center !important;
}

.ion-align-items-end,
[align-items-end] {
  -webkit-box-align: end !important;
          align-items: flex-end !important;
}

.ion-align-items-stretch,
[align-items-stretch] {
  -webkit-box-align: stretch !important;
          align-items: stretch !important;
}

.ion-align-items-baseline,
[align-items-baseline] {
  -webkit-box-align: baseline !important;
          align-items: baseline !important;
}

.txt-filter {
  color: #828282;
  font-size: 13px;
  letter-spacing: 0.2px;
  line-height: 15px;
  color: #8e9091;
  font-weight: 500; }

.txt-filter.green {
    color: #00d2bd; }

.navBarSmallText {
  color: #00d2bd;
  font-size: 16px;
  letter-spacing: 0.2px;
  line-height: 19px;
  font-weight: 500; }

.card-small-txt {
  font-size: 13px;
  font-weight: 500;
  letter-spacing: 0.2px;
  line-height: 18px;
  color: #848F9A; }

.card-small-txt.light {
    color: #B1BCC6; }

app-button {
  flex-shrink: 0; }

app-button:last-child .btn-with-image {
    margin-right: 20px; }

app-button:last-child:first-child .btn-with-image {
    margin-right: 8px; }

.container-btn {
  width: 50%;
  padding: 0 20px; }

.container-btn .btn {
    border-radius: 24px;
    padding: 0 14px; }

.container-btn .btn .image-left {
      margin-right: auto; }

.container-btn .btn .text {
      width: 100%;
      padding: 0 3px; }

.container-btn app-button {
    width: 100%; }

.container-btn.right {
    padding-right: 8px; }

.container-btn.left {
    padding-left: 8px; }

.btn {
  height: 48px;
  width: 100%;
  border-radius: 8px;
  display: -webkit-box;
  display: flex;
  -webkit-box-pack: center;
          justify-content: center;
  -webkit-box-align: center;
          align-items: center;
  min-width: 72px;
  padding: 0 8px; }

.btn.small-height {
    height: 32px;
    padding: 0 8px; }

.btn.green {
    background-color: #00d2bd;
    color: #ffffff; }

.btn.green.createText::after {
      content: &quot;Create Account&quot;;
      font-weight: 600;
      text-transform: none;
      font-size: 16px;
      letter-spacing: 0.2px;
      line-height: 22px;
      color: white; }

.btn.green-blue {
    background-color: #3ABCA5; }

.btn.gradient-green {
    background-color: #00d2bd;
    color: #ffffff; }

.btn.link-help {
    background-color: transparent; }

.btn.light-blue {
    background-color: #F5F8FF; }

.btn.darkGray {
    background-color: #848F9A; }

.btn.white {
    background-color: #ffffff; }

.btn.green-border {
    box-shadow: 0 0 2px 0 rgba(0, 0, 0, 0.12);
    border: 1px solid #00d2bd;
    background: #ffffff; }

.btn.green-border.loginText::after {
      content: &quot;Log in&quot;;
      font-weight: 600;
      text-transform: none;
      font-size: 16px;
      letter-spacing: 0.2px;
      line-height: 22px;
      color: #00d2bd; }

.btn.white-with-border {
    background-color: #ffffff;
    border: 1px solid #00d2bd;
    color: #00d2bd;
    height: 42px; }

.btn.white-with-border.dark-green {
      border-color: #00A5C9;
      color: #00A5C9;
      font-family: 'Open Sans', sans-serif !important; }

.btn.white-with-border.dark-blue {
      border-color: #39579A;
      color: #39579A;
      font-family: 'Open Sans', sans-serif !important; }

.btn.white-with-border.green {
      border-color: #007bb5;
      color: #007bb5;
      font-family: 'Open Sans', sans-serif !important; }

.btn .image-right {
    width: 11px;
    margin-left: 6px; }

.btn .image-left {
    width: 20px;
    margin-right: 8px; }

.btn.inputButton {
    padding: 0 16px;
    background-color: #F0F5FF;
    color: #000000;
    margin-bottom: 30px;
    margin-top: 6px; }

.btn.inputButton .image-right {
      width: 16px;
      margin-left: auto; }

.btn.inputButton.left-item {
      text-align: left; }

.btn.complexButton {
    height: 32px;
    border-radius: 6px;
    min-width: auto;
    width: 72px; }

.btn-with-image {
  height: 39px;
  background-color: #ffffff;
  border-radius: 6px;
  display: -webkit-box;
  display: flex;
  -webkit-box-pack: center;
          justify-content: center;
  -webkit-box-align: center;
          align-items: center;
  padding: 0px 12px;
  margin-right: 12px;
  max-width: 300px;
  box-shadow: inset 0 -2px 0 0 #f1f3f5;
  border: 1px solid #f1f3f5;
  border-bottom: 0;
  background: #ffffff !important;
  margin-bottom: 3px;
  padding-bottom: 1px; }

.btn-with-image .image-right {
    width: 16px;
    margin-left: 6px; }

.btn-with-image .image-left {
    width: 16px;
    margin-right: 6px; }

.btn-with-image.no-border {
    border: 0; }

.btn-with-image .close-icon {
    margin-left: 10px;
    font-size: 15px; }

.wrap-button {
  margin-top: -16px; }

.wrap-button .btn-with-image {
    margin-top: 16px;
    margin-bottom: 3px; }

.trim-one-line {
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 1;
  /* autoprefixer: ignore next */
  -webkit-box-orient: vertical; }

.button-withImage-text {
  white-space: nowrap;
  display: block;
  text-overflow: ellipsis;
  overflow: hidden;
  line-height: normal; }

.send-button {
  position: absolute;
  bottom: 12px;
  right: 12px;
  background: transparent; }

.mainSectionClass {
  border-bottom: 1px solid #f1f3f5; }

.mainSectionClass .seperate-left-container {
    margin-left: 12px; }

.mainSectionClass .seprator-left-item {
    margin-top: 8px; }

.seperate-with-image {
  display: -webkit-box;
  display: flex;
  margin-bottom: 20px;
  background-color: #ffffff; }

.seperate-with-image.no-margin {
    margin-top: 0; }

.seperate-with-image .seperate-left-container {
    display: -webkit-box;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
            flex-direction: column;
    -webkit-box-align: start;
            align-items: flex-start;
    text-align: left; }

.seperate-with-image .seperate-left-container .seperator-image {
      width: 12px;
      margin-right: 9px; }

.seperate-with-image .seprator-left-item {
    margin-left: auto;
    height: 33px;
    padding: 0 8px;
    border-radius: 14px;
    display: -webkit-box;
    display: flex;
    -webkit-box-pack: center;
            justify-content: center;
    -webkit-box-align: center;
            align-items: center;
    text-transform: capitalize;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
            flex-direction: column;
    flex-shrink: 0; }

.seperate-with-image .seprator-left-item img {
      width: 18px;
      margin-left: 6px; }

.seperate-with-image .seprator-left-item:active {
      -webkit-transform: scale(0.9);
              transform: scale(0.9); }

.seperate-with-image.main .seperator-image {
    width: 32px;
    height: 32px;
    margin-top: 6px; }

.seperate-with-image.main.no-border {
    border: 0;
    padding-bottom: 0; }

.seperate-with-image.speacial-case {
    padding: 24px 20px;
    padding-bottom: 0;
    border-radius: 13px 13px 0 0;
    margin-top: -13px; }

.ios .seprator-left-item {
  padding-top: 1px;
  padding-left: 12px; }

.simple-separator {
  display: -webkit-box;
  display: flex;
  padding-top: 25px;
  -webkit-box-align: center;
          align-items: center; }

.simple-separator.border {
    border-bottom: 1px solid rgba(215, 215, 234, 0.46); }

.simple-separator .right {
    margin-left: auto;
    display: -webkit-box;
    display: flex; }

.simple-separator .right img {
      width: 12px;
      margin-left: 6px; }

.simple-separator.left-subtitle {
    margin-top: 2px; }

.image-container {
  width: 18px;
  height: 18px;
  background-image: url(&quot;/assets/icons/diasporaIcon/More_Arrow.svg&quot;);
  background-repeat: no-repeat; }

app-list:last-child .list .list-items-middle {
  border: 0; }

app-list:last-child .list .list-items-right {
  border: 0; }

app-list:last-child .list-container {
  border: 0; }

app-list:last-child .small {
  border-bottom: 0; }

app-list:last-child .simple-list .list-container {
  border-bottom: 0; }

app-list-card:last-child .list .list-items-middle {
  border: 0;
  padding-bottom: 10px; }

app-list-card:last-child .list-container {
  border: 0; }

app-list-card:last-child .list-container .list-items-right {
    border-bottom: 0 !important; }

.app-list-container:last-child .second-type-list {
  border: 0;
  margin-bottom: 10px;
  padding-bottom: 0px; }

.app-list-container:last-child .with-padding-bottom .second-type-list {
  padding-bottom: 10px; }

app-filter-people-result:last-child .list .list-items-middle {
  border: 0; }

app-card-search:last-child .list .list-items-middle {
  border: 0;
  padding-bottom: 10px; }

app-card-search:last-child .list .list-items-right {
  border: 0 !important; }

.list-scroll-container {
  flex-shrink: 0;
  padding-left: 8px;
  min-width: 96%; }

.list-scroll-container:first-child {
    padding: 0; }

.list.big-list {
  min-height: 60px; }

.list .list-container {
  display: -webkit-box;
  display: flex;
  margin-top: 19px;
  background: #ffffff; }

.list .list-container .first-txt {
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    /* autoprefixer: ignore next */
    -webkit-box-orient: vertical; }

.list .list-container .list-items-left {
    flex-shrink: 0;
    height: 54px;
    width: 54px;
    background-color: #f1f3f5;
    border-radius: 12px;
    border: 1px solid #f1f3f5; }

.list .list-container .list-items-left img {
      width: 54px;
      height: 52px;
      border-radius: 50%;
      -o-object-fit: cover;
         object-fit: cover; }

.list .list-container .list-items-left .small-radius {
      border-radius: 12px; }

.list .list-container .list-items-middle {
    margin-left: 14px;
    border-bottom: 1px solid #f1f3f5;
    min-height: 56px;
    width: calc(100% - 55px);
    padding-bottom: 19px;
    text-align: left;
    padding-right: 10px; }

.list .list-container .list-items-middle .second-txt {
      width: 95%;
      text-align: left;
      margin-top: 3px;
      overflow: hidden;
      text-overflow: ellipsis;
      display: -webkit-box;
      -webkit-line-clamp: 1;
      /* autoprefixer: ignore next */
      -webkit-box-orient: vertical; }

.list .list-container .list-items-middle .list-items-middle-container {
      width: 100%;
      padding-right: 16px; }

.list .list-container .list-items-right {
    margin-left: auto;
    margin-top: 16px;
    flex-shrink: 0;
    border-bottom: 1px solid #f1f3f5 !important; }

.list .list-container .list-items-right img {
      height: 23.5px;
      width: 23.5px; }

.list .list-container .list-items-right.big {
      margin-top: 14px !important;
      padding-left: 10px; }

.list .list-container .list-items-right.big img {
        height: 27px;
        width: 27px; }

.list .list-container .list-items-right.center {
      margin-top: 14px !important; }

.list .list-container .list-items-right.left-gray-line {
      position: relative; }

.list .list-container .list-items-right.left-gray-line::before {
        content: &quot; &quot;;
        position: absolute;
        height: 30px;
        width: 2px;
        background-color: #d0d3d6;
        right: 34px; }

.list .list-container.with-one-text {
    -webkit-box-align: center;
            align-items: center;
    margin-top: 0px; }

.list .list-container.with-one-text .second-txt {
      margin-top: 0; }

.list .list-container.with-one-text .list-items-middle {
      display: -webkit-box;
      display: flex;
      -webkit-box-align: center;
              align-items: center;
      padding: 0;
      min-height: 42px; }

.list .list-container.with-one-text .list-items-middle.big-list {
        min-height: 60px; }

.list .list-container.with-one-text .list-items-middle .right-icon {
        width: 12px;
        margin-left: auto;
        flex-shrink: 0; }

.list .list-container .entity-entity-type {
    margin-top: 1px; }

.list .list-container.center .list-items-middle {
    display: -webkit-box;
    display: flex;
    -webkit-box-pack: center;
            justify-content: center;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
            flex-direction: column;
    padding-bottom: 0; }

.list .list-container.center .list-items-right {
    margin-top: 13px; }

.swiper-container .first-txt {
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 1 !important;
  /* autoprefixer: ignore next */
  -webkit-box-orient: vertical; }

.no-trim .first-txt {
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 6 !important;
  /* autoprefixer: ignore next */
  -webkit-box-orient: vertical; }

.no-trim .small-list {
  max-width: 100% !important; }

.no-trim .list .list-container {
  margin-top: 0 !important;
  padding-top: 20px !important; }

.second-type-list {
  display: -webkit-box;
  display: flex;
  margin: 20px 0;
  text-align: left;
  border-bottom: 1px solid #f1f3f5;
  background-color: #ffffff;
  font-weight: 600;
  padding-bottom: 20px; }

.second-type-list .left-item {
    width: 100%; }

.second-type-list .left-item .start-item {
      display: -webkit-box;
      display: flex;
      -webkit-box-align: center;
              align-items: center;
      margin-bottom: 9px; }

.second-type-list .left-item .start-item span {
        overflow: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;
        -webkit-line-clamp: 1;
        /* autoprefixer: ignore next */
        -webkit-box-orient: vertical;
        padding-right: 10px;
        margin-left: 8px;
        width: 100%; }

.second-type-list .left-item .middle-item {
      margin-bottom: 6px;
      overflow: hidden;
      text-overflow: ellipsis;
      display: -webkit-box;
      -webkit-line-clamp: 2;
      /* autoprefixer: ignore next */
      -webkit-box-orient: vertical;
      width: 90%; }

.second-type-list .right-item {
    flex-shrink: 0;
    margin-left: auto;
    height: 80px;
    width: 80px;
    background-color: #f1f3f5;
    border-radius: 16px; }

.second-type-list .right-item img {
      height: 80px;
      width: 80px;
      border-radius: 16px;
      -o-object-fit: cover;
         object-fit: cover; }

.second-type-list .image-item {
    height: 20px;
    width: 20px;
    border-radius: 4px;
    -o-object-fit: cover;
       object-fit: cover;
    border: 1px solid #f1f3f5; }

.third-type-list {
  width: 100%;
  padding: 16px 15px;
  background-color: #ffffff;
  box-shadow: inset 0 -2px 0 0 #f1f3f5;
  border: 1px solid #f1f3f5;
  border-bottom: 0;
  border-radius: 8px;
  display: -webkit-box;
  display: flex; }

.third-type-list .left {
    height: 44px;
    width: 44px;
    flex-shrink: 0; }

.third-type-list .left img {
      width: 44px;
      height: 44px;
      -o-object-fit: cover;
         object-fit: cover; }

.third-type-list .middle {
    margin-left: 12px;
    text-align: left;
    padding-right: 2px; }

.third-type-list .middle .first {
      overflow: hidden;
      text-overflow: ellipsis;
      display: -webkit-box;
      -webkit-line-clamp: 1;
      /* autoprefixer: ignore next */
      -webkit-box-orient: vertical; }

.third-type-list .middle .second {
      margin-top: 5px;
      overflow: hidden;
      text-overflow: ellipsis;
      display: -webkit-box;
      -webkit-line-clamp: 1;
      /* autoprefixer: ignore next */
      -webkit-box-orient: vertical; }

.third-type-list .right {
    margin-left: auto;
    display: -webkit-box;
    display: flex;
    -webkit-box-align: center;
            align-items: center;
    width: 24px; }

.third-type-list .right img {
      width: 24px;
      height: 24px; }

.third-type-list.small {
    padding: 16px 18px; }

.third-type-list.small .left {
      height: 36px;
      width: 36px;
      flex-shrink: 0; }

.third-type-list.small .left img {
        width: 36px;
        height: 36px;
        -o-object-fit: cover;
           object-fit: cover;
        border-radius: 8px; }

.third-type-list.small .right {
      width: 27px; }

.third-type-list.small .right img {
        width: 27px;
        height: 27px; }

.fourth-type-list {
  border-bottom: 1px solid #f1f3f5;
  padding-bottom: 20px; }

.fourth-type-list:not(:first-child) {
    padding-top: 6px; }

.fourth-type-list:last-child {
    border-bottom: 0 !important;
    padding-bottom: 10px; }

.fourth-type-list.no-border {
    border-bottom: 0; }

.fourth-type-list .container {
    display: -webkit-box;
    display: flex;
    margin-top: 20px;
    -webkit-box-align: center;
            align-items: center; }

.fourth-type-list .container img {
      width: 20px;
      height: 20px;
      margin-right: 12px; }

.fourth-type-list .container .txt-container .txt {
      padding-right: 8px; }

.fourth-type-list .right-item {
    margin-left: auto;
    display: -webkit-box;
    display: flex;
    flex-shrink: 0; }

.fourth-type-list .right-item .right-item-container {
      display: -webkit-box;
      display: flex;
      -webkit-box-align: center;
              align-items: center; }

.fourth-type-list .right-item .right-item-container img {
        width: 12px !important;
        height: 12px;
        margin-right: 4px; }

.fourth-type-list .right-item img {
      width: 24px;
      height: 24px;
      margin-right: 0;
      margin-left: 16px; }

.fourth-type-list .right-big-image {
    width: 108px;
    height: 108px;
    margin-left: auto;
    position: relative;
    flex-shrink: 0;
    margin-top: 20px; }

.fourth-type-list .right-big-image img {
      width: 100%;
      height: 100%;
      border-radius: 6px;
      -o-object-fit: cover;
         object-fit: cover; }

.fourth-type-list .right-big-image .overlay {
      width: 100%;
      height: 32px;
      border-radius: 0 0 6px 6px;
      background-color: rgba(0, 0, 0, 0.6);
      color: #ECEFF1;
      font-size: 13px;
      font-weight: 500;
      letter-spacing: 0.2px;
      line-height: 18px;
      text-align: center;
      position: absolute;
      bottom: 0;
      display: -webkit-box;
      display: flex;
      -webkit-box-pack: center;
              justify-content: center;
      -webkit-box-align: center;
              align-items: center; }

.action-list {
  box-sizing: border-box;
  height: 75px;
  border-radius: 12px;
  background-color: rgba(240, 245, 255, 0.98); }

.action-list .list-container {
    display: -webkit-box;
    display: flex;
    justify-content: space-around;
    -webkit-box-align: center;
            align-items: center;
    height: 100%; }

.action-list .list-container .item {
      display: -webkit-box;
      display: flex;
      -webkit-box-align: center;
              align-items: center;
      -webkit-box-orient: vertical;
      -webkit-box-direction: normal;
              flex-direction: column;
      color: #00d2bd;
      font-size: 13px;
      font-weight: 600;
      letter-spacing: 0.2px;
      line-height: 18px;
      text-align: center; }

.action-list .list-container .item img {
        width: 18px; }

.action-list .list-container .item span {
        margin-top: 4px; }

app-info-card:last-child .info-list {
  border: 0; }

.info-list {
  display: -webkit-box;
  display: flex;
  padding: 20px 0;
  border-bottom: 1px solid #F0F5FF;
  -webkit-box-align: center;
          align-items: center; }

.info-list .first-item {
    margin-right: 12px;
    flex-shrink: 0;
    width: 32px;
    display: -webkit-box;
    display: flex;
    -webkit-box-align: center;
            align-items: center;
    -webkit-box-pack: center;
            justify-content: center;
    border-radius: 12px;
    height: 54px; }

.info-list .first-item img {
      height: 32px;
      width: 32px;
      -o-object-fit: contain;
         object-fit: contain; }

.info-list .middle-item {
    font-size: 14px;
    padding: 5px 7px;
    width: 85%; }

.info-list .middle-item .title {
      overflow: hidden;
      text-overflow: ellipsis;
      display: -webkit-box;
      -webkit-line-clamp: 1;
      /* autoprefixer: ignore next */
      -webkit-box-orient: vertical; }

.info-list .middle-item .subtitle {
      font-family: 13px;
      color: #B1BCC6; }

.info-list .end-item {
    margin-left: auto;
    display: -webkit-box;
    display: flex;
    -webkit-box-align: center;
            align-items: center;
    flex-shrink: 0; }

.info-list .end-item img {
      height: 24px; }

app-media-card:last-child .list-image-container {
  margin: 0; }

.list-image {
  display: -webkit-box;
  display: flex;
  margin-top: 20px; }

.list-image app-media-card {
    width: 25%;
    margin-right: 10px; }

.list-image app-media-card:last-child {
      margin-right: 0; }

.list-image .list-image-container {
    margin-right: 10px;
    position: relative;
    border-radius: 6px;
    background-size: cover;
    background-position: center;
    width: 100%;
    padding-top: 100%; }

.list-image .list-image-container .icon {
      width: 30%;
      height: 30%;
      position: absolute;
      top: 50%;
      -webkit-transform: translate(-50%, -50%);
              transform: translate(-50%, -50%);
      left: 50%; }

.small-list {
  -webkit-box-align: center;
          align-items: center;
  min-height: 16px;
  margin-top: 3px;
  padding-right: 10px; }

.small-list:nth-child(2) {
    position: relative; }

.small-list:nth-child(2)::before {
      content: &quot; &quot;;
      height: 11px;
      width: 1px;
      background: #eaecef;
      margin-right: 10px;
      margin-top: 3px;
      flex-shrink: 0; }

.small-list:first-child {
    padding-left: 0;
    margin-left: 0;
    border: 0; }

.small-list .small-list-image {
    width: 16px;
    height: 12px;
    margin-right: 6px;
    -o-object-fit: cover;
       object-fit: cover;
    margin-top: 3px; }

.small-list .txt {
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 1;
    /* autoprefixer: ignore next */
    -webkit-box-orient: vertical;
    margin-top: 3px; }

ion-slide {
  background: #ffffff; }

ion-slide .small-list-inline-container {
    display: -webkit-box;
    display: flex; }

.small-list-inline-container {
  display: -webkit-box;
  display: flex; }

.small-list-inline-container .small-list:nth-child(2) {
    max-width: 80%; }

.list-with-btn-scroll {
  border-bottom: 1px solid rgba(214, 214, 233, 0.46);
  padding: 20px;
  padding-right: 0;
  padding-bottom: 17px;
  background: #ffffff; }

.list-with-btn-scroll.no-left-space {
    padding-left: 0; }

.list-with-btn-scroll.no-left-space .seperate-with-image {
      padding-left: 20px; }

.list-with-btn-scroll.no-left-space .scroll app-button:first-child .main-input-container {
      margin-left: 20px !important; }

.list-with-btn-scroll.no-left-space .filterButton app-button:first-child .main-input-container {
      margin-left: 0px !important; }

.list-with-btn-scroll.explore {
    padding-bottom: 9px; }

.list-with-btn-scroll.explore.multipleScroll {
      overflow: scroll;
      padding-bottom: 0px; }

.list-with-btn-scroll .remove-scroll-line-container {
    height: 43px;
    overflow: hidden; }

.list-with-btn-scroll .remove-scroll-line-container .remove-scroll-line {
      height: 60px; }

.simple-list .list-container {
  display: -webkit-box;
  display: flex;
  -webkit-box-align: center;
          align-items: center;
  min-height: 60px;
  border-bottom: 1px solid #f1f3f5; }

.simple-list .list-container.no-border {
    border-bottom: 0; }

.simple-list .list-container.height-76 {
    padding-top: 5px;
    padding-bottom: 10px;
    border-bottom: 1px solid #f1f3f5; }

.simple-list .list-container.height-76 .subLbale {
      margin-top: 4px; }

.simple-list .list-container.height-75 {
    height: 75px; }

.simple-list .list-items-left {
  display: -webkit-box;
  display: flex;
  flex-shrink: 0;
  -o-object-fit: cover;
     object-fit: cover; }

.simple-list .list-items-left img {
    width: 22px;
    height: 22px; }

.simple-list .list-items-left .small-image {
    width: 16px; }

.simple-list .list-items-right {
  height: 60px;
  display: -webkit-box;
  display: flex;
  -webkit-box-align: start;
          align-items: flex-start;
  -webkit-box-pack: center;
          justify-content: center;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
          flex-direction: column;
  width: 100%; }

.simple-list .list-items-right.buttom-border {
    border-bottom: 1px solid #f1f3f5; }

.simple-list .right-icon {
  width: 21px;
  height: 21px;
  margin-left: auto;
  flex-shrink: 0;
  -webkit-transform: rotate(0);
          transform: rotate(0);
  -webkit-transition-duration: 0.2s;
          transition-duration: 0.2s; }

.simple-list .right-icon img {
    width: 100%;
    height: 100%; }

.simple-list .right-icon.rotate {
    -webkit-transform: rotate(180deg);
            transform: rotate(180deg);
    -webkit-transition-duration: 0.2s;
            transition-duration: 0.2s; }

.simple-list .right-icon.hasNotifications {
    height: 17px;
    width: 17px;
    background-color: #FE4A49;
    border-radius: 50%;
    display: -webkit-box;
    display: flex;
    -webkit-box-align: center;
            align-items: center;
    -webkit-box-pack: center;
            justify-content: center;
    margin-top: 1px; }

.simple-list .right-icon.hasCounter {
    display: -webkit-box;
    display: flex;
    background-color: #F5F8FF;
    -webkit-box-align: center;
            align-items: center;
    width: 56px;
    height: 40px;
    -webkit-box-pack: center;
            justify-content: center;
    border: 1px solid #ECEFF1;
    border-radius: 6px; }

.simple-list .right-icon.hasCounter img {
      width: 16px;
      height: 16px; }

.simple-list .right-icon.hasCounter .counter {
      margin-left: 4px;
      color: #707070;
      font-family: &quot;Avenir Next&quot;;
      font-size: 16px;
      font-weight: 600;
      letter-spacing: -1px;
      line-height: 22px;
      text-align: center; }

.ios .right-icon.hasNotifications {
  padding-left: 1px; }

.mail-box .list-items-left {
  width: 32px;
  height: 32px;
  -o-object-fit: cover;
     object-fit: cover;
  flex-shrink: 0; }

.mail-box .list-items-left img {
    width: 32px;
    height: 32px;
    border-radius: 6px;
    border: 1px solid #f1f3f5; }

.list-special-container {
  width: 100%;
  padding: 0 20px;
  border-radius: 6px; }

.list-special-container .list-special-item {
    padding: 20px 0;
    display: -webkit-box;
    display: flex;
    -webkit-box-align: center;
            align-items: center;
    border-bottom: 1px solid #f1f3f5; }

.list-special-container .list-special-item .left {
      width: 100%; }

.list-special-container .list-special-item .left .second-item {
        width: 96%;
        margin-top: 4px;
        overflow: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;
        -webkit-line-clamp: 1;
        /* autoprefixer: ignore next */
        -webkit-box-orient: vertical;
        padding-left: 13px;
        display: flex;
        -webkit-box-align: center;
                align-items: center; }

.list-special-container .list-special-item .left .second-item .txt {
          margin-left: 5px; }

.list-special-container .list-special-item .left .second-item img {
          width: 16px;
          height: 16px;
          -o-object-fit: cover;
             object-fit: cover;
          border: 1px solid #ffffff;
          border-radius: 4px;
          margin-left: -10px; }

.list-special-container .list-special-item .right {
      display: -webkit-box;
      display: flex;
      margin-left: auto;
      flex-shrink: 0; }

.list-special-container .list-special-item .right img {
        width: 24px;
        height: 24px;
        -o-object-fit: cover;
           object-fit: cover;
        border: 1px solid #ffffff;
        border-radius: 4px; }

.hide-scroll-container {
  height: 184px;
  overflow: hidden; }

.hide-scroll-container .hide-scroll {
    display: -webkit-box;
    display: flex;
    height: 190px;
    overflow: scroll; }

.treeSpace {
  padding-left: 20px; }

.filter-count {
  display: -webkit-box;
  display: flex;
  min-width: 42px;
  -webkit-box-align: center;
          align-items: center;
  padding-right: 8px;
  margin-right: 10px;
  height: 20px;
  border-right: 1px solid #d5d9db;
  color: #00d2bd;
  font-size: 13px; }

.filter-count img {
    width: 16px;
    margin-right: 8px; }

.filter-help {
  width: 45px;
  flex-shrink: 0;
  text-align: center;
  margin-left: 5px;
  -webkit-box-align: center;
          align-items: center;
  display: -webkit-box;
  display: flex;
  height: 35px; }

.list-box {
  display: -webkit-box;
  display: flex;
  overflow: scroll;
  padding: 0 20px;
  margin-bottom: 10px; }

.list-box .card-box-container {
    height: 211px;
    width: 152px;
    border: 1px solid #f1f3f5;
    border-radius: 10px;
    margin-right: 10px; }

.list-box .card-box-container .card-box-item .first-item {
      height: 132px;
      border-radius: 8px 8px 0 0;
      background-size: cover;
      background-position: center;
      background-repeat: no-repeat; }

.list-box .card-box-container .card-box-item .first-item img {
        width: 100%;
        height: 100%;
        border-radius: 10px 10px 0 0;
        -o-object-fit: cover;
           object-fit: cover; }

.list-box .card-box-container .card-box-item .second-item {
      border-top: 1px solid #f1f3f5;
      padding: 10px 13px 13px 13px;
      height: 78px; }

.list-box .card-box-container .card-box-item .second-item span {
        overflow: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;
        -webkit-line-clamp: 2;
        /* autoprefixer: ignore next */
        -webkit-box-orient: vertical;
        text-align: left; }

.list-box .card-box-container .card-box-item .second-item .location {
        overflow: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;
        -webkit-line-clamp: 1;
        /* autoprefixer: ignore next */
        -webkit-box-orient: vertical;
        text-align: left; }

.list-box .card-box-container .subtitle {
      margin-top: 6px;
      display: -webkit-box;
      display: flex;
      -webkit-box-align: center;
              align-items: center; }

.list-box .card-box-container .subtitle img {
        margin-right: 6px;
        height: 12px;
        width: 12px; }

.list-box app-product-card:last-child .card-box-container {
    margin-right: 20px; }

.list-box-wrap {
  text-align: left;
  display: -webkit-box;
  display: flex;
  flex-wrap: wrap;
  border-top: 1px solid #f1f3f5; }

.list-box-wrap app-product-card {
    width: 50%;
    flex-shrink: 0; }

.list-box-wrap app-product-card:nth-child(odd) .card-box-container {
      border: 1px solid #f1f3f5;
      border-top: 0;
      border-left: 0; }

.list-box-wrap app-product-card:nth-child(even) {
      border-bottom: 1px solid #f1f3f5;
      border-left: 1px solid transparent; }

.list-box-wrap .first-item {
    height: 160px;
    padding-top: 100%;
    background-size: cover;
    background-position: center; }

.list-box-wrap .second-item {
    padding: 10px 21px;
    height: 107px; }

.list-box-wrap .second-item span {
      overflow: hidden;
      text-overflow: ellipsis;
      display: -webkit-box;
      -webkit-line-clamp: 3;
      /* autoprefixer: ignore next */
      -webkit-box-orient: vertical; }

.list-box-wrap .second-item .subtitle {
      margin-top: 6px;
      display: -webkit-box;
      display: flex;
      -webkit-box-align: center;
              align-items: center; }

.list-box-wrap .second-item .subtitle img {
        margin-right: 6px;
        height: 12px;
        width: 12px; }

.list-box-wrap .second-item .subtitle .location {
        overflow: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;
        -webkit-line-clamp: 1;
        /* autoprefixer: ignore next */
        -webkit-box-orient: vertical; }

.complexType {
  padding-top: 20px;
  display: -webkit-box;
  display: flex; }

.complexType .left-item {
    height: 54px;
    width: 54px;
    border-radius: 12px;
    flex-shrink: 0; }

.complexType .left-item img {
      height: 54px;
      width: 54px;
      border-radius: 12px;
      -o-object-fit: cover;
         object-fit: cover; }

.complexType .container {
    padding-bottom: 20px;
    border-bottom: 1px solid #F0F5FF;
    width: 100%;
    display: -webkit-box;
    display: flex;
    -webkit-box-align: center;
            align-items: center; }

.complexType .container .middle-item {
      margin-left: 10px;
      width: 100%;
      padding-right: 8px; }

.complexType .container .middle-item .first-item {
        display: -webkit-box;
        display: flex;
        -webkit-box-align: center;
                align-items: center; }

.complexType .container .middle-item .first-item .details {
          overflow: hidden;
          text-overflow: ellipsis;
          display: -webkit-box;
          -webkit-line-clamp: 1;
          /* autoprefixer: ignore next */
          -webkit-box-orient: vertical;
          padding-right: 10px; }

.complexType .container .middle-item .first-item .subDetails {
          margin-left: auto;
          flex-shrink: 0; }

.complexType .container .middle-item .second-item {
        margin-top: 2px;
        display: -webkit-box;
        display: flex;
        -webkit-box-align: center;
                align-items: center; }

.complexType .container .middle-item .second-item .details {
          overflow: hidden;
          text-overflow: ellipsis;
          display: -webkit-box;
          -webkit-line-clamp: 1;
          /* autoprefixer: ignore next */
          -webkit-box-orient: vertical;
          padding-right: 10px; }

.complexType .container .middle-item .second-item .badge {
          height: 18px;
          width: 18px;
          background-color: #FE4A49;
          display: -webkit-box;
          display: flex;
          -webkit-box-pack: center;
                  justify-content: center;
          -webkit-box-align: center;
                  align-items: center;
          border-radius: 50%;
          flex-shrink: 0;
          margin-left: auto;
          font-size: 11px !important; }

.complexType .container .right-item {
      width: 21px;
      height: 21px;
      flex-shrink: 0;
      margin-left: auto; }

.complexType .container .right-item img {
        width: 100%;
        height: 100%; }

.complexType.small {
    border-bottom: 1px solid #F0F5FF;
    -webkit-box-align: center;
            align-items: center;
    padding: 20px 0; }

.complexType.small .left-item {
      width: 32px;
      height: 32px;
      border-radius: 0px; }

.complexType.small .left-item img {
        width: 32px;
        height: 32px;
        border-radius: 0px;
        -o-object-fit: none;
           object-fit: none; }

.complexType.small .container {
      border: 0;
      padding-bottom: 0; }

.center-middle-item .container .middle-item {
  margin-top: 5px; }

.center-middle-item .right-item {
  margin-top: 10px; }

.complexTreeType {
  position: relative; }

.complexTreeType .simple-list .list-container {
    padding: 10px 16px;
    border: 1px solid #f1f3f5;
    position: relative;
    margin-bottom: 40px;
    border-radius: 6px; }

.complexTreeType .simple-list .list-container::after {
      content: &quot; &quot;;
      border-right: 2px dashed #D5E0EA;
      margin-bottom: 40px;
      position: absolute;
      top: 100%;
      left: 36px;
      height: 40px;
      -webkit-animation-name: faded;
              animation-name: faded;
      -webkit-animation-duration: 2s;
              animation-duration: 2s; }

.complexTreeType .simple-list .list-container.active {
      background-color: #ECEFF1; }

.complexTreeType .simple-list .list-container .list-items-right {
      height: auto; }

.complexTreeType .simple-list .list-container .list-items-left img {
      width: 41px;
      -o-object-fit: cover;
         object-fit: cover;
      height: 41px;
      border-radius: 12px;
      border: 1px solid #f1f3f5;
      object-fit: cover; }

.complexTreeType .simple-list .list-container .location {
      display: -webkit-box;
      display: flex;
      -webkit-box-align: center;
              align-items: center; }

.complexTreeType .simple-list .list-container .location img {
        width: 20px;
        margin-right: 10px; }

.complexTreeType .simple-list .list-container.no-border::after {
      border-right: 0 !important; }

.complexTreeType .treeCounterContainer {
    position: relative; }

.complexTreeType .treeCounterContainer .treeCountNumber {
      position: absolute;
      z-index: 10;
      font-size: 11px;
      border-radius: 50%;
      height: 16px;
      width: 16px;
      display: -webkit-box;
      display: flex;
      -webkit-box-align: center;
              align-items: center;
      -webkit-box-pack: center;
              justify-content: center;
      color: #ffffff; }

@-webkit-keyframes faded {
  0% {
    height: 0px; }
  100% {
    height: 40px; } }

@keyframes faded {
  0% {
    height: 0px; }
  100% {
    height: 40px; } }

.ios .second-item .badge {
  padding-left: 1px;
  padding-top: 2px; }

.ios .complexType .second-item .badge {
  padding-left: 1px;
  padding-top: 0px; }

.card-type-five {
  border: 1px solid #e7e7f0;
  border-radius: 8px; }

.card-type-five .list {
    background-color: #F5F8FF;
    padding: 16px;
    border-radius: 8px 8px 0 0;
    border-bottom: 1px solid #e7e7f0; }

.card-type-five .list .first {
      display: -webkit-box;
      display: flex;
      -webkit-box-align: center;
              align-items: center; }

.card-type-five .list .first img {
        height: 48px;
        width: 48px;
        border-radius: 12px; }

.card-type-five .list .first span {
        margin-left: 12px;
        text-align: left; }

.card-type-five .action {
    padding: 16px 0;
    display: -webkit-box;
    display: flex; }

.card-type-five .action .action-item {
      display: -webkit-box;
      display: flex;
      -webkit-box-orient: vertical;
      -webkit-box-direction: normal;
              flex-direction: column;
      -webkit-box-align: center;
              align-items: center;
      width: 33.33%; }

.card-type-five .action .action-item img {
        height: 20px;
        width: 20px; }

.card-type-five .action .action-item span {
        margin-top: 7px; }

.active-item-tree .list-container {
  background-color: #F5F8FF; }

.event .date-container {
  margin-top: 20px; }

.event .date-container .text {
    margin-left: 10px; }

.event .date-container .v-line-container {
    height: 24px;
    width: 20px;
    padding: 6px 0;
    display: -webkit-box;
    display: flex;
    -webkit-box-pack: center;
            justify-content: center; }

.event .date-container .v-line-container .v-line {
      height: 100%;
      width: 1px;
      background: #f1f3f5; }

.event .location-container {
  border: 1px solid #f1f3f5;
  padding: 20px 0;
  border-right: 0;
  border-left: 0;
  border-bottom: 0;
  margin-top: 20px; }

.event .location-container .text {
    margin-left: 10px; }

.navBar {
  width: 100%;
  height: 58px; }

.navBar .navBar-container {
    width: 100%;
    height: 100%;
    display: -webkit-box;
    display: flex;
    -webkit-box-align: center;
            align-items: center;
    border-bottom: 1px solid #ECEFF1;
    background-color: #ffffff; }

.navBar .navBar-container.black {
      background-color: #000000;
      border: 0; }

.navBar .navBar-container.black .middle {
        color: #ffffff; }

.navBar .navBar-container.black .middle .pages-top-bar-title {
          color: #ffffff !important; }

.navBar .navBar-container.no-border {
      border: 0; }

.navBar .navBar-container img {
      width: 24px;
      height: 24px;
      margin-left: 1px; }

.navBar .navBar-container .middle-icon {
      width: 16px;
      flex-shrink: 0;
      margin-top: 1px; }

.navBar .navBar-container .hasCountMessage {
      height: 7px;
      width: 7px;
      margin-left: 2px;
      background: #FE4A49;
      border-radius: 50%;
      flex-shrink: 0;
      margin-left: 3px;
      margin-top: 2px; }

.navBar .navBar-container .item {
      width: 56px;
      height: 100%;
      flex-shrink: 0;
      display: -webkit-box;
      display: flex;
      -webkit-box-align: center;
              align-items: center;
      -webkit-box-pack: center;
              justify-content: center; }

.navBar .navBar-container .item.right-txt {
        -webkit-box-pack: end;
                justify-content: end; }

.navBar .navBar-container .middle {
      width: 100%;
      text-align: center;
      -webkit-box-align: center;
              align-items: center;
      display: -webkit-box;
      display: flex;
      -webkit-box-pack: center;
              justify-content: center;
      max-width: calc(100% - 112px); }

.navBar .navBar-container .middle.column {
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
                flex-direction: column; }

.navBar .navBar-container .middle .txt {
        display: inline-block;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis; }

.navBar .navBar-container.fullNavBar .middle {
      max-width: calc(100% - 10px); }

.navBar .complex-type {
    height: 100%; }

.navBar .complex-type .middle {
      -webkit-box-orient: horizontal !important;
      -webkit-box-direction: normal !important;
              flex-direction: row !important;
      -webkit-box-pack: start;
              justify-content: flex-start;
      -webkit-box-align: center;
              align-items: center;
      text-align: left;
      padding-right: 11px; }

.navBar .complex-type .middle .text {
        overflow: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;
        -webkit-line-clamp: 1;
        /* autoprefixer: ignore next */
        -webkit-box-orient: vertical; }

.navBar .complex-type .image-bar {
      height: 32px;
      width: 32px;
      border-radius: 6px;
      margin-right: 10px;
      flex-shrink: 0; }

.navBar .complex-type .image-bar img {
        width: 100%;
        height: 100%;
        border-radius: 6px;
        border: 1px solid #F0F5FF;
        -o-object-fit: cover;
           object-fit: cover; }

.navBar .complex-type .middle-bar-details {
      width: 100%; }

.navBar .complex-type .middle-bar-details .middle-bar-details-text {
        width: 100%; }

.navBar .btn {
    margin-right: 10px;
    margin-left: auto; }

.input-navbar {
  position: relative;
  padding: 0 20px;
  padding-bottom: 20px;
  padding-top: 20px;
  background-color: #ffffff; }

.input-navbar.no-top-space {
    margin-top: 0;
    padding-top: 0 !important; }

.input-navbar.without-seperator {
    display: -webkit-box;
    display: flex;
    -webkit-box-align: center;
            align-items: center; }

.input-navbar.without-seperator .input {
      width: 100%;
      border: 0;
      background-size: 16px;
      background-repeat: no-repeat;
      background-position: 16px;
      border-radius: 18px;
      background-color: #F0F5FF;
      height: 34px; }

.input-navbar.without-seperator .input.search {
        background-image: url(&quot;/assets/icons/diasporaIcon/Search_Grey.svg&quot;);
        background-size: 20px;
        padding-left: 50px; }

.input-navbar.without-seperator .input.search::-webkit-input-placeholder {
          font-size: 14px;
          color: #B1BCC6; }

.input-navbar.without-seperator .input.search::-moz-placeholder {
          font-size: 14px;
          color: #B1BCC6; }

.input-navbar.without-seperator .input.search::-ms-input-placeholder {
          font-size: 14px;
          color: #B1BCC6; }

.input-navbar.without-seperator .input.search::placeholder {
          font-size: 14px;
          color: #B1BCC6; }

.input-navbar.without-seperator app-input {
      width: 100%; }

.input-navbar.without-seperator .inputWithIcon {
      width: 100%; }

.input-navbar.without-seperator.with-icon .icon-container {
      margin-right: 16px;
      flex-shrink: 0; }

.input-navbar.without-seperator.with-icon .icon-container img {
        width: 24px; }

.input-navbar.without-seperator.with-icon .icon-container.right {
        margin-left: 18px;
        margin-right: 0;
        flex-shrink: 0;
        height: 24px;
        margin-top: 2px; }

.input-navbar.without-seperator.with-icon .icon-container.left {
        margin-left: 20px;
        margin-right: 0;
        margin-top: 2px;
        top: 2px; }

.input-navbar .input {
    height: 42px;
    width: 100%;
    padding: 0 16px;
    padding-left: 38px;
    border: 1px solid rgba(214, 214, 233, 0.46);
    border-radius: 8px;
    font-size: 13px;
    background-color: #D5E0EA;
    letter-spacing: 0.2px;
    font-weight: 500;
    background-size: 16px;
    background-repeat: no-repeat;
    background-position: 16px; }

.input-navbar .input.search {
      background-image: url(&quot;/assets/icons/diaspora/Search_Grey.svg&quot;); }

.input-navbar ::-webkit-input-placeholder {
    color: #707070;
    font-size: 13px;
    letter-spacing: 0.2px;
    font-weight: 300;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 1;
    /* autoprefixer: ignore next */
    -webkit-box-orient: vertical; }

.input-navbar ::-moz-placeholder {
    color: #707070;
    font-size: 13px;
    letter-spacing: 0.2px;
    font-weight: 300;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 1;
    /* autoprefixer: ignore next */
    -webkit-box-orient: vertical; }

.input-navbar ::-ms-input-placeholder {
    color: #707070;
    font-size: 13px;
    letter-spacing: 0.2px;
    font-weight: 300;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 1;
    /* autoprefixer: ignore next */
    -webkit-box-orient: vertical; }

.input-navbar ::placeholder {
    color: #707070;
    font-size: 13px;
    letter-spacing: 0.2px;
    font-weight: 300;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 1;
    /* autoprefixer: ignore next */
    -webkit-box-orient: vertical; }

input[disabled] {
  background: #F5F8FF;
  background-position: 8px 8px;
  background-repeat: no-repeat;
  background-size: 16px;
  opacity: 1 !important; }

.ios .input {
  word-break: break-all !important; }

.ios .input-navbar .input {
  padding-bottom: 2px; }

input, textarea {
  caret-color: #00d2bd; }

app-list-card-with-date:last-child .card-container {
  margin-right: 20px; }

.card-container-with-border {
  position: relative;
  border-top: 28px solid #f0f0f0;
  padding-top: 10px;
  border-bottom: 28px solid #f0f0f0;
  background: #ffffff; }

.card-container-with-border::after {
    content: &quot; &quot;;
    position: absolute;
    height: 17px;
    width: 100%;
    background: #ffffff;
    border-radius: 0 0 13px 13px;
    bottom: -16px;
    box-shadow: 0 3px 3px 0 rgba(0, 0, 0, 0.05);
    left: 0; }

.card-container-with-border::before {
    content: &quot; &quot;;
    position: absolute;
    height: 17px;
    width: 100%;
    background: white;
    border-radius: 13px 13px 0 0;
    top: -16px;
    left: 0; }

.card-container-with-border.no-border-top-radius {
    border-top: 0;
    padding-top: 0; }

.card-container-with-border.no-border-top-radius::before {
      content: none; }

.card-container-with-border .vertical-line {
    height: 17px;
    width: 1px;
    position: absolute;
    background: #f1f3f5;
    left: 50%;
    z-index: 10;
    margin-top: -1px; }

.card-profile-container {
  width: 100%;
  padding: 16px 0;
  margin-top: 100px;
  border: 1px solid #D8EFF6;
  background-color: #FAFCFE;
  border-radius: 4px;
  display: -webkit-box;
  display: flex;
  -webkit-box-pack: center;
          justify-content: center; }

.card-profile-container .card-profile-item {
    display: -webkit-box;
    display: flex;
    max-width: 179px;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
            flex-direction: column;
    -webkit-box-align: center;
            align-items: center;
    padding: 0 16px;
    border-right: 1px solid #D8EFF6;
    box-shadow: 4px 4px 4px -8px rgba(69, 139, 219, 0.19); }

.card-profile-container .card-profile-item img {
      height: 50px;
      width: 50px;
      border-radius: 50%; }

.card-profile-container .card-profile-item .first-txt {
      margin-top: 12px;
      text-align: center;
      overflow: hidden;
      text-overflow: ellipsis;
      display: -webkit-box;
      -webkit-line-clamp: 1;
      /* autoprefixer: ignore next */
      -webkit-box-orient: vertical; }

.card-profile-container .card-profile-item .second-txt {
      text-align: center;
      margin-top: 4px;
      overflow: hidden;
      text-overflow: ellipsis;
      display: -webkit-box;
      -webkit-line-clamp: 2;
      /* autoprefixer: ignore next */
      -webkit-box-orient: vertical; }

.card-profile-container .card-profile-item .third-txt {
      text-align: center;
      margin-top: 14px; }

.card-profile-container .line-middle {
    width: 2px;
    height: 100%;
    background-color: #D8EFF6; }

.card-one {
  height: 223px;
  width: 100%;
  border-radius: 6px;
  overflow: hidden; }

.card-one .card-container {
    border-radius: 6px;
    width: 272px;
    margin-right: 16px;
    height: 255px; }

.card-one .card-container .top {
      width: 272px;
      height: 137px;
      border: 1px solid #EFEFF3;
      border-bottom: 0;
      border-top-right-radius: 6px;
      border-top-left-radius: 6px;
      background: #F0F5FF; }

.card-one .card-container .top img {
        width: 100%;
        height: 100%;
        border-top-left-radius: 6px;
        border-top-right-radius: 6px;
        -o-object-fit: cover;
           object-fit: cover; }

.card-one .card-container .bottom-container {
      min-height: 77px;
      display: -webkit-box;
      display: flex;
      -webkit-box-align: center;
              align-items: center;
      border: 1px solid #EFEFF3;
      border-top: 0;
      border-bottom-right-radius: 6px;
      border-bottom-left-radius: 6px;
      box-shadow: 0 0 2px 0 rgba(96, 96, 96, 0.01);
      padding: 11px 0; }

.card-one .card-container .bottom-container .bottom {
        padding: 0 11px;
        display: -webkit-box;
        display: flex; }

.card-one .card-container .bottom-container .bottom .left {
          display: -webkit-box;
          display: flex;
          -webkit-box-align: center;
                  align-items: center;
          -webkit-box-orient: vertical;
          -webkit-box-direction: normal;
                  flex-direction: column;
          flex-shrink: 0;
          font-size: 15px;
          margin-right: 10px;
          width: 43px; }

.card-one .card-container .bottom-container .bottom .left .pages-events-date {
            text-transform: uppercase; }

.card-one .card-container .bottom-container .bottom .left .green {
            color: #00d2bd; }

.card-one .card-container .bottom-container .bottom .middle {
          text-align: left; }

.card-one .card-container .bottom-container .bottom .middle .title {
            overflow: hidden;
            text-overflow: ellipsis;
            display: -webkit-box;
            -webkit-line-clamp: 1;
            /* autoprefixer: ignore next */
            -webkit-box-orient: vertical; }

.card-one .card-container .bottom-container .bottom .middle .subtitle {
            margin-top: 5px;
            text-align: left; }

.card-two {
  margin-top: 20px;
  padding-bottom: 20px;
  border-bottom: 1px solid #f1f3f5; }

.card-two .card-subtitle {
    margin-left: 8px;
    display: -webkit-box;
    display: flex;
    -webkit-box-align: center;
            align-items: center; }

.card-two .card-image {
    width: 100%;
    padding-top: 100%;
    border-radius: 21px;
    background-size: cover;
    background-repeat: no-repeat;
    border: 1px solid #f1f3f5;
    max-height: 500px;
    position: relative; }

.card-two .card-image .card-body-details {
      background: white;
      padding: 12px 16px;
      border-radius: 19px;
      box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.07);
      position: absolute;
      width: 100%;
      bottom: 0; }

.card-two .card-image.small-image-aspect-ratio {
      padding-top: 56.25%; }

.card-two .start-item {
    margin-top: 13px;
    display: -webkit-box;
    display: flex;
    overflow: hidden;
    text-overflow: ellipsis;
    -webkit-line-clamp: 3;
    /* autoprefixer: ignore next */
    -webkit-box-orient: vertical; }

.card-two .middle-item {
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    /* autoprefixer: ignore next */
    -webkit-box-orient: vertical;
    margin-top: 6px; }

.card-two .end-item {
    margin-top: 6px; }

.card-two .image-item {
    height: 20px;
    width: 20px;
    border-radius: 4px;
    -o-object-fit: cover;
       object-fit: cover; }

.seperate-bullet {
  margin-left: 6px;
  margin-right: 6px;
  font-size: 8px; }

.card-four {
  padding-bottom: 20px;
  border-bottom: 1px solid #f1f3f5; }

.card-four.no-border {
    border: 0; }

.card-four .with-shadow {
    box-shadow: 0 5px 16px 0 rgba(229, 236, 245, 0.35);
    padding-bottom: 10px;
    border-radius: 0 0 13px 13px; }

.card-four .card-container {
    background-color: #ffffff;
    display: -webkit-box;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
            flex-direction: column;
    -webkit-box-align: center;
            align-items: center;
    padding-top: 19px; }

.card-four .card-container .card-header {
      display: -webkit-box;
      display: flex;
      width: 100%;
      padding: 0 20px;
      -webkit-box-pack: center;
              justify-content: center; }

.card-four .card-container .card-header.size-100 {
        height: 100px; }

.card-four .card-container .card-header.size-100 img {
          width: 100px;
          height: 100px; }

.card-four .card-container .card-header .right-icon {
        margin-left: auto;
        height: 24px; }

.card-four .card-container .card-header .right-icon img {
          width: 24px; }

.card-four .card-container .card-header .right-icon::before {
          content: &quot;&quot;;
          height: 24px;
          width: 24px;
          opacity: 0;
          background: rgba(223, 223, 223, 0.5);
          border-radius: 50%;
          position: absolute; }

.card-four .card-container .card-header .right-icon:active::before {
          -webkit-transform: scale(2);
                  transform: scale(2);
          -webkit-transition: all 0.1s;
          transition: all 0.1s;
          opacity: 1; }

.card-four .card-container .card-header .left-icon {
        margin-right: auto;
        height: 24px; }

.card-four .card-container .card-header .left-icon img {
          width: 24px; }

.card-four .card-container .card-header .left-icon::before {
          content: &quot;&quot;;
          height: 24px;
          width: 24px;
          opacity: 0;
          background: rgba(223, 223, 223, 0.2);
          border-radius: 50%;
          position: absolute; }

.card-four .card-container .card-header .left-icon:active::before {
          -webkit-transform: scale(2);
                  transform: scale(2);
          opacity: 1; }

.card-four .card-container .profile-image img {
      height: 100px;
      width: 100px;
      border-radius: 18px;
      -o-object-fit: cover;
         object-fit: cover;
      border: 1px solid #f1f3f5; }

.card-four .card-container .subtitle {
      margin-top: 6px;
      text-align: center;
      display: -webkit-box;
      display: flex;
      -webkit-box-align: center;
              align-items: center;
      padding: 0 20px; }

.card-four .card-container .subtitle.action {
        margin-top: 10px; }

.card-four .card-container .subtitle img {
        width: 12px;
        margin-right: 6px; }

.card-four .card-container.with-background {
      background-position: center;
      background-repeat: no-repeat;
      background-size: cover;
      padding-top: 56.25%;
      position: relative; }

.card-four .card-container.with-background .card-header {
        position: absolute;
        top: 20px;
        padding: 0 20px; }

.card-four .card-container.with-background .profile-image {
        position: absolute;
        top: 50%;
        left: 50%;
        -webkit-transform: translate(-50%, -50%);
                transform: translate(-50%, -50%); }

.card-four .card-container.with-background .view-more {
        height: 32px;
        width: 118px;
        border-radius: 6px;
        background-color: rgba(0, 0, 0, 0.6);
        position: absolute;
        display: -webkit-box;
        display: flex;
        -webkit-box-align: center;
                align-items: center;
        -webkit-box-pack: center;
                justify-content: center;
        color: #ECEFF1;
        font-size: 13px;
        font-weight: 500;
        letter-spacing: 0.2px;
        line-height: 18px;
        right: 16px;
        bottom: 16px; }

.card-four .card-container.with-background .view-more img {
          width: 12px;
          margin-right: 6px; }

.card-four .title {
    margin-top: 12px;
    text-align: center;
    padding: 0 20px;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    /* autoprefixer: ignore next */
    -webkit-box-orient: vertical;
    width: 100%; }

.card-four.media .card-container {
    padding: 0; }

.card-four.media .card-container .card-header {
      padding: 0;
      height: auto; }

.card-four.media .card-container .card-header .profile-image {
        position: static;
        -webkit-transform: translate(0);
                transform: translate(0);
        width: 100%;
        padding-top: 56.25%;
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center; }

.card-four.media .card-container .card-header .profile-image img {
          width: 100%;
          height: 100%;
          border-radius: 0; }

.card-four .desc {
    margin-top: 20px;
    display: -webkit-box;
    display: flex;
    -webkit-box-pack: center;
            justify-content: center;
    -webkit-box-align: start;
            align-items: flex-start;
    border-radius: 0 0 13px 13px; }

.card-four .desc.space-between-10 {
      padding: 0 10px; }

.card-four .desc.space-between-30 {
      padding: 0 30px; }

.card-four .desc.space-between-65 {
      padding: 0 65px; }

.card-four .desc .desc-container {
      display: -webkit-box;
      display: flex;
      -webkit-box-orient: vertical;
      -webkit-box-direction: normal;
              flex-direction: column;
      -webkit-box-pack: center;
              justify-content: center;
      -webkit-box-align: center;
              align-items: center;
      width: 82.5px;
      flex-shrink: 0;
      padding: 0 8px; }

.card-four .desc .desc-container.width-120 {
        width: 120px; }

.card-four .desc .desc-container.width-100 {
        width: 100px; }

.card-four .desc .desc-container.width-81 {
        width: 81px; }

.card-four .desc .desc-container:first-child {
        margin-left: 0; }

.card-four .desc .desc-container:last-child {
        margin-right: 0; }

.card-four .desc .desc-container .desc-first {
        height: 32px;
        width: 32px;
        color: #ffffff;
        font-size: 12px;
        font-weight: 600;
        display: -webkit-box;
        display: flex;
        -webkit-box-align: center;
                align-items: center;
        -webkit-box-pack: center;
                justify-content: center;
        border-radius: 7px;
        border: 1px solid #f1f3f5; }

.card-four .desc .desc-container .desc-first.green-gradient {
          background-color: #00d2bd; }

.card-four .desc .desc-container .desc-first img {
          width: 100%;
          height: 100%;
          border-radius: 7px;
          -o-object-fit: cover;
             object-fit: cover; }

.card-four .desc .desc-container .desc-first.country-flag {
          width: 32px;
          height: 32xp; }

.card-four .desc .desc-container .desc-first.country-flag img {
            width: 100%;
            height: 100%; }

.card-four .desc .desc-container span {
        margin-top: 8px;
        overflow: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;
        -webkit-line-clamp: 2;
        /* autoprefixer: ignore next */
        -webkit-box-orient: vertical;
        text-align: center; }

.card-with-desc .card-desc-container {
  width: 100%;
  padding: 0 16px;
  margin-top: 100px; }

.card-with-desc .card-desc-container .card-desc-img {
    padding-top: 56.25%;
    border: 1px solid #ECEFF1;
    border-radius: 7px;
    position: relative; }

.card-with-desc .card-desc-container .card-desc-img img {
      position: absolute;
      top: 50%;
      left: 50%;
      -webkit-transform: translate(-50%, -50%);
              transform: translate(-50%, -50%);
      width: 100%;
      border-radius: 7px;
      -o-object-fit: contain;
         object-fit: contain; }

.card-with-desc .card-desc-container .list-with-image {
    display: -webkit-box;
    display: flex;
    margin-top: 10px;
    -webkit-box-align: center;
            align-items: center; }

.card-with-desc .card-desc-container .list-with-image img {
      width: 20px;
      height: 20px;
      border-radius: 7px;
      margin-right: 8px; }

.card-with-desc .card-desc-container .bold-title {
    margin-top: 10px; }

.card-with-desc .card-desc-container .title {
    margin-top: 20px; }

.card-with-desc .card-desc-container .bullet-lis {
    margin-top: 20px; }

.card-box {
  display: -webkit-box;
  display: flex;
  width: 100%;
  overflow: scroll;
  padding-bottom: 20px;
  padding-left: 20px; }

.card-box .card-box-container {
    margin-right: 10px; }

.card-box .card-box-container .card-box-item {
      height: 209px;
      width: 152px;
      border: 1px solid #ECEFF1;
      border-radius: 8px;
      background-color: #ffffff; }

.card-box .card-box-container .card-box-item .first-item {
        height: 132px;
        border-bottom: 1px solid #ECEFF1;
        display: -webkit-box;
        display: flex;
        -webkit-box-pack: center;
                justify-content: center;
        -webkit-box-align: center;
                align-items: center;
        border-radius: 8px 8px 0 0; }

.card-box .card-box-container .card-box-item .first-item img {
          height: 100%;
          -o-object-fit: cover;
             object-fit: cover;
          border-radius: 7px 7px 0 0; }

.card-box .card-box-container .card-box-item .second-item {
        padding: 13px;
        font-size: 13px;
        height: 77px; }

.card-box .card-box-container .card-box-item .second-item span {
          overflow: hidden;
          text-overflow: ellipsis;
          display: -webkit-box;
          -webkit-line-clamp: 2;
          /* autoprefixer: ignore next */
          -webkit-box-orient: vertical; }

.card-box .card-box-container .card-box-item .second-item div {
          overflow: hidden;
          text-overflow: ellipsis;
          display: -webkit-box;
          -webkit-line-clamp: 1;
          /* autoprefixer: ignore next */
          -webkit-box-orient: vertical; }

.card-general-info .product-image {
  border-radius: 12px;
  height: 100%;
  background-position: center;
  background-size: cover;
  background-color: #f1f3f5;
  border: 1px solid #f1f3f5;
  background-repeat: no-repeat;
  padding-top: 100%;
  background-repeat: no-repeat;
  max-height: 500px; }

.card-general-info .actor {
  margin-top: 16px;
  display: -webkit-box;
  display: flex;
  -webkit-box-align: center;
          align-items: center; }

.card-general-info .actor .actor-image {
    height: 20px;
    flex-shrink: 0;
    background-color: #F0F5FF; }

.card-general-info .actor img {
    width: 20px;
    height: 20px;
    border-radius: 4px;
    -o-object-fit: cover;
       object-fit: cover;
    border: 1px solid #F0F5FF; }

.card-general-info .actor .actor-name {
    margin-left: 8px;
    width: 100%; }

.card-general-info .title {
  margin-top: 9px;
  margin-bottom: 2px; }

.card-general-info .description {
  margin-top: 20px; }

.scroll-country-container {
  display: -webkit-box;
  display: flex;
  overflow: scroll;
  padding: 0 20px; }

.scroll-country-container .scroll-country-item {
    max-width: 90px;
    margin-right: 40px;
    display: -webkit-box;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
            flex-direction: column;
    -webkit-box-align: center;
            align-items: center;
    flex-shrink: 0;
    text-align: center; }

.scroll-country-container .scroll-country-item:last-child {
      margin-right: 0; }

.scroll-country-container .country-image {
    width: 32px;
    height: 32px;
    border-radius: 7px;
    background-color: #f1f3f5; }

.scroll-country-container .country-image img {
      height: 100%;
      width: 100%;
      border-radius: 7px; }

.scroll-country-container .country-title {
    margin-top: 8px;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    /* autoprefixer: ignore next */
    -webkit-box-orient: vertical; }

.description.post-body li {
  margin-left: 15px; }

.description.post-body img {
  width: 100% !important;
  margin-top: 11px;
  border-radius: 12px; }

.description iframe {
  width: 100% !important;
  margin-top: 30px;
  border-radius: 12px; }

.main-card {
  background: #ffffff; }

.main-card .main-card-container {
    width: 100%;
    height: 230px;
    background-position: center;
    background-size: cover;
    background-image: url(&quot;/assets/icons/emptyState/placeholder_cover.jpg&quot;); }

.main-card .main-card-container .cover-image {
      width: 100%;
      height: 230px; }

.main-card .main-card-container .card-navbar {
      display: -webkit-box;
      display: flex;
      -webkit-box-pack: justify;
              justify-content: space-between;
      position: absolute;
      top: 20px;
      width: 100%;
      z-index: 100;
      padding: 0 20px; }

.main-card .main-card-container .card-navbar .left img {
        width: 24px;
        height: 24px; }

.main-card .main-card-container .card-navbar .left::before {
        content: &quot;&quot;;
        height: 24px;
        width: 24px;
        opacity: 0;
        background: rgba(223, 223, 223, 0.2);
        border-radius: 50%;
        position: absolute; }

.main-card .main-card-container .card-navbar .left:active::before {
        -webkit-transform: scale(2);
                transform: scale(2);
        -webkit-transition: all 0.1s;
        transition: all 0.1s;
        opacity: 1; }

.main-card .main-card-container .card-navbar .right img {
        width: 24px;
        height: 24px; }

.main-card .main-card-container .card-navbar .right::before {
        content: &quot;&quot;;
        height: 24px;
        width: 24px;
        opacity: 0;
        background: rgba(223, 223, 223, 0.3);
        border-radius: 50%;
        position: absolute; }

.main-card .main-card-container .card-navbar .right:active::before {
        -webkit-transform: scale(2);
                transform: scale(2);
        -webkit-transition: all 0.1s;
        transition: all 0.1s;
        opacity: 1; }

.main-card .main-card-container .overlay {
      position: absolute;
      width: 100%;
      height: 230px;
      top: 0;
      left: 0;
      background-repeat: no-repeat;
      background-size: cover;
      background-position: center; }

.main-card .main-card-container .overlay.green {
        background-color: rgba(5, 229, 206, 0.41); }

.main-card .main-card-container .card-search {
      display: -webkit-box;
      display: flex;
      height: 39px;
      -webkit-box-align: center;
              align-items: center;
      position: absolute;
      top: 20px;
      width: 100%;
      padding: 0 20px; }

.main-card .main-card-container .card-search .input {
        margin-right: 20px;
        width: 100%;
        position: relative;
        height: 39px; }

.main-card .main-card-container .card-search .input input {
          width: 100%;
          height: 34px;
          border-radius: 18px;
          background-color: #F0F5FF;
          box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.05);
          border: 0;
          padding-left: 50px;
          padding-right: 20px;
          position: relative; }

.main-card .main-card-container .card-search .input input.search {
            position: absolute;
            background-image: url(&quot;/assets/icons/diasporaIcon/Search_Grey.svg&quot;);
            background-position: 16px;
            background-repeat: no-repeat;
            background-size: 20px;
            padding-bottom: 2px; }

.main-card .main-card-container .card-search .input input::-webkit-input-placeholder {
            font-size: 14px;
            color: #B1BCC6; }

.main-card .main-card-container .card-search .input input::-moz-placeholder {
            font-size: 14px;
            color: #B1BCC6; }

.main-card .main-card-container .card-search .input input::-ms-input-placeholder {
            font-size: 14px;
            color: #B1BCC6; }

.main-card .main-card-container .card-search .input input::placeholder {
            font-size: 14px;
            color: #B1BCC6; }

.main-card .main-card-container .icons {
      margin-left: auto;
      display: -webkit-box;
      display: flex;
      flex-shrink: 0;
      z-index: 10; }

.main-card .main-card-container .icons .first {
        flex-shrink: 0;
        margin-right: 18px;
        position: relative; }

.main-card .main-card-container .icons .first img {
          width: 24px;
          height: 25px; }

.main-card .main-card-container .icons .first .withBadge {
          position: absolute;
          background: #FE4A49;
          width: 18px;
          height: 18px;
          border-radius: 50%;
          display: -webkit-box;
          display: flex;
          -webkit-box-align: center;
                  align-items: center;
          -webkit-box-pack: center;
                  justify-content: center;
          border: 2px solid #ffffff;
          padding: 1px;
          top: -5px;
          left: 12px;
          padding-bottom: 1px; }

.main-card .main-card-container .icons .first .withBadge.notifications-count {
            font-size: 9px; }

.main-card .main-card-container .icons .second {
        flex-shrink: 0; }

.main-card .main-card-container .icons .second img {
          width: 24px;
          height: 24px; }

.main-card .card-body {
    background: #ffffff;
    width: 100%;
    box-shadow: 0 5px 16px 0 rgba(229, 236, 245, 0.35);
    margin-top: -78px;
    border-radius: 13px;
    z-index: 72;
    padding: 20px 0;
    padding-top: 68px;
    position: relative; }

.main-card .card-body .card-body-image {
      width: 100px;
      height: 100px;
      position: absolute;
      box-shadow: 0 6px 14px 0 rgba(0, 0, 0, 0.1);
      top: -50px;
      left: 50%;
      border-radius: 50%;
      -webkit-transform: translate(-50%, 0);
              transform: translate(-50%, 0); }

.main-card .card-body .card-body-image img {
        width: 100px;
        height: 100px;
        border-radius: 50%;
        -o-object-fit: cover;
           object-fit: cover; }

.main-card .card-body .card-body-container {
      text-align: center; }

.main-card .card-body .card-body-container .card-body-title {
        padding: 0 20px;
        overflow: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;
        -webkit-line-clamp: 2;
        /* autoprefixer: ignore next */
        -webkit-box-orient: vertical; }

.main-card .card-body .card-body-container .card-body-subtitle {
        margin-top: 2px; }

.main-card .card-body .card-body-container .card-body-subtitle-with-image {
        display: -webkit-box;
        display: flex;
        -webkit-box-pack: center;
                justify-content: center;
        -webkit-box-align: center;
                align-items: center;
        padding: 0 20px;
        margin-top: 2px; }

.main-card .card-body .card-body-container .card-body-subtitle-with-image img {
          width: 12px;
          margin-right: 6px;
          flex-shrink: 0; }

.main-card .desc {
    margin-top: 20px;
    display: -webkit-box;
    display: flex;
    -webkit-box-pack: center;
            justify-content: center;
    -webkit-box-align: start;
            align-items: flex-start;
    border-radius: 0 0 13px 13px; }

.main-card .desc.space-between-10 {
      padding: 0 10px; }

.main-card .desc.space-between-30 {
      padding: 0 30px; }

.main-card .desc.space-between-65 {
      padding: 0 65px; }

.main-card .desc .desc-container {
      display: -webkit-box;
      display: flex;
      -webkit-box-orient: vertical;
      -webkit-box-direction: normal;
              flex-direction: column;
      -webkit-box-pack: center;
              justify-content: center;
      -webkit-box-align: center;
              align-items: center;
      width: 82.5px;
      flex-shrink: 0;
      padding: 0 8px; }

.main-card .desc .desc-container.width-120 {
        width: 120px; }

.main-card .desc .desc-container.width-100 {
        width: 100px; }

.main-card .desc .desc-container.width-81 {
        width: 81px; }

.main-card .desc .desc-container:first-child {
        margin-left: 0; }

.main-card .desc .desc-container:last-child {
        margin-right: 0; }

.main-card .desc .desc-container .desc-first {
        height: 32px;
        width: 32px;
        color: #ffffff;
        font-size: 12px;
        font-weight: 600;
        display: -webkit-box;
        display: flex;
        -webkit-box-align: center;
                align-items: center;
        -webkit-box-pack: center;
                justify-content: center;
        border-radius: 7px;
        border: 1px solid #eeeeee; }

.main-card .desc .desc-container .desc-first.green-gradient {
          background-color: #00d2bd; }

.main-card .desc .desc-container .desc-first img {
          width: 100%;
          height: 100%;
          border-radius: 7px;
          -o-object-fit: cover;
             object-fit: cover; }

.main-card .desc .desc-container .desc-first.country-flag {
          width: 32px;
          height: 32px; }

.main-card .desc .desc-container .desc-first.country-flag img {
            width: 100%;
            height: 100%; }

.main-card .desc .desc-container span {
        margin-top: 8px;
        overflow: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;
        -webkit-line-clamp: 2;
        /* autoprefixer: ignore next */
        -webkit-box-orient: vertical;
        text-align: center; }

.ios .input input {
  padding-top: 5px; }

.card-main-button-container {
  background-color: #f0f0f0; }

.card-main-button-container .card-main-button {
    width: 100%;
    background: #ffffff;
    height: 50px;
    display: -webkit-box;
    display: flex; }

.card-main-button-container .card-main-button .card-main-button-item {
      width: 50%;
      display: -webkit-box;
      display: flex;
      -webkit-box-pack: center;
              justify-content: center;
      -webkit-box-align: center;
              align-items: center;
      padding-top: 16px; }

.card-main-button-container .card-main-button .card-main-button-item img {
        width: 20px;
        height: 20px;
        margin-right: 8px; }

.card-main-button-container .card-main-button .card-main-button-item.width-100 {
        width: 100%; }

.card-main-button-container .card-main-button .card-main-button-item:nth-child(even) {
        border-left: 1px solid #f1f3f5; }

.main-seperator {
  padding: 20px 0;
  padding-top: 30px;
  display: -webkit-box;
  display: flex;
  -webkit-box-align: center;
          align-items: center;
  background: #ffffff; }

.main-seperator .right-item {
    margin-left: auto;
    display: -webkit-box;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
            flex-direction: column;
    -webkit-box-align: center;
            align-items: center; }

.main-seperator .right-item .first img {
      width: 18px; }

.main-seperator .right-item .first img.large-image {
        width: 41px;
        height: 41px; }

.slider-card .card-container {
  position: relative;
  height: 100%; }

.slider-card .card-container .card-main-image {
    height: 100%;
    position: relative; }

.slider-card .card-container .card-main-image img {
      height: 100%;
      width: 100%;
      border-radius: 13px;
      -o-object-fit: cover;
         object-fit: cover;
      border: 1px solid #eeeeee; }

.slider-card .card-container .card-main-image .overlay {
      position: absolute;
      background-color: rgba(74, 74, 74, 0.71);
      width: 100%;
      height: 100%;
      left: 0;
      top: 0;
      border-radius: 13px; }

.slider-card .card-container .card-main-image .card-text {
      font-size: 13px;
      font-weight: bold;
      color: #ffffff;
      letter-spacing: 0.13px;
      text-align: center;
      position: absolute;
      top: 39px;
      padding: 0 10px;
      text-align: center;
      width: 100%;
      overflow: hidden;
      text-overflow: ellipsis;
      display: -webkit-box;
      -webkit-line-clamp: 2;
      /* autoprefixer: ignore next */
      -webkit-box-orient: vertical; }

.slider-card .card-container .card-main-image .card-logo {
      position: absolute;
      top: -27px;
      height: 55px;
      width: 55px;
      left: 50%;
      -webkit-transform: translate(-50%, 0px);
              transform: translate(-50%, 0px); }

.slider-card .card-container .actions {
    background-color: #00d2bd;
    width: 100%;
    height: 56px;
    position: absolute;
    bottom: 0;
    border-radius: 13px;
    display: -webkit-box;
    display: flex;
    justify-content: space-around; }

.slider-card .card-container .actions .actions-item {
      display: -webkit-box;
      display: flex;
      -webkit-box-orient: vertical;
      -webkit-box-direction: normal;
              flex-direction: column;
      -webkit-box-align: center;
              align-items: center;
      -webkit-box-pack: center;
              justify-content: center;
      padding: 11px; }

.slider-card .card-container .actions .actions-item img {
        width: 15px;
        height: 15px; }

.slider-card .card-container .actions .actions-item span {
        color: #ffffff;
        font-size: 11px;
        margin-top: 3px; }

.hints {
  padding: 0 16px; }

.hints .hints-container {
    min-height: 72px;
    border: 1px solid #D8EFF6;
    padding: 0 16px;
    border-radius: 6px;
    display: -webkit-box;
    display: flex;
    -webkit-box-align: center;
            align-items: center;
    background-color: #FAFCFE;
    margin-top: 100px; }

.hints .hints-container .left {
      flex-shrink: 0; }

.hints .hints-container .left img {
        height: 32px;
        width: 32px; }

.hints .hints-container .middle {
      margin-left: 14px;
      max-width: 200px; }

.hints .hints-container .right {
      margin-left: auto; }

.hints .hints-container .right img {
        height: 16px; }

* {
  box-sizing: border-box;
  margin: 0;
  padding: 0;
  outline: none;
  font-family: 'Open Sans', sans-serif !important;
  word-break: break-word;
  --ripple-color: transparent; }

::-webkit-scrollbar,
*::-webkit-scrollbar {
  display: none; }

body.blackBody {
  background: #000000; }

.inline {
  display: -webkit-box;
  display: flex; }

.inline.wrap {
    flex-wrap: wrap; }

.inline.scroll {
    overflow: scroll; }

.inline.center {
    -webkit-box-align: center;
            align-items: center; }

.inline.h-center {
    -webkit-box-pack: center;
            justify-content: center; }

.scroll-multiple-line {
  display: -webkit-box;
  display: flex; }

app-list-card {
  width: 100%; }

.slide-container {
  width: 100%;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
          flex-direction: column;
  padding-right: 12px; }

.slide-container .list-title {
    -webkit-line-clamp: 1; }

.slide-container.action-slider {
    margin-right: 2px;
    padding-right: 0 !important; }

ion-slide:last-child .action-slider {
  margin-right: 0 !important; }

ion-slide:last-child .slide-container {
  padding-right: 0 !important; }

.text-with-line-container {
  display: -webkit-box;
  display: flex;
  -webkit-box-align: center;
          align-items: center;
  padding: 0 20px;
  padding-top: 20px;
  background-color: #ffffff; }

.text-with-line-container .line-left {
    width: 100%;
    height: 1px;
    background-color: #f1f3f5; }

.text-with-line-container .line-right {
    width: 100%;
    height: 1px;
    background-color: #f1f3f5; }

.text-with-line-container .txt {
    flex-shrink: 0;
    padding: 0 16px;
    z-index: 10; }

.center-text {
  text-align: center; }

.spinnerHolder {
  display: -webkit-box;
  display: flex;
  -webkit-box-pack: center;
          justify-content: center;
  margin-top: 80px; }

.spinnerHolder ion-spinner {
    color: #00d2bd !important; }

.bullet-point {
  font-size: 9px;
  margin-right: 8px;
  margin-left: 8px; }

.infinite-loading {
  margin: 0 !important; }

ion-infinite-scroll-content {
  min-height: 70px; }

ion-infinite-scroll-content.gray-background {
    background-color: #f0f0f0; }

ion-infinite-scroll-content.long-height {
    min-height: 79px; }

ion-spinner {
  color: #00d2bd !important; }

.mainFabButton {
  bottom: 20px !important;
  right: 20px !important; }

.mainFabButton ion-fab-button {
    --background: #00d2bd;
    height: 50px;
    width: 50px;
    --background-activated: #00d2bd; }

.mainFabButton img {
    height: 21px; }

ion-app {
  margin-top: env(safe-area-inset-top);
  margin-bottom: env(safe-area-inset-bottom); }

.header-ios ion-toolbar:last-child {
  --border-width: 0 !important; }

.ios ion-header ion-toolbar:first-child {
  --padding-top: 0px !important;
  --padding-bottom: 0px !important;
  --padding-start: 0px !important;
  --padding-end: 0px !important;
  padding: 0 !important; }

.ios ion-content {
  --keyboard-offset: 0px !important; }

.ios .fr-toolbar.fr-top {
  border: 0;
  height: 57px !important;
  display: -webkit-box !important;
  display: flex !important;
  -webkit-box-align: center !important;
          align-items: center !important;
  background: #f0f5ff !important;
  border-top-left-radius: 10px !important;
  border-top-right-radius: 10px !important; }

.blackToolbar {
  --background: #000000; }

.blackToolbar .pages-top-bar-title {
    color: #ffffff !important; }

ion-footer {
  background: #ffffff; }

.ios ion-footer {
  border-top: 1px solid #ECEFF1 !important; }

.ios ion-footer.no-border {
    border-top: 0 !important; }

.ios ion-footer.no-border-footer-top {
    border-top: 0 !important; }

.remove-cursor-input input {
  caret-color: transparent !important;
  pointer-events: none; }

.remove-cursor-input textarea {
  caret-color: transparent !important;
  pointer-events: none; }

.add-cursor-input input {
  caret-color: #00d2bd !important;
  pointer-events: auto; }

.add-cursor-input textarea {
  caret-color: #00d2bd !important;
  pointer-events: auto; }

.toast-with-button {
  --button-color: #00d2bd; }

.ios .footer-ios ion-toolbar:first-child {
  padding: 0 !important;
  --border-color:#D5E0EA; }

.ios ion-footer.fixedspacefooter {
  padding: !important; }

.checkbox {
  height: 60px;
  display: -webkit-box;
  display: flex;
  -webkit-box-align: center;
          align-items: center;
  border-bottom: 1px solid #f2f5f7; }

.checkbox img {
    width: 24px;
    margin-right: 18px; }

.checkbox:last-child {
    border: 0; }

.checkbox .checkbox-container {
    display: -webkit-box;
    display: flex; }

.checkbox .container {
    display: block;
    position: relative;
    cursor: pointer;
    width: 100%;
    display: -webkit-box;
    display: flex;
    -webkit-box-align: center;
            align-items: center; }

.checkbox .container input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    height: 0;
    width: 0; }

.checkbox .checkmark {
    position: absolute;
    top: 4px;
    right: 0;
    height: 20px;
    width: 20px;
    background-color: #ffffff;
    border: 2px solid #d5d9db;
    border-radius: 2px;
    border-radius: 50%; }

.checkbox .container input:checked ~ .checkmark {
    background-color: #ffffff;
    border: 2px solid #00d2bd; }

.checkbox .checkmark:after {
    content: &quot;&quot;;
    position: absolute;
    display: none; }

.checkbox .container input:checked ~ .checkmark:after {
    display: block; }

.checkbox .container .checkmark:after {
    left: 5px;
    top: 2px;
    width: 4px;
    height: 8px;
    border: solid #00d2bd;
    border-width: 0 2px 2px 0;
    -webkit-transform: rotate(45deg);
            transform: rotate(45deg); }

.checkbox .txt {
    width: 80%;
    display: inline-block;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis; }

.checkbox input[disabled] {
    display: none; }

.main-input {
  margin-bottom: 13px;
  border-bottom: 1px solid #f1f3f5; }

.main-input.no-border {
    border: 0; }

.main-input.navbar-input {
    margin: 0;
    border: 0;
    padding-bottom: 0;
    margin-top: 20px; }

.main-input app-input {
    width: 100%; }

.main-input .main-input-container {
    margin-top: 13px;
    width: 100%; }

.main-input .main-input-container .input-title {
      min-height: 18px;
      text-align: left; }

.main-input .main-input-container .input-title.required::after {
        content: &quot; *&quot;;
        color: red;
        font-size: 14px; }

.main-input .main-input-container .input-container {
      margin-top: 6px; }

.main-input .main-input-container .input-container .input {
        border-radius: 8px;
        -webkit-appearance: none;
        background-color: #F0F5FF;
        box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.02);
        border: 0;
        height: 48px;
        width: 100%;
        padding: 0 16px;
        display: -webkit-box;
        display: flex;
        -webkit-box-pack: start;
                justify-content: flex-start; }

.main-input .main-input-container .input-container .input::-webkit-input-placeholder {
          font-family: 'Open Sans', sans-serif;
          font-weight: 500;
          text-transform: none;
          font-size: 14px;
          letter-spacing: 0.2px;
          overflow: hidden;
          text-overflow: ellipsis;
          display: -webkit-box;
          -webkit-line-clamp: 1;
          /* autoprefixer: ignore next */
          -webkit-box-orient: vertical;
          color: #B1BCC6; }

.main-input .main-input-container .input-container .input::-moz-placeholder {
          font-family: 'Open Sans', sans-serif;
          font-weight: 500;
          text-transform: none;
          font-size: 14px;
          letter-spacing: 0.2px;
          overflow: hidden;
          text-overflow: ellipsis;
          display: -webkit-box;
          -webkit-line-clamp: 1;
          /* autoprefixer: ignore next */
          -webkit-box-orient: vertical;
          color: #B1BCC6; }

.main-input .main-input-container .input-container .input::-ms-input-placeholder {
          font-family: 'Open Sans', sans-serif;
          font-weight: 500;
          text-transform: none;
          font-size: 14px;
          letter-spacing: 0.2px;
          overflow: hidden;
          text-overflow: ellipsis;
          display: -webkit-box;
          -webkit-line-clamp: 1;
          /* autoprefixer: ignore next */
          -webkit-box-orient: vertical;
          color: #B1BCC6; }

.main-input .main-input-container .input-container .input::placeholder {
          font-family: 'Open Sans', sans-serif;
          font-weight: 500;
          text-transform: none;
          font-size: 14px;
          letter-spacing: 0.2px;
          overflow: hidden;
          text-overflow: ellipsis;
          display: -webkit-box;
          -webkit-line-clamp: 1;
          /* autoprefixer: ignore next */
          -webkit-box-orient: vertical;
          color: #B1BCC6; }

.main-input .main-input-container .input-container.with-logo {
        position: relative; }

.main-input .main-input-container .input-container.with-logo.left .input {
          padding-left: 42px; }

.main-input .main-input-container .input-container.with-logo.left .left-logo {
          position: absolute;
          width: 20px;
          height: 20px;
          top: 5px;
          left: 16px;
          -webkit-transform: translate(0, 50%);
                  transform: translate(0, 50%); }

.main-input .main-input-container .input-container.with-logo.right .input {
          padding: 0px 48px 0 16px; }

.main-input .main-input-container .input-container.with-logo.right .right-logo {
          position: absolute;
          right: 16px;
          top: 20%; }

.main-input .main-input-container .input-container.with-logo.right .right-logo img {
            width: 16px; }

.main-input .error-message-container {
    height: 17px;
    display: -webkit-box;
    display: flex; }

.main-input .error-message {
    padding-top: 6px;
    color: #FE4A49 !important;
    height: 17px;
    text-align: left;
    display: -webkit-box;
    display: flex; }

.main-input .error-message span {
      height: 17px; }

.main-input .inline-input {
    display: -webkit-box;
    display: flex; }

.main-input .inline-input .main-input-container.left-item {
      padding-right: 8px; }

.main-input .inline-input .main-input-container.right-item {
      padding-left: 8px; }

.main-input .inline-input.special-width-item .main-input-container {
      flex-shrink: 0; }

.main-input .inline-input.special-width-item .left-item {
      width: 130px;
      margin-bottom: 0px; }

.main-input .inline-input app-button {
      flex-shrink: 0; }

.input-container {
  margin-top: 6px; }

.textarea-container {
  margin-bottom: 10px;
  margin-top: 6px; }

.textarea {
  min-height: 120px;
  width: 100%;
  border-radius: 8px;
  background-color: #F0F5FF;
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.02);
  border: 0;
  resize: none;
  padding: 16px;
  max-height: 210px;
  font-size: 14px; }

.textarea::-webkit-input-placeholder {
    font-family: 'Open Sans', sans-serif !important;
    font-weight: 500;
    text-transform: none;
    font-size: 14px;
    letter-spacing: 0.2px;
    color: #b1bcc6; }

.textarea::-moz-placeholder {
    font-family: 'Open Sans', sans-serif !important;
    font-weight: 500;
    text-transform: none;
    font-size: 14px;
    letter-spacing: 0.2px;
    color: #b1bcc6; }

.textarea::-ms-input-placeholder {
    font-family: 'Open Sans', sans-serif !important;
    font-weight: 500;
    text-transform: none;
    font-size: 14px;
    letter-spacing: 0.2px;
    color: #b1bcc6; }

.textarea::placeholder {
    font-family: 'Open Sans', sans-serif !important;
    font-weight: 500;
    text-transform: none;
    font-size: 14px;
    letter-spacing: 0.2px;
    color: #b1bcc6; }

.textarea.noMaxheight {
    max-height: none; }

.send-input-container {
  padding: 12px 20px; }

.send-input-container .send-input-item {
    position: relative;
    max-height: 138px;
    height: 44px; }

.send-input-container .send-input-item .send-input {
      height: 44px;
      background-color: #F0F5FF;
      resize: none;
      width: 100%;
      border: 0;
      padding: 13px;
      padding-left: 46px;
      padding-right: 55px;
      box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.02);
      border-radius: 8px;
      max-height: 138px; }

.send-input-container .send-input-item .send-input::-webkit-input-placeholder {
        color: #B1BCC6;
        font-family: 'Open Sans', sans-serif;
        font-size: 14px;
        font-weight: 500;
        letter-spacing: 0.2px;
        line-height: 19px; }

.send-input-container .send-input-item .send-input::-moz-placeholder {
        color: #B1BCC6;
        font-family: 'Open Sans', sans-serif;
        font-size: 14px;
        font-weight: 500;
        letter-spacing: 0.2px;
        line-height: 19px; }

.send-input-container .send-input-item .send-input::-ms-input-placeholder {
        color: #B1BCC6;
        font-family: 'Open Sans', sans-serif;
        font-size: 14px;
        font-weight: 500;
        letter-spacing: 0.2px;
        line-height: 19px; }

.send-input-container .send-input-item .send-input::placeholder {
        color: #B1BCC6;
        font-family: 'Open Sans', sans-serif;
        font-size: 14px;
        font-weight: 500;
        letter-spacing: 0.2px;
        line-height: 19px; }

.send-input-container .send-input-item .send-image {
      width: 32px;
      height: 32px;
      position: absolute;
      top: 6px;
      left: 7px;
      border-radius: 7px;
      max-height: 138px; }

.send-input-container .send-input-item.height-60 {
      height: 50px;
      overflow: auto; }

.label-required::after {
  content: &quot;  *&quot;;
  color: red;
  font-size: 14px;
  margin-left: 2px; }

.invite-container {
  width: 100%;
  background-color: #ffffff;
  padding: 10px 20px;
  padding-top: 0;
  display: -webkit-box;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
          flex-direction: column;
  -webkit-box-pack: center;
          justify-content: center;
  -webkit-box-align: center;
          align-items: center;
  text-align: center;
  position: relative;
  z-index: 10; }

.invite-container .subtitle {
    margin-top: 10px; }

.invite-container .item-container {
    margin-top: 18px;
    width: 100%;
    display: -webkit-box;
    display: flex;
    -webkit-box-pack: center;
            justify-content: center; }

.invite-container .item-container.column {
      -webkit-box-orient: vertical;
      -webkit-box-direction: normal;
              flex-direction: column; }

.invite-container .item-container .item {
      position: relative;
      flex-shrink: 0;
      margin-top: 25px; }

.invite-container .item-container .item img {
        width: 55px;
        height: 55px;
        border-radius: 12px;
        -o-object-fit: cover;
           object-fit: cover; }

.invite-container .item-container .item .title {
        width: 107px;
        margin-top: 9px;
        overflow: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;
        -webkit-line-clamp: 2;
        /* autoprefixer: ignore next */
        -webkit-box-orient: vertical; }

.invite-container .item-container .item .subtitle {
        margin-top: 3px; }

.invite-container .item-container .item.big-item {
        margin-top: 0; }

.invite-container .item-container .item.big-item img {
          width: 80px;
          height: 80px; }

.invite-container .item-container .item.big-item .badge {
          top: 67px; }

.invite-container .item-container .item .badge {
        position: absolute;
        top: 42px;
        left: 43px; }

.invite-container .item-container .item .badge img {
          width: 24px;
          height: 24px;
          border: 1px solid #ffffff; }

.invite-container .end-item {
    margin-top: 30px;
    width: 100%; }

@font-face {
  font-family: 'Open Sans';
  src: url(&quot;/assets/font/OpenSans-Regular.ttf&quot;); }

@font-face {
  font-family: 'Open Sans';
  src: url(&quot;/assets/font/OpenSans-SemiBold.ttf&quot;);
  font-weight: 600; }

.content-space {
  --padding-start: 20px;
  --padding-end: 20px;
  padding-left: 20px;
  padding-right: 20px; }

.content-space.bottom {
    --padding-bottom: 20px;
    padding-bottom: 20px; }

.content-space.bottom-30 {
    --padding-bottom: 30px;
    padding-bottom: 30px; }

.content-space.top {
    --padding-top: 20px;
    padding-top: 20px; }

.content-space.top-10 {
    --padding-top: 10px;
    padding-top: 10px; }

.content-space.top-20 {
    --padding-top: 20px;
    padding-top: 20px; }

.content-space.top-2 {
    --padding-top: 2px;
    padding-top: 2px; }

.content-space.top-30 {
    --padding-top: 30px;
    padding-top: 30px; }

.content-space.right-0 {
    --padding-right: 0px;
    padding-right: 0px; }

.content-space.card-container-seperator {
    background-color: #f0f0f0;
    position: relative; }

.content-space.card-container-seperator.special-margin {
      margin-top: -7px; }

.no-margin {
  margin: 0; }

.negative-margin-7 {
  margin-top: -7px; }

.no-margin-top {
  margin-top: 0px !important; }

.margin-top-1 {
  margin-top: 1px !important; }

.margin-top-2 {
  margin-top: 2px !important; }

.margin-top-6 {
  margin-top: 6px !important; }

.margin-top-7 {
  margin-top: 7px !important; }

.margin-top-10 {
  margin-top: 10px; }

.margin-top-12 {
  margin-top: 12px !important; }

.margin-top-13 {
  margin-top: 13px !important; }

.margin-top-16 {
  margin-top: 16px; }

.margin-top-18 {
  margin-top: 18px; }

.margin-top-20 {
  margin-top: 20px !important; }

.margin-top-30 {
  margin-top: 30px !important; }

.margin-top-40 {
  margin-top: 40px !important; }

.margin-top-50 {
  margin-top: 50px; }

.no-margin-bottom {
  margin-bottom: 0 !important; }

.margin-bottom-6 {
  margin-bottom: 6px; }

.margin-bottom-10 {
  margin-bottom: 10px; }

.margin-bottom-12 {
  margin-bottom: 12px; }

.margin-bottom-13 {
  margin-bottom: 13px; }

.margin-bottom-14 {
  margin-bottom: 14px !important; }

.margin-bottom-17 {
  margin-bottom: 17px; }

.margin-bottom-20 {
  margin-bottom: 20px !important; }

.margin-bottom-30 {
  margin-bottom: 30px !important; }

.margin-bottom-40 {
  margin-bottom: 40px !important; }

.margin-bottom-50 {
  margin-bottom: 50px !important; }

.margin-right-14 {
  margin-right: 14px; }

.margin-right-8 {
  margin-right: 8px; }

.no-padding {
  padding: 0; }

.padding-top-0 {
  padding-top: 0 !important; }

.padding-top-1 {
  padding-top: 1px !important; }

.padding-top-3 {
  padding-top: 3px !important; }

.padding-top-4 {
  padding-top: 4px !important; }

.padding-top-10 {
  --padding-top: 10px !important;
  padding-top: 10px !important; }

.padding-top-20 {
  --padding-top: 20px !important;
  padding-top: 20px !important; }

.padding-top-30 {
  padding-top: 30px; }

.padding-bottom-4 {
  padding-bottom: 4px; }

.padding-bottom-8 {
  padding-bottom: 8px; }

.padding-bottom-10 {
  padding-bottom: 10px;
  --padding-bottom: 10px; }

.padding-bottom-13 {
  padding-bottom: 13px; }

.padding-bottom-16 {
  padding-bottom: 16px; }

.padding-bottom-20 {
  padding-bottom: 20px; }

.padding-bottom-30 {
  padding-bottom: 30px; }

.padding-bottom-70 {
  --padding-bottom: 70px; }

.padding-bottom-79 {
  --padding-bottom: 79px; }

.padding-bottom-100 {
  --padding-bottom: 100px; }

.segment-container {
  display: -webkit-box;
  display: flex;
  width: 100%;
  background-color: #ffffff;
  margin-top: 5px; }

ion-segment {
  min-height: 41px;
  height: 41px;
  background-color: #ffffff;
  width: 100% !important;
  margin-top: -4px; }

ion-segment.with-icon {
    height: 68px; }

ion-segment.with-icon ion-segment-button {
      height: 51px; }

ion-segment-button {
  min-height: 38px;
  opacity: 1;
  letter-spacing: 0.2px;
  line-height: 20px;
  font-weight: 600;
  font-size: 15px;
  --background-checked: #ffffff;
  --indicator-color: transparent !important;
  --indicator-color-checked: #00d2bd !important;
  --color: #b1bcc6;
  --color-checked: #00d2bd;
  --ripple-color: transparent;
  border: 0;
  border-bottom: 1px solid #ECEFF1;
  min-width: 25%;
  max-width: none;
  border-radius: 0;
  height: 100% !important;
  text-transform: capitalize; }

.four-segment ion-segment-button {
  width: 25%; }

.three-segment ion-segment-button {
  width: 33.33%; }

.two-segment ion-segment-button {
  width: 50%; }

.directory {
  margin-top: 0; }

.directory .segment-icon {
    height: 28px;
    width: 28px;
    margin-bottom: 6px;
    margin-top: 5px !important;
    background-color: #707070;
    -webkit-mask-repeat: no-repeat;
    -webkit-mask-size: contain; }

.directory .segment-icon.first {
      -webkit-mask-image: url('People_Active.svg'); }

.directory .segment-icon.second {
      -webkit-mask-image: url('Networks_Active.svg'); }

.directory .segment-icon.third {
      -webkit-mask-image: url('Business_Inactive.svg'); }

.directory .search-segment-checked {
    border: 0;
    padding-top: 1px;
    --indicator-color-checked: transparent !important;
    border-bottom: 2px solid #00d2bd;
    background: #ffffff; }

.directory .search-segment-checked.first {
      color: #00d2bd !important; }

.directory .search-segment-checked.first .segment-icon {
        background-color: #00d2bd; }

.directory .search-segment-checked.second {
      color: #00d2bd; }

.directory .search-segment-checked.second .segment-icon {
        background-color: #00d2bd; }

.directory .search-segment-checked.third {
      color: #00d2bd; }

.directory .search-segment-checked.third .segment-icon {
        background-color: #00d2bd; }

.directory ion-segment {
    min-height: 39px;
    background-color: #ffffff;
    width: 100% !important; }

.directory ion-segment.with-icon {
      height: 60px; }

.directory ion-segment.with-icon ion-segment-button {
        height: 51px; }

.directory ion-segment-button {
    margin-top: 0px; }

.searchSegment {
  margin-top: 0; }

.searchSegment .segment-icon {
    height: 28px;
    width: 28px;
    margin-top: 3px !important;
    margin-bottom: 6px;
    position: relative;
    -webkit-mask-repeat: no-repeat !important;
    -webkit-mask-size: contain !important;
    background-color: #707070; }

.searchSegment .segment-icon.first {
      -webkit-mask-image: url('Everything_Inactive.svg'); }

.searchSegment .segment-icon.second {
      -webkit-mask-image: url('People_Active.svg'); }

.searchSegment .segment-icon.third {
      -webkit-mask-image: url('Networks_Active.svg'); }

.searchSegment .segment-icon.fourth {
      -webkit-mask-image: url('Business_Inactive.svg'); }

.searchSegment .search-segment-checked {
    border: 0;
    border-bottom: 2px solid #00d2bd;
    --indicator-color-checked: transparent !important;
    padding-top: 1px;
    background: #ffffff; }

.searchSegment .search-segment-checked .first {
      color: #00d2bd; }

.searchSegment .search-segment-checked .second {
      color: #00d2bd; }

.searchSegment .search-segment-checked .third {
      color: #00d2bd; }

.searchSegment .search-segment-checked .fourth {
      color: #00d2bd; }

.searchSegment .search-segment-checked.first {
      border-color: #00d2bd; }

.searchSegment .search-segment-checked.second {
      border-color: #00d2bd; }

.searchSegment .search-segment-checked.third {
      border-color: #00d2bd; }

.searchSegment .search-segment-checked.fourth {
      border-color: #00d2bd; }

.searchSegment .search-segment-checked .segment-icon {
      background: #00d2bd; }

.gallery {
  padding: 1px; }

.gallery .gallery-container {
    display: -webkit-box;
    display: flex;
    flex-shrink: 0;
    flex-wrap: wrap; }

.gallery .gallery-container .item-container {
      padding: 1px;
      width: 100%;
      flex-shrink: 0;
      position: relative;
      background-position: center;
      background-size: cover;
      background-repeat: no-repeat; }

.gallery .gallery-container .item-container .item {
        padding-top: 99%;
        background-position: center;
        background-size: cover;
        background-repeat: no-repeat; }

.gallery .gallery-container .item-container .icon {
        position: absolute;
        width: 50px;
        top: 50%;
        left: 50%;
        -webkit-transform: translate(-50%, -50%);
                transform: translate(-50%, -50%); }

.gallery-view-image {
  height: 100%;
  background: #000000;
  position: relative;
  display: -webkit-box;
  display: flex;
  width: 100%; }

.gallery-view-image .video-player {
    width: 100%; }

.gallery-view-image .gallery-view-image-container {
    display: -webkit-box;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
            flex-direction: column;
    width: 100%;
    padding-bottom: 20px; }

.gallery-view-image .slider__video-container {
    position: absolute;
    width: 100%;
    width: 100%;
    position: absolute;
    top: 50%;
    -webkit-transform: translate(0, -50%);
            transform: translate(0, -50%); }

.gallery-view-image .slider__video-container .slider__image {
      width: 100%; }

.gallery-view-image .slider__video-container .video {
      position: absolute;
      top: 50%;
      left: 50%;
      -webkit-transform: translate(-50%, -50%);
              transform: translate(-50%, -50%);
      max-width: 100px; }

.media-list-item {
  height: 100%;
  display: -webkit-box;
  display: flex;
  background-color: #000000;
  padding-top: 5px;
  border-top: 1px solid rgba(214, 214, 233, 0.46); }

.media-list-item .list-item-row-title-large {
    color: #ffffff !important;
    text-align: left; }

.media-list-item .list-items-left {
    height: 0;
    width: 0; }

.media-list-item app-list {
    width: 100%; }

.slider__caption {
  text-align: left;
  background-color: #000000;
  position: absolute;
  bottom: 0;
  background-color: rgba(0, 0, 0, 0.5);
  width: 100%; }

.slider__caption .truncatedTextHolder {
    max-height: 60vh;
    overflow: scroll;
    padding: 15px 20px;
    line-height: 21px; }

.imageViewer-container {
  width: 100%; }

.imageViewer-container ion-footer::before {
    background-image: none !important;
    height: 0 !important;
    background-color: #000000;
    content: none !important; }

.imageViewer-container .content-space {
    background: #000000; }

.detailsGalleryType {
  display: -webkit-box;
  display: flex;
  flex-wrap: wrap; }

.detailsGalleryType app-media-card {
    flex-shrink: 0;
    width: 33.33%; }

ion-modal.action-sheet-style {
  --background: transparent; }

ion-modal.action-sheet-style ion-content {
    --background: transparent; }

ion-modal.action-sheet-style .sheet {
    background-color: #ffffff;
    position: absolute;
    bottom: 0;
    width: 100%;
    padding: 20px;
    padding-top: 12px;
    border-radius: 16px 16px 0 0; }

ion-modal.action-sheet-style .sheet .vertical-sheet .vertical-sheet-item {
      display: -webkit-box;
      display: flex;
      -webkit-box-align: center;
              align-items: center;
      height: 60px; }

ion-modal.action-sheet-style .sheet .vertical-sheet .vertical-sheet-item img {
        margin-right: 18px;
        width: 24px;
        height: 24px;
        flex-shrink: 0; }

ion-modal.action-sheet-style .sheet .vertical-sheet .vertical-sheet-item .txt {
        border-bottom: 1px solid #f1f3f5;
        height: 100%;
        display: -webkit-box;
        display: flex;
        -webkit-box-align: center;
                align-items: center;
        width: 100%; }

ion-modal.action-sheet-style .sheet .vertical-sheet .vertical-sheet-item:last-child .txt {
        border: 0; }

ion-modal.action-sheet-style .sheet .vertical-sheet .vertical-sheet-item.main-sheet {
        height: 74px; }

ion-modal.action-sheet-style .sheet .vertical-sheet .vertical-sheet-item.main-sheet img {
          width: 32px;
          height: 32px; }

ion-modal.action-sheet-style .opacity-sheet {
    height: 100vh; }

.cancel-sheet {
  margin-top: 12px; }

.banner-button-button-subtitle {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 14px;
  letter-spacing: 0.2px;
  color: #b1bcc6; }

.button-small-gray {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 14px;
  letter-spacing: 0.2px;
  color: #707070;
  font-size: 11px; }

.button-small-green {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 14px !important;
  letter-spacing: 0.2px;
  color: #00D2BD;
  font-size: 11px; }

.button-small-green.block {
    display: block; }

.button-small-white {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 14px;
  letter-spacing: 0.2px;
  color: white; }

.empty-state-block-button-title {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 14px;
  letter-spacing: 0px;
  color: #ffffff; }

.button-large-white {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 16px;
  letter-spacing: 0.2px;
  line-height: 22px;
  color: white; }

.banner-button-button-title {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 15px;
  letter-spacing: 0.2px;
  color: #00d2bd; }

.button-large-green {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 16px;
  letter-spacing: 0.2px;
  line-height: 22px;
  color: #00d2bd; }

.ios .button-large-green {
  font-family: 'Open Sans', sans-serif !important;
  font-weight: 500;
  text-transform: none;
  font-size: 16px;
  letter-spacing: 0.2px;
  line-height: 22px;
  color: #00d2bd; }

.ios .banner-button-button-title {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 15px;
  letter-spacing: 0.2px;
  color: #00d2bd; }

.ios .empty-state-block-button-title {
  font-family: 'Open Sans', sans-serif !important !important;
  font-weight: 600;
  text-transform: none;
  font-size: 14px;
  letter-spacing: 0px;
  color: #ffffff; }

.page-header-page-title-large {
  color: #ffffff;
  font-family: 'Open Sans', sans-serif;
  font-size: 24px;
  font-weight: 600;
  text-align: center; }

.page-header-page-subtitle-large {
  color: #ffffff;
  font-family: 'Open Sans', sans-serif;
  font-size: 18px;
  text-align: center; }

.page-header-page-title {
  color: #707070;
  font-family: 'Open Sans', sans-serif;
  font-size: 18px;
  font-weight: 600;
  text-align: center; }

.page-header-page-subtitle {
  color: #B1BCC6;
  font-family: 'Open Sans', sans-serif;
  font-size: 13px;
  font-weight: 500;
  text-align: center; }

.pages-blocks-news-dot {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 8px;
  letter-spacing: 0px;
  color: #b1bcc6; }

.text-body-dot {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 8px;
  letter-spacing: 0px;
  color: #3a4143; }

.pages-comment-dot {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 8px;
  letter-spacing: 0px;
  color: #b1bcc6; }

.post-dot {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 8px;
  letter-spacing: 0px;
  color: #b1bcc6; }

.welcome-locations-and-roles {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 11px;
  color: #848f9a; }

.pages-messages-time {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 11px;
  letter-spacing: 0px;
  color: #b1bcc6; }

.post-location {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 11px;
  color: #848f9a; }

.product-country-of-availability {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 11px;
  color: #848f9a; }

.bottom-bar-inactive {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 12px;
  letter-spacing: 0.1px;
  color: #848f9a;
  min-height: 14px; }

.bottom-bar-active {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 12px;
  color: #00d2bd; }

.welcome-manage-account {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 11px;
  color: #848f9a; }

.welcome-user-reach {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 12px;
  letter-spacing: 0px;
  color: white; }

.create-post-post-under {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 12px;
  color: #b1bcc6; }

.input-info-message {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 12px;
  color: #b1bcc6; }

.text-body-hint {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 12px;
  letter-spacing: 0.1px;
  color: #b1bcc6; }

.pages-blocks-news-posted-under {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 13px;
  color: #848f9a; }

.pages-blocks-news-date {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 13px;
  color: #b1bcc6; }

.pages-blocks-news-author {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 13px;
  color: #b1bcc6; }

.banner-header-main-subtitle {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 13px;
  color: #848f9a; }

.entity-entity-location {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 13px;
  color: #707070; }

.pages-events-host {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 13px;
  color: #b1bcc6; }

.pages-notifications-notification-time {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 13px;
  color: #b1bcc6; }

.pages-comment-likes {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 13px;
  color: #b1bcc6; }

.pages-comment-date {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 13px;
  color: #b1bcc6; }

.post-date {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 13px;
  color: #b1bcc6; }

.post-post-posted-under {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 13px;
  color: #848f9a; }

.post-post-author {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 13px;
  color: #707070; }

.more-txt {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 13px;
  color: #707070; }

.post-post-like {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 13px;
  color: #848f9a; }

.pages-manage-subtitle {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 13px;
  color: #b1bcc6; }

.input-title {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 13px;
  color: #b1bcc6; }

.pages-top-bar-stepper {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 13px;
  color: #00d2bd;
  width: 100%; }

.pages-top-bar-subtitle {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 13px;
  color: #b1bcc6; }

.pages-sub-section-sub-section-description {
  font-family: 'Open Sans', sans-serif;
  text-transform: none;
  font-size: 16px;
  letter-spacing: 0.3px;
  font-weight: bold;
  color: #00d2bd;
  font-size: 18px; }

.pages-sub-section-sub-section-description.second-title {
    font-size: 13px;
    font-weight: 500; }

.pages-image-preview-image-date {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 13px;
  color: #b1bcc6; }

.banner-top-networker-networker-invites {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 13px;
  color: #fe4c4b; }

.welcome-mypages-action {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 13px;
  color: #00d2bd; }

.empty-state-page-subtitle {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 13px;
  color: #b1bcc6; }

.text-body-subtitle {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 13px;
  color: #848f9a; }

.post-post-like-active {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 13px;
  color: #00d2bd; }

.tabs-small-inactive {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 13px;
  color: #b1bcc6; }

.card-subtitle {
  color: #707070;
  font-size: 12px;
  letter-spacing: 0.2px; }

.list-subtitle {
  color: #2B2B2B;
  font-size: 13px;
  letter-spacing: 0.2px;
  font-weight: 500; }

.list-subtitle.green {
    color: #00d2bd; }

.list-subtitle.gray {
    color: #707070; }

.list-subtitle.bolder {
    font-weight: 600;
    font-size: 13px;
    min-height: 18px; }

.tabs-small-active {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 13px;
  color: #00d2bd; }

.tabs-small-networks {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 13px;
  color: #185bdc; }

.tabs-small-business {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 13px;
  color: #283593; }

.tabs-small-everything {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 13px;
  color: #243557; }

.input-hint {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 14px;
  color: #b1bcc6; }

.entity-entity-type {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 14px;
  color: #3a4143; }

.text-body-left {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 14px;
  color: #3a4143; }

.input-text {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 14px;
  color: #707070;
  word-break: normal !important; }

.input-text.not-selected {
    color: #B1BCC6; }

.welcome-mypages-entity-name {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 14px;
  color: #848f9a; }

.pages-messages-message {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 14px;
  color: #3a4143; }

.pages-messages-incoming {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 14px;
  color: #3a4143; }

.pages-messages-outgoing {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 14px;
  color: white; }

.welcome-primary-role {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 12px;
  color: #848f9a; }

.pages-comment-user-name {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 14px;
  color: #3a4143; }

.pages-comment-comment {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 14px;
  color: #3a4143; }

.post-body {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 14px;
  color: #3a4143; }

.chip-inactive {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 14px;
  color: #848f9a; }

.chip-active {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 14px;
  color: #00d2bd; }

.create-post-entity-name {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 14px;
  color: #00d2bd; }

.create-post-post-type-selected {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 14px;
  color: #00d2bd; }

.create-post-post-type-deselected {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 14px;
  color: #848f9a; }

.text-body-center {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 14px;
  color: #3a4143; }

.dialog-subtitle {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 14px;
  color: #3a4143; }

.empty-state-block-block-title {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 14px;
  color: #848f9a; }

.pages-image-preview-image-caption {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 14px;
  color: white; }

.pages-image-preview-cropper-actions {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 14px;
  color: white; }

.list-item-row-title-small {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 14px;
  color: #3a4143; }

.links-link-left {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 14px;
  color: #00d2bd; }

.links-link-center {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 14px;
  color: #00d2bd; }

.product-name {
  font-family: 'Open Sans', sans-serif;
  font-weight: 500;
  text-transform: none;
  font-size: 14px;
  color: #3a4143; }

.tabs-large-people {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 15px;
  color: #00d2bd; }

.tabs-large-inactive {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 15px;
  color: #b1bcc6; }

.tabs-large-networks {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 15px;
  color: #185bdc; }

.tabs-large-business {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 15px;
  color: #283593; }

.tabs-large-active {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 15px;
  color: #00d2bd; }

.pages-blocks-news-title {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 16px;
  color: #3a4143; }

.pages-sub-section-sub-section-title {
  font-family: 'Open Sans', sans-serif !important;
  font-weight: 600;
  font-size: 16px;
  letter-spacing: 0.6px;
  color: #3a4143; }

.entity-entity-name {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 16px;
  color: #3a4143; }

.pages-events-title {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 16px;
  color: #3a4143; }

.pages-events-date {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 16px;
  color: #00d2bd; }

.pages-messages-person-name {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 16px;
  color: #3a4143; }

.pages-notifications-notification-title {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 16px;
  color: #3a4143; }

.post-post-title {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 16px;
  color: #3a4143; }

.pages-manage-title {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 16px;
  color: #3a4143; }

.pages-image-preview-person-name {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 16px;
  color: #b1bcc6; }

.banner-top-networker-networker-name {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 16px;
  color: #3a4143; }

.text-body-title {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 16px;
  color: #3a4143; }

.banner-header-main-title {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 17px;
  color: #707070;
  width: 100%; }

.welcome-user-name {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 18px;
  color: #707070; }

.pages-top-bar-title {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 18px;
  color: #3a4143; }

.pages-top-title {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  color: #3a4143; }

.dialog-title {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 18px;
  color: #3a4143; }

.empty-state-page-title {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 18px;
  color: #3a4143; }

.pages-top-bar-title-white {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 18px;
  color: white; }

.pages-blocks-news-block-title {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 20px;
  letter-spacing: 0.18px;
  color: #fe4a49; }

.pages-blocks-people-block-title {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 20px;
  color: #00d2bd; }

.pages-blocks-networks-block-title {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 20px;
  color: #185bdc; }

.pages-blocks-business-block-title {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 20px;
  letter-spacing: 0.18px;
  color: #283593; }

.symbols-group-title {
  font-family: 'Open Sans', sans-serif;
  font-weight: 600;
  text-transform: none;
  font-size: 20px;
  letter-spacing: 0px;
  color: black; }

.button-more-txt {
  color: #656D76;
  font-family: 'Open Sans', sans-serif;
  font-size: 12px;
  font-weight: 600;
  letter-spacing: 0.4px; }

.list-item-row-title-large {
  color: #707070;
  font-family: 'Open Sans', sans-serif;
  font-size: 16px;
  font-weight: 600;
  width: 100%; }

.news-title {
  color: #707070;
  font-family: 'Open Sans', sans-serif;
  font-size: 14px;
  font-weight: 500;
  text-align: center; }

.pages-sub-section-sub-section-mini {
  color: #848F9A;
  font-family: 'Open Sans', sans-serif;
  font-size: 13px;
  font-weight: 500; }

.notifications-count {
  color: #ffffff;
  font-size: 9px;
  font-weight: bold;
  text-align: center; }

.pages-image-preview-image-capt {
  color: #ffffff;
  font-family: &quot;Avenir Next&quot;;
  font-size: 14px;
  font-weight: 500; }

.welcome-my-pages-entity-name {
  color: #707070;
  font-family: 'Open Sans', sans-serif;
  font-size: 14px;
  font-weight: 600; }

.welcome-my-pages-action {
  color: #00d2bd;
  font-family: 'Open Sans', sans-serif;
  font-size: 13px;
  font-weight: 600;
  text-align: center; }

.diaspora-id-can-help {
  color: #6F7578;
  font-family: 'Open Sans', sans-serif;
  font-size: 14px;
  font-weight: 500; }

.view-all-copy {
  color: #00D2BD;
  font-family: &quot;Open Sans&quot;;
  font-size: 16px;
  font-weight: bold;
  letter-spacing: 0.6px;
  text-align: center; }

.banner-header-main-title {
  color: #707070;
  font-size: 16px;
  font-weight: 600;
  letter-spacing: 0.2px; }

.banner-header-main-title.small {
    font-size: 14px; }

.banner-header-main-subtitle {
  color: #848F9A;
  font-size: 13px;
  font-weight: 500;
  letter-spacing: 0.2px; }

.welcome-header {
  display: -webkit-box;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
          flex-direction: column;
  -webkit-box-pack: center;
          justify-content: center;
  -webkit-box-align: center;
          align-items: center; }

.welcome-header img {
    margin-top: 82px;
    border-radius: 24px; }

.welcome-header .welcome-txt {
    margin-top: 92px; }

.welcome-header .welcome-txt .first-item {
      color: #707070;
      font-family: 'Open Sans', sans-serif;
      font-size: 22px;
      font-weight: 600;
      letter-spacing: 0.2px;
      line-height: 33px;
      text-align: center; }

.welcome-header .welcome-txt .second-item {
      margin-top: 16px;
      color: #707070;
      font-family: 'Open Sans', sans-serif;
      font-size: 18px;
      letter-spacing: 0.2px;
      line-height: 26px;
      text-align: center; }

.welcome-page-btn {
  position: absolute;
  bottom: 50px;
  width: calc(100% - 40px);
  padding: 0 5px; }

.welcome-page-btn .second-item {
    margin-top: 25px; }

.login-txt .title {
  margin-bottom: 10px; }

.login-txt .subtitle {
  margin-bottom: 20px; }

.diaspora-footer-with-inline-btn {
  display: -webkit-box;
  display: flex;
  -webkit-box-pack: center;
          justify-content: center;
  padding: 20px; }

.diaspora-footer-with-inline-btn app-button {
    width: calc(33.33% - 7px);
    margin-right: 10px; }

.diaspora-footer-with-inline-btn app-button:last-child {
      margin-right: 0; }

.diaspora-footer-with-inline-btn .btn {
    height: 41px;
    border-radius: 7px;
    margin-right: 10px; }

.diaspora-footer-with-inline-btn .text {
    font-family: 'Open Sans', sans-serif !important;
    font-size: 14px;
    font-weight: 600;
    letter-spacing: 0.2px;
    line-height: 19px;
    text-align: center; }

.inline-link-container {
  display: -webkit-box;
  display: flex;
  margin-top: 40px;
  -webkit-box-pack: center;
          justify-content: center; }

.inline-link-container .h-line {
    width: 1px;
    height: 15px;
    background-color: #f1f3f5;
    margin: 2px 20px; }

.status-container {
  margin-top: 100px;
  display: -webkit-box;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
          flex-direction: column;
  -webkit-box-pack: center;
          justify-content: center;
  -webkit-box-align: center;
          align-items: center; }

.status-container .image {
    width: 64px;
    height: 64px; }

.status-container .image img {
      width: 100%;
      height: 100%; }

.status-container .title {
    margin-top: 20px;
    width: 100%;
    text-align: center; }

.status-container .subtitle {
    margin-top: 8px;
    text-align: center; }

.status-container .empty-button {
    margin-top: 30px;
    width: 100%; }

.card-status {
  padding-bottom: 10px; }

.card-status.last-item-status {
    border: 0;
    padding: 0; }

.card-status .title {
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 3;
    /* autoprefixer: ignore next */
    -webkit-box-orient: vertical;
    padding: 0 20px; }

.card-status .card-status-container {
    width: 100%; }

.card-status .card-status-container .start-item {
      display: -webkit-box;
      display: flex;
      flex-shrink: 0;
      -webkit-box-orient: vertical;
      -webkit-box-direction: normal;
              flex-direction: column;
      -webkit-box-align: center;
              align-items: center;
      text-align: center; }

.card-status .card-status-container .start-item .image {
        margin-bottom: 10px;
        flex-shrink: 0; }

.card-status .card-status-container .start-item .image img {
          height: 48px; }

.card-status .card-status-container .end-item {
      margin-top: 20px; }

.card-status .status-button {
    padding: 0 30px;
    width: 100%;
    height: 48px;
    background-color: #ffffff;
    border-radius: 8px;
    text-align: center; }

.alert-wrapper.sc-ion-alert-ios {
  border-radius: 16px;
  max-width: 300px; }

.alert-sub-title.sc-ion-alert-ios,
.alert-title.sc-ion-alert-ios {
  color: #707070;
  font-family: 'Open Sans', sans-serif !important;
  font-size: 18px;
  font-weight: 600;
  letter-spacing: 0.2px;
  line-height: 25px;
  text-align: center; }

.alert-head.sc-ion-alert-ios + .alert-message.sc-ion-alert-ios {
  color: #707070;
  font-family: 'Open Sans', sans-serif !important;
  font-size: 15px;
  font-weight: 500;
  letter-spacing: 0.2px;
  line-height: 19px;
  text-align: center;
  padding-bottom: 20px; }

.alert-button-group.sc-ion-alert-ios {
  display: -webkit-box;
  display: flex;
  -webkit-box-pack: center;
          justify-content: center;
  padding-right: 20px;
  padding-left: 20px;
  padding-bottom: 20px; }

.alert-button.sc-ion-alert-ios {
  -webkit-box-pack: center !important;
          justify-content: center !important;
  width: 50%;
  height: 48px;
  border: 0; }

.alert-button.sc-ion-alert-ios:first-child {
    padding-left: 0;
    margin-right: 0;
    padding-right: 6px; }

.alert-button.sc-ion-alert-ios:nth-child(2) {
    padding-left: 6px;
    margin-right: 0; }

.alert-button.sc-ion-alert-ios:nth-child(2) .alert-button-inner.sc-ion-alert-ios,
    .alert-button.sc-ion-alert-ios:nth-child(2) .alert-tappable.sc-ion-alert-ios {
      background: #FE4A49;
      color: #ffffff;
      border: 0; }

.alert-button-inner.sc-ion-alert-ios,
.alert-tappable.sc-ion-alert-ios {
  border: 1px solid #707070;
  color: #707070;
  height: 48px;
  border-radius: 8px;
  font-size: 15px;
  font-weight: 600;
  letter-spacing: 0.2px;
  line-height: 22px;
  text-align: center;
  display: -webkit-box;
  display: flex;
  -webkit-box-pack: center;
          justify-content: center;
  padding: 0 7px; }

.alert-wrapper.sc-ion-alert-md {
  border-radius: 16px;
  max-width: 300px; }

.alert-sub-title.sc-ion-alert-md,
.alert-title.sc-ion-alert-md {
  color: #707070;
  font-family: 'Open Sans', sans-serif;
  font-size: 18px;
  font-weight: 600;
  letter-spacing: 0.2px;
  line-height: 25px;
  text-align: center; }

.alert-head.sc-ion-alert-md + .alert-message.sc-ion-alert-md {
  color: #707070;
  font-family: 'Open Sans', sans-serif;
  font-size: 14px;
  letter-spacing: 0.2px;
  line-height: 19px;
  text-align: center;
  padding-bottom: 4px; }

.alert-button-group.sc-ion-alert-md {
  display: -webkit-box;
  display: flex;
  -webkit-box-pack: center;
          justify-content: center;
  padding-right: 20px;
  padding-left: 20px;
  padding-bottom: 11px; }

.alert-button.sc-ion-alert-md {
  -webkit-box-pack: center !important;
          justify-content: center !important;
  width: 50%; }

.alert-button.sc-ion-alert-md:first-child {
    padding-left: 0;
    margin-right: 0;
    padding-right: 6px !important; }

.alert-button.sc-ion-alert-md:nth-child(2) {
    margin-right: 0;
    padding-right: 0;
    padding-left: 6px !important; }

.alert-button.sc-ion-alert-md:nth-child(2) .alert-button-inner.sc-ion-alert-md,
    .alert-button.sc-ion-alert-md:nth-child(2) .alert-tappable.sc-ion-alert-md {
      background: #FE4A49;
      color: #ffffff;
      border: 0; }

.alert-button-inner.sc-ion-alert-md,
.alert-tappable.sc-ion-alert-md {
  height: 48px;
  border-radius: 8px;
  color: #707070;
  border: 1px solid #707070;
  font-family: 'Open Sans', sans-serif;
  font-size: 15px;
  font-weight: 600;
  letter-spacing: 0.2px;
  line-height: 22px;
  text-align: center;
  display: -webkit-box;
  display: flex;
  -webkit-box-pack: center;
          justify-content: center;
  text-transform: capitalize;
  padding: 0 7px; }

.column-button .alert-button-group.sc-ion-alert-md, .column-button .alert-button-group.sc-ion-alert-ios {
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
          flex-direction: column; }

.column-button .alert-button-group.sc-ion-alert-md .alert-button.sc-ion-alert-md, .column-button .alert-button-group.sc-ion-alert-md .alert-button.sc-ion-alert-ios, .column-button .alert-button-group.sc-ion-alert-ios .alert-button.sc-ion-alert-md, .column-button .alert-button-group.sc-ion-alert-ios .alert-button.sc-ion-alert-ios {
    width: 100%; }

.column-button .alert-button.sc-ion-alert-md:nth-child(2), .column-button .alert-button.sc-ion-alert-ios:nth-child(2) {
  padding-left: 0 !important;
  padding-top: 0; }

.column-button .alert-button.sc-ion-alert-md:nth-child(2) span, .column-button .alert-button.sc-ion-alert-ios:nth-child(2) span {
    background-color: #FE4A49 !important; }

.column-button .alert-button.sc-ion-alert-md:nth-child(1), .column-button .alert-button.sc-ion-alert-ios:nth-child(1) {
  padding-right: 0 !important;
  margin-bottom: 20px;
  padding-bottom: 0; }

.column-button .alert-button.sc-ion-alert-md:nth-child(1) span, .column-button .alert-button.sc-ion-alert-ios:nth-child(1) span {
    background-color: #ffffff !important; }

.alert-wrapper.sc-ion-alert-md {
  border-radius: 16px;
  width: 100%;
  max-width: calc(100% - 60px); }

.alert-wrapper.sc-ion-alert-ios {
  border-radius: 16px;
  width: 100%;
  max-width: calc(100% - 60px); }

.green-controle .alert-input-group.sc-ion-alert-md, .green-controle .alert-message.sc-ion-alert-md {
  -webkit-padding-start: 20px !important;
  padding-inline-start: 20px  !important;
  -webkit-padding-end: 20px  !important;
  padding-inline-end: 20px  !important; }

.green-controle .alert-input-wrapper.sc-ion-alert-md input {
  background-color: #F0F5FF;
  padding: 13px 16px;
  border-radius: 8px;
  border-bottom: 0; }

.green-controle .alert-input-wrapper.sc-ion-alert-md input::-webkit-input-placeholder {
  color: #B1BCC6;
  font-family: 'Open Sans', sans-serif;
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0.2px;
  line-height: 19px; }

.green-controle .alert-input-wrapper.sc-ion-alert-md input::-moz-placeholder {
  color: #B1BCC6;
  font-family: 'Open Sans', sans-serif;
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0.2px;
  line-height: 19px; }

.green-controle .alert-input-wrapper.sc-ion-alert-md input::-ms-input-placeholder {
  color: #B1BCC6;
  font-family: 'Open Sans', sans-serif;
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0.2px;
  line-height: 19px; }

.green-controle .alert-input-wrapper.sc-ion-alert-md input::placeholder {
  color: #B1BCC6;
  font-family: 'Open Sans', sans-serif;
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0.2px;
  line-height: 19px; }

.green-controle .alert-input-wrapper.sc-ion-alert-ios input {
  background-color: #F0F5FF;
  padding: 13px 16px;
  border-radius: 8px;
  border: 0; }

.green-controle .alert-input-wrapper.sc-ion-alert-ios input::-webkit-input-placeholder {
  color: #B1BCC6;
  font-family: 'Open Sans', sans-serif;
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0.2px;
  line-height: 19px; }

.green-controle .alert-input-wrapper.sc-ion-alert-ios input::-moz-placeholder {
  color: #B1BCC6;
  font-family: 'Open Sans', sans-serif;
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0.2px;
  line-height: 19px; }

.green-controle .alert-input-wrapper.sc-ion-alert-ios input::-ms-input-placeholder {
  color: #B1BCC6;
  font-family: 'Open Sans', sans-serif;
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0.2px;
  line-height: 19px; }

.green-controle .alert-input-wrapper.sc-ion-alert-ios input::placeholder {
  color: #B1BCC6;
  font-family: 'Open Sans', sans-serif;
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0.2px;
  line-height: 19px; }

.green-controle .alert-input-group.sc-ion-alert-md, .green-controle .alert-message.sc-ion-alert-md {
  padding-top: 0px;
  padding-bottom: 0px; }

.green-controle .alert-button.sc-ion-alert-ios:nth-child(2) {
  padding-left: 8px;
  margin-right: 0; }

.green-controle .alert-button.sc-ion-alert-ios:nth-child(2) span.alert-button-inner.sc-ion-alert-ios {
    background: #00d2bd; }

.green-controle .alert-button.sc-ion-alert-md:nth-child(2) {
  padding-left: 8px;
  margin-right: 0; }

.green-controle .alert-button.sc-ion-alert-md:nth-child(2) span.alert-button-inner.sc-ion-alert-md {
    background: #00d2bd; }

.green-controle.hide-sub-header .alert-head.sc-ion-alert-ios + .alert-message.sc-ion-alert-ios {
  padding-bottom: 0 !important; }

.alert-button ion-ripple-effect {
  display: none; }

.alert-input-group.sc-ion-alert-ios, .alert-message.sc-ion-alert-ios {
  padding: 20px !important;
  padding-top: 0 !important; }

.activated {
  background-color: transparent !important; }

.border-bottom {
  border-bottom: 1px solid #f1f3f5; }

.no-border {
  border: 0; }

.seperate-border {
  width: 100%;
  height: 1px;
  background-color: #f1f3f5; }

.seperate-border-action {
  background: #f0f0f0;
  margin-top: 10px; }

.seperate-border-action .seperate-border-action-body {
    border-top: 1px solid #f1f3f5;
    height: 42px;
    background: #ffffff;
    display: -webkit-box;
    display: flex;
    -webkit-box-align: center;
            align-items: center;
    -webkit-box-pack: center;
            justify-content: center;
    padding-top: 18px; }

.seperate-border-action img {
    height: 13px;
    width: 13px;
    margin-left: 7px; }

.main-border {
  height: 53px;
  background-color: #f0f0f0;
  position: relative; }

.main-border::after {
    content: &quot; &quot;;
    height: 16px;
    width: 100%;
    background: #ffffff;
    position: absolute;
    border-bottom-left-radius: 21px;
    border-bottom-right-radius: 21px;
    box-shadow: 0 4px 4px 0 rgba(0, 0, 0, 0.07);
    left: 0;
    z-index: 5; }

.main-border::before {
    content: &quot; &quot;;
    height: 16px;
    width: 100%;
    background: #ffffff;
    position: absolute;
    border-top-left-radius: 21px;
    border-top-right-radius: 21px;
    bottom: 0;
    left: 0; }

.main-border.no-border-bottom-radius::after {
    content: none; }

.main-border.no-border-top-radius::before {
    content: none; }

.main-border.small-height {
    height: 37px; }

.main-border.small-height-20 {
    height: 20px; }

.main-border.small-height-23 {
    height: 23px; }

.remove-additional-part {
  position: relative; }

.remove-additional-part .remove {
    position: absolute;
    width: 100%;
    height: 16px;
    background: #f0f0f0;
    bottom: 0; }

.remove-additional-part .remove.big-height {
      height: 100px;
      bottom: -100px; }

ion-footer::before {
  background-image: none !important;
  border-top: 1px solid rgba(214, 214, 233, 0.46);
  background-color: #ffffff; }

ion-footer.no-border::before {
  border-top: 0; }

ion-header::after {
  background-image: none !important; }

.fr-toolbar.fr-mobile.fr-top.fr-basic {
  height: 57px;
  display: -webkit-box;
  display: flex;
  -webkit-box-align: center;
          align-items: center;
  background: #F0F5FF;
  border: 0 !important;
  border-bottom: 1px solid #D5E0EA !important;
  box-shadow: none !important;
  border-top-left-radius: 8px;
  border-top-right-radius: 8px;
  color: #00d2bd; }

.fr-toolbar.fr-mobile.fr-top.fr-basic i {
    font: normal normal normal 14px/1 FontAwesome !important;
    font-size: 18px !important; }

.fr-command.fr-btn + .fr-dropdown-menu .fr-dropdown-wrapper .fr-dropdown-content ul.fr-dropdown-list li a.fr-active {
  background: rgba(1, 176, 188, 0.2) !important; }

.fr-command.fr-btn.fr-active + .fr-dropdown-menu {
  box-shadow: none !important;
  box-shadow: 0 3px 6px rgba(1, 176, 188, 0.16), 0 2px 2px 1px rgba(1, 176, 188, 0.14) !important;
  top: 51px !important;
  border-radius: 8px; }

.fr-command.fr-btn.fr-active + .fr-dropdown-menu .fr-command.fr-btn + .fr-dropdown-menu .fr-dropdown-wrapper {
    background: rgba(1, 176, 188, 0.2) !important; }

.fr-box.fr-basic.fr-top .fr-wrapper {
  background-color: #F0F5FF;
  box-shadow: none !important;
  border-bottom-left-radius: 8px !important;
  border-bottom-right-radius: 8px !important; }

.fr-box.fr-basic .fr-element {
  color: #B1BCC6;
  font-family: &quot;Avenir Next&quot;;
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0.2px;
  line-height: 19px;
  min-height: 150px !important; }

.fr-box .fr-counter {
  display: none !important; }

.fr-toolbar .fr-command.fr-btn, .fr-popup .fr-command.fr-btn {
  color: #9fa8b3 !important; }

.fr-toolbar .fr-command.fr-btn.fr-active, .fr-popup .fr-command.fr-btn.fr-active {
  color: #00d2bd !important;
  background: rgba(1, 176, 188, 0.2) !important;
  border-radius: 8px; }

.fr-toolbar .fr-command.fr-btn.fr-dropdown::after, .fr-popup .fr-command.fr-btn.fr-dropdown::after {
  display: none !important; }

.fr-popup .fr-action-buttons button.fr-command {
  color: #00d2bd !important; }

.fr-popup .fr-checkbox-line {
  display: none; }

.fr-popup {
  border: 0 !important; }

.fr-popup .fr-buttons {
  display: none !important; }

.fr-popup .fr-arrow {
  border-bottom: 5px solid #9fa8b3 !important;
  top: -5px !important; }

.fr-popup.fr-mobile.fr-active {
  box-shadow: none;
  border: 1px solid #D5E0EA !important; }

button#linkList-1 {
  display: none !important; }

.fr-popup .fr-input-line input[type=text]:focus, .fr-popup .fr-input-line textarea:focus {
  border-bottom: 1px solid #00d2bd;
  box-shadow: none !important; }

.fr-popup .fr-input-line input.fr-not-empty + label, .fr-popup .fr-input-line textarea.fr-not-empty + label {
  display: none; }

.fr-popup .fr-input-line input[type=text]:focus, .fr-popup .fr-input-line textarea:focus {
  border-bottom: 2px solid #00d2bd !important; }

.fr-quick-insert.fr-visible {
  display: none !important; }

.fr-toolbar .fr-command.fr-btn i, .fr-popup .fr-command.fr-btn i, .fr-toolbar .fr-command.fr-btn svg, .fr-popup .fr-command.fr-btn svg {
  font: normal normal normal 14px/1 FontAwesome !important;
  font-size: 18px !important; }

.fr-box.fr-basic.fr-top {
  margin-top: 6px; }

.fr-popup .fr-input-line input + label, .fr-popup .fr-input-line textarea + label {
  display: none !important; }

.ios .fr-wrapper .fr-placeholder {
  padding: 16px 16px 20px !important;
  font-size: 14px !important; }

.ios .fr-toolbar .fr-command.fr-btn.fr-dropdown.fr-active:hover i, .ios .fr-popup .fr-command.fr-btn.fr-dropdown.fr-active:hover i, .ios .fr-toolbar .fr-command.fr-btn.fr-dropdown.fr-active:focus i, .ios .fr-popup .fr-command.fr-btn.fr-dropdown.fr-active:focus i {
  color: #00d2bd !important; }

.fr-element ul {
  padding-left: 20px !important; }

.fr-wrapper.show-placeholder .fr-placeholder {
  color: #B1BCC6 !important; }

.message {
  display: -webkit-box;
  display: flex; }

.message .left-item {
    flex-shrink: 0; }

.message .left-item img {
      width: 32px;
      height: 32px;
      border-radius: 7px;
      -o-object-fit: cover;
         object-fit: cover; }

.message .middle-item {
    margin-left: 12px;
    max-width: calc(100% - 85px); }

.message .middle-item .container {
      margin-bottom: 32px;
      position: relative; }

.message .middle-item .container .text-container {
        padding: 10px 12px;
        background-color: #F0F5FF;
        border-radius: 20px 20px 20px 0;
        margin-bottom: 4px; }

.message .middle-item .container .time-container {
        width: 100%;
        position: absolute;
        min-width: 125px;
        opacity: 0.7; }

.message.not-me {
    direction: rtl; }

.message.not-me .middle-item {
      margin: 0;
      margin-right: 10px;
      max-width: calc(100% - 85px); }

.message.not-me .middle-item .container .text-container {
        background-color: #00d2bd;
        text-align: left;
        color: #ffffff;
        border-radius: 20px 20px 0px 20px; }

.message.not-me .left-item {
      position: relative;
      width: 32px; }

.message.not-me .left-item img {
        position: absolute;
        bottom: 32px; }

app-card-comment:last-child .card-comment-container {
  border-bottom: 0;
  padding-bottom: 0; }

.list-boxes {
  height: 54px;
  border: 1px solid #f1f3f5;
  border-left: 0;
  border-right: 0;
  display: -webkit-box;
  display: flex;
  padding: 0 20px;
  margin-top: 20px; }

.list-boxes .list-boxes-container {
    display: -webkit-box;
    display: flex;
    -webkit-box-align: center;
            align-items: center;
    width: 100%; }

.list-boxes .list-boxes-container .item {
      width: 33.33%;
      display: -webkit-box;
      display: flex;
      -webkit-box-align: center;
              align-items: center;
      border-right: 1px solid #f1f3f5;
      -webkit-box-pack: center;
              justify-content: center; }

.list-boxes .list-boxes-container .item.start {
        -webkit-box-pack: start;
                justify-content: flex-start; }

.list-boxes .list-boxes-container .item.end {
        -webkit-box-pack: end;
                justify-content: flex-end; }

.list-boxes .list-boxes-container .item img {
        height: 20px;
        width: 20px; }

.list-boxes .list-boxes-container .item span {
        margin-left: 12px; }

.list-boxes .list-boxes-container .item:last-child {
        border: 0; }

.card-comment {
  padding: 20px;
  padding-bottom: 0; }

.card-comment .card-comment-container {
    border-bottom: 1px solid #f1f3f5;
    padding-bottom: 20px; }

.card-comment .card-comment-container .first-item {
      display: -webkit-box;
      display: flex; }

.card-comment .card-comment-container .first-item .left {
        display: -webkit-box;
        display: flex; }

.card-comment .card-comment-container .first-item .left .left-item img {
          width: 32px;
          height: 32px;
          border-radius: 7px; }

.card-comment .card-comment-container .first-item .left .right-item {
          margin-left: 12px; }

.card-comment .card-comment-container .first-item .left .right-item .small {
            font-weight: 500;
            text-transform: none;
            font-size: 8px;
            letter-spacing: 0.2px;
            color: #B1BCC6;
            margin-left: 6px;
            margin-right: 6px; }

.card-comment .card-comment-container .first-item .left .top {
          margin-top: -3px; }

.card-comment .card-comment-container .first-item .left .bottom {
          margin-top: -4px; }

.card-comment .card-comment-container .first-item .right {
        margin-left: auto; }

.card-comment .card-comment-container .second-item {
      margin-top: 16px; }

* {
  margin: 0;
  padding: 0;
  box-sizing: border-box; }

.main-profile-skeleton {
  background: #ffffff; }

.main-profile-skeleton .main-profile-skeleton-container {
    width: 100%;
    height: 230px;
    background: #F0F5FF;
    padding: 0 20px;
    position: relative; }

.main-profile-skeleton .main-profile-skeleton-container .main-profile-skeleton-search {
      padding-top: 20px;
      display: -webkit-box;
      display: flex;
      -webkit-box-align: center;
              align-items: center; }

.main-profile-skeleton .main-profile-skeleton-container .main-profile-skeleton-search .first-item {
        width: 100%;
        height: 32px;
        background: #ffffff;
        margin-right: 20px;
        border-radius: 13px; }

.main-profile-skeleton .main-profile-skeleton-container .main-profile-skeleton-search .withLeftIcon {
        width: 24px;
        height: 24px;
        border-radius: 0;
        background-image: url('Back_White.svg'); }

.main-profile-skeleton .main-profile-skeleton-container .main-profile-skeleton-search .second-item {
        display: -webkit-box;
        display: flex;
        margin-left: auto; }

.main-profile-skeleton .main-profile-skeleton-container .main-profile-skeleton-search .second-item .first-item-icon {
          height: 24px;
          width: 24px;
          border-radius: 7px;
          background: #ffffff;
          margin-right: 10px; }

.main-profile-skeleton .main-profile-skeleton-container .main-profile-skeleton-search .second-item .second-item-icon {
          height: 15px;
          width: 28px;
          border-radius: 7px;
          background: #ffffff;
          display: -webkit-box;
          display: flex; }

.main-profile-skeleton .main-profile-skeleton-container.small-height {
      padding-bottom: 56.25%;
      height: 0; }

.main-profile-skeleton .main-profile-skeleton-body {
    padding: 1px 20px; }

.main-profile-skeleton .main-profile-skeleton-body .main-profile-skeleton-body-container {
      width: 100%;
      background: #ffffff;
      margin-top: -78px;
      position: relative;
      border-radius: 13px;
      padding-top: 53px;
      padding-bottom: 16px;
      border: 1px solid #F0F5FF;
      box-shadow: 0 5px 16px 0 rgba(229, 236, 245, 0.35); }

.main-profile-skeleton .main-profile-skeleton-body .main-profile-skeleton-body-container .image {
        width: 100px;
        height: 100px;
        background: #ffffff;
        border-radius: 50%;
        margin: 0 auto;
        position: absolute;
        left: 50%;
        top: -50px;
        -webkit-transform: translate(-50%, 0);
                transform: translate(-50%, 0);
        border: 1px solid #F0F5FF; }

.main-profile-skeleton .main-profile-skeleton-body .main-profile-skeleton-body-container .txt-sekeleton-container {
        display: -webkit-box;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
                flex-direction: column;
        -webkit-box-pack: center;
                justify-content: center;
        width: 100%;
        -webkit-box-align: center;
                align-items: center; }

.main-profile-skeleton .main-profile-skeleton-body .main-profile-skeleton-body-container .txt-sekeleton-container .title {
          width: 50%;
          height: 14px;
          margin-top: 12px;
          background: #F0F5FF; }

.main-profile-skeleton .main-profile-skeleton-body .main-profile-skeleton-body-container .txt-sekeleton-container .subtitle {
          width: 30%;
          height: 11px;
          margin-top: 12px;
          background: #F0F5FF; }

.main-profile-skeleton .main-profile-skeleton-body .main-profile-skeleton-body-container .desc-container {
        display: -webkit-box;
        display: flex;
        justify-content: space-around;
        margin-top: 16px; }

.main-profile-skeleton .main-profile-skeleton-body .main-profile-skeleton-body-container .desc-container .item {
          display: -webkit-box;
          display: flex;
          -webkit-box-orient: vertical;
          -webkit-box-direction: normal;
                  flex-direction: column;
          -webkit-box-align: center;
                  align-items: center; }

.main-profile-skeleton .main-profile-skeleton-body .main-profile-skeleton-body-container .desc-container .item .image-desc {
            height: 32px;
            width: 32px;
            border-radius: 8px;
            background-color: #F0F5FF; }

.main-profile-skeleton .main-profile-skeleton-body .main-profile-skeleton-body-container .desc-container .item .image-title {
            height: 10px;
            width: 60px;
            border-radius: 2px;
            background-color: #F0F5FF;
            margin-top: 11px; }

.main-profile-skeleton .main-profile-skeleton-body .main-profile-skeleton-body-container.simple {
        padding-top: 0;
        margin: 0;
        border-radius: 0;
        border-bottom-left-radius: 13px;
        border-bottom-right-radius: 13px;
        border-top: 0; }

.main-profile-skeleton::after {
    content: &quot; &quot;;
    height: 100%;
    width: 30px;
    position: absolute;
    -webkit-animation-name: shadow;
            animation-name: shadow;
    -webkit-animation-duration: 3s;
            animation-duration: 3s;
    -webkit-animation-iteration-count: infinite;
            animation-iteration-count: infinite;
    opacity: 0.5;
    border-radius: 2px;
    background-image: -webkit-gradient(linear, left top, right top, from(white), to(rgba(255, 255, 255, 0.2)));
    background-image: linear-gradient(to right, white, rgba(255, 255, 255, 0.2));
    top: 0; }

.main-profile-skeleton-button {
  height: 50px;
  width: 100%;
  display: -webkit-box;
  display: flex; }

.main-profile-skeleton-button .first-button {
    width: calc(50% + 2px);
    border-right: 1px solid #f1f3f5;
    height: 100%;
    display: -webkit-box;
    display: flex;
    -webkit-box-align: center;
            align-items: center;
    -webkit-box-pack: center;
            justify-content: center;
    padding-top: 16px; }

.main-profile-skeleton-button .first-button .logo {
      width: 24px;
      height: 24px;
      background: #f0f5ff;
      border-radius: 7px; }

.main-profile-skeleton-button .first-button .text {
      width: 40%;
      height: 12px;
      margin-left: 10px;
      background: #f0f5ff; }

.main-profile-skeleton-button .first-button:last-child {
      width: 50%;
      border-right: 0; }

@-webkit-keyframes shadow {
  0% {
    left: 0; }
  100% {
    left: calc(100% - 30px); } }

@keyframes shadow {
  0% {
    left: 0; }
  100% {
    left: calc(100% - 30px); } }

.bar-skeleton {
  width: calc(100% - 56px);
  height: 44px;
  background-color: #F0F5FF;
  border-radius: 8px; }

.bar-skeleton::after {
    content: &quot; &quot;;
    width: 44px;
    height: 44px;
    border-radius: 8px;
    background-color: #F0F5FF;
    position: absolute;
    right: 20px; }

.profile-skeleton {
  width: 100%;
  background-color: #ffffff;
  padding: 27px 0;
  padding-bottom: 0;
  display: -webkit-box;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
          flex-direction: column;
  -webkit-box-align: center;
          align-items: center;
  position: relative; }

.profile-skeleton .profile-skeleton-container {
    width: 100%;
    display: -webkit-box;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
            flex-direction: column;
    -webkit-box-align: center;
            align-items: center;
    position: relative; }

.profile-skeleton .profile-skeleton-container .back-button {
      width: 24px;
      height: 24px;
      background-image: url('Back_Grey.svg');
      position: absolute;
      left: 0; }

.profile-skeleton .profile-skeleton-container .profile-skeleton-image {
      height: 80px;
      width: 80px;
      border-radius: 18px;
      background-color: #F0F5FF; }

.profile-skeleton .profile-skeleton-container .txt-container {
      width: 100%;
      display: -webkit-box;
      display: flex;
      -webkit-box-orient: vertical;
      -webkit-box-direction: normal;
              flex-direction: column;
      -webkit-box-align: center;
              align-items: center; }

.profile-skeleton .profile-skeleton-container .txt-container .txt {
        width: 70%;
        height: 16px;
        border-radius: 2px;
        background-color: #F0F5FF;
        margin-top: 16px;
        width: 59.8%; }

.profile-skeleton .profile-skeleton-container .txt-container .txt.subtitle {
          height: 10px;
          width: 29.9%; }

.profile-skeleton .profile-skeleton-container::after {
      content: &quot; &quot;;
      height: 100%;
      width: 30px;
      position: absolute;
      -webkit-animation-name: shadow;
              animation-name: shadow;
      -webkit-animation-duration: 3s;
              animation-duration: 3s;
      -webkit-animation-iteration-count: infinite;
              animation-iteration-count: infinite;
      opacity: 0.5;
      border-radius: 2px;
      background-image: -webkit-gradient(linear, left top, right top, from(white), to(rgba(255, 255, 255, 0.2)));
      background-image: linear-gradient(to right, white, rgba(255, 255, 255, 0.2));
      top: 0; }

@keyframes shadow {
  0% {
    left: 0; }
  100% {
    left: calc(100% - 30px); } }

.list-details {
  display: -webkit-box;
  display: flex;
  position: relative;
  width: 100%;
  margin-top: 24px; }

.list-details .list-details-item {
    display: -webkit-box;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
            flex-direction: column;
    -webkit-box-align: center;
            align-items: center;
    width: 25%; }

.list-details .list-details-item .list-image {
      height: 32px;
      width: 32px;
      border-radius: 8px;
      background-color: #F0F5FF; }

.list-details .list-details-item .gallery-list-image {
      height: 250px;
      width: 250px;
      border-radius: 8px;
      background-color: #F0F5FF; }

.list-details .list-details-item .list-txt {
      height: 10px;
      width: 60px;
      border-radius: 2px;
      background-color: #F0F5FF;
      margin-top: 11px;
      position: relative; }

.list-details::after {
    content: &quot; &quot;;
    height: 100%;
    width: 30px;
    position: absolute;
    -webkit-animation-name: listShadow;
            animation-name: listShadow;
    -webkit-animation-duration: 3s;
            animation-duration: 3s;
    -webkit-animation-iteration-count: infinite;
            animation-iteration-count: infinite;
    opacity: 0.5;
    border-radius: 2px;
    background-image: -webkit-gradient(linear, left top, right top, from(white), color-stop(rgba(255, 255, 255, 0.5)), to(rgba(255, 255, 255, 0.2)));
    background-image: linear-gradient(to right, white, rgba(255, 255, 255, 0.5), rgba(255, 255, 255, 0.2)); }

.list-details.small {
    margin-top: 16px;
    width: 100%; }

.list-details.small .list-details-item {
      width: 33.33%; }

.list-details.small .list-details-item .list-image {
        height: 20px;
        width: 20px;
        border-radius: 4px; }

@-webkit-keyframes listShadow {
  0% {
    left: 0; }
  100% {
    left: calc(100% - 10px); } }

@keyframes listShadow {
  0% {
    left: 0; }
  100% {
    left: calc(100% - 10px); } }

.list-left-image-skeleton {
  width: 100%;
  border-radius: 8px;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
          flex-direction: column;
  display: -webkit-box;
  display: flex;
  -webkit-box-align: center;
          align-items: center;
  margin-top: 22px;
  padding-bottom: 22px;
  border: 1px solid #F0F5FF; }

.list-left-image-skeleton .list-left-image-container {
    position: relative;
    display: -webkit-box;
    display: flex;
    padding: 0 16px;
    -webkit-box-align: center;
            align-items: center;
    width: 100%;
    height: 80px;
    background-color: #F0F5FF;
    border-radius: 8px 8px 0 0; }

.list-left-image-skeleton .list-left-image-container .image {
      height: 48px;
      width: 48px;
      border-radius: 8px;
      background-color: #ffffff;
      flex-shrink: 0; }

.list-left-image-skeleton .list-left-image-container .txt {
      height: 10px;
      width: 60%;
      border-radius: 2px;
      background-color: #ffffff;
      margin-left: 12px; }

.list-left-image-skeleton .list-left-image-container::after {
      content: &quot; &quot;;
      height: 100%;
      width: 30px;
      position: absolute;
      -webkit-animation-name: shadow;
              animation-name: shadow;
      -webkit-animation-duration: 3s;
              animation-duration: 3s;
      -webkit-animation-iteration-count: infinite;
              animation-iteration-count: infinite;
      opacity: 0.5;
      border-radius: 2px;
      background-image: -webkit-gradient(linear, left top, right top, from(#f0f5ff), color-stop(rgba(240, 245, 255, 0.5)), to(rgba(240, 245, 255, 0.2)));
      background-image: linear-gradient(to right, #f0f5ff, rgba(240, 245, 255, 0.5), rgba(240, 245, 255, 0.2)); }

.seperator-skeleton {
  display: -webkit-box;
  display: flex;
  height: 50px;
  border-bottom: 1px solid #f1f3f5;
  -webkit-box-align: center;
          align-items: center;
  position: relative;
  padding-bottom: 20px; }

.seperator-skeleton .left {
    display: -webkit-box;
    display: flex;
    -webkit-box-align: center;
            align-items: center; }

.seperator-skeleton .left .image {
      height: 32px;
      width: 32px;
      border-radius: 8px;
      background-color: #F0F5FF; }

.seperator-skeleton .txt {
    height: 16px;
    width: 100px;
    border-radius: 2px;
    background-color: #F0F5FF;
    margin-left: 12px; }

.seperator-skeleton .right {
    height: 28px;
    width: 71px;
    border-radius: 7px;
    background-color: #F0F5FF;
    margin-left: auto; }

.card-skeleton {
  padding-bottom: 24px;
  border-bottom: 1px solid #F0F5FF; }

.card-skeleton .card-image {
    width: 100%;
    padding-bottom: 56.25%;
    border-radius: 12px;
    background-color: #F0F5FF; }

.card-skeleton .list-container {
    position: relative; }

.card-skeleton .list-container .list-with-image {
      height: 20px;
      width: 20px;
      border-radius: 4px;
      background-color: #F0F5FF;
      margin-top: 12px;
      position: relative; }

.card-skeleton .list-container .list-with-image::after {
        content: &quot; &quot;;
        height: 10px;
        width: 140px;
        border-radius: 2px;
        background-color: #F0F5FF;
        position: absolute;
        left: 28px;
        top: 50%;
        -webkit-transform: translate(0, -50%);
                transform: translate(0, -50%); }

.card-skeleton .list-container .list {
      margin-top: 16px;
      height: 10px;
      width: 100%;
      border-radius: 2px;
      background-color: #F0F5FF;
      position: relative; }

.card-skeleton .list-container .list::after {
        content: &quot; &quot;;
        position: absolute;
        height: 10px;
        width: 77.7%;
        border-radius: 2px;
        background-color: #F0F5FF;
        top: 16px; }

.card-skeleton .list-container .list-two-text {
      height: 10px;
      width: 40px;
      border-radius: 2px;
      background-color: #F0F5FF;
      margin-top: 30px;
      position: relative; }

.card-skeleton .list-container .list-two-text::after {
        content: &quot; &quot;;
        height: 10px;
        width: 100px;
        border-radius: 2px;
        background-color: #F0F5FF;
        position: absolute;
        left: 48px; }

.card-skeleton .list-container::after {
      content: &quot; &quot;;
      height: 100%;
      width: 30px;
      position: absolute;
      -webkit-animation-name: shadow;
              animation-name: shadow;
      -webkit-animation-duration: 3s;
              animation-duration: 3s;
      -webkit-animation-iteration-count: infinite;
              animation-iteration-count: infinite;
      opacity: 0.5;
      border-radius: 2px;
      background-image: -webkit-gradient(linear, left top, right top, from(white), to(rgba(255, 255, 255, 0.2)));
      background-image: linear-gradient(to right, white, rgba(255, 255, 255, 0.2));
      top: 0; }

app-skeleton:last-child .list-right-image {
  border-bottom: 0;
  padding-bottom: 10px; }

.list-right-image {
  padding: 20px 0;
  display: -webkit-box;
  display: flex;
  border-bottom: 1px solid #f1f3f5;
  position: relative; }

.list-right-image .left {
    width: calc(100% - 96px); }

.list-right-image .left .list-with-image {
      height: 20px;
      width: 20px;
      border-radius: 4px;
      background-color: #F0F5FF;
      position: relative; }

.list-right-image .left .list-with-image::after {
        content: &quot; &quot;;
        height: 10px;
        width: 140px;
        border-radius: 2px;
        background-color: #F0F5FF;
        position: absolute;
        left: 28px;
        top: 50%;
        -webkit-transform: translate(0, -50%);
                transform: translate(0, -50%); }

.list-right-image .left .list {
      margin-top: 16px;
      height: 10px;
      width: 100%;
      border-radius: 2px;
      background-color: #F0F5FF;
      position: relative; }

.list-right-image .left .list::after {
        content: &quot; &quot;;
        position: absolute;
        height: 10px;
        width: 100%;
        border-radius: 2px;
        background-color: #F0F5FF;
        top: 16px; }

.list-right-image .left .list-two-text {
      height: 10px;
      width: 40px;
      border-radius: 2px;
      background-color: #F0F5FF;
      margin-top: 30px;
      position: relative; }

.list-right-image .left .list-two-text::after {
        content: &quot; &quot;;
        height: 10px;
        width: 100px;
        border-radius: 2px;
        background-color: #F0F5FF;
        position: absolute;
        left: 48px; }

.list-right-image .right {
    height: 80px;
    width: 80px;
    border-radius: 16px;
    background-color: #F0F5FF;
    margin-left: auto; }

.list-right-image::after {
    content: &quot; &quot;;
    height: 100%;
    width: 30px;
    position: absolute;
    -webkit-animation-name: shadow;
            animation-name: shadow;
    -webkit-animation-duration: 3s;
            animation-duration: 3s;
    -webkit-animation-iteration-count: infinite;
            animation-iteration-count: infinite;
    opacity: 0.5;
    border-radius: 2px;
    top: 0;
    background-image: -webkit-gradient(linear, left top, right top, from(white), to(rgba(255, 255, 255, 0.2)));
    background-image: linear-gradient(to right, white, rgba(255, 255, 255, 0.2)); }

.slide-list-skeleton {
  display: -webkit-box;
  display: flex;
  position: relative;
  padding-top: 10px; }

.slide-list-skeleton .left-image {
    height: 54px;
    width: 54px;
    border-radius: 12px;
    flex-shrink: 0;
    background-color: #F0F5FF;
    margin-top: 10px; }

.slide-list-skeleton .middle {
    width: 100%;
    margin-left: 10px;
    padding-bottom: 16px;
    display: -webkit-box;
    display: flex;
    -webkit-box-align: center;
            align-items: center;
    border-bottom: 1px solid #f1f3f5; }

.slide-list-skeleton .middle .list-container {
      padding-right: 18px;
      width: 100%; }

.slide-list-skeleton .middle .list-container .list {
        margin-top: 32px;
        height: 10px;
        width: 100%;
        border-radius: 2px;
        background-color: #F0F5FF;
        position: relative; }

.slide-list-skeleton .middle .list-container .list::after {
          content: &quot; &quot;;
          position: absolute;
          height: 10px;
          width: 52%;
          border-radius: 2px;
          background-color: #F0F5FF;
          top: -19px;
          left: 0; }

.slide-list-skeleton .middle .list-container .list-with-image {
        height: 20px;
        width: 20px;
        border-radius: 4px;
        background-color: #F0F5FF;
        margin-top: 8px;
        position: relative; }

.slide-list-skeleton .middle .list-container .list-with-image::after {
          content: &quot; &quot;;
          height: 10px;
          width: 140px;
          border-radius: 2px;
          background-color: #F0F5FF;
          position: absolute;
          left: 28px;
          top: 50%;
          -webkit-transform: translate(0, -50%);
                  transform: translate(0, -50%); }

.slide-list-skeleton .right {
    height: 28px;
    width: 28px;
    border-radius: 14px;
    background-color: #F0F5FF;
    flex-shrink: 0; }

.slide-list-skeleton:last-child .middle {
    border-bottom: 0; }

.slide-list-skeleton::after {
    content: &quot; &quot;;
    height: 100%;
    width: 30px;
    position: absolute;
    -webkit-animation-name: shadow;
            animation-name: shadow;
    -webkit-animation-duration: 3s;
            animation-duration: 3s;
    -webkit-animation-iteration-count: infinite;
            animation-iteration-count: infinite;
    opacity: 0.5;
    border-radius: 2px;
    background-image: -webkit-gradient(linear, left top, right top, from(white), to(rgba(255, 255, 255, 0.2)));
    background-image: linear-gradient(to right, white, rgba(255, 255, 255, 0.2));
    top: 0; }

.scroll-button-skeleton {
  padding-bottom: 0;
  overflow: hidden; }

.scroll-button-skeleton .scroll-button-skeleton-container {
    position: relative; }

.scroll-button-skeleton .scroll-button-skeleton-container .title {
      height: 16px;
      width: 48%;
      border-radius: 2px;
      background-color: #F0F5FF; }

.scroll-button-skeleton .scroll-button-skeleton-container .button-container {
      margin-top: 23px;
      display: -webkit-box;
      display: flex;
      padding-bottom: 30px; }

.scroll-button-skeleton .scroll-button-skeleton-container .button-container .button {
        height: 39px;
        width: 100px;
        border-radius: 7px;
        background-color: #F0F5FF;
        flex-shrink: 0;
        margin-right: 10px; }

.scroll-button-skeleton .scroll-button-skeleton-container::after {
      content: &quot; &quot;;
      height: 100%;
      width: 30px;
      position: absolute;
      -webkit-animation-name: shadow;
              animation-name: shadow;
      -webkit-animation-duration: 3s;
              animation-duration: 3s;
      -webkit-animation-iteration-count: infinite;
              animation-iteration-count: infinite;
      opacity: 0.5;
      border-radius: 2px;
      background-image: -webkit-gradient(linear, left top, right top, from(white), to(rgba(255, 255, 255, 0.2)));
      background-image: linear-gradient(to right, white, rgba(255, 255, 255, 0.2));
      top: 0; }

.scroll-button-skeleton.wrap .button-container {
    flex-wrap: wrap;
    margin-top: 11px; }

.scroll-button-skeleton.wrap .button-container .button {
      margin-top: 12px; }

.scroll-button-skeleton.wrap .button-container .button.width-80 {
        width: 80px; }

.big-cover-image-skeleton {
  border-bottom: 1px solid #F0F5FF; }

.big-cover-image-skeleton .big-cover-image-container {
    padding: 30px 0; }

.big-cover-image-skeleton .big-cover-image-container .cover-image {
      height: 439px;
      border-radius: 12px;
      background-color: #F0F5FF; }

.gallery-skeleton {
  display: -webkit-box;
  display: flex;
  position: relative;
  margin-top: 20px; }

.gallery-skeleton .list-details-item {
    width: 25%;
    margin-right: 10px; }

.gallery-skeleton .list-details-item .gallery-list-image {
      width: 100%;
      padding-top: 100%;
      background-color: #F0F5FF;
      margin-right: 10px;
      border-radius: 6px; }

.gallery-skeleton .list-details-item:last-child {
      margin-right: 0; }

.gallery-skeleton::after {
    content: &quot; &quot;;
    height: 100%;
    width: 30px;
    position: absolute;
    -webkit-animation-name: shadow;
            animation-name: shadow;
    -webkit-animation-duration: 3s;
            animation-duration: 3s;
    -webkit-animation-iteration-count: infinite;
            animation-iteration-count: infinite;
    opacity: 0.5;
    border-radius: 2px;
    background-image: -webkit-gradient(linear, left top, right top, from(white), to(rgba(255, 255, 255, 0.2)));
    background-image: linear-gradient(to right, white, rgba(255, 255, 255, 0.2));
    top: 0; }

.market-skeleton-container {
  display: -webkit-box;
  display: flex;
  margin-top: 20px;
  border-bottom: 1px solid #f1f3f5; }

.market-skeleton-container .market-item-skeleton {
    border: 1px solid #f1f3f5;
    height: 211px;
    width: 152px;
    border-radius: 7px;
    flex-shrink: 0;
    margin-right: 10px;
    margin-bottom: 20px; }

.market-skeleton-container .market-item-skeleton:last-child {
      margin-right: 0; }

.market-skeleton-container .market-item-skeleton .first-item {
      height: 132px;
      width: 100%;
      background-color: #F5F8FF;
      border-radius: 7px;
      border-bottom-right-radius: 0;
      border-bottom-left-radius: 0; }

.market-skeleton-container .market-item-skeleton .second-item {
      padding: 10px 13px 13px 13px;
      position: relative; }

.market-skeleton-container .market-item-skeleton .second-item .first {
        height: 14px;
        width: 100%;
        background-color: #F5F8FF; }

.market-skeleton-container .market-item-skeleton .second-item .second {
        height: 14px;
        width: 70%;
        background-color: #F5F8FF;
        margin-top: 6px; }

.market-skeleton-container .market-item-skeleton .second-item::after {
        content: &quot; &quot;;
        height: 100%;
        width: 30px;
        position: absolute;
        -webkit-animation-name: shadow;
                animation-name: shadow;
        -webkit-animation-duration: 3s;
                animation-duration: 3s;
        -webkit-animation-iteration-count: infinite;
                animation-iteration-count: infinite;
        opacity: 0.5;
        border-radius: 2px;
        background-image: -webkit-gradient(linear, left top, right top, from(white), to(rgba(255, 255, 255, 0.2)));
        background-image: linear-gradient(to right, white, rgba(255, 255, 255, 0.2));
        top: 0; }

.applyToAmbassadror {
  padding-bottom: 30px;
  border-bottom: 1px solid #f1f3f5;
  padding-bottom: 30px; }

.applyToAmbassadror .title {
    margin-top: 30px; }

.applyToAmbassadror .subtitle {
    margin-top: 10px; }

.amb-content {
  padding: 0 20px;
  padding-bottom: 20px; }

.amb-content .header-amb {
    text-align: center;
    padding-bottom: 30px;
    border-bottom: 1px solid #f1f3f5; }

.amb-content .header-amb .title {
      margin-top: 30px; }

.amb-content .header-amb .subtitle {
      margin-top: 10px;
      margin-bottom: 30px; }

.amb-content .header-amb .image {
      width: 64px; }

.amb-content .list {
    display: -webkit-box;
    display: flex;
    -webkit-box-align: start;
            align-items: flex-start;
    margin-top: 30px; }

.amb-content .list img {
      margin-right: 12px; }

ion-tab-bar {
  height: 50px; }

ion-tab-button {
  --padding-start: 0px;
  --padding-end: 0px;
  --ripple-color: #00d2bd; }

.tab-button-icon {
  position: relative;
  width: 20px;
  height: 24px; }

.tab-button-icon img {
    width: 20px;
    height: 24px; }

.tab-button-icon img.active {
      position: absolute;
      left: 0;
      top: 0;
      display: none; }

.ios .tab-button-icon {
  height: 26px; }

.tab-selected .tab-button-icon img {
  display: none; }

.tab-selected .tab-button-icon img.active {
    display: block; }

.tab-selected .bottom-bar-inactive {
  color: #00d2bd; }

.tab-container-icon {
  position: relative; }

.tab-container-icon .hasNotification.message {
    height: 18px;
    width: 18px;
    border-radius: 50%;
    border: 1.5px solid #FFFFFF;
    background-color: #FE4A49;
    position: absolute;
    display: -webkit-box;
    display: flex;
    -webkit-box-pack: center;
            justify-content: center;
    -webkit-box-align: center;
            align-items: center;
    right: -6px;
    top: -4px; }

.icon-container {
  position: relative; }

.icon-container .hasNotification.bar-notification {
    height: 18px;
    width: 18px;
    border-radius: 50%;
    border: 1.5px solid #FFFFFF;
    background-color: #FE4A49;
    position: absolute;
    display: -webkit-box;
    display: flex;
    -webkit-box-pack: center;
            justify-content: center;
    -webkit-box-align: center;
            align-items: center;
    right: -6px;
    top: -6px; }

.ios .tab-container-icon {
  position: relative; }

.ios .tab-container-icon .hasNotification.message {
    padding-left: 1px; }

.notification-title {
  width: 100%;
  background: #ffffff;
  z-index: 10; }

.notification-title-container {
  display: -webkit-box;
  display: flex;
  -webkit-box-align: center;
          align-items: center;
  padding: 20px; }

.notification-title-container .not-small-title-container {
    display: -webkit-box;
    display: flex;
    -webkit-box-align: center;
            align-items: center;
    margin-left: auto; }

.notification-title-container .not-small-title-container .not-small-title {
      margin-left: 20px; }

.settings {
  padding: 0 20px; }

.settings .setting-not {
    display: -webkit-box;
    display: flex;
    border-bottom: 1px solid #f1f3f5; }

.settings .setting-not .setting-container {
      margin-left: auto;
      display: -webkit-box;
      display: flex;
      height: 22px;
      padding-right: 8px; }

.settings .setting-not .setting-container .container {
        margin-left: 35px;
        flex-shrink: 0;
        width: 18px; }

.unreadNotification {
  background-color: #F5F8FF !important; }

.app-status {
  display: -webkit-box;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
          flex-direction: column;
  -webkit-box-align: center;
          align-items: center;
  -webkit-box-pack: center;
          justify-content: center;
  height: 100%;
  padding: 0 20px; }

.app-status .title {
    margin-top: 20px;
    text-align: center; }

.app-status .subtitle {
    margin-top: 8px;
    margin-bottom: 30px;
    text-align: center; }

.app-status app-button {
    width: 100%; }

.about {
  display: -webkit-box;
  display: flex; }

.about .about-us-image {
    width: 49%;
    padding-top: 50%;
    background-position: center;
    background-size: 90%;
    background-repeat: no-repeat; }

.swiper-container-horizontal > .swiper-pagination-bullets, .swiper-pagination-custom, .swiper-pagination-fraction {
  position: relative !important;
  bottom: -5px;
  padding-top: 20px;
  padding-bottom: 0; }

.swiper-container-horizontal > .swiper-pagination-bullets .swiper-pagination-bullet, .swiper-pagination-custom .swiper-pagination-bullet, .swiper-pagination-fraction .swiper-pagination-bullet {
    opacity: 1; }

.swiper-container-horizontal > .swiper-pagination-bullets .swiper-pagination-bullet-active, .swiper-pagination-custom .swiper-pagination-bullet-active, .swiper-pagination-fraction .swiper-pagination-bullet-active {
    background: #00d2bd; }

.fr-toolbar .fr-command.fr-btn svg path, .fr-popup .fr-command.fr-btn svg path, .fr-modal .fr-command.fr-btn svg path {
  fill: #9fa8b3 !important; }

.fr-toolbar .fr-tabs .fr-command.fr-btn, .fr-popup .fr-tabs .fr-command.fr-btn, .fr-modal .fr-tabs .fr-command.fr-btn {
  display: none !important; }

.ios ion-footer ion-toolbar:last-child {
  padding: 0  !important; }

.blackBody .ios ion-footer ion-toolbar:last-child {
  padding-bottom: var(--ion-safe-area-bottom, 0) !important; }

.center-slide {
  padding-bottom: 10px; }

.center-slide .carousel-cell {
    width: 186px;
    height: 146px;
    opacity: 0.5; }

.center-slide .swiper-slide-active .carousel-cell {
    width: 186px;
    height: 146px;
    opacity: 1;
    -webkit-transform: scale(1.1);
            transform: scale(1.1);
    -webkit-transition: -webkit-transform .2s ease-out;
    transition: -webkit-transform .2s ease-out;
    transition: transform .2s ease-out;
    transition: transform .2s ease-out, -webkit-transform .2s ease-out; }

.center-slide .swiper-slide-active .card-text {
    top: 42px !important; }

.center-slide .swiper-slide {
    width: 184px !important; }

.filter {
  -webkit-filter: url(#duotone);
          filter: url(#duotone); }

/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9AaW9uaWMvYW5ndWxhci9zcmMvY3NzL2NvcmUuc2NzcyIsIm5vZGVfbW9kdWxlcy9AaW9uaWMvYW5ndWxhci9jc3MvY29yZS5jc3MiLCJub2RlX21vZHVsZXMvQGlvbmljL2FuZ3VsYXIvc3JjL3RoZW1lcy9pb25pYy5taXhpbnMuc2NzcyIsIm5vZGVfbW9kdWxlcy9AaW9uaWMvYW5ndWxhci9zcmMvdGhlbWVzL2lvbmljLmdsb2JhbHMuc2NzcyIsIm5vZGVfbW9kdWxlcy9AaW9uaWMvYW5ndWxhci9zcmMvY29tcG9uZW50cy9tZW51L21lbnUuaW9zLnZhcnMuc2NzcyIsIm5vZGVfbW9kdWxlcy9AaW9uaWMvYW5ndWxhci9zcmMvY29tcG9uZW50cy9tZW51L21lbnUubWQudmFycy5zY3NzIiwibm9kZV9tb2R1bGVzL0Bpb25pYy9hbmd1bGFyL3NyYy9jc3Mvbm9ybWFsaXplLnNjc3MiLCJub2RlX21vZHVsZXMvQGlvbmljL2FuZ3VsYXIvY3NzL25vcm1hbGl6ZS5jc3MiLCJub2RlX21vZHVsZXMvQGlvbmljL2FuZ3VsYXIvc3JjL2Nzcy9zdHJ1Y3R1cmUuc2NzcyIsIm5vZGVfbW9kdWxlcy9AaW9uaWMvYW5ndWxhci9jc3Mvc3RydWN0dXJlLmNzcyIsIm5vZGVfbW9kdWxlcy9AaW9uaWMvYW5ndWxhci9zcmMvY3NzL3R5cG9ncmFwaHkuc2NzcyIsIm5vZGVfbW9kdWxlcy9AaW9uaWMvYW5ndWxhci9jc3MvdHlwb2dyYXBoeS5jc3MiLCJub2RlX21vZHVsZXMvQGlvbmljL2FuZ3VsYXIvc3JjL2Nzcy9kaXNwbGF5LnNjc3MiLCJub2RlX21vZHVsZXMvQGlvbmljL2FuZ3VsYXIvY3NzL2Rpc3BsYXkuY3NzIiwibm9kZV9tb2R1bGVzL0Bpb25pYy9hbmd1bGFyL3NyYy9jc3MvcGFkZGluZy5zY3NzIiwibm9kZV9tb2R1bGVzL0Bpb25pYy9hbmd1bGFyL2Nzcy9wYWRkaW5nLmNzcyIsIm5vZGVfbW9kdWxlcy9AaW9uaWMvYW5ndWxhci9zcmMvY3NzL2Zsb2F0LWVsZW1lbnRzLnNjc3MiLCJub2RlX21vZHVsZXMvQGlvbmljL2FuZ3VsYXIvY3NzL2Zsb2F0LWVsZW1lbnRzLmNzcyIsIm5vZGVfbW9kdWxlcy9AaW9uaWMvYW5ndWxhci9zcmMvY3NzL3RleHQtYWxpZ25tZW50LnNjc3MiLCJub2RlX21vZHVsZXMvQGlvbmljL2FuZ3VsYXIvY3NzL3RleHQtYWxpZ25tZW50LmNzcyIsIm5vZGVfbW9kdWxlcy9AaW9uaWMvYW5ndWxhci9zcmMvY3NzL3RleHQtdHJhbnNmb3JtYXRpb24uc2NzcyIsIm5vZGVfbW9kdWxlcy9AaW9uaWMvYW5ndWxhci9jc3MvdGV4dC10cmFuc2Zvcm1hdGlvbi5jc3MiLCJub2RlX21vZHVsZXMvQGlvbmljL2FuZ3VsYXIvc3JjL2Nzcy9mbGV4LXV0aWxzLnNjc3MiLCJub2RlX21vZHVsZXMvQGlvbmljL2FuZ3VsYXIvY3NzL2ZsZXgtdXRpbHMuY3NzIiwic3JjL0M6XFxVc2Vyc1xcYWJsYXliZWxcXERlc2t0b3BcXERpYXNwb3JhIHY0XFxEaWFzcG9yYSB2OCAyMDIwIC0gQ29kZSBwdXNoL3NyY1xcYXNzZXRzXFxzY3NzXFx0eXBvZ3JhcGh5LnNjc3MiLCJzcmMvQzpcXFVzZXJzXFxhYmxheWJlbFxcRGVza3RvcFxcRGlhc3BvcmEgdjRcXERpYXNwb3JhIHY4IDIwMjAgLSBDb2RlIHB1c2gvc3JjXFxhc3NldHNcXHNjc3NcXHZhcmlhYmxlLnNjc3MiLCJzcmMvQzpcXFVzZXJzXFxhYmxheWJlbFxcRGVza3RvcFxcRGlhc3BvcmEgdjRcXERpYXNwb3JhIHY4IDIwMjAgLSBDb2RlIHB1c2gvc3JjXFxhc3NldHNcXHNjc3NcXGJ1dHRvbi5zY3NzIiwic3JjL0M6XFxVc2Vyc1xcYWJsYXliZWxcXERlc2t0b3BcXERpYXNwb3JhIHY0XFxEaWFzcG9yYSB2OCAyMDIwIC0gQ29kZSBwdXNoL3NyY1xcYXNzZXRzXFxzY3NzXFxzZXBhcmF0b3JzLnNjc3MiLCJzcmMvQzpcXFVzZXJzXFxhYmxheWJlbFxcRGVza3RvcFxcRGlhc3BvcmEgdjRcXERpYXNwb3JhIHY4IDIwMjAgLSBDb2RlIHB1c2gvc3JjXFxhc3NldHNcXHNjc3NcXGxpc3Quc2NzcyIsInNyYy9DOlxcVXNlcnNcXGFibGF5YmVsXFxEZXNrdG9wXFxEaWFzcG9yYSB2NFxcRGlhc3BvcmEgdjggMjAyMCAtIENvZGUgcHVzaC9zcmNcXGFzc2V0c1xcc2Nzc1xcbmF2YmFyLnNjc3MiLCJzcmMvQzpcXFVzZXJzXFxhYmxheWJlbFxcRGVza3RvcFxcRGlhc3BvcmEgdjRcXERpYXNwb3JhIHY4IDIwMjAgLSBDb2RlIHB1c2gvc3JjXFxhc3NldHNcXHNjc3NcXGNhcmQuc2NzcyIsInNyYy9DOlxcVXNlcnNcXGFibGF5YmVsXFxEZXNrdG9wXFxEaWFzcG9yYSB2NFxcRGlhc3BvcmEgdjggMjAyMCAtIENvZGUgcHVzaC9zcmNcXGFzc2V0c1xcc2Nzc1xcaGludHMuc2NzcyIsInNyYy9DOlxcVXNlcnNcXGFibGF5YmVsXFxEZXNrdG9wXFxEaWFzcG9yYSB2NFxcRGlhc3BvcmEgdjggMjAyMCAtIENvZGUgcHVzaC9zcmNcXGFzc2V0c1xcc2Nzc1xcZ2VuZXJhbC5zY3NzIiwic3JjL0M6XFxVc2Vyc1xcYWJsYXliZWxcXERlc2t0b3BcXERpYXNwb3JhIHY0XFxEaWFzcG9yYSB2OCAyMDIwIC0gQ29kZSBwdXNoL3NyY1xcYXNzZXRzXFxzY3NzXFxpbnB1dC5zY3NzIiwic3JjL0M6XFxVc2Vyc1xcYWJsYXliZWxcXERlc2t0b3BcXERpYXNwb3JhIHY0XFxEaWFzcG9yYSB2OCAyMDIwIC0gQ29kZSBwdXNoL3NyY1xcYXNzZXRzXFxzY3NzXFxpbnZpdGUuc2NzcyIsInNyYy9DOlxcVXNlcnNcXGFibGF5YmVsXFxEZXNrdG9wXFxEaWFzcG9yYSB2NFxcRGlhc3BvcmEgdjggMjAyMCAtIENvZGUgcHVzaC9zcmNcXGFzc2V0c1xcc2Nzc1xcZm9udC5zY3NzIiwic3JjL0M6XFxVc2Vyc1xcYWJsYXliZWxcXERlc2t0b3BcXERpYXNwb3JhIHY0XFxEaWFzcG9yYSB2OCAyMDIwIC0gQ29kZSBwdXNoL3NyY1xcYXNzZXRzXFxzY3NzXFxzcGFjaW5nLnNjc3MiLCJzcmMvQzpcXFVzZXJzXFxhYmxheWJlbFxcRGVza3RvcFxcRGlhc3BvcmEgdjRcXERpYXNwb3JhIHY4IDIwMjAgLSBDb2RlIHB1c2gvc3JjXFxhc3NldHNcXHNjc3NcXHNlZ21lbnQtYnV0dG9uLnNjc3MiLCJzcmMvQzpcXFVzZXJzXFxhYmxheWJlbFxcRGVza3RvcFxcRGlhc3BvcmEgdjRcXERpYXNwb3JhIHY4IDIwMjAgLSBDb2RlIHB1c2gvc3JjXFxhc3NldHNcXHNjc3NcXGdhbGxlcnkuc2NzcyIsInNyYy9DOlxcVXNlcnNcXGFibGF5YmVsXFxEZXNrdG9wXFxEaWFzcG9yYSB2NFxcRGlhc3BvcmEgdjggMjAyMCAtIENvZGUgcHVzaC9zcmNcXGFzc2V0c1xcc2Nzc1xcYWN0aW9uU2hlZXQuc2NzcyIsInNyYy9DOlxcVXNlcnNcXGFibGF5YmVsXFxEZXNrdG9wXFxEaWFzcG9yYSB2NFxcRGlhc3BvcmEgdjggMjAyMCAtIENvZGUgcHVzaC9zcmNcXGFzc2V0c1xcc2Nzc1xcYnV0dG9uLXR5cG9ncmFwaHkuc2NzcyIsInNyYy9DOlxcVXNlcnNcXGFibGF5YmVsXFxEZXNrdG9wXFxEaWFzcG9yYSB2NFxcRGlhc3BvcmEgdjggMjAyMCAtIENvZGUgcHVzaC9zcmNcXGFzc2V0c1xcc2Nzc1xcaW52aXNpb24tdHlwb2dyYXBoeS5zY3NzIiwic3JjL0M6XFxVc2Vyc1xcYWJsYXliZWxcXERlc2t0b3BcXERpYXNwb3JhIHY0XFxEaWFzcG9yYSB2OCAyMDIwIC0gQ29kZSBwdXNoL3NyY1xcYXNzZXRzXFxzY3NzXFxwYWdlc1xcbGFuZGluZC1wYWdlLnNjc3MiLCJzcmMvQzpcXFVzZXJzXFxhYmxheWJlbFxcRGVza3RvcFxcRGlhc3BvcmEgdjRcXERpYXNwb3JhIHY4IDIwMjAgLSBDb2RlIHB1c2gvc3JjXFxhc3NldHNcXHNjc3NcXHBhZ2VzXFxsb2dpbi1wYWdlLnNjc3MiLCJzcmMvQzpcXFVzZXJzXFxhYmxheWJlbFxcRGVza3RvcFxcRGlhc3BvcmEgdjRcXERpYXNwb3JhIHY4IDIwMjAgLSBDb2RlIHB1c2gvc3JjXFxhc3NldHNcXHNjc3NcXGVtcHR5LXN0YXRlLnNjc3MiLCJzcmMvQzpcXFVzZXJzXFxhYmxheWJlbFxcRGVza3RvcFxcRGlhc3BvcmEgdjRcXERpYXNwb3JhIHY4IDIwMjAgLSBDb2RlIHB1c2gvc3JjXFxhc3NldHNcXHNjc3NcXGFsZXJ0LWNvbnRyb2wuc2NzcyIsInNyYy9DOlxcVXNlcnNcXGFibGF5YmVsXFxEZXNrdG9wXFxEaWFzcG9yYSB2NFxcRGlhc3BvcmEgdjggMjAyMCAtIENvZGUgcHVzaC9zcmNcXGFzc2V0c1xcc2Nzc1xcYm9yZGVycy5zY3NzIiwic3JjL0M6XFxVc2Vyc1xcYWJsYXliZWxcXERlc2t0b3BcXERpYXNwb3JhIHY0XFxEaWFzcG9yYSB2OCAyMDIwIC0gQ29kZSBwdXNoL3NyY1xcYXNzZXRzXFxzY3NzXFxjb21wb25lbnQuc2NzcyIsInNyYy9DOlxcVXNlcnNcXGFibGF5YmVsXFxEZXNrdG9wXFxEaWFzcG9yYSB2NFxcRGlhc3BvcmEgdjggMjAyMCAtIENvZGUgcHVzaC9zcmNcXGFzc2V0c1xcc2Nzc1xcZWRpdG9yLnNjc3MiLCJzcmMvQzpcXFVzZXJzXFxhYmxheWJlbFxcRGVza3RvcFxcRGlhc3BvcmEgdjRcXERpYXNwb3JhIHY4IDIwMjAgLSBDb2RlIHB1c2gvc3JjXFxhc3NldHNcXHNjc3NcXGNvbnZlcnNhdGlvbi5zY3NzIiwic3JjL0M6XFxVc2Vyc1xcYWJsYXliZWxcXERlc2t0b3BcXERpYXNwb3JhIHY0XFxEaWFzcG9yYSB2OCAyMDIwIC0gQ29kZSBwdXNoL3NyY1xcYXNzZXRzXFxzY3NzXFxjb21tZW50LnNjc3MiLCJzcmMvQzpcXFVzZXJzXFxhYmxheWJlbFxcRGVza3RvcFxcRGlhc3BvcmEgdjRcXERpYXNwb3JhIHY4IDIwMjAgLSBDb2RlIHB1c2gvc3JjXFxhc3NldHNcXHNjc3NcXHNrZWxldG9uLnNjc3MiLCJzcmMvQzpcXFVzZXJzXFxhYmxheWJlbFxcRGVza3RvcFxcRGlhc3BvcmEgdjRcXERpYXNwb3JhIHY4IDIwMjAgLSBDb2RlIHB1c2gvc3JjXFxhc3NldHNcXHNjc3NcXGFtYmFzc2Fkb3Iuc2NzcyIsInNyYy9DOlxcVXNlcnNcXGFibGF5YmVsXFxEZXNrdG9wXFxEaWFzcG9yYSB2NFxcRGlhc3BvcmEgdjggMjAyMCAtIENvZGUgcHVzaC9zcmNcXGFzc2V0c1xcc2Nzc1xcbWFpbi10YWJzLnNjc3MiLCJzcmMvQzpcXFVzZXJzXFxhYmxheWJlbFxcRGVza3RvcFxcRGlhc3BvcmEgdjRcXERpYXNwb3JhIHY4IDIwMjAgLSBDb2RlIHB1c2gvc3JjXFxhc3NldHNcXHNjc3NcXG5vdGlmaWNhdGlvbnMuc2NzcyIsInNyYy9DOlxcVXNlcnNcXGFibGF5YmVsXFxEZXNrdG9wXFxEaWFzcG9yYSB2NFxcRGlhc3BvcmEgdjggMjAyMCAtIENvZGUgcHVzaC9zcmNcXGFzc2V0c1xcc2Nzc1xcYXBwLXN0YXR1cy5zY3NzIiwic3JjL0M6XFxVc2Vyc1xcYWJsYXliZWxcXERlc2t0b3BcXERpYXNwb3JhIHY0XFxEaWFzcG9yYSB2OCAyMDIwIC0gQ29kZSBwdXNoL3NyY1xcYXNzZXRzXFxzY3NzXFxhYm91dC5zY3NzIiwic3JjL0M6XFxVc2Vyc1xcYWJsYXliZWxcXERlc2t0b3BcXERpYXNwb3JhIHY0XFxEaWFzcG9yYSB2OCAyMDIwIC0gQ29kZSBwdXNoL3NyY1xcZ2xvYmFsLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBUUE7RUFDRSw2RkFBQTtBQ1BGOztBRFNBO0VBQ0UsMERBQUE7QUNORjs7QURTQTtFQUNFLDBDQUFBO0FDTkY7O0FEU0E7RUFDRSx1Q0FBQTtBQ05GOztBRFNBO0VBQ0UsZ0JBQUE7QUNORjs7QURxQ0U7RUFUQSw4REFBQTtFQUNBLDJFQUFBO0VBQ0Esd0VBQUE7RUFDQSx5RkFBQTtFQUNBLHFFQUFBO0VBQ0EsbUVBQUE7QUN4QkY7O0FENEJFO0VBVEEsZ0VBQUE7RUFDQSw2RUFBQTtFQUNBLDBFQUFBO0VBQ0EsMkZBQUE7RUFDQSx1RUFBQTtFQUNBLHFFQUFBO0FDZkY7O0FEbUJFO0VBVEEsK0RBQUE7RUFDQSw0RUFBQTtFQUNBLHlFQUFBO0VBQ0EsMEZBQUE7RUFDQSxzRUFBQTtFQUNBLG9FQUFBO0FDTkY7O0FEVUU7RUFUQSw4REFBQTtFQUNBLDBFQUFBO0VBQ0Esd0VBQUE7RUFDQSx5RkFBQTtFQUNBLHFFQUFBO0VBQ0EsbUVBQUE7QUNHRjs7QURDRTtFQVRBLDhEQUFBO0VBQ0EsMEVBQUE7RUFDQSx3RUFBQTtFQUNBLHlGQUFBO0VBQ0EscUVBQUE7RUFDQSxtRUFBQTtBQ1lGOztBRFJFO0VBVEEsNkRBQUE7RUFDQSx5RUFBQTtFQUNBLHVFQUFBO0VBQ0Esd0ZBQUE7RUFDQSxvRUFBQTtFQUNBLGtFQUFBO0FDcUJGOztBRGpCRTtFQVRBLDREQUFBO0VBQ0EsMEVBQUE7RUFDQSxzRUFBQTtFQUNBLGlGQUFBO0VBQ0EsbUVBQUE7RUFDQSxpRUFBQTtBQzhCRjs7QUQxQkU7RUFUQSw2REFBQTtFQUNBLDJFQUFBO0VBQ0EsdUVBQUE7RUFDQSx3RkFBQTtFQUNBLG9FQUFBO0VBQ0Esa0VBQUE7QUN1Q0Y7O0FEbkNFO0VBVEEsMkRBQUE7RUFDQSxzRUFBQTtFQUNBLHFFQUFBO0VBQ0Esc0ZBQUE7RUFDQSxrRUFBQTtFQUNBLGdFQUFBO0FDZ0RGOztBRG5DQTtFRTZOTSxPRjVOdUI7RUU2TnZCLFFGN05pQjtFRXNQckIsTUZ0UGtCO0VFdVBsQixTRnZQd0I7RUFFeEIsb0JBQUE7RUFBQSxhQUFBO0VBQ0Esa0JBQUE7RUFFQSw0QkFBQTtFQUFBLDZCQUFBO1VBQUEsc0JBQUE7RUFDQSx5QkFBQTtVQUFBLDhCQUFBO0VBRUEsMEJBQUE7RUFDQSxnQkFBQTtFQUNBLFVHekIrQjtBRitEakM7O0FEbkNBOzs7Ozs7Ozs7Ozs7Ozs7RUFlRSx5REFBQTtFQUNBLHdCQUFBO0FDc0NGOztBRG5DQTtFQUNFLFVBQUE7QUNzQ0Y7O0FEaENBO0VBQ0UsNkJBQUE7QUNtQ0Y7O0FEaENBO0VBQ0U7SUFDRSxpREFBQTtFQ21DRjtBQUNGOztBRC9CQTtFQUNFO0lBQ0Usa0RBQUE7SUFDQSx3REFBQTtJQUNBLG9EQUFBO0lBQ0Esc0RBQUE7RUNpQ0Y7QUFDRjs7QUQ5QkE7RUFDRTtJQUNFLDZDQUFBO0lBQ0EsbURBQUE7SUFDQSwrQ0FBQTtJQUNBLGlEQUFBO0VDZ0NGO0FBQ0Y7O0FEekJBO0VFaVZNLHlDQUFBO1VBQUEsaUNBQUE7QURyVE47O0FEeEJBO0VBQ0UsZUFBQTtFQUNBLDBCQUFBO0VBSUEsb0JBQUE7QUN3QkY7O0FEckJBO0VBQ0UsMkNJM0krQjtBSG1LakM7O0FEckJBO0VBQ0UsMENJNUkrQjtBSG9LakM7O0FEakJBO0VBQ0UsOEVLMUo4QjtBSjhLaEM7O0FEakJBO0VBQ0UsOEVLOUo4QjtBSmtMaEM7O0FLcExBOzs7O0VBSUUsd0JBQUE7QUNORjs7QURXQTtFQUNFLGFBQUE7RUFFQSxTQUFBO0FDVEY7O0FEaUJBOztFQUVFLGlCQUFBO0FDZEY7O0FEc0JBO0VBQ0UsZUFBQTtFQUVBLFNBQUE7QUNwQkY7O0FEd0JBO0VBQ0UsZ0JBQUE7QUNyQkY7O0FENkJBO0VBQ0UsZ0JBQUE7QUMxQkY7O0FENkJBO0VBQ0UsV0FBQTtFQUVBLGVBQUE7RUFFQSx1QkFBQTtBQzVCRjs7QURnQ0E7RUFDRSxjQUFBO0FDN0JGOztBRGlDQTs7OztFQUlFLGlDQUFBO0VBQ0EsY0FBQTtBQzlCRjs7QUQ4Q0E7Ozs7RUFJRSxvQkFBQTtFQUNBLG1CQUFBO0FDM0NGOztBRDhDQTtFQUNFLGNBQUE7RUFFQSxZQUFBO0VBRUEsYUFBQTtFQUNBLGNBQUE7QUM3Q0Y7O0FEZ0RBO0VBQ0UsaUJBQUE7QUM3Q0Y7O0FENENBO0VBQ0UsaUJBQUE7QUM3Q0Y7O0FENENBO0VBQ0UsaUJBQUE7QUM3Q0Y7O0FENENBO0VBQ0UsaUJBQUE7QUM3Q0Y7O0FEZ0RBOzs7O0VBSUUsU0FBQTtFQUVBLGFBQUE7RUFDQSxjQUFBO0FDOUNGOztBRHNEQTs7O0VBR0UsZUFBQTtFQUVBLDBCQUFBO0FDcERGOztBRHdEQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0VBa0JFLDBCQUFBO0FDckRGOztBRHdEQTs7RUFFRSxvQkFBQTtBQ3JERjs7QUR3REE7RUFDRSxTQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQkFBQTtFQUNBLG1CQUFBO0VBQ0EscUJBQUE7RUFDQSxjQUFBO0VBQ0Esb0JBQUE7RUFDQSxlQUFBO0VBRUEsMEJBQUE7QUN0REY7O0FEeURBO0VBQ0UsZUFBQTtBQ3RERjs7QUQwREE7OztFQUdFLGVBQUE7QUN2REY7O0FEMkRBOztFQUVFLFVBQUE7RUFFQSxTQUFBO0FDekRGOztBRCtEQTs7RUFFRSxVQUFBO0VBRUEsc0JBQUE7QUM3REY7O0FEbUVBOztFQUVFLFlBQUE7QUNoRUY7O0FEc0VBOztFQUVFLHdCQUFBO0FDbkVGOztBRDJFQTtFQUNFLHlCQUFBO0VBQ0EsaUJBQUE7QUN4RUY7O0FEMkVBOztFQUVFLFVBQUE7QUN4RUY7O0FDeEpBO0VBQ0Usc0JBQUE7RUFFQSw2Q0FBQTtFQUNBLHdDQUFBO0VBQ0EsMkJBQUE7QUNURjs7QURZQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBRUEsOEJBQUE7S0FBQSwyQkFBQTtNQUFBLDBCQUFBO1VBQUEsc0JBQUE7QUNWRjs7QURhQTtFQUNFLGFBQUE7QUNWRjs7QURhQTtFQUNFLGFBQUE7QUNWRjs7QURhQTtFTlNFLGtDQUFBO0VBQ0EsbUNBQUE7RUErSkUsY012S2M7RU53S2QsZU14S2M7RU40TWhCLGFNNU1nQjtFTjZNaEIsZ0JNN01nQjtFTnVLZCxlTXRLZTtFTnVLZixnQk12S2U7RU4yTWpCLGNNM01pQjtFTjRNakIsaUJNNU1pQjtFQUVqQixlQUFBO0VBRUEsV0FBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFFQSxrQ0FBQTtFQUVBLGdCQUFBO0VBRUEsMEJBQUE7RUFFQSx1QkFBQTtFQUVBLHlCQUFBO0VBRUEscUJBQUE7RUFFQSwyQkFBQTtFQUVBLDhCQUFBO0tBQUEsMkJBQUE7TUFBQSwwQkFBQTtVQUFBLHNCQUFBO0FDYkY7O0FDZEE7RUFDRSxtQ0FBQTtBQzlCRjs7QURpQ0E7RUFDRSw2QkFBQTtFQUNBLHdDQUFBO0FDOUJGOztBRGlDQTs7Ozs7O0VSc01FLGdCUWhNZ0I7RVJpTWhCLG1CUWpNNEI7RUFFNUIsZ0JBeEM2QjtFQTBDN0IsZ0JBdkM2QjtBQ1EvQjs7QURrQ0E7RVJ5TEUsZ0JReExnQjtFQUVoQixlQTFDNkI7QUNTL0I7O0FEb0NBO0VSbUxFLGdCUWxMZ0I7RUFFaEIsZUE3QzZCO0FDVS9COztBRHNDQTtFQUNFLGVBOUM2QjtBQ1UvQjs7QUR1Q0E7RUFDRSxlQS9DNkI7QUNXL0I7O0FEdUNBO0VBQ0UsZUFoRDZCO0FDWS9COztBRHVDQTtFQUNFLGVBakQ2QjtBQ2EvQjs7QUR1Q0E7RUFDRSxjQUFBO0FDcENGOztBRHVDQTs7RUFFRSxrQkFBQTtFQUVBLGNBQUE7RUFFQSxjQUFBO0VBRUEsd0JBQUE7QUN2Q0Y7O0FEMENBO0VBQ0UsV0FBQTtBQ3ZDRjs7QUQwQ0E7RUFDRSxlQUFBO0FDdkNGOztBQ3REQTtFQUNFLHdCQUFBO0FDUEY7O0FEaUJJO0VBQ0Usd0JBQUE7QUNkTjs7QVgwSEk7RVVyR0E7SUFDRSx3QkFBQTtFQ2pCSjtBQUNGOztBWGdGSTtFVXpFQTtJQUNFLHdCQUFBO0VDSko7QUFDRjs7QVgrR0k7RVVyR0E7SUFDRSx3QkFBQTtFQ1BKO0FBQ0Y7O0FYc0VJO0VVekVBO0lBQ0Usd0JBQUE7RUNNSjtBQUNGOztBWHFHSTtFVXJHQTtJQUNFLHdCQUFBO0VDR0o7QUFDRjs7QVg0REk7RVV6RUE7SUFDRSx3QkFBQTtFQ2dCSjtBQUNGOztBWDJGSTtFVXJHQTtJQUNFLHdCQUFBO0VDYUo7QUFDRjs7QVhrREk7RVV6RUE7SUFDRSx3QkFBQTtFQzBCSjtBQUNGOztBRHBCSTtFQUNFLHdCQUFBO0FDc0JOOztBQ2xDQTs7RUFFRSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFWm9MRSxlWWxMZTtFWm1MZixnQlluTGU7RVp1TmpCLGNZdk5pQjtFWndOakIsaUJZeE5pQjtBQ2JuQjs7QURnQkE7O0VBRUUseUNBQUE7RUFDQSx1Q0FBQTtFQUNBLHVDQUFBO0VBQ0EsMENBQUE7RVo4S0Usc0NZbk1NO0Vab01OLHVDWXBNTTtFWm9PUixxQ1lwT1E7RVpxT1Isd0NZck9RO0FDWVY7O0FiMkxNO0VBQ0U7O0lBRUksbUJBQUE7SUFHQSxvQkFBQTtJQUdGLCtDWWhOQTtJWmlOQSw4Q1lqTkE7SVprTkEsNkNZbE5BO0labU5BLDRDWW5OQTtFQ3NCUjtBQUNGOztBREdBOztFQUVFLHVDQUFBO0Vad01BLHFDWXBPUTtBQzZCVjs7QURJQTs7RUFFRSx5Q0FBQTtFWmdLRSxzQ1luTU07QUNrQ1Y7O0FicUtNO0VBQ0U7O0lBRUksbUJBQUE7SUFNRiwrQ1loTkE7SVppTkEsOENZak5BO0VDeUNSO0FBQ0Y7O0FERkE7O0VBRUUsdUNBQUE7RVowSkUsdUNZcE1NO0FDZ0RWOztBYnVKTTtFQUNFOztJQUtJLG9CQUFBO0lBS0YsNkNZbE5BO0labU5BLDRDWW5OQTtFQ3VEUjtBQUNGOztBRFRBOztFQUVFLDBDQUFBO0Vab0xBLHdDWXJPUTtBQzhEVjs7QURSQTs7RUFFRSx1Q0FBQTtFQUNBLDBDQUFBO0VaMktBLHFDWXBPUTtFWnFPUix3Q1lyT1E7QUNxRVY7O0FEUEE7O0VBRUUseUNBQUE7RUFDQSx1Q0FBQTtFWmtJRSxzQ1luTU07RVpvTU4sdUNZcE1NO0FDNEVWOztBYjJITTtFQUNFOztJQUVJLG1CQUFBO0lBR0Esb0JBQUE7SUFHRiwrQ1loTkE7SVppTkEsOENZak5BO0laa05BLDZDWWxOQTtJWm1OQSw0Q1luTkE7RUNzRlI7QUFDRjs7QURiQTs7RUFFRSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RVpnSEUsY1k5R2M7RVorR2QsZVkvR2M7RVptSmhCLGFZbkpnQjtFWm9KaEIsZ0JZcEpnQjtBQ2tCbEI7O0FEZkE7O0VBRUUsdUNBQUE7RUFDQSxxQ0FBQTtFQUNBLHFDQUFBO0VBQ0Esd0NBQUE7RVowR0Usb0NZbE1LO0VabU1MLHFDWW5NSztFWm1PUCxtQ1luT087RVpvT1Asc0NZcE9PO0FDOEdUOztBYndGTTtFQUNFOztJQUVJLGtCQUFBO0lBR0EsbUJBQUE7SUFHRiw2Q1kvTUQ7SVpnTkMsNENZaE5EO0laaU5DLDJDWWpORDtJWmtOQywwQ1lsTkQ7RUN3SFA7QUFDRjs7QUQ1QkE7O0VBRUUscUNBQUE7RVpvSUEsbUNZbk9PO0FDK0hUOztBRDNCQTs7RUFFRSx1Q0FBQTtFWjRGRSxvQ1lsTUs7QUNvSVQ7O0Fia0VNO0VBQ0U7O0lBRUksa0JBQUE7SUFNRiw2Q1kvTUQ7SVpnTkMsNENZaE5EO0VDMklQO0FBQ0Y7O0FEakNBOztFQUVFLHFDQUFBO0Vac0ZFLHFDWW5NSztBQ2tKVDs7QWJvRE07RUFDRTs7SUFLSSxtQkFBQTtJQUtGLDJDWWpORDtJWmtOQywwQ1lsTkQ7RUN5SlA7QUFDRjs7QUR4Q0E7O0VBRUUsd0NBQUE7RVpnSEEsc0NZcE9PO0FDZ0tUOztBRHZDQTs7RUFFRSxxQ0FBQTtFQUNBLHdDQUFBO0VadUdBLG1DWW5PTztFWm9PUCxzQ1lwT087QUN1S1Q7O0FEdENBOztFQUVFLHVDQUFBO0VBQ0EscUNBQUE7RVo4REUsb0NZbE1LO0VabU1MLHFDWW5NSztBQzhLVDs7QWJ3Qk07RUFDRTs7SUFFSSxrQkFBQTtJQUdBLG1CQUFBO0lBR0YsNkNZL01EO0laZ05DLDRDWWhORDtJWmlOQywyQ1lqTkQ7SVprTkMsMENZbE5EO0VDd0xQO0FBQ0Y7O0FDdExJOztFZGdYRSxzQkFBQTtBZTNYTjs7QURnQkk7O0VkMldFLHVCQUFBO0FldFhOOztBRGdCSTs7RWR3VkUsc0JBQUE7QWVuV047O0FmZ0pXO0VBc05MLHVCQUFBO0FlbldOOztBRGFJOztFZDBWRSx1QkFBQTtBZWxXTjs7QWZ3SVc7RUE2Tkwsc0JBQUE7QWVsV047O0Fmb0VJO0VjOUVBOztJZGdYRSxzQkFBQTtFZWhXSjs7RURYRTs7SWQyV0UsdUJBQUE7RWUzVko7O0VEWEU7O0lkd1ZFLHNCQUFBO0VleFVKO0VmcUhTO0lBc05MLHVCQUFBO0VleFVKOztFRGRFOztJZDBWRSx1QkFBQTtFZXZVSjtFZjZHUztJQTZOTCxzQkFBQTtFZXZVSjtBQUNGOztBZndDSTtFYzlFQTs7SWRnWEUsc0JBQUE7RWVyVUo7O0VEdENFOztJZDJXRSx1QkFBQTtFZWhVSjs7RUR0Q0U7O0lkd1ZFLHNCQUFBO0VlN1NKO0VmMEZTO0lBc05MLHVCQUFBO0VlN1NKOztFRHpDRTs7SWQwVkUsdUJBQUE7RWU1U0o7RWZrRlM7SUE2Tkwsc0JBQUE7RWU1U0o7QUFDRjs7QWZhSTtFYzlFQTs7SWRnWEUsc0JBQUE7RWUxU0o7O0VEakVFOztJZDJXRSx1QkFBQTtFZXJTSjs7RURqRUU7O0lkd1ZFLHNCQUFBO0VlbFJKO0VmK0RTO0lBc05MLHVCQUFBO0VlbFJKOztFRHBFRTs7SWQwVkUsdUJBQUE7RWVqUko7RWZ1RFM7SUE2Tkwsc0JBQUE7RWVqUko7QUFDRjs7QWZkSTtFYzlFQTs7SWRnWEUsc0JBQUE7RWUvUUo7O0VENUZFOztJZDJXRSx1QkFBQTtFZTFRSjs7RUQ1RkU7O0lkd1ZFLHNCQUFBO0VldlBKO0Vmb0NTO0lBc05MLHVCQUFBO0VldlBKOztFRC9GRTs7SWQwVkUsdUJBQUE7RWV0UEo7RWY0QlM7SUE2Tkwsc0JBQUE7RWV0UEo7QUFDRjs7QUN2SEk7O0VBRUUsNkJBQUE7QUNiTjs7QURnQkk7O0VBRUUsOEJBQUE7QUNiTjs7QURnQkk7O0VBRUUsNEJBQUE7QUNiTjs7QURnQkk7O0VBRUUsMEJBQUE7QUNiTjs7QURnQkk7O0VBRUUsMkJBQUE7QUNiTjs7QURnQkk7O0VBRUUsNEJBQUE7QUNiTjs7QURnQkk7O0VBRUUsOEJBQUE7QUNiTjs7QURnQkk7O0VBRUUsOEJBQUE7QUNiTjs7QWpCc0RJO0VnQjlFQTs7SUFFRSw2QkFBQTtFQzRCSjs7RUR6QkU7O0lBRUUsOEJBQUE7RUM0Qko7O0VEekJFOztJQUVFLDRCQUFBO0VDNEJKOztFRHpCRTs7SUFFRSwwQkFBQTtFQzRCSjs7RUR6QkU7O0lBRUUsMkJBQUE7RUM0Qko7O0VEekJFOztJQUVFLDRCQUFBO0VDNEJKOztFRHpCRTs7SUFFRSw4QkFBQTtFQzRCSjs7RUR6QkU7O0lBRUUsOEJBQUE7RUM0Qko7QUFDRjs7QWpCWUk7RWdCOUVBOztJQUVFLDZCQUFBO0VDcUVKOztFRGxFRTs7SUFFRSw4QkFBQTtFQ3FFSjs7RURsRUU7O0lBRUUsNEJBQUE7RUNxRUo7O0VEbEVFOztJQUVFLDBCQUFBO0VDcUVKOztFRGxFRTs7SUFFRSwyQkFBQTtFQ3FFSjs7RURsRUU7O0lBRUUsNEJBQUE7RUNxRUo7O0VEbEVFOztJQUVFLDhCQUFBO0VDcUVKOztFRGxFRTs7SUFFRSw4QkFBQTtFQ3FFSjtBQUNGOztBakI3Qkk7RWdCOUVBOztJQUVFLDZCQUFBO0VDOEdKOztFRDNHRTs7SUFFRSw4QkFBQTtFQzhHSjs7RUQzR0U7O0lBRUUsNEJBQUE7RUM4R0o7O0VEM0dFOztJQUVFLDBCQUFBO0VDOEdKOztFRDNHRTs7SUFFRSwyQkFBQTtFQzhHSjs7RUQzR0U7O0lBRUUsNEJBQUE7RUM4R0o7O0VEM0dFOztJQUVFLDhCQUFBO0VDOEdKOztFRDNHRTs7SUFFRSw4QkFBQTtFQzhHSjtBQUNGOztBakJ0RUk7RWdCOUVBOztJQUVFLDZCQUFBO0VDdUpKOztFRHBKRTs7SUFFRSw4QkFBQTtFQ3VKSjs7RURwSkU7O0lBRUUsNEJBQUE7RUN1Sko7O0VEcEpFOztJQUVFLDBCQUFBO0VDdUpKOztFRHBKRTs7SUFFRSwyQkFBQTtFQ3VKSjs7RURwSkU7O0lBRUUsNEJBQUE7RUN1Sko7O0VEcEpFOztJQUVFLDhCQUFBO0VDdUpKOztFRHBKRTs7SUFFRSw4QkFBQTtFQ3VKSjtBQUNGOztBQzdMSTs7RUFFRSx5REFBQTtFQUNBLG9DQUFBO0FDYk47O0FEZ0JJOztFQUVFLHlEQUFBO0VBQ0Esb0NBQUE7QUNiTjs7QURnQkk7O0VBRUUseURBQUE7RUFDQSxxQ0FBQTtBQ2JOOztBbkI0RUk7RWtCOUVBOztJQUVFLHlEQUFBO0lBQ0Esb0NBQUE7RUNNSjs7RURIRTs7SUFFRSx5REFBQTtJQUNBLG9DQUFBO0VDTUo7O0VESEU7O0lBRUUseURBQUE7SUFDQSxxQ0FBQTtFQ01KO0FBQ0Y7O0FuQndESTtFa0I5RUE7O0lBRUUseURBQUE7SUFDQSxvQ0FBQTtFQ3lCSjs7RUR0QkU7O0lBRUUseURBQUE7SUFDQSxvQ0FBQTtFQ3lCSjs7RUR0QkU7O0lBRUUseURBQUE7SUFDQSxxQ0FBQTtFQ3lCSjtBQUNGOztBbkJxQ0k7RWtCOUVBOztJQUVFLHlEQUFBO0lBQ0Esb0NBQUE7RUM0Q0o7O0VEekNFOztJQUVFLHlEQUFBO0lBQ0Esb0NBQUE7RUM0Q0o7O0VEekNFOztJQUVFLHlEQUFBO0lBQ0EscUNBQUE7RUM0Q0o7QUFDRjs7QW5Ca0JJO0VrQjlFQTs7SUFFRSx5REFBQTtJQUNBLG9DQUFBO0VDK0RKOztFRDVERTs7SUFFRSx5REFBQTtJQUNBLG9DQUFBO0VDK0RKOztFRDVERTs7SUFFRSx5REFBQTtJQUNBLHFDQUFBO0VDK0RKO0FBQ0Y7O0FDckZBOztFQUVFLGlDQUFBO0FDUEY7O0FEVUE7O0VBRUUsK0JBQUE7QUNQRjs7QURVQTs7RUFFRSw2QkFBQTtBQ1BGOztBRFVBOztFQUVFLDhCQUFBO0FDUEY7O0FEVUE7O0VBRUUsK0JBQUE7QUNQRjs7QURVQTs7RUFFRSwyQkFBQTtBQ1BGOztBRGNBOztFQUVFLDBCQUFBO0FDWEY7O0FEY0E7O0VBRUUsNEJBQUE7QUNYRjs7QURjQTs7RUFFRSxrQ0FBQTtBQ1hGOztBRGtCQTs7RUFFRSxrQ0FBQTtVQUFBLHNDQUFBO0FDZkY7O0FEa0JBOztFQUVFLG1DQUFBO1VBQUEsa0NBQUE7QUNmRjs7QURrQkE7O0VBRUUsZ0NBQUE7VUFBQSxvQ0FBQTtBQ2ZGOztBRGtCQTs7RUFFRSx3Q0FBQTtBQ2ZGOztBRGtCQTs7RUFFRSxvQ0FBQTtVQUFBLHlDQUFBO0FDZkY7O0FEa0JBOztFQUVFLHlDQUFBO1VBQUEsd0NBQUE7QUNmRjs7QURzQkE7O0VBRUUsbUNBQUE7VUFBQSxrQ0FBQTtBQ25CRjs7QURzQkE7O0VBRUUsb0NBQUE7VUFBQSw4QkFBQTtBQ25CRjs7QURzQkE7O0VBRUUsaUNBQUE7VUFBQSxnQ0FBQTtBQ25CRjs7QURzQkE7O0VBRUUscUNBQUE7VUFBQSwrQkFBQTtBQ25CRjs7QURzQkE7O0VBRUUsc0NBQUE7VUFBQSxnQ0FBQTtBQ25CRjs7QUNoR0E7RUFDRSxjQ2tCb0I7RURqQnBCLGVBQWU7RUFDZixxQkFBcUI7RUFDckIsaUJBQWlCO0VBQ2pCLGNDbUJzQjtFRGxCdEIsZ0JBQWdCLEVBQUE7O0FBTmxCO0lBUUksY0NSaUIsRUFBQTs7QURZckI7RUFDRSxjQ2JtQjtFRGNuQixlQUFlO0VBQ2YscUJBQXFCO0VBQ3JCLGlCQUFpQjtFQUNqQixnQkFBZ0IsRUFBQTs7QUFLbEI7RUFDQyxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLHFCQUFxQjtFQUNyQixpQkFBaUI7RUFDakIsY0NDc0IsRUFBQTs7QUROdkI7SUFPRSxjQ0FxQixFQUFBOztBQy9CdkI7RUFDSSxjQUFlLEVBQUE7O0FBRG5CO0lBSVksa0JBQWtCLEVBQUE7O0FBSjlCO0lBUWdCLGlCQUFpQixFQUFBOztBQUtqQztFQUNJLFVBQVU7RUFDVixlQUFlLEVBQUE7O0FBRm5CO0lBSVEsbUJBQW1CO0lBQ25CLGVBQWUsRUFBQTs7QUFMdkI7TUFPWSxrQkFBa0IsRUFBQTs7QUFQOUI7TUFVWSxXQUFXO01BQ1gsY0FBYyxFQUFBOztBQVgxQjtJQWdCUSxXQUFXLEVBQUE7O0FBaEJuQjtJQW1CUSxrQkFBa0IsRUFBQTs7QUFuQjFCO0lBc0JRLGlCQUFpQixFQUFBOztBQUd6QjtFQUNJLFlBQVk7RUFDWixXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLG9CQUFhO0VBQWIsYUFBYTtFQUNiLHdCQUF1QjtVQUF2Qix1QkFBdUI7RUFDdkIseUJBQW1CO1VBQW5CLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2YsY0FBYyxFQUFBOztBQVJsQjtJQVdRLFlBQVk7SUFDWixjQUFjLEVBQUE7O0FBWnRCO0lBZ0JRLHlCRGpEYztJQ2tEZCxjRGJhLEVBQUE7O0FDSnJCO01Bb0JnQix5QkFBeUI7TUFDekIsZ0JBQWdCO01BQ2hCLG9CQUFvQjtNQUNwQixlQUFlO01BQ2YscUJBQXFCO01BQ3JCLGlCQUFpQjtNQUNqQixZQUE2QixFQUFBOztBQTFCN0M7SUErQlEseUJEN0RlLEVBQUE7O0FDOEJ2QjtJQWtDUSx5QkRuRWM7SUNvRWQsY0QvQmEsRUFBQTs7QUNKckI7SUFzQ1EsNkJBQTZCLEVBQUE7O0FBdENyQztJQXlDUSx5QkQzQmlCLEVBQUE7O0FDZHpCO0lBNENRLHlCRHBEZSxFQUFBOztBQ1F2QjtJQStDUSx5QkQzQ2EsRUFBQTs7QUNKckI7SUFrRFEseUNBQXNDO0lBQ3RDLHlCRHBGYztJQ3FGZCxtQkRoRGEsRUFBQTs7QUNKckI7TUF1RGdCLGlCQUFpQjtNQUNqQixnQkFBZ0I7TUFDaEIsb0JBQW9CO01BQ3BCLGVBQWU7TUFDZixxQkFBcUI7TUFDckIsaUJBQWlCO01BQ2pCLGNBQWMsRUFBQTs7QUE3RDlCO0lBa0VRLHlCRDlEYTtJQytEYix5QkRwR2M7SUNxR2QsY0RyR2M7SUNzR2QsWUFBWSxFQUFBOztBQXJFcEI7TUF3RVkscUJEdkdXO01Dd0dYLGNEeEdXO01DeUdYLCtDQUErQyxFQUFBOztBQTFFM0Q7TUE2RVkscUJEaEVhO01DaUViLGNEakVhO01Da0ViLCtDQUErQyxFQUFBOztBQS9FM0Q7TUFrRlkscUJEbEhXO01DbUhYLGNEbkhXO01Db0hYLCtDQUErQyxFQUFBOztBQXBGM0Q7SUF3RlEsV0FBVztJQUNYLGdCQUFnQixFQUFBOztBQXpGeEI7SUE0RlEsV0FBVztJQUNYLGlCQUFpQixFQUFBOztBQTdGekI7SUFnR1EsZUFBZTtJQUNmLHlCRHRHZTtJQ3VHZixjRG5GYTtJQ29GYixtQkFBbUI7SUFDbkIsZUFBZSxFQUFBOztBQXBHdkI7TUFzR1ksV0FBVztNQUNYLGlCQUFpQixFQUFBOztBQXZHN0I7TUEwR1ksZ0JBQWdCLEVBQUE7O0FBMUc1QjtJQThHUSxZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixXQUFXLEVBQUE7O0FBR25CO0VBQ0ksWUFBWTtFQUNaLHlCRGxIaUI7RUNtSGpCLGtCQUFrQjtFQUNsQixvQkFBYTtFQUFiLGFBQWE7RUFDYix3QkFBdUI7VUFBdkIsdUJBQXVCO0VBQ3ZCLHlCQUFtQjtVQUFuQixtQkFBbUI7RUFDbkIsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsb0NEdkltQjtFQ3dJbkIseUJEeEltQjtFQ3lJbkIsZ0JBQWdCO0VBQ2hCLDhCQUFtQztFQUNuQyxrQkFBa0I7RUFDbEIsbUJBQW1CLEVBQUE7O0FBZnZCO0lBaUJRLFdBQVc7SUFDWCxnQkFBZ0IsRUFBQTs7QUFsQnhCO0lBcUJRLFdBQVc7SUFDWCxpQkFBaUIsRUFBQTs7QUF0QnpCO0lBNkJRLFNBQVMsRUFBQTs7QUE3QmpCO0lBZ0NRLGlCQUFpQjtJQUNqQixlQUFlLEVBQUE7O0FBSXZCO0VBQ0ksaUJBQWlCLEVBQUE7O0FBRHJCO0lBR1EsZ0JBQWdCO0lBQ2hCLGtCQUFrQixFQUFBOztBQUcxQjtFQUNJLGdCQUFnQjtFQUNoQix1QkFBdUI7RUFDdkIsb0JBQW9CO0VBQ3BCLHFCQUFxQjtFQUNyQiw4QkFBQTtFQUNBLDRCQUE0QixFQUFBOztBQUVoQztFQUNJLG1CQUFtQjtFQUNuQixjQUFjO0VBQ2QsdUJBQXVCO0VBQ3ZCLGdCQUFnQjtFQUNoQixtQkFBbUIsRUFBQTs7QUFFdkI7RUFDSSxrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLFdBQVc7RUFDWCx1QkFBdUIsRUFBQTs7QUN6TjNCO0VBQ0UsZ0NGNEJxQixFQUFBOztBRTdCdkI7SUFHSSxpQkFBaUIsRUFBQTs7QUFIckI7SUFNSSxlQUFlLEVBQUE7O0FBSW5CO0VBQ0Usb0JBQWE7RUFBYixhQUFhO0VBRWIsbUJBQW1CO0VBQ25CLHlCRjRCbUIsRUFBQTs7QUVoQ3JCO0lBTUksYUFBYSxFQUFBOztBQU5qQjtJQVVJLG9CQUFhO0lBQWIsYUFBYTtJQUNiLDRCQUFzQjtJQUF0Qiw2QkFBc0I7WUFBdEIsc0JBQXNCO0lBQ3RCLHdCQUF1QjtZQUF2Qix1QkFBdUI7SUFDdkIsZ0JBQWdCLEVBQUE7O0FBYnBCO01BZU0sV0FBVztNQUNYLGlCQUFpQixFQUFBOztBQWhCdkI7SUFxQkksaUJBQWlCO0lBQ2pCLFlBQVk7SUFDWixjQUFjO0lBQ2QsbUJBQW1CO0lBQ25CLG9CQUFhO0lBQWIsYUFBYTtJQUNiLHdCQUF1QjtZQUF2Qix1QkFBdUI7SUFDdkIseUJBQW1CO1lBQW5CLG1CQUFtQjtJQUNuQiwwQkFBMEI7SUFDMUIsNEJBQXNCO0lBQXRCLDZCQUFzQjtZQUF0QixzQkFBc0I7SUFDdEIsY0FBYyxFQUFBOztBQTlCbEI7TUFrQ00sV0FBVztNQUNYLGdCQUFnQixFQUFBOztBQW5DdEI7TUFzQ00sNkJBQXFCO2NBQXJCLHFCQUFxQixFQUFBOztBQXRDM0I7SUE0RE0sV0FBVztJQUNYLFlBQVk7SUFDWixlQUFlLEVBQUE7O0FBOURyQjtJQWlFTSxTQUFTO0lBQ1QsaUJBQWlCLEVBQUE7O0FBbEV2QjtJQXNFSSxrQkFBa0I7SUFDbEIsaUJBQWlCO0lBQ2pCLDRCQUE0QjtJQUM1QixpQkFBaUIsRUFBQTs7QUFJckI7RUFFSSxnQkFBZ0I7RUFDaEIsa0JBQWtCLEVBQUE7O0FBR3RCO0VBQ0Usb0JBQWE7RUFBYixhQUFhO0VBQ2IsaUJBQWlCO0VBQ2pCLHlCQUFtQjtVQUFuQixtQkFBbUIsRUFBQTs7QUFIckI7SUFLSSxrREFBa0QsRUFBQTs7QUFMdEQ7SUFRSSxpQkFBaUI7SUFDakIsb0JBQWE7SUFBYixhQUFhLEVBQUE7O0FBVGpCO01BWU0sV0FBVztNQUNYLGdCQUFnQixFQUFBOztBQWJ0QjtJQWlCSSxlQUFlLEVBQUE7O0FBS25CO0VBQ0UsV0FBVztFQUNYLFlBQVk7RUFDWixrRUFBa0U7RUFDbEUsNEJBQTRCLEVBQUE7O0FDdEg5QjtFQUlRLFNBQVMsRUFBQTs7QUFKakI7RUFPUSxTQUFTLEVBQUE7O0FBUGpCO0VBV00sU0FBUyxFQUFBOztBQVhmO0VBY00sZ0JBQWdCLEVBQUE7O0FBZHRCO0VBa0JRLGdCQUFnQixFQUFBOztBQUt4QjtFQUlRLFNBQVM7RUFDVCxvQkFBb0IsRUFBQTs7QUFMNUI7RUFTTSxTQUFTLEVBQUE7O0FBVGY7SUFXUSwyQkFBMkIsRUFBQTs7QUFNbkM7RUFHTSxTQUFTO0VBQ1QsbUJBQW1CO0VBQ25CLG1CQUFtQixFQUFBOztBQUx6QjtFQVNRLG9CQUFxQixFQUFBOztBQUs3QjtFQUlRLFNBQVMsRUFBQTs7QUFNakI7RUFJUSxTQUFTO0VBQ1Qsb0JBQXFCLEVBQUE7O0FBTDdCO0VBUVEsb0JBQW9CLEVBQUE7O0FBTzVCO0VBQ0UsY0FBYztFQUNkLGlCQUFpQjtFQUNqQixjQUFjLEVBQUE7O0FBSGhCO0lBS0ksVUFBVSxFQUFBOztBQU1kO0VBRUksZ0JBQWdCLEVBQUE7O0FBRnBCO0VBTUksb0JBQWE7RUFBYixhQUFhO0VBQ2IsZ0JBQWdCO0VBQ2hCLG1CSHpEaUIsRUFBQTs7QUdpRHJCO0lBVU0sZ0JBQWdCO0lBQ2hCLHVCQUF1QjtJQUN2QixvQkFBb0I7SUFDcEIscUJBQXFCO0lBQ3JCLDhCQUFBO0lBQ0EsNEJBQTRCLEVBQUE7O0FBZmxDO0lBa0JNLGNBQWM7SUFDZCxZQUFZO0lBQ1osV0FBVztJQUNYLHlCSG5GaUI7SUdvRmpCLG1CQUFtQjtJQUNuQix5QkhyRmlCLEVBQUE7O0FHOER2QjtNQXlCUSxXQUFXO01BQ1gsWUFBWTtNQUNaLGtCQUFrQjtNQUNsQixvQkFBaUI7U0FBakIsaUJBQWlCLEVBQUE7O0FBNUJ6QjtNQStCUSxtQkFBbUIsRUFBQTs7QUEvQjNCO0lBbUNNLGlCQUFpQjtJQUNqQixnQ0hsR2lCO0lHbUdqQixnQkFBZ0I7SUFDaEIsd0JBQXdCO0lBQ3hCLG9CQUFvQjtJQUNwQixnQkFBZ0I7SUFDaEIsbUJBQW1CLEVBQUE7O0FBekN6QjtNQTJDUSxVQUFVO01BQ1YsZ0JBQWdCO01BQ2hCLGVBQWU7TUFDZixnQkFBZ0I7TUFDaEIsdUJBQXVCO01BQ3ZCLG9CQUFvQjtNQUNwQixxQkFBcUI7TUFDckIsOEJBQUE7TUFDQSw0QkFBNEIsRUFBQTs7QUFuRHBDO01Bc0RRLFdBQVc7TUFDWCxtQkFBbUIsRUFBQTs7QUF2RDNCO0lBMkRNLGlCQUFpQjtJQUNqQixnQkFBZ0I7SUFDaEIsY0FBYztJQUNkLDJDQUFrRCxFQUFBOztBQTlEeEQ7TUFnRVEsY0FBYztNQUNkLGFBQWEsRUFBQTs7QUFqRXJCO01Bb0VRLDJCQUEyQjtNQUMzQixrQkFBa0IsRUFBQTs7QUFyRTFCO1FBdUVVLFlBQVk7UUFDWixXQUFXLEVBQUE7O0FBeEVyQjtNQTZFUSwyQkFBMkIsRUFBQTs7QUE3RW5DO01BZ0ZRLGtCQUFrQixFQUFBOztBQWhGMUI7UUFrRlUsWUFBWTtRQUNaLGtCQUFrQjtRQUNsQixZQUFZO1FBQ1osVUFBVTtRQUNWLHlCQUF5QjtRQUN6QixXQUFXLEVBQUE7O0FBdkZyQjtJQTZGTSx5QkFBbUI7WUFBbkIsbUJBQW1CO0lBQ25CLGVBQWUsRUFBQTs7QUE5RnJCO01BaUdRLGFBQWEsRUFBQTs7QUFqR3JCO01BcUdRLG9CQUFhO01BQWIsYUFBYTtNQUNiLHlCQUFtQjtjQUFuQixtQkFBbUI7TUFDbkIsVUFBVTtNQUNWLGdCQUFnQixFQUFBOztBQXhHeEI7UUEwR1UsZ0JBQWdCLEVBQUE7O0FBMUcxQjtRQTZHVSxXQUFXO1FBQ1gsaUJBQWlCO1FBQ2pCLGNBQWMsRUFBQTs7QUEvR3hCO0lBb0hNLGVBQWUsRUFBQTs7QUFwSHJCO0lBd0hRLG9CQUFhO0lBQWIsYUFBYTtJQUNiLHdCQUF1QjtZQUF2Qix1QkFBdUI7SUFDdkIsNEJBQXNCO0lBQXRCLDZCQUFzQjtZQUF0QixzQkFBc0I7SUFDdEIsaUJBQWlCLEVBQUE7O0FBM0h6QjtJQThIUSxnQkFBZ0IsRUFBQTs7QUFLeEI7RUFFSSxnQkFBZ0I7RUFDaEIsdUJBQXVCO0VBQ3ZCLG9CQUFvQjtFQUNwQixnQ0FBZ0M7RUFDaEMsOEJBQUE7RUFDQSw0QkFBNEIsRUFBQTs7QUFHaEM7RUFFSSxnQkFBZ0I7RUFDaEIsdUJBQXVCO0VBQ3ZCLG9CQUFvQjtFQUNwQixnQ0FBZ0M7RUFDaEMsOEJBQUE7RUFDQSw0QkFBNEIsRUFBQTs7QUFQaEM7RUFVSSwwQkFBMEIsRUFBQTs7QUFWOUI7RUFjTSx3QkFBd0I7RUFDeEIsNEJBQTRCLEVBQUE7O0FBT2xDO0VBQ0Usb0JBQWE7RUFBYixhQUFhO0VBQ2IsY0FBYztFQUNkLGdCQUFnQjtFQUNoQixnQ0hyT3FCO0VHc09yQix5Qkh6Tm1CO0VHME5uQixnQkFBZ0I7RUFDaEIsb0JBQW9CLEVBQUE7O0FBUHRCO0lBU0ksV0FBWSxFQUFBOztBQVRoQjtNQVdNLG9CQUFhO01BQWIsYUFBYTtNQUNiLHlCQUFtQjtjQUFuQixtQkFBbUI7TUFDbkIsa0JBQWtCLEVBQUE7O0FBYnhCO1FBZVEsZ0JBQWdCO1FBQ2hCLHVCQUF1QjtRQUN2QixvQkFBb0I7UUFDcEIscUJBQXFCO1FBQ3JCLDhCQUFBO1FBQ0EsNEJBQTRCO1FBQzVCLG1CQUFtQjtRQUNuQixnQkFBZ0I7UUFDaEIsV0FBVyxFQUFBOztBQXZCbkI7TUEyQk0sa0JBQWtCO01BQ2xCLGdCQUFnQjtNQUNoQix1QkFBdUI7TUFDdkIsb0JBQW9CO01BQ3BCLHFCQUFxQjtNQUNyQiw4QkFBQTtNQUNBLDRCQUE0QjtNQUM1QixVQUFVLEVBQUE7O0FBbENoQjtJQXdDSSxjQUFjO0lBQ2QsaUJBQWlCO0lBQ2pCLFlBQVk7SUFDWixXQUFXO0lBQ1gseUJIN1FtQjtJRzhRbkIsbUJBQW1CLEVBQUE7O0FBN0N2QjtNQStDTSxZQUFZO01BQ1osV0FBVztNQUNYLG1CQUFtQjtNQUNuQixvQkFBaUI7U0FBakIsaUJBQWlCLEVBQUE7O0FBbER2QjtJQXNESSxZQUFZO0lBQ1osV0FBVztJQUNYLGtCQUFrQjtJQUNsQixvQkFBaUI7T0FBakIsaUJBQWlCO0lBQ2pCLHlCSDNSbUIsRUFBQTs7QUcrUnZCO0VBQ0UsV0FBVztFQUNYLGtCQUFrQjtFQUNsQix5QkhyUm1CO0VHc1JuQixvQ0huU3FCO0VHb1NyQix5QkhwU3FCO0VHcVNyQixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLG9CQUFhO0VBQWIsYUFBYSxFQUFBOztBQVJmO0lBVUksWUFBWTtJQUNaLFdBQVc7SUFDWCxjQUFjLEVBQUE7O0FBWmxCO01BY00sV0FBVztNQUNYLFlBQVk7TUFDWixvQkFBaUI7U0FBakIsaUJBQWlCLEVBQUE7O0FBaEJ2QjtJQW9CSSxpQkFBaUI7SUFDakIsZ0JBQWdCO0lBQ2hCLGtCQUFrQixFQUFBOztBQXRCdEI7TUF3Qk0sZ0JBQWdCO01BQ2hCLHVCQUF1QjtNQUN2QixvQkFBb0I7TUFDcEIscUJBQXFCO01BQ3JCLDhCQUFBO01BQ0EsNEJBQTRCLEVBQUE7O0FBN0JsQztNQWdDTSxlQUFlO01BQ2YsZ0JBQWdCO01BQ2hCLHVCQUF1QjtNQUN2QixvQkFBb0I7TUFDcEIscUJBQXFCO01BQ3JCLDhCQUFBO01BQ0EsNEJBQTRCLEVBQUE7O0FBdENsQztJQTBDSSxpQkFBaUI7SUFDakIsb0JBQWE7SUFBYixhQUFhO0lBQ2IseUJBQW1CO1lBQW5CLG1CQUFtQjtJQUNuQixXQUFXLEVBQUE7O0FBN0NmO01BK0NNLFdBQVc7TUFDWCxZQUFZLEVBQUE7O0FBaERsQjtJQW9ESSxrQkFBa0IsRUFBQTs7QUFwRHRCO01Bc0RNLFlBQVk7TUFDWixXQUFXO01BQ1gsY0FBYyxFQUFBOztBQXhEcEI7UUEwRFEsV0FBVztRQUNYLFlBQVk7UUFDWixvQkFBaUI7V0FBakIsaUJBQWlCO1FBQ2pCLGtCQUFrQixFQUFBOztBQTdEMUI7TUFpRU0sV0FBVyxFQUFBOztBQWpFakI7UUFtRVEsV0FBVztRQUNYLFlBQVksRUFBQTs7QUFNcEI7RUFDRSxnQ0gxV3FCO0VHMldyQixvQkFBb0IsRUFBQTs7QUFGdEI7SUFJSSxnQkFBZ0IsRUFBQTs7QUFKcEI7SUFPSSwyQkFBMkI7SUFDM0Isb0JBQW9CLEVBQUE7O0FBUnhCO0lBV0ksZ0JBQWdCLEVBQUE7O0FBWHBCO0lBY0ksb0JBQWE7SUFBYixhQUFhO0lBQ2IsZ0JBQWdCO0lBQ2hCLHlCQUFtQjtZQUFuQixtQkFBbUIsRUFBQTs7QUFoQnZCO01BbUJNLFdBQVc7TUFDWCxZQUFZO01BQ1osa0JBQWtCLEVBQUE7O0FBckJ4QjtNQWdDUSxrQkFBa0IsRUFBQTs7QUFoQzFCO0lBcUNJLGlCQUFpQjtJQUNqQixvQkFBYTtJQUFiLGFBQWE7SUFDYixjQUFjLEVBQUE7O0FBdkNsQjtNQXlDTSxvQkFBYTtNQUFiLGFBQWE7TUFDYix5QkFBbUI7Y0FBbkIsbUJBQW1CLEVBQUE7O0FBMUN6QjtRQTRDUSxzQkFBc0I7UUFDdEIsWUFBWTtRQUNaLGlCQUFpQixFQUFBOztBQTlDekI7TUFrRE0sV0FBVztNQUNYLFlBQVk7TUFDWixlQUFlO01BQ2YsaUJBQWlCLEVBQUE7O0FBckR2QjtJQXlESSxZQUFZO0lBQ1osYUFBYTtJQUNiLGlCQUFpQjtJQUNqQixrQkFBa0I7SUFDbEIsY0FBYztJQUNkLGdCQUFnQixFQUFBOztBQTlEcEI7TUFnRU0sV0FBVztNQUNYLFlBQVk7TUFDWixrQkFBa0I7TUFDbEIsb0JBQWlCO1NBQWpCLGlCQUFpQixFQUFBOztBQW5FdkI7TUFzRU0sV0FBVztNQUNYLFlBQVk7TUFDWiwwQkFBMEI7TUFDMUIsb0NBQW9DO01BQ3BDLGNIaGJpQjtNR2liakIsZUFBZTtNQUNmLGdCQUFnQjtNQUNoQixxQkFBcUI7TUFDckIsaUJBQWlCO01BQ2pCLGtCQUFrQjtNQUNsQixrQkFBa0I7TUFDbEIsU0FBUztNQUNULG9CQUFhO01BQWIsYUFBYTtNQUNiLHdCQUF1QjtjQUF2Qix1QkFBdUI7TUFDdkIseUJBQW1CO2NBQW5CLG1CQUFtQixFQUFBOztBQU96QjtFQUNFLHNCQUFzQjtFQUN0QixZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLDJDQUEyQyxFQUFBOztBQUo3QztJQU1JLG9CQUFhO0lBQWIsYUFBYTtJQUNiLDZCQUE2QjtJQUM3Qix5QkFBbUI7WUFBbkIsbUJBQW1CO0lBQ25CLFlBQVksRUFBQTs7QUFUaEI7TUFXTSxvQkFBYTtNQUFiLGFBQWE7TUFDYix5QkFBbUI7Y0FBbkIsbUJBQW1CO01BQ25CLDRCQUFzQjtNQUF0Qiw2QkFBc0I7Y0FBdEIsc0JBQXNCO01BQ3RCLGNIMWVnQjtNRzJlaEIsZUFBZTtNQUNmLGdCQUFnQjtNQUNoQixxQkFBcUI7TUFDckIsaUJBQWlCO01BQ2pCLGtCQUFrQixFQUFBOztBQW5CeEI7UUFxQlEsV0FBVyxFQUFBOztBQXJCbkI7UUF3QlEsZUFBZSxFQUFBOztBQU12QjtFQUdNLFNBQVMsRUFBQTs7QUFJZjtFQUNFLG9CQUFhO0VBQWIsYUFBYTtFQUNiLGVBQWU7RUFDZixnQ0h4ZXFCO0VHeWVyQix5QkFBbUI7VUFBbkIsbUJBQW1CLEVBQUE7O0FBSnJCO0lBTUksa0JBQWtCO0lBQ2xCLGNBQWM7SUFFZCxXQUFXO0lBQ1gsb0JBQWE7SUFBYixhQUFhO0lBQ2IseUJBQW1CO1lBQW5CLG1CQUFtQjtJQUNuQix3QkFBdUI7WUFBdkIsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQixZQUFZLEVBQUE7O0FBZGhCO01BaUJNLFlBQVk7TUFDWixXQUFXO01BQ1gsc0JBQW1CO1NBQW5CLG1CQUFtQixFQUFBOztBQW5CekI7SUF1QkksZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixVQUFXLEVBQUE7O0FBekJmO01BMkJNLGdCQUFnQjtNQUNoQix1QkFBdUI7TUFDdkIsb0JBQW9CO01BQ3BCLHFCQUFxQjtNQUNyQiw4QkFBQTtNQUNBLDRCQUE0QixFQUFBOztBQWhDbEM7TUFtQ00saUJBQWlCO01BQ2pCLGNIM2dCaUIsRUFBQTs7QUd1ZXZCO0lBd0NJLGlCQUFpQjtJQUNqQixvQkFBYTtJQUFiLGFBQWE7SUFDYix5QkFBbUI7WUFBbkIsbUJBQW1CO0lBQ25CLGNBQWMsRUFBQTs7QUEzQ2xCO01BNkNNLFlBQVksRUFBQTs7QUFRbEI7RUFHTSxTQUFTLEVBQUE7O0FBSWY7RUFDRSxvQkFBYTtFQUFiLGFBQWE7RUFDYixnQkFBZ0IsRUFBQTs7QUFGbEI7SUFJSSxVQUFVO0lBQ1Ysa0JBQWtCLEVBQUE7O0FBTHRCO01BT00sZUFBZSxFQUFBOztBQVByQjtJQVdJLGtCQUFrQjtJQUNsQixrQkFBa0I7SUFDbEIsa0JBQWtCO0lBQ2xCLHNCQUFzQjtJQUN0QiwyQkFBMkI7SUFDM0IsV0FBVztJQUNYLGlCQUFpQixFQUFBOztBQWpCckI7TUFtQk0sVUFBVTtNQUNWLFdBQVc7TUFDWCxrQkFBa0I7TUFDbEIsUUFBUTtNQUNSLHdDQUFnQztjQUFoQyxnQ0FBZ0M7TUFDaEMsU0FBUyxFQUFBOztBQU9mO0VBQ0UseUJBQW1CO1VBQW5CLG1CQUFtQjtFQUNuQixnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLG1CQUFtQixFQUFBOztBQUpyQjtJQU1NLGtCQUFrQixFQUFBOztBQU54QjtNQVFVLFlBQVk7TUFDWixZQUFZO01BQ1osVUFBVTtNQUNWLG1CSHhrQmE7TUd5a0JiLGtCQUFrQjtNQUNsQixlQUFlO01BQ2YsY0FBYyxFQUFBOztBQWR4QjtJQW1CSSxlQUFlO0lBQ2YsY0FBYztJQUNkLFNBQVMsRUFBQTs7QUFyQmI7SUF5QkksV0FBVztJQUNYLFlBQVk7SUFDWixpQkFBaUI7SUFDakIsb0JBQWlCO09BQWpCLGlCQUFpQjtJQUNqQixlQUFlLEVBQUE7O0FBN0JuQjtJQWdDSSxnQkFBZ0I7SUFDaEIsdUJBQXVCO0lBQ3ZCLG9CQUFvQjtJQUNwQixxQkFBcUI7SUFDckIsOEJBQUE7SUFDQSw0QkFBNEI7SUFFNUIsZUFBZSxFQUFBOztBQUduQjtFQUNFLG1CSGxtQm1CLEVBQUE7O0FHaW1CckI7SUFHSSxvQkFBYTtJQUFiLGFBQWEsRUFBQTs7QUFHakI7RUFDRSxvQkFBYTtFQUFiLGFBQWEsRUFBQTs7QUFEZjtJQUlNLGNBQWUsRUFBQTs7QUFPckI7RUFDRSxrREFBa0Q7RUFDbEQsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsbUJIdm5CbUIsRUFBQTs7QUdrbkJyQjtJQU9JLGVBQWUsRUFBQTs7QUFQbkI7TUFTTSxrQkFBa0IsRUFBQTs7QUFUeEI7TUFlWSw0QkFBNEIsRUFBQTs7QUFmeEM7TUF3QlksMkJBQTJCLEVBQUE7O0FBeEJ2QztJQStCSSxtQkFBb0IsRUFBQTs7QUEvQnhCO01BaUNNLGdCQUFnQjtNQUNoQixtQkFBbUIsRUFBQTs7QUFsQ3pCO0lBc0NJLFlBQVk7SUFDWixnQkFBZ0IsRUFBQTs7QUF2Q3BCO01BeUNNLFlBQVksRUFBQTs7QUFPbEI7RUFFSSxvQkFBYTtFQUFiLGFBQWE7RUFDYix5QkFBbUI7VUFBbkIsbUJBQW1CO0VBQ25CLGdCQUFnQjtFQUNoQixnQ0hwckJtQixFQUFBOztBRytxQnZCO0lBT00sZ0JBQWdCLEVBQUE7O0FBUHRCO0lBVU0sZ0JBQWdCO0lBQ2hCLG9CQUFvQjtJQUNwQixnQ0gzckJpQixFQUFBOztBRytxQnZCO01BY1EsZUFBZSxFQUFBOztBQWR2QjtJQWtCTSxZQUFZLEVBQUE7O0FBbEJsQjtFQXNCSSxvQkFBYTtFQUFiLGFBQWE7RUFDYixjQUFjO0VBQ2Qsb0JBQWlCO0tBQWpCLGlCQUFpQixFQUFBOztBQXhCckI7SUEyQk0sV0FBVztJQUNYLFlBQVksRUFBQTs7QUE1QmxCO0lBbUNNLFdBQVcsRUFBQTs7QUFuQ2pCO0VBdUNJLFlBQVk7RUFDWixvQkFBYTtFQUFiLGFBQWE7RUFDYix3QkFBdUI7VUFBdkIsdUJBQXVCO0VBQ3ZCLHdCQUF1QjtVQUF2Qix1QkFBdUI7RUFDdkIsNEJBQXNCO0VBQXRCLDZCQUFzQjtVQUF0QixzQkFBc0I7RUFDdEIsV0FBVyxFQUFBOztBQTVDZjtJQThDTSxnQ0g3dEJpQixFQUFBOztBRytxQnZCO0VBa0RJLFdBQVc7RUFDWCxZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLGNBQWM7RUFDZCw0QkFBb0I7VUFBcEIsb0JBQW9CO0VBQ3BCLGlDQUF5QjtVQUF6Qix5QkFBeUIsRUFBQTs7QUF2RDdCO0lBeURNLFdBQVc7SUFDWCxZQUFZLEVBQUE7O0FBMURsQjtJQTZETSxpQ0FBeUI7WUFBekIseUJBQXlCO0lBQ3pCLGlDQUNGO1lBREUseUJBQ0YsRUFBQTs7QUEvREo7SUFpRU0sWUFBWTtJQUNaLFdBQVc7SUFDWCx5QkhodUJnQjtJR2l1QmhCLGtCQUFrQjtJQUNsQixvQkFBYTtJQUFiLGFBQWE7SUFDYix5QkFBbUI7WUFBbkIsbUJBQW1CO0lBQ25CLHdCQUF1QjtZQUF2Qix1QkFBdUI7SUFDdkIsZUFBZSxFQUFBOztBQXhFckI7SUEyRU0sb0JBQWE7SUFBYixhQUFhO0lBQ2IseUJIcHVCbUI7SUdxdUJuQix5QkFBbUI7WUFBbkIsbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxZQUFZO0lBQ1osd0JBQXVCO1lBQXZCLHVCQUF1QjtJQUN2Qix5Qkg3dkJpQjtJRzh2QmpCLGtCQUFrQixFQUFBOztBQWxGeEI7TUFvRlEsV0FBVztNQUNYLFlBQVksRUFBQTs7QUFyRnBCO01Bd0ZRLGdCQUFnQjtNQUNoQixjSC92QmU7TUdnd0JmLDBCQUF5QjtNQUN6QixlQUFlO01BQ2YsZ0JBQWdCO01BQ2hCLG9CQUFvQjtNQUNwQixpQkFBaUI7TUFDakIsa0JBQWtCLEVBQUE7O0FBSzFCO0VBSUksaUJBQWlCLEVBQUE7O0FBS3JCO0VBRUksV0FBVztFQUNYLFlBQVk7RUFDWixvQkFBaUI7S0FBakIsaUJBQWlCO0VBQ2pCLGNBQWMsRUFBQTs7QUFMbEI7SUFPTSxXQUFXO0lBQ1gsWUFBWTtJQUNaLGtCQUFrQjtJQUNsQix5Qkh0eUJpQixFQUFBOztBRzZ5QnZCO0VBQ0UsV0FBVztFQUNYLGVBQWU7RUFDZixrQkFBa0IsRUFBQTs7QUFIcEI7SUFLSSxlQUFlO0lBQ2Ysb0JBQWE7SUFBYixhQUFhO0lBQ2IseUJBQW1CO1lBQW5CLG1CQUFtQjtJQUNuQixnQ0hyekJtQixFQUFBOztBRzZ5QnZCO01BVU0sV0FBVyxFQUFBOztBQVZqQjtRQWNRLFVBQVU7UUFDVixlQUFlO1FBQ2YsZ0JBQWdCO1FBQ2hCLHVCQUF1QjtRQUN2QixvQkFBb0I7UUFDcEIscUJBQXFCO1FBQ3JCLDhCQUFBO1FBQ0EsNEJBQTRCO1FBQzVCLGtCQUFrQjtRQUNsQixhQUFhO1FBQ2IseUJBQW1CO2dCQUFuQixtQkFBbUIsRUFBQTs7QUF4QjNCO1VBMEJVLGdCQUFnQixFQUFBOztBQTFCMUI7VUE2QlUsV0FBVztVQUNYLFlBQVk7VUFDWixvQkFBaUI7YUFBakIsaUJBQWlCO1VBQ2pCLHlCSGgwQlc7VUdpMEJYLGtCQUFrQjtVQUNsQixrQkFBa0IsRUFBQTs7QUFsQzVCO01BdUNNLG9CQUFhO01BQWIsYUFBYTtNQUNiLGlCQUFpQjtNQUNqQixjQUFjLEVBQUE7O0FBekNwQjtRQTJDUSxXQUFXO1FBQ1gsWUFBWTtRQUNaLG9CQUFpQjtXQUFqQixpQkFBaUI7UUFDakIseUJIOTBCYTtRRyswQmIsa0JBQWtCLEVBQUE7O0FBTzFCO0VBQ0UsYUFBYTtFQUNiLGdCQUFnQixFQUFBOztBQUZsQjtJQUlJLG9CQUFhO0lBQWIsYUFBYTtJQUNiLGFBQWE7SUFDYixnQkFBZ0IsRUFBQTs7QUFNcEI7RUFDRSxrQkFBa0IsRUFBQTs7QUFJcEI7RUFDRSxvQkFBYTtFQUFiLGFBQWE7RUFDYixlQUFlO0VBQ2YseUJBQW1CO1VBQW5CLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWiwrQkg3M0JzQjtFRzgzQnRCLGNIdjVCbUI7RUd3NUJuQixlQUFlLEVBQUE7O0FBVGpCO0lBV0ksV0FBVztJQUNYLGlCQUFpQixFQUFBOztBQUdyQjtFQUNFLFdBQVc7RUFDWCxjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQix5QkFBbUI7VUFBbkIsbUJBQW1CO0VBQ25CLG9CQUFhO0VBQWIsYUFBYTtFQUNiLFlBQVksRUFBQTs7QUFFZDtFQUNFLG9CQUFhO0VBQWIsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2YsbUJBQW1CLEVBQUE7O0FBSnJCO0lBTUksYUFBYTtJQUNiLFlBQVk7SUFDWix5QkhwNUJtQjtJR3E1Qm5CLG1CQUFtQjtJQUNuQixrQkFBa0IsRUFBQTs7QUFWdEI7TUFhUSxhQUFhO01BQ2IsMEJBQTBCO01BQzFCLHNCQUFzQjtNQUN0QiwyQkFBMkI7TUFDM0IsNEJBQTRCLEVBQUE7O0FBakJwQztRQW1CVSxXQUFXO1FBQ1gsWUFBWTtRQUNaLDRCQUE0QjtRQUM1QixvQkFBaUI7V0FBakIsaUJBQWlCLEVBQUE7O0FBdEIzQjtNQTBCUSw2Qkh0NkJlO01HdTZCZiw0QkFBNEI7TUFDNUIsWUFBWSxFQUFBOztBQTVCcEI7UUE4QlUsZ0JBQWdCO1FBQ2hCLHVCQUF1QjtRQUN2QixvQkFBb0I7UUFDcEIscUJBQXFCO1FBQ3JCLDhCQUFBO1FBQ0EsNEJBQTRCO1FBQzVCLGdCQUFnQixFQUFBOztBQXBDMUI7UUF1Q1UsZ0JBQWdCO1FBQ2hCLHVCQUF1QjtRQUN2QixvQkFBb0I7UUFDcEIscUJBQXFCO1FBQ3JCLDhCQUFBO1FBQ0EsNEJBQTRCO1FBQzVCLGdCQUFnQixFQUFBOztBQTdDMUI7TUFrRE0sZUFBZTtNQUNmLG9CQUFhO01BQWIsYUFBYTtNQUNiLHlCQUFtQjtjQUFuQixtQkFBbUIsRUFBQTs7QUFwRHpCO1FBc0RRLGlCQUFpQjtRQUNqQixZQUFZO1FBQ1osV0FBVyxFQUFBOztBQXhEbkI7SUErRFEsa0JBQWtCLEVBQUE7O0FBSzFCO0VBQ0UsZ0JBQWdCO0VBQ2hCLG9CQUFhO0VBQWIsYUFBYTtFQUNiLGVBQWU7RUFDZiw2QkhwOUJxQixFQUFBOztBR2c5QnZCO0lBTUksVUFBVTtJQUNWLGNBQWMsRUFBQTs7QUFQbEI7TUFVUSx5QkgxOUJlO01HMjlCZixhQUFhO01BQ2IsY0FBYyxFQUFBOztBQVp0QjtNQWdCTSxnQ0hoK0JpQjtNR2krQmpCLGtDQUFrQyxFQUFBOztBQWpCeEM7SUFxQkksYUFBYTtJQUNiLGlCQUFpQjtJQUNqQixzQkFBc0I7SUFDdEIsMkJBQTJCLEVBQUE7O0FBeEIvQjtJQTJCSSxrQkFBa0I7SUFDbEIsYUFBYSxFQUFBOztBQTVCakI7TUE4Qk0sZ0JBQWdCO01BQ2hCLHVCQUF1QjtNQUN2QixvQkFBb0I7TUFDcEIscUJBQXFCO01BQ3JCLDhCQUFBO01BQ0EsNEJBQTRCLEVBQUE7O0FBbkNsQztNQXNDTSxlQUFlO01BQ2Ysb0JBQWE7TUFBYixhQUFhO01BQ2IseUJBQW1CO2NBQW5CLG1CQUFtQixFQUFBOztBQXhDekI7UUEwQ1EsaUJBQWlCO1FBQ2pCLFlBQVk7UUFDWixXQUFXLEVBQUE7O0FBNUNuQjtRQStDUSxnQkFBZ0I7UUFDaEIsdUJBQXVCO1FBQ3ZCLG9CQUFvQjtRQUNwQixxQkFBcUI7UUFDckIsOEJBQUE7UUFDQSw0QkFBNEIsRUFBQTs7QUFRcEM7RUFDRSxpQkFBaUI7RUFDakIsb0JBQWE7RUFBYixhQUFhLEVBQUE7O0FBRmY7SUFJSSxZQUFZO0lBQ1osV0FBVztJQUNYLG1CQUFtQjtJQUNuQixjQUFjLEVBQUE7O0FBUGxCO01BU00sWUFBWTtNQUNaLFdBQVc7TUFDWCxtQkFBbUI7TUFDbkIsb0JBQWlCO1NBQWpCLGlCQUFpQixFQUFBOztBQVp2QjtJQWdCSSxvQkFBb0I7SUFDcEIsZ0NIemhDbUI7SUcwaENuQixXQUFXO0lBQ1gsb0JBQWE7SUFBYixhQUFhO0lBQ2IseUJBQW1CO1lBQW5CLG1CQUFtQixFQUFBOztBQXBCdkI7TUFzQk0saUJBQWlCO01BQ2pCLFdBQVc7TUFDWCxrQkFBa0IsRUFBQTs7QUF4QnhCO1FBMEJRLG9CQUFhO1FBQWIsYUFBYTtRQUNiLHlCQUFtQjtnQkFBbkIsbUJBQW1CLEVBQUE7O0FBM0IzQjtVQTZCVSxnQkFBZ0I7VUFDaEIsdUJBQXVCO1VBQ3ZCLG9CQUFvQjtVQUNwQixxQkFBcUI7VUFDckIsOEJBQUE7VUFDQSw0QkFBNEI7VUFDNUIsbUJBQW1CLEVBQUE7O0FBbkM3QjtVQXNDVSxpQkFBaUI7VUFDakIsY0FBYyxFQUFBOztBQXZDeEI7UUEyQ1EsZUFBZTtRQUNmLG9CQUFhO1FBQWIsYUFBYTtRQUNiLHlCQUFtQjtnQkFBbkIsbUJBQW1CLEVBQUE7O0FBN0MzQjtVQStDVSxnQkFBZ0I7VUFDaEIsdUJBQXVCO1VBQ3ZCLG9CQUFvQjtVQUNwQixxQkFBcUI7VUFDckIsOEJBQUE7VUFDQSw0QkFBNEI7VUFDNUIsbUJBQW1CLEVBQUE7O0FBckQ3QjtVQXdEVSxZQUFZO1VBQ1osV0FBVztVQUNYLHlCSHBqQ1k7VUdxakNaLG9CQUFhO1VBQWIsYUFBYTtVQUNiLHdCQUF1QjtrQkFBdkIsdUJBQXVCO1VBQ3ZCLHlCQUFtQjtrQkFBbkIsbUJBQW1CO1VBQ25CLGtCQUFrQjtVQUNsQixjQUFjO1VBQ2QsaUJBQWlCO1VBQ2pCLDBCQUEwQixFQUFBOztBQWpFcEM7TUFzRU0sV0FBVztNQUNYLFlBQVk7TUFDWixjQUFjO01BQ2QsaUJBQWlCLEVBQUE7O0FBekV2QjtRQTJFUSxXQUFXO1FBQ1gsWUFBWSxFQUFBOztBQTVFcEI7SUFpRkksZ0NIemxDbUI7SUcwbENuQix5QkFBbUI7WUFBbkIsbUJBQW1CO0lBQ25CLGVBQWUsRUFBQTs7QUFuRm5CO01BcUZNLFdBQVc7TUFDWCxZQUFZO01BQ1osa0JBQWtCLEVBQUE7O0FBdkZ4QjtRQXlGUSxXQUFXO1FBQ1gsWUFBWTtRQUNaLGtCQUFrQjtRQUNsQixtQkFBZ0I7V0FBaEIsZ0JBQWdCLEVBQUE7O0FBNUZ4QjtNQWdHTSxTQUFTO01BQ1QsaUJBQWlCLEVBQUE7O0FBSXZCO0VBR00sZUFBZSxFQUFBOztBQUhyQjtFQU9FLGdCQUFnQixFQUFBOztBQUdsQjtFQUNFLGtCQUFrQixFQUFBOztBQURwQjtJQUlNLGtCQUFrQjtJQUNsQix5Qkhob0NpQjtJR2lvQ2pCLGtCQUFrQjtJQUNsQixtQkFBbUI7SUFDbkIsa0JBQWtCLEVBQUE7O0FBUnhCO01BVVEsWUFBWTtNQUNaLGdDSGpvQ2U7TUdrb0NmLG1CQUFtQjtNQUNuQixrQkFBa0I7TUFDbEIsU0FBUztNQUNULFVBQVU7TUFDVixZQUFZO01BQ1osNkJBQXFCO2NBQXJCLHFCQUFxQjtNQUNyQiw4QkFBc0I7Y0FBdEIsc0JBQXNCLEVBQUE7O0FBbEI5QjtNQXFCUSx5Qkg3b0NlLEVBQUE7O0FHd25DdkI7TUF3QlEsWUFBWSxFQUFBOztBQXhCcEI7TUE0QlUsV0FBVztNQUNYLG9CQUFpQjtTQUFqQixpQkFBaUI7TUFDakIsWUFBWTtNQUNaLG1CQUFtQjtNQUNuQix5QkgzcENhO01HNHBDYixpQkFBaUIsRUFBQTs7QUFqQzNCO01BcUNRLG9CQUFhO01BQWIsYUFBYTtNQUNiLHlCQUFtQjtjQUFuQixtQkFBbUIsRUFBQTs7QUF0QzNCO1FBd0NVLFdBQVc7UUFDWCxrQkFBa0IsRUFBQTs7QUF6QzVCO01BOENnQiwwQkFBMEIsRUFBQTs7QUE5QzFDO0lBb0RJLGtCQUFrQixFQUFBOztBQXBEdEI7TUFzRE0sa0JBQWtCO01BQ2xCLFdBQVc7TUFDWCxlQUFlO01BQ2Ysa0JBQWtCO01BQ2xCLFlBQVk7TUFDWixXQUFXO01BQ1gsb0JBQWE7TUFBYixhQUFhO01BQ2IseUJBQW1CO2NBQW5CLG1CQUFtQjtNQUNuQix3QkFBdUI7Y0FBdkIsdUJBQXVCO01BQ3ZCLGNIN3FDZSxFQUFBOztBR2lyQ3JCO0VBQ0U7SUFDRSxXQUFXLEVBQUE7RUFFYjtJQUNFLFlBQVksRUFBQSxFQUFBOztBQUxoQjtFQUNFO0lBQ0UsV0FBVyxFQUFBO0VBRWI7SUFDRSxZQUFZLEVBQUEsRUFBQTs7QUFHaEI7RUFHTSxpQkFBaUI7RUFDakIsZ0JBQWdCLEVBQUE7O0FBSXRCO0VBSVEsaUJBQWlCO0VBQ2pCLGdCQUFnQixFQUFBOztBQUt4QjtFQUNFLHlCSDd0Q21CO0VHOHRDbkIsa0JBQWtCLEVBQUE7O0FBRnBCO0lBSUkseUJIcnNDcUI7SUdzc0NyQixhQUFhO0lBQ2IsMEJBQTBCO0lBQzFCLGdDSG51Q2lCLEVBQUE7O0FHNHRDckI7TUFTTSxvQkFBYTtNQUFiLGFBQWE7TUFDYix5QkFBbUI7Y0FBbkIsbUJBQW1CLEVBQUE7O0FBVnpCO1FBWVEsWUFBWTtRQUNaLFdBQVc7UUFDWCxtQkFBbUIsRUFBQTs7QUFkM0I7UUFpQlEsaUJBQWlCO1FBQ2pCLGdCQUFnQixFQUFBOztBQWxCeEI7SUF3QkksZUFBZTtJQUNmLG9CQUFhO0lBQWIsYUFBYSxFQUFBOztBQXpCakI7TUEyQk0sb0JBQWE7TUFBYixhQUFhO01BQ2IsNEJBQXNCO01BQXRCLDZCQUFzQjtjQUF0QixzQkFBc0I7TUFDdEIseUJBQW1CO2NBQW5CLG1CQUFtQjtNQUNuQixhQUFhLEVBQUE7O0FBOUJuQjtRQWdDUSxZQUFZO1FBQ1osV0FBVyxFQUFBOztBQWpDbkI7UUFvQ1EsZUFBZSxFQUFBOztBQUt2QjtFQUVJLHlCSDV1Q3FCLEVBQUE7O0FHa3ZDekI7RUFFSSxnQkFBZ0IsRUFBQTs7QUFGcEI7SUFJTSxpQkFBaUIsRUFBQTs7QUFKdkI7SUFPTSxZQUFZO0lBQ1osV0FBVztJQUNYLGNBQWM7SUFDZCxvQkFBYTtJQUFiLGFBQWE7SUFDYix3QkFBdUI7WUFBdkIsdUJBQXVCLEVBQUE7O0FBWDdCO01BYVEsWUFBWTtNQUNaLFVBQVU7TUFDVixtQkh4eENlLEVBQUE7O0FHeXdDdkI7RUFvQkkseUJIN3hDbUI7RUc4eENuQixlQUFlO0VBQ2YsZUFBZTtFQUNmLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsZ0JBQWdCLEVBQUE7O0FBekJwQjtJQTJCTSxpQkFBaUIsRUFBQTs7QUNqMEN2QjtFQUNFLFdBQVc7RUFDWCxZQUFZLEVBQUE7O0FBRmQ7SUFJSSxXQUFXO0lBQ1gsWUFBWTtJQUNaLG9CQUFhO0lBQWIsYUFBYTtJQUNiLHlCQUFtQjtZQUFuQixtQkFBbUI7SUFDbkIsZ0NKd0JtQjtJSXZCbkIseUJKaUNpQixFQUFBOztBSTFDckI7TUFXTSx5QkowQ2U7TUl6Q2YsU0FBUyxFQUFBOztBQVpmO1FBY1EsY0o0QmEsRUFBQTs7QUkxQ3JCO1VBZ0JVLHlCQUE4QixFQUFBOztBQWhCeEM7TUFxQk0sU0FBUyxFQUFBOztBQXJCZjtNQXdCTSxXQUFXO01BQ1gsWUFBWTtNQUNaLGdCQUFnQixFQUFBOztBQTFCdEI7TUE2Qk0sV0FBVztNQUNYLGNBQWM7TUFDZCxlQUFlLEVBQUE7O0FBL0JyQjtNQWtDTSxXQUFXO01BQ1gsVUFBVTtNQUNWLGdCQUFnQjtNQUNoQixtQkpVZ0I7TUlUaEIsa0JBQWtCO01BQ2xCLGNBQWM7TUFDZCxnQkFBZ0I7TUFDaEIsZUFBZSxFQUFBOztBQXpDckI7TUE0Q00sV0FBVztNQUNYLFlBQVk7TUFDWixjQUFjO01BQ2Qsb0JBQWE7TUFBYixhQUFhO01BQ2IseUJBQW1CO2NBQW5CLG1CQUFtQjtNQUNuQix3QkFBdUI7Y0FBdkIsdUJBQXVCLEVBQUE7O0FBakQ3QjtRQW1EUSxxQkFBb0I7Z0JBQXBCLG9CQUFvQixFQUFBOztBQW5ENUI7TUF1RE0sV0FBVztNQUNYLGtCQUFrQjtNQUNsQix5QkFBbUI7Y0FBbkIsbUJBQW1CO01BQ25CLG9CQUFhO01BQWIsYUFBYTtNQUNiLHdCQUF1QjtjQUF2Qix1QkFBdUI7TUFDdkIsNkJBQTZCLEVBQUE7O0FBNURuQztRQThEUSw0QkFBc0I7UUFBdEIsNkJBQXNCO2dCQUF0QixzQkFBc0IsRUFBQTs7QUE5RDlCO1FBaUVRLHFCQUFxQjtRQUNyQixtQkFBbUI7UUFDbkIsZ0JBQWdCO1FBQ2hCLHVCQUF1QixFQUFBOztBQXBFL0I7TUF5RVEsNEJBQTRCLEVBQUE7O0FBekVwQztJQThFSSxZQUFZLEVBQUE7O0FBOUVoQjtNQWdGTSx5Q0FBOEI7TUFBOUIsd0NBQThCO2NBQTlCLDhCQUE4QjtNQUM5Qix1QkFBMkI7Y0FBM0IsMkJBQTJCO01BQzNCLHlCQUFtQjtjQUFuQixtQkFBbUI7TUFDbkIsZ0JBQWdCO01BQ2hCLG1CQUFtQixFQUFBOztBQXBGekI7UUFzRlEsZ0JBQWdCO1FBQ2hCLHVCQUF1QjtRQUN2QixvQkFBb0I7UUFDcEIscUJBQXFCO1FBQ3JCLDhCQUFBO1FBQ0EsNEJBQTRCLEVBQUE7O0FBM0ZwQztNQStGTSxZQUFZO01BQ1osV0FBVztNQUNYLGtCQUFrQjtNQUNsQixrQkFBa0I7TUFDbEIsY0FBYyxFQUFBOztBQW5HcEI7UUFxR1EsV0FBVztRQUNYLFlBQVk7UUFDWixrQkFBa0I7UUFDbEIseUJKdkVlO1FJd0VmLG9CQUFpQjtXQUFqQixpQkFBaUIsRUFBQTs7QUF6R3pCO01BNkdNLFdBQVcsRUFBQTs7QUE3R2pCO1FBK0dRLFdBQVcsRUFBQTs7QUEvR25CO0lBb0hJLGtCQUFrQjtJQUNsQixpQkFBaUIsRUFBQTs7QUFJckI7RUFDRSxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLG9CQUFvQjtFQUNwQixpQkFBaUI7RUFDakIseUJKcEZtQixFQUFBOztBSStFckI7SUFPSSxhQUFhO0lBQ2IseUJBQXlCLEVBQUE7O0FBUjdCO0lBV0ksb0JBQWE7SUFBYixhQUFhO0lBRWIseUJBQW1CO1lBQW5CLG1CQUFtQixFQUFBOztBQWJ2QjtNQWdCTSxXQUFXO01BQ1gsU0FBUztNQUNULHFCQUFxQjtNQUNyQiw0QkFBNEI7TUFDNUIseUJBQXlCO01BQ3pCLG1CQUFtQjtNQUNuQix5Qko5R2lCO01JK0dqQixZQUFZLEVBQUE7O0FBdkJsQjtRQXlCUSxtRUFBbUU7UUFDbkUscUJBQXFCO1FBQ3JCLGtCQUFrQixFQUFBOztBQTNCMUI7VUE2QlUsZUFBZTtVQUNmLGNKeEhhLEVBQUE7O0FJMEZ2QjtVQTZCVSxlQUFlO1VBQ2YsY0p4SGEsRUFBQTs7QUkwRnZCO1VBNkJVLGVBQWU7VUFDZixjSnhIYSxFQUFBOztBSTBGdkI7VUE2QlUsZUFBZTtVQUNmLGNKeEhhLEVBQUE7O0FJMEZ2QjtNQW1DTSxXQUFXLEVBQUE7O0FBbkNqQjtNQXNDTSxXQUFXLEVBQUE7O0FBdENqQjtNQTJDUSxrQkFBa0I7TUFDbEIsY0FBYyxFQUFBOztBQTVDdEI7UUE4Q1UsV0FBVyxFQUFBOztBQTlDckI7UUFpRFUsaUJBQWlCO1FBQ2pCLGVBQWU7UUFDZixjQUFjO1FBQ2QsWUFBWTtRQUNaLGVBQWUsRUFBQTs7QUFyRHpCO1FBd0RVLGlCQUFpQjtRQUNqQixlQUFlO1FBQ2YsZUFBZTtRQUNmLFFBQVEsRUFBQTs7QUEzRGxCO0lBaUZJLFlBQVk7SUFDWixXQUFXO0lBQ1gsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQiwyQ0FBMkM7SUFDM0Msa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZix5QkovS21CO0lJZ0xuQixxQkFBcUI7SUFDckIsZ0JBQWdCO0lBQ2hCLHFCQUFxQjtJQUNyQiw0QkFBNEI7SUFDNUIseUJBQXlCLEVBQUE7O0FBN0Y3QjtNQStGTSwrREFBK0QsRUFBQTs7QUEvRnJFO0lBbUdJLGNKdExtQjtJSXVMbkIsZUFBZTtJQUNmLHFCQUFxQjtJQUNyQixnQkFBZ0I7SUFDaEIsZ0JBQWdCO0lBQ2hCLHVCQUF1QjtJQUN2QixvQkFBb0I7SUFDcEIscUJBQXFCO0lBQ3JCLDhCQUFBO0lBQ0EsNEJBQTRCLEVBQUE7O0FBNUdoQztJQW1HSSxjSnRMbUI7SUl1TG5CLGVBQWU7SUFDZixxQkFBcUI7SUFDckIsZ0JBQWdCO0lBQ2hCLGdCQUFnQjtJQUNoQix1QkFBdUI7SUFDdkIsb0JBQW9CO0lBQ3BCLHFCQUFxQjtJQUNyQiw4QkFBQTtJQUNBLDRCQUE0QixFQUFBOztBQTVHaEM7SUFtR0ksY0p0TG1CO0lJdUxuQixlQUFlO0lBQ2YscUJBQXFCO0lBQ3JCLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsdUJBQXVCO0lBQ3ZCLG9CQUFvQjtJQUNwQixxQkFBcUI7SUFDckIsOEJBQUE7SUFDQSw0QkFBNEIsRUFBQTs7QUE1R2hDO0lBbUdJLGNKdExtQjtJSXVMbkIsZUFBZTtJQUNmLHFCQUFxQjtJQUNyQixnQkFBZ0I7SUFDaEIsZ0JBQWdCO0lBQ2hCLHVCQUF1QjtJQUN2QixvQkFBb0I7SUFDcEIscUJBQXFCO0lBQ3JCLDhCQUFBO0lBQ0EsNEJBQTRCLEVBQUE7O0FBU2hDO0VBQ0UsbUJKM0x1QjtFSTRMdkIsNEJBQTRCO0VBQzVCLDRCQUE0QjtFQUM1QixxQkFBcUI7RUFDckIscUJBQXFCLEVBQUE7O0FBRXZCO0VBRUksZ0NBQWdDLEVBQUE7O0FBRnBDO0VBTUssbUJBQW1CLEVBQUE7O0FDM1B4QjtFQUNFLG9CTENtQixFQUFBOztBS0VyQjtFQUdNLGtCQUFrQixFQUFBOztBQUl4QjtFQUNFLGtCQUFrQjtFQUNsQiw4Qkw4QndCO0VLN0J4QixpQkFBaUI7RUFDakIsaUNMNEJ3QjtFSzNCeEIsbUJMMEJtQixFQUFBOztBSy9CckI7SUFPSSxZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLFlBQVk7SUFDWixXQUFXO0lBQ1gsbUJMb0JpQjtJS25CakIsNEJBQTRCO0lBQzVCLGFBQWE7SUFDYiwyQ0FBMkM7SUFDM0MsT0FBTyxFQUFBOztBQWZYO0lBa0JJLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsWUFBWTtJQUNaLFdBQVc7SUFDWCxpQkFBaUI7SUFDakIsNEJBQTRCO0lBQzVCLFVBQVU7SUFDVixPQUFPLEVBQUE7O0FBekJYO0lBK0JJLGFBQWE7SUFDYixjQUFjLEVBQUE7O0FBaENsQjtNQTZCTSxhQUFhLEVBQUE7O0FBN0JuQjtJQW1DSSxZQUFZO0lBQ1osVUFBVTtJQUNWLGtCQUFrQjtJQUNsQixtQkxwQm1CO0lLcUJuQixTQUFTO0lBQ1QsV0FBVztJQUNYLGdCQUFnQixFQUFBOztBQUdwQjtFQUNFLFdBQVc7RUFDWCxlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLHlCTHhEb0I7RUt5RHBCLHlCTHhEcUI7RUt5RHJCLGtCQUFrQjtFQUNsQixvQkFBYTtFQUFiLGFBQWE7RUFDYix3QkFBdUI7VUFBdkIsdUJBQXVCLEVBQUE7O0FBUnpCO0lBVUksb0JBQWE7SUFBYixhQUFhO0lBQ2IsZ0JBQWdCO0lBQ2hCLDRCQUFzQjtJQUF0Qiw2QkFBc0I7WUFBdEIsc0JBQXNCO0lBQ3RCLHlCQUFtQjtZQUFuQixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLCtCTG5Fa0I7SUtvRWxCLHFEQUFxRCxFQUFBOztBQWhCekQ7TUFrQk0sWUFBWTtNQUNaLFdBQVc7TUFDWCxrQkFBa0IsRUFBQTs7QUFwQnhCO01BdUJNLGdCQUFnQjtNQUNoQixrQkFBa0I7TUFDbEIsZ0JBQWdCO01BQ2hCLHVCQUF1QjtNQUN2QixvQkFBb0I7TUFDcEIscUJBQXFCO01BQ3JCLDhCQUFBO01BQ0EsNEJBQTRCLEVBQUE7O0FBOUJsQztNQWlDTSxrQkFBa0I7TUFDbEIsZUFBZTtNQUNmLGdCQUFnQjtNQUNoQix1QkFBdUI7TUFDdkIsb0JBQW9CO01BQ3BCLHFCQUFxQjtNQUNyQiw4QkFBQTtNQUNBLDRCQUE0QixFQUFBOztBQXhDbEM7TUEyQ00sa0JBQWtCO01BQ2xCLGdCQUFnQixFQUFBOztBQTVDdEI7SUFnREksVUFBVTtJQUNWLFlBQVk7SUFDWix5Qkx0R2tCLEVBQUE7O0FLMEd0QjtFQUNFLGFBQWE7RUFDYixXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLGdCQUFnQixFQUFBOztBQUpsQjtJQU1JLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLGFBQWEsRUFBQTs7QUFUakI7TUFXTSxZQUFZO01BQ1osYUFBYTtNQUNiLHlCTHZGaUI7TUt3RmpCLGdCQUFnQjtNQUNoQiw0QkFBNEI7TUFDNUIsMkJBQTJCO01BQzNCLG1CTDdGaUIsRUFBQTs7QUs0RXZCO1FBbUJRLFdBQVc7UUFDWCxZQUFZO1FBQ1osMkJBQTJCO1FBQzNCLDRCQUE0QjtRQUM1QixvQkFBaUI7V0FBakIsaUJBQWlCLEVBQUE7O0FBdkJ6QjtNQTJCTSxnQkFBZ0I7TUFDaEIsb0JBQWE7TUFBYixhQUFhO01BQ2IseUJBQW1CO2NBQW5CLG1CQUFtQjtNQUNuQix5Qkx4R2lCO01LeUdqQixhQUFhO01BQ2IsK0JBQStCO01BQy9CLDhCQUE4QjtNQUM5Qiw0Q0FBNEM7TUFDNUMsZUFBZSxFQUFBOztBQW5DckI7UUFxQ1EsZUFBZTtRQUNmLG9CQUFhO1FBQWIsYUFBYSxFQUFBOztBQXRDckI7VUF3Q1Usb0JBQWE7VUFBYixhQUFhO1VBQ2IseUJBQW1CO2tCQUFuQixtQkFBbUI7VUFDbkIsNEJBQXNCO1VBQXRCLDZCQUFzQjtrQkFBdEIsc0JBQXNCO1VBQ3RCLGNBQWM7VUFDZCxlQUFlO1VBQ2Ysa0JBQWtCO1VBQ2xCLFdBQVcsRUFBQTs7QUE5Q3JCO1lBZ0RZLHlCQUF5QixFQUFBOztBQWhEckM7WUFtRFksY0w5SlMsRUFBQTs7QUsyR3JCO1VBdURVLGdCQUFnQixFQUFBOztBQXZEMUI7WUF5RFksZ0JBQWdCO1lBQ2hCLHVCQUF1QjtZQUN2QixvQkFBb0I7WUFDcEIscUJBQXFCO1lBQ3JCLDhCQUFBO1lBQ0EsNEJBQTRCLEVBQUE7O0FBOUR4QztZQWlFWSxlQUFlO1lBQ2YsZ0JBQWdCLEVBQUE7O0FBTzVCO0VBQ0UsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixnQ0w1SnFCLEVBQUE7O0FLeUp2QjtJQUtJLGdCQUFnQjtJQUNoQixvQkFBYTtJQUFiLGFBQWE7SUFDYix5QkFBbUI7WUFBbkIsbUJBQW1CLEVBQUE7O0FBUHZCO0lBVUksV0FBVztJQUNYLGlCQUFpQjtJQUNqQixtQkFBbUI7SUFDbkIsc0JBQXNCO0lBQ3RCLDRCQUE0QjtJQUM1Qix5Qkx4S21CO0lLeUtuQixpQkFBaUI7SUFDakIsa0JBQWtCLEVBQUE7O0FBakJ0QjtNQW1CTSxpQkFBaUI7TUFDakIsa0JBQWtCO01BQ2xCLG1CQUFtQjtNQUNuQiwyQ0FBMkM7TUFDM0Msa0JBQWtCO01BQ2xCLFdBQVc7TUFDWCxTQUFTLEVBQUE7O0FBekJmO01BNEJNLG1CQUFtQixFQUFBOztBQTVCekI7SUFnQ0ksZ0JBQWdCO0lBQ2hCLG9CQUFhO0lBQWIsYUFBYTtJQUViLGdCQUFnQjtJQUNoQix1QkFBdUI7SUFFdkIscUJBQXFCO0lBQ3JCLDhCQUFBO0lBQ0EsNEJBQTRCLEVBQUE7O0FBeENoQztJQTJDSSxnQkFBZ0I7SUFDaEIsdUJBQXVCO0lBQ3ZCLG9CQUFvQjtJQUNwQixxQkFBcUI7SUFDckIsOEJBQUE7SUFDQSw0QkFBNEI7SUFDNUIsZUFBZSxFQUFBOztBQWpEbkI7SUFvREksZUFBZSxFQUFBOztBQXBEbkI7SUF1REksWUFBWTtJQUNaLFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIsb0JBQWlCO09BQWpCLGlCQUFpQixFQUFBOztBQUdyQjtFQUNFLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsY0FBYyxFQUFBOztBQUdoQjtFQUNFLG9CQUFvQjtFQUNwQixnQ0w5TnFCLEVBQUE7O0FLNE52QjtJQUlJLFNBQVMsRUFBQTs7QUFKYjtJQU9JLGtEQUFrRDtJQUNsRCxvQkFBb0I7SUFDcEIsNEJBQTRCLEVBQUE7O0FBVGhDO0lBWUkseUJMM05pQjtJSzROakIsb0JBQWE7SUFBYixhQUFhO0lBQ2IsNEJBQXNCO0lBQXRCLDZCQUFzQjtZQUF0QixzQkFBc0I7SUFDdEIseUJBQW1CO1lBQW5CLG1CQUFtQjtJQUNuQixpQkFBaUIsRUFBQTs7QUFoQnJCO01Ba0JNLG9CQUFhO01BQWIsYUFBYTtNQUNiLFdBQVc7TUFDWCxlQUFlO01BQ2Ysd0JBQXVCO2NBQXZCLHVCQUF1QixFQUFBOztBQXJCN0I7UUF1QlEsYUFBYSxFQUFBOztBQXZCckI7VUF5QlUsWUFBWTtVQUNaLGFBQWEsRUFBQTs7QUExQnZCO1FBOEJRLGlCQUFpQjtRQUNqQixZQUFZLEVBQUE7O0FBL0JwQjtVQWlDVSxXQUFXLEVBQUE7O0FBakNyQjtVQW9DVSxXQUFXO1VBQ1gsWUFBWTtVQUNaLFdBQVc7VUFDWCxVQUFVO1VBQ1Ysb0NBQW9DO1VBQ3BDLGtCQUFrQjtVQUNsQixrQkFBa0IsRUFBQTs7QUExQzVCO1VBOENZLDJCQUFtQjtrQkFBbkIsbUJBQW1CO1VBQ25CLDRCQUFvQjtVQUFwQixvQkFBb0I7VUFDcEIsVUFBVSxFQUFBOztBQWhEdEI7UUFxRFEsa0JBQWtCO1FBQ2xCLFlBQVksRUFBQTs7QUF0RHBCO1VBd0RVLFdBQVcsRUFBQTs7QUF4RHJCO1VBNkVZLFdBQVc7VUFDWCxZQUFZO1VBQ1osV0FBVztVQUNYLFVBQVU7VUFDVixvQ0FBb0M7VUFDcEMsa0JBQWtCO1VBQ2xCLGtCQUFrQixFQUFBOztBQW5GOUI7VUF1RmMsMkJBQW1CO2tCQUFuQixtQkFBbUI7VUFDbkIsVUFBVSxFQUFBOztBQXhGeEI7TUErRlEsYUFBYTtNQUNiLFlBQVk7TUFDWixtQkFBbUI7TUFDbkIsb0JBQWlCO1NBQWpCLGlCQUFpQjtNQUNqQix5QkwvVGUsRUFBQTs7QUs0TnZCO01BdUdNLGVBQWU7TUFDZixrQkFBa0I7TUFDbEIsb0JBQWE7TUFBYixhQUFhO01BQ2IseUJBQW1CO2NBQW5CLG1CQUFtQjtNQUNuQixlQUFlLEVBQUE7O0FBM0dyQjtRQTZHUSxnQkFBZ0IsRUFBQTs7QUE3R3hCO1FBZ0hRLFdBQVc7UUFDWCxpQkFBaUIsRUFBQTs7QUFqSHpCO01Bc0hNLDJCQUEyQjtNQUMzQiw0QkFBNEI7TUFDNUIsc0JBQXNCO01BQ3RCLG1CQUFtQjtNQUNuQixrQkFBa0IsRUFBQTs7QUExSHhCO1FBNEhRLGtCQUFrQjtRQUNsQixTQUFTO1FBQ1QsZUFBZSxFQUFBOztBQTlIdkI7UUFpSVEsa0JBQWtCO1FBQ2xCLFFBQVE7UUFDUixTQUFTO1FBQ1Qsd0NBQWdDO2dCQUFoQyxnQ0FBZ0MsRUFBQTs7QUFwSXhDO1FBdUlRLFlBQVk7UUFDWixZQUFZO1FBQ1osa0JBQWtCO1FBQ2xCLG9DQUFvQztRQUNwQyxrQkFBa0I7UUFDbEIsb0JBQWE7UUFBYixhQUFhO1FBQ2IseUJBQW1CO2dCQUFuQixtQkFBbUI7UUFDbkIsd0JBQXVCO2dCQUF2Qix1QkFBdUI7UUFDdkIsY0x4V2U7UUt5V2YsZUFBZTtRQUNmLGdCQUFnQjtRQUNoQixxQkFBcUI7UUFDckIsaUJBQWlCO1FBQ2pCLFdBQVc7UUFDWCxZQUFZLEVBQUE7O0FBckpwQjtVQXVKVSxXQUFXO1VBQ1gsaUJBQWlCLEVBQUE7O0FBeEozQjtJQThKSSxnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsdUJBQXVCO0lBQ3ZCLG9CQUFvQjtJQUNwQixxQkFBcUI7SUFDckIsOEJBQUE7SUFDQSw0QkFBNEI7SUFDNUIsV0FBVyxFQUFBOztBQXZLZjtJQTJLTSxVQUFVLEVBQUE7O0FBM0toQjtNQTZLUSxVQUFVO01BQ1YsWUFBWSxFQUFBOztBQTlLcEI7UUFnTFUsZ0JBQWdCO1FBQ2hCLCtCQUF1QjtnQkFBdkIsdUJBQXVCO1FBQ3ZCLFdBQVc7UUFDWCxtQkFBbUI7UUFDbkIsNEJBQTRCO1FBQzVCLHNCQUFzQjtRQUN0QiwyQkFBMkIsRUFBQTs7QUF0THJDO1VBd0xZLFdBQVc7VUFDWCxZQUFZO1VBQ1osZ0JBQWdCLEVBQUE7O0FBMUw1QjtJQWlNSSxnQkFBZ0I7SUFDaEIsb0JBQWE7SUFBYixhQUFhO0lBQ2Isd0JBQXVCO1lBQXZCLHVCQUF1QjtJQUN2Qix3QkFBdUI7WUFBdkIsdUJBQXVCO0lBQ3ZCLDRCQUE0QixFQUFBOztBQXJNaEM7TUF1TU0sZUFBZSxFQUFBOztBQXZNckI7TUEwTU0sZUFBZSxFQUFBOztBQTFNckI7TUE2TU0sZUFBZSxFQUFBOztBQTdNckI7TUFnTk0sb0JBQWE7TUFBYixhQUFhO01BQ2IsNEJBQXNCO01BQXRCLDZCQUFzQjtjQUF0QixzQkFBc0I7TUFDdEIsd0JBQXVCO2NBQXZCLHVCQUF1QjtNQUN2Qix5QkFBbUI7Y0FBbkIsbUJBQW1CO01BQ25CLGFBQWE7TUFDYixjQUFjO01BQ2QsY0FBYyxFQUFBOztBQXROcEI7UUF3TlEsWUFBWSxFQUFBOztBQXhOcEI7UUEyTlEsWUFBWSxFQUFBOztBQTNOcEI7UUE4TlEsV0FBVyxFQUFBOztBQTlObkI7UUFpT1EsY0FBYyxFQUFBOztBQWpPdEI7UUFvT1EsZUFBZSxFQUFBOztBQXBPdkI7UUF1T1EsWUFBWTtRQUNaLFdBQVc7UUFDWCxjTHhiYTtRS3liYixlQUFlO1FBQ2YsZ0JBQWdCO1FBQ2hCLG9CQUFhO1FBQWIsYUFBYTtRQUNiLHlCQUFtQjtnQkFBbkIsbUJBQW1CO1FBQ25CLHdCQUF1QjtnQkFBdkIsdUJBQXVCO1FBQ3ZCLGtCQUFrQjtRQUNsQix5Qkw1Y2UsRUFBQTs7QUs0TnZCO1VBa1BVLHlCTHplVyxFQUFBOztBS3VQckI7VUFzUFUsV0FBVztVQUNYLFlBQVk7VUFDWixrQkFBa0I7VUFDbEIsb0JBQWlCO2FBQWpCLGlCQUFpQixFQUFBOztBQXpQM0I7VUE0UFUsV0FBVztVQUNYLFlBQVksRUFBQTs7QUE3UHRCO1lBK1BZLFdBQVc7WUFDWCxZQUFZLEVBQUE7O0FBaFF4QjtRQXFRUSxlQUFlO1FBQ2YsZ0JBQWdCO1FBQ2hCLHVCQUF1QjtRQUN2QixvQkFBb0I7UUFDcEIscUJBQXFCO1FBQ3JCLDhCQUFBO1FBQ0EsNEJBQTRCO1FBQzVCLGtCQUFrQixFQUFBOztBQU0xQjtFQUVJLFdBQVc7RUFDWCxlQUFlO0VBQ2YsaUJBQWlCLEVBQUE7O0FBSnJCO0lBTU0sbUJBQW1CO0lBQ25CLHlCTGxmaUI7SUttZmpCLGtCQUFrQjtJQUNsQixrQkFBa0IsRUFBQTs7QUFUeEI7TUFXUSxrQkFBa0I7TUFDbEIsUUFBUTtNQUNSLFNBQVM7TUFDVCx3Q0FBZ0M7Y0FBaEMsZ0NBQWdDO01BQ2hDLFdBQVc7TUFDWCxrQkFBa0I7TUFDbEIsc0JBQW1CO1NBQW5CLG1CQUFtQixFQUFBOztBQWpCM0I7SUFxQk0sb0JBQWE7SUFBYixhQUFhO0lBQ2IsZ0JBQWdCO0lBQ2hCLHlCQUFtQjtZQUFuQixtQkFBbUIsRUFBQTs7QUF2QnpCO01BeUJRLFdBQVc7TUFDWCxZQUFZO01BQ1osa0JBQWtCO01BQ2xCLGlCQUFpQixFQUFBOztBQTVCekI7SUFnQ00sZ0JBQWdCLEVBQUE7O0FBaEN0QjtJQW1DTSxnQkFBZ0IsRUFBQTs7QUFuQ3RCO0lBc0NNLGdCQUFnQixFQUFBOztBQUl0QjtFQUNFLG9CQUFhO0VBQWIsYUFBYTtFQUNiLFdBQVc7RUFDWCxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGtCQUFrQixFQUFBOztBQUxwQjtJQU9JLGtCQUFrQixFQUFBOztBQVB0QjtNQVNNLGFBQWE7TUFDYixZQUFZO01BQ1oseUJMaGlCaUI7TUtpaUJqQixrQkFBa0I7TUFDbEIseUJMeGhCZSxFQUFBOztBSzJnQnJCO1FBZVEsYUFBYTtRQUNiLGdDTHJpQmU7UUtzaUJmLG9CQUFhO1FBQWIsYUFBYTtRQUNiLHdCQUF1QjtnQkFBdkIsdUJBQXVCO1FBQ3ZCLHlCQUFtQjtnQkFBbkIsbUJBQW1CO1FBQ25CLDBCQUEwQixFQUFBOztBQXBCbEM7VUFzQlUsWUFBWTtVQUNaLG9CQUFpQjthQUFqQixpQkFBaUI7VUFDakIsMEJBQTBCLEVBQUE7O0FBeEJwQztRQTRCUSxhQUFhO1FBQ2IsZUFBZTtRQUNmLFlBQVksRUFBQTs7QUE5QnBCO1VBZ0NVLGdCQUFnQjtVQUNoQix1QkFBdUI7VUFDdkIsb0JBQW9CO1VBQ3BCLHFCQUFxQjtVQUNyQiw4QkFBQTtVQUNBLDRCQUE0QixFQUFBOztBQXJDdEM7VUF3Q1UsZ0JBQWdCO1VBQ2hCLHVCQUF1QjtVQUN2QixvQkFBb0I7VUFDcEIscUJBQXFCO1VBQ3JCLDhCQUFBO1VBQ0EsNEJBQTRCLEVBQUE7O0FBT3RDO0VBRUksbUJBQW1CO0VBQ25CLFlBQVk7RUFDWiwyQkFBMkI7RUFDM0Isc0JBQXNCO0VBQ3RCLHlCTGxsQm1CO0VLbWxCbkIseUJMbmxCbUI7RUtvbEJuQiw0QkFBNEI7RUFDNUIsaUJBQWlCO0VBQ2pCLDRCQUE0QjtFQUM1QixpQkFBaUIsRUFBQTs7QUFYckI7RUFjSSxnQkFBZ0I7RUFDaEIsb0JBQWE7RUFBYixhQUFhO0VBQ2IseUJBQW1CO1VBQW5CLG1CQUFtQixFQUFBOztBQWhCdkI7SUFrQk0sWUFBWTtJQUNaLGNBQWM7SUFDZCx5Qkw1bEJpQixFQUFBOztBS3drQnZCO0lBdUJNLFdBQVc7SUFDWCxZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLG9CQUFpQjtPQUFqQixpQkFBaUI7SUFDakIseUJMbm1CaUIsRUFBQTs7QUt3a0J2QjtJQThCTSxnQkFBZ0I7SUFDaEIsV0FBVyxFQUFBOztBQS9CakI7RUFtQ0ksZUFBZTtFQUNmLGtCQUFrQixFQUFBOztBQXBDdEI7RUF1Q0ksZ0JBQWdCLEVBQUE7O0FBR3BCO0VBQ0Usb0JBQWE7RUFBYixhQUFhO0VBQ2IsZ0JBQWdCO0VBQ2hCLGVBQWUsRUFBQTs7QUFIakI7SUFNSSxlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCLG9CQUFhO0lBQWIsYUFBYTtJQUNiLDRCQUFzQjtJQUF0Qiw2QkFBc0I7WUFBdEIsc0JBQXNCO0lBQ3RCLHlCQUFtQjtZQUFuQixtQkFBbUI7SUFDbkIsY0FBYztJQUNkLGtCQUFrQixFQUFBOztBQVp0QjtNQWNNLGVBQWUsRUFBQTs7QUFkckI7SUFrQkksV0FBVztJQUNYLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIseUJMM29CbUIsRUFBQTs7QUtzbkJ2QjtNQXdCTSxZQUFZO01BQ1osV0FBVztNQUNYLGtCQUFrQixFQUFBOztBQTFCeEI7SUE4QkksZUFBZTtJQUNmLGdCQUFnQjtJQUNoQix1QkFBdUI7SUFDdkIsb0JBQW9CO0lBQ3BCLHFCQUFxQjtJQUNyQiw4QkFBQTtJQUNBLDRCQUE0QixFQUFBOztBQUdoQztFQUdNLGlCQUFpQixFQUFBOztBQUh2QjtFQU1NLHNCQUFzQjtFQUV0QixnQkFBZ0I7RUFDaEIsbUJBQW1CLEVBQUE7O0FBVHpCO0VBYUksc0JBQXNCO0VBQ3RCLGdCQUFnQjtFQUNoQixtQkFBbUIsRUFBQTs7QUFLdkI7RUFDRSxtQkxycUJtQixFQUFBOztBS29xQnJCO0lBSUksV0FBVztJQUNYLGFBQWE7SUFDYiwyQkFBMkI7SUFDM0Isc0JBQXNCO0lBQ3RCLHVFQUF3RSxFQUFBOztBQVI1RTtNQVVNLFdBQVc7TUFDWCxhQUFhLEVBQUE7O0FBWG5CO01BY00sb0JBQWE7TUFBYixhQUFhO01BQ2IseUJBQThCO2NBQTlCLDhCQUE4QjtNQUM5QixrQkFBa0I7TUFDbEIsU0FBUztNQUNULFdBQVc7TUFDWCxZQUFZO01BQ1osZUFBZSxFQUFBOztBQXBCckI7UUF1QlUsV0FBVztRQUNYLFlBQVksRUFBQTs7QUF4QnRCO1FBMkJVLFdBQVc7UUFDWCxZQUFZO1FBQ1osV0FBVztRQUNYLFVBQVU7UUFDVixvQ0FBb0M7UUFDcEMsa0JBQWtCO1FBQ2xCLGtCQUFrQixFQUFBOztBQWpDNUI7UUFxQ1ksMkJBQW1CO2dCQUFuQixtQkFBbUI7UUFDbkIsNEJBQW9CO1FBQXBCLG9CQUFvQjtRQUNwQixVQUFVLEVBQUE7O0FBdkN0QjtRQTZDVSxXQUFXO1FBQ1gsWUFBWSxFQUFBOztBQTlDdEI7UUFpRFUsV0FBVztRQUNYLFlBQVk7UUFDWixXQUFXO1FBQ1gsVUFBVTtRQUNWLG9DQUFvQztRQUNwQyxrQkFBa0I7UUFDbEIsa0JBQWtCLEVBQUE7O0FBdkQ1QjtRQTJEWSwyQkFBbUI7Z0JBQW5CLG1CQUFtQjtRQUNuQiw0QkFBb0I7UUFBcEIsb0JBQW9CO1FBQ3BCLFVBQVUsRUFBQTs7QUE3RHRCO01BbUVNLGtCQUFrQjtNQUNsQixXQUFXO01BQ1gsYUFBYTtNQUNiLE1BQU07TUFDTixPQUFPO01BQ1AsNEJBQTRCO01BQzVCLHNCQUFzQjtNQUN0QiwyQkFBMkIsRUFBQTs7QUExRWpDO1FBNEVRLHlDQUF5QyxFQUFBOztBQTVFakQ7TUFnRk0sb0JBQWE7TUFBYixhQUFhO01BQ2IsWUFBWTtNQUNaLHlCQUFtQjtjQUFuQixtQkFBbUI7TUFDbkIsa0JBQWtCO01BQ2xCLFNBQVM7TUFDVCxXQUFXO01BQ1gsZUFBZSxFQUFBOztBQXRGckI7UUF3RlEsa0JBQWtCO1FBQ2xCLFdBQVc7UUFDWCxrQkFBa0I7UUFDbEIsWUFBWSxFQUFBOztBQTNGcEI7VUE2RlUsV0FBVztVQUNYLFlBQVk7VUFDWixtQkFBbUI7VUFDbkIseUJMN3dCYTtVSzh3QmIsMENBQTBDO1VBQzFDLFNBQVM7VUFDVCxrQkFBa0I7VUFDbEIsbUJBQW1CO1VBQ25CLGtCQUFrQixFQUFBOztBQXJHNUI7WUF1R1ksa0JBQWtCO1lBQ2xCLG1FQUFtRTtZQUNuRSx5QkFBeUI7WUFDekIsNEJBQTRCO1lBQzVCLHFCQUFxQjtZQUNyQixtQkFBbUIsRUFBQTs7QUE1Ry9CO1lBK0dZLGVBQWU7WUFDZixjTC94QlcsRUFBQTs7QUsrcUJ2QjtZQStHWSxlQUFlO1lBQ2YsY0wveEJXLEVBQUE7O0FLK3FCdkI7WUErR1ksZUFBZTtZQUNmLGNML3hCVyxFQUFBOztBSytxQnZCO1lBK0dZLGVBQWU7WUFDZixjTC94QlcsRUFBQTs7QUsrcUJ2QjtNQXNITSxpQkFBaUI7TUFDakIsb0JBQWE7TUFBYixhQUFhO01BQ2IsY0FBYztNQUNkLFdBQVcsRUFBQTs7QUF6SGpCO1FBMkhRLGNBQWM7UUFDZCxrQkFBa0I7UUFDbEIsa0JBQWtCLEVBQUE7O0FBN0gxQjtVQStIVSxXQUFXO1VBQ1gsWUFBWSxFQUFBOztBQWhJdEI7VUFtSVUsa0JBQWtCO1VBQ2xCLG1CTG55Qlk7VUtveUJaLFdBQVc7VUFDWCxZQUFZO1VBQ1osa0JBQWtCO1VBQ2xCLG9CQUFhO1VBQWIsYUFBYTtVQUNiLHlCQUFtQjtrQkFBbkIsbUJBQW1CO1VBQ25CLHdCQUF1QjtrQkFBdkIsdUJBQXVCO1VBQ3ZCLHlCTC95Qlc7VUtnekJYLFlBQVk7VUFDWixTQUFTO1VBQ1QsVUFBVTtVQUNWLG1CQUFtQixFQUFBOztBQS9JN0I7WUFrSlksY0FBYyxFQUFBOztBQWxKMUI7UUF1SlEsY0FBYyxFQUFBOztBQXZKdEI7VUF5SlUsV0FBVztVQUNYLFlBQVksRUFBQTs7QUExSnRCO0lBZ0tJLG1CTHAwQmlCO0lLcTBCakIsV0FBVztJQUNYLGtEQUFrRDtJQUNsRCxpQkFBaUI7SUFDakIsbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLGtCQUFrQixFQUFBOztBQXhLdEI7TUEwS00sWUFBWTtNQUNaLGFBQWE7TUFDYixrQkFBa0I7TUFDbEIsMkNBQTJDO01BQzNDLFVBQVU7TUFDVixTQUFTO01BQ1Qsa0JBQWtCO01BQ2xCLHFDQUE2QjtjQUE3Qiw2QkFBNkIsRUFBQTs7QUFqTG5DO1FBbUxRLFlBQVk7UUFDWixhQUFhO1FBQ2Isa0JBQWtCO1FBQ2xCLG9CQUFpQjtXQUFqQixpQkFBaUIsRUFBQTs7QUF0THpCO01BNExNLGtCQUFrQixFQUFBOztBQTVMeEI7UUE4TFEsZUFBZTtRQUNmLGdCQUFnQjtRQUNoQix1QkFBdUI7UUFDdkIsb0JBQW9CO1FBQ3BCLHFCQUFxQjtRQUNyQiw4QkFBQTtRQUNBLDRCQUE0QixFQUFBOztBQXBNcEM7UUF1TVEsZUFBZSxFQUFBOztBQXZNdkI7UUEwTVEsb0JBQWE7UUFBYixhQUFhO1FBQ2Isd0JBQXVCO2dCQUF2Qix1QkFBdUI7UUFDdkIseUJBQW1CO2dCQUFuQixtQkFBbUI7UUFDbkIsZUFBZTtRQUNmLGVBQWUsRUFBQTs7QUE5TXZCO1VBZ05VLFdBQVc7VUFDWCxpQkFBaUI7VUFDakIsY0FBYyxFQUFBOztBQWxOeEI7SUF3TkksZ0JBQWdCO0lBQ2hCLG9CQUFhO0lBQWIsYUFBYTtJQUNiLHdCQUF1QjtZQUF2Qix1QkFBdUI7SUFDdkIsd0JBQXVCO1lBQXZCLHVCQUF1QjtJQUN2Qiw0QkFBNEIsRUFBQTs7QUE1TmhDO01BOE5NLGVBQWUsRUFBQTs7QUE5TnJCO01BaU9NLGVBQWUsRUFBQTs7QUFqT3JCO01Bb09NLGVBQWUsRUFBQTs7QUFwT3JCO01BdU9NLG9CQUFhO01BQWIsYUFBYTtNQUNiLDRCQUFzQjtNQUF0Qiw2QkFBc0I7Y0FBdEIsc0JBQXNCO01BQ3RCLHdCQUF1QjtjQUF2Qix1QkFBdUI7TUFDdkIseUJBQW1CO2NBQW5CLG1CQUFtQjtNQUNuQixhQUFhO01BQ2IsY0FBYztNQUNkLGNBQWMsRUFBQTs7QUE3T3BCO1FBK09RLFlBQVksRUFBQTs7QUEvT3BCO1FBa1BRLFlBQVksRUFBQTs7QUFsUHBCO1FBcVBRLFdBQVcsRUFBQTs7QUFyUG5CO1FBd1BRLGNBQWMsRUFBQTs7QUF4UHRCO1FBMlBRLGVBQWUsRUFBQTs7QUEzUHZCO1FBOFBRLFlBQVk7UUFDWixXQUFXO1FBQ1gsY0xwNkJhO1FLcTZCYixlQUFlO1FBQ2YsZ0JBQWdCO1FBQ2hCLG9CQUFhO1FBQWIsYUFBYTtRQUNiLHlCQUFtQjtnQkFBbkIsbUJBQW1CO1FBQ25CLHdCQUF1QjtnQkFBdkIsdUJBQXVCO1FBQ3ZCLGtCQUFrQjtRQUNsQix5Qkw5NkJlLEVBQUE7O0FLdXFCdkI7VUEwUVUseUJMdDlCVyxFQUFBOztBSzRzQnJCO1VBNlFVLFdBQVc7VUFDWCxZQUFZO1VBQ1osa0JBQWtCO1VBRWxCLG9CQUFpQjthQUFqQixpQkFBaUIsRUFBQTs7QUFqUjNCO1VBb1JVLFdBQVc7VUFDWCxZQUFZLEVBQUE7O0FBclJ0QjtZQXVSWSxXQUFXO1lBQ1gsWUFBWSxFQUFBOztBQXhSeEI7UUE2UlEsZUFBZTtRQUNmLGdCQUFnQjtRQUNoQix1QkFBdUI7UUFDdkIsb0JBQW9CO1FBQ3BCLHFCQUFxQjtRQUNyQiw4QkFBQTtRQUNBLDRCQUE0QjtRQUM1QixrQkFBa0IsRUFBQTs7QUFLMUI7RUFHTSxnQkFBZ0IsRUFBQTs7QUFJdEI7RUFDRSx5QkxwOUJ3QixFQUFBOztBS205QjFCO0lBR0ksV0FBVztJQUNYLG1CTHg5QmlCO0lLeTlCakIsWUFBWTtJQUNaLG9CQUFhO0lBQWIsYUFBYSxFQUFBOztBQU5qQjtNQVFNLFVBQVU7TUFDVixvQkFBYTtNQUFiLGFBQWE7TUFDYix3QkFBdUI7Y0FBdkIsdUJBQXVCO01BQ3ZCLHlCQUFtQjtjQUFuQixtQkFBbUI7TUFDbkIsaUJBQWlCLEVBQUE7O0FBWnZCO1FBY1EsV0FBVztRQUNYLFlBQVk7UUFDWixpQkFBaUIsRUFBQTs7QUFoQnpCO1FBbUJRLFdBQVcsRUFBQTs7QUFuQm5CO1FBc0JRLDhCTHYvQmUsRUFBQTs7QUs2L0J2QjtFQUNFLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsb0JBQWE7RUFBYixhQUFhO0VBQ2IseUJBQW1CO1VBQW5CLG1CQUFtQjtFQUNuQixtQkxyL0JtQixFQUFBOztBS2cvQnJCO0lBYUksaUJBQWlCO0lBQ2pCLG9CQUFhO0lBQWIsYUFBYTtJQUNiLDRCQUFzQjtJQUF0Qiw2QkFBc0I7WUFBdEIsc0JBQXNCO0lBQ3RCLHlCQUFtQjtZQUFuQixtQkFBbUIsRUFBQTs7QUFoQnZCO01BbUJRLFdBQVcsRUFBQTs7QUFuQm5CO1FBcUJVLFdBQVc7UUFDWCxZQUFZLEVBQUE7O0FBT3RCO0VBRUksa0JBQWtCO0VBQ2xCLFlBQVksRUFBQTs7QUFIaEI7SUFLTSxZQUFZO0lBQ1osa0JBQWtCLEVBQUE7O0FBTnhCO01BUVEsWUFBWTtNQUNaLFdBQVc7TUFDWCxtQkFBbUI7TUFDbkIsb0JBQWlCO1NBQWpCLGlCQUFpQjtNQUNqQix5Qkw1aENlLEVBQUE7O0FLZ2hDdkI7TUFlUSxrQkFBa0I7TUFDbEIsd0NBQXdDO01BQ3hDLFdBQVc7TUFDWCxZQUFZO01BQ1osT0FBTztNQUNQLE1BQU07TUFDTixtQkFBbUIsRUFBQTs7QUFyQjNCO01Bd0JRLGVBQWU7TUFDZixpQkFBaUI7TUFDakIsY0x2aUNhO01Ld2lDYixzQkFBc0I7TUFDdEIsa0JBQWtCO01BQ2xCLGtCQUFrQjtNQUNsQixTQUFTO01BQ1QsZUFBZTtNQUNmLGtCQUFrQjtNQUNsQixXQUFXO01BQ1gsZ0JBQWdCO01BQ2hCLHVCQUF1QjtNQUN2QixvQkFBb0I7TUFDcEIscUJBQXFCO01BQ3JCLDhCQUFBO01BQ0EsNEJBQTRCLEVBQUE7O0FBdkNwQztNQTBDUSxrQkFBa0I7TUFDbEIsVUFBVTtNQUNWLFlBQVk7TUFDWixXQUFXO01BQ1gsU0FBUztNQUNULHVDQUErQjtjQUEvQiwrQkFBK0IsRUFBQTs7QUEvQ3ZDO0lBbURNLHlCTHJtQ2dCO0lLc21DaEIsV0FBVztJQUNYLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsU0FBUztJQUNULG1CQUFtQjtJQUNuQixvQkFBYTtJQUFiLGFBQWE7SUFDYiw2QkFBNkIsRUFBQTs7QUExRG5DO01BNERRLG9CQUFhO01BQWIsYUFBYTtNQUNiLDRCQUFzQjtNQUF0Qiw2QkFBc0I7Y0FBdEIsc0JBQXNCO01BQ3RCLHlCQUFtQjtjQUFuQixtQkFBbUI7TUFDbkIsd0JBQXVCO2NBQXZCLHVCQUF1QjtNQUN2QixhQUFhLEVBQUE7O0FBaEVyQjtRQWtFVSxXQUFXO1FBQ1gsWUFBWSxFQUFBOztBQW5FdEI7UUFzRVUsY0xubENXO1FLb2xDWCxlQUFlO1FBQ2YsZUFBZSxFQUFBOztBQy9uQ3pCO0VBQ0ksZUFBZSxFQUFBOztBQURuQjtJQUdRLGdCQUFnQjtJQUNoQix5Qk5EYztJTUVkLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsb0JBQWE7SUFBYixhQUFhO0lBQ2IseUJBQW1CO1lBQW5CLG1CQUFtQjtJQUNuQix5Qk5MZTtJTU1mLGlCQUFpQixFQUFBOztBQVZ6QjtNQVlZLGNBQWMsRUFBQTs7QUFaMUI7UUFjZ0IsWUFBWTtRQUNaLFdBQVcsRUFBQTs7QUFmM0I7TUFtQlksaUJBQWdCO01BQ2hCLGdCQUFnQixFQUFBOztBQXBCNUI7TUF1QlksaUJBQWlCLEVBQUE7O0FBdkI3QjtRQXlCZ0IsWUFBWSxFQUFBOztBQ3pCNUI7RUFDRSxzQkFBc0I7RUFDdEIsU0FBUztFQUNULFVBQVU7RUFDVixhQUFhO0VBQ2IsK0NBQStDO0VBQy9DLHNCQUFzQjtFQUN0QiwyQkFBZSxFQUFBOztBQUVqQjs7RUFFRSxhQUFhLEVBQUE7O0FBRWY7RUFFSSxtQlBzQ2lCLEVBQUE7O0FPaENyQjtFQUNFLG9CQUFhO0VBQWIsYUFBYSxFQUFBOztBQURmO0lBR0ksZUFBZSxFQUFBOztBQUhuQjtJQU1JLGdCQUFnQixFQUFBOztBQU5wQjtJQVNJLHlCQUFtQjtZQUFuQixtQkFBbUIsRUFBQTs7QUFUdkI7SUFZSSx3QkFBdUI7WUFBdkIsdUJBQXVCLEVBQUE7O0FBSzNCO0VBQ0Esb0JBQWE7RUFBYixhQUFhLEVBQUE7O0FBSWI7RUFDRSxXQUFXLEVBQUE7O0FBR2I7RUFDRSxXQUFXO0VBQ1gsNEJBQXNCO0VBQXRCLDZCQUFzQjtVQUF0QixzQkFBc0I7RUFDdEIsbUJBQW1CLEVBQUE7O0FBSHJCO0lBUUkscUJBQXFCLEVBQUE7O0FBUnpCO0lBV0ksaUJBQWlCO0lBQ2pCLDJCQUEyQixFQUFBOztBQUcvQjtFQUdNLDBCQUEwQixFQUFBOztBQUhoQztFQU1NLDJCQUEyQixFQUFBOztBQU1qQztFQUNFLG9CQUFhO0VBQWIsYUFBYTtFQUNiLHlCQUFtQjtVQUFuQixtQkFBbUI7RUFDbkIsZUFBZTtFQUNmLGlCQUFpQjtFQUNqQix5QlByQ21CLEVBQUE7O0FPZ0NyQjtJQU9JLFdBQVc7SUFDWCxXQUFXO0lBQ1gseUJQdERtQixFQUFBOztBTzZDdkI7SUFZSSxXQUFXO0lBQ1gsV0FBVztJQUNYLHlCUDNEbUIsRUFBQTs7QU82Q3ZCO0lBaUJJLGNBQWM7SUFDZCxlQUFlO0lBQ2YsV0FBVyxFQUFBOztBQU1mO0VBQ0Usa0JBQWtCLEVBQUE7O0FBSXBCO0VBQ0Usb0JBQWE7RUFBYixhQUFhO0VBQ2Isd0JBQXVCO1VBQXZCLHVCQUF1QjtFQUN2QixnQkFBZ0IsRUFBQTs7QUFIbEI7SUFLRyx5QkFBZ0MsRUFBQTs7QUFLbkM7RUFDSSxjQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLGdCQUFnQixFQUFBOztBQUlwQjtFQUNFLG9CQUFvQixFQUFBOztBQUV0QjtFQUNFLGdCQUFnQixFQUFBOztBQURsQjtJQUdJLHlCUHBGc0IsRUFBQTs7QU9pRjFCO0lBTUksZ0JBQWdCLEVBQUE7O0FBR3BCO0VBQ0UseUJBQWdDLEVBQUE7O0FBRWxDO0VBT0EsdUJBQXVCO0VBQ3ZCLHNCQUFzQixFQUFBOztBQVJ0QjtJQUVFLHFCQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7SUFDWCwrQkFBd0IsRUFBQTs7QUFMMUI7SUFVRSxZQUFZLEVBQUE7O0FBSWQ7RUFDRSxvQ0FBb0M7RUFDcEMsMENBQTBDLEVBQUE7O0FBRTVDO0VBQ0UsNEJBQWUsRUFBQTs7QUFLakI7RUFHSSw2QkFBYztFQUNkLGdDQUFpQjtFQUNqQiwrQkFBZ0I7RUFDaEIsNkJBQWM7RUFDZCxxQkFBcUIsRUFBQTs7QUFQekI7RUFXSSxpQ0FBa0IsRUFBQTs7QUFYdEI7RUFjSyxTQUFTO0VBQ1IsdUJBQXVCO0VBQ3ZCLCtCQUF3QjtFQUF4Qix3QkFBd0I7RUFDeEIsb0NBQ2lCO1VBRGpCLDhCQUNpQjtFQUNqQiw4QkFBOEI7RUFDN0IsdUNBQXVDO0VBQ3ZDLHdDQUF3QyxFQUFBOztBQUd4QztFQUNDLHFCQUFhLEVBQUE7O0FBRGQ7SUFHRyx5QkFBOEIsRUFBQTs7QUFHcEM7RUFDRSxtQkFBbUIsRUFBQTs7QUFHdkI7RUFFSSx3Q0FBd0MsRUFBQTs7QUFGNUM7SUFJTSx3QkFBd0IsRUFBQTs7QUFKOUI7SUFPTSx3QkFBd0IsRUFBQTs7QUFLaEM7RUFFSSxtQ0FBbUM7RUFDbkMsb0JBQW9CLEVBQUE7O0FBSHhCO0VBUUksbUNBQW1DO0VBQ25DLG9CQUFvQixFQUFBOztBQUl4QjtFQUVJLCtCQUFzQztFQUN0QyxvQkFBb0IsRUFBQTs7QUFIeEI7RUFPSSwrQkFBb0M7RUFDcEMsb0JBQW9CLEVBQUE7O0FBR3hCO0VBQ0UsdUJBQWUsRUFBQTs7QUFFakI7RUFFTSxxQkFBc0I7RUFDeEIsc0JBQWdCLEVBQUE7O0FBR3BCO0VBR00sbUJBQW1CLEVBQUE7O0FDbFB6QjtFQUNFLFlBQVk7RUFDWixvQkFBYTtFQUFiLGFBQWE7RUFDYix5QkFBbUI7VUFBbkIsbUJBQW1CO0VBQ25CLGdDUndCcUIsRUFBQTs7QVE1QnZCO0lBTUUsV0FBVztJQUNYLGtCQUFrQixFQUFBOztBQVBwQjtJQVVJLFNBQVMsRUFBQTs7QUFWYjtJQWFJLG9CQUFhO0lBQWIsYUFBYSxFQUFBOztBQWJqQjtJQWdCSSxjQUFjO0lBQ2Qsa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixXQUFXO0lBQ1gsb0JBQWE7SUFBYixhQUFhO0lBQ2IseUJBQW1CO1lBQW5CLG1CQUFtQixFQUFBOztBQXJCdkI7SUF3Qkksa0JBQWtCO0lBQ2xCLFVBQVU7SUFDVixlQUFlO0lBQ2YsU0FBUztJQUNULFFBQVEsRUFBQTs7QUE1Qlo7SUErQkksa0JBQWtCO0lBQ2xCLFFBQVE7SUFDUixRQUFRO0lBQ1IsWUFBWTtJQUNaLFdBQVc7SUFDWCx5QlJNaUI7SVFMakIseUJSVm9CO0lRV3BCLGtCQUFrQjtJQUNsQixrQkFBa0IsRUFBQTs7QUF2Q3RCO0lBMENJLHlCUkFpQjtJUUNqQix5QlJ0Q2tCLEVBQUE7O0FRTHRCO0lBOENJLFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIsYUFBYSxFQUFBOztBQWhEakI7SUFtREksY0FBYyxFQUFBOztBQW5EbEI7SUFzREksU0FBUztJQUNULFFBQVE7SUFDUixVQUFVO0lBQ1YsV0FBVztJQUNYLHFCUnJEa0I7SVFzRGxCLHlCQUF5QjtJQUN6QixnQ0FBd0I7WUFBeEIsd0JBQXdCLEVBQUE7O0FBNUQ1QjtJQStESSxVQUFVO0lBQ1YscUJBQXFCO0lBQ3JCLG1CQUFtQjtJQUNuQixnQkFBZ0I7SUFDaEIsdUJBQXVCLEVBQUE7O0FBbkUzQjtJQXNFSSxhQUFhLEVBQUE7O0FBSWpCO0VBQ0UsbUJBQW1CO0VBQ25CLGdDUi9DcUIsRUFBQTs7QVE2Q3ZCO0lBSUksU0FBUyxFQUFBOztBQUpiO0lBT0ksU0FBUztJQUNULFNBQVM7SUFDVCxpQkFBaUI7SUFDakIsZ0JBQWdCLEVBQUE7O0FBVnBCO0lBYUksV0FBVyxFQUFBOztBQWJmO0lBb0JJLGdCQUFnQjtJQUNoQixXQUFXLEVBQUE7O0FBckJmO01BdUJNLGdCQUFnQjtNQUNoQixnQkFBZ0IsRUFBQTs7QUF4QnRCO1FBMkJVLGFBQWE7UUFDYixVQUFVO1FBQ1YsZUFBZSxFQUFBOztBQTdCekI7TUFrQ00sZUFBZSxFQUFBOztBQWxDckI7UUFvQ1Esa0JBQWtCO1FBQ2xCLHdCQUF3QjtRQUN4Qix5QlIvRWU7UVFnRmYsMENBQTBDO1FBQzFDLFNBQVM7UUFDVCxZQUFZO1FBQ1osV0FBVztRQUNYLGVBQWU7UUFDZixvQkFBYTtRQUFiLGFBQWE7UUFDYix1QkFBMkI7Z0JBQTNCLDJCQUEyQixFQUFBOztBQTdDbkM7VUErQ1Usb0NBQW9DO1VBQ3BDLGdCQUFnQjtVQUNoQixvQkFBb0I7VUFDcEIsZUFBZTtVQUNmLHFCQUFxQjtVQUNyQixnQkFBZ0I7VUFDaEIsdUJBQXVCO1VBQ3ZCLG9CQUFvQjtVQUNwQixxQkFBcUI7VUFDckIsOEJBQUE7VUFDQSw0QkFBNEI7VUFDNUIsY1JyR2EsRUFBQTs7QVEyQ3ZCO1VBK0NVLG9DQUFvQztVQUNwQyxnQkFBZ0I7VUFDaEIsb0JBQW9CO1VBQ3BCLGVBQWU7VUFDZixxQkFBcUI7VUFDckIsZ0JBQWdCO1VBQ2hCLHVCQUF1QjtVQUN2QixvQkFBb0I7VUFDcEIscUJBQXFCO1VBQ3JCLDhCQUFBO1VBQ0EsNEJBQTRCO1VBQzVCLGNSckdhLEVBQUE7O0FRMkN2QjtVQStDVSxvQ0FBb0M7VUFDcEMsZ0JBQWdCO1VBQ2hCLG9CQUFvQjtVQUNwQixlQUFlO1VBQ2YscUJBQXFCO1VBQ3JCLGdCQUFnQjtVQUNoQix1QkFBdUI7VUFDdkIsb0JBQW9CO1VBQ3BCLHFCQUFxQjtVQUNyQiw4QkFBQTtVQUNBLDRCQUE0QjtVQUM1QixjUnJHYSxFQUFBOztBUTJDdkI7VUErQ1Usb0NBQW9DO1VBQ3BDLGdCQUFnQjtVQUNoQixvQkFBb0I7VUFDcEIsZUFBZTtVQUNmLHFCQUFxQjtVQUNyQixnQkFBZ0I7VUFDaEIsdUJBQXVCO1VBQ3ZCLG9CQUFvQjtVQUNwQixxQkFBcUI7VUFDckIsOEJBQUE7VUFDQSw0QkFBNEI7VUFDNUIsY1JyR2EsRUFBQTs7QVEyQ3ZCO1FBOERRLGtCQUFrQixFQUFBOztBQTlEMUI7VUFpRVksa0JBQWtCLEVBQUE7O0FBakU5QjtVQW9FWSxrQkFBa0I7VUFDbEIsV0FBVztVQUNYLFlBQVk7VUFDWixRQUFRO1VBQ1IsVUFBVTtVQUNWLG9DQUE0QjtrQkFBNUIsNEJBQTRCLEVBQUE7O0FBekV4QztVQThFWSx3QkFBd0IsRUFBQTs7QUE5RXBDO1VBaUZZLGtCQUFrQjtVQUNsQixXQUFXO1VBQ1gsUUFBUSxFQUFBOztBQW5GcEI7WUFxRmMsV0FBVyxFQUFBOztBQXJGekI7SUE2RkksWUFBWTtJQUNaLG9CQUFhO0lBQWIsYUFBYSxFQUFBOztBQTlGakI7SUFpR0ksZ0JBQWdCO0lBQ2hCLHlCQUErQjtJQUMvQixZQUFZO0lBQ1osZ0JBQWdCO0lBQ2hCLG9CQUFhO0lBQWIsYUFBYSxFQUFBOztBQXJHakI7TUF1R00sWUFBWSxFQUFBOztBQXZHbEI7SUEyR0ksb0JBQWE7SUFBYixhQUFhLEVBQUE7O0FBM0dqQjtNQThHUSxrQkFBa0IsRUFBQTs7QUE5RzFCO01BaUhRLGlCQUFpQixFQUFBOztBQWpIekI7TUFzSFEsY0FBYyxFQUFBOztBQXRIdEI7TUF5SFEsWUFBWTtNQUNaLGtCQUFrQixFQUFBOztBQTFIMUI7TUFnSU0sY0FBYyxFQUFBOztBQUlsQjtFQUNFLGVBQWUsRUFBQTs7QUFHakI7RUFDRSxtQkFBbUI7RUFDbkIsZUFBZSxFQUFBOztBQUVqQjtFQUNFLGlCQUFpQjtFQUNqQixXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLHlCUnpMbUI7RVEwTG5CLDBDQUEwQztFQUMxQyxTQUFTO0VBQ1QsWUFBWTtFQUNaLGFBQWE7RUFDYixpQkFBaUI7RUFDakIsZUFBZSxFQUFBOztBQVZqQjtJQVlJLCtDQUErQztJQUMvQyxnQkFBZ0I7SUFDaEIsb0JBQW9CO0lBQ3BCLGVBQWU7SUFDZixxQkFBcUI7SUFDckIsY0FBeUIsRUFBQTs7QUFqQjdCO0lBWUksK0NBQStDO0lBQy9DLGdCQUFnQjtJQUNoQixvQkFBb0I7SUFDcEIsZUFBZTtJQUNmLHFCQUFxQjtJQUNyQixjQUF5QixFQUFBOztBQWpCN0I7SUFZSSwrQ0FBK0M7SUFDL0MsZ0JBQWdCO0lBQ2hCLG9CQUFvQjtJQUNwQixlQUFlO0lBQ2YscUJBQXFCO0lBQ3JCLGNBQXlCLEVBQUE7O0FBakI3QjtJQVlJLCtDQUErQztJQUMvQyxnQkFBZ0I7SUFDaEIsb0JBQW9CO0lBQ3BCLGVBQWU7SUFDZixxQkFBcUI7SUFDckIsY0FBeUIsRUFBQTs7QUFqQjdCO0lBb0JJLGdCQUFnQixFQUFBOztBQUt0QjtFQUNFLGtCQUFrQixFQUFBOztBQURwQjtJQUdFLGtCQUFrQjtJQUNsQixpQkFBaUI7SUFDakIsWUFBWSxFQUFBOztBQUxkO01BT00sWUFBYztNQUNkLHlCUnROaUI7TVF1TmpCLFlBQVk7TUFDWixXQUFXO01BQ1gsU0FBUztNQUNULGFBQWE7TUFDYixrQkFBa0I7TUFDbEIsbUJBQW1CO01BQ25CLDBDQUF1QztNQUN2QyxrQkFBa0I7TUFDbEIsaUJBQWlCLEVBQUE7O0FBakJ2QjtRQW1CUSxjUm5PZTtRUW9PZixvQ0FBb0M7UUFDcEMsZUFBZTtRQUNmLGdCQUFnQjtRQUNoQixxQkFBcUI7UUFDckIsaUJBQWlCLEVBQUE7O0FBeEJ6QjtRQW1CUSxjUm5PZTtRUW9PZixvQ0FBb0M7UUFDcEMsZUFBZTtRQUNmLGdCQUFnQjtRQUNoQixxQkFBcUI7UUFDckIsaUJBQWlCLEVBQUE7O0FBeEJ6QjtRQW1CUSxjUm5PZTtRUW9PZixvQ0FBb0M7UUFDcEMsZUFBZTtRQUNmLGdCQUFnQjtRQUNoQixxQkFBcUI7UUFDckIsaUJBQWlCLEVBQUE7O0FBeEJ6QjtRQW1CUSxjUm5PZTtRUW9PZixvQ0FBb0M7UUFDcEMsZUFBZTtRQUNmLGdCQUFnQjtRQUNoQixxQkFBcUI7UUFDckIsaUJBQWlCLEVBQUE7O0FBeEJ6QjtNQTRCTSxXQUFXO01BQ1gsWUFBWTtNQUNaLGtCQUFrQjtNQUNsQixRQUFRO01BQ1IsU0FBUztNQUNULGtCQUFrQjtNQUNsQixpQkFBaUIsRUFBQTs7QUFsQ3ZCO01BcUNNLFlBQVk7TUFDWixjQUFjLEVBQUE7O0FBSXBCO0VBRUksY0FBYztFQUNkLFVBQVU7RUFDVixlQUFlO0VBQ2YsZ0JBQWdCLEVBQUE7O0FDOVJwQjtFQUNFLFdBQVc7RUFDWCx5QlR3Q21CO0VTdkNuQixrQkFBa0I7RUFDbEIsY0FBYztFQUNkLG9CQUFhO0VBQWIsYUFBYTtFQUNiLDRCQUFzQjtFQUF0Qiw2QkFBc0I7VUFBdEIsc0JBQXNCO0VBQ3RCLHdCQUF1QjtVQUF2Qix1QkFBdUI7RUFDdkIseUJBQW1CO1VBQW5CLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLFdBQVcsRUFBQTs7QUFYYjtJQWdCSSxnQkFBZ0IsRUFBQTs7QUFoQnBCO0lBbUJJLGdCQUFnQjtJQUNoQixXQUFXO0lBQ1gsb0JBQWE7SUFBYixhQUFhO0lBQ2Isd0JBQXVCO1lBQXZCLHVCQUF1QixFQUFBOztBQXRCM0I7TUF3QlEsNEJBQXNCO01BQXRCLDZCQUFzQjtjQUF0QixzQkFBc0IsRUFBQTs7QUF4QjlCO01BNEJNLGtCQUFrQjtNQUNsQixjQUFjO01BRWQsZ0JBQWdCLEVBQUE7O0FBL0J0QjtRQWlDUSxXQUFXO1FBQ1gsWUFBWTtRQUNaLG1CQUFtQjtRQUNuQixvQkFBaUI7V0FBakIsaUJBQWlCLEVBQUE7O0FBcEN6QjtRQXVDUSxZQUFZO1FBRVosZUFBZTtRQUNmLGdCQUFnQjtRQUNoQix1QkFBdUI7UUFDdkIsb0JBQW9CO1FBQ3BCLHFCQUFxQjtRQUNyQiw4QkFBQTtRQUNBLDRCQUE0QixFQUFBOztBQS9DcEM7UUFrRFEsZUFBZSxFQUFBOztBQWxEdkI7UUFxRFEsYUFBYSxFQUFBOztBQXJEckI7VUF1RFUsV0FBVztVQUNYLFlBQVksRUFBQTs7QUF4RHRCO1VBMkRZLFNBQVMsRUFBQTs7QUEzRHJCO1FBK0RRLGtCQUFrQjtRQUNsQixTQUFTO1FBQ1QsVUFBVSxFQUFBOztBQWpFbEI7VUFtRVUsV0FBVztVQUNYLFlBQVk7VUFDWix5QlQzQlcsRUFBQTs7QVMxQ3JCO0lBMkVJLGdCQUFnQjtJQUNoQixXQUFXLEVBQUE7O0FDNUVmO0VBQ0ksd0JBQXdCO0VBQ3hCLDZDQUE2QyxFQUFBOztBQUUvQztFQUNFLHdCQUF3QjtFQUN4Qiw4Q0FBOEM7RUFDOUMsZ0JBQWdCLEVBQUE7O0FDTHBCO0VBQ0UscUJBQWdCO0VBQ2hCLG1CQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLG1CQUFtQixFQUFBOztBQUpyQjtJQU1JLHNCQUFpQjtJQUNqQixvQkFBb0IsRUFBQTs7QUFQeEI7SUFjSSxzQkFBaUI7SUFDakIsb0JBQW9CLEVBQUE7O0FBZnhCO0lBa0JJLG1CQUFjO0lBQ2QsaUJBQWlCLEVBQUE7O0FBbkJyQjtJQXNCSSxtQkFBYztJQUNkLGlCQUFpQixFQUFBOztBQXZCckI7SUEwQkksbUJBQWM7SUFDZCxpQkFBaUIsRUFBQTs7QUEzQnJCO0lBOEJJLGtCQUFjO0lBQ2QsZ0JBQWdCLEVBQUE7O0FBL0JwQjtJQWtDSSxtQkFBYztJQUNkLGlCQUFpQixFQUFBOztBQW5DckI7SUFzQ0ksb0JBQWdCO0lBQ2hCLGtCQUFrQixFQUFBOztBQXZDdEI7SUEwQ0kseUJYRHNCO0lXRXRCLGtCQUFrQixFQUFBOztBQTNDdEI7TUE4Q00sZ0JBQWdCLEVBQUE7O0FBTXRCO0VBQ0UsU0FBUyxFQUFBOztBQUlYO0VBQ0UsZ0JBQWdCLEVBQUE7O0FBRWxCO0VBQ0UsMEJBQTBCLEVBQUE7O0FBRzVCO0VBQ0UsMEJBQTBCLEVBQUE7O0FBRzVCO0VBQ0UsMEJBQTBCLEVBQUE7O0FBRTVCO0VBQ0UsMEJBQTBCLEVBQUE7O0FBRTVCO0VBQ0UsMEJBQTBCLEVBQUE7O0FBRTVCO0VBQ0UsZ0JBQWdCLEVBQUE7O0FBRWxCO0VBQ0UsMkJBQTJCLEVBQUE7O0FBRTdCO0VBQ0UsMkJBQTJCLEVBQUE7O0FBRTdCO0VBQ0UsZ0JBQWdCLEVBQUE7O0FBRWxCO0VBQ0UsZ0JBQWdCLEVBQUE7O0FBRWxCO0VBQ0UsMkJBQTJCLEVBQUE7O0FBRTdCO0VBQ0UsMkJBQTJCLEVBQUE7O0FBRTdCO0VBQ0UsMkJBQTJCLEVBQUE7O0FBRTdCO0VBQ0UsZ0JBQWdCLEVBQUE7O0FBS2xCO0VBQ0UsMkJBQTJCLEVBQUE7O0FBRTdCO0VBQ0Usa0JBQWtCLEVBQUE7O0FBRXBCO0VBQ0UsbUJBQW1CLEVBQUE7O0FBRXJCO0VBQ0UsbUJBQW1CLEVBQUE7O0FBRXJCO0VBQ0UsbUJBQW1CLEVBQUE7O0FBRXJCO0VBQ0UsOEJBQThCLEVBQUE7O0FBRWhDO0VBQ0UsbUJBQW1CLEVBQUE7O0FBRXJCO0VBQ0UsOEJBQThCLEVBQUE7O0FBRWhDO0VBQ0UsOEJBQThCLEVBQUE7O0FBRWhDO0VBQ0UsOEJBQThCLEVBQUE7O0FBRWhDO0VBQ0UsOEJBQThCLEVBQUE7O0FBSWhDO0VBQ0Usa0JBQWtCLEVBQUE7O0FBR3BCO0VBQ0UsaUJBQWlCLEVBQUE7O0FBSW5CO0VBQ0UsVUFBVSxFQUFBOztBQUVaO0VBQ0UseUJBQXlCLEVBQUE7O0FBRTNCO0VBQ0UsMkJBQTJCLEVBQUE7O0FBRTdCO0VBQ0UsMkJBQTJCLEVBQUE7O0FBRTdCO0VBQ0UsMkJBQTJCLEVBQUE7O0FBRTdCO0VBQ0UsOEJBQWM7RUFDZCw0QkFBNEIsRUFBQTs7QUFFOUI7RUFDRSw4QkFBYztFQUNkLDRCQUE0QixFQUFBOztBQUU5QjtFQUNFLGlCQUFpQixFQUFBOztBQUduQjtFQUNFLG1CQUFtQixFQUFBOztBQUVyQjtFQUNFLG1CQUFtQixFQUFBOztBQUVyQjtFQUNFLG9CQUFvQjtFQUNwQixzQkFBaUIsRUFBQTs7QUFFbkI7RUFDRSxvQkFBb0IsRUFBQTs7QUFFdEI7RUFDRSxvQkFBb0IsRUFBQTs7QUFFdEI7RUFDRSxvQkFBb0IsRUFBQTs7QUFFdEI7RUFDRSxvQkFBb0IsRUFBQTs7QUFFdEI7RUFDRSxzQkFBaUIsRUFBQTs7QUFFbkI7RUFDRSxzQkFBaUIsRUFBQTs7QUFFbkI7RUFDRSx1QkFBaUIsRUFBQTs7QUNoTm5CO0VBRUUsb0JBQWE7RUFBYixhQUFhO0VBQ2IsV0FBVztFQUNYLHlCWnFDbUI7RVlwQ25CLGVBQWUsRUFBQTs7QUFFakI7RUFDRSxnQkFBZ0I7RUFDaEIsWUFBYTtFQUNiLHlCWitCbUI7RVk5Qm5CLHNCQUFzQjtFQUN0QixnQkFBZ0IsRUFBQTs7QUFMbEI7SUFRSSxZQUFZLEVBQUE7O0FBUmhCO01BVU0sWUFBWSxFQUFBOztBQUtsQjtFQUNFLGdCQUFnQjtFQUNoQixVQUFVO0VBQ1YscUJBQXFCO0VBQ3JCLGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLDZCQUFxQjtFQUNyQix5Q0FBa0I7RUFDbEIsNkNBQTBCO0VBQzFCLGdCQUFRO0VBQ1Isd0JBQWdCO0VBQ2hCLDJCQUFlO0VBQ2YsU0FBUztFQUNULGdDWkxxQjtFWU1yQixjQUFjO0VBQ2QsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQix1QkFBdUI7RUFDdkIsMEJBQTBCLEVBQUE7O0FBRzVCO0VBRUksVUFBVSxFQUFBOztBQUdkO0VBRUksYUFBYSxFQUFBOztBQUdqQjtFQUVJLFVBQVUsRUFBQTs7QUFLZDtFQUNFLGFBQWMsRUFBQTs7QUFEaEI7SUFHSSxZQUFZO0lBQ1osV0FBVztJQUNYLGtCQUFrQjtJQUNsQiwwQkFBMEI7SUFDMUIseUJaL0JtQjtJWWdDbkIsOEJBQThCO0lBQzlCLDBCQUEwQixFQUFBOztBQVQ5QjtNQVdNLDRDQUF3RSxFQUFBOztBQVg5RTtNQWNNLDhDQUEwRSxFQUFBOztBQWRoRjtNQWlCTSxnREFBNEUsRUFBQTs7QUFqQmxGO0lBc0JJLFNBQVM7SUFDVCxnQkFBZ0I7SUFDaEIsaURBQTBCO0lBQzFCLGdDWmxGa0I7SVltRmxCLG1CWjlDaUIsRUFBQTs7QVlvQnJCO01BNEJNLHlCQUFnQyxFQUFBOztBQTVCdEM7UUE4QlEseUJadkZjLEVBQUE7O0FZeUR0QjtNQWtDTSxjWjNGZ0IsRUFBQTs7QVl5RHRCO1FBb0NRLHlCWjdGYyxFQUFBOztBWXlEdEI7TUF3Q00sY1pqR2dCLEVBQUE7O0FZeUR0QjtRQTBDUSx5QlpuR2MsRUFBQTs7QVl5RHRCO0lBZ0RJLGdCQUFnQjtJQUNoQix5QlpyRWlCO0lZc0VqQixzQkFBc0IsRUFBQTs7QUFsRDFCO01Bb0RNLFlBQVksRUFBQTs7QUFwRGxCO1FBc0RRLFlBQVksRUFBQTs7QUF0RHBCO0lBNERJLGVBQWUsRUFBQTs7QUFJbkI7RUFDRSxhQUFhLEVBQUE7O0FBRGY7SUFHSSxZQUFZO0lBQ1osV0FBVztJQUNYLDBCQUEwQjtJQUMxQixrQkFBa0I7SUFDbEIsa0JBQWtCO0lBQ2xCLHlDQUF5QztJQUN6QyxxQ0FBcUM7SUFDckMseUJabEdtQixFQUFBOztBWXdGdkI7TUFhTSxrREFBOEUsRUFBQTs7QUFicEY7TUFpQk0sNENBQXdFLEVBQUE7O0FBakI5RTtNQXFCTSw4Q0FBMEUsRUFBQTs7QUFyQmhGO01BeUJNLGdEQUE0RSxFQUFBOztBQXpCbEY7SUE4QkksU0FBUztJQUNULGdDWnhKa0I7SVl5SmxCLGlEQUEwQjtJQUMxQixnQkFBZ0I7SUFDaEIsbUJadEhpQixFQUFBOztBWW9GckI7TUFxQ00sY1o5SmdCLEVBQUE7O0FZeUh0QjtNQXlDTSxjWmxLZ0IsRUFBQTs7QVl5SHRCO01BNkNNLGNadEtnQixFQUFBOztBWXlIdEI7TUFpRE0sY1oxS2dCLEVBQUE7O0FZeUh0QjtNQXFETSxxQlo5S2dCLEVBQUE7O0FZeUh0QjtNQXdETSxxQlpqTGdCLEVBQUE7O0FZeUh0QjtNQTJETSxxQlpwTGdCLEVBQUE7O0FZeUh0QjtNQThETSxxQlp2TGdCLEVBQUE7O0FZeUh0QjtNQWlFTSxtQloxTGdCLEVBQUE7O0FhTHRCO0VBQ0UsWUFBWSxFQUFBOztBQURkO0lBR0ksb0JBQWE7SUFBYixhQUFhO0lBQ2IsY0FBYztJQUNkLGVBQWUsRUFBQTs7QUFMbkI7TUFPTSxZQUFZO01BQ1osV0FBVztNQUNYLGNBQWM7TUFDZCxrQkFBa0I7TUFDbEIsMkJBQTJCO01BQzNCLHNCQUFzQjtNQUN0Qiw0QkFBNEIsRUFBQTs7QUFibEM7UUFnQlEsZ0JBQWdCO1FBQ2hCLDJCQUEyQjtRQUMzQixzQkFBc0I7UUFDdEIsNEJBQTRCLEVBQUE7O0FBbkJwQztRQXNCUSxrQkFBa0I7UUFDbEIsV0FBVztRQUNYLFFBQVE7UUFDUixTQUFTO1FBQ1Qsd0NBQWdDO2dCQUFoQyxnQ0FBZ0MsRUFBQTs7QUFLeEM7RUFDRSxZQUFZO0VBQ1osbUJib0JtQjtFYW5CbkIsa0JBQWtCO0VBQ2xCLG9CQUFhO0VBQWIsYUFBYTtFQUNiLFdBQVcsRUFBQTs7QUFMYjtJQU9JLFdBQVcsRUFBQTs7QUFQZjtJQVVJLG9CQUFhO0lBQWIsYUFBYTtJQUNiLDRCQUFzQjtJQUF0Qiw2QkFBc0I7WUFBdEIsc0JBQXNCO0lBQ3RCLFdBQVc7SUFDWCxvQkFBb0IsRUFBQTs7QUFieEI7SUFnQkksa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCxXQUFXO0lBQ1gsa0JBQWtCO0lBQ2xCLFFBQVE7SUFDUixxQ0FBNkI7WUFBN0IsNkJBQTZCLEVBQUE7O0FBckJqQztNQXVCTSxXQUFXLEVBQUE7O0FBdkJqQjtNQTBCTSxrQkFBa0I7TUFDbEIsUUFBUTtNQUNSLFNBQVM7TUFDVCx3Q0FBZ0M7Y0FBaEMsZ0NBQWdDO01BQ2hDLGdCQUFnQixFQUFBOztBQUl0QjtFQUNFLFlBQVk7RUFDWixvQkFBYTtFQUFiLGFBQWE7RUFDYix5QmJmbUI7RWFnQm5CLGdCQUFnQjtFQUNoQiwrQ0FBK0MsRUFBQTs7QUFMakQ7SUFPSSx5QkFBOEI7SUFDOUIsZ0JBQWdCLEVBQUE7O0FBUnBCO0lBV0ksU0FBUztJQUNULFFBQVEsRUFBQTs7QUFaWjtJQWVJLFdBQVcsRUFBQTs7QUFHZjtFQUNFLGdCQUFnQjtFQUNoQix5QmJoQ21CO0VhaUNuQixrQkFBa0I7RUFDbEIsU0FBUztFQUNULG9DQUFvQztFQUNwQyxXQUFXLEVBQUE7O0FBTmI7SUFRSSxnQkFBZ0I7SUFDaEIsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNsQixpQkFBaUIsRUFBQTs7QUFHckI7RUFDRSxXQUFXLEVBQUE7O0FBRGI7SUFJTSxpQ0FBaUM7SUFDakMsb0JBQW9CO0lBQ3BCLHlCYmxEZTtJYW1EZix3QkFBd0IsRUFBQTs7QUFQOUI7SUFXSSxtQmJ2RGlCLEVBQUE7O0FhMERyQjtFQUNFLG9CQUFhO0VBQWIsYUFBYTtFQUNiLGVBQWUsRUFBQTs7QUFGakI7SUFLSSxjQUFjO0lBQ2QsYUFBYSxFQUFBOztBQ3JIakI7RUFDRSx5QkFBYSxFQUFBOztBQURmO0lBR0kseUJBQWEsRUFBQTs7QUFIakI7SUFNSSx5QmRvQ2lCO0ljbkNqQixrQkFBa0I7SUFDbEIsU0FBUztJQUNULFdBQVc7SUFDWCxhQUFhO0lBQ2IsaUJBQWlCO0lBQ2pCLDRCQUE0QixFQUFBOztBQVpoQztNQWVRLG9CQUFhO01BQWIsYUFBYTtNQUNiLHlCQUFtQjtjQUFuQixtQkFBbUI7TUFDbkIsWUFBWSxFQUFBOztBQWpCcEI7UUFtQlUsa0JBQWtCO1FBQ2xCLFdBQVc7UUFDWCxZQUFZO1FBQ1osY0FBYyxFQUFBOztBQXRCeEI7UUF5QlUsZ0NkSWE7UWNIYixZQUFZO1FBQ1osb0JBQWE7UUFBYixhQUFhO1FBQ2IseUJBQW1CO2dCQUFuQixtQkFBbUI7UUFDbkIsV0FBVyxFQUFBOztBQTdCckI7UUFpQ2dCLFNBQVMsRUFBQTs7QUFqQ3pCO1FBcUNVLFlBQVksRUFBQTs7QUFyQ3RCO1VBdUNZLFdBQVc7VUFDWCxZQUFZLEVBQUE7O0FBeEN4QjtJQStDSSxhQUFhLEVBQUE7O0FBR2pCO0VBQ0UsZ0JBQWdCLEVBQUE7O0FDbkRsQjtFQUVDLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFDZixxQkFBcUI7RUFDckIsY0FBNkIsRUFBQTs7QUFFOUI7RUFFQyxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBQ2YscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxlQUFlLEVBQUE7O0FBRWhCO0VBRUMsb0NBQW9DO0VBQ3BDLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsMEJBQTBCO0VBQzFCLHFCQUFxQjtFQUNyQixjQUFjO0VBQ2QsZUFBZSxFQUFBOztBQVJoQjtJQVVFLGNBQWMsRUFBQTs7QUFJaEI7RUFFQyxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBQ2YscUJBQXFCO0VBQ3JCLFlBQTZCLEVBQUE7O0FBRTlCO0VBRUMsb0NBQW9DO0VBQ3BDLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixjQUFjLEVBQUE7O0FBRWY7RUFFQyxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBQ2YscUJBQXFCO0VBQ3JCLGlCQUFpQjtFQUNqQixZQUE2QixFQUFBOztBQUU5QjtFQUVDLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFDZixxQkFBcUI7RUFDckIsY0FBYyxFQUFBOztBQUVmO0VBRUMsb0NBQW9DO0VBQ3BDLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUNmLHFCQUFxQjtFQUNyQixpQkFBaUI7RUFDakIsY0FBYyxFQUFBOztBQUVkO0VBR0MsK0NBQStDO0VBQy9DLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUNmLHFCQUFxQjtFQUNyQixpQkFBaUI7RUFDakIsY0FBYyxFQUFBOztBQVRmO0VBWUEsb0NBQW9DO0VBQ3BDLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUNmLHFCQUFxQjtFQUNyQixjQUFjLEVBQUE7O0FBakJkO0VBcUJBLDBEQUEwRDtFQUMxRCxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsY0FBYyxFQUFBOztBQ3RHZjtFQUNFLGNBQWM7RUFDZCxvQ0FBb0M7RUFDcEMsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixrQkFBa0IsRUFBQTs7QUFFcEI7RUFDRSxjQUFjO0VBQ2Qsb0NBQW9DO0VBQ3BDLGVBQWU7RUFDZixrQkFBa0IsRUFBQTs7QUFHcEI7RUFDRSxjQUFjO0VBQ2Qsb0NBQW9DO0VBQ3BDLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsa0JBQWtCLEVBQUE7O0FBRXBCO0VBQ0UsY2hCS3FCO0VnQkpyQixvQ0FBb0M7RUFDcEMsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixrQkFBa0IsRUFBQTs7QUFHcEI7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLGNBQTZCLEVBQUE7O0FBRy9CO0VBQ0Usb0NBQW9DO0VBQ3BDLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsY0FBYztFQUNkLG1CQUFtQjtFQUNuQixjQUEwQixFQUFBOztBQUc1QjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGNBQWM7RUFDZCxtQkFBbUI7RUFDbkIsY0FBNkIsRUFBQTs7QUFHL0I7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLGNBQTZCLEVBQUE7O0FBRy9CO0VBQ0Usb0NBQW9DO0VBQ3BDLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUVmLGNBQTZCLEVBQUE7O0FBRy9CO0VBQ0Usb0NBQW9DO0VBQ3BDLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixjQUE2QixFQUFBOztBQUcvQjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixjQUE2QixFQUFBOztBQUcvQjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFDZixjQUE2QixFQUFBOztBQUcvQjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFDZixxQkFBcUI7RUFDckIsY0FBNkI7RUFDN0IsZ0JBQWdCLEVBQUE7O0FBR2xCO0VBQ0Usb0NBQW9DO0VBQ3BDLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUVmLGNBQWMsRUFBQTs7QUFHaEI7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBRWYsY0FBNkIsRUFBQTs7QUFHL0I7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBQ2YsbUJBQW1CO0VBQ25CLFlBQTZCLEVBQUE7O0FBRy9CO0VBQ0Usb0NBQW9DO0VBQ3BDLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUVmLGNBQTZCLEVBQUE7O0FBRy9CO0VBQ0Usb0NBQW9DO0VBQ3BDLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUVmLGNBQTZCLEVBQUE7O0FBRy9CO0VBQ0Usb0NBQW9DO0VBQ3BDLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUNmLHFCQUFxQjtFQUNyQixjQUE2QixFQUFBOztBQUcvQjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixjQUE2QixFQUFBOztBQUcvQjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixjQUE2QixFQUFBOztBQUcvQjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixjQUE2QixFQUFBOztBQUcvQjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixjQUE2QixFQUFBOztBQUcvQjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixjQUFjLEVBQUE7O0FBR2hCO0VBQ0Usb0NBQW9DO0VBQ3BDLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUVmLGNBQTZCLEVBQUE7O0FBRy9CO0VBQ0Usb0NBQW9DO0VBQ3BDLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUVmLGNBQTZCLEVBQUE7O0FBRy9CO0VBQ0Usb0NBQW9DO0VBQ3BDLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUVmLGNBQTZCLEVBQUE7O0FBRy9CO0VBQ0Usb0NBQW9DO0VBQ3BDLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUVmLGNBQTZCLEVBQUE7O0FBRy9CO0VBQ0Usb0NBQW9DO0VBQ3BDLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUVmLGNBQTZCLEVBQUE7O0FBRy9CO0VBQ0Usb0NBQW9DO0VBQ3BDLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUVmLGNBQTZCLEVBQUE7O0FBRy9CO0VBQ0Usb0NBQW9DO0VBQ3BDLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUNmLGNBQWMsRUFBQTs7QUFFaEI7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBQ2YsY0FBYyxFQUFBOztBQUdoQjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixjQUE2QixFQUFBOztBQUcvQjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixjQUE2QixFQUFBOztBQUcvQjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixjQUE2QixFQUFBOztBQUcvQjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixjQUFjO0VBQ2QsV0FBVyxFQUFBOztBQUdiO0VBQ0Usb0NBQW9DO0VBQ3BDLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUVmLGNBQTZCLEVBQUE7O0FBRy9CO0VBQ0Usb0NBQW9DO0VBQ3BDLG9CQUFvQjtFQUNwQixlQUFlO0VBQ2YscUJBQXFCO0VBQ3JCLGlCQUFpQjtFQUNqQixjaEI1VW1CO0VnQjZVbkIsZUFBZSxFQUFBOztBQVBqQjtJQVVJLGVBQWU7SUFDZixnQkFBZ0IsRUFBQTs7QUFJcEI7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBRWYsY0FBNkIsRUFBQTs7QUFHL0I7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBRWYsY0FBMkIsRUFBQTs7QUFHN0I7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBRWYsY0FBYyxFQUFBOztBQUdoQjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixjQUE2QixFQUFBOztBQUcvQjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixjQUE2QixFQUFBOztBQUcvQjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixjQUFjLEVBQUE7O0FBR2hCO0VBQ0Usb0NBQW9DO0VBQ3BDLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUVmLGNBQTZCLEVBQUE7O0FBRy9CO0VBQ0UsY0FBYztFQUNkLGVBQWU7RUFDZixxQkFBcUIsRUFBQTs7QUFFdkI7RUFDRSxjaEJqWnFCO0VnQmtackIsZUFBZTtFQUNmLHFCQUFxQjtFQUNyQixnQkFBZ0IsRUFBQTs7QUFKbEI7SUFNSSxjaEIvWmlCLEVBQUE7O0FnQnlackI7SUFTSSxjQUFjLEVBQUE7O0FBVGxCO0lBWUksZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixnQkFDRixFQUFBOztBQUdGO0VBQ0Usb0NBQW9DO0VBQ3BDLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUVmLGNBQWMsRUFBQTs7QUFHaEI7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBRWYsY0FBMkIsRUFBQTs7QUFHN0I7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBRWYsY0FBMkIsRUFBQTs7QUFHN0I7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBRWYsY0FBMEIsRUFBQTs7QUFHNUI7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBQ2YsY0FBNkIsRUFBQTs7QUFHL0I7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBRWYsY0FBMEIsRUFBQTs7QUFHNUI7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBRWYsY0FBMEIsRUFBQTs7QUFFNUI7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBQ2YsY2hCemNxQjtFZ0IwY3JCLDZCQUE2QixFQUFBOztBQU4vQjtJQVFJLGNoQm5kbUIsRUFBQTs7QWdCdWR2QjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixjQUE2QixFQUFBOztBQUcvQjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixjQUEwQixFQUFBOztBQUc1QjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixjQUEwQixFQUFBOztBQUc1QjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixZQUE2QixFQUFBOztBQUcvQjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixjQUE2QixFQUFBOztBQUcvQjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixjQUEwQixFQUFBOztBQUc1QjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixjQUEwQixFQUFBOztBQUc1QjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixjQUEwQixFQUFBOztBQUc1QjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFDZixjQUE2QixFQUFBOztBQUcvQjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFDZixjQUFjLEVBQUE7O0FBSWhCO0VBQ0Usb0NBQW9DO0VBQ3BDLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUVmLGNBQWMsRUFBQTs7QUFHaEI7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBRWYsY0FBYyxFQUFBOztBQUdoQjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixjQUE2QixFQUFBOztBQUcvQjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixjQUEwQixFQUFBOztBQUc1QjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixjQUEwQixFQUFBOztBQUc1QjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixjQUE2QixFQUFBOztBQUcvQjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixZQUE2QixFQUFBOztBQUcvQjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixZQUE2QixFQUFBOztBQUcvQjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixjQUEwQixFQUFBOztBQUc1QjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixjQUFjLEVBQUE7O0FBR2hCO0VBQ0Usb0NBQW9DO0VBQ3BDLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUVmLGNBQWMsRUFBQTs7QUFHaEI7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBRWYsY0FBMEIsRUFBQTs7QUFHNUI7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBRWYsY0FBYyxFQUFBOztBQUdoQjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixjQUE2QixFQUFBOztBQUcvQjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixjQUEyQixFQUFBOztBQUc3QjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixjQUEyQixFQUFBOztBQUc3QjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixjQUFjLEVBQUE7O0FBR2hCO0VBQ0Usb0NBQW9DO0VBQ3BDLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUVmLGNBQTBCLEVBQUE7O0FBRzVCO0VBQ0UsK0NBQStDO0VBQy9DLGdCQUFnQjtFQUVoQixlQUFlO0VBQ2YscUJBQXFCO0VBQ3JCLGNBQTBCLEVBQUE7O0FBRTVCO0VBQ0Usb0NBQW9DO0VBQ3BDLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUNmLGNBQTBCLEVBQUE7O0FBRzVCO0VBQ0Usb0NBQW9DO0VBQ3BDLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUVmLGNBQTBCLEVBQUE7O0FBRzVCO0VBQ0Usb0NBQW9DO0VBQ3BDLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUVmLGNBQWMsRUFBQTs7QUFHaEI7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBRWYsY0FBMEIsRUFBQTs7QUFHNUI7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBRWYsY0FBMEIsRUFBQTs7QUFHNUI7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBRWYsY0FBMEIsRUFBQTs7QUFHNUI7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBRWYsY0FBMEIsRUFBQTs7QUFHNUI7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBRWYsY0FBNkIsRUFBQTs7QUFHL0I7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBRWYsY0FBMEIsRUFBQTs7QUFHNUI7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBRWYsY0FBMEIsRUFBQTs7QUFHNUI7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBRWYsY0FBYTtFQUNiLFdBQVcsRUFBQTs7QUFHYjtFQUNFLG9DQUFvQztFQUNwQyxnQkFBZ0I7RUFDaEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFFZixjQUFjLEVBQUE7O0FBR2hCO0VBQ0Usb0NBQW9DO0VBQ3BDLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUVmLGNBQTBCLEVBQUE7O0FBRzVCO0VBQ0Usb0NBQW9DO0VBQ3BDLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFFcEIsY0FBMEIsRUFBQTs7QUFHNUI7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBRWYsY0FBMEIsRUFBQTs7QUFHNUI7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBRWYsY0FBMEIsRUFBQTs7QUFHNUI7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBRWYsWUFBNkIsRUFBQTs7QUFHL0I7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBQ2Ysc0JBQXNCO0VBQ3RCLGNBQTJCLEVBQUE7O0FBRzdCO0VBQ0Usb0NBQW9DO0VBQ3BDLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUVmLGNBQWMsRUFBQTs7QUFHaEI7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBRWYsY0FBMkIsRUFBQTs7QUFHN0I7RUFDRSxvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixlQUFlO0VBQ2Ysc0JBQXNCO0VBQ3RCLGNBQTJCLEVBQUE7O0FBRzdCO0VBQ0Usb0NBQW9DO0VBQ3BDLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixZQUF1QixFQUFBOztBQUt6QjtFQUNFLGNBQWM7RUFDZCxvQ0FBb0M7RUFDcEMsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixxQkFBcUIsRUFBQTs7QUFFdkI7RUFDRSxjQUFjO0VBQ2Qsb0NBQW9DO0VBQ3BDLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsV0FBVyxFQUFBOztBQUViO0VBQ0UsY0FBYztFQUNkLG9DQUFvQztFQUNwQyxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGtCQUFrQixFQUFBOztBQUVwQjtFQUNFLGNoQng3QnFCO0VnQnk3QnJCLG9DQUFvQztFQUNwQyxlQUFlO0VBQ2YsZ0JBQWdCLEVBQUE7O0FBRWxCO0VBQ0UsY2hCbDdCbUI7RWdCbTdCbkIsY0FBYztFQUNkLGlCQUFpQjtFQUNqQixrQkFBa0IsRUFBQTs7QUFFbEI7RUFDRSxjaEJ4N0JpQjtFZ0J5N0JqQiwwQkFBMEI7RUFDMUIsZUFBZTtFQUNmLGdCQUFnQixFQUFBOztBQUVsQjtFQUNJLGNBQWM7RUFDZCxvQ0FBb0M7RUFDcEMsZUFBZTtFQUNmLGdCQUFnQixFQUFBOztBQUVwQjtFQUNFLGNBQWM7RUFDZCxvQ0FBb0M7RUFDcEMsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixrQkFBa0IsRUFBQTs7QUFFcEI7RUFDRSxjQUFjO0VBQ2Qsb0NBQW9DO0VBQ3BDLGVBQWU7RUFDZixnQkFBZ0IsRUFBQTs7QUFFbEI7RUFDRSxjQUFjO0VBQ2Qsd0JBQXdCO0VBQ3hCLGVBQWU7RUFDZixpQkFBaUI7RUFDakIscUJBQXFCO0VBQ3JCLGtCQUFrQixFQUFBOztBQUdsQjtFQUNFLGNoQjk5QmlCO0VnQis5QmpCLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIscUJBQXFCLEVBQUE7O0FBSnZCO0lBTUksZUFBZSxFQUFBOztBQUluQjtFQUNFLGNoQmgvQmlCO0VnQmkvQmpCLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIscUJBQXFCLEVBQUE7O0FDamhDM0I7RUFDSSxvQkFBYTtFQUFiLGFBQWE7RUFDYiw0QkFBc0I7RUFBdEIsNkJBQXNCO1VBQXRCLHNCQUFzQjtFQUN0Qix3QkFBdUI7VUFBdkIsdUJBQXVCO0VBQ3ZCLHlCQUFtQjtVQUFuQixtQkFBbUIsRUFBQTs7QUFKdkI7SUFNUSxnQkFBZ0I7SUFDaEIsbUJBQW1CLEVBQUE7O0FBUDNCO0lBVVEsZ0JBQWdCLEVBQUE7O0FBVnhCO01BWVksY0FBYztNQUFFLG9DQUFvQztNQUNwRCxlQUFlO01BQ2YsZ0JBQWdCO01BQ2hCLHFCQUFxQjtNQUNyQixpQkFBaUI7TUFDakIsa0JBQWtCLEVBQUE7O0FBakI5QjtNQW9CWSxnQkFBZ0I7TUFDaEIsY0FBYztNQUFFLG9DQUFvQztNQUNwRCxlQUFlO01BQ2YscUJBQXFCO01BQ3JCLGlCQUFpQjtNQUNqQixrQkFBa0IsRUFBQTs7QUFLOUI7RUFDSSxrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLHdCQUF3QjtFQUN4QixjQUFjLEVBQUE7O0FBSmxCO0lBTVEsZ0JBQWdCLEVBQUE7O0FDcEN4QjtFQUVJLG1CQUFtQixFQUFBOztBQUZ2QjtFQUtJLG1CQUFtQixFQUFBOztBQUd2QjtFQUNFLG9CQUFhO0VBQWIsYUFBYTtFQUNiLHdCQUF1QjtVQUF2Qix1QkFBdUI7RUFDdkIsYUFBYSxFQUFBOztBQUhmO0lBS0kseUJBQXlCO0lBQ3pCLGtCQUFrQixFQUFBOztBQU50QjtNQVFRLGVBQWUsRUFBQTs7QUFSdkI7SUFZSSxZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLGtCQUFrQixFQUFBOztBQWR0QjtJQWlCSSwrQ0FBK0M7SUFDL0MsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixxQkFBcUI7SUFDckIsaUJBQWlCO0lBQ2pCLGtCQUFrQixFQUFBOztBQUd0QjtFQUNJLG9CQUFhO0VBQWIsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQix3QkFBdUI7VUFBdkIsdUJBQXVCLEVBQUE7O0FBSDNCO0lBS1EsVUFBVTtJQUNWLFlBQVk7SUFDWix5QmxCWGU7SWtCWWYsZ0JBQWdCLEVBQUE7O0FDekN4QjtFQUNFLGlCQUFpQjtFQUNqQixvQkFBYTtFQUFiLGFBQWE7RUFDYiw0QkFBc0I7RUFBdEIsNkJBQXNCO1VBQXRCLHNCQUFzQjtFQUN0Qix3QkFBdUI7VUFBdkIsdUJBQXVCO0VBQ3ZCLHlCQUFtQjtVQUFuQixtQkFBbUIsRUFBQTs7QUFMckI7SUFPSSxXQUFXO0lBQ1gsWUFBWSxFQUFBOztBQVJoQjtNQVVNLFdBQVc7TUFDWCxZQUFZLEVBQUE7O0FBWGxCO0lBZUksZ0JBQWdCO0lBQ2hCLFdBQVc7SUFDWCxrQkFBa0IsRUFBQTs7QUFqQnRCO0lBb0JJLGVBQWU7SUFDZixrQkFBa0IsRUFBQTs7QUFyQnRCO0lBd0JJLGdCQUFnQjtJQUNoQixXQUFXLEVBQUE7O0FBSWY7RUFFRSxvQkFBb0IsRUFBQTs7QUFGdEI7SUFJSSxTQUFTO0lBQ1QsVUFBVSxFQUFBOztBQUxkO0lBUUksZ0JBQWdCO0lBQ2hCLHVCQUF1QjtJQUN2QixvQkFBb0I7SUFDcEIscUJBQXFCO0lBQ3JCLDhCQUFBO0lBQ0EsNEJBQTRCO0lBQzVCLGVBQWUsRUFBQTs7QUFkbkI7SUFpQkksV0FBVyxFQUFBOztBQWpCZjtNQW1CTSxvQkFBYTtNQUFiLGFBQWE7TUFDYixjQUFjO01BQ2QsNEJBQXNCO01BQXRCLDZCQUFzQjtjQUF0QixzQkFBc0I7TUFDdEIseUJBQW1CO2NBQW5CLG1CQUFtQjtNQUNuQixrQkFBa0IsRUFBQTs7QUF2QnhCO1FBeUJRLG1CQUFtQjtRQUNuQixjQUFjLEVBQUE7O0FBMUJ0QjtVQTRCVSxZQUFZLEVBQUE7O0FBNUJ0QjtNQWlDTSxnQkFBZ0IsRUFBQTs7QUFqQ3RCO0lBcUNJLGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtJQUNaLHlCbkIzQmlCO0ltQjRCakIsa0JBQWtCO0lBQ2xCLGtCQUFrQixFQUFBOztBQ3RFdEI7RUFDSSxtQkFBbUI7RUFDbkIsZ0JBQWdCLEVBQUE7O0FBRXBCOztFQUVFLGNwQitCcUI7RW9COUJyQiwrQ0FBK0M7RUFDL0MsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsaUJBQWlCO0VBQ2pCLGtCQUFrQixFQUFBOztBQUVwQjtFQUNFLGNwQnNCcUI7RW9CckJyQiwrQ0FBK0M7RUFDL0MsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixvQkFBb0IsRUFBQTs7QUFFdEI7RUFDRSxvQkFBYTtFQUFiLGFBQWE7RUFDYix3QkFBdUI7VUFBdkIsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsb0JBQW9CLEVBQUE7O0FBRXRCO0VBQ0UsbUNBQWtDO1VBQWxDLGtDQUFrQztFQUNsQyxVQUFVO0VBQ1YsWUFBWTtFQUNaLFNBQVMsRUFBQTs7QUFKWDtJQU1JLGVBQWU7SUFDZixlQUFlO0lBQ2Ysa0JBQWtCLEVBQUE7O0FBUnRCO0lBV0ksaUJBQWlCO0lBQ2pCLGVBQWUsRUFBQTs7QUFabkI7O01BZVEsbUJwQkFjO01vQkNkLGNwQk5hO01vQk9iLFNBQVMsRUFBQTs7QUFJakI7O0VBRUEseUJwQmpCdUI7RW9Ca0JyQixjcEJsQnFCO0VvQm1CckIsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLHFCQUFxQjtFQUNyQixpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLG9CQUFhO0VBQWIsYUFBYTtFQUNiLHdCQUF1QjtVQUF2Qix1QkFBdUI7RUFDdkIsY0FBYyxFQUFBOztBQUloQjtFQUNJLG1CQUFtQjtFQUNuQixnQkFBZ0IsRUFBQTs7QUFFcEI7O0VBRUUsY3BCdENxQjtFb0J1Q3JCLG9DQUFvQztFQUNwQyxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLHFCQUFxQjtFQUNyQixpQkFBaUI7RUFDakIsa0JBQWtCLEVBQUE7O0FBRXBCO0VBQ0UsY3BCL0NxQjtFb0JnRHJCLG9DQUFvQztFQUNwQyxlQUFlO0VBQ2YscUJBQXFCO0VBQ3JCLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsbUJBQW1CLEVBQUE7O0FBRXJCO0VBQ0Usb0JBQWE7RUFBYixhQUFhO0VBQ2Isd0JBQXVCO1VBQXZCLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLG9CQUFvQixFQUFBOztBQUV0QjtFQUNFLG1DQUFrQztVQUFsQyxrQ0FBa0M7RUFDbEMsVUFBVSxFQUFBOztBQUZaO0lBSUksZUFBZTtJQUNmLGVBQWU7SUFDZiw2QkFBNkIsRUFBQTs7QUFOakM7SUFTSSxlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLDRCQUE0QixFQUFBOztBQVhoQzs7TUFjUSxtQnBCbkVjO01vQm9FZCxjcEJ6RWE7TW9CMEViLFNBQVMsRUFBQTs7QUFJakI7O0VBRUUsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixjcEJ0RnFCO0VvQnVGckIseUJwQnZGcUI7RW9Cd0ZyQixvQ0FBb0M7RUFDcEMsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixvQkFBYTtFQUFiLGFBQWE7RUFDYix3QkFBdUI7VUFBdkIsdUJBQXVCO0VBQ3ZCLDBCQUEyQjtFQUMzQixjQUFlLEVBQUE7O0FBRWpCO0VBRUksNEJBQXNCO0VBQXRCLDZCQUFzQjtVQUF0QixzQkFBc0IsRUFBQTs7QUFGMUI7SUFJTSxXQUFXLEVBQUE7O0FBSmpCO0VBU00sMEJBQTBCO0VBQzFCLGNBQWMsRUFBQTs7QUFWcEI7SUFZUSxvQ0FBMEMsRUFBQTs7QUFabEQ7RUFrQk0sMkJBQTJCO0VBQzNCLG1CQUFtQjtFQUNuQixpQkFBaUIsRUFBQTs7QUFwQnZCO0lBc0JRLG9DQUF5QyxFQUFBOztBQUtqRDtFQUNFLG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsNEJBQTRCLEVBQUE7O0FBRTlCO0VBQ0UsbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCw0QkFBNEIsRUFBQTs7QUFFOUI7RUFFSSxzQ0FBc0M7RUFDdEMsc0NBQXNDO0VBQ3RDLHFDQUFxQztFQUNyQyxvQ0FBb0MsRUFBQTs7QUFMeEM7RUFTSSx5QnBCdEptQjtFb0J1Sm5CLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsZ0JBQWdCLEVBQUE7O0FBWnBCO0VBZ0JRLGNwQi9KZTtFb0JnS2Ysb0NBQW9DO0VBQ3BDLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIscUJBQXFCO0VBQ3JCLGlCQUFpQixFQUFBOztBQXJCekI7RUFnQlEsY3BCL0plO0VvQmdLZixvQ0FBb0M7RUFDcEMsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsaUJBQWlCLEVBQUE7O0FBckJ6QjtFQWdCUSxjcEIvSmU7RW9CZ0tmLG9DQUFvQztFQUNwQyxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLHFCQUFxQjtFQUNyQixpQkFBaUIsRUFBQTs7QUFyQnpCO0VBZ0JRLGNwQi9KZTtFb0JnS2Ysb0NBQW9DO0VBQ3BDLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIscUJBQXFCO0VBQ3JCLGlCQUFpQixFQUFBOztBQXJCekI7RUEyQksseUJwQnhLa0I7RW9CeUtsQixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLFNBQVMsRUFBQTs7QUE5QmQ7RUFrQ1MsY3BCakxjO0VvQmtMZCxvQ0FBb0M7RUFDcEMsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsaUJBQWlCLEVBQUE7O0FBdkMxQjtFQWtDUyxjcEJqTGM7RW9Ca0xkLG9DQUFvQztFQUNwQyxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLHFCQUFxQjtFQUNyQixpQkFBaUIsRUFBQTs7QUF2QzFCO0VBa0NTLGNwQmpMYztFb0JrTGQsb0NBQW9DO0VBQ3BDLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIscUJBQXFCO0VBQ3JCLGlCQUFpQixFQUFBOztBQXZDMUI7RUFrQ1MsY3BCakxjO0VvQmtMZCxvQ0FBb0M7RUFDcEMsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsaUJBQWlCLEVBQUE7O0FBdkMxQjtFQTRDSSxnQkFBZ0I7RUFDaEIsbUJBQW1CLEVBQUE7O0FBN0N2QjtFQWlETSxpQkFBaUI7RUFDakIsZUFBZSxFQUFBOztBQWxEckI7SUFvRFUsbUJwQjdOWSxFQUFBOztBb0J5S3RCO0VBMERNLGlCQUFpQjtFQUNqQixlQUFlLEVBQUE7O0FBM0RyQjtJQTZEVSxtQnBCdE9ZLEVBQUE7O0FvQnlLdEI7RUFtRU0sNEJBQTZCLEVBQUE7O0FBSW5DO0VBRUksYUFBYyxFQUFBOztBQUdsQjtFQUNFLHdCQUF5QjtFQUN6Qix5QkFBeUIsRUFBQTs7QUFFM0I7RUFDRSx3Q0FBd0MsRUFBQTs7QUMvUDFDO0VBQ0ksZ0NyQjRCbUIsRUFBQTs7QXFCMUJyQjtFQUNFLFNBQVMsRUFBQTs7QUFHWDtFQUNFLFdBQVc7RUFDWCxXQUFXO0VBQ1gseUJyQm1CbUIsRUFBQTs7QXFCakJyQjtFQUNFLG1CckI4QnNCO0VxQjdCdEIsZ0JBQWdCLEVBQUE7O0FBRmxCO0lBSUksNkJyQmFpQjtJcUJaakIsWUFBWTtJQUNaLG1CckJ3QmU7SXFCdkJmLG9CQUFhO0lBQWIsYUFBYTtJQUNiLHlCQUFtQjtZQUFuQixtQkFBbUI7SUFDbkIsd0JBQXVCO1lBQXZCLHVCQUF1QjtJQUN2QixpQkFBaUIsRUFBQTs7QUFWckI7SUFhSSxZQUFZO0lBQ1osV0FBVztJQUNYLGdCQUFnQixFQUFBOztBQUdwQjtFQUNFLFlBQVk7RUFDWix5QnJCV3NCO0VxQlZ0QixrQkFBa0IsRUFBQTs7QUFIcEI7SUFLSSxZQUFZO0lBQ1osWUFBWTtJQUNaLFdBQVc7SUFDWCxtQnJCSWU7SXFCSGYsa0JBQWtCO0lBQ2xCLCtCQUErQjtJQUMvQixnQ0FBZ0M7SUFDaEMsMkNBQXdDO0lBQ3hDLE9BQU87SUFDUCxVQUFVLEVBQUE7O0FBZGQ7SUFpQkksWUFBWTtJQUNaLFlBQVk7SUFDWixXQUFXO0lBQ1gsbUJyQlJlO0lxQlNmLGtCQUFrQjtJQUNsQiw0QkFBNEI7SUFDNUIsNkJBQTZCO0lBQzdCLFNBQVM7SUFFVCxPQUFPLEVBQUE7O0FBMUJYO0lBOEJNLGFBQWEsRUFBQTs7QUE5Qm5CO0lBbUNNLGFBQWEsRUFBQTs7QUFuQ25CO0lBdUNJLFlBQWEsRUFBQTs7QUF2Q2pCO0lBMENJLFlBQVksRUFBQTs7QUExQ2hCO0lBNkNJLFlBQVksRUFBQTs7QUFJaEI7RUFDRSxrQkFBa0IsRUFBQTs7QUFEcEI7SUFHTSxrQkFBa0I7SUFDbEIsV0FBVztJQUNYLFlBQVk7SUFDWixtQnJCMUNrQjtJcUIyQ2xCLFNBQVMsRUFBQTs7QUFQZjtNQVNRLGFBQWE7TUFDYixjQUFjLEVBQUE7O0FDekZ4QjtFQUVNLGlDQUFpQztFQUNqQywrQ0FBK0M7RUFDL0MseUJ0QnNDZSxFQUFBOztBc0IxQ3JCO0VBUVEsYUFBYSxFQUFBOztBQUluQjtFQUVJLGlDQUFpQyxFQUFBOztBQ2R2QztFQUNJLFlBQVk7RUFDWixvQkFBYTtFQUFiLGFBQWE7RUFDYix5QkFBbUI7VUFBbkIsbUJBQW1CO0VBQ25CLG1CdkI2Qm1CO0V1QjVCbkIsb0JBQW9CO0VBQ3BCLDJDQUEyQztFQUMzQywyQkFBMkI7RUFDM0IsMkJBQTJCO0VBQzNCLDRCQUE0QjtFQUM1QixjdkJMa0IsRUFBQTs7QXVCTHRCO0lBWVEsd0RBQXdEO0lBQ3hELDBCQUEwQixFQUFBOztBQUdsQztFQUNJLDZDQUE0QyxFQUFBOztBQUVoRDtFQUNJLDJCQUEyQjtFQUMzQiwrRkFBMkY7RUFDM0Ysb0JBQW9CO0VBQ3BCLGtCQUFrQixFQUFBOztBQUp0QjtJQU1RLDZDQUE0QyxFQUFBOztBQUdwRDtFQUNJLHlCdkJJbUI7RXVCSG5CLDJCQUEyQjtFQUMzQix5Q0FBeUM7RUFDekMsMENBQTBDLEVBQUE7O0FBRTlDO0VBQ0ksY3ZCSm1CO0V1QktuQiwwQkFBMEI7RUFDMUIsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsaUJBQWlCO0VBQ2pCLDRCQUE0QixFQUFBOztBQUVoQztFQUNJLHdCQUF3QixFQUFBOztBQUU1QjtFQUNJLHlCQUF5QixFQUFBOztBQUU3QjtFQUNJLHlCQUFnQztFQUNoQyw2Q0FBNEM7RUFDNUMsa0JBQWtCLEVBQUE7O0FBRXRCO0VBQ0ksd0JBQXdCLEVBQUE7O0FBRTVCO0VBQ0kseUJBQWdDLEVBQUE7O0FBRXBDO0VBQ0ksYUFBYSxFQUFBOztBQUVqQjtFQUNJLG9CQUFvQixFQUFBOztBQUV4QjtFQUNFLHdCQUF3QixFQUFBOztBQUUxQjtFQUNJLDJDQUEyQztFQUMzQyxvQkFBb0IsRUFBQTs7QUFFeEI7RUFDSSxnQkFBZ0I7RUFDaEIsb0NBQW9DLEVBQUE7O0FBRXhDO0VBQ0ksd0JBQXdCLEVBQUE7O0FBRTVCO0VBRVEsZ0N2QjdFYztFdUI4RWQsMkJBQTJCLEVBQUE7O0FBR25DO0VBQ0ksYUFBYSxFQUFBOztBQUVqQjtFQUNJLDJDQUFrRCxFQUFBOztBQUV0RDtFQUNJLHdCQUF3QixFQUFBOztBQUc1QjtFQUNDLHdEQUF3RDtFQUN4RCwwQkFBMEIsRUFBQTs7QUFFM0I7RUFDSSxlQUFlLEVBQUE7O0FBRW5CO0VBQ0ksd0JBQXdCLEVBQUE7O0FBRTVCO0VBRVEsa0NBQWtDO0VBQ2xDLDBCQUEwQixFQUFBOztBQUhsQztFQU9ZLHlCQUFnQyxFQUFBOztBQUk1QztFQUVRLDZCQUE2QixFQUFBOztBQUdyQztFQUNJLHlCQUFnQyxFQUFBOztBQzNIcEM7RUFDRSxvQkFBYTtFQUFiLGFBQWEsRUFBQTs7QUFEZjtJQUdJLGNBQWMsRUFBQTs7QUFIbEI7TUFLTSxXQUFXO01BQ1gsWUFBWTtNQUNaLGtCQUFrQjtNQUNsQixvQkFBaUI7U0FBakIsaUJBQWlCLEVBQUE7O0FBUnZCO0lBWUksaUJBQWlCO0lBQ2pCLDRCQUE0QixFQUFBOztBQWJoQztNQWVNLG1CQUFtQjtNQUNuQixrQkFBa0IsRUFBQTs7QUFoQnhCO1FBa0JRLGtCQUFrQjtRQUNsQix5QnhCY2U7UXdCYmYsK0JBQStCO1FBQy9CLGtCQUFrQixFQUFBOztBQXJCMUI7UUF3QlEsV0FBVztRQUNYLGtCQUFrQjtRQUNsQixnQkFBZ0I7UUFDaEIsWUFBWSxFQUFBOztBQTNCcEI7SUFpQ0ksY0FBYyxFQUFBOztBQWpDbEI7TUFtQ00sU0FBUztNQUNULGtCQUFrQjtNQUNsQiw0QkFBNEIsRUFBQTs7QUFyQ2xDO1FBd0NVLHlCeEJuQ1k7UXdCb0NaLGdCQUFnQjtRQUNoQixjeEJBVztRd0JDWCxpQ0FBaUMsRUFBQTs7QUEzQzNDO01BZ0RNLGtCQUFrQjtNQUNsQixXQUFXLEVBQUE7O0FBakRqQjtRQW1EUSxrQkFBa0I7UUFDbEIsWUFBWSxFQUFBOztBQ3BEcEI7RUFHTSxnQkFBZ0I7RUFDaEIsaUJBQWlCLEVBQUE7O0FBSXZCO0VBQ0UsWUFBWTtFQUNaLHlCekJtQnFCO0V5QmxCckIsY0FBYztFQUNkLGVBQWU7RUFDZixvQkFBYTtFQUFiLGFBQWE7RUFDYixlQUFlO0VBQ2YsZ0JBQWdCLEVBQUE7O0FBUGxCO0lBU0ksb0JBQWE7SUFBYixhQUFhO0lBQ2IseUJBQW1CO1lBQW5CLG1CQUFtQjtJQUNuQixXQUFXLEVBQUE7O0FBWGY7TUFjTSxhQUFhO01BQ2Isb0JBQWE7TUFBYixhQUFhO01BQ2IseUJBQW1CO2NBQW5CLG1CQUFtQjtNQUNuQiwrQnpCSWlCO015QkhqQix3QkFBdUI7Y0FBdkIsdUJBQXVCLEVBQUE7O0FBbEI3QjtRQW9CUSx1QkFBMkI7Z0JBQTNCLDJCQUEyQixFQUFBOztBQXBCbkM7UUE0QlEscUJBQXlCO2dCQUF6Qix5QkFBeUIsRUFBQTs7QUE1QmpDO1FBZ0NRLFlBQVk7UUFDWixXQUFXLEVBQUE7O0FBakNuQjtRQXFDUSxpQkFBaUIsRUFBQTs7QUFyQ3pCO1FBeUNRLFNBQVMsRUFBQTs7QUFNakI7RUFDRSxhQUFhO0VBQ2IsaUJBQWlCLEVBQUE7O0FBRm5CO0lBSUksZ0N6QjlCbUI7SXlCK0JuQixvQkFBb0IsRUFBQTs7QUFMeEI7TUFPTSxvQkFBYTtNQUFiLGFBQWEsRUFBQTs7QUFQbkI7UUFTUSxvQkFBYTtRQUFiLGFBQWEsRUFBQTs7QUFUckI7VUFhWSxXQUFXO1VBQ1gsWUFBWTtVQUNaLGtCQUFrQixFQUFBOztBQWY5QjtVQW9CVSxpQkFBaUIsRUFBQTs7QUFwQjNCO1lBc0JZLGdCQUFnQjtZQUNoQixvQkFBb0I7WUFDcEIsY0FBYztZQUNkLHFCQUFxQjtZQUNyQixjekJsRFc7WXlCbURYLGdCQUFnQjtZQUNoQixpQkFBaUIsRUFBQTs7QUE1QjdCO1VBZ0NVLGdCQUFnQixFQUFBOztBQWhDMUI7VUFtQ1UsZ0JBQWdCLEVBQUE7O0FBbkMxQjtRQXdDUSxpQkFBaUIsRUFBQTs7QUF4Q3pCO01BNkNNLGdCQUFnQixFQUFBOztBQ2pHdEI7RUFDRSxTQUFTO0VBQ1QsVUFBVTtFQUNWLHNCQUFzQixFQUFBOztBQUV4QjtFQUNFLG1CMUJpQ21CLEVBQUE7O0EwQmxDckI7SUFHSSxXQUFXO0lBQ1gsYUFBYTtJQUNiLG1CMUJvQm1CO0kwQm5CbkIsZUFBZTtJQUNmLGtCQUFrQixFQUFBOztBQVB0QjtNQVNNLGlCQUFpQjtNQUNqQixvQkFBYTtNQUFiLGFBQWE7TUFDYix5QkFBbUI7Y0FBbkIsbUJBQW1CLEVBQUE7O0FBWHpCO1FBYVEsV0FBVztRQUNYLFlBQVk7UUFDWixtQjFCbUJhO1EwQmxCYixrQkFBa0I7UUFDbEIsbUJBQW1CLEVBQUE7O0FBakIzQjtRQW9CUSxXQUFXO1FBQ1gsWUFBWTtRQUNaLGdCQUFnQjtRQUNoQix1Q0FBbUUsRUFBQTs7QUF2QjNFO1FBMEJRLG9CQUFhO1FBQWIsYUFBYTtRQUNiLGlCQUFpQixFQUFBOztBQTNCekI7VUE2QlUsWUFBWTtVQUNaLFdBQVc7VUFDWCxrQkFBa0I7VUFDbEIsbUIxQkVXO1UwQkRYLGtCQUFrQixFQUFBOztBQWpDNUI7VUFvQ1UsWUFBWTtVQUNaLFdBQVc7VUFDWCxrQkFBa0I7VUFDbEIsbUIxQkxXO1UwQk1YLG9CQUFhO1VBQWIsYUFBYSxFQUFBOztBQXhDdkI7TUE2Q00sc0JBQXNCO01BQ3RCLFNBQVMsRUFBQTs7QUE5Q2Y7SUFrREksaUJBQWlCLEVBQUE7O0FBbERyQjtNQW9ETSxXQUFXO01BQ1gsbUIxQm5CZTtNMEJvQmYsaUJBQWlCO01BQ2pCLGtCQUFrQjtNQUNsQixtQkFBbUI7TUFDbkIsaUJBQWlCO01BQ2pCLG9CQUFvQjtNQUNwQix5QjFCbENpQjtNMEJtQ2pCLGtEQUFrRCxFQUFBOztBQTVEeEQ7UUE4RFEsWUFBWTtRQUNaLGFBQWE7UUFDYixtQjFCOUJhO1EwQitCYixrQkFBa0I7UUFDbEIsY0FBYztRQUNkLGtCQUFrQjtRQUNsQixTQUFTO1FBQ1QsVUFBVTtRQUNWLHFDQUE2QjtnQkFBN0IsNkJBQTZCO1FBQzdCLHlCMUI5Q2UsRUFBQTs7QTBCekJ2QjtRQTBFUSxvQkFBYTtRQUFiLGFBQWE7UUFDYiw0QkFBc0I7UUFBdEIsNkJBQXNCO2dCQUF0QixzQkFBc0I7UUFDdEIsd0JBQXVCO2dCQUF2Qix1QkFBdUI7UUFDdkIsV0FBVztRQUNYLHlCQUFtQjtnQkFBbkIsbUJBQW1CLEVBQUE7O0FBOUUzQjtVQWdGVSxVQUFVO1VBQ1YsWUFBWTtVQUNaLGdCQUFnQjtVQUNoQixtQjFCMURhLEVBQUE7O0EwQnpCdkI7VUFzRlUsVUFBVTtVQUNWLFlBQVk7VUFDWixnQkFBZ0I7VUFDaEIsbUIxQmhFYSxFQUFBOztBMEJ6QnZCO1FBNkZRLG9CQUFhO1FBQWIsYUFBYTtRQUNiLDZCQUE2QjtRQUM3QixnQkFBZ0IsRUFBQTs7QUEvRnhCO1VBaUdVLG9CQUFhO1VBQWIsYUFBYTtVQUNiLDRCQUFzQjtVQUF0Qiw2QkFBc0I7a0JBQXRCLHNCQUFzQjtVQUN0Qix5QkFBbUI7a0JBQW5CLG1CQUFtQixFQUFBOztBQW5HN0I7WUFxR1ksWUFBWTtZQUNaLFdBQVc7WUFDWCxrQkFBa0I7WUFDbEIseUIxQi9FVyxFQUFBOztBMEJ6QnZCO1lBMkdZLFlBQVk7WUFDWixXQUFXO1lBQ1gsa0JBQWtCO1lBQ2xCLHlCMUJyRlc7WTBCc0ZYLGdCQUFnQixFQUFBOztBQS9HNUI7UUFvSFEsY0FBYztRQUNkLFNBQVM7UUFDVCxnQkFBZ0I7UUFDaEIsK0JBQStCO1FBQy9CLGdDQUFnQztRQUNoQyxhQUFhLEVBQUE7O0FBekhyQjtJQStISSxZQUFZO0lBQ1osWUFBWTtJQUNaLFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIsOEJBQXNCO1lBQXRCLHNCQUFzQjtJQUN0Qiw4QkFBc0I7WUFBdEIsc0JBQXNCO0lBQ3RCLDJDQUFtQztZQUFuQyxtQ0FBbUM7SUFDbkMsWUFBWTtJQUNaLGtCQUFrQjtJQUNsQiwwR0FBNkY7SUFBN0YsNEVBQTZGO0lBQzdGLE1BQU0sRUFBQTs7QUFHVjtFQUNFLFlBQVk7RUFDWixXQUFXO0VBQ1gsb0JBQWE7RUFBYixhQUFhLEVBQUE7O0FBSGY7SUFLSSxzQkFBc0I7SUFDdEIsK0IxQjdIbUI7STBCOEhuQixZQUFZO0lBQ1osb0JBQWE7SUFBYixhQUFhO0lBQ2IseUJBQW1CO1lBQW5CLG1CQUFtQjtJQUNuQix3QkFBdUI7WUFBdkIsdUJBQXVCO0lBQ3ZCLGlCQUFpQixFQUFBOztBQVhyQjtNQWFNLFdBQVc7TUFDWCxZQUFZO01BQ1osbUJBQW1CO01BQ25CLGtCQUFrQixFQUFBOztBQWhCeEI7TUFtQk0sVUFBVTtNQUNWLFlBQVk7TUFDWixpQkFBaUI7TUFDakIsbUJBQW1CLEVBQUE7O0FBdEJ6QjtNQXlCTSxVQUFVO01BQ1YsZUFBZSxFQUFBOztBQUlyQjtFQUNFO0lBQ0UsT0FBTyxFQUFBO0VBRVQ7SUFDRSx1QkFBdUIsRUFBQSxFQUFBOztBQUwzQjtFQUNFO0lBQ0UsT0FBTyxFQUFBO0VBRVQ7SUFDRSx1QkFBdUIsRUFBQSxFQUFBOztBQUkzQjtFQUNFLHdCQUF3QjtFQUN4QixZQUFZO0VBQ1oseUIxQjdKcUI7RTBCOEpyQixrQkFBa0IsRUFBQTs7QUFKcEI7SUFPSSxZQUFZO0lBQ1osV0FBVztJQUNYLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIseUIxQnJLbUI7STBCc0tuQixrQkFBa0I7SUFDbEIsV0FBVyxFQUFBOztBQU1mO0VBQ0UsV0FBVztFQUNYLHlCMUJ0S21CO0UwQnVLbkIsZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixvQkFBYTtFQUFiLGFBQWE7RUFDYiw0QkFBc0I7RUFBdEIsNkJBQXNCO1VBQXRCLHNCQUFzQjtFQUN0Qix5QkFBbUI7VUFBbkIsbUJBQW1CO0VBQ25CLGtCQUFrQixFQUFBOztBQVJwQjtJQVVJLFdBQVc7SUFDWCxvQkFBYTtJQUFiLGFBQWE7SUFDYiw0QkFBc0I7SUFBdEIsNkJBQXNCO1lBQXRCLHNCQUFzQjtJQUN0Qix5QkFBbUI7WUFBbkIsbUJBQW1CO0lBQ25CLGtCQUFrQixFQUFBOztBQWR0QjtNQWdCTSxXQUFXO01BQ1gsWUFBWTtNQUNaLHNDQUFrRTtNQUNsRSxrQkFBa0I7TUFDbEIsT0FBTyxFQUFBOztBQXBCYjtNQXVCTSxZQUFZO01BQ1osV0FBVztNQUNYLG1CQUFtQjtNQUNuQix5QjFCdk1pQixFQUFBOztBMEI2S3ZCO01BNkJNLFdBQVc7TUFDWCxvQkFBYTtNQUFiLGFBQWE7TUFDYiw0QkFBc0I7TUFBdEIsNkJBQXNCO2NBQXRCLHNCQUFzQjtNQUN0Qix5QkFBbUI7Y0FBbkIsbUJBQW1CLEVBQUE7O0FBaEN6QjtRQWtDUSxVQUFVO1FBQ1YsWUFBWTtRQUNaLGtCQUFrQjtRQUNsQix5QjFCbE5lO1EwQm1OZixnQkFBZ0I7UUFDaEIsWUFBWSxFQUFBOztBQXZDcEI7VUF5Q1UsWUFBWTtVQUNaLFlBQVksRUFBQTs7QUExQ3RCO01BK0NNLFlBQVk7TUFDWixZQUFZO01BQ1osV0FBVztNQUNYLGtCQUFrQjtNQUNsQiw4QkFBc0I7Y0FBdEIsc0JBQXNCO01BQ3RCLDhCQUFzQjtjQUF0QixzQkFBc0I7TUFDdEIsMkNBQW1DO2NBQW5DLG1DQUFtQztNQUNuQyxZQUFZO01BQ1osa0JBQWtCO01BQ2xCLDBHQUE2RjtNQUE3Riw0RUFBNkY7TUFDN0YsTUFBTSxFQUFBOztBQUlaO0VBQ0U7SUFDRSxPQUFPLEVBQUE7RUFFVDtJQUNFLHVCQUF1QixFQUFBLEVBQUE7O0FBTTNCO0VBQ0Usb0JBQWE7RUFBYixhQUFhO0VBQ2Isa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCxnQkFBZ0IsRUFBQTs7QUFKbEI7SUFNSSxvQkFBYTtJQUFiLGFBQWE7SUFDYiw0QkFBc0I7SUFBdEIsNkJBQXNCO1lBQXRCLHNCQUFzQjtJQUN0Qix5QkFBbUI7WUFBbkIsbUJBQW1CO0lBQ25CLFVBQVUsRUFBQTs7QUFUZDtNQVdNLFlBQVk7TUFDWixXQUFXO01BQ1gsa0JBQWtCO01BQ2xCLHlCMUJuUWlCLEVBQUE7O0EwQnFQdkI7TUFpQk0sYUFBYTtNQUNiLFlBQVk7TUFDWixrQkFBa0I7TUFDbEIseUIxQnpRaUIsRUFBQTs7QTBCcVB2QjtNQXVCTSxZQUFZO01BQ1osV0FBVztNQUNYLGtCQUFrQjtNQUNsQix5QjFCL1FpQjtNMEJnUmpCLGdCQUFnQjtNQUNoQixrQkFBa0IsRUFBQTs7QUE1QnhCO0lBZ0NJLFlBQVk7SUFDWixZQUFZO0lBQ1osV0FBVztJQUNYLGtCQUFrQjtJQUNsQixrQ0FBMEI7WUFBMUIsMEJBQTBCO0lBQzFCLDhCQUFzQjtZQUF0QixzQkFBc0I7SUFDdEIsMkNBQW1DO1lBQW5DLG1DQUFtQztJQUNuQyxZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLGdKQUtDO0lBTEQsc0dBS0MsRUFBQTs7QUE5Q0w7SUFpREksZ0JBQWdCO0lBQ2hCLFdBQVcsRUFBQTs7QUFsRGY7TUFvRE0sYUFBYSxFQUFBOztBQXBEbkI7UUFzRFEsWUFBWTtRQUNaLFdBQVc7UUFDWCxrQkFBa0IsRUFBQTs7QUFLMUI7RUFDRTtJQUNFLE9BQU8sRUFBQTtFQUVUO0lBQ0UsdUJBQXVCLEVBQUEsRUFBQTs7QUFMM0I7RUFDRTtJQUNFLE9BQU8sRUFBQTtFQUVUO0lBQ0UsdUJBQXVCLEVBQUEsRUFBQTs7QUFLM0I7RUFDRSxXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLDRCQUFzQjtFQUF0Qiw2QkFBc0I7VUFBdEIsc0JBQXNCO0VBQ3RCLG9CQUFhO0VBQWIsYUFBYTtFQUNiLHlCQUFtQjtVQUFuQixtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQix5QjFCcFVxQixFQUFBOztBMEI0VHZCO0lBVUksa0JBQWtCO0lBQ2xCLG9CQUFhO0lBQWIsYUFBYTtJQUNiLGVBQWU7SUFDZix5QkFBbUI7WUFBbkIsbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxZQUFZO0lBQ1oseUIxQjVVbUI7STBCNlVuQiwwQkFBMEIsRUFBQTs7QUFqQjlCO01BbUJNLFlBQVk7TUFDWixXQUFXO01BQ1gsa0JBQWtCO01BQ2xCLHlCMUJ6VWU7TTBCMFVmLGNBQWMsRUFBQTs7QUF2QnBCO01BMEJNLFlBQVk7TUFDWixVQUFVO01BQ1Ysa0JBQWtCO01BQ2xCLHlCMUJoVmU7TTBCaVZmLGlCQUFpQixFQUFBOztBQTlCdkI7TUFpQ00sWUFBWTtNQUNaLFlBQVk7TUFDWixXQUFXO01BQ1gsa0JBQWtCO01BQ2xCLDhCQUFzQjtjQUF0QixzQkFBc0I7TUFDdEIsOEJBQXNCO2NBQXRCLHNCQUFzQjtNQUN0QiwyQ0FBbUM7Y0FBbkMsbUNBQW1DO01BQ25DLFlBQVk7TUFDWixrQkFBa0I7TUFDbEIsa0pBS0M7TUFMRCx3R0FLQyxFQUFBOztBQU9QO0VBQ0Usb0JBQWE7RUFBYixhQUFhO0VBQ2IsWUFBWTtFQUNaLGdDQUFnQztFQUNoQyx5QkFBbUI7VUFBbkIsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixvQkFBb0IsRUFBQTs7QUFOdEI7SUFRSSxvQkFBYTtJQUFiLGFBQWE7SUFDYix5QkFBbUI7WUFBbkIsbUJBQW1CLEVBQUE7O0FBVHZCO01BV00sWUFBWTtNQUNaLFdBQVc7TUFDWCxrQkFBa0I7TUFDbEIseUIxQmhZaUIsRUFBQTs7QTBCa1h2QjtJQWtCSSxZQUFZO0lBQ1osWUFBWTtJQUNaLGtCQUFrQjtJQUNsQix5QjFCdlltQjtJMEJ3WW5CLGlCQUFpQixFQUFBOztBQXRCckI7SUF5QkksWUFBWTtJQUNaLFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIseUIxQjlZbUI7STBCK1luQixpQkFBaUIsRUFBQTs7QUFNckI7RUFFRSxvQkFBb0I7RUFDcEIsZ0MxQnhacUIsRUFBQTs7QTBCcVp2QjtJQUtJLFdBQVc7SUFDWCxzQkFBc0I7SUFDdEIsbUJBQW1CO0lBQ25CLHlCMUI3Wm1CLEVBQUE7O0EwQnFadkI7SUFXSSxrQkFBa0IsRUFBQTs7QUFYdEI7TUFhTSxZQUFZO01BQ1osV0FBVztNQUNYLGtCQUFrQjtNQUNsQix5QjFCcmFpQjtNMEJzYWpCLGdCQUFnQjtNQUNoQixrQkFBa0IsRUFBQTs7QUFsQnhCO1FBb0JRLFlBQVk7UUFDWixZQUFZO1FBQ1osWUFBWTtRQUNaLGtCQUFrQjtRQUNsQix5QjFCN2FlO1EwQjhhZixrQkFBa0I7UUFDbEIsVUFBVTtRQUNWLFFBQVE7UUFDUixxQ0FBNkI7Z0JBQTdCLDZCQUE2QixFQUFBOztBQTVCckM7TUFnQ00sZ0JBQWdCO01BQ2hCLFlBQVk7TUFDWixXQUFXO01BQ1gsa0JBQWtCO01BQ2xCLHlCMUJ6YmlCO00wQjBiakIsa0JBQWtCLEVBQUE7O0FBckN4QjtRQXVDUSxZQUFZO1FBQ1osa0JBQWtCO1FBQ2xCLFlBQVk7UUFDWixZQUFZO1FBQ1osa0JBQWtCO1FBQ2xCLHlCMUJqY2U7UTBCa2NmLFNBQVMsRUFBQTs7QUE3Q2pCO01BaURNLFlBQVk7TUFDWixXQUFXO01BQ1gsa0JBQWtCO01BQ2xCLHlCMUJ6Y2lCO00wQjBjakIsZ0JBQWdCO01BQ2hCLGtCQUFrQixFQUFBOztBQXREeEI7UUF3RFEsWUFBWTtRQUNaLFlBQVk7UUFDWixZQUFZO1FBQ1osa0JBQWtCO1FBQ2xCLHlCMUJqZGU7UTBCa2RmLGtCQUFrQjtRQUNsQixVQUFVLEVBQUE7O0FBOURsQjtNQWtFTSxZQUFZO01BQ1osWUFBWTtNQUNaLFdBQVc7TUFDWCxrQkFBa0I7TUFDbEIsOEJBQXNCO2NBQXRCLHNCQUFzQjtNQUN0Qiw4QkFBc0I7Y0FBdEIsc0JBQXNCO01BQ3RCLDJDQUFtQztjQUFuQyxtQ0FBbUM7TUFDbkMsWUFBWTtNQUNaLGtCQUFrQjtNQUNsQiwwR0FBNkY7TUFBN0YsNEVBQTZGO01BQzdGLE1BQU0sRUFBQTs7QUFNWjtFQUdNLGdCQUFnQjtFQUNoQixvQkFBb0IsRUFBQTs7QUFJMUI7RUFDRSxlQUFlO0VBQ2Ysb0JBQWE7RUFBYixhQUFhO0VBQ2IsZ0MxQnRmcUI7RTBCdWZyQixrQkFBa0IsRUFBQTs7QUFKcEI7SUFNSSx3QkFBd0IsRUFBQTs7QUFONUI7TUFRTSxZQUFZO01BQ1osV0FBVztNQUNYLGtCQUFrQjtNQUNsQix5QjFCMWZpQjtNMEIyZmpCLGtCQUFrQixFQUFBOztBQVp4QjtRQWNRLFlBQVk7UUFDWixZQUFZO1FBQ1osWUFBWTtRQUNaLGtCQUFrQjtRQUNsQix5QjFCamdCZTtRMEJrZ0JmLGtCQUFrQjtRQUNsQixVQUFVO1FBQ1YsUUFBUTtRQUNSLHFDQUE2QjtnQkFBN0IsNkJBQTZCLEVBQUE7O0FBdEJyQztNQTBCTSxnQkFBZ0I7TUFDaEIsWUFBWTtNQUNaLFdBQVc7TUFDWCxrQkFBa0I7TUFDbEIseUIxQjdnQmlCO00wQjhnQmpCLGtCQUFrQixFQUFBOztBQS9CeEI7UUFpQ1EsWUFBWTtRQUNaLGtCQUFrQjtRQUNsQixZQUFZO1FBQ1osV0FBVztRQUNYLGtCQUFrQjtRQUNsQix5QjFCcmhCZTtRMEJzaEJmLFNBQVMsRUFBQTs7QUF2Q2pCO01BMkNNLFlBQVk7TUFDWixXQUFXO01BQ1gsa0JBQWtCO01BQ2xCLHlCMUI3aEJpQjtNMEI4aEJqQixnQkFBZ0I7TUFDaEIsa0JBQWtCLEVBQUE7O0FBaER4QjtRQWtEUSxZQUFZO1FBQ1osWUFBWTtRQUNaLFlBQVk7UUFDWixrQkFBa0I7UUFDbEIseUIxQnJpQmU7UTBCc2lCZixrQkFBa0I7UUFDbEIsVUFBVSxFQUFBOztBQXhEbEI7SUE2REksWUFBWTtJQUNaLFdBQVc7SUFDWCxtQkFBbUI7SUFDbkIseUIxQi9pQm1CO0kwQmdqQm5CLGlCQUFpQixFQUFBOztBQWpFckI7SUFvRUksWUFBWTtJQUNaLFlBQVk7SUFDWixXQUFXO0lBQ1gsa0JBQWtCO0lBQ2xCLDhCQUFzQjtZQUF0QixzQkFBc0I7SUFDdEIsOEJBQXNCO1lBQXRCLHNCQUFzQjtJQUN0QiwyQ0FBbUM7WUFBbkMsbUNBQW1DO0lBQ25DLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsTUFBTTtJQUNOLDBHQUE2RjtJQUE3Riw0RUFBNkYsRUFBQTs7QUFRakc7RUFDRSxvQkFBYTtFQUFiLGFBQWE7RUFDYixrQkFBa0I7RUFDbEIsaUJBQWlCLEVBQUE7O0FBSG5CO0lBS0ksWUFBWTtJQUNaLFdBQVc7SUFDWCxtQkFBbUI7SUFDbkIsY0FBYztJQUNkLHlCMUI5a0JtQjtJMEIra0JuQixnQkFBZ0IsRUFBQTs7QUFWcEI7SUFhSSxXQUFXO0lBQ1gsaUJBQWlCO0lBQ2pCLG9CQUFvQjtJQUNwQixvQkFBYTtJQUFiLGFBQWE7SUFDYix5QkFBbUI7WUFBbkIsbUJBQW1CO0lBQ25CLGdDMUIzbEJtQixFQUFBOztBMEJ5a0J2QjtNQW9CTSxtQkFBbUI7TUFDbkIsV0FBVyxFQUFBOztBQXJCakI7UUF1QlEsZ0JBQWdCO1FBQ2hCLFlBQVk7UUFDWixXQUFXO1FBQ1gsa0JBQWtCO1FBQ2xCLHlCMUJobUJlO1EwQmltQmYsa0JBQWtCLEVBQUE7O0FBNUIxQjtVQThCVSxZQUFZO1VBQ1osa0JBQWtCO1VBQ2xCLFlBQVk7VUFDWixVQUFVO1VBQ1Ysa0JBQWtCO1VBQ2xCLHlCMUJ4bUJhO1UwQnltQmIsVUFBVTtVQUNWLE9BQU8sRUFBQTs7QUFyQ2pCO1FBeUNRLFlBQVk7UUFDWixXQUFXO1FBQ1gsa0JBQWtCO1FBQ2xCLHlCMUJqbkJlO1EwQmtuQmYsZUFBZTtRQUNmLGtCQUFrQixFQUFBOztBQTlDMUI7VUFnRFUsWUFBWTtVQUNaLFlBQVk7VUFDWixZQUFZO1VBQ1osa0JBQWtCO1VBQ2xCLHlCMUJ6bkJhO1UwQjBuQmIsa0JBQWtCO1VBQ2xCLFVBQVU7VUFDVixRQUFRO1VBQ1IscUNBQTZCO2tCQUE3Qiw2QkFBNkIsRUFBQTs7QUF4RHZDO0lBOERJLFlBQVk7SUFDWixXQUFXO0lBQ1gsbUJBQW1CO0lBQ25CLHlCMUJ0b0JtQjtJMEJ1b0JuQixjQUFjLEVBQUE7O0FBbEVsQjtJQXNFTSxnQkFBZ0IsRUFBQTs7QUF0RXRCO0lBMEVJLFlBQVk7SUFDWixZQUFZO0lBQ1osV0FBVztJQUNYLGtCQUFrQjtJQUNsQiw4QkFBc0I7WUFBdEIsc0JBQXNCO0lBQ3RCLDhCQUFzQjtZQUF0QixzQkFBc0I7SUFDdEIsMkNBQW1DO1lBQW5DLG1DQUFtQztJQUNuQyxZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLDBHQUE2RjtJQUE3Riw0RUFBNkY7SUFDN0YsTUFBTSxFQUFBOztBQU1WO0VBQ0UsaUJBQWlCO0VBQ2pCLGdCQUFnQixFQUFBOztBQUZsQjtJQUlJLGtCQUFrQixFQUFBOztBQUp0QjtNQU1NLFlBQVk7TUFDWixVQUFVO01BQ1Ysa0JBQWtCO01BQ2xCLHlCMUJ4cUJpQixFQUFBOztBMEIrcEJ2QjtNQVlNLGdCQUFnQjtNQUNoQixvQkFBYTtNQUFiLGFBQWE7TUFDYixvQkFBb0IsRUFBQTs7QUFkMUI7UUFnQlEsWUFBWTtRQUNaLFlBQVk7UUFDWixrQkFBa0I7UUFDbEIseUIxQmxyQmU7UTBCbXJCZixjQUFjO1FBQ2Qsa0JBQWtCLEVBQUE7O0FBckIxQjtNQXlCTSxZQUFZO01BQ1osWUFBWTtNQUNaLFdBQVc7TUFDWCxrQkFBa0I7TUFDbEIsOEJBQXNCO2NBQXRCLHNCQUFzQjtNQUN0Qiw4QkFBc0I7Y0FBdEIsc0JBQXNCO01BQ3RCLDJDQUFtQztjQUFuQyxtQ0FBbUM7TUFDbkMsWUFBWTtNQUNaLGtCQUFrQjtNQUNsQiwwR0FBNkY7TUFBN0YsNEVBQTZGO01BQzdGLE1BQU0sRUFBQTs7QUFuQ1o7SUF3Q00sZUFBZTtJQUNmLGdCQUFnQixFQUFBOztBQXpDdEI7TUEyQ1EsZ0JBQWdCLEVBQUE7O0FBM0N4QjtRQTZDVSxXQUFXLEVBQUE7O0FBTXJCO0VBQ0UsZ0MxQm50QnFCLEVBQUE7O0EwQmt0QnZCO0lBR0ksZUFBZSxFQUFBOztBQUhuQjtNQUtNLGFBQWE7TUFDYixtQkFBbUI7TUFDbkIseUIxQnp0QmlCLEVBQUE7O0EwQjh0QnZCO0VBQ0Usb0JBQWE7RUFBYixhQUFhO0VBQ2Isa0JBQWtCO0VBQ2xCLGdCQUFnQixFQUFBOztBQUhsQjtJQUtJLFVBQVU7SUFDVixrQkFBa0IsRUFBQTs7QUFOdEI7TUFRTSxXQUFXO01BQ1gsaUJBQWlCO01BQ2pCLHlCMUJ4dUJpQjtNMEJ5dUJqQixrQkFBa0I7TUFDbEIsa0JBQWtCLEVBQUE7O0FBWnhCO01BZU0sZUFBZSxFQUFBOztBQWZyQjtJQW1CSSxZQUFZO0lBQ1osWUFBWTtJQUNaLFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIsOEJBQXNCO1lBQXRCLHNCQUFzQjtJQUN0Qiw4QkFBc0I7WUFBdEIsc0JBQXNCO0lBQ3RCLDJDQUFtQztZQUFuQyxtQ0FBbUM7SUFDbkMsWUFBWTtJQUNaLGtCQUFrQjtJQUNsQiwwR0FBNkY7SUFBN0YsNEVBQTZGO0lBQzdGLE1BQU0sRUFBQTs7QUFHVjtFQUNFLG9CQUFhO0VBQWIsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQixnQzFCcndCcUIsRUFBQTs7QTBCa3dCdkI7SUFLSSx5QjFCdndCbUI7STBCd3dCbkIsYUFBYTtJQUNiLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsY0FBYztJQUNkLGtCQUFrQjtJQUNsQixtQkFBbUIsRUFBQTs7QUFYdkI7TUFhTSxlQUFlLEVBQUE7O0FBYnJCO01BZ0JNLGFBQWE7TUFDYixXQUFXO01BQ1gseUIxQjd2Qm1CO00wQjh2Qm5CLGtCQUFrQjtNQUNsQiw2QkFBNkI7TUFDN0IsNEJBQTRCLEVBQUE7O0FBckJsQztNQXdCTSw0QkFBNEI7TUFDNUIsa0JBQWtCLEVBQUE7O0FBekJ4QjtRQTJCUSxZQUFZO1FBQ1osV0FBVztRQUNYLHlCMUJ4d0JpQixFQUFBOztBMEIydUJ6QjtRQWdDUSxZQUFZO1FBQ1osVUFBVTtRQUNWLHlCMUI3d0JpQjtRMEI4d0JqQixlQUFlLEVBQUE7O0FBbkN2QjtRQXNDUSxZQUFZO1FBQ1osWUFBWTtRQUNaLFdBQVc7UUFDWCxrQkFBa0I7UUFDbEIsOEJBQXNCO2dCQUF0QixzQkFBc0I7UUFDdEIsOEJBQXNCO2dCQUF0QixzQkFBc0I7UUFDdEIsMkNBQW1DO2dCQUFuQyxtQ0FBbUM7UUFDbkMsWUFBWTtRQUNaLGtCQUFrQjtRQUNsQiwwR0FBNkY7UUFBN0YsNEVBQTZGO1FBQzdGLE1BQU0sRUFBQTs7QUM3MEJkO0VBQ0Usb0JBQW9CO0VBQ3BCLGdDM0J5QnFCO0UyQnhCckIsb0JBQW9CLEVBQUE7O0FBSHRCO0lBS0ksZ0JBQWdCLEVBQUE7O0FBTHBCO0lBUUksZ0JBQWdCLEVBQUE7O0FBSXBCO0VBQ0UsZUFBZTtFQUNmLG9CQUFvQixFQUFBOztBQUZ0QjtJQUlJLGtCQUFrQjtJQUNsQixvQkFBb0I7SUFDcEIsZ0MzQlNtQixFQUFBOztBMkJmdkI7TUFRTSxnQkFBZ0IsRUFBQTs7QUFSdEI7TUFXTSxnQkFBZ0I7TUFDaEIsbUJBQW1CLEVBQUE7O0FBWnpCO01BZVEsV0FBVyxFQUFBOztBQWZuQjtJQW1CTSxvQkFBYTtJQUFiLGFBQWE7SUFDYix3QkFBdUI7WUFBdkIsdUJBQXVCO0lBQ3ZCLGdCQUFnQixFQUFBOztBQXJCdEI7TUF1QlUsa0JBQWtCLEVBQUE7O0FDckM1QjtFQUNFLFlBQVksRUFBQTs7QUFFZDtFQUNFLG9CQUFnQjtFQUNoQixrQkFBYztFQUNkLHVCQUFlLEVBQUE7O0FBR2pCO0VBR0ksa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCxZQUFZLEVBQUE7O0FBTGhCO0lBT0ksV0FBVztJQUNYLFlBQVksRUFBQTs7QUFSaEI7TUFVTSxrQkFBa0I7TUFDbEIsT0FBTztNQUNQLE1BQU07TUFDTixhQUFhLEVBQUE7O0FBSW5CO0VBRUksWUFBWSxFQUFBOztBQUdoQjtFQUdVLGFBQWEsRUFBQTs7QUFIdkI7SUFLWSxjQUFjLEVBQUE7O0FBTDFCO0VBVVUsYzVCcENZLEVBQUE7O0E0QnVDdEI7RUFDRSxrQkFBa0IsRUFBQTs7QUFEcEI7SUFJTSxZQUFZO0lBQ1osV0FBVztJQUNYLGtCQUFrQjtJQUNsQiwyQkFBMkI7SUFDM0IseUI1QkxnQjtJNEJNaEIsa0JBQWtCO0lBQ2xCLG9CQUFhO0lBQWIsYUFBYTtJQUNiLHdCQUF1QjtZQUF2Qix1QkFBdUI7SUFDdkIseUJBQW1CO1lBQW5CLG1CQUFtQjtJQUNuQixXQUFXO0lBQ1gsU0FBUyxFQUFBOztBQUlmO0VBQ0Usa0JBQWtCLEVBQUE7O0FBRHBCO0lBSU0sWUFBWTtJQUNaLFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIsMkJBQTJCO0lBQzNCLHlCNUJ2QmdCO0k0QndCaEIsa0JBQWtCO0lBQ2xCLG9CQUFhO0lBQWIsYUFBYTtJQUNiLHdCQUF1QjtZQUF2Qix1QkFBdUI7SUFDdkIseUJBQW1CO1lBQW5CLG1CQUFtQjtJQUNuQixXQUFXO0lBQ1gsU0FBUyxFQUFBOztBQUlmO0VBRUksa0JBQWtCLEVBQUE7O0FBRnRCO0lBS1EsaUJBQWlCLEVBQUE7O0FDckZ6QjtFQUNFLFdBQVc7RUFDWCxtQjdCd0NtQjtFNkJ2Q25CLFdBQVcsRUFBQTs7QUFFYjtFQUNFLG9CQUFhO0VBQWIsYUFBYTtFQUNiLHlCQUFtQjtVQUFuQixtQkFBbUI7RUFDbkIsYUFBYSxFQUFBOztBQUhmO0lBS0ksb0JBQWE7SUFBYixhQUFhO0lBQ2IseUJBQW1CO1lBQW5CLG1CQUFtQjtJQUNuQixpQkFBaUIsRUFBQTs7QUFQckI7TUFTTSxpQkFBaUIsRUFBQTs7QUFJdkI7RUFDSSxlQUFlLEVBQUE7O0FBRG5CO0lBR0ksb0JBQWE7SUFBYixhQUFhO0lBQ2IsZ0M3Qk9tQixFQUFBOztBNkJYdkI7TUFNTSxpQkFBaUI7TUFDakIsb0JBQWE7TUFBYixhQUFhO01BQ2IsWUFBWTtNQUNaLGtCQUFrQixFQUFBOztBQVR4QjtRQVdRLGlCQUFpQjtRQUNqQixjQUFjO1FBQ2QsV0FBVyxFQUFBOztBQU1uQjtFQUNFLG9DQUE2QyxFQUFBOztBQ3RDL0M7RUFDRSxvQkFBYTtFQUFiLGFBQWE7RUFDYiw0QkFBc0I7RUFBdEIsNkJBQXNCO1VBQXRCLHNCQUFzQjtFQUN0Qix5QkFBbUI7VUFBbkIsbUJBQW1CO0VBQ25CLHdCQUF1QjtVQUF2Qix1QkFBdUI7RUFDdkIsWUFBWTtFQUNaLGVBQWUsRUFBQTs7QUFOakI7SUFRSSxnQkFBZ0I7SUFDaEIsa0JBQWtCLEVBQUE7O0FBVHRCO0lBWUksZUFBZTtJQUNmLG1CQUFtQjtJQUNuQixrQkFBa0IsRUFBQTs7QUFkdEI7SUFpQk0sV0FBVyxFQUFBOztBQ2pCakI7RUFDSSxvQkFBYTtFQUFiLGFBQWEsRUFBQTs7QUFEakI7SUFHUSxVQUFVO0lBQ1YsZ0JBQWdCO0lBQ2hCLDJCQUEyQjtJQUMzQixvQkFBb0I7SUFDcEIsNEJBQTRCLEVBQUE7O0FDc0NwQztFQUNJLDZCQUE2QjtFQUM3QixZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLGlCQUFpQixFQUFBOztBQUpyQjtJQU1JLFVBQVUsRUFBQTs7QUFOZDtJQVNRLG1CQUFtQixFQUFBOztBQUl6QjtFQUNJLHdCQUF3QixFQUFBOztBQUU1QjtFQUNJLHdCQUF3QixFQUFBOztBQUU1QjtFQUVRLHNCQUFzQixFQUFBOztBQUk5QjtFQUdVLHlEQUF3RCxFQUFBOztBQUlsRTtFQUNFLG9CQUFvQixFQUFBOztBQUR0QjtJQUdJLFlBQVk7SUFDWixhQUFjO0lBQ2QsWUFBWSxFQUFBOztBQUxoQjtJQVNJLFlBQVk7SUFDWixhQUFjO0lBQ2QsVUFBVTtJQUNWLDZCQUFxQjtZQUFyQixxQkFBcUI7SUFDckIsa0RBQWtDO0lBQWxDLDBDQUFrQztJQUFsQyxrQ0FBa0M7SUFBbEMsa0VBQWtDLEVBQUE7O0FBYnRDO0lBZ0JRLG9CQUFvQixFQUFBOztBQWhCNUI7SUFvQkksdUJBQXVCLEVBQUE7O0FBRzdCO0VBQ0ksNkJBQXFCO1VBQXJCLHFCQUFxQixFQUFBIiwiZmlsZSI6InNyYy9nbG9iYWwuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbbnVsbCwiaHRtbC5pb3Mge1xuICAtLWlvbi1kZWZhdWx0LWZvbnQ6IC1hcHBsZS1zeXN0ZW0sIEJsaW5rTWFjU3lzdGVtRm9udCwgXCJIZWx2ZXRpY2EgTmV1ZVwiLCBcIlJvYm90b1wiLCBzYW5zLXNlcmlmO1xufVxuXG5odG1sLm1kIHtcbiAgLS1pb24tZGVmYXVsdC1mb250OiBcIlJvYm90b1wiLCBcIkhlbHZldGljYSBOZXVlXCIsIHNhbnMtc2VyaWY7XG59XG5cbmh0bWwge1xuICAtLWlvbi1mb250LWZhbWlseTogdmFyKC0taW9uLWRlZmF1bHQtZm9udCk7XG59XG5cbmJvZHkge1xuICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZC1jb2xvcik7XG59XG5cbmJvZHkuYmFja2Ryb3Atbm8tc2Nyb2xsIHtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cblxuLmlvbi1jb2xvci1wcmltYXJ5IHtcbiAgLS1pb24tY29sb3ItYmFzZTogdmFyKC0taW9uLWNvbG9yLXByaW1hcnksICMzODgwZmYpICFpbXBvcnRhbnQ7XG4gIC0taW9uLWNvbG9yLWJhc2UtcmdiOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeS1yZ2IsIDU2LCAxMjgsIDI1NSkgIWltcG9ydGFudDtcbiAgLS1pb24tY29sb3ItY29udHJhc3Q6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5LWNvbnRyYXN0LCAjZmZmKSAhaW1wb3J0YW50O1xuICAtLWlvbi1jb2xvci1jb250cmFzdC1yZ2I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5LWNvbnRyYXN0LXJnYiwgMjU1LCAyNTUsIDI1NSkgIWltcG9ydGFudDtcbiAgLS1pb24tY29sb3Itc2hhZGU6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5LXNoYWRlLCAjMzE3MWUwKSAhaW1wb3J0YW50O1xuICAtLWlvbi1jb2xvci10aW50OiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeS10aW50LCAjNGM4ZGZmKSAhaW1wb3J0YW50O1xufVxuXG4uaW9uLWNvbG9yLXNlY29uZGFyeSB7XG4gIC0taW9uLWNvbG9yLWJhc2U6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnksICMwY2QxZTgpICFpbXBvcnRhbnQ7XG4gIC0taW9uLWNvbG9yLWJhc2UtcmdiOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5LXJnYiwgMTIsIDIwOSwgMjMyKSAhaW1wb3J0YW50O1xuICAtLWlvbi1jb2xvci1jb250cmFzdDogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeS1jb250cmFzdCwgI2ZmZikgIWltcG9ydGFudDtcbiAgLS1pb24tY29sb3ItY29udHJhc3QtcmdiOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5LWNvbnRyYXN0LXJnYiwgMjU1LCAyNTUsIDI1NSkgIWltcG9ydGFudDtcbiAgLS1pb24tY29sb3Itc2hhZGU6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnktc2hhZGUsICMwYmI4Y2MpICFpbXBvcnRhbnQ7XG4gIC0taW9uLWNvbG9yLXRpbnQ6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnktdGludCwgIzI0ZDZlYSkgIWltcG9ydGFudDtcbn1cblxuLmlvbi1jb2xvci10ZXJ0aWFyeSB7XG4gIC0taW9uLWNvbG9yLWJhc2U6IHZhcigtLWlvbi1jb2xvci10ZXJ0aWFyeSwgIzcwNDRmZikgIWltcG9ydGFudDtcbiAgLS1pb24tY29sb3ItYmFzZS1yZ2I6IHZhcigtLWlvbi1jb2xvci10ZXJ0aWFyeS1yZ2IsIDExMiwgNjgsIDI1NSkgIWltcG9ydGFudDtcbiAgLS1pb24tY29sb3ItY29udHJhc3Q6IHZhcigtLWlvbi1jb2xvci10ZXJ0aWFyeS1jb250cmFzdCwgI2ZmZikgIWltcG9ydGFudDtcbiAgLS1pb24tY29sb3ItY29udHJhc3QtcmdiOiB2YXIoLS1pb24tY29sb3ItdGVydGlhcnktY29udHJhc3QtcmdiLCAyNTUsIDI1NSwgMjU1KSAhaW1wb3J0YW50O1xuICAtLWlvbi1jb2xvci1zaGFkZTogdmFyKC0taW9uLWNvbG9yLXRlcnRpYXJ5LXNoYWRlLCAjNjMzY2UwKSAhaW1wb3J0YW50O1xuICAtLWlvbi1jb2xvci10aW50OiB2YXIoLS1pb24tY29sb3ItdGVydGlhcnktdGludCwgIzdlNTdmZikgIWltcG9ydGFudDtcbn1cblxuLmlvbi1jb2xvci1zdWNjZXNzIHtcbiAgLS1pb24tY29sb3ItYmFzZTogdmFyKC0taW9uLWNvbG9yLXN1Y2Nlc3MsICMxMGRjNjApICFpbXBvcnRhbnQ7XG4gIC0taW9uLWNvbG9yLWJhc2UtcmdiOiB2YXIoLS1pb24tY29sb3Itc3VjY2Vzcy1yZ2IsIDE2LCAyMjAsIDk2KSAhaW1wb3J0YW50O1xuICAtLWlvbi1jb2xvci1jb250cmFzdDogdmFyKC0taW9uLWNvbG9yLXN1Y2Nlc3MtY29udHJhc3QsICNmZmYpICFpbXBvcnRhbnQ7XG4gIC0taW9uLWNvbG9yLWNvbnRyYXN0LXJnYjogdmFyKC0taW9uLWNvbG9yLXN1Y2Nlc3MtY29udHJhc3QtcmdiLCAyNTUsIDI1NSwgMjU1KSAhaW1wb3J0YW50O1xuICAtLWlvbi1jb2xvci1zaGFkZTogdmFyKC0taW9uLWNvbG9yLXN1Y2Nlc3Mtc2hhZGUsICMwZWMyNTQpICFpbXBvcnRhbnQ7XG4gIC0taW9uLWNvbG9yLXRpbnQ6IHZhcigtLWlvbi1jb2xvci1zdWNjZXNzLXRpbnQsICMyOGUwNzApICFpbXBvcnRhbnQ7XG59XG5cbi5pb24tY29sb3Itd2FybmluZyB7XG4gIC0taW9uLWNvbG9yLWJhc2U6IHZhcigtLWlvbi1jb2xvci13YXJuaW5nLCAjZmZjZTAwKSAhaW1wb3J0YW50O1xuICAtLWlvbi1jb2xvci1iYXNlLXJnYjogdmFyKC0taW9uLWNvbG9yLXdhcm5pbmctcmdiLCAyNTUsIDIwNiwgMCkgIWltcG9ydGFudDtcbiAgLS1pb24tY29sb3ItY29udHJhc3Q6IHZhcigtLWlvbi1jb2xvci13YXJuaW5nLWNvbnRyYXN0LCAjZmZmKSAhaW1wb3J0YW50O1xuICAtLWlvbi1jb2xvci1jb250cmFzdC1yZ2I6IHZhcigtLWlvbi1jb2xvci13YXJuaW5nLWNvbnRyYXN0LXJnYiwgMjU1LCAyNTUsIDI1NSkgIWltcG9ydGFudDtcbiAgLS1pb24tY29sb3Itc2hhZGU6IHZhcigtLWlvbi1jb2xvci13YXJuaW5nLXNoYWRlLCAjZTBiNTAwKSAhaW1wb3J0YW50O1xuICAtLWlvbi1jb2xvci10aW50OiB2YXIoLS1pb24tY29sb3Itd2FybmluZy10aW50LCAjZmZkMzFhKSAhaW1wb3J0YW50O1xufVxuXG4uaW9uLWNvbG9yLWRhbmdlciB7XG4gIC0taW9uLWNvbG9yLWJhc2U6IHZhcigtLWlvbi1jb2xvci1kYW5nZXIsICNmMDQxNDEpICFpbXBvcnRhbnQ7XG4gIC0taW9uLWNvbG9yLWJhc2UtcmdiOiB2YXIoLS1pb24tY29sb3ItZGFuZ2VyLXJnYiwgMjQwLCA2NSwgNjUpICFpbXBvcnRhbnQ7XG4gIC0taW9uLWNvbG9yLWNvbnRyYXN0OiB2YXIoLS1pb24tY29sb3ItZGFuZ2VyLWNvbnRyYXN0LCAjZmZmKSAhaW1wb3J0YW50O1xuICAtLWlvbi1jb2xvci1jb250cmFzdC1yZ2I6IHZhcigtLWlvbi1jb2xvci1kYW5nZXItY29udHJhc3QtcmdiLCAyNTUsIDI1NSwgMjU1KSAhaW1wb3J0YW50O1xuICAtLWlvbi1jb2xvci1zaGFkZTogdmFyKC0taW9uLWNvbG9yLWRhbmdlci1zaGFkZSwgI2QzMzkzOSkgIWltcG9ydGFudDtcbiAgLS1pb24tY29sb3ItdGludDogdmFyKC0taW9uLWNvbG9yLWRhbmdlci10aW50LCAjZjI1NDU0KSAhaW1wb3J0YW50O1xufVxuXG4uaW9uLWNvbG9yLWxpZ2h0IHtcbiAgLS1pb24tY29sb3ItYmFzZTogdmFyKC0taW9uLWNvbG9yLWxpZ2h0LCAjZjRmNWY4KSAhaW1wb3J0YW50O1xuICAtLWlvbi1jb2xvci1iYXNlLXJnYjogdmFyKC0taW9uLWNvbG9yLWxpZ2h0LXJnYiwgMjQ0LCAyNDUsIDI0OCkgIWltcG9ydGFudDtcbiAgLS1pb24tY29sb3ItY29udHJhc3Q6IHZhcigtLWlvbi1jb2xvci1saWdodC1jb250cmFzdCwgIzAwMCkgIWltcG9ydGFudDtcbiAgLS1pb24tY29sb3ItY29udHJhc3QtcmdiOiB2YXIoLS1pb24tY29sb3ItbGlnaHQtY29udHJhc3QtcmdiLCAwLCAwLCAwKSAhaW1wb3J0YW50O1xuICAtLWlvbi1jb2xvci1zaGFkZTogdmFyKC0taW9uLWNvbG9yLWxpZ2h0LXNoYWRlLCAjZDdkOGRhKSAhaW1wb3J0YW50O1xuICAtLWlvbi1jb2xvci10aW50OiB2YXIoLS1pb24tY29sb3ItbGlnaHQtdGludCwgI2Y1ZjZmOSkgIWltcG9ydGFudDtcbn1cblxuLmlvbi1jb2xvci1tZWRpdW0ge1xuICAtLWlvbi1jb2xvci1iYXNlOiB2YXIoLS1pb24tY29sb3ItbWVkaXVtLCAjOTg5YWEyKSAhaW1wb3J0YW50O1xuICAtLWlvbi1jb2xvci1iYXNlLXJnYjogdmFyKC0taW9uLWNvbG9yLW1lZGl1bS1yZ2IsIDE1MiwgMTU0LCAxNjIpICFpbXBvcnRhbnQ7XG4gIC0taW9uLWNvbG9yLWNvbnRyYXN0OiB2YXIoLS1pb24tY29sb3ItbWVkaXVtLWNvbnRyYXN0LCAjZmZmKSAhaW1wb3J0YW50O1xuICAtLWlvbi1jb2xvci1jb250cmFzdC1yZ2I6IHZhcigtLWlvbi1jb2xvci1tZWRpdW0tY29udHJhc3QtcmdiLCAyNTUsIDI1NSwgMjU1KSAhaW1wb3J0YW50O1xuICAtLWlvbi1jb2xvci1zaGFkZTogdmFyKC0taW9uLWNvbG9yLW1lZGl1bS1zaGFkZSwgIzg2ODg4ZikgIWltcG9ydGFudDtcbiAgLS1pb24tY29sb3ItdGludDogdmFyKC0taW9uLWNvbG9yLW1lZGl1bS10aW50LCAjYTJhNGFiKSAhaW1wb3J0YW50O1xufVxuXG4uaW9uLWNvbG9yLWRhcmsge1xuICAtLWlvbi1jb2xvci1iYXNlOiB2YXIoLS1pb24tY29sb3ItZGFyaywgIzIyMjQyOCkgIWltcG9ydGFudDtcbiAgLS1pb24tY29sb3ItYmFzZS1yZ2I6IHZhcigtLWlvbi1jb2xvci1kYXJrLXJnYiwgMzQsIDM2LCA0MCkgIWltcG9ydGFudDtcbiAgLS1pb24tY29sb3ItY29udHJhc3Q6IHZhcigtLWlvbi1jb2xvci1kYXJrLWNvbnRyYXN0LCAjZmZmKSAhaW1wb3J0YW50O1xuICAtLWlvbi1jb2xvci1jb250cmFzdC1yZ2I6IHZhcigtLWlvbi1jb2xvci1kYXJrLWNvbnRyYXN0LXJnYiwgMjU1LCAyNTUsIDI1NSkgIWltcG9ydGFudDtcbiAgLS1pb24tY29sb3Itc2hhZGU6IHZhcigtLWlvbi1jb2xvci1kYXJrLXNoYWRlLCAjMWUyMDIzKSAhaW1wb3J0YW50O1xuICAtLWlvbi1jb2xvci10aW50OiB2YXIoLS1pb24tY29sb3ItZGFyay10aW50LCAjMzgzYTNlKSAhaW1wb3J0YW50O1xufVxuXG4uaW9uLXBhZ2Uge1xuICBsZWZ0OiAwO1xuICByaWdodDogMDtcbiAgdG9wOiAwO1xuICBib3R0b206IDA7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBjb250YWluOiBsYXlvdXQgc2l6ZSBzdHlsZTtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgei1pbmRleDogMDtcbn1cblxuaW9uLXJvdXRlLFxuaW9uLXJvdXRlLXJlZGlyZWN0LFxuaW9uLXJvdXRlcixcbmlvbi1zZWxlY3Qtb3B0aW9uLFxuaW9uLW5hdi1jb250cm9sbGVyLFxuaW9uLW1lbnUtY29udHJvbGxlcixcbmlvbi1hY3Rpb24tc2hlZXQtY29udHJvbGxlcixcbmlvbi1hbGVydC1jb250cm9sbGVyLFxuaW9uLWxvYWRpbmctY29udHJvbGxlcixcbmlvbi1tb2RhbC1jb250cm9sbGVyLFxuaW9uLXBpY2tlci1jb250cm9sbGVyLFxuaW9uLXBvcG92ZXItY29udHJvbGxlcixcbmlvbi10b2FzdC1jb250cm9sbGVyLFxuLmlvbi1wYWdlLWhpZGRlbixcbltoaWRkZW5dIHtcbiAgLyogc3R5bGVsaW50LWRpc2FibGUtbmV4dC1saW5lIGRlY2xhcmF0aW9uLW5vLWltcG9ydGFudCAqL1xuICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XG59XG5cbi5pb24tcGFnZS1pbnZpc2libGUge1xuICBvcGFjaXR5OiAwO1xufVxuXG5odG1sLnBsdC1pb3MucGx0LWh5YnJpZCwgaHRtbC5wbHQtaW9zLnBsdC1wd2Ege1xuICAtLWlvbi1zdGF0dXNiYXItcGFkZGluZzogMjBweDtcbn1cblxuQHN1cHBvcnRzIChwYWRkaW5nLXRvcDogMjBweCkge1xuICBodG1sIHtcbiAgICAtLWlvbi1zYWZlLWFyZWEtdG9wOiB2YXIoLS1pb24tc3RhdHVzYmFyLXBhZGRpbmcpO1xuICB9XG59XG5Ac3VwcG9ydHMgKHBhZGRpbmctdG9wOiBjb25zdGFudChzYWZlLWFyZWEtaW5zZXQtdG9wKSkge1xuICBodG1sIHtcbiAgICAtLWlvbi1zYWZlLWFyZWEtdG9wOiBjb25zdGFudChzYWZlLWFyZWEtaW5zZXQtdG9wKTtcbiAgICAtLWlvbi1zYWZlLWFyZWEtYm90dG9tOiBjb25zdGFudChzYWZlLWFyZWEtaW5zZXQtYm90dG9tKTtcbiAgICAtLWlvbi1zYWZlLWFyZWEtbGVmdDogY29uc3RhbnQoc2FmZS1hcmVhLWluc2V0LWxlZnQpO1xuICAgIC0taW9uLXNhZmUtYXJlYS1yaWdodDogY29uc3RhbnQoc2FmZS1hcmVhLWluc2V0LXJpZ2h0KTtcbiAgfVxufVxuQHN1cHBvcnRzIChwYWRkaW5nLXRvcDogZW52KHNhZmUtYXJlYS1pbnNldC10b3ApKSB7XG4gIGh0bWwge1xuICAgIC0taW9uLXNhZmUtYXJlYS10b3A6IGVudihzYWZlLWFyZWEtaW5zZXQtdG9wKTtcbiAgICAtLWlvbi1zYWZlLWFyZWEtYm90dG9tOiBlbnYoc2FmZS1hcmVhLWluc2V0LWJvdHRvbSk7XG4gICAgLS1pb24tc2FmZS1hcmVhLWxlZnQ6IGVudihzYWZlLWFyZWEtaW5zZXQtbGVmdCk7XG4gICAgLS1pb24tc2FmZS1hcmVhLXJpZ2h0OiBlbnYoc2FmZS1hcmVhLWluc2V0LXJpZ2h0KTtcbiAgfVxufVxuLm1lbnUtY29udGVudCB7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlM2QoMCwgIDAsICAwKTtcbn1cblxuLm1lbnUtY29udGVudC1vcGVuIHtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICB0b3VjaC1hY3Rpb246IG1hbmlwdWxhdGlvbjtcbiAgcG9pbnRlci1ldmVudHM6IG5vbmU7XG59XG5cbi5pb3MgLm1lbnUtY29udGVudC1yZXZlYWwge1xuICBib3gtc2hhZG93OiAtOHB4IDAgNDJweCByZ2JhKDAsIDAsIDAsIDAuMDgpO1xufVxuXG5bZGlyPXJ0bF0uaW9zIC5tZW51LWNvbnRlbnQtcmV2ZWFsIHtcbiAgYm94LXNoYWRvdzogOHB4IDAgNDJweCByZ2JhKDAsIDAsIDAsIDAuMDgpO1xufVxuXG4ubWQgLm1lbnUtY29udGVudC1yZXZlYWwge1xuICBib3gtc2hhZG93OiAwIDJweCAyMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjA5KSwgNHB4IDAgMTZweCAwIHJnYmEoMCwgMCwgMCwgMC4xOCk7XG59XG5cbi5tZCAubWVudS1jb250ZW50LXB1c2gge1xuICBib3gtc2hhZG93OiAwIDJweCAyMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjA5KSwgNHB4IDAgMTZweCAwIHJnYmEoMCwgMCwgMCwgMC4xOCk7XG59XG5cbi8qIyBzb3VyY2VNYXBwaW5nVVJMPWNvcmUuY3NzLm1hcCAqL1xuIixudWxsLG51bGwsbnVsbCxudWxsLG51bGwsImF1ZGlvLFxuY2FudmFzLFxucHJvZ3Jlc3MsXG52aWRlbyB7XG4gIHZlcnRpY2FsLWFsaWduOiBiYXNlbGluZTtcbn1cblxuYXVkaW86bm90KFtjb250cm9sc10pIHtcbiAgZGlzcGxheTogbm9uZTtcbiAgaGVpZ2h0OiAwO1xufVxuXG5iLFxuc3Ryb25nIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbmltZyB7XG4gIG1heC13aWR0aDogMTAwJTtcbiAgYm9yZGVyOiAwO1xufVxuXG5zdmc6bm90KDpyb290KSB7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbmZpZ3VyZSB7XG4gIG1hcmdpbjogMWVtIDQwcHg7XG59XG5cbmhyIHtcbiAgaGVpZ2h0OiAxcHg7XG4gIGJvcmRlci13aWR0aDogMDtcbiAgYm94LXNpemluZzogY29udGVudC1ib3g7XG59XG5cbnByZSB7XG4gIG92ZXJmbG93OiBhdXRvO1xufVxuXG5jb2RlLFxua2JkLFxucHJlLFxuc2FtcCB7XG4gIGZvbnQtZmFtaWx5OiBtb25vc3BhY2UsIG1vbm9zcGFjZTtcbiAgZm9udC1zaXplOiAxZW07XG59XG5cbmxhYmVsLFxuaW5wdXQsXG5zZWxlY3QsXG50ZXh0YXJlYSB7XG4gIGZvbnQtZmFtaWx5OiBpbmhlcml0O1xuICBsaW5lLWhlaWdodDogbm9ybWFsO1xufVxuXG50ZXh0YXJlYSB7XG4gIG92ZXJmbG93OiBhdXRvO1xuICBoZWlnaHQ6IGF1dG87XG4gIGZvbnQ6IGluaGVyaXQ7XG4gIGNvbG9yOiBpbmhlcml0O1xufVxuXG50ZXh0YXJlYTo6cGxhY2Vob2xkZXIge1xuICBwYWRkaW5nLWxlZnQ6IDJweDtcbn1cblxuZm9ybSxcbmlucHV0LFxub3B0Z3JvdXAsXG5zZWxlY3Qge1xuICBtYXJnaW46IDA7XG4gIGZvbnQ6IGluaGVyaXQ7XG4gIGNvbG9yOiBpbmhlcml0O1xufVxuXG5odG1sIGlucHV0W3R5cGU9YnV0dG9uXSxcbmlucHV0W3R5cGU9cmVzZXRdLFxuaW5wdXRbdHlwZT1zdWJtaXRdIHtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICAtd2Via2l0LWFwcGVhcmFuY2U6IGJ1dHRvbjtcbn1cblxuYSxcbmEgZGl2LFxuYSBzcGFuLFxuYSBpb24taWNvbixcbmEgaW9uLWxhYmVsLFxuYnV0dG9uLFxuYnV0dG9uIGRpdixcbmJ1dHRvbiBzcGFuLFxuYnV0dG9uIGlvbi1pY29uLFxuYnV0dG9uIGlvbi1sYWJlbCxcbi5pb24tdGFwcGFibGUsXG5bdGFwcGFibGVdLFxuW3RhcHBhYmxlXSBkaXYsXG5bdGFwcGFibGVdIHNwYW4sXG5bdGFwcGFibGVdIGlvbi1pY29uLFxuW3RhcHBhYmxlXSBpb24tbGFiZWwsXG5pbnB1dCxcbnRleHRhcmVhIHtcbiAgdG91Y2gtYWN0aW9uOiBtYW5pcHVsYXRpb247XG59XG5cbmEgaW9uLWxhYmVsLFxuYnV0dG9uIGlvbi1sYWJlbCB7XG4gIHBvaW50ZXItZXZlbnRzOiBub25lO1xufVxuXG5idXR0b24ge1xuICBib3JkZXI6IDA7XG4gIGJvcmRlci1yYWRpdXM6IDA7XG4gIGZvbnQtZmFtaWx5OiBpbmhlcml0O1xuICBmb250LXN0eWxlOiBpbmhlcml0O1xuICBmb250LXZhcmlhbnQ6IGluaGVyaXQ7XG4gIGxpbmUtaGVpZ2h0OiAxO1xuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICAtd2Via2l0LWFwcGVhcmFuY2U6IGJ1dHRvbjtcbn1cblxuW3RhcHBhYmxlXSB7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuYVtkaXNhYmxlZF0sXG5idXR0b25bZGlzYWJsZWRdLFxuaHRtbCBpbnB1dFtkaXNhYmxlZF0ge1xuICBjdXJzb3I6IGRlZmF1bHQ7XG59XG5cbmJ1dHRvbjo6LW1vei1mb2N1cy1pbm5lcixcbmlucHV0OjotbW96LWZvY3VzLWlubmVyIHtcbiAgcGFkZGluZzogMDtcbiAgYm9yZGVyOiAwO1xufVxuXG5pbnB1dFt0eXBlPWNoZWNrYm94XSxcbmlucHV0W3R5cGU9cmFkaW9dIHtcbiAgcGFkZGluZzogMDtcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbn1cblxuaW5wdXRbdHlwZT1udW1iZXJdOjotd2Via2l0LWlubmVyLXNwaW4tYnV0dG9uLFxuaW5wdXRbdHlwZT1udW1iZXJdOjotd2Via2l0LW91dGVyLXNwaW4tYnV0dG9uIHtcbiAgaGVpZ2h0OiBhdXRvO1xufVxuXG5pbnB1dFt0eXBlPXNlYXJjaF06Oi13ZWJraXQtc2VhcmNoLWNhbmNlbC1idXR0b24sXG5pbnB1dFt0eXBlPXNlYXJjaF06Oi13ZWJraXQtc2VhcmNoLWRlY29yYXRpb24ge1xuICAtd2Via2l0LWFwcGVhcmFuY2U6IG5vbmU7XG59XG5cbnRhYmxlIHtcbiAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcbiAgYm9yZGVyLXNwYWNpbmc6IDA7XG59XG5cbnRkLFxudGgge1xuICBwYWRkaW5nOiAwO1xufVxuXG4vKiMgc291cmNlTWFwcGluZ1VSTD1ub3JtYWxpemUuY3NzLm1hcCAqL1xuIixudWxsLCIqIHtcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgLXdlYmtpdC10YXAtaGlnaGxpZ2h0LWNvbG9yOiByZ2JhKDAsIDAsIDAsIDApO1xuICAtd2Via2l0LXRhcC1oaWdobGlnaHQtY29sb3I6IHRyYW5zcGFyZW50O1xuICAtd2Via2l0LXRvdWNoLWNhbGxvdXQ6IG5vbmU7XG59XG5cbmh0bWwge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICB0ZXh0LXNpemUtYWRqdXN0OiAxMDAlO1xufVxuXG5odG1sOm5vdCguaHlkcmF0ZWQpIGJvZHkge1xuICBkaXNwbGF5OiBub25lO1xufVxuXG5odG1sLnBsdC1wd2Ege1xuICBoZWlnaHQ6IDEwMHZoO1xufVxuXG5ib2R5IHtcbiAgLW1vei1vc3gtZm9udC1zbW9vdGhpbmc6IGdyYXlzY2FsZTtcbiAgLXdlYmtpdC1mb250LXNtb290aGluZzogYW50aWFsaWFzZWQ7XG4gIG1hcmdpbi1sZWZ0OiAwO1xuICBtYXJnaW4tcmlnaHQ6IDA7XG4gIG1hcmdpbi10b3A6IDA7XG4gIG1hcmdpbi1ib3R0b206IDA7XG4gIHBhZGRpbmctbGVmdDogMDtcbiAgcGFkZGluZy1yaWdodDogMDtcbiAgcGFkZGluZy10b3A6IDA7XG4gIHBhZGRpbmctYm90dG9tOiAwO1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXgtd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgbWF4LWhlaWdodDogMTAwJTtcbiAgdGV4dC1yZW5kZXJpbmc6IG9wdGltaXplTGVnaWJpbGl0eTtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgdG91Y2gtYWN0aW9uOiBtYW5pcHVsYXRpb247XG4gIC13ZWJraXQtdXNlci1kcmFnOiBub25lO1xuICAtbXMtY29udGVudC16b29taW5nOiBub25lO1xuICB3b3JkLXdyYXA6IGJyZWFrLXdvcmQ7XG4gIG92ZXJzY3JvbGwtYmVoYXZpb3IteTogbm9uZTtcbiAgdGV4dC1zaXplLWFkanVzdDogbm9uZTtcbn1cblxuLyojIHNvdXJjZU1hcHBpbmdVUkw9c3RydWN0dXJlLmNzcy5tYXAgKi9cbiIsbnVsbCwiaHRtbCB7XG4gIGZvbnQtZmFtaWx5OiB2YXIoLS1pb24tZm9udC1mYW1pbHkpO1xufVxuXG5hIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSwgIzM4ODBmZik7XG59XG5cbmgxLFxuaDIsXG5oMyxcbmg0LFxuaDUsXG5oNiB7XG4gIG1hcmdpbi10b3A6IDE2cHg7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIGxpbmUtaGVpZ2h0OiAxLjI7XG59XG5oMSB7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG4gIGZvbnQtc2l6ZTogMjZweDtcbn1cbmgyIHtcbiAgbWFyZ2luLXRvcDogMThweDtcbiAgZm9udC1zaXplOiAyNHB4O1xufVxuaDMge1xuICBmb250LXNpemU6IDIycHg7XG59XG5cbmg0IHtcbiAgZm9udC1zaXplOiAyMHB4O1xufVxuXG5oNSB7XG4gIGZvbnQtc2l6ZTogMThweDtcbn1cblxuaDYge1xuICBmb250LXNpemU6IDE2cHg7XG59XG5cbnNtYWxsIHtcbiAgZm9udC1zaXplOiA3NSU7XG59XG5cbnN1YixcbnN1cCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgZm9udC1zaXplOiA3NSU7XG4gIGxpbmUtaGVpZ2h0OiAwO1xuICB2ZXJ0aWNhbC1hbGlnbjogYmFzZWxpbmU7XG59XG5cbnN1cCB7XG4gIHRvcDogLTAuNWVtO1xufVxuXG5zdWIge1xuICBib3R0b206IC0wLjI1ZW07XG59XG5cbi8qIyBzb3VyY2VNYXBwaW5nVVJMPXR5cG9ncmFwaHkuY3NzLm1hcCAqL1xuIixudWxsLCIuaW9uLWhpZGUge1xuICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XG59XG5cbi5pb24taGlkZS11cCB7XG4gIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcbn1cblxuQG1lZGlhIChtYXgtd2lkdGg6IDU3NXB4KSB7XG4gIC5pb24taGlkZS1kb3duIHtcbiAgICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XG4gIH1cbn1cbkBtZWRpYSAobWluLXdpZHRoOiA1NzZweCkge1xuICAuaW9uLWhpZGUtc20tdXAge1xuICAgIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcbiAgfVxufVxuQG1lZGlhIChtYXgtd2lkdGg6IDc2N3B4KSB7XG4gIC5pb24taGlkZS1zbS1kb3duIHtcbiAgICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XG4gIH1cbn1cbkBtZWRpYSAobWluLXdpZHRoOiA3NjhweCkge1xuICAuaW9uLWhpZGUtbWQtdXAge1xuICAgIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcbiAgfVxufVxuQG1lZGlhIChtYXgtd2lkdGg6IDk5MXB4KSB7XG4gIC5pb24taGlkZS1tZC1kb3duIHtcbiAgICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XG4gIH1cbn1cbkBtZWRpYSAobWluLXdpZHRoOiA5OTJweCkge1xuICAuaW9uLWhpZGUtbGctdXAge1xuICAgIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcbiAgfVxufVxuQG1lZGlhIChtYXgtd2lkdGg6IDExOTlweCkge1xuICAuaW9uLWhpZGUtbGctZG93biB7XG4gICAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogMTIwMHB4KSB7XG4gIC5pb24taGlkZS14bC11cCB7XG4gICAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xuICB9XG59XG4uaW9uLWhpZGUteGwtZG93biB7XG4gIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcbn1cblxuLyojIHNvdXJjZU1hcHBpbmdVUkw9ZGlzcGxheS5jc3MubWFwICovXG4iLG51bGwsIi5pb24tbm8tcGFkZGluZyxcbltuby1wYWRkaW5nXSB7XG4gIC0tcGFkZGluZy1zdGFydDogMDtcbiAgLS1wYWRkaW5nLWVuZDogMDtcbiAgLS1wYWRkaW5nLXRvcDogMDtcbiAgLS1wYWRkaW5nLWJvdHRvbTogMDtcbiAgcGFkZGluZy1sZWZ0OiAwO1xuICBwYWRkaW5nLXJpZ2h0OiAwO1xuICBwYWRkaW5nLXRvcDogMDtcbiAgcGFkZGluZy1ib3R0b206IDA7XG59XG5cbi5pb24tcGFkZGluZyxcbltwYWRkaW5nXSB7XG4gIC0tcGFkZGluZy1zdGFydDogdmFyKC0taW9uLXBhZGRpbmcsIDE2cHgpO1xuICAtLXBhZGRpbmctZW5kOiB2YXIoLS1pb24tcGFkZGluZywgMTZweCk7XG4gIC0tcGFkZGluZy10b3A6IHZhcigtLWlvbi1wYWRkaW5nLCAxNnB4KTtcbiAgLS1wYWRkaW5nLWJvdHRvbTogdmFyKC0taW9uLXBhZGRpbmcsIDE2cHgpO1xuICBwYWRkaW5nLWxlZnQ6IHZhcigtLWlvbi1wYWRkaW5nLCAxNnB4KTtcbiAgcGFkZGluZy1yaWdodDogdmFyKC0taW9uLXBhZGRpbmcsIDE2cHgpO1xuICBwYWRkaW5nLXRvcDogdmFyKC0taW9uLXBhZGRpbmcsIDE2cHgpO1xuICBwYWRkaW5nLWJvdHRvbTogdmFyKC0taW9uLXBhZGRpbmcsIDE2cHgpO1xufVxuQHN1cHBvcnRzIChtYXJnaW4taW5saW5lLXN0YXJ0OiAwKSBvciAoLXdlYmtpdC1tYXJnaW4tc3RhcnQ6IDApIHtcbiAgLmlvbi1wYWRkaW5nLFxuW3BhZGRpbmddIHtcbiAgICBwYWRkaW5nLWxlZnQ6IHVuc2V0O1xuICAgIHBhZGRpbmctcmlnaHQ6IHVuc2V0O1xuICAgIC13ZWJraXQtcGFkZGluZy1zdGFydDogdmFyKC0taW9uLXBhZGRpbmcsIDE2cHgpO1xuICAgIHBhZGRpbmctaW5saW5lLXN0YXJ0OiB2YXIoLS1pb24tcGFkZGluZywgMTZweCk7XG4gICAgLXdlYmtpdC1wYWRkaW5nLWVuZDogdmFyKC0taW9uLXBhZGRpbmcsIDE2cHgpO1xuICAgIHBhZGRpbmctaW5saW5lLWVuZDogdmFyKC0taW9uLXBhZGRpbmcsIDE2cHgpO1xuICB9XG59XG5cbi5pb24tcGFkZGluZy10b3AsXG5bcGFkZGluZy10b3BdIHtcbiAgLS1wYWRkaW5nLXRvcDogdmFyKC0taW9uLXBhZGRpbmcsIDE2cHgpO1xuICBwYWRkaW5nLXRvcDogdmFyKC0taW9uLXBhZGRpbmcsIDE2cHgpO1xufVxuLmlvbi1wYWRkaW5nLXN0YXJ0LFxuW3BhZGRpbmctc3RhcnRdIHtcbiAgLS1wYWRkaW5nLXN0YXJ0OiB2YXIoLS1pb24tcGFkZGluZywgMTZweCk7XG4gIHBhZGRpbmctbGVmdDogdmFyKC0taW9uLXBhZGRpbmcsIDE2cHgpO1xufVxuQHN1cHBvcnRzIChtYXJnaW4taW5saW5lLXN0YXJ0OiAwKSBvciAoLXdlYmtpdC1tYXJnaW4tc3RhcnQ6IDApIHtcbiAgLmlvbi1wYWRkaW5nLXN0YXJ0LFxuW3BhZGRpbmctc3RhcnRdIHtcbiAgICBwYWRkaW5nLWxlZnQ6IHVuc2V0O1xuICAgIC13ZWJraXQtcGFkZGluZy1zdGFydDogdmFyKC0taW9uLXBhZGRpbmcsIDE2cHgpO1xuICAgIHBhZGRpbmctaW5saW5lLXN0YXJ0OiB2YXIoLS1pb24tcGFkZGluZywgMTZweCk7XG4gIH1cbn1cblxuLmlvbi1wYWRkaW5nLWVuZCxcbltwYWRkaW5nLWVuZF0ge1xuICAtLXBhZGRpbmctZW5kOiB2YXIoLS1pb24tcGFkZGluZywgMTZweCk7XG4gIHBhZGRpbmctcmlnaHQ6IHZhcigtLWlvbi1wYWRkaW5nLCAxNnB4KTtcbn1cbkBzdXBwb3J0cyAobWFyZ2luLWlubGluZS1zdGFydDogMCkgb3IgKC13ZWJraXQtbWFyZ2luLXN0YXJ0OiAwKSB7XG4gIC5pb24tcGFkZGluZy1lbmQsXG5bcGFkZGluZy1lbmRdIHtcbiAgICBwYWRkaW5nLXJpZ2h0OiB1bnNldDtcbiAgICAtd2Via2l0LXBhZGRpbmctZW5kOiB2YXIoLS1pb24tcGFkZGluZywgMTZweCk7XG4gICAgcGFkZGluZy1pbmxpbmUtZW5kOiB2YXIoLS1pb24tcGFkZGluZywgMTZweCk7XG4gIH1cbn1cblxuLmlvbi1wYWRkaW5nLWJvdHRvbSxcbltwYWRkaW5nLWJvdHRvbV0ge1xuICAtLXBhZGRpbmctYm90dG9tOiB2YXIoLS1pb24tcGFkZGluZywgMTZweCk7XG4gIHBhZGRpbmctYm90dG9tOiB2YXIoLS1pb24tcGFkZGluZywgMTZweCk7XG59XG4uaW9uLXBhZGRpbmctdmVydGljYWwsXG5bcGFkZGluZy12ZXJ0aWNhbF0ge1xuICAtLXBhZGRpbmctdG9wOiB2YXIoLS1pb24tcGFkZGluZywgMTZweCk7XG4gIC0tcGFkZGluZy1ib3R0b206IHZhcigtLWlvbi1wYWRkaW5nLCAxNnB4KTtcbiAgcGFkZGluZy10b3A6IHZhcigtLWlvbi1wYWRkaW5nLCAxNnB4KTtcbiAgcGFkZGluZy1ib3R0b206IHZhcigtLWlvbi1wYWRkaW5nLCAxNnB4KTtcbn1cbi5pb24tcGFkZGluZy1ob3Jpem9udGFsLFxuW3BhZGRpbmctaG9yaXpvbnRhbF0ge1xuICAtLXBhZGRpbmctc3RhcnQ6IHZhcigtLWlvbi1wYWRkaW5nLCAxNnB4KTtcbiAgLS1wYWRkaW5nLWVuZDogdmFyKC0taW9uLXBhZGRpbmcsIDE2cHgpO1xuICBwYWRkaW5nLWxlZnQ6IHZhcigtLWlvbi1wYWRkaW5nLCAxNnB4KTtcbiAgcGFkZGluZy1yaWdodDogdmFyKC0taW9uLXBhZGRpbmcsIDE2cHgpO1xufVxuQHN1cHBvcnRzIChtYXJnaW4taW5saW5lLXN0YXJ0OiAwKSBvciAoLXdlYmtpdC1tYXJnaW4tc3RhcnQ6IDApIHtcbiAgLmlvbi1wYWRkaW5nLWhvcml6b250YWwsXG5bcGFkZGluZy1ob3Jpem9udGFsXSB7XG4gICAgcGFkZGluZy1sZWZ0OiB1bnNldDtcbiAgICBwYWRkaW5nLXJpZ2h0OiB1bnNldDtcbiAgICAtd2Via2l0LXBhZGRpbmctc3RhcnQ6IHZhcigtLWlvbi1wYWRkaW5nLCAxNnB4KTtcbiAgICBwYWRkaW5nLWlubGluZS1zdGFydDogdmFyKC0taW9uLXBhZGRpbmcsIDE2cHgpO1xuICAgIC13ZWJraXQtcGFkZGluZy1lbmQ6IHZhcigtLWlvbi1wYWRkaW5nLCAxNnB4KTtcbiAgICBwYWRkaW5nLWlubGluZS1lbmQ6IHZhcigtLWlvbi1wYWRkaW5nLCAxNnB4KTtcbiAgfVxufVxuXG4uaW9uLW5vLW1hcmdpbixcbltuby1tYXJnaW5dIHtcbiAgLS1tYXJnaW4tc3RhcnQ6IDA7XG4gIC0tbWFyZ2luLWVuZDogMDtcbiAgLS1tYXJnaW4tdG9wOiAwO1xuICAtLW1hcmdpbi1ib3R0b206IDA7XG4gIG1hcmdpbi1sZWZ0OiAwO1xuICBtYXJnaW4tcmlnaHQ6IDA7XG4gIG1hcmdpbi10b3A6IDA7XG4gIG1hcmdpbi1ib3R0b206IDA7XG59XG5cbi5pb24tbWFyZ2luLFxuW21hcmdpbl0ge1xuICAtLW1hcmdpbi1zdGFydDogdmFyKC0taW9uLW1hcmdpbiwgMTZweCk7XG4gIC0tbWFyZ2luLWVuZDogdmFyKC0taW9uLW1hcmdpbiwgMTZweCk7XG4gIC0tbWFyZ2luLXRvcDogdmFyKC0taW9uLW1hcmdpbiwgMTZweCk7XG4gIC0tbWFyZ2luLWJvdHRvbTogdmFyKC0taW9uLW1hcmdpbiwgMTZweCk7XG4gIG1hcmdpbi1sZWZ0OiB2YXIoLS1pb24tbWFyZ2luLCAxNnB4KTtcbiAgbWFyZ2luLXJpZ2h0OiB2YXIoLS1pb24tbWFyZ2luLCAxNnB4KTtcbiAgbWFyZ2luLXRvcDogdmFyKC0taW9uLW1hcmdpbiwgMTZweCk7XG4gIG1hcmdpbi1ib3R0b206IHZhcigtLWlvbi1tYXJnaW4sIDE2cHgpO1xufVxuQHN1cHBvcnRzIChtYXJnaW4taW5saW5lLXN0YXJ0OiAwKSBvciAoLXdlYmtpdC1tYXJnaW4tc3RhcnQ6IDApIHtcbiAgLmlvbi1tYXJnaW4sXG5bbWFyZ2luXSB7XG4gICAgbWFyZ2luLWxlZnQ6IHVuc2V0O1xuICAgIG1hcmdpbi1yaWdodDogdW5zZXQ7XG4gICAgLXdlYmtpdC1tYXJnaW4tc3RhcnQ6IHZhcigtLWlvbi1tYXJnaW4sIDE2cHgpO1xuICAgIG1hcmdpbi1pbmxpbmUtc3RhcnQ6IHZhcigtLWlvbi1tYXJnaW4sIDE2cHgpO1xuICAgIC13ZWJraXQtbWFyZ2luLWVuZDogdmFyKC0taW9uLW1hcmdpbiwgMTZweCk7XG4gICAgbWFyZ2luLWlubGluZS1lbmQ6IHZhcigtLWlvbi1tYXJnaW4sIDE2cHgpO1xuICB9XG59XG5cbi5pb24tbWFyZ2luLXRvcCxcblttYXJnaW4tdG9wXSB7XG4gIC0tbWFyZ2luLXRvcDogdmFyKC0taW9uLW1hcmdpbiwgMTZweCk7XG4gIG1hcmdpbi10b3A6IHZhcigtLWlvbi1tYXJnaW4sIDE2cHgpO1xufVxuLmlvbi1tYXJnaW4tc3RhcnQsXG5bbWFyZ2luLXN0YXJ0XSB7XG4gIC0tbWFyZ2luLXN0YXJ0OiB2YXIoLS1pb24tbWFyZ2luLCAxNnB4KTtcbiAgbWFyZ2luLWxlZnQ6IHZhcigtLWlvbi1tYXJnaW4sIDE2cHgpO1xufVxuQHN1cHBvcnRzIChtYXJnaW4taW5saW5lLXN0YXJ0OiAwKSBvciAoLXdlYmtpdC1tYXJnaW4tc3RhcnQ6IDApIHtcbiAgLmlvbi1tYXJnaW4tc3RhcnQsXG5bbWFyZ2luLXN0YXJ0XSB7XG4gICAgbWFyZ2luLWxlZnQ6IHVuc2V0O1xuICAgIC13ZWJraXQtbWFyZ2luLXN0YXJ0OiB2YXIoLS1pb24tbWFyZ2luLCAxNnB4KTtcbiAgICBtYXJnaW4taW5saW5lLXN0YXJ0OiB2YXIoLS1pb24tbWFyZ2luLCAxNnB4KTtcbiAgfVxufVxuXG4uaW9uLW1hcmdpbi1lbmQsXG5bbWFyZ2luLWVuZF0ge1xuICAtLW1hcmdpbi1lbmQ6IHZhcigtLWlvbi1tYXJnaW4sIDE2cHgpO1xuICBtYXJnaW4tcmlnaHQ6IHZhcigtLWlvbi1tYXJnaW4sIDE2cHgpO1xufVxuQHN1cHBvcnRzIChtYXJnaW4taW5saW5lLXN0YXJ0OiAwKSBvciAoLXdlYmtpdC1tYXJnaW4tc3RhcnQ6IDApIHtcbiAgLmlvbi1tYXJnaW4tZW5kLFxuW21hcmdpbi1lbmRdIHtcbiAgICBtYXJnaW4tcmlnaHQ6IHVuc2V0O1xuICAgIC13ZWJraXQtbWFyZ2luLWVuZDogdmFyKC0taW9uLW1hcmdpbiwgMTZweCk7XG4gICAgbWFyZ2luLWlubGluZS1lbmQ6IHZhcigtLWlvbi1tYXJnaW4sIDE2cHgpO1xuICB9XG59XG5cbi5pb24tbWFyZ2luLWJvdHRvbSxcblttYXJnaW4tYm90dG9tXSB7XG4gIC0tbWFyZ2luLWJvdHRvbTogdmFyKC0taW9uLW1hcmdpbiwgMTZweCk7XG4gIG1hcmdpbi1ib3R0b206IHZhcigtLWlvbi1tYXJnaW4sIDE2cHgpO1xufVxuLmlvbi1tYXJnaW4tdmVydGljYWwsXG5bbWFyZ2luLXZlcnRpY2FsXSB7XG4gIC0tbWFyZ2luLXRvcDogdmFyKC0taW9uLW1hcmdpbiwgMTZweCk7XG4gIC0tbWFyZ2luLWJvdHRvbTogdmFyKC0taW9uLW1hcmdpbiwgMTZweCk7XG4gIG1hcmdpbi10b3A6IHZhcigtLWlvbi1tYXJnaW4sIDE2cHgpO1xuICBtYXJnaW4tYm90dG9tOiB2YXIoLS1pb24tbWFyZ2luLCAxNnB4KTtcbn1cbi5pb24tbWFyZ2luLWhvcml6b250YWwsXG5bbWFyZ2luLWhvcml6b250YWxdIHtcbiAgLS1tYXJnaW4tc3RhcnQ6IHZhcigtLWlvbi1tYXJnaW4sIDE2cHgpO1xuICAtLW1hcmdpbi1lbmQ6IHZhcigtLWlvbi1tYXJnaW4sIDE2cHgpO1xuICBtYXJnaW4tbGVmdDogdmFyKC0taW9uLW1hcmdpbiwgMTZweCk7XG4gIG1hcmdpbi1yaWdodDogdmFyKC0taW9uLW1hcmdpbiwgMTZweCk7XG59XG5Ac3VwcG9ydHMgKG1hcmdpbi1pbmxpbmUtc3RhcnQ6IDApIG9yICgtd2Via2l0LW1hcmdpbi1zdGFydDogMCkge1xuICAuaW9uLW1hcmdpbi1ob3Jpem9udGFsLFxuW21hcmdpbi1ob3Jpem9udGFsXSB7XG4gICAgbWFyZ2luLWxlZnQ6IHVuc2V0O1xuICAgIG1hcmdpbi1yaWdodDogdW5zZXQ7XG4gICAgLXdlYmtpdC1tYXJnaW4tc3RhcnQ6IHZhcigtLWlvbi1tYXJnaW4sIDE2cHgpO1xuICAgIG1hcmdpbi1pbmxpbmUtc3RhcnQ6IHZhcigtLWlvbi1tYXJnaW4sIDE2cHgpO1xuICAgIC13ZWJraXQtbWFyZ2luLWVuZDogdmFyKC0taW9uLW1hcmdpbiwgMTZweCk7XG4gICAgbWFyZ2luLWlubGluZS1lbmQ6IHZhcigtLWlvbi1tYXJnaW4sIDE2cHgpO1xuICB9XG59XG5cbi8qIyBzb3VyY2VNYXBwaW5nVVJMPXBhZGRpbmcuY3NzLm1hcCAqL1xuIixudWxsLCIuaW9uLWZsb2F0LWxlZnQsXG5bZmxvYXQtbGVmdF0ge1xuICBmbG9hdDogbGVmdCAhaW1wb3J0YW50O1xufVxuXG4uaW9uLWZsb2F0LXJpZ2h0LFxuW2Zsb2F0LXJpZ2h0XSB7XG4gIGZsb2F0OiByaWdodCAhaW1wb3J0YW50O1xufVxuXG4uaW9uLWZsb2F0LXN0YXJ0LFxuW2Zsb2F0LXN0YXJ0XSB7XG4gIGZsb2F0OiBsZWZ0ICFpbXBvcnRhbnQ7XG59XG5bZGlyPXJ0bF0gLmlvbi1mbG9hdC1zdGFydCwgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIC5pb24tZmxvYXQtc3RhcnQsIFtkaXI9cnRsXSBbZmxvYXQtc3RhcnRdLCA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgW2Zsb2F0LXN0YXJ0XSB7XG4gIGZsb2F0OiByaWdodCAhaW1wb3J0YW50O1xufVxuXG4uaW9uLWZsb2F0LWVuZCxcbltmbG9hdC1lbmRdIHtcbiAgZmxvYXQ6IHJpZ2h0ICFpbXBvcnRhbnQ7XG59XG5bZGlyPXJ0bF0gLmlvbi1mbG9hdC1lbmQsIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSAuaW9uLWZsb2F0LWVuZCwgW2Rpcj1ydGxdIFtmbG9hdC1lbmRdLCA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgW2Zsb2F0LWVuZF0ge1xuICBmbG9hdDogbGVmdCAhaW1wb3J0YW50O1xufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogNTc2cHgpIHtcbiAgLmlvbi1mbG9hdC1zbS1sZWZ0LFxuW2Zsb2F0LXNtLWxlZnRdIHtcbiAgICBmbG9hdDogbGVmdCAhaW1wb3J0YW50O1xuICB9XG5cbiAgLmlvbi1mbG9hdC1zbS1yaWdodCxcbltmbG9hdC1zbS1yaWdodF0ge1xuICAgIGZsb2F0OiByaWdodCAhaW1wb3J0YW50O1xuICB9XG5cbiAgLmlvbi1mbG9hdC1zbS1zdGFydCxcbltmbG9hdC1zbS1zdGFydF0ge1xuICAgIGZsb2F0OiBsZWZ0ICFpbXBvcnRhbnQ7XG4gIH1cbiAgW2Rpcj1ydGxdIC5pb24tZmxvYXQtc20tc3RhcnQsIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSAuaW9uLWZsb2F0LXNtLXN0YXJ0LCBbZGlyPXJ0bF0gW2Zsb2F0LXNtLXN0YXJ0XSwgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIFtmbG9hdC1zbS1zdGFydF0ge1xuICAgIGZsb2F0OiByaWdodCAhaW1wb3J0YW50O1xuICB9XG5cbiAgLmlvbi1mbG9hdC1zbS1lbmQsXG5bZmxvYXQtc20tZW5kXSB7XG4gICAgZmxvYXQ6IHJpZ2h0ICFpbXBvcnRhbnQ7XG4gIH1cbiAgW2Rpcj1ydGxdIC5pb24tZmxvYXQtc20tZW5kLCA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgLmlvbi1mbG9hdC1zbS1lbmQsIFtkaXI9cnRsXSBbZmxvYXQtc20tZW5kXSwgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIFtmbG9hdC1zbS1lbmRdIHtcbiAgICBmbG9hdDogbGVmdCAhaW1wb3J0YW50O1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIHtcbiAgLmlvbi1mbG9hdC1tZC1sZWZ0LFxuW2Zsb2F0LW1kLWxlZnRdIHtcbiAgICBmbG9hdDogbGVmdCAhaW1wb3J0YW50O1xuICB9XG5cbiAgLmlvbi1mbG9hdC1tZC1yaWdodCxcbltmbG9hdC1tZC1yaWdodF0ge1xuICAgIGZsb2F0OiByaWdodCAhaW1wb3J0YW50O1xuICB9XG5cbiAgLmlvbi1mbG9hdC1tZC1zdGFydCxcbltmbG9hdC1tZC1zdGFydF0ge1xuICAgIGZsb2F0OiBsZWZ0ICFpbXBvcnRhbnQ7XG4gIH1cbiAgW2Rpcj1ydGxdIC5pb24tZmxvYXQtbWQtc3RhcnQsIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSAuaW9uLWZsb2F0LW1kLXN0YXJ0LCBbZGlyPXJ0bF0gW2Zsb2F0LW1kLXN0YXJ0XSwgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIFtmbG9hdC1tZC1zdGFydF0ge1xuICAgIGZsb2F0OiByaWdodCAhaW1wb3J0YW50O1xuICB9XG5cbiAgLmlvbi1mbG9hdC1tZC1lbmQsXG5bZmxvYXQtbWQtZW5kXSB7XG4gICAgZmxvYXQ6IHJpZ2h0ICFpbXBvcnRhbnQ7XG4gIH1cbiAgW2Rpcj1ydGxdIC5pb24tZmxvYXQtbWQtZW5kLCA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgLmlvbi1mbG9hdC1tZC1lbmQsIFtkaXI9cnRsXSBbZmxvYXQtbWQtZW5kXSwgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIFtmbG9hdC1tZC1lbmRdIHtcbiAgICBmbG9hdDogbGVmdCAhaW1wb3J0YW50O1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogOTkycHgpIHtcbiAgLmlvbi1mbG9hdC1sZy1sZWZ0LFxuW2Zsb2F0LWxnLWxlZnRdIHtcbiAgICBmbG9hdDogbGVmdCAhaW1wb3J0YW50O1xuICB9XG5cbiAgLmlvbi1mbG9hdC1sZy1yaWdodCxcbltmbG9hdC1sZy1yaWdodF0ge1xuICAgIGZsb2F0OiByaWdodCAhaW1wb3J0YW50O1xuICB9XG5cbiAgLmlvbi1mbG9hdC1sZy1zdGFydCxcbltmbG9hdC1sZy1zdGFydF0ge1xuICAgIGZsb2F0OiBsZWZ0ICFpbXBvcnRhbnQ7XG4gIH1cbiAgW2Rpcj1ydGxdIC5pb24tZmxvYXQtbGctc3RhcnQsIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSAuaW9uLWZsb2F0LWxnLXN0YXJ0LCBbZGlyPXJ0bF0gW2Zsb2F0LWxnLXN0YXJ0XSwgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIFtmbG9hdC1sZy1zdGFydF0ge1xuICAgIGZsb2F0OiByaWdodCAhaW1wb3J0YW50O1xuICB9XG5cbiAgLmlvbi1mbG9hdC1sZy1lbmQsXG5bZmxvYXQtbGctZW5kXSB7XG4gICAgZmxvYXQ6IHJpZ2h0ICFpbXBvcnRhbnQ7XG4gIH1cbiAgW2Rpcj1ydGxdIC5pb24tZmxvYXQtbGctZW5kLCA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgLmlvbi1mbG9hdC1sZy1lbmQsIFtkaXI9cnRsXSBbZmxvYXQtbGctZW5kXSwgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIFtmbG9hdC1sZy1lbmRdIHtcbiAgICBmbG9hdDogbGVmdCAhaW1wb3J0YW50O1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogMTIwMHB4KSB7XG4gIC5pb24tZmxvYXQteGwtbGVmdCxcbltmbG9hdC14bC1sZWZ0XSB7XG4gICAgZmxvYXQ6IGxlZnQgIWltcG9ydGFudDtcbiAgfVxuXG4gIC5pb24tZmxvYXQteGwtcmlnaHQsXG5bZmxvYXQteGwtcmlnaHRdIHtcbiAgICBmbG9hdDogcmlnaHQgIWltcG9ydGFudDtcbiAgfVxuXG4gIC5pb24tZmxvYXQteGwtc3RhcnQsXG5bZmxvYXQteGwtc3RhcnRdIHtcbiAgICBmbG9hdDogbGVmdCAhaW1wb3J0YW50O1xuICB9XG4gIFtkaXI9cnRsXSAuaW9uLWZsb2F0LXhsLXN0YXJ0LCA6aG9zdC1jb250ZXh0KFtkaXI9cnRsXSkgLmlvbi1mbG9hdC14bC1zdGFydCwgW2Rpcj1ydGxdIFtmbG9hdC14bC1zdGFydF0sIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSBbZmxvYXQteGwtc3RhcnRdIHtcbiAgICBmbG9hdDogcmlnaHQgIWltcG9ydGFudDtcbiAgfVxuXG4gIC5pb24tZmxvYXQteGwtZW5kLFxuW2Zsb2F0LXhsLWVuZF0ge1xuICAgIGZsb2F0OiByaWdodCAhaW1wb3J0YW50O1xuICB9XG4gIFtkaXI9cnRsXSAuaW9uLWZsb2F0LXhsLWVuZCwgOmhvc3QtY29udGV4dChbZGlyPXJ0bF0pIC5pb24tZmxvYXQteGwtZW5kLCBbZGlyPXJ0bF0gW2Zsb2F0LXhsLWVuZF0sIDpob3N0LWNvbnRleHQoW2Rpcj1ydGxdKSBbZmxvYXQteGwtZW5kXSB7XG4gICAgZmxvYXQ6IGxlZnQgIWltcG9ydGFudDtcbiAgfVxufVxuXG4vKiMgc291cmNlTWFwcGluZ1VSTD1mbG9hdC1lbGVtZW50cy5jc3MubWFwICovXG4iLG51bGwsIi5pb24tdGV4dC1jZW50ZXIsXG5bdGV4dC1jZW50ZXJdIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyICFpbXBvcnRhbnQ7XG59XG5cbi5pb24tdGV4dC1qdXN0aWZ5LFxuW3RleHQtanVzdGlmeV0ge1xuICB0ZXh0LWFsaWduOiBqdXN0aWZ5ICFpbXBvcnRhbnQ7XG59XG5cbi5pb24tdGV4dC1zdGFydCxcblt0ZXh0LXN0YXJ0XSB7XG4gIHRleHQtYWxpZ246IHN0YXJ0ICFpbXBvcnRhbnQ7XG59XG5cbi5pb24tdGV4dC1lbmQsXG5bdGV4dC1lbmRdIHtcbiAgdGV4dC1hbGlnbjogZW5kICFpbXBvcnRhbnQ7XG59XG5cbi5pb24tdGV4dC1sZWZ0LFxuW3RleHQtbGVmdF0ge1xuICB0ZXh0LWFsaWduOiBsZWZ0ICFpbXBvcnRhbnQ7XG59XG5cbi5pb24tdGV4dC1yaWdodCxcblt0ZXh0LXJpZ2h0XSB7XG4gIHRleHQtYWxpZ246IHJpZ2h0ICFpbXBvcnRhbnQ7XG59XG5cbi5pb24tdGV4dC1ub3dyYXAsXG5bdGV4dC1ub3dyYXBdIHtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcCAhaW1wb3J0YW50O1xufVxuXG4uaW9uLXRleHQtd3JhcCxcblt0ZXh0LXdyYXBdIHtcbiAgd2hpdGUtc3BhY2U6IG5vcm1hbCAhaW1wb3J0YW50O1xufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogNTc2cHgpIHtcbiAgLmlvbi10ZXh0LXNtLWNlbnRlcixcblt0ZXh0LXNtLWNlbnRlcl0ge1xuICAgIHRleHQtYWxpZ246IGNlbnRlciAhaW1wb3J0YW50O1xuICB9XG5cbiAgLmlvbi10ZXh0LXNtLWp1c3RpZnksXG5bdGV4dC1zbS1qdXN0aWZ5XSB7XG4gICAgdGV4dC1hbGlnbjoganVzdGlmeSAhaW1wb3J0YW50O1xuICB9XG5cbiAgLmlvbi10ZXh0LXNtLXN0YXJ0LFxuW3RleHQtc20tc3RhcnRdIHtcbiAgICB0ZXh0LWFsaWduOiBzdGFydCAhaW1wb3J0YW50O1xuICB9XG5cbiAgLmlvbi10ZXh0LXNtLWVuZCxcblt0ZXh0LXNtLWVuZF0ge1xuICAgIHRleHQtYWxpZ246IGVuZCAhaW1wb3J0YW50O1xuICB9XG5cbiAgLmlvbi10ZXh0LXNtLWxlZnQsXG5bdGV4dC1zbS1sZWZ0XSB7XG4gICAgdGV4dC1hbGlnbjogbGVmdCAhaW1wb3J0YW50O1xuICB9XG5cbiAgLmlvbi10ZXh0LXNtLXJpZ2h0LFxuW3RleHQtc20tcmlnaHRdIHtcbiAgICB0ZXh0LWFsaWduOiByaWdodCAhaW1wb3J0YW50O1xuICB9XG5cbiAgLmlvbi10ZXh0LXNtLW5vd3JhcCxcblt0ZXh0LXNtLW5vd3JhcF0ge1xuICAgIHdoaXRlLXNwYWNlOiBub3dyYXAgIWltcG9ydGFudDtcbiAgfVxuXG4gIC5pb24tdGV4dC1zbS13cmFwLFxuW3RleHQtc20td3JhcF0ge1xuICAgIHdoaXRlLXNwYWNlOiBub3JtYWwgIWltcG9ydGFudDtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDc2OHB4KSB7XG4gIC5pb24tdGV4dC1tZC1jZW50ZXIsXG5bdGV4dC1tZC1jZW50ZXJdIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDtcbiAgfVxuXG4gIC5pb24tdGV4dC1tZC1qdXN0aWZ5LFxuW3RleHQtbWQtanVzdGlmeV0ge1xuICAgIHRleHQtYWxpZ246IGp1c3RpZnkgIWltcG9ydGFudDtcbiAgfVxuXG4gIC5pb24tdGV4dC1tZC1zdGFydCxcblt0ZXh0LW1kLXN0YXJ0XSB7XG4gICAgdGV4dC1hbGlnbjogc3RhcnQgIWltcG9ydGFudDtcbiAgfVxuXG4gIC5pb24tdGV4dC1tZC1lbmQsXG5bdGV4dC1tZC1lbmRdIHtcbiAgICB0ZXh0LWFsaWduOiBlbmQgIWltcG9ydGFudDtcbiAgfVxuXG4gIC5pb24tdGV4dC1tZC1sZWZ0LFxuW3RleHQtbWQtbGVmdF0ge1xuICAgIHRleHQtYWxpZ246IGxlZnQgIWltcG9ydGFudDtcbiAgfVxuXG4gIC5pb24tdGV4dC1tZC1yaWdodCxcblt0ZXh0LW1kLXJpZ2h0XSB7XG4gICAgdGV4dC1hbGlnbjogcmlnaHQgIWltcG9ydGFudDtcbiAgfVxuXG4gIC5pb24tdGV4dC1tZC1ub3dyYXAsXG5bdGV4dC1tZC1ub3dyYXBdIHtcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwICFpbXBvcnRhbnQ7XG4gIH1cblxuICAuaW9uLXRleHQtbWQtd3JhcCxcblt0ZXh0LW1kLXdyYXBdIHtcbiAgICB3aGl0ZS1zcGFjZTogbm9ybWFsICFpbXBvcnRhbnQ7XG4gIH1cbn1cbkBtZWRpYSAobWluLXdpZHRoOiA5OTJweCkge1xuICAuaW9uLXRleHQtbGctY2VudGVyLFxuW3RleHQtbGctY2VudGVyXSB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyICFpbXBvcnRhbnQ7XG4gIH1cblxuICAuaW9uLXRleHQtbGctanVzdGlmeSxcblt0ZXh0LWxnLWp1c3RpZnldIHtcbiAgICB0ZXh0LWFsaWduOiBqdXN0aWZ5ICFpbXBvcnRhbnQ7XG4gIH1cblxuICAuaW9uLXRleHQtbGctc3RhcnQsXG5bdGV4dC1sZy1zdGFydF0ge1xuICAgIHRleHQtYWxpZ246IHN0YXJ0ICFpbXBvcnRhbnQ7XG4gIH1cblxuICAuaW9uLXRleHQtbGctZW5kLFxuW3RleHQtbGctZW5kXSB7XG4gICAgdGV4dC1hbGlnbjogZW5kICFpbXBvcnRhbnQ7XG4gIH1cblxuICAuaW9uLXRleHQtbGctbGVmdCxcblt0ZXh0LWxnLWxlZnRdIHtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0ICFpbXBvcnRhbnQ7XG4gIH1cblxuICAuaW9uLXRleHQtbGctcmlnaHQsXG5bdGV4dC1sZy1yaWdodF0ge1xuICAgIHRleHQtYWxpZ246IHJpZ2h0ICFpbXBvcnRhbnQ7XG4gIH1cblxuICAuaW9uLXRleHQtbGctbm93cmFwLFxuW3RleHQtbGctbm93cmFwXSB7XG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcCAhaW1wb3J0YW50O1xuICB9XG5cbiAgLmlvbi10ZXh0LWxnLXdyYXAsXG5bdGV4dC1sZy13cmFwXSB7XG4gICAgd2hpdGUtc3BhY2U6IG5vcm1hbCAhaW1wb3J0YW50O1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogMTIwMHB4KSB7XG4gIC5pb24tdGV4dC14bC1jZW50ZXIsXG5bdGV4dC14bC1jZW50ZXJdIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDtcbiAgfVxuXG4gIC5pb24tdGV4dC14bC1qdXN0aWZ5LFxuW3RleHQteGwtanVzdGlmeV0ge1xuICAgIHRleHQtYWxpZ246IGp1c3RpZnkgIWltcG9ydGFudDtcbiAgfVxuXG4gIC5pb24tdGV4dC14bC1zdGFydCxcblt0ZXh0LXhsLXN0YXJ0XSB7XG4gICAgdGV4dC1hbGlnbjogc3RhcnQgIWltcG9ydGFudDtcbiAgfVxuXG4gIC5pb24tdGV4dC14bC1lbmQsXG5bdGV4dC14bC1lbmRdIHtcbiAgICB0ZXh0LWFsaWduOiBlbmQgIWltcG9ydGFudDtcbiAgfVxuXG4gIC5pb24tdGV4dC14bC1sZWZ0LFxuW3RleHQteGwtbGVmdF0ge1xuICAgIHRleHQtYWxpZ246IGxlZnQgIWltcG9ydGFudDtcbiAgfVxuXG4gIC5pb24tdGV4dC14bC1yaWdodCxcblt0ZXh0LXhsLXJpZ2h0XSB7XG4gICAgdGV4dC1hbGlnbjogcmlnaHQgIWltcG9ydGFudDtcbiAgfVxuXG4gIC5pb24tdGV4dC14bC1ub3dyYXAsXG5bdGV4dC14bC1ub3dyYXBdIHtcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwICFpbXBvcnRhbnQ7XG4gIH1cblxuICAuaW9uLXRleHQteGwtd3JhcCxcblt0ZXh0LXhsLXdyYXBdIHtcbiAgICB3aGl0ZS1zcGFjZTogbm9ybWFsICFpbXBvcnRhbnQ7XG4gIH1cbn1cblxuLyojIHNvdXJjZU1hcHBpbmdVUkw9dGV4dC1hbGlnbm1lbnQuY3NzLm1hcCAqL1xuIixudWxsLCIuaW9uLXRleHQtdXBwZXJjYXNlLFxuW3RleHQtdXBwZXJjYXNlXSB7XG4gIC8qIHN0eWxlbGludC1kaXNhYmxlLW5leHQtbGluZSBkZWNsYXJhdGlvbi1uby1pbXBvcnRhbnQgKi9cbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZSAhaW1wb3J0YW50O1xufVxuXG4uaW9uLXRleHQtbG93ZXJjYXNlLFxuW3RleHQtbG93ZXJjYXNlXSB7XG4gIC8qIHN0eWxlbGludC1kaXNhYmxlLW5leHQtbGluZSBkZWNsYXJhdGlvbi1uby1pbXBvcnRhbnQgKi9cbiAgdGV4dC10cmFuc2Zvcm06IGxvd2VyY2FzZSAhaW1wb3J0YW50O1xufVxuXG4uaW9uLXRleHQtY2FwaXRhbGl6ZSxcblt0ZXh0LWNhcGl0YWxpemVdIHtcbiAgLyogc3R5bGVsaW50LWRpc2FibGUtbmV4dC1saW5lIGRlY2xhcmF0aW9uLW5vLWltcG9ydGFudCAqL1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZSAhaW1wb3J0YW50O1xufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogNTc2cHgpIHtcbiAgLmlvbi10ZXh0LXNtLXVwcGVyY2FzZSxcblt0ZXh0LXNtLXVwcGVyY2FzZV0ge1xuICAgIC8qIHN0eWxlbGludC1kaXNhYmxlLW5leHQtbGluZSBkZWNsYXJhdGlvbi1uby1pbXBvcnRhbnQgKi9cbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlICFpbXBvcnRhbnQ7XG4gIH1cblxuICAuaW9uLXRleHQtc20tbG93ZXJjYXNlLFxuW3RleHQtc20tbG93ZXJjYXNlXSB7XG4gICAgLyogc3R5bGVsaW50LWRpc2FibGUtbmV4dC1saW5lIGRlY2xhcmF0aW9uLW5vLWltcG9ydGFudCAqL1xuICAgIHRleHQtdHJhbnNmb3JtOiBsb3dlcmNhc2UgIWltcG9ydGFudDtcbiAgfVxuXG4gIC5pb24tdGV4dC1zbS1jYXBpdGFsaXplLFxuW3RleHQtc20tY2FwaXRhbGl6ZV0ge1xuICAgIC8qIHN0eWxlbGludC1kaXNhYmxlLW5leHQtbGluZSBkZWNsYXJhdGlvbi1uby1pbXBvcnRhbnQgKi9cbiAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZSAhaW1wb3J0YW50O1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIHtcbiAgLmlvbi10ZXh0LW1kLXVwcGVyY2FzZSxcblt0ZXh0LW1kLXVwcGVyY2FzZV0ge1xuICAgIC8qIHN0eWxlbGludC1kaXNhYmxlLW5leHQtbGluZSBkZWNsYXJhdGlvbi1uby1pbXBvcnRhbnQgKi9cbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlICFpbXBvcnRhbnQ7XG4gIH1cblxuICAuaW9uLXRleHQtbWQtbG93ZXJjYXNlLFxuW3RleHQtbWQtbG93ZXJjYXNlXSB7XG4gICAgLyogc3R5bGVsaW50LWRpc2FibGUtbmV4dC1saW5lIGRlY2xhcmF0aW9uLW5vLWltcG9ydGFudCAqL1xuICAgIHRleHQtdHJhbnNmb3JtOiBsb3dlcmNhc2UgIWltcG9ydGFudDtcbiAgfVxuXG4gIC5pb24tdGV4dC1tZC1jYXBpdGFsaXplLFxuW3RleHQtbWQtY2FwaXRhbGl6ZV0ge1xuICAgIC8qIHN0eWxlbGludC1kaXNhYmxlLW5leHQtbGluZSBkZWNsYXJhdGlvbi1uby1pbXBvcnRhbnQgKi9cbiAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZSAhaW1wb3J0YW50O1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogOTkycHgpIHtcbiAgLmlvbi10ZXh0LWxnLXVwcGVyY2FzZSxcblt0ZXh0LWxnLXVwcGVyY2FzZV0ge1xuICAgIC8qIHN0eWxlbGludC1kaXNhYmxlLW5leHQtbGluZSBkZWNsYXJhdGlvbi1uby1pbXBvcnRhbnQgKi9cbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlICFpbXBvcnRhbnQ7XG4gIH1cblxuICAuaW9uLXRleHQtbGctbG93ZXJjYXNlLFxuW3RleHQtbGctbG93ZXJjYXNlXSB7XG4gICAgLyogc3R5bGVsaW50LWRpc2FibGUtbmV4dC1saW5lIGRlY2xhcmF0aW9uLW5vLWltcG9ydGFudCAqL1xuICAgIHRleHQtdHJhbnNmb3JtOiBsb3dlcmNhc2UgIWltcG9ydGFudDtcbiAgfVxuXG4gIC5pb24tdGV4dC1sZy1jYXBpdGFsaXplLFxuW3RleHQtbGctY2FwaXRhbGl6ZV0ge1xuICAgIC8qIHN0eWxlbGludC1kaXNhYmxlLW5leHQtbGluZSBkZWNsYXJhdGlvbi1uby1pbXBvcnRhbnQgKi9cbiAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZSAhaW1wb3J0YW50O1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogMTIwMHB4KSB7XG4gIC5pb24tdGV4dC14bC11cHBlcmNhc2UsXG5bdGV4dC14bC11cHBlcmNhc2VdIHtcbiAgICAvKiBzdHlsZWxpbnQtZGlzYWJsZS1uZXh0LWxpbmUgZGVjbGFyYXRpb24tbm8taW1wb3J0YW50ICovXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZSAhaW1wb3J0YW50O1xuICB9XG5cbiAgLmlvbi10ZXh0LXhsLWxvd2VyY2FzZSxcblt0ZXh0LXhsLWxvd2VyY2FzZV0ge1xuICAgIC8qIHN0eWxlbGludC1kaXNhYmxlLW5leHQtbGluZSBkZWNsYXJhdGlvbi1uby1pbXBvcnRhbnQgKi9cbiAgICB0ZXh0LXRyYW5zZm9ybTogbG93ZXJjYXNlICFpbXBvcnRhbnQ7XG4gIH1cblxuICAuaW9uLXRleHQteGwtY2FwaXRhbGl6ZSxcblt0ZXh0LXhsLWNhcGl0YWxpemVdIHtcbiAgICAvKiBzdHlsZWxpbnQtZGlzYWJsZS1uZXh0LWxpbmUgZGVjbGFyYXRpb24tbm8taW1wb3J0YW50ICovXG4gICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemUgIWltcG9ydGFudDtcbiAgfVxufVxuXG4vKiMgc291cmNlTWFwcGluZ1VSTD10ZXh0LXRyYW5zZm9ybWF0aW9uLmNzcy5tYXAgKi9cbiIsbnVsbCwiLmlvbi1hbGlnbi1zZWxmLXN0YXJ0LFxuW2FsaWduLXNlbGYtc3RhcnRdIHtcbiAgYWxpZ24tc2VsZjogZmxleC1zdGFydCAhaW1wb3J0YW50O1xufVxuXG4uaW9uLWFsaWduLXNlbGYtZW5kLFxuW2FsaWduLXNlbGYtZW5kXSB7XG4gIGFsaWduLXNlbGY6IGZsZXgtZW5kICFpbXBvcnRhbnQ7XG59XG5cbi5pb24tYWxpZ24tc2VsZi1jZW50ZXIsXG5bYWxpZ24tc2VsZi1jZW50ZXJdIHtcbiAgYWxpZ24tc2VsZjogY2VudGVyICFpbXBvcnRhbnQ7XG59XG5cbi5pb24tYWxpZ24tc2VsZi1zdHJldGNoLFxuW2FsaWduLXNlbGYtc3RyZXRjaF0ge1xuICBhbGlnbi1zZWxmOiBzdHJldGNoICFpbXBvcnRhbnQ7XG59XG5cbi5pb24tYWxpZ24tc2VsZi1iYXNlbGluZSxcblthbGlnbi1zZWxmLWJhc2VsaW5lXSB7XG4gIGFsaWduLXNlbGY6IGJhc2VsaW5lICFpbXBvcnRhbnQ7XG59XG5cbi5pb24tYWxpZ24tc2VsZi1hdXRvLFxuW2FsaWduLXNlbGYtYXV0b10ge1xuICBhbGlnbi1zZWxmOiBhdXRvICFpbXBvcnRhbnQ7XG59XG5cbi5pb24td3JhcCxcblt3cmFwXSB7XG4gIGZsZXgtd3JhcDogd3JhcCAhaW1wb3J0YW50O1xufVxuXG4uaW9uLW5vd3JhcCxcbltub3dyYXBdIHtcbiAgZmxleC13cmFwOiBub3dyYXAgIWltcG9ydGFudDtcbn1cblxuLmlvbi13cmFwLXJldmVyc2UsXG5bd3JhcC1yZXZlcnNlXSB7XG4gIGZsZXgtd3JhcDogd3JhcC1yZXZlcnNlICFpbXBvcnRhbnQ7XG59XG5cbi5pb24tanVzdGlmeS1jb250ZW50LXN0YXJ0LFxuW2p1c3RpZnktY29udGVudC1zdGFydF0ge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQgIWltcG9ydGFudDtcbn1cblxuLmlvbi1qdXN0aWZ5LWNvbnRlbnQtY2VudGVyLFxuW2p1c3RpZnktY29udGVudC1jZW50ZXJdIHtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXIgIWltcG9ydGFudDtcbn1cblxuLmlvbi1qdXN0aWZ5LWNvbnRlbnQtZW5kLFxuW2p1c3RpZnktY29udGVudC1lbmRdIHtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZCAhaW1wb3J0YW50O1xufVxuXG4uaW9uLWp1c3RpZnktY29udGVudC1hcm91bmQsXG5banVzdGlmeS1jb250ZW50LWFyb3VuZF0ge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZCAhaW1wb3J0YW50O1xufVxuXG4uaW9uLWp1c3RpZnktY29udGVudC1iZXR3ZWVuLFxuW2p1c3RpZnktY29udGVudC1iZXR3ZWVuXSB7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbiAhaW1wb3J0YW50O1xufVxuXG4uaW9uLWp1c3RpZnktY29udGVudC1ldmVubHksXG5banVzdGlmeS1jb250ZW50LWV2ZW5seV0ge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWV2ZW5seSAhaW1wb3J0YW50O1xufVxuXG4uaW9uLWFsaWduLWl0ZW1zLXN0YXJ0LFxuW2FsaWduLWl0ZW1zLXN0YXJ0XSB7XG4gIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0ICFpbXBvcnRhbnQ7XG59XG5cbi5pb24tYWxpZ24taXRlbXMtY2VudGVyLFxuW2FsaWduLWl0ZW1zLWNlbnRlcl0ge1xuICBhbGlnbi1pdGVtczogY2VudGVyICFpbXBvcnRhbnQ7XG59XG5cbi5pb24tYWxpZ24taXRlbXMtZW5kLFxuW2FsaWduLWl0ZW1zLWVuZF0ge1xuICBhbGlnbi1pdGVtczogZmxleC1lbmQgIWltcG9ydGFudDtcbn1cblxuLmlvbi1hbGlnbi1pdGVtcy1zdHJldGNoLFxuW2FsaWduLWl0ZW1zLXN0cmV0Y2hdIHtcbiAgYWxpZ24taXRlbXM6IHN0cmV0Y2ggIWltcG9ydGFudDtcbn1cblxuLmlvbi1hbGlnbi1pdGVtcy1iYXNlbGluZSxcblthbGlnbi1pdGVtcy1iYXNlbGluZV0ge1xuICBhbGlnbi1pdGVtczogYmFzZWxpbmUgIWltcG9ydGFudDtcbn1cblxuLyojIHNvdXJjZU1hcHBpbmdVUkw9ZmxleC11dGlscy5jc3MubWFwICovXG4iLCIvLyBzZWFyY2ggJiYgZmlsdGVyc1xyXG5cclxuLnR4dC1maWx0ZXIge1xyXG4gIGNvbG9yOiAkY29sb3ItZ3JheS0yO1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxuICBsZXR0ZXItc3BhY2luZzogMC4ycHg7XHJcbiAgbGluZS1oZWlnaHQ6IDE1cHg7XHJcbiAgY29sb3I6ICRjb2xvci1ncmF5LTEwO1xyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgJi5ncmVlbiB7XHJcbiAgICBjb2xvcjogJGNvbG9yLWdyZWVuO1xyXG4gIH1cclxufVxyXG5cclxuLm5hdkJhclNtYWxsVGV4dCB7XHJcbiAgY29sb3I6ICRjb2xvci1ncmVlbjtcclxuICBmb250LXNpemU6IDE2cHg7XHJcbiAgbGV0dGVyLXNwYWNpbmc6IDAuMnB4O1xyXG4gIGxpbmUtaGVpZ2h0OiAxOXB4O1xyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbn1cclxuXHJcbi8vICEgY2FyZCB0ZXh0XHJcblxyXG4uY2FyZC1zbWFsbC10eHQge1xyXG4gZm9udC1zaXplOiAxM3B4O1x0XHJcbiBmb250LXdlaWdodDogNTAwO1x0XHJcbiBsZXR0ZXItc3BhY2luZzogMC4ycHg7XHRcclxuIGxpbmUtaGVpZ2h0OiAxOHB4O1xyXG4gY29sb3I6ICRjb2xvci1ncmF5LTE0O1xyXG4gJi5saWdodCB7XHJcbiAgY29sb3IgOiAkY29sb3ItZ3JheS0xNTtcclxuIH1cclxufVxyXG4iLCIvLyBHcmVlblxyXG5cclxuJGNvbG9yLWdyZWVuOiAjMDBkMmJkOyBcclxuJGNvbG9yLWdyZWVuLTI6I0Q4RUZGNjtcclxuJGNvbG9yLWdyZWVuLTM6ICNGQUZDRkU7XHJcbiRjb2xvci1ncmVlbi01OiMwMGQyYmQ7XHJcbiRjb2xvci1ncmVlbi02OiAjMDA3YmI1O1xyXG4kY29sb3ItZ3JlZW4tNzogIzAwQTVDOTtcclxuJGNvbG9yLWdyZWVuLTg6ICMzQUJDQTU7XHJcbi8vICBCbGFja1xyXG5cclxuJGNvbG9yLWJsYWNrLTE6ICMyQjJCMkI7IFxyXG4kY29sb3ItYmxhY2stMiA6ICMwNjA2MDY7XHJcbiRjb2xvci1ibGFjay01OiAjMUMxQzFDOyBcclxuJGNvbG9yLWJsYWNrLTY6ICMzQTQxNDM7IFxyXG4kY29sb3ItYmxhY2stNzogIzY1NkQ3NjtcclxuXHJcblxyXG4vLyAgR3JheVxyXG5cclxuJGNvbG9yLWdyYXktMTogIzkwOTA5NDsgXHJcbiRjb2xvci1ncmF5LTI6ICM4MjgyODI7IFxyXG4kY29sb3ItZ3JheS0xMjogI0RBRENERTtcclxuJGNvbG9yLWdyYXktNzogI0U0RTRFQjtcclxuJGNvbG9yLWdyYXktODojRTNFM0YwO1xyXG4kY29sb3ItZ3JheS05OiNlN2U3ZjA7XHJcbiRjb2xvci1ncmF5LTEwOiAgIzhlOTA5MTtcclxuJGNvbG9yLWdyYXktMTE6ICAjZDVkOWRiO1xyXG4kY29sb3ItZ3JheS0xMjogI2YyZjVmNztcclxuJGNvbG9yLWdyYXktMTM6ICNmMWYzZjU7XHJcbiRjb2xvci1ncmF5LTE0OiAjODQ4RjlBO1xyXG4kY29sb3ItZ3JheS0xNTogI0IxQkNDNjtcclxuJGNvbG9yLWdyYXktMTY6ICNFQ0VGRjE7XHJcbiRjb2xvci1ncmF5LTE3OiAjRjBGNUZGO1xyXG4kY29sb3ItZ3JheS0xODogI0Q1RTBFQTtcclxuJGNvbG9yLWdyYXktMTk6ICNFRkVGRjM7XHJcbiRjb2xvci1ncmF5LTIwOiAjZWFlY2VmO1xyXG4kY29sb3ItZ3JheS0yMTogI2Q4ZDhkODtcclxuJGNvbG9yLWdyYXktMjI6ICM3MDcwNzA7XHJcbiRjb2xvci1ncmF5LTIzOiAjZWVlZWVlO1xyXG4vLyB3aGl0ZVxyXG5cclxuJGNvbG9yLXdoaXRlOiAjZmZmZmZmO1xyXG4kY29sb3Itd2hpdGUtZ3JheTogI2YwZjBmMDtcclxuXHJcbi8vIFJlZFxyXG5cclxuJGNvbG9yLW9yYW5nZTogI0ZFNEE0OTtcclxuXHJcbiRjb2xvci12aW9sZXQ6ICMyODM1OTM7XHJcbiRjb2xvci1ibHVlIDogIzE4NUJEQztcclxuJGNvbG9yLWRhcmstYmx1ZTogIzM5NTc5QTtcclxuJGNvbG9yLWxpZ2h0Qmx1ZTogI0Y1RjhGRjtcclxuJGNvbG9yLWJsYWNrOiAjMDAwMDAwOyIsImFwcC1idXR0b24ge1xyXG4gICAgZmxleC1zaHJpbmsgOiAwO1xyXG4gICAgJjpsYXN0LWNoaWxkIHtcclxuICAgICAgICAuYnRuLXdpdGgtaW1hZ2Uge1xyXG4gICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDIwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgICY6Zmlyc3QtY2hpbGQge1xyXG4gICAgICAgICAgICAuYnRuLXdpdGgtaW1hZ2Uge1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiA4cHg7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuLmNvbnRhaW5lci1idG4ge1xyXG4gICAgd2lkdGg6IDUwJTtcclxuICAgIHBhZGRpbmc6IDAgMjBweDtcclxuICAgIC5idG4ge1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDI0cHg7XHJcbiAgICAgICAgcGFkZGluZzogMCAxNHB4O1xyXG4gICAgICAgIC5pbWFnZS1sZWZ0IHtcclxuICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG4gICAgICAgIH1cclxuICAgICAgICAudGV4dCB7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAwIDNweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgYXBwLWJ1dHRvbiB7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcbiAgICAmLnJpZ2h0IHtcclxuICAgICAgICBwYWRkaW5nLXJpZ2h0OiA4cHg7XHJcbiAgICB9XHJcbiAgICAmLmxlZnQge1xyXG4gICAgICAgIHBhZGRpbmctbGVmdDogOHB4O1xyXG4gICAgfVxyXG59XHJcbi5idG57XHJcbiAgICBoZWlnaHQ6IDQ4cHg7XHRcclxuICAgIHdpZHRoOiAxMDAlO1x0XHJcbiAgICBib3JkZXItcmFkaXVzOiA4cHg7XHRcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBtaW4td2lkdGg6IDcycHg7XHJcbiAgICBwYWRkaW5nOiAwIDhweDtcclxuXHJcbiAgICAmLnNtYWxsLWhlaWdodCB7XHJcbiAgICAgICAgaGVpZ2h0OiAzMnB4O1xyXG4gICAgICAgIHBhZGRpbmc6IDAgOHB4O1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAmLmdyZWVuIHsgXHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLWdyZWVuLTU7XHJcbiAgICAgICAgY29sb3I6ICRjb2xvci13aGl0ZTtcclxuICAgICAgICAmLmNyZWF0ZVRleHQge1xyXG4gICAgICAgICAgICAmOjphZnRlciB7XHJcbiAgICAgICAgICAgICAgICBjb250ZW50OiBcIkNyZWF0ZSBBY2NvdW50XCI7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xyXG4gICAgICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgICAgICAgICAgICAgICBsZXR0ZXItc3BhY2luZzogMC4ycHg7XHJcbiAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogMjJweDtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDEpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBcclxuICAgIH1cclxuICAgICYuZ3JlZW4tYmx1ZSB7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLWdyZWVuLTg7XHJcbiAgICB9XHJcbiAgICAmLmdyYWRpZW50LWdyZWVuIHtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3ItZ3JlZW4tNTtcclxuICAgICAgICBjb2xvcjogJGNvbG9yLXdoaXRlO1xyXG4gICAgfVxyXG4gICAgJi5saW5rLWhlbHAge1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1x0XHRcdFx0XHJcbiAgICB9XHJcbiAgICAmLmxpZ2h0LWJsdWUge1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1saWdodEJsdWU7XHJcbiAgICB9XHJcbiAgICAmLmRhcmtHcmF5IHtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiRjb2xvci1ncmF5LTE0O1xyXG4gICAgfVxyXG4gICAgJi53aGl0ZSB7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLXdoaXRlO1xyXG4gICAgfVxyXG4gICAgJi5ncmVlbi1ib3JkZXIge1xyXG4gICAgICAgIGJveC1zaGFkb3c6IDAgMCAycHggMCByZ2JhKDAsMCwwLDAuMTIpO1xyXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICRjb2xvci1ncmVlbi01O1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICRjb2xvci13aGl0ZTtcclxuICAgICAgICAmLmxvZ2luVGV4dCB7XHJcbiAgICAgICAgICAgICY6OmFmdGVyIHtcclxuICAgICAgICAgICAgICAgIGNvbnRlbnQ6IFwiTG9nIGluXCI7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xyXG4gICAgICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgICAgICAgICAgICAgICBsZXR0ZXItc3BhY2luZzogMC4ycHg7XHJcbiAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogMjJweDtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiAjMDBkMmJkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgJi53aGl0ZS13aXRoLWJvcmRlciB7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLXdoaXRlO1xyXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICRjb2xvci1ncmVlbi01O1xyXG4gICAgICAgIGNvbG9yOiAkY29sb3ItZ3JlZW4tNTtcclxuICAgICAgICBoZWlnaHQ6IDQycHg7XHJcblxyXG4gICAgICAgICYuZGFyay1ncmVlbiB7XHJcbiAgICAgICAgICAgIGJvcmRlci1jb2xvcjogJGNvbG9yLWdyZWVuLTc7XHJcbiAgICAgICAgICAgIGNvbG9yOiAkY29sb3ItZ3JlZW4tNztcclxuICAgICAgICAgICAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgICYuZGFyay1ibHVlIHtcclxuICAgICAgICAgICAgYm9yZGVyLWNvbG9yOiAkY29sb3ItZGFyay1ibHVlO1xyXG4gICAgICAgICAgICBjb2xvcjogJGNvbG9yLWRhcmstYmx1ZTtcclxuICAgICAgICAgICAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgICYuZ3JlZW4ge1xyXG4gICAgICAgICAgICBib3JkZXItY29sb3I6ICRjb2xvci1ncmVlbi02O1xyXG4gICAgICAgICAgICBjb2xvcjogJGNvbG9yLWdyZWVuLTY7XHJcbiAgICAgICAgICAgIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZiAhaW1wb3J0YW50O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5pbWFnZS1yaWdodCB7XHJcbiAgICAgICAgd2lkdGg6IDExcHg7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDZweDtcclxuICAgIH1cclxuICAgIC5pbWFnZS1sZWZ0IHtcclxuICAgICAgICB3aWR0aDogMjBweDtcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDhweDtcclxuICAgIH1cclxuICAgICYuaW5wdXRCdXR0b24ge1xyXG4gICAgICAgIHBhZGRpbmc6IDAgMTZweDtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3ItZ3JheS0xNzsgXHJcbiAgICAgICAgY29sb3I6ICRjb2xvci1ibGFjaztcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAzMHB4O1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDZweDtcclxuICAgICAgICAuaW1hZ2UtcmlnaHQge1xyXG4gICAgICAgICAgICB3aWR0aDogMTZweDtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgICAgICAgfVxyXG4gICAgICAgICYubGVmdC1pdGVtIHtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAmLmNvbXBsZXhCdXR0b24ge1xyXG4gICAgICAgIGhlaWdodDogMzJweDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA2cHg7XHJcbiAgICAgICAgbWluLXdpZHRoOiBhdXRvO1xyXG4gICAgICAgIHdpZHRoOiA3MnB4O1xyXG4gICAgfSBcclxufVxyXG4uYnRuLXdpdGgtaW1hZ2Uge1xyXG4gICAgaGVpZ2h0OiAzOXB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLXdoaXRlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNnB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHBhZGRpbmc6IDBweCAxMnB4O1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAxMnB4O1xyXG4gICAgbWF4LXdpZHRoOiAzMDBweDtcclxuICAgIGJveC1zaGFkb3c6IGluc2V0IDAgLTJweCAwIDAgJGNvbG9yLWdyYXktMTM7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAkY29sb3ItZ3JheS0xMztcclxuICAgIGJvcmRlci1ib3R0b206IDA7XHJcbiAgICBiYWNrZ3JvdW5kOiAkY29sb3Itd2hpdGUgIWltcG9ydGFudDtcclxuICAgIG1hcmdpbi1ib3R0b206IDNweDtcclxuICAgIHBhZGRpbmctYm90dG9tOiAxcHg7XHJcbiAgICAuaW1hZ2UtcmlnaHQge1xyXG4gICAgICAgIHdpZHRoOiAxNnB4O1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiA2cHg7XHJcbiAgICB9XHJcbiAgICAuaW1hZ2UtbGVmdCB7XHJcbiAgICAgICAgd2lkdGg6IDE2cHg7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiA2cHg7XHJcbiAgICB9XHJcbiAgICAvLyAmLmdyZWVuIHtcclxuICAgIC8vICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3ItbGlnaHRCbHVlO1xyXG4gICAgLy8gICAgIGJvcmRlcjogMXB4IHNvbGlkICRjb2xvci1saWdodEJsdWU7XHJcbiAgICAvLyB9XHJcbiAgICAmLm5vLWJvcmRlciB7XHJcbiAgICAgICAgYm9yZGVyOiAwO1xyXG4gICAgfVxyXG4gICAgLmNsb3NlLWljb24ge1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgIH1cclxuXHJcbn1cclxuLndyYXAtYnV0dG9uIHtcclxuICAgIG1hcmdpbi10b3A6IC0xNnB4O1xyXG4gICAgLmJ0bi13aXRoLWltYWdlIHtcclxuICAgICAgICBtYXJnaW4tdG9wOiAxNnB4O1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDNweDtcclxuICAgIH1cclxufVxyXG4udHJpbS1vbmUtbGluZSB7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcclxuICAgIC13ZWJraXQtbGluZS1jbGFtcDogMTtcclxuICAgIC8qIGF1dG9wcmVmaXhlcjogaWdub3JlIG5leHQgKi9cclxuICAgIC13ZWJraXQtYm94LW9yaWVudDogdmVydGljYWw7XHJcbn1cclxuLmJ1dHRvbi13aXRoSW1hZ2UtdGV4dCB7XHJcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xyXG59XHJcbi5zZW5kLWJ1dHRvbiB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBib3R0b206IDEycHg7XHJcbiAgICByaWdodDogMTJweDtcclxuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG59XHJcblxyXG4iLCIubWFpblNlY3Rpb25DbGFzcyB7XHJcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICRjb2xvci1ncmF5LTEzO1xyXG4gIC5zZXBlcmF0ZS1sZWZ0LWNvbnRhaW5lciB7XHJcbiAgICBtYXJnaW4tbGVmdDogMTJweDtcclxuICB9XHJcbiAgLnNlcHJhdG9yLWxlZnQtaXRlbSAge1xyXG4gICAgbWFyZ2luLXRvcDogOHB4O1xyXG4gIH1cclxufVxyXG5cclxuLnNlcGVyYXRlLXdpdGgtaW1hZ2Uge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgXHJcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3Itd2hpdGU7XHJcbiAgJi5uby1tYXJnaW4ge1xyXG4gICAgbWFyZ2luLXRvcDogMDtcclxuICB9XHJcbiAgXHJcbiAgLnNlcGVyYXRlLWxlZnQtY29udGFpbmVyIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgLnNlcGVyYXRvci1pbWFnZSB7XHJcbiAgICAgIHdpZHRoOiAxMnB4O1xyXG4gICAgICBtYXJnaW4tcmlnaHQ6IDlweDtcclxuICAgIH1cclxuICAgIFxyXG4gIH1cclxuICAuc2VwcmF0b3ItbGVmdC1pdGVtIHtcclxuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gICAgaGVpZ2h0OiAzM3B4O1xyXG4gICAgcGFkZGluZzogMCA4cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxNHB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gICAgLy8gbWFyZ2luLXRvcDogOXB4O1xyXG5cclxuICAgIGltZyB7XHJcbiAgICAgIHdpZHRoOiAxOHB4O1xyXG4gICAgICBtYXJnaW4tbGVmdDogNnB4O1xyXG4gICAgfVxyXG4gICAgJjphY3RpdmUge1xyXG4gICAgICB0cmFuc2Zvcm06IHNjYWxlKDAuOSk7XHJcbiAgICAgIC8vIHRyYW5zaXRpb246IGFsbCAwLjFzO1xyXG4gICAgfVxyXG4gICAgLy8gJjo6YmVmb3JlIHtcclxuICAgIC8vICAgY29udGVudDogXCJcIjtcclxuICAgIC8vICAgaGVpZ2h0OiAyMnB4O1xyXG4gICAgLy8gICB3aWR0aDogMzNweDtcclxuICAgIC8vICAgb3BhY2l0eTogMDtcclxuICAgIC8vICAgYmFja2dyb3VuZDogcmdiYSgwLCAyMTAsIDE4OSwwLjEpO1xyXG4gICAgLy8gICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAvLyB9XHJcbiAgICAvLyAmOmFjdGl2ZSB7XHJcbiAgICAvLyAgICY6OmJlZm9yZSB7XHJcbiAgICAvLyAgICAgdHJhbnNmb3JtOiBzY2FsZSgyKTtcclxuICAgIC8vICAgICB0cmFuc2l0aW9uOiBhbGwgMC4xcztcclxuICAgIC8vICAgICBvcGFjaXR5OiAxO1xyXG4gICAgLy8gICB9XHJcbiAgICAvLyB9XHJcbiAgfVxyXG4gIFxyXG4gICYubWFpbiB7XHJcbiAgICAuc2VwZXJhdG9yLWltYWdlIHtcclxuICAgICAgd2lkdGg6IDMycHg7XHJcbiAgICAgIGhlaWdodDogMzJweDtcclxuICAgICAgbWFyZ2luLXRvcDogNnB4O1xyXG4gICAgfVxyXG4gICAgJi5uby1ib3JkZXIge1xyXG4gICAgICBib3JkZXI6IDA7XHJcbiAgICAgIHBhZGRpbmctYm90dG9tOiAwO1xyXG4gICAgfVxyXG4gIH1cclxuICAmLnNwZWFjaWFsLWNhc2Uge1xyXG4gICAgcGFkZGluZzogMjRweCAyMHB4O1xyXG4gICAgcGFkZGluZy1ib3R0b206IDA7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxM3B4IDEzcHggMCAwO1xyXG4gICAgbWFyZ2luLXRvcDogLTEzcHg7XHJcbiAgfVxyXG59XHJcblxyXG4uaW9zIHtcclxuICAuc2VwcmF0b3ItbGVmdC1pdGVtIHtcclxuICAgIHBhZGRpbmctdG9wOiAxcHg7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDEycHg7XHJcbiAgfVxyXG59XHJcbi5zaW1wbGUtc2VwYXJhdG9yIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIHBhZGRpbmctdG9wOiAyNXB4O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgJi5ib3JkZXIge1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGhzbGEoMjQwLCAzMCUsIDg4JSwgMC40Nik7XHJcbiAgfVxyXG4gIC5yaWdodCB7XHJcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcblxyXG4gICAgaW1nIHtcclxuICAgICAgd2lkdGg6IDEycHg7XHJcbiAgICAgIG1hcmdpbi1sZWZ0OiA2cHg7XHJcbiAgICB9XHJcbiAgfVxyXG4gICYubGVmdC1zdWJ0aXRsZSB7XHJcbiAgICBtYXJnaW4tdG9wOiAycHg7XHJcbiAgfVxyXG59XHJcblxyXG4vL2R5bmFtaWMgY2xhc3NcclxuLmltYWdlLWNvbnRhaW5lciB7XHJcbiAgd2lkdGg6IDE4cHg7XHJcbiAgaGVpZ2h0OiAxOHB4O1xyXG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi9hc3NldHMvaWNvbnMvZGlhc3BvcmFJY29uL01vcmVfQXJyb3cuc3ZnXCIpO1xyXG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7IFxyXG5cclxufVxyXG5cclxuXHJcblxyXG5cclxuXHJcbiIsIi8vIHJlbW92ZSB0aGUgYm9yZGVyIGZyb20gdGhlIGxhc3QgaXRlbSBpbiB0aGUgbGlzdFxyXG5hcHAtbGlzdCB7XHJcbiAgJjpsYXN0LWNoaWxkIHtcclxuICAgIC5saXN0IHtcclxuICAgICAgLmxpc3QtaXRlbXMtbWlkZGxlIHtcclxuICAgICAgICBib3JkZXI6IDA7XHJcbiAgICAgIH1cclxuICAgICAgLmxpc3QtaXRlbXMtcmlnaHQge1xyXG4gICAgICAgIGJvcmRlcjogMDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmxpc3QtY29udGFpbmVyIHtcclxuICAgICAgYm9yZGVyOiAwO1xyXG4gICAgfVxyXG4gICAgLnNtYWxsIHtcclxuICAgICAgYm9yZGVyLWJvdHRvbTogMDtcclxuICAgIH1cclxuICAgIC5zaW1wbGUtbGlzdCB7XHJcbiAgICAgIC5saXN0LWNvbnRhaW5lciB7XHJcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG5hcHAtbGlzdC1jYXJkIHtcclxuICAmOmxhc3QtY2hpbGQge1xyXG4gICAgLmxpc3Qge1xyXG4gICAgICAubGlzdC1pdGVtcy1taWRkbGUge1xyXG4gICAgICAgIGJvcmRlcjogMDtcclxuICAgICAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmxpc3QtY29udGFpbmVyIHtcclxuICAgICAgYm9yZGVyOiAwO1xyXG4gICAgICAubGlzdC1pdGVtcy1yaWdodCB7XHJcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMCAhaW1wb3J0YW50O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfSBcclxufVxyXG5cclxuLmFwcC1saXN0LWNvbnRhaW5lciB7XHJcbiAgJjpsYXN0LWNoaWxkIHtcclxuICAgIC5zZWNvbmQtdHlwZS1saXN0IHtcclxuICAgICAgYm9yZGVyOiAwO1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gICAgICBwYWRkaW5nLWJvdHRvbTogMHB4O1xyXG4gICAgfVxyXG4gICAgLndpdGgtcGFkZGluZy1ib3R0b217XHJcbiAgICAgIC5zZWNvbmQtdHlwZS1saXN0IHtcclxuICAgICAgICBwYWRkaW5nLWJvdHRvbTogMTBweCA7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuYXBwLWZpbHRlci1wZW9wbGUtcmVzdWx0IHtcclxuICAmOmxhc3QtY2hpbGQge1xyXG4gICAgLmxpc3Qge1xyXG4gICAgICAubGlzdC1pdGVtcy1taWRkbGUge1xyXG4gICAgICAgIGJvcmRlcjogMDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuYXBwLWNhcmQtc2VhcmNoIHtcclxuICAmOmxhc3QtY2hpbGQge1xyXG4gICAgLmxpc3Qge1xyXG4gICAgICAubGlzdC1pdGVtcy1taWRkbGUge1xyXG4gICAgICAgIGJvcmRlcjogMDtcclxuICAgICAgICBwYWRkaW5nLWJvdHRvbSA6IDEwcHg7XHJcbiAgICAgIH1cclxuICAgICAgLmxpc3QtaXRlbXMtcmlnaHQge1xyXG4gICAgICAgIGJvcmRlcjogMCAhaW1wb3J0YW50O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG4vLyBzY3JvbGwgTGlzdFxyXG4ubGlzdC1zY3JvbGwtY29udGFpbmVyIHtcclxuICBmbGV4LXNocmluazogMDtcclxuICBwYWRkaW5nLWxlZnQ6IDhweDtcclxuICBtaW4td2lkdGg6IDk2JTtcclxuICAmOmZpcnN0LWNoaWxkIHtcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgfVxyXG59XHJcblxyXG4vLyBub3JtYWwgbGlzdCBmaXJzdCBkZXNpZ24gdGhlIGZpcnN0IGRlc2lnbiBvZiBsaXN0XHJcblxyXG4ubGlzdCB7XHJcbiAgJi5iaWctbGlzdCB7XHJcbiAgICBtaW4taGVpZ2h0OiA2MHB4O1xyXG4gIH1cclxuICAubGlzdC1jb250YWluZXIge1xyXG4gICAgLy8gbGlzdCB3aXRoIGltYWdlIGFuZCAyIHRleHRcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBtYXJnaW4tdG9wOiAxOXB4O1xyXG4gICAgYmFja2dyb3VuZDogJGNvbG9yLXdoaXRlO1xyXG4gICAgLmZpcnN0LXR4dCB7XHJcbiAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcclxuICAgICAgLXdlYmtpdC1saW5lLWNsYW1wOiAyO1xyXG4gICAgICAvKiBhdXRvcHJlZml4ZXI6IGlnbm9yZSBuZXh0ICovXHJcbiAgICAgIC13ZWJraXQtYm94LW9yaWVudDogdmVydGljYWw7XHJcbiAgICB9XHJcbiAgICAubGlzdC1pdGVtcy1sZWZ0IHtcclxuICAgICAgZmxleC1zaHJpbms6IDA7XHJcbiAgICAgIGhlaWdodDogNTRweDtcclxuICAgICAgd2lkdGg6IDU0cHg7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1ncmF5LTEzO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gICAgICBib3JkZXI6IDFweCBzb2xpZCAkY29sb3ItZ3JheS0xMztcclxuICAgICAgaW1nIHtcclxuICAgICAgICB3aWR0aDogNTRweDtcclxuICAgICAgICBoZWlnaHQ6IDUycHg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgICAgIG9iamVjdC1maXQ6IGNvdmVyO1xyXG4gICAgICB9XHJcbiAgICAgIC5zbWFsbC1yYWRpdXMge1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIC5saXN0LWl0ZW1zLW1pZGRsZSB7XHJcbiAgICAgIG1hcmdpbi1sZWZ0OiAxNHB4O1xyXG4gICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgJGNvbG9yLWdyYXktMTM7XHJcbiAgICAgIG1pbi1oZWlnaHQ6IDU2cHg7XHJcbiAgICAgIHdpZHRoOiBjYWxjKDEwMCUgLSA1NXB4KTtcclxuICAgICAgcGFkZGluZy1ib3R0b206IDE5cHg7XHJcbiAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgIHBhZGRpbmctcmlnaHQ6IDEwcHg7XHJcbiAgICAgIC5zZWNvbmQtdHh0IHtcclxuICAgICAgICB3aWR0aDogOTUlO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogM3B4O1xyXG4gICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICAgICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XHJcbiAgICAgICAgLXdlYmtpdC1saW5lLWNsYW1wOiAxO1xyXG4gICAgICAgIC8qIGF1dG9wcmVmaXhlcjogaWdub3JlIG5leHQgKi9cclxuICAgICAgICAtd2Via2l0LWJveC1vcmllbnQ6IHZlcnRpY2FsO1xyXG4gICAgICB9XHJcbiAgICAgIC5saXN0LWl0ZW1zLW1pZGRsZS1jb250YWluZXIge1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDE2cHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIC5saXN0LWl0ZW1zLXJpZ2h0IHtcclxuICAgICAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgICAgIG1hcmdpbi10b3A6IDE2cHg7XHJcbiAgICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgJGNvbG9yLWdyYXktMTMgIWltcG9ydGFudDtcclxuICAgICAgaW1nIHtcclxuICAgICAgICBoZWlnaHQ6IDIzLjVweDtcclxuICAgICAgICB3aWR0aDogMjMuNXB4O1xyXG4gICAgICB9XHJcbiAgICAgICYuYmlnIHtcclxuICAgICAgICBtYXJnaW4tdG9wOiAxNHB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAxMHB4O1xyXG4gICAgICAgIGltZyB7XHJcbiAgICAgICAgICBoZWlnaHQ6IDI3cHg7XHJcbiAgICAgICAgICB3aWR0aDogMjdweDtcclxuICAgICAgICAgIC8vIG1hcmdpbi1yaWdodDogLThweDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgJi5jZW50ZXIge1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDE0cHggIWltcG9ydGFudDtcclxuICAgICAgfVxyXG4gICAgICAmLmxlZnQtZ3JheS1saW5lIHtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgJjo6YmVmb3JlIHtcclxuICAgICAgICAgIGNvbnRlbnQ6IFwiIFwiO1xyXG4gICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgd2lkdGg6IDJweDtcclxuICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNkMGQzZDY7XHJcbiAgICAgICAgICByaWdodDogMzRweDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgICYud2l0aC1vbmUtdGV4dCB7XHJcbiAgICAgIC8vIGxpc3Qgd2l0aCBpbWFnZSBhbmQgb25lIHRleHRcclxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgbWFyZ2luLXRvcDogMHB4O1xyXG5cclxuICAgICAgLnNlY29uZC10eHQge1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDA7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC5saXN0LWl0ZW1zLW1pZGRsZSB7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIHBhZGRpbmc6IDA7XHJcbiAgICAgICAgbWluLWhlaWdodDogNDJweDtcclxuICAgICAgICAmLmJpZy1saXN0IHtcclxuICAgICAgICAgIG1pbi1oZWlnaHQ6IDYwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5yaWdodC1pY29uIHtcclxuICAgICAgICAgIHdpZHRoOiAxMnB4O1xyXG4gICAgICAgICAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgICAgICAgICBmbGV4LXNocmluazogMDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIC5lbnRpdHktZW50aXR5LXR5cGUge1xyXG4gICAgICBtYXJnaW4tdG9wOiAxcHg7XHJcbiAgICB9XHJcbiAgICAmLmNlbnRlciB7XHJcbiAgICAgIC5saXN0LWl0ZW1zLW1pZGRsZSB7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAwO1xyXG4gICAgICB9XHJcbiAgICAgIC5saXN0LWl0ZW1zLXJpZ2h0ICB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMTNweDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG4uc3dpcGVyLWNvbnRhaW5lciB7XHJcbiAgLmZpcnN0LXR4dCB7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcclxuICAgIC13ZWJraXQtbGluZS1jbGFtcDogMSAhaW1wb3J0YW50O1xyXG4gICAgLyogYXV0b3ByZWZpeGVyOiBpZ25vcmUgbmV4dCAqL1xyXG4gICAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcclxuICB9XHJcbn1cclxuLm5vLXRyaW0ge1xyXG4gIC5maXJzdC10eHQge1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XHJcbiAgICAtd2Via2l0LWxpbmUtY2xhbXA6IDYgIWltcG9ydGFudDtcclxuICAgIC8qIGF1dG9wcmVmaXhlcjogaWdub3JlIG5leHQgKi9cclxuICAgIC13ZWJraXQtYm94LW9yaWVudDogdmVydGljYWw7XHJcbiAgfVxyXG4gIC5zbWFsbC1saXN0IHtcclxuICAgIG1heC13aWR0aDogMTAwJSAhaW1wb3J0YW50O1xyXG4gIH1cclxuICAubGlzdCB7XHJcbiAgICAubGlzdC1jb250YWluZXIge1xyXG4gICAgICBtYXJnaW4tdG9wOiAwICFpbXBvcnRhbnQ7XHJcbiAgICAgIHBhZGRpbmctdG9wOiAyMHB4ICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxufVxyXG4vLyBub3JtYWwgbGlzdCBmaXJzdCBkZXNpZ24gdGhlIHNlY29uZCBkZXNpZ24gb2YgbGlzdFxyXG5cclxuLnNlY29uZC10eXBlLWxpc3Qge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgbWFyZ2luOiAyMHB4IDA7XHJcbiAgdGV4dC1hbGlnbjogbGVmdDtcclxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgJGNvbG9yLWdyYXktMTM7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLXdoaXRlO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgcGFkZGluZy1ib3R0b206IDIwcHg7XHJcbiAgLmxlZnQtaXRlbSB7XHJcbiAgICB3aWR0aCA6IDEwMCU7XHJcbiAgICAuc3RhcnQtaXRlbSB7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDlweDtcclxuICAgICAgc3BhbiB7XHJcbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuICAgICAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcclxuICAgICAgICAtd2Via2l0LWxpbmUtY2xhbXA6IDE7XHJcbiAgICAgICAgLyogYXV0b3ByZWZpeGVyOiBpZ25vcmUgbmV4dCAqL1xyXG4gICAgICAgIC13ZWJraXQtYm94LW9yaWVudDogdmVydGljYWw7XHJcbiAgICAgICAgcGFkZGluZy1yaWdodDogMTBweDtcclxuICAgICAgICBtYXJnaW4tbGVmdDogOHB4O1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAubWlkZGxlLWl0ZW0ge1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiA2cHg7XHJcbiAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcclxuICAgICAgLXdlYmtpdC1saW5lLWNsYW1wOiAyO1xyXG4gICAgICAvKiBhdXRvcHJlZml4ZXI6IGlnbm9yZSBuZXh0ICovXHJcbiAgICAgIC13ZWJraXQtYm94LW9yaWVudDogdmVydGljYWw7XHJcbiAgICAgIHdpZHRoOiA5MCU7XHJcbiAgICB9XHJcbiAgICAuZW5kLWl0ZW0ge1xyXG4gICAgfVxyXG4gIH1cclxuICAucmlnaHQtaXRlbSB7XHJcbiAgICBmbGV4LXNocmluazogMDtcclxuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gICAgaGVpZ2h0OiA4MHB4O1xyXG4gICAgd2lkdGg6IDgwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3ItZ3JheS0xMztcclxuICAgIGJvcmRlci1yYWRpdXM6IDE2cHg7XHJcbiAgICBpbWcge1xyXG4gICAgICBoZWlnaHQ6IDgwcHg7XHJcbiAgICAgIHdpZHRoOiA4MHB4O1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAxNnB4O1xyXG4gICAgICBvYmplY3QtZml0OiBjb3ZlcjtcclxuICAgIH1cclxuICB9XHJcbiAgLmltYWdlLWl0ZW0ge1xyXG4gICAgaGVpZ2h0OiAyMHB4O1xyXG4gICAgd2lkdGg6IDIwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XHJcbiAgICBvYmplY3QtZml0OiBjb3ZlcjtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICRjb2xvci1ncmF5LTEzO1xyXG4gIH1cclxufVxyXG5cclxuLnRoaXJkLXR5cGUtbGlzdCB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgcGFkZGluZzogMTZweCAxNXB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci13aGl0ZTtcclxuICBib3gtc2hhZG93OiBpbnNldCAwIC0ycHggMCAwICRjb2xvci1ncmF5LTEzO1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICRjb2xvci1ncmF5LTEzO1xyXG4gIGJvcmRlci1ib3R0b206IDA7XHJcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgLmxlZnQge1xyXG4gICAgaGVpZ2h0OiA0NHB4O1xyXG4gICAgd2lkdGg6IDQ0cHg7XHJcbiAgICBmbGV4LXNocmluazogMDtcclxuICAgIGltZyB7XHJcbiAgICAgIHdpZHRoOiA0NHB4O1xyXG4gICAgICBoZWlnaHQ6IDQ0cHg7XHJcbiAgICAgIG9iamVjdC1maXQ6IGNvdmVyO1xyXG4gICAgfVxyXG4gIH1cclxuICAubWlkZGxlIHtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMnB4O1xyXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgIHBhZGRpbmctcmlnaHQ6IDJweDtcclxuICAgIC5maXJzdCB7XHJcbiAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcclxuICAgICAgLXdlYmtpdC1saW5lLWNsYW1wOiAxO1xyXG4gICAgICAvKiBhdXRvcHJlZml4ZXI6IGlnbm9yZSBuZXh0ICovXHJcbiAgICAgIC13ZWJraXQtYm94LW9yaWVudDogdmVydGljYWw7XHJcbiAgICB9XHJcbiAgICAuc2Vjb25kIHtcclxuICAgICAgbWFyZ2luLXRvcDogNXB4O1xyXG4gICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuICAgICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XHJcbiAgICAgIC13ZWJraXQtbGluZS1jbGFtcDogMTtcclxuICAgICAgLyogYXV0b3ByZWZpeGVyOiBpZ25vcmUgbmV4dCAqL1xyXG4gICAgICAtd2Via2l0LWJveC1vcmllbnQ6IHZlcnRpY2FsO1xyXG4gICAgfVxyXG4gIH1cclxuICAucmlnaHQge1xyXG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOiAyNHB4O1xyXG4gICAgaW1nIHtcclxuICAgICAgd2lkdGg6IDI0cHg7XHJcbiAgICAgIGhlaWdodDogMjRweDtcclxuICAgIH1cclxuICB9XHJcbiAgJi5zbWFsbCB7XHJcbiAgICBwYWRkaW5nOiAxNnB4IDE4cHg7XHJcbiAgICAubGVmdCB7XHJcbiAgICAgIGhlaWdodDogMzZweDtcclxuICAgICAgd2lkdGg6IDM2cHg7XHJcbiAgICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gICAgICBpbWcge1xyXG4gICAgICAgIHdpZHRoOiAzNnB4O1xyXG4gICAgICAgIGhlaWdodDogMzZweDtcclxuICAgICAgICBvYmplY3QtZml0OiBjb3ZlcjtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIC5yaWdodCB7XHJcbiAgICAgIHdpZHRoOiAyN3B4O1xyXG4gICAgICBpbWcge1xyXG4gICAgICAgIHdpZHRoOiAyN3B4O1xyXG4gICAgICAgIGhlaWdodDogMjdweDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuLmZvdXJ0aC10eXBlLWxpc3Qge1xyXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAkY29sb3ItZ3JheS0xMztcclxuICBwYWRkaW5nLWJvdHRvbTogMjBweDtcclxuICAmOm5vdCg6Zmlyc3QtY2hpbGQpIHtcclxuICAgIHBhZGRpbmctdG9wOiA2cHg7XHJcbiAgfVxyXG4gICY6bGFzdC1jaGlsZCB7XHJcbiAgICBib3JkZXItYm90dG9tOiAwICFpbXBvcnRhbnQ7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcclxuICB9XHJcbiAgJi5uby1ib3JkZXIge1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMDtcclxuICB9XHJcbiAgLmNvbnRhaW5lciB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgbWFyZ2luLXRvcDogMjBweDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAvLyBwYWRkaW5nLXJpZ2h0OiA3cHg7XHJcbiAgICBpbWcge1xyXG4gICAgICB3aWR0aDogMjBweDtcclxuICAgICAgaGVpZ2h0OiAyMHB4O1xyXG4gICAgICBtYXJnaW4tcmlnaHQ6IDEycHg7XHJcbiAgICB9XHJcbiAgICAudHh0LWNvbnRhaW5lciB7XHJcbiAgICAgIC8vIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgICAgIC50eHQge1xyXG4gICAgICAgIC8vIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgLy8gdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICAgICAgLy8gZGlzcGxheTogLXdlYmtpdC1ib3g7XHJcbiAgICAgICAgLy8gLXdlYmtpdC1saW5lLWNsYW1wOiAwO1xyXG4gICAgICAgIC8vIC8qIGF1dG9wcmVmaXhlcjogaWdub3JlIG5leHQgKi9cclxuICAgICAgICAvLyAtd2Via2l0LWJveC1vcmllbnQ6IHZlcnRpY2FsO1xyXG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDhweDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuICAucmlnaHQtaXRlbSB7XHJcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LXNocmluazogMDtcclxuICAgIC5yaWdodC1pdGVtLWNvbnRhaW5lciB7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgIGltZyB7XHJcbiAgICAgICAgd2lkdGg6IDEycHggIWltcG9ydGFudDtcclxuICAgICAgICBoZWlnaHQ6IDEycHg7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiA0cHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIGltZyB7XHJcbiAgICAgIHdpZHRoOiAyNHB4O1xyXG4gICAgICBoZWlnaHQ6IDI0cHg7XHJcbiAgICAgIG1hcmdpbi1yaWdodDogMDtcclxuICAgICAgbWFyZ2luLWxlZnQ6IDE2cHg7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5yaWdodC1iaWctaW1hZ2Uge1xyXG4gICAgd2lkdGg6IDEwOHB4O1xyXG4gICAgaGVpZ2h0OiAxMDhweDtcclxuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZmxleC1zaHJpbms6IDA7XHJcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gICAgaW1nIHtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgYm9yZGVyLXJhZGl1czogNnB4O1xyXG4gICAgICBvYmplY3QtZml0OiBjb3ZlcjtcclxuICAgIH1cclxuICAgIC5vdmVybGF5IHtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgIGhlaWdodDogMzJweDtcclxuICAgICAgYm9yZGVyLXJhZGl1czogMCAwIDZweCA2cHg7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC42KTtcclxuICAgICAgY29sb3I6ICRjb2xvci1ncmF5LTE2O1xyXG4gICAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICAgIGxldHRlci1zcGFjaW5nOiAwLjJweDtcclxuICAgICAgbGluZS1oZWlnaHQ6IDE4cHg7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICBib3R0b206IDA7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuLy8gbGlzdCB0YWtlIG1hbnkgYWN0aW9uXHJcblxyXG4uYWN0aW9uLWxpc3Qge1xyXG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgaGVpZ2h0OiA3NXB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgyNDAsIDI0NSwgMjU1LCAwLjk4KTtcclxuICAubGlzdC1jb250YWluZXIge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIC5pdGVtIHtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgY29sb3I6ICRjb2xvci1ncmVlbi01O1xyXG4gICAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgICAgIGxldHRlci1zcGFjaW5nOiAwLjJweDtcclxuICAgICAgbGluZS1oZWlnaHQ6IDE4cHg7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgaW1nIHtcclxuICAgICAgICB3aWR0aDogMThweDtcclxuICAgICAgfVxyXG4gICAgICBzcGFuIHtcclxuICAgICAgICBtYXJnaW4tdG9wOiA0cHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbmFwcC1pbmZvLWNhcmQge1xyXG4gICY6bGFzdC1jaGlsZCB7XHJcbiAgICAuaW5mby1saXN0IHtcclxuICAgICAgYm9yZGVyOiAwO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4uaW5mby1saXN0IHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIHBhZGRpbmc6IDIwcHggMDtcclxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgJGNvbG9yLWdyYXktMTc7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAuZmlyc3QtaXRlbSB7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDEycHg7XHJcbiAgICBmbGV4LXNocmluazogMDtcclxuICAgIC8vIGJhY2tncm91bmQ6IHJnYigyNDAsIDI0NSwgMjU1KTtcclxuICAgIHdpZHRoOiAzMnB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcbiAgICBoZWlnaHQ6IDU0cHg7XHJcbiAgICBcclxuICAgIGltZyB7XHJcbiAgICAgIGhlaWdodDogMzJweDtcclxuICAgICAgd2lkdGg6IDMycHg7XHJcbiAgICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5taWRkbGUtaXRlbSB7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBwYWRkaW5nOiA1cHggN3B4O1xyXG4gICAgd2lkdGggOiA4NSU7XHJcbiAgICAudGl0bGUge1xyXG4gICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuICAgICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XHJcbiAgICAgIC13ZWJraXQtbGluZS1jbGFtcDogMTtcclxuICAgICAgLyogYXV0b3ByZWZpeGVyOiBpZ25vcmUgbmV4dCAqL1xyXG4gICAgICAtd2Via2l0LWJveC1vcmllbnQ6IHZlcnRpY2FsO1xyXG4gICAgfVxyXG4gICAgLnN1YnRpdGxlIHtcclxuICAgICAgZm9udC1mYW1pbHk6IDEzcHg7XHJcbiAgICAgIGNvbG9yOiAkY29sb3ItZ3JheS0xNTtcclxuICAgIH1cclxuICB9XHJcbiAgLmVuZC1pdGVtIHtcclxuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBmbGV4LXNocmluazogMDtcclxuICAgIGltZyB7XHJcbiAgICAgIGhlaWdodDogMjRweDtcclxuICAgIH1cclxuICB9XHJcbiAgXHJcbn1cclxuXHJcbi8vIGxpc3Qgb2YgaW1hZ2UgbGlrZSBpbWFnZSB0byBvcGVuIGdhbGxlcnlcclxuXHJcbmFwcC1tZWRpYS1jYXJkIHtcclxuICAmOmxhc3QtY2hpbGQge1xyXG4gICAgLmxpc3QtaW1hZ2UtY29udGFpbmVyIHtcclxuICAgICAgbWFyZ2luOiAwO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4ubGlzdC1pbWFnZSB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gIGFwcC1tZWRpYS1jYXJkIHtcclxuICAgIHdpZHRoOiAyNSU7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbiAgICAmOmxhc3QtY2hpbGQge1xyXG4gICAgICBtYXJnaW4tcmlnaHQ6IDA7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5saXN0LWltYWdlLWNvbnRhaW5lciB7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBib3JkZXItcmFkaXVzOiA2cHg7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBwYWRkaW5nLXRvcDogMTAwJTtcclxuICAgIC5pY29uIHtcclxuICAgICAgd2lkdGg6IDMwJTtcclxuICAgICAgaGVpZ2h0OiAzMCU7XHJcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgdG9wOiA1MCU7XHJcbiAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xyXG4gICAgICBsZWZ0OiA1MCU7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG4vLyBsaXN0IHdpdGggc21hbGwgaW1hZ2UgYW5kIHNtYWxsIHRleHRcclxuXHJcbi5zbWFsbC1saXN0IHtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIG1pbi1oZWlnaHQ6IDE2cHg7XHJcbiAgbWFyZ2luLXRvcDogM3B4O1xyXG4gIHBhZGRpbmctcmlnaHQ6IDEwcHg7XHJcbiAgICAmOm50aC1jaGlsZCgyKSAge1xyXG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgJjo6YmVmb3JlIHtcclxuICAgICAgICAgIGNvbnRlbnQ6IFwiIFwiO1xyXG4gICAgICAgICAgaGVpZ2h0OiAxMXB4O1xyXG4gICAgICAgICAgd2lkdGg6IDFweDtcclxuICAgICAgICAgIGJhY2tncm91bmQ6JGNvbG9yLWdyYXktMjA7XHJcbiAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbiAgICAgICAgICBtYXJnaW4tdG9wOiAzcHg7XHJcbiAgICAgICAgICBmbGV4LXNocmluazogMDtcclxuICAgICAgICAgfVxyXG4gIH1cclxuXHJcbiAgJjpmaXJzdC1jaGlsZCB7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDA7XHJcbiAgICBtYXJnaW4tbGVmdDogMDtcclxuICAgIGJvcmRlcjogMDtcclxuICB9XHJcblxyXG4gIC5zbWFsbC1saXN0LWltYWdlIHtcclxuICAgIHdpZHRoOiAxNnB4O1xyXG4gICAgaGVpZ2h0OiAxMnB4O1xyXG4gICAgbWFyZ2luLXJpZ2h0OiA2cHg7XHJcbiAgICBvYmplY3QtZml0OiBjb3ZlcjtcclxuICAgIG1hcmdpbi10b3A6IDNweDtcclxuICB9XHJcbiAgLnR4dCB7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcclxuICAgIC13ZWJraXQtbGluZS1jbGFtcDogMTtcclxuICAgIC8qIGF1dG9wcmVmaXhlcjogaWdub3JlIG5leHQgKi9cclxuICAgIC13ZWJraXQtYm94LW9yaWVudDogdmVydGljYWw7XHJcbiAgICAvLyBtYXgtd2lkdGg6IDkxcHg7XHJcbiAgICBtYXJnaW4tdG9wOiAzcHg7XHJcbiAgfVxyXG59XHJcbmlvbi1zbGlkZSB7XHJcbiAgYmFja2dyb3VuZDogJGNvbG9yLXdoaXRlO1xyXG4gIC5zbWFsbC1saXN0LWlubGluZS1jb250YWluZXIge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICB9XHJcbn1cclxuLnNtYWxsLWxpc3QtaW5saW5lLWNvbnRhaW5lciB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICAuc21hbGwtbGlzdCB7XHJcbiAgICAmOm50aC1jaGlsZCgyKSAge1xyXG4gICAgICBtYXgtd2lkdGggOiA4MCU7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG4vLyBsaXN0IHdpdGggdGhlIGJ0biBpbWFnZVxyXG5cclxuLmxpc3Qtd2l0aC1idG4tc2Nyb2xsIHtcclxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiYSgyMTQsIDIxNCwgMjMzLCAwLjQ2KTtcclxuICBwYWRkaW5nOiAyMHB4O1xyXG4gIHBhZGRpbmctcmlnaHQ6IDA7XHJcbiAgcGFkZGluZy1ib3R0b206IDE3cHg7XHJcbiAgYmFja2dyb3VuZDogJGNvbG9yLXdoaXRlO1xyXG4gICYubm8tbGVmdC1zcGFjZSB7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDA7XHJcbiAgICAuc2VwZXJhdGUtd2l0aC1pbWFnZSAge1xyXG4gICAgICBwYWRkaW5nLWxlZnQ6IDIwcHg7XHJcbiAgICB9XHJcbiAgICAuc2Nyb2xsIHtcclxuICAgICAgYXBwLWJ1dHRvbntcclxuICAgICAgICAmOmZpcnN0LWNoaWxkIHtcclxuICAgICAgICAgIC5tYWluLWlucHV0LWNvbnRhaW5lciB7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAyMHB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgfVxyXG4gICAgfVxyXG4gICAgIC5maWx0ZXJCdXR0b24ge1xyXG4gICAgICBhcHAtYnV0dG9uIHtcclxuICAgICAgICAmOmZpcnN0LWNoaWxkIHtcclxuICAgICAgICAgIC5tYWluLWlucHV0LWNvbnRhaW5lciB7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAwcHggIWltcG9ydGFudDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9IFxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gICYuZXhwbG9yZSB7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogOXB4IDtcclxuICAgICYubXVsdGlwbGVTY3JvbGwge1xyXG4gICAgICBvdmVyZmxvdzogc2Nyb2xsO1xyXG4gICAgICBwYWRkaW5nLWJvdHRvbTogMHB4O1xyXG4gICAgfVxyXG4gIH1cclxuICAucmVtb3ZlLXNjcm9sbC1saW5lLWNvbnRhaW5lciB7XHJcbiAgICBoZWlnaHQ6IDQzcHg7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuOyBcclxuICAgIC5yZW1vdmUtc2Nyb2xsLWxpbmUge1xyXG4gICAgICBoZWlnaHQ6IDYwcHg7IC8vICBoaWRlIHRoZSBzY3JvbGwgb2YgbGlzdCBvZiBidG4gd2l0aCBpbWFnZVxyXG4gICAgfVxyXG4gIH1cclxufSBcclxuXHJcbi8vIHNpbXBsZSBsaXN0IGxpa2UgbGlzdCBpbiBjb21wYW55IGFuZCBjaXR5XHJcblxyXG4uc2ltcGxlLWxpc3Qge1xyXG4gIC5saXN0LWNvbnRhaW5lciB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIG1pbi1oZWlnaHQ6IDYwcHg7XHJcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgJGNvbG9yLWdyYXktMTM7XHJcbiAgICAmLm5vLWJvcmRlciB7XHJcbiAgICAgIGJvcmRlci1ib3R0b206IDA7XHJcbiAgICB9XHJcbiAgICAmLmhlaWdodC03NiB7XHJcbiAgICAgIHBhZGRpbmctdG9wOiA1cHg7XHJcbiAgICAgIHBhZGRpbmctYm90dG9tOiAxMHB4O1xyXG4gICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgJGNvbG9yLWdyYXktMTM7XHJcbiAgICAgIC5zdWJMYmFsZSAge1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDRweDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgJi5oZWlnaHQtNzUge1xyXG4gICAgICBoZWlnaHQ6IDc1cHg7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5saXN0LWl0ZW1zLWxlZnQge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gICAgb2JqZWN0LWZpdDogY292ZXI7XHJcblxyXG4gICAgaW1nIHtcclxuICAgICAgd2lkdGg6IDIycHg7XHJcbiAgICAgIGhlaWdodDogMjJweDtcclxuICAgICAgLy8gbWFyZ2luLXJpZ2h0OiAxNHB4O1xyXG4gICAgfVxyXG4gICAgLy8gLmF2YXRhci1jb250YWluZXIge1xyXG4gICAgLy8gICBtYXJnaW4tcmlnaHQ6IDE0cHg7XHJcbiAgICAvLyB9XHJcbiAgICAuc21hbGwtaW1hZ2Uge1xyXG4gICAgICB3aWR0aDogMTZweDtcclxuICAgIH1cclxuICB9XHJcbiAgLmxpc3QtaXRlbXMtcmlnaHQge1xyXG4gICAgaGVpZ2h0OiA2MHB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICAmLmJ1dHRvbS1ib3JkZXIge1xyXG4gICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgJGNvbG9yLWdyYXktMTM7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5yaWdodC1pY29uIHtcclxuICAgIHdpZHRoOiAyMXB4O1xyXG4gICAgaGVpZ2h0OiAyMXB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgICBmbGV4LXNocmluazogMDtcclxuICAgIHRyYW5zZm9ybTogcm90YXRlKDApO1xyXG4gICAgdHJhbnNpdGlvbi1kdXJhdGlvbjogMC4ycztcclxuICAgIGltZyB7XHJcbiAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB9XHJcbiAgICAmLnJvdGF0ZSB7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDE4MGRlZyk7XHJcbiAgICAgIHRyYW5zaXRpb24tZHVyYXRpb246IDAuMnNcclxuICAgIH1cclxuICAgICYuaGFzTm90aWZpY2F0aW9ucyB7XHJcbiAgICAgIGhlaWdodDogMTdweDtcclxuICAgICAgd2lkdGg6IDE3cHg7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1vcmFuZ2U7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgIG1hcmdpbi10b3A6IDFweDtcclxuICAgIH1cclxuICAgICYuaGFzQ291bnRlciB7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1saWdodEJsdWU7XHJcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgIHdpZHRoOiA1NnB4O1xyXG4gICAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICBib3JkZXI6IDFweCBzb2xpZCAkY29sb3ItZ3JheS0xNjtcclxuICAgICAgYm9yZGVyLXJhZGl1czogNnB4O1xyXG4gICAgICBpbWcge1xyXG4gICAgICAgIHdpZHRoOiAxNnB4O1xyXG4gICAgICAgIGhlaWdodDogMTZweDtcclxuICAgICAgfVxyXG4gICAgICAuY291bnRlciB7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDRweDtcclxuICAgICAgICBjb2xvcjogJGNvbG9yLWdyYXktMjI7XHRcclxuICAgICAgICBmb250LWZhbWlseTpcIkF2ZW5pciBOZXh0XCI7XHRcclxuICAgICAgICBmb250LXNpemU6IDE2cHg7XHRcclxuICAgICAgICBmb250LXdlaWdodDogNjAwO1x0XHJcbiAgICAgICAgbGV0dGVyLXNwYWNpbmc6IC0xcHg7XHRcclxuICAgICAgICBsaW5lLWhlaWdodDogMjJweDtcdFxyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG4uaW9zIHtcclxuICAucmlnaHQtaWNvbiB7XHJcbiAgXHJcbiAgICAmLmhhc05vdGlmaWNhdGlvbnMge1xyXG4gICAgcGFkZGluZy1sZWZ0OiAxcHg7XHJcbiAgICAgfVxyXG4gIH1cclxufVxyXG5cclxuLm1haWwtYm94IHtcclxuICAubGlzdC1pdGVtcy1sZWZ0IHtcclxuICAgIHdpZHRoOiAzMnB4O1xyXG4gICAgaGVpZ2h0OiAzMnB4O1xyXG4gICAgb2JqZWN0LWZpdDogY292ZXI7XHJcbiAgICBmbGV4LXNocmluazogMDtcclxuICAgIGltZyB7XHJcbiAgICAgIHdpZHRoOiAzMnB4O1xyXG4gICAgICBoZWlnaHQ6IDMycHg7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDZweDtcclxuICAgICAgYm9yZGVyOiAxcHggc29saWQgJGNvbG9yLWdyYXktMTM7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG4vLyBsaXN0IHdpdGggc3BlY2lhbCBkZXNpZ24gbGlrZSB0aGUgMyBpbWFnZVxyXG5cclxuLmxpc3Qtc3BlY2lhbC1jb250YWluZXIge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIHBhZGRpbmc6IDAgMjBweDtcclxuICBib3JkZXItcmFkaXVzOiA2cHg7XHJcbiAgLmxpc3Qtc3BlY2lhbC1pdGVtIHtcclxuICAgIHBhZGRpbmc6IDIwcHggMDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICRjb2xvci1ncmF5LTEzO1xyXG4gICAgLmxlZnQge1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgLmZpcnN0LWl0ZW0ge1xyXG4gICAgICB9XHJcbiAgICAgIC5zZWNvbmQtaXRlbSB7XHJcbiAgICAgICAgd2lkdGg6IDk2JTtcclxuICAgICAgICBtYXJnaW4tdG9wOiA0cHg7XHJcbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuICAgICAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcclxuICAgICAgICAtd2Via2l0LWxpbmUtY2xhbXA6IDE7XHJcbiAgICAgICAgLyogYXV0b3ByZWZpeGVyOiBpZ25vcmUgbmV4dCAqL1xyXG4gICAgICAgIC13ZWJraXQtYm94LW9yaWVudDogdmVydGljYWw7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAxM3B4O1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAudHh0IHtcclxuICAgICAgICAgIG1hcmdpbi1sZWZ0OiA1cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGltZyB7XHJcbiAgICAgICAgICB3aWR0aDogMTZweDtcclxuICAgICAgICAgIGhlaWdodDogMTZweDtcclxuICAgICAgICAgIG9iamVjdC1maXQ6IGNvdmVyO1xyXG4gICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgJGNvbG9yLXdoaXRlO1xyXG4gICAgICAgICAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gICAgICAgICAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgLnJpZ2h0IHtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gICAgICBpbWcge1xyXG4gICAgICAgIHdpZHRoOiAyNHB4O1xyXG4gICAgICAgIGhlaWdodDogMjRweDtcclxuICAgICAgICBvYmplY3QtZml0OiBjb3ZlcjtcclxuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAkY29sb3Itd2hpdGU7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbi8vIGhpZGUgdGhlIHNjcm9sbGJhciBvZiB0aGUgc2Nyb2xsIGxpc3Qgd2l0aCByaWdodCBpbWFnZVxyXG5cclxuLmhpZGUtc2Nyb2xsLWNvbnRhaW5lciB7XHJcbiAgaGVpZ2h0OiAxODRweDtcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gIC5oaWRlLXNjcm9sbCB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgaGVpZ2h0OiAxOTBweDtcclxuICAgIG92ZXJmbG93OiBzY3JvbGw7XHJcbiAgfVxyXG59XHJcblxyXG4vLyBzcGFjZSBmb3IgcmVjdXJzaXZlIHRyZWVcclxuXHJcbi50cmVlU3BhY2Uge1xyXG4gIHBhZGRpbmctbGVmdDogMjBweDtcclxufVxyXG5cclxuLy8gdGhlIGZpbHRlciBjb3VudCBkZXNpZ25cclxuLmZpbHRlci1jb3VudCB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBtaW4td2lkdGg6IDQycHg7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBwYWRkaW5nLXJpZ2h0OiA4cHg7XHJcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG4gIGhlaWdodDogMjBweDtcclxuICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCAkY29sb3ItZ3JheS0xMTtcclxuICBjb2xvcjogJGNvbG9yLWdyZWVuO1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxuICBpbWcge1xyXG4gICAgd2lkdGg6IDE2cHg7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDhweDtcclxuICB9XHJcbn1cclxuLmZpbHRlci1oZWxwIHtcclxuICB3aWR0aDogNDVweDtcclxuICBmbGV4LXNocmluazogMDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgbWFyZ2luLWxlZnQ6IDVweDtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgaGVpZ2h0OiAzNXB4O1xyXG59XHJcbi5saXN0LWJveCB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBvdmVyZmxvdzogc2Nyb2xsO1xyXG4gIHBhZGRpbmc6IDAgMjBweDtcclxuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gIC5jYXJkLWJveC1jb250YWluZXIge1xyXG4gICAgaGVpZ2h0OiAyMTFweDtcclxuICAgIHdpZHRoOiAxNTJweDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICRjb2xvci1ncmF5LTEzO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxuICAgIC5jYXJkLWJveC1pdGVtIHtcclxuICAgICAgLmZpcnN0LWl0ZW0ge1xyXG4gICAgICAgIGhlaWdodDogMTMycHg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogOHB4IDhweCAwIDA7XHJcbiAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgICAgICBpbWcge1xyXG4gICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4IDEwcHggMCAwO1xyXG4gICAgICAgICAgb2JqZWN0LWZpdDogY292ZXI7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIC5zZWNvbmQtaXRlbSB7XHJcbiAgICAgICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICRjb2xvci1ncmF5LTEzO1xyXG4gICAgICAgIHBhZGRpbmc6IDEwcHggMTNweCAxM3B4IDEzcHg7XHJcbiAgICAgICAgaGVpZ2h0OiA3OHB4O1xyXG4gICAgICAgIHNwYW4ge1xyXG4gICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgICAgICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XHJcbiAgICAgICAgICAtd2Via2l0LWxpbmUtY2xhbXA6IDI7XHJcbiAgICAgICAgICAvKiBhdXRvcHJlZml4ZXI6IGlnbm9yZSBuZXh0ICovXHJcbiAgICAgICAgICAtd2Via2l0LWJveC1vcmllbnQ6IHZlcnRpY2FsO1xyXG4gICAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLmxvY2F0aW9uIHtcclxuICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuICAgICAgICAgIGRpc3BsYXk6IC13ZWJraXQtYm94O1xyXG4gICAgICAgICAgLXdlYmtpdC1saW5lLWNsYW1wOiAxO1xyXG4gICAgICAgICAgLyogYXV0b3ByZWZpeGVyOiBpZ25vcmUgbmV4dCAqL1xyXG4gICAgICAgICAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcclxuICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAuc3VidGl0bGUge1xyXG4gICAgICBtYXJnaW4tdG9wOiA2cHg7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgIGltZyB7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiA2cHg7XHJcbiAgICAgICAgaGVpZ2h0OiAxMnB4O1xyXG4gICAgICAgIHdpZHRoOiAxMnB4O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIGFwcC1wcm9kdWN0LWNhcmQge1xyXG4gICAgJjpsYXN0LWNoaWxkIHtcclxuICAgICAgLmNhcmQtYm94LWNvbnRhaW5lciB7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbi5saXN0LWJveC13cmFwIHtcclxuICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC13cmFwOiB3cmFwO1xyXG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAkY29sb3ItZ3JheS0xMztcclxuICBhcHAtcHJvZHVjdC1jYXJkIHtcclxuICAgIHdpZHRoOiA1MCU7XHJcbiAgICBmbGV4LXNocmluazogMDtcclxuICAgICY6bnRoLWNoaWxkKG9kZCkge1xyXG4gICAgICAuY2FyZC1ib3gtY29udGFpbmVyIHtcclxuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAkY29sb3ItZ3JheS0xMztcclxuICAgICAgICBib3JkZXItdG9wOiAwO1xyXG4gICAgICAgIGJvcmRlci1sZWZ0OiAwO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAmOm50aC1jaGlsZChldmVuKSB7XHJcbiAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAkY29sb3ItZ3JheS0xMztcclxuICAgICAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCB0cmFuc3BhcmVudDtcclxuICAgIH1cclxuICB9XHJcbiAgLmZpcnN0LWl0ZW0ge1xyXG4gICAgaGVpZ2h0OiAxNjBweDtcclxuICAgIHBhZGRpbmctdG9wOiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcclxuICB9XHJcbiAgLnNlY29uZC1pdGVtIHtcclxuICAgIHBhZGRpbmc6IDEwcHggMjFweDtcclxuICAgIGhlaWdodDogMTA3cHg7XHJcbiAgICBzcGFuIHtcclxuICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICAgIGRpc3BsYXk6IC13ZWJraXQtYm94O1xyXG4gICAgICAtd2Via2l0LWxpbmUtY2xhbXA6IDM7XHJcbiAgICAgIC8qIGF1dG9wcmVmaXhlcjogaWdub3JlIG5leHQgKi9cclxuICAgICAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcclxuICAgIH1cclxuICAgIC5zdWJ0aXRsZSB7XHJcbiAgICAgIG1hcmdpbi10b3A6IDZweDtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgaW1nIHtcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDZweDtcclxuICAgICAgICBoZWlnaHQ6IDEycHg7XHJcbiAgICAgICAgd2lkdGg6IDEycHg7XHJcbiAgICAgIH1cclxuICAgICAgLmxvY2F0aW9uIHtcclxuICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgICAgIGRpc3BsYXk6IC13ZWJraXQtYm94O1xyXG4gICAgICAgIC13ZWJraXQtbGluZS1jbGFtcDogMTtcclxuICAgICAgICAvKiBhdXRvcHJlZml4ZXI6IGlnbm9yZSBuZXh0ICovXHJcbiAgICAgICAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuLy9jb21wbGV4IExpc3QgVHlwZVxyXG5cclxuLmNvbXBsZXhUeXBlIHtcclxuICBwYWRkaW5nLXRvcDogMjBweDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIC5sZWZ0LWl0ZW0ge1xyXG4gICAgaGVpZ2h0OiA1NHB4O1xyXG4gICAgd2lkdGg6IDU0cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gICAgZmxleC1zaHJpbms6IDA7XHJcbiAgICBpbWcge1xyXG4gICAgICBoZWlnaHQ6IDU0cHg7XHJcbiAgICAgIHdpZHRoOiA1NHB4O1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gICAgICBvYmplY3QtZml0OiBjb3ZlcjtcclxuICAgIH1cclxuICB9XHJcbiAgLmNvbnRhaW5lciB7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMjBweDtcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAkY29sb3ItZ3JheS0xNztcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAubWlkZGxlLWl0ZW0ge1xyXG4gICAgICBtYXJnaW4tbGVmdDogMTBweDtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgIHBhZGRpbmctcmlnaHQ6IDhweDtcclxuICAgICAgLmZpcnN0LWl0ZW0ge1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAuZGV0YWlscyB7XHJcbiAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICAgICAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcclxuICAgICAgICAgIC13ZWJraXQtbGluZS1jbGFtcDogMTtcclxuICAgICAgICAgIC8qIGF1dG9wcmVmaXhlcjogaWdub3JlIG5leHQgKi9cclxuICAgICAgICAgIC13ZWJraXQtYm94LW9yaWVudDogdmVydGljYWw7XHJcbiAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuc3ViRGV0YWlscyB7XHJcbiAgICAgICAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgICAgICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICAuc2Vjb25kLWl0ZW0ge1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDJweDtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgLmRldGFpbHMge1xyXG4gICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgICAgICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XHJcbiAgICAgICAgICAtd2Via2l0LWxpbmUtY2xhbXA6IDE7XHJcbiAgICAgICAgICAvKiBhdXRvcHJlZml4ZXI6IGlnbm9yZSBuZXh0ICovXHJcbiAgICAgICAgICAtd2Via2l0LWJveC1vcmllbnQ6IHZlcnRpY2FsO1xyXG4gICAgICAgICAgcGFkZGluZy1yaWdodDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLmJhZGdlIHtcclxuICAgICAgICAgIGhlaWdodDogMThweDtcclxuICAgICAgICAgIHdpZHRoOiAxOHB4O1xyXG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLW9yYW5nZTtcclxuICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICAgICAgICBmbGV4LXNocmluazogMDtcclxuICAgICAgICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gICAgICAgICAgZm9udC1zaXplOiAxMXB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAucmlnaHQtaXRlbSB7XHJcbiAgICAgIHdpZHRoOiAyMXB4O1xyXG4gICAgICBoZWlnaHQ6IDIxcHg7XHJcbiAgICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gICAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgICAgaW1nIHtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgJi5zbWFsbCB7XHJcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgJGNvbG9yLWdyYXktMTc7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgcGFkZGluZzogMjBweCAwO1xyXG4gICAgLmxlZnQtaXRlbSB7XHJcbiAgICAgIHdpZHRoOiAzMnB4O1xyXG4gICAgICBoZWlnaHQ6IDMycHg7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDBweDtcclxuICAgICAgaW1nIHtcclxuICAgICAgICB3aWR0aDogMzJweDtcclxuICAgICAgICBoZWlnaHQ6IDMycHg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMHB4O1xyXG4gICAgICAgIG9iamVjdC1maXQ6IG5vbmU7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIC5jb250YWluZXIge1xyXG4gICAgICBib3JkZXI6IDA7XHJcbiAgICAgIHBhZGRpbmctYm90dG9tOiAwO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4uY2VudGVyLW1pZGRsZS1pdGVtIHtcclxuICAuY29udGFpbmVyIHtcclxuICAgIC5taWRkbGUtaXRlbSB7XHJcbiAgICAgIG1hcmdpbi10b3A6IDVweDtcclxuICAgIH1cclxuICB9XHJcbi5yaWdodC1pdGVtIHtcclxuICBtYXJnaW4tdG9wOiAxMHB4O1xyXG59XHJcbn1cclxuLmNvbXBsZXhUcmVlVHlwZSB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIC5zaW1wbGUtbGlzdCB7XHJcbiAgICAubGlzdC1jb250YWluZXIge1xyXG4gICAgICBwYWRkaW5nOiAxMHB4IDE2cHg7XHJcbiAgICAgIGJvcmRlcjogMXB4IHNvbGlkICRjb2xvci1ncmF5LTEzO1xyXG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDQwcHg7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDZweDtcclxuICAgICAgJjo6YWZ0ZXIge1xyXG4gICAgICAgIGNvbnRlbnQ6IFwiIFwiO1xyXG4gICAgICAgIGJvcmRlci1yaWdodDogMnB4IGRhc2hlZCAkY29sb3ItZ3JheS0xODtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiA0MHB4O1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICB0b3A6IDEwMCU7XHJcbiAgICAgICAgbGVmdDogMzZweDtcclxuICAgICAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICAgICAgYW5pbWF0aW9uLW5hbWU6IGZhZGVkO1xyXG4gICAgICAgIGFuaW1hdGlvbi1kdXJhdGlvbjogMnM7XHJcbiAgICAgIH1cclxuICAgICAgJi5hY3RpdmUge1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1ncmF5LTE2O1xyXG4gICAgICB9XHJcbiAgICAgIC5saXN0LWl0ZW1zLXJpZ2h0IHtcclxuICAgICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICAgIH1cclxuICAgICAgLmxpc3QtaXRlbXMtbGVmdCB7XHJcbiAgICAgICAgaW1nIHtcclxuICAgICAgICAgIHdpZHRoOiA0MXB4O1xyXG4gICAgICAgICAgb2JqZWN0LWZpdDogY292ZXI7XHJcbiAgICAgICAgICBoZWlnaHQ6IDQxcHg7XHJcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgJGNvbG9yLWdyYXktMTM7XHJcbiAgICAgICAgICBvYmplY3QtZml0OiBjb3ZlcjtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgLmxvY2F0aW9uIHtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgaW1nIHtcclxuICAgICAgICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgICAgICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICAmLm5vLWJvcmRlciB7XHJcbiAgICAgICAgJjo6YWZ0ZXIge1xyXG4gICAgICAgICAgICAgICAgYm9yZGVyLXJpZ2h0OiAwICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIC50cmVlQ291bnRlckNvbnRhaW5lciB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAudHJlZUNvdW50TnVtYmVyIHtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICB6LWluZGV4OiAxMDtcclxuICAgICAgZm9udC1zaXplOiAxMXB4O1xyXG4gICAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICAgIGhlaWdodDogMTZweDtcclxuICAgICAgd2lkdGg6IDE2cHg7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICBjb2xvcjokY29sb3Itd2hpdGU7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbkBrZXlmcmFtZXMgZmFkZWQge1xyXG4gIDAlIHtcclxuICAgIGhlaWdodDogMHB4O1xyXG4gIH1cclxuICAxMDAlIHtcclxuICAgIGhlaWdodDogNDBweDtcclxuICB9XHJcbn1cclxuLmlvcyB7XHJcbiAgLnNlY29uZC1pdGVtIHsgXHJcbiAgICAuYmFkZ2Uge1xyXG4gICAgICBwYWRkaW5nLWxlZnQ6IDFweDtcclxuICAgICAgcGFkZGluZy10b3A6IDJweDtcclxuICAgIH1cclxuICB9XHJcbn1cclxuLmlvcyB7XHJcbiAgLmNvbXBsZXhUeXBlIHtcclxuICAgIC5zZWNvbmQtaXRlbSB7IFxyXG4gICAgICAuYmFkZ2Uge1xyXG4gICAgICAgIHBhZGRpbmctbGVmdDogMXB4O1xyXG4gICAgICAgIHBhZGRpbmctdG9wOiAwcHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuLmNhcmQtdHlwZS1maXZlIHtcclxuICBib3JkZXI6IDFweCBzb2xpZCAkY29sb3ItZ3JheS05O1xyXG4gIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICAubGlzdCB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3ItbGlnaHRCbHVlO1xyXG4gICAgcGFkZGluZzogMTZweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDhweCA4cHggMCAwO1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICRjb2xvci1ncmF5LTk7XHJcbiAgICAuZmlyc3Qge1xyXG4gICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICBpbWcge1xyXG4gICAgICAgIGhlaWdodDogNDhweDtcclxuICAgICAgICB3aWR0aDogNDhweDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gICAgICB9XHJcbiAgICAgIHNwYW4ge1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxMnB4O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgICAgXHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgLmFjdGlvbiB7XHJcbiAgICBwYWRkaW5nOiAxNnB4IDA7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgLmFjdGlvbi1pdGVtIHtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgd2lkdGg6IDMzLjMzJTtcclxuICAgICAgaW1nIHtcclxuICAgICAgICBoZWlnaHQ6IDIwcHg7XHJcbiAgICAgICAgd2lkdGg6IDIwcHg7XHJcbiAgICAgIH1cclxuICAgICAgc3BhbiB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogN3B4O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbi5hY3RpdmUtaXRlbS10cmVlIHtcclxuICAubGlzdC1jb250YWluZXIge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjokY29sb3ItbGlnaHRCbHVlIDtcclxuICB9XHJcbn1cclxuXHJcblxyXG4vL2V2ZW50IFxyXG4uZXZlbnQge1xyXG4gIC5kYXRlLWNvbnRhaW5lciB7XHJcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gICAgLnRleHQge1xyXG4gICAgICBtYXJnaW4tbGVmdDogMTBweDtcclxuICAgIH1cclxuICAgIC52LWxpbmUtY29udGFpbmVyIHtcclxuICAgICAgaGVpZ2h0OiAyNHB4O1xyXG4gICAgICB3aWR0aDogMjBweDtcclxuICAgICAgcGFkZGluZzogNnB4IDA7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAudi1saW5lIHtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgd2lkdGg6IDFweDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAkY29sb3ItZ3JheS0xMztcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuICAubG9jYXRpb24tY29udGFpbmVyIHtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICRjb2xvci1ncmF5LTEzO1xyXG4gICAgcGFkZGluZzogMjBweCAwO1xyXG4gICAgYm9yZGVyLXJpZ2h0OiAwO1xyXG4gICAgYm9yZGVyLWxlZnQ6IDA7XHJcbiAgICBib3JkZXItYm90dG9tOiAwO1xyXG4gICAgbWFyZ2luLXRvcDogMjBweDtcclxuICAgIC50ZXh0IHtcclxuICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbiAgICB9XHJcbiAgfVxyXG4gIFxyXG59XHJcbiIsIi5uYXZCYXIge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogNThweDtcclxuICAubmF2QmFyLWNvbnRhaW5lciB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICRjb2xvci1ncmF5LTE2O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLXdoaXRlO1xyXG4gICAgJi5ibGFjayB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1ibGFjaztcclxuICAgICAgYm9yZGVyOiAwO1xyXG4gICAgICAubWlkZGxlIHtcclxuICAgICAgICBjb2xvcjogJGNvbG9yLXdoaXRlO1xyXG4gICAgICAgIC5wYWdlcy10b3AtYmFyLXRpdGxlIHtcclxuICAgICAgICAgIGNvbG9yOiAkY29sb3Itd2hpdGUgIWltcG9ydGFudDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgICYubm8tYm9yZGVyIHtcclxuICAgICAgYm9yZGVyOiAwO1xyXG4gICAgfVxyXG4gICAgaW1nIHtcclxuICAgICAgd2lkdGg6IDI0cHg7XHJcbiAgICAgIGhlaWdodDogMjRweDtcclxuICAgICAgbWFyZ2luLWxlZnQ6IDFweDtcclxuICAgIH1cclxuICAgIC5taWRkbGUtaWNvbiB7XHJcbiAgICAgIHdpZHRoOiAxNnB4O1xyXG4gICAgICBmbGV4LXNocmluazogMDtcclxuICAgICAgbWFyZ2luLXRvcDogMXB4O1xyXG4gICAgfVxyXG4gICAgLmhhc0NvdW50TWVzc2FnZSB7XHJcbiAgICAgIGhlaWdodDogN3B4O1xyXG4gICAgICB3aWR0aDogN3B4O1xyXG4gICAgICBtYXJnaW4tbGVmdDogMnB4O1xyXG4gICAgICBiYWNrZ3JvdW5kOiAkY29sb3Itb3JhbmdlO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gICAgICBtYXJnaW4tbGVmdDogM3B4O1xyXG4gICAgICBtYXJnaW4tdG9wOiAycHg7XHJcbiAgICB9XHJcbiAgICAuaXRlbSB7XHJcbiAgICAgIHdpZHRoOiA1NnB4O1xyXG4gICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgJi5yaWdodC10eHQge1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogZW5kO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAubWlkZGxlIHtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgIG1heC13aWR0aDogY2FsYygxMDAlIC0gMTEycHgpO1xyXG4gICAgICAmLmNvbHVtbiB7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgfVxyXG4gICAgICAudHh0IHtcclxuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAmLmZ1bGxOYXZCYXIge1xyXG4gICAgICAubWlkZGxlIHtcclxuICAgICAgICBtYXgtd2lkdGg6IGNhbGMoMTAwJSAtIDEwcHgpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5jb21wbGV4LXR5cGUge1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgLm1pZGRsZSB7XHJcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3cgIWltcG9ydGFudDtcclxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xyXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgICBwYWRkaW5nLXJpZ2h0OiAxMXB4O1xyXG4gICAgICAudGV4dCB7XHJcbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuICAgICAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcclxuICAgICAgICAtd2Via2l0LWxpbmUtY2xhbXA6IDE7XHJcbiAgICAgICAgLyogYXV0b3ByZWZpeGVyOiBpZ25vcmUgbmV4dCAqL1xyXG4gICAgICAgIC13ZWJraXQtYm94LW9yaWVudDogdmVydGljYWw7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIC5pbWFnZS1iYXIge1xyXG4gICAgICBoZWlnaHQ6IDMycHg7XHJcbiAgICAgIHdpZHRoOiAzMnB4O1xyXG4gICAgICBib3JkZXItcmFkaXVzOiA2cHg7XHJcbiAgICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxuICAgICAgZmxleC1zaHJpbms6IDA7XHJcbiAgICAgIGltZyB7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDZweDtcclxuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAkY29sb3ItZ3JheS0xNztcclxuICAgICAgICBvYmplY3QtZml0OiBjb3ZlcjtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgLm1pZGRsZS1iYXItZGV0YWlscyB7XHJcbiAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAubWlkZGxlLWJhci1kZXRhaWxzLXRleHQge1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5idG4ge1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgfVxyXG59XHJcblxyXG4uaW5wdXQtbmF2YmFyIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgcGFkZGluZzogMCAyMHB4O1xyXG4gIHBhZGRpbmctYm90dG9tOiAyMHB4O1xyXG4gIHBhZGRpbmctdG9wOiAyMHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci13aGl0ZTtcclxuICAmLm5vLXRvcC1zcGFjZSB7XHJcbiAgICBtYXJnaW4tdG9wOiAwO1xyXG4gICAgcGFkZGluZy10b3A6IDAgIWltcG9ydGFudDtcclxuICB9XHJcbiAgJi53aXRob3V0LXNlcGVyYXRvciB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG5cclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAvLyBwYWRkaW5nLXRvcDogOHB4O1xyXG4gICAgLmlucHV0IHtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgIGJvcmRlcjogMDtcclxuICAgICAgYmFja2dyb3VuZC1zaXplOiAxNnB4O1xyXG4gICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiAxNnB4O1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAxOHB4O1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAgJGNvbG9yLWdyYXktMTc7XHJcbiAgICAgIGhlaWdodDogMzRweDtcclxuICAgICAgJi5zZWFyY2gge1xyXG4gICAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi9hc3NldHMvaWNvbnMvZGlhc3BvcmFJY29uL1NlYXJjaF9HcmV5LnN2Z1wiKTtcclxuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IDIwcHg7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiA1MHB4O1xyXG4gICAgICAgICY6OnBsYWNlaG9sZGVyIHtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICAgIGNvbG9yOiAkY29sb3ItZ3JheS0xNTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIGFwcC1pbnB1dCB7XHJcbiAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLmlucHV0V2l0aEljb24ge1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgIH1cclxuXHJcbiAgICAmLndpdGgtaWNvbiB7XHJcbiAgICAgIC5pY29uLWNvbnRhaW5lciB7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAxNnB4O1xyXG4gICAgICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gICAgICAgIGltZyB7XHJcbiAgICAgICAgICB3aWR0aDogMjRweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgJi5yaWdodCB7XHJcbiAgICAgICAgICBtYXJnaW4tbGVmdDogMThweDtcclxuICAgICAgICAgIG1hcmdpbi1yaWdodDogMDtcclxuICAgICAgICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gICAgICAgICAgaGVpZ2h0OiAyNHB4O1xyXG4gICAgICAgICAgbWFyZ2luLXRvcDogMnB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAmLmxlZnQge1xyXG4gICAgICAgICAgbWFyZ2luLWxlZnQ6IDIwcHg7XHJcbiAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDA7XHJcbiAgICAgICAgICBtYXJnaW4tdG9wOiAycHg7XHJcbiAgICAgICAgICB0b3A6IDJweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gJjo6YmVmb3JlIHtcclxuICAgICAgICAvLyAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICAgICAgLy8gICBoZWlnaHQ6IDI0cHg7XHJcbiAgICAgICAgLy8gICB3aWR0aDogMjRweDtcclxuICAgICAgICAvLyAgIG9wYWNpdHk6IDA7XHJcbiAgICAgIC8vICAgYmFja2dyb3VuZDogcmdiYSgyMDIsIDE5NiwgMTk2LCAwLjUpO1xyXG4gICAgICAgIC8vICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgICAgIC8vICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIC8vIH1cclxuICAgICAgICAvLyAmOmFjdGl2ZSB7XHJcbiAgICAgICAgLy8gICAmOjpiZWZvcmUge1xyXG4gICAgICAgIC8vICAgICB0cmFuc2Zvcm06IHNjYWxlKDIpO1xyXG4gICAgICAgIC8vICAgICB0cmFuc2l0aW9uOiBhbGwgMC4xcztcclxuICAgICAgICAvLyAgICAgb3BhY2l0eTogMTtcclxuICAgICAgICAvLyAgIH1cclxuICAgICAgICAvLyB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgLmlucHV0IHtcclxuICAgIGhlaWdodDogNDJweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgcGFkZGluZzogMCAxNnB4O1xyXG4gICAgcGFkZGluZy1sZWZ0OiAzOHB4O1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgyMTQsIDIxNCwgMjMzLCAwLjQ2KTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1ncmF5LTE4O1xyXG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuMnB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogMTZweDtcclxuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiAxNnB4O1xyXG4gICAgJi5zZWFyY2gge1xyXG4gICAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIvYXNzZXRzL2ljb25zL2RpYXNwb3JhL1NlYXJjaF9HcmV5LnN2Z1wiKTtcclxuICAgIH1cclxuICB9XHJcbiAgOjpwbGFjZWhvbGRlciB7XHJcbiAgICBjb2xvcjogJGNvbG9yLWdyYXktMjI7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICBsZXR0ZXItc3BhY2luZzogMC4ycHg7XHJcbiAgICBmb250LXdlaWdodDogMzAwO1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XHJcbiAgICAtd2Via2l0LWxpbmUtY2xhbXA6IDE7XHJcbiAgICAvKiBhdXRvcHJlZml4ZXI6IGlnbm9yZSBuZXh0ICovXHJcbiAgICAtd2Via2l0LWJveC1vcmllbnQ6IHZlcnRpY2FsO1xyXG4gIH1cclxuICAvLyAuaWNvbiB7XHJcbiAgLy8gICB3aWR0aDogMTZweDtcclxuICAvLyAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAvLyAgIHRvcDogMTJweDtcclxuICAvLyAgIGxlZnQ6IDI4cHg7XHJcbiAgLy8gfVxyXG59XHJcbmlucHV0W2Rpc2FibGVkXSB7XHJcbiAgYmFja2dyb3VuZDogJGNvbG9yLWxpZ2h0Qmx1ZTtcclxuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiA4cHggOHB4O1xyXG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgYmFja2dyb3VuZC1zaXplOiAxNnB4O1xyXG4gIG9wYWNpdHk6IDEgIWltcG9ydGFudDtcclxufVxyXG4uaW9zIHtcclxuICAuaW5wdXQge1xyXG4gICAgd29yZC1icmVhazogYnJlYWstYWxsICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG4gIC5pbnB1dC1uYXZiYXIge1xyXG4gICAuaW5wdXQge1xyXG4gICAgIHBhZGRpbmctYm90dG9tOiAycHg7XHJcbiAgIH1cclxuICB9XHJcbn1cclxuIiwiaW5wdXQsIHRleHRhcmVhIHtcclxuICBjYXJldC1jb2xvcjogJGNvbG9yLWdyZWVuO1xyXG59XHJcblxyXG5hcHAtbGlzdC1jYXJkLXdpdGgtZGF0ZSB7XHJcbiAgJjpsYXN0LWNoaWxkIHtcclxuICAgIC5jYXJkLWNvbnRhaW5lciB7XHJcbiAgICAgIG1hcmdpbi1yaWdodDogMjBweDtcclxuICAgIH1cclxuICB9XHJcbn1cclxuLmNhcmQtY29udGFpbmVyLXdpdGgtYm9yZGVyIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgYm9yZGVyLXRvcDogMjhweCBzb2xpZCAkY29sb3Itd2hpdGUtZ3JheTtcclxuICBwYWRkaW5nLXRvcDogMTBweDtcclxuICBib3JkZXItYm90dG9tOiAyOHB4IHNvbGlkICRjb2xvci13aGl0ZS1ncmF5O1xyXG4gIGJhY2tncm91bmQ6ICRjb2xvci13aGl0ZTtcclxuICAmOjphZnRlciB7XHJcbiAgICBjb250ZW50OiBcIiBcIjtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGhlaWdodDogMTdweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZDogJGNvbG9yLXdoaXRlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMCAwIDEzcHggMTNweDtcclxuICAgIGJvdHRvbTogLTE2cHg7XHJcbiAgICBib3gtc2hhZG93OiAwIDNweCAzcHggMCByZ2JhKDAsIDAsIDAsIDAuMDUpO1xyXG4gICAgbGVmdDogMDtcclxuICB9XHJcbiAgJjo6YmVmb3JlIHtcclxuICAgIGNvbnRlbnQ6IFwiIFwiO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgaGVpZ2h0OiAxN3B4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEzcHggMTNweCAwIDA7XHJcbiAgICB0b3A6IC0xNnB4O1xyXG4gICAgbGVmdDogMDtcclxuICB9XHJcbiAgJi5uby1ib3JkZXItdG9wLXJhZGl1cyB7XHJcbiAgICAmOjpiZWZvcmUge1xyXG4gICAgICBjb250ZW50OiBub25lO1xyXG4gICAgfVxyXG4gICAgYm9yZGVyLXRvcDogMDtcclxuICAgIHBhZGRpbmctdG9wOiAwO1xyXG4gIH1cclxuICAudmVydGljYWwtbGluZSB7XHJcbiAgICBoZWlnaHQ6IDE3cHg7XHJcbiAgICB3aWR0aDogMXB4O1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYmFja2dyb3VuZDogJGNvbG9yLWdyYXktMTM7XHJcbiAgICBsZWZ0OiA1MCU7XHJcbiAgICB6LWluZGV4OiAxMDtcclxuICAgIG1hcmdpbi10b3A6IC0xcHg7XHJcbiAgfVxyXG59XHJcbi5jYXJkLXByb2ZpbGUtY29udGFpbmVyIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBwYWRkaW5nOiAxNnB4IDA7XHJcbiAgbWFyZ2luLXRvcDogMTAwcHg7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgJGNvbG9yLWdyZWVuLTI7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLWdyZWVuLTM7XHJcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgLmNhcmQtcHJvZmlsZS1pdGVtIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBtYXgtd2lkdGg6IDE3OXB4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBwYWRkaW5nOiAwIDE2cHg7XHJcbiAgICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCAkY29sb3ItZ3JlZW4tMjtcclxuICAgIGJveC1zaGFkb3c6IDRweCA0cHggNHB4IC04cHggcmdiYSg2OSwgMTM5LCAyMTksIDAuMTkpO1xyXG4gICAgaW1nIHtcclxuICAgICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgICB3aWR0aDogNTBweDtcclxuICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgfVxyXG4gICAgLmZpcnN0LXR4dCB7XHJcbiAgICAgIG1hcmdpbi10b3A6IDEycHg7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICAgIGRpc3BsYXk6IC13ZWJraXQtYm94O1xyXG4gICAgICAtd2Via2l0LWxpbmUtY2xhbXA6IDE7XHJcbiAgICAgIC8qIGF1dG9wcmVmaXhlcjogaWdub3JlIG5leHQgKi9cclxuICAgICAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcclxuICAgIH1cclxuICAgIC5zZWNvbmQtdHh0IHtcclxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICBtYXJnaW4tdG9wOiA0cHg7XHJcbiAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcclxuICAgICAgLXdlYmtpdC1saW5lLWNsYW1wOiAyO1xyXG4gICAgICAvKiBhdXRvcHJlZml4ZXI6IGlnbm9yZSBuZXh0ICovXHJcbiAgICAgIC13ZWJraXQtYm94LW9yaWVudDogdmVydGljYWw7XHJcbiAgICB9XHJcbiAgICAudGhpcmQtdHh0IHtcclxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICBtYXJnaW4tdG9wOiAxNHB4O1xyXG4gICAgfVxyXG4gIH1cclxuICAubGluZS1taWRkbGUge1xyXG4gICAgd2lkdGg6IDJweDtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1ncmVlbi0yO1xyXG4gIH1cclxufVxyXG5cclxuLmNhcmQtb25lIHtcclxuICBoZWlnaHQ6IDIyM3B4O1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGJvcmRlci1yYWRpdXM6IDZweDtcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gIC5jYXJkLWNvbnRhaW5lciB7XHJcbiAgICBib3JkZXItcmFkaXVzOiA2cHg7XHJcbiAgICB3aWR0aDogMjcycHg7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDE2cHg7XHJcbiAgICBoZWlnaHQ6IDI1NXB4O1xyXG4gICAgLnRvcCB7XHJcbiAgICAgIHdpZHRoOiAyNzJweDtcclxuICAgICAgaGVpZ2h0OiAxMzdweDtcclxuICAgICAgYm9yZGVyOiAxcHggc29saWQgJGNvbG9yLWdyYXktMTk7XHJcbiAgICAgIGJvcmRlci1ib3R0b206IDA7XHJcbiAgICAgIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiA2cHg7XHJcbiAgICAgIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDZweDtcclxuICAgICAgYmFja2dyb3VuZDogJGNvbG9yLWdyYXktMTc7XHJcbiAgICAgIGltZyB7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDZweDtcclxuICAgICAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogNnB4O1xyXG4gICAgICAgIG9iamVjdC1maXQ6IGNvdmVyO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAuYm90dG9tLWNvbnRhaW5lciB7XHJcbiAgICAgIG1pbi1oZWlnaHQ6IDc3cHg7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgIGJvcmRlcjogMXB4IHNvbGlkICRjb2xvci1ncmF5LTE5O1xyXG4gICAgICBib3JkZXItdG9wOiAwO1xyXG4gICAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogNnB4O1xyXG4gICAgICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiA2cHg7XHJcbiAgICAgIGJveC1zaGFkb3c6IDAgMCAycHggMCByZ2JhKDk2LCA5NiwgOTYsIDAuMDEpO1xyXG4gICAgICBwYWRkaW5nOiAxMXB4IDA7XHJcbiAgICAgIC5ib3R0b20ge1xyXG4gICAgICAgIHBhZGRpbmc6IDAgMTFweDtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIC5sZWZ0IHtcclxuICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gICAgICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgICAgICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG4gICAgICAgICAgd2lkdGg6IDQzcHg7XHJcbiAgICAgICAgICAucGFnZXMtZXZlbnRzLWRhdGUge1xyXG4gICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgLmdyZWVuIHtcclxuICAgICAgICAgICAgY29sb3I6ICRjb2xvci1ncmVlbjtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgLm1pZGRsZSB7XHJcbiAgICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgICAgICAgLnRpdGxlIHtcclxuICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IC13ZWJraXQtYm94O1xyXG4gICAgICAgICAgICAtd2Via2l0LWxpbmUtY2xhbXA6IDE7XHJcbiAgICAgICAgICAgIC8qIGF1dG9wcmVmaXhlcjogaWdub3JlIG5leHQgKi9cclxuICAgICAgICAgICAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIC5zdWJ0aXRsZSB7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDVweDtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuLmNhcmQtdHdvIHtcclxuICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gIHBhZGRpbmctYm90dG9tOiAyMHB4O1xyXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAkY29sb3ItZ3JheS0xMztcclxuICAuY2FyZC1zdWJ0aXRsZSB7XHJcbiAgICBtYXJnaW4tbGVmdDogOHB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgfVxyXG4gIC5jYXJkLWltYWdlIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgcGFkZGluZy10b3A6IDEwMCU7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyMXB4O1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAkY29sb3ItZ3JheS0xMztcclxuICAgIG1heC1oZWlnaHQ6IDUwMHB4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgLmNhcmQtYm9keS1kZXRhaWxzIHtcclxuICAgICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICAgIHBhZGRpbmc6IDEycHggMTZweDtcclxuICAgICAgYm9yZGVyLXJhZGl1czogMTlweDtcclxuICAgICAgYm94LXNoYWRvdzogMCAycHggNHB4IDAgcmdiYSgwLCAwLCAwLCAwLjA3KTtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgYm90dG9tOiAwO1xyXG4gICAgfVxyXG4gICAgJi5zbWFsbC1pbWFnZS1hc3BlY3QtcmF0aW8ge1xyXG4gICAgICBwYWRkaW5nLXRvcDogNTYuMjUlO1xyXG4gICAgfVxyXG4gIH1cclxuICAuc3RhcnQtaXRlbSB7XHJcbiAgICBtYXJnaW4tdG9wOiAxM3B4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIC8vIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICAvLyBkaXNwbGF5OiAtd2Via2l0LWJveDtcclxuICAgIC13ZWJraXQtbGluZS1jbGFtcDogMztcclxuICAgIC8qIGF1dG9wcmVmaXhlcjogaWdub3JlIG5leHQgKi9cclxuICAgIC13ZWJraXQtYm94LW9yaWVudDogdmVydGljYWw7XHJcbiAgfVxyXG4gIC5taWRkbGUtaXRlbSB7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcclxuICAgIC13ZWJraXQtbGluZS1jbGFtcDogMjtcclxuICAgIC8qIGF1dG9wcmVmaXhlcjogaWdub3JlIG5leHQgKi9cclxuICAgIC13ZWJraXQtYm94LW9yaWVudDogdmVydGljYWw7XHJcbiAgICBtYXJnaW4tdG9wOiA2cHg7XHJcbiAgfVxyXG4gIC5lbmQtaXRlbSB7XHJcbiAgICBtYXJnaW4tdG9wOiA2cHg7XHJcbiAgfVxyXG4gIC5pbWFnZS1pdGVtIHtcclxuICAgIGhlaWdodDogMjBweDtcclxuICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gICAgb2JqZWN0LWZpdDogY292ZXI7XHJcbiAgfVxyXG59XHJcbi5zZXBlcmF0ZS1idWxsZXQge1xyXG4gIG1hcmdpbi1sZWZ0OiA2cHg7XHJcbiAgbWFyZ2luLXJpZ2h0OiA2cHg7XHJcbiAgZm9udC1zaXplOiA4cHg7XHJcbn1cclxuXHJcbi5jYXJkLWZvdXIge1xyXG4gIHBhZGRpbmctYm90dG9tOiAyMHB4O1xyXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAkY29sb3ItZ3JheS0xMztcclxuICAmLm5vLWJvcmRlciB7XHJcbiAgICBib3JkZXI6IDA7XHJcbiAgfVxyXG4gIC53aXRoLXNoYWRvdyB7XHJcbiAgICBib3gtc2hhZG93OiAwIDVweCAxNnB4IDAgcmdiYSgyMjksIDIzNiwgMjQ1LCAwLjM1KTtcclxuICAgIHBhZGRpbmctYm90dG9tOiAxMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMCAwIDEzcHggMTNweDtcclxuICB9XHJcbiAgLmNhcmQtY29udGFpbmVyIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci13aGl0ZTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHBhZGRpbmctdG9wOiAxOXB4O1xyXG4gICAgLmNhcmQtaGVhZGVyIHtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgIHBhZGRpbmc6IDAgMjBweDtcclxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICYuc2l6ZS0xMDAge1xyXG4gICAgICAgIGhlaWdodDogMTAwcHg7XHJcbiAgICAgICAgaW1nIHtcclxuICAgICAgICAgIHdpZHRoOiAxMDBweDtcclxuICAgICAgICAgIGhlaWdodDogMTAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIC5yaWdodC1pY29uIHtcclxuICAgICAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgICAgICBoZWlnaHQ6IDI0cHg7XHJcbiAgICAgICAgaW1nIHtcclxuICAgICAgICAgIHdpZHRoOiAyNHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAmOjpiZWZvcmUge1xyXG4gICAgICAgICAgY29udGVudDogXCJcIjtcclxuICAgICAgICAgIGhlaWdodDogMjRweDtcclxuICAgICAgICAgIHdpZHRoOiAyNHB4O1xyXG4gICAgICAgICAgb3BhY2l0eTogMDtcclxuICAgICAgICAgIGJhY2tncm91bmQ6IHJnYmEoMjIzLCAyMjMsIDIyMywgMC41KTtcclxuICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgJjphY3RpdmUge1xyXG4gICAgICAgICAgJjo6YmVmb3JlIHtcclxuICAgICAgICAgICAgdHJhbnNmb3JtOiBzY2FsZSgyKTtcclxuICAgICAgICAgICAgdHJhbnNpdGlvbjogYWxsIDAuMXM7XHJcbiAgICAgICAgICAgIG9wYWNpdHk6IDE7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIC5sZWZ0LWljb24ge1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogYXV0bztcclxuICAgICAgICBoZWlnaHQ6IDI0cHg7XHJcbiAgICAgICAgaW1nIHtcclxuICAgICAgICAgIHdpZHRoOiAyNHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyAmOjpiZWZvcmUge1xyXG4gICAgICAgIC8vICAgY29udGVudDogJyc7XHJcbiAgICAgICAgLy8gICBvcGFjaXR5OiAwO1xyXG4gICAgICAgIC8vICAgdHJhbnNmb3JtOiBzY2FsZSgzKTtcclxuICAgICAgICAvLyAgIHRyYW5zaXRpb246IGFsbCAuMnM7XHJcbiAgICAgICAgLy8gICBiYWNrZ3JvdW5kOiByZ2IoMTkyLCAyNywgMjcpO1xyXG4gICAgICAgIC8vICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgICAgIC8vICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIC8vICAgd2lkdGg6IDI0cHg7XHJcbiAgICAgICAgLy8gICBoZWlnaHQ6IDI0cHg7XHJcbiAgICAgICAgLy8gfVxyXG4gICAgICAgIC8vICY6YWN0aXZlIHtcclxuICAgICAgICAvLyAgIDpiZWZvcmUge1xyXG4gICAgICAgIC8vICAgICBvcGFjaXR5OiAxO1xyXG4gICAgICAgIC8vICAgICB0cmFuc2Zvcm06IHNjYWxlKDApO1xyXG4gICAgICAgIC8vICAgICB0cmFuc2l0aW9uOiBub25lO1xyXG4gICAgICAgIC8vICAgfVxyXG4gICAgICAgIC8vIH1cclxuICAgICAgICAgICY6OmJlZm9yZSB7XHJcbiAgICAgICAgICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICAgICAgICAgIGhlaWdodDogMjRweDtcclxuICAgICAgICAgICAgd2lkdGg6IDI0cHg7XHJcbiAgICAgICAgICAgIG9wYWNpdHk6IDA7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6IHJnYmEoMjIzLCAyMjMsIDIyMywgMC4yKTtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICAmOmFjdGl2ZSB7XHJcbiAgICAgICAgICAgICY6OmJlZm9yZSB7XHJcbiAgICAgICAgICAgICAgdHJhbnNmb3JtOiBzY2FsZSgyKTtcclxuICAgICAgICAgICAgICBvcGFjaXR5OiAxO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIC5wcm9maWxlLWltYWdlIHtcclxuICAgICAgaW1nIHtcclxuICAgICAgICBoZWlnaHQ6IDEwMHB4O1xyXG4gICAgICAgIHdpZHRoOiAxMDBweDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxOHB4O1xyXG4gICAgICAgIG9iamVjdC1maXQ6IGNvdmVyO1xyXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICRjb2xvci1ncmF5LTEzO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAuc3VidGl0bGUge1xyXG4gICAgICBtYXJnaW4tdG9wOiA2cHg7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgcGFkZGluZzogMCAyMHB4O1xyXG4gICAgICAmLmFjdGlvbiB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgfVxyXG4gICAgICBpbWcge1xyXG4gICAgICAgIHdpZHRoOiAxMnB4O1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogNnB4O1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgJi53aXRoLWJhY2tncm91bmQge1xyXG4gICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICAgIHBhZGRpbmctdG9wOiA1Ni4yNSU7XHJcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgLmNhcmQtaGVhZGVyIHtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgdG9wOiAyMHB4O1xyXG4gICAgICAgIHBhZGRpbmc6IDAgMjBweDtcclxuICAgICAgfVxyXG4gICAgICAucHJvZmlsZS1pbWFnZSB7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIHRvcDogNTAlO1xyXG4gICAgICAgIGxlZnQ6IDUwJTtcclxuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuICAgICAgfVxyXG4gICAgICAudmlldy1tb3JlIHtcclxuICAgICAgICBoZWlnaHQ6IDMycHg7XHJcbiAgICAgICAgd2lkdGg6IDExOHB4O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDZweDtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNik7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBjb2xvcjogJGNvbG9yLWdyYXktMTY7XHJcbiAgICAgICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICAgICAgbGV0dGVyLXNwYWNpbmc6IDAuMnB4O1xyXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAxOHB4O1xyXG4gICAgICAgIHJpZ2h0OiAxNnB4O1xyXG4gICAgICAgIGJvdHRvbTogMTZweDtcclxuICAgICAgICBpbWcge1xyXG4gICAgICAgICAgd2lkdGg6IDEycHg7XHJcbiAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDZweDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgLnRpdGxlIHtcclxuICAgIG1hcmdpbi10b3A6IDEycHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBwYWRkaW5nOiAwIDIwcHg7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcclxuICAgIC13ZWJraXQtbGluZS1jbGFtcDogMjtcclxuICAgIC8qIGF1dG9wcmVmaXhlcjogaWdub3JlIG5leHQgKi9cclxuICAgIC13ZWJraXQtYm94LW9yaWVudDogdmVydGljYWw7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcbiAgJi5tZWRpYSB7XHJcbiAgICAuY2FyZC1jb250YWluZXIge1xyXG4gICAgICBwYWRkaW5nOiAwO1xyXG4gICAgICAuY2FyZC1oZWFkZXIge1xyXG4gICAgICAgIHBhZGRpbmc6IDA7XHJcbiAgICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgICAgIC5wcm9maWxlLWltYWdlIHtcclxuICAgICAgICAgIHBvc2l0aW9uOiBzdGF0aWM7XHJcbiAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgwKTtcclxuICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgcGFkZGluZy10b3A6IDU2LjI1JTtcclxuICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xyXG4gICAgICAgICAgaW1nIHtcclxuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgLmRlc2Mge1xyXG4gICAgbWFyZ2luLXRvcDogMjBweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMCAwIDEzcHggMTNweDtcclxuICAgICYuc3BhY2UtYmV0d2Vlbi0xMCB7XHJcbiAgICAgIHBhZGRpbmc6IDAgMTBweDtcclxuICAgIH1cclxuICAgICYuc3BhY2UtYmV0d2Vlbi0zMCB7XHJcbiAgICAgIHBhZGRpbmc6IDAgMzBweDtcclxuICAgIH1cclxuICAgICYuc3BhY2UtYmV0d2Vlbi02NSB7XHJcbiAgICAgIHBhZGRpbmc6IDAgNjVweDtcclxuICAgIH1cclxuICAgIC5kZXNjLWNvbnRhaW5lciB7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICB3aWR0aDogODIuNXB4O1xyXG4gICAgICBmbGV4LXNocmluazogMDtcclxuICAgICAgcGFkZGluZzogMCA4cHg7XHJcbiAgICAgICYud2lkdGgtMTIwIHtcclxuICAgICAgICB3aWR0aDogMTIwcHg7XHJcbiAgICAgIH1cclxuICAgICAgJi53aWR0aC0xMDAge1xyXG4gICAgICAgIHdpZHRoOiAxMDBweDtcclxuICAgICAgfVxyXG4gICAgICAmLndpZHRoLTgxIHtcclxuICAgICAgICB3aWR0aDogODFweDtcclxuICAgICAgfVxyXG4gICAgICAmOmZpcnN0LWNoaWxkIHtcclxuICAgICAgICBtYXJnaW4tbGVmdDogMDtcclxuICAgICAgfVxyXG4gICAgICAmOmxhc3QtY2hpbGQge1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogMDtcclxuICAgICAgfVxyXG4gICAgICAuZGVzYy1maXJzdCB7XHJcbiAgICAgICAgaGVpZ2h0OiAzMnB4O1xyXG4gICAgICAgIHdpZHRoOiAzMnB4O1xyXG4gICAgICAgIGNvbG9yOiAkY29sb3Itd2hpdGU7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDdweDtcclxuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAkY29sb3ItZ3JheS0xMztcclxuICAgICAgICAmLmdyZWVuLWdyYWRpZW50IHtcclxuICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1ncmVlbjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGltZyB7XHJcbiAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDdweDtcclxuICAgICAgICAgIG9iamVjdC1maXQ6IGNvdmVyO1xyXG4gICAgICAgIH1cclxuICAgICAgICAmLmNvdW50cnktZmxhZyB7XHJcbiAgICAgICAgICB3aWR0aDogMzJweDtcclxuICAgICAgICAgIGhlaWdodDogMzJ4cDtcclxuICAgICAgICAgIGltZyB7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIHNwYW4ge1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDhweDtcclxuICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgICAgIGRpc3BsYXk6IC13ZWJraXQtYm94O1xyXG4gICAgICAgIC13ZWJraXQtbGluZS1jbGFtcDogMjtcclxuICAgICAgICAvKiBhdXRvcHJlZml4ZXI6IGlnbm9yZSBuZXh0ICovXHJcbiAgICAgICAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbi5jYXJkLXdpdGgtZGVzYyB7XHJcbiAgLmNhcmQtZGVzYy1jb250YWluZXIge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBwYWRkaW5nOiAwIDE2cHg7XHJcbiAgICBtYXJnaW4tdG9wOiAxMDBweDtcclxuICAgIC5jYXJkLWRlc2MtaW1nIHtcclxuICAgICAgcGFkZGluZy10b3A6IDU2LjI1JTtcclxuICAgICAgYm9yZGVyOiAxcHggc29saWQgJGNvbG9yLWdyYXktMTY7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDdweDtcclxuICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICBpbWcge1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICB0b3A6IDUwJTtcclxuICAgICAgICBsZWZ0OiA1MCU7XHJcbiAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogN3B4O1xyXG4gICAgICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIC5saXN0LXdpdGgtaW1hZ2Uge1xyXG4gICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICBpbWcge1xyXG4gICAgICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgICAgIGhlaWdodDogMjBweDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA3cHg7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiA4cHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIC5ib2xkLXRpdGxlIHtcclxuICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgIH1cclxuICAgIC50aXRsZSB7XHJcbiAgICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgICB9XHJcbiAgICAuYnVsbGV0LWxpcyB7XHJcbiAgICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbi5jYXJkLWJveCB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICB3aWR0aDogMTAwJTtcclxuICBvdmVyZmxvdzogc2Nyb2xsO1xyXG4gIHBhZGRpbmctYm90dG9tOiAyMHB4O1xyXG4gIHBhZGRpbmctbGVmdDogMjBweDtcclxuICAuY2FyZC1ib3gtY29udGFpbmVyIHtcclxuICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxuICAgIC5jYXJkLWJveC1pdGVtIHtcclxuICAgICAgaGVpZ2h0OiAyMDlweDtcclxuICAgICAgd2lkdGg6IDE1MnB4O1xyXG4gICAgICBib3JkZXI6IDFweCBzb2xpZCAkY29sb3ItZ3JheS0xNjtcclxuICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3Itd2hpdGU7XHJcbiAgICAgIC5maXJzdC1pdGVtIHtcclxuICAgICAgICBoZWlnaHQ6IDEzMnB4O1xyXG4gICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAkY29sb3ItZ3JheS0xNjtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogOHB4IDhweCAwIDA7XHJcbiAgICAgICAgaW1nIHtcclxuICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgIG9iamVjdC1maXQ6IGNvdmVyO1xyXG4gICAgICAgICAgYm9yZGVyLXJhZGl1czogN3B4IDdweCAwIDA7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIC5zZWNvbmQtaXRlbSB7XHJcbiAgICAgICAgcGFkZGluZzogMTNweDtcclxuICAgICAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICAgICAgaGVpZ2h0OiA3N3B4O1xyXG4gICAgICAgIHNwYW4ge1xyXG4gICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgICAgICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XHJcbiAgICAgICAgICAtd2Via2l0LWxpbmUtY2xhbXA6IDI7XHJcbiAgICAgICAgICAvKiBhdXRvcHJlZml4ZXI6IGlnbm9yZSBuZXh0ICovXHJcbiAgICAgICAgICAtd2Via2l0LWJveC1vcmllbnQ6IHZlcnRpY2FsO1xyXG4gICAgICAgIH1cclxuICAgICAgICBkaXYge1xyXG4gICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgICAgICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XHJcbiAgICAgICAgICAtd2Via2l0LWxpbmUtY2xhbXA6IDE7XHJcbiAgICAgICAgICAvKiBhdXRvcHJlZml4ZXI6IGlnbm9yZSBuZXh0ICovXHJcbiAgICAgICAgICAtd2Via2l0LWJveC1vcmllbnQ6IHZlcnRpY2FsO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuLmNhcmQtZ2VuZXJhbC1pbmZvIHtcclxuICAucHJvZHVjdC1pbWFnZSB7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1ncmF5LTEzO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgJGNvbG9yLWdyYXktMTM7XHJcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gICAgcGFkZGluZy10b3A6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gICAgbWF4LWhlaWdodDogNTAwcHg7XHJcbiAgfVxyXG4gIC5hY3RvciB7XHJcbiAgICBtYXJnaW4tdG9wOiAxNnB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAuYWN0b3ItaW1hZ2Uge1xyXG4gICAgICBoZWlnaHQ6IDIwcHg7XHJcbiAgICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3ItZ3JheS0xNztcclxuICAgIH1cclxuICAgIGltZyB7XHJcbiAgICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgICBoZWlnaHQ6IDIwcHg7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDRweDtcclxuICAgICAgb2JqZWN0LWZpdDogY292ZXI7XHJcbiAgICAgIGJvcmRlcjogMXB4IHNvbGlkICRjb2xvci1ncmF5LTE3O1xyXG4gICAgfVxyXG4gICAgLmFjdG9yLW5hbWUge1xyXG4gICAgICBtYXJnaW4tbGVmdDogOHB4O1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgIH1cclxuICB9XHJcbiAgLnRpdGxlIHtcclxuICAgIG1hcmdpbi10b3A6IDlweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDJweDtcclxuICB9XHJcbiAgLmRlc2NyaXB0aW9uIHtcclxuICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgfVxyXG59XHJcbi5zY3JvbGwtY291bnRyeS1jb250YWluZXIge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgb3ZlcmZsb3c6IHNjcm9sbDtcclxuICBwYWRkaW5nOiAwIDIwcHg7XHJcblxyXG4gIC5zY3JvbGwtY291bnRyeS1pdGVtIHtcclxuICAgIG1heC13aWR0aDogOTBweDtcclxuICAgIG1hcmdpbi1yaWdodDogNDBweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgJjpsYXN0LWNoaWxkIHtcclxuICAgICAgbWFyZ2luLXJpZ2h0OiAwO1xyXG4gICAgfVxyXG4gIH1cclxuICAuY291bnRyeS1pbWFnZSB7XHJcbiAgICB3aWR0aDogMzJweDtcclxuICAgIGhlaWdodDogMzJweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDdweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1ncmF5LTEzO1xyXG5cclxuICAgIGltZyB7XHJcbiAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDdweDtcclxuICAgIH1cclxuICB9XHJcbiAgLmNvdW50cnktdGl0bGUge1xyXG4gICAgbWFyZ2luLXRvcDogOHB4O1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XHJcbiAgICAtd2Via2l0LWxpbmUtY2xhbXA6IDI7XHJcbiAgICAvKiBhdXRvcHJlZml4ZXI6IGlnbm9yZSBuZXh0ICovXHJcbiAgICAtd2Via2l0LWJveC1vcmllbnQ6IHZlcnRpY2FsO1xyXG4gIH1cclxufVxyXG4uZGVzY3JpcHRpb24ge1xyXG4gICYucG9zdC1ib2R5IHtcclxuICAgIGxpIHtcclxuICAgICAgbWFyZ2luLWxlZnQ6IDE1cHg7XHJcbiAgICB9XHJcbiAgICBpbWcge1xyXG4gICAgICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xyXG4gICAgICAvLyBtYXJnaW4tYm90dG9tOiAzMHB4O1xyXG4gICAgICBtYXJnaW4tdG9wOiAxMXB4O1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gICAgfVxyXG4gIH1cclxuICBpZnJhbWUge1xyXG4gICAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcclxuICAgIG1hcmdpbi10b3A6IDMwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gIH1cclxufVxyXG5cclxuLy8gbmV3IGNhcmQgdHlwZVxyXG4ubWFpbi1jYXJkIHtcclxuICBiYWNrZ3JvdW5kOiAkY29sb3Itd2hpdGU7XHJcbiBcclxuICAubWFpbi1jYXJkLWNvbnRhaW5lciB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMjMwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogIHVybCgnL2Fzc2V0cy9pY29ucy9lbXB0eVN0YXRlL3BsYWNlaG9sZGVyX2NvdmVyLmpwZycpO1xyXG4gICAgLmNvdmVyLWltYWdlIHtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgIGhlaWdodDogMjMwcHg7XHJcbiAgICB9XHJcbiAgICAuY2FyZC1uYXZiYXIge1xyXG4gICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgdG9wOiAyMHB4O1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgei1pbmRleDogMTAwO1xyXG4gICAgICBwYWRkaW5nOiAwIDIwcHg7XHJcbiAgICAgIC5sZWZ0IHtcclxuICAgICAgICBpbWcge1xyXG4gICAgICAgICAgd2lkdGg6IDI0cHg7XHJcbiAgICAgICAgICBoZWlnaHQ6IDI0cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgICY6OmJlZm9yZSB7XHJcbiAgICAgICAgICBjb250ZW50OiBcIlwiO1xyXG4gICAgICAgICAgaGVpZ2h0OiAyNHB4O1xyXG4gICAgICAgICAgd2lkdGg6IDI0cHg7XHJcbiAgICAgICAgICBvcGFjaXR5OiAwO1xyXG4gICAgICAgICAgYmFja2dyb3VuZDogcmdiYSgyMjMsIDIyMywgMjIzLCAwLjIpO1xyXG4gICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIH1cclxuICAgICAgICAmOmFjdGl2ZSB7XHJcbiAgICAgICAgICAmOjpiZWZvcmUge1xyXG4gICAgICAgICAgICB0cmFuc2Zvcm06IHNjYWxlKDIpO1xyXG4gICAgICAgICAgICB0cmFuc2l0aW9uOiBhbGwgMC4xcztcclxuICAgICAgICAgICAgb3BhY2l0eTogMTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgLnJpZ2h0IHtcclxuICAgICAgICBpbWcge1xyXG4gICAgICAgICAgd2lkdGg6IDI0cHg7XHJcbiAgICAgICAgICBoZWlnaHQ6IDI0cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgICY6OmJlZm9yZSB7XHJcbiAgICAgICAgICBjb250ZW50OiBcIlwiO1xyXG4gICAgICAgICAgaGVpZ2h0OiAyNHB4O1xyXG4gICAgICAgICAgd2lkdGg6IDI0cHg7XHJcbiAgICAgICAgICBvcGFjaXR5OiAwO1xyXG4gICAgICAgICAgYmFja2dyb3VuZDogcmdiYSgyMjMsIDIyMywgMjIzLCAwLjMpO1xyXG4gICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIH1cclxuICAgICAgICAmOmFjdGl2ZSB7XHJcbiAgICAgICAgICAmOjpiZWZvcmUge1xyXG4gICAgICAgICAgICB0cmFuc2Zvcm06IHNjYWxlKDIpO1xyXG4gICAgICAgICAgICB0cmFuc2l0aW9uOiBhbGwgMC4xcztcclxuICAgICAgICAgICAgb3BhY2l0eTogMTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIC5vdmVybGF5IHtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgaGVpZ2h0OiAyMzBweDtcclxuICAgICAgdG9wOiAwO1xyXG4gICAgICBsZWZ0OiAwO1xyXG4gICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgICAgICYuZ3JlZW4ge1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoNSwgMjI5LCAyMDYsIDAuNDEpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAuY2FyZC1zZWFyY2gge1xyXG4gICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICBoZWlnaHQ6IDM5cHg7XHJcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgdG9wOiAyMHB4O1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgcGFkZGluZzogMCAyMHB4O1xyXG4gICAgICAuaW5wdXQge1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogMjBweDtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgaGVpZ2h0OiAzOXB4O1xyXG4gICAgICAgIGlucHV0IHtcclxuICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgaGVpZ2h0OiAzNHB4O1xyXG4gICAgICAgICAgYm9yZGVyLXJhZGl1czogMThweDtcclxuICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1ncmF5LTE3O1xyXG4gICAgICAgICAgYm94LXNoYWRvdzogMCAwIDEwcHggMCByZ2JhKDAsIDAsIDAsIDAuMDUpO1xyXG4gICAgICAgICAgYm9yZGVyOiAwO1xyXG4gICAgICAgICAgcGFkZGluZy1sZWZ0OiA1MHB4O1xyXG4gICAgICAgICAgcGFkZGluZy1yaWdodDogMjBweDtcclxuICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAgICYuc2VhcmNoIHtcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIvYXNzZXRzL2ljb25zL2RpYXNwb3JhSWNvbi9TZWFyY2hfR3JleS5zdmdcIik7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IDE2cHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtc2l6ZTogMjBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDJweDtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgICY6OnBsYWNlaG9sZGVyIHtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgICAgICBjb2xvcjogJGNvbG9yLWdyYXktMTU7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAuaWNvbnMge1xyXG4gICAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgZmxleC1zaHJpbms6IDA7XHJcbiAgICAgIHotaW5kZXg6IDEwO1xyXG4gICAgICAuZmlyc3Qge1xyXG4gICAgICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogMThweDtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgaW1nIHtcclxuICAgICAgICAgIHdpZHRoOiAyNHB4O1xyXG4gICAgICAgICAgaGVpZ2h0OiAyNXB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAud2l0aEJhZGdlIHtcclxuICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgIGJhY2tncm91bmQ6ICRjb2xvci1vcmFuZ2U7XHJcbiAgICAgICAgICB3aWR0aDogMThweDtcclxuICAgICAgICAgIGhlaWdodDogMThweDtcclxuICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICBib3JkZXI6IDJweCBzb2xpZCAkY29sb3Itd2hpdGU7XHJcbiAgICAgICAgICBwYWRkaW5nOiAxcHg7XHJcbiAgICAgICAgICB0b3A6IC01cHg7XHJcbiAgICAgICAgICBsZWZ0OiAxMnB4O1xyXG4gICAgICAgICAgcGFkZGluZy1ib3R0b206IDFweDtcclxuXHJcbiAgICAgICAgICAmLm5vdGlmaWNhdGlvbnMtY291bnQge1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDlweDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgLnNlY29uZCB7XHJcbiAgICAgICAgZmxleC1zaHJpbms6IDA7XHJcbiAgICAgICAgaW1nIHtcclxuICAgICAgICAgIHdpZHRoOiAyNHB4O1xyXG4gICAgICAgICAgaGVpZ2h0OiAyNHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuICAuY2FyZC1ib2R5IHtcclxuICAgIGJhY2tncm91bmQ6ICRjb2xvci13aGl0ZTtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYm94LXNoYWRvdzogMCA1cHggMTZweCAwIHJnYmEoMjI5LCAyMzYsIDI0NSwgMC4zNSk7XHJcbiAgICBtYXJnaW4tdG9wOiAtNzhweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEzcHg7XHJcbiAgICB6LWluZGV4OiA3MjtcclxuICAgIHBhZGRpbmc6IDIwcHggMDtcclxuICAgIHBhZGRpbmctdG9wOiA2OHB4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgLmNhcmQtYm9keS1pbWFnZSB7XHJcbiAgICAgIHdpZHRoOiAxMDBweDtcclxuICAgICAgaGVpZ2h0OiAxMDBweDtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICBib3gtc2hhZG93OiAwIDZweCAxNHB4IDAgcmdiYSgwLCAwLCAwLCAwLjEpO1xyXG4gICAgICB0b3A6IC01MHB4O1xyXG4gICAgICBsZWZ0OiA1MCU7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgMCk7XHJcbiAgICAgIGltZyB7XHJcbiAgICAgICAgd2lkdGg6IDEwMHB4O1xyXG4gICAgICAgIGhlaWdodDogMTAwcHg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgICAgIG9iamVjdC1maXQ6IGNvdmVyO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLmNhcmQtYm9keS1jb250YWluZXIge1xyXG4gICAgICAvLyBtYXJnaW4tdG9wOiA2NnB4O1xyXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgIC5jYXJkLWJvZHktdGl0bGUge1xyXG4gICAgICAgIHBhZGRpbmc6IDAgMjBweDtcclxuICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgICAgIGRpc3BsYXk6IC13ZWJraXQtYm94O1xyXG4gICAgICAgIC13ZWJraXQtbGluZS1jbGFtcDogMjtcclxuICAgICAgICAvKiBhdXRvcHJlZml4ZXI6IGlnbm9yZSBuZXh0ICovXHJcbiAgICAgICAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcclxuICAgICAgfVxyXG4gICAgICAuY2FyZC1ib2R5LXN1YnRpdGxlIHtcclxuICAgICAgICBtYXJnaW4tdG9wOiAycHg7XHJcbiAgICAgIH1cclxuICAgICAgLmNhcmQtYm9keS1zdWJ0aXRsZS13aXRoLWltYWdlIHtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgcGFkZGluZzogMCAyMHB4O1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDJweDtcclxuICAgICAgICBpbWcge1xyXG4gICAgICAgICAgd2lkdGg6IDEycHg7XHJcbiAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDZweDtcclxuICAgICAgICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuICAuZGVzYyB7XHJcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbiAgICBib3JkZXItcmFkaXVzOiAwIDAgMTNweCAxM3B4O1xyXG4gICAgJi5zcGFjZS1iZXR3ZWVuLTEwIHtcclxuICAgICAgcGFkZGluZzogMCAxMHB4O1xyXG4gICAgfVxyXG4gICAgJi5zcGFjZS1iZXR3ZWVuLTMwIHtcclxuICAgICAgcGFkZGluZzogMCAzMHB4O1xyXG4gICAgfVxyXG4gICAgJi5zcGFjZS1iZXR3ZWVuLTY1IHtcclxuICAgICAgcGFkZGluZzogMCA2NXB4O1xyXG4gICAgfVxyXG4gICAgLmRlc2MtY29udGFpbmVyIHtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgIHdpZHRoOiA4Mi41cHg7XHJcbiAgICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gICAgICBwYWRkaW5nOiAwIDhweDtcclxuICAgICAgJi53aWR0aC0xMjAge1xyXG4gICAgICAgIHdpZHRoOiAxMjBweDtcclxuICAgICAgfVxyXG4gICAgICAmLndpZHRoLTEwMCB7XHJcbiAgICAgICAgd2lkdGg6IDEwMHB4O1xyXG4gICAgICB9XHJcbiAgICAgICYud2lkdGgtODEge1xyXG4gICAgICAgIHdpZHRoOiA4MXB4O1xyXG4gICAgICB9XHJcbiAgICAgICY6Zmlyc3QtY2hpbGQge1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAwO1xyXG4gICAgICB9XHJcbiAgICAgICY6bGFzdC1jaGlsZCB7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAwO1xyXG4gICAgICB9XHJcbiAgICAgIC5kZXNjLWZpcnN0IHtcclxuICAgICAgICBoZWlnaHQ6IDMycHg7XHJcbiAgICAgICAgd2lkdGg6IDMycHg7XHJcbiAgICAgICAgY29sb3I6ICRjb2xvci13aGl0ZTtcclxuICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogN3B4O1xyXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICRjb2xvci1ncmF5LTIzO1xyXG5cclxuICAgICAgICAmLmdyZWVuLWdyYWRpZW50IHtcclxuICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1ncmVlbjtcclxuICAgICAgICB9XHJcbiAgICAgICAgaW1nIHtcclxuICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgICAgYm9yZGVyLXJhZGl1czogN3B4O1xyXG5cclxuICAgICAgICAgIG9iamVjdC1maXQ6IGNvdmVyO1xyXG4gICAgICAgIH1cclxuICAgICAgICAmLmNvdW50cnktZmxhZyB7XHJcbiAgICAgICAgICB3aWR0aDogMzJweDtcclxuICAgICAgICAgIGhlaWdodDogMzJweDtcclxuICAgICAgICAgIGltZyB7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIHNwYW4ge1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDhweDtcclxuICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgICAgIGRpc3BsYXk6IC13ZWJraXQtYm94O1xyXG4gICAgICAgIC13ZWJraXQtbGluZS1jbGFtcDogMjtcclxuICAgICAgICAvKiBhdXRvcHJlZml4ZXI6IGlnbm9yZSBuZXh0ICovXHJcbiAgICAgICAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuLmlvcyB7XHJcbiAgLmlucHV0IHtcclxuICAgIGlucHV0IHtcclxuICAgICAgcGFkZGluZy10b3A6IDVweDtcclxuICAgIH1cclxuICB9XHJcbn1cclxuLmNhcmQtbWFpbi1idXR0b24tY29udGFpbmVyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3Itd2hpdGUtZ3JheTtcclxuICAuY2FyZC1tYWluLWJ1dHRvbiB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJhY2tncm91bmQ6ICRjb2xvci13aGl0ZTtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAuY2FyZC1tYWluLWJ1dHRvbi1pdGVtIHtcclxuICAgICAgd2lkdGg6IDUwJTtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgIHBhZGRpbmctdG9wOiAxNnB4O1xyXG4gICAgICBpbWcge1xyXG4gICAgICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgICAgIGhlaWdodDogMjBweDtcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDhweDtcclxuICAgICAgfVxyXG4gICAgICAmLndpZHRoLTEwMCB7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgIH1cclxuICAgICAgJjpudGgtY2hpbGQoZXZlbikge1xyXG4gICAgICAgIGJvcmRlci1sZWZ0OiAxcHggc29saWQgJGNvbG9yLWdyYXktMTM7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbi5tYWluLXNlcGVyYXRvciB7XHJcbiAgcGFkZGluZzogMjBweCAwO1xyXG4gIHBhZGRpbmctdG9wOiAzMHB4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBiYWNrZ3JvdW5kOiAkY29sb3Itd2hpdGU7XHJcbiAgLmxlZnQtaXRlbSB7XHJcbiAgICAuZmlyc3Qge1xyXG4gICAgfVxyXG4gICAgLnNlY29uZCB7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5yaWdodC1pdGVtIHtcclxuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgLmZpcnN0IHtcclxuICAgICAgaW1nIHtcclxuICAgICAgICB3aWR0aDogMThweDtcclxuICAgICAgICAmLmxhcmdlLWltYWdlIHtcclxuICAgICAgICAgIHdpZHRoOiA0MXB4O1xyXG4gICAgICAgICAgaGVpZ2h0OiA0MXB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuLnNsaWRlci1jYXJkIHtcclxuICAuY2FyZC1jb250YWluZXIge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgLmNhcmQtbWFpbi1pbWFnZSB7XHJcbiAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICBpbWcge1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxM3B4O1xyXG4gICAgICAgIG9iamVjdC1maXQ6IGNvdmVyO1xyXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICRjb2xvci1ncmF5LTIzO1xyXG4gICAgICB9XHJcbiAgICAgIC5vdmVybGF5IHtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSg3NCwgNzQsIDc0LCAwLjcxKTtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgbGVmdDogMDtcclxuICAgICAgICB0b3A6IDA7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTNweDtcclxuICAgICAgfVxyXG4gICAgICAuY2FyZC10ZXh0IHtcclxuICAgICAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgY29sb3I6ICRjb2xvci13aGl0ZTtcclxuICAgICAgICBsZXR0ZXItc3BhY2luZzogMC4xM3B4O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgdG9wOiAzOXB4O1xyXG4gICAgICAgIHBhZGRpbmc6IDAgMTBweDtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuICAgICAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcclxuICAgICAgICAtd2Via2l0LWxpbmUtY2xhbXA6IDI7XHJcbiAgICAgICAgLyogYXV0b3ByZWZpeGVyOiBpZ25vcmUgbmV4dCAqL1xyXG4gICAgICAgIC13ZWJraXQtYm94LW9yaWVudDogdmVydGljYWw7XHJcbiAgICAgIH1cclxuICAgICAgLmNhcmQtbG9nbyB7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIHRvcDogLTI3cHg7XHJcbiAgICAgICAgaGVpZ2h0OiA1NXB4O1xyXG4gICAgICAgIHdpZHRoOiA1NXB4O1xyXG4gICAgICAgIGxlZnQ6IDUwJTtcclxuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAwcHgpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAuYWN0aW9ucyB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1ncmVlbi01O1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgaGVpZ2h0OiA1NnB4O1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIGJvdHRvbTogMDtcclxuICAgICAgYm9yZGVyLXJhZGl1czogMTNweDtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICAgIC5hY3Rpb25zLWl0ZW0ge1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIHBhZGRpbmc6IDExcHg7XHJcbiAgICAgICAgaW1nIHtcclxuICAgICAgICAgIHdpZHRoOiAxNXB4O1xyXG4gICAgICAgICAgaGVpZ2h0OiAxNXB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBzcGFuIHtcclxuICAgICAgICAgIGNvbG9yOiAkY29sb3Itd2hpdGU7XHJcbiAgICAgICAgICBmb250LXNpemU6IDExcHg7XHJcbiAgICAgICAgICBtYXJnaW4tdG9wOiAzcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiIsIi5oaW50cyB7XHJcbiAgICBwYWRkaW5nOiAwIDE2cHg7XHJcbiAgICAuaGludHMtY29udGFpbmVyIHtcclxuICAgICAgICBtaW4taGVpZ2h0OiA3MnB4O1xyXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICRjb2xvci1ncmVlbi0yO1xyXG4gICAgICAgIHBhZGRpbmc6IDAgMTZweDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA2cHg7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1ncmVlbi0zO1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDEwMHB4O1xyXG4gICAgICAgIC5sZWZ0IHtcclxuICAgICAgICAgICAgZmxleC1zaHJpbms6IDA7XHJcbiAgICAgICAgICAgIGltZyB7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDMycHg7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMzJweDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAubWlkZGxlIHtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6MTRweDsgXHJcbiAgICAgICAgICAgIG1heC13aWR0aDogMjAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5yaWdodCB7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gICAgICAgICAgICBpbWcge1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAxNnB4O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59IiwiKiB7XHJcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICBtYXJnaW46IDA7XHJcbiAgcGFkZGluZzogMDtcclxuICBvdXRsaW5lOiBub25lO1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZiAhaW1wb3J0YW50O1xyXG4gIHdvcmQtYnJlYWs6IGJyZWFrLXdvcmQ7XHJcbiAgLS1yaXBwbGUtY29sb3I6IHRyYW5zcGFyZW50O1xyXG59XHJcbjo6LXdlYmtpdC1zY3JvbGxiYXIsXHJcbio6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcclxuICBkaXNwbGF5OiBub25lO1xyXG59XHJcbmJvZHkge1xyXG4gICYuYmxhY2tCb2R5IHtcclxuICAgIGJhY2tncm91bmQ6ICRjb2xvci1ibGFjaztcclxuICB9XHJcbn1cclxuXHJcbi8vIG1ha2UgaXRlbSBpbmxpbmVcclxuXHJcbi5pbmxpbmUge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgJi53cmFwIHtcclxuICAgIGZsZXgtd3JhcDogd3JhcDtcclxuICB9XHJcbiAgJi5zY3JvbGwge1xyXG4gICAgb3ZlcmZsb3c6IHNjcm9sbDtcclxuICB9XHJcbiAgJi5jZW50ZXIge1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICB9XHJcbiAgJi5oLWNlbnRlciB7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICB9XHJcbn1cclxuLy8gbWFrZSBpdGVtIGlubGluZSBhbmQgc2Nyb2xsXHJcblxyXG4uc2Nyb2xsLW11bHRpcGxlLWxpbmUge1xyXG5kaXNwbGF5OiBmbGV4O1xyXG59XHJcblxyXG4vLyBzbGlkZSBjb250YWluIHRocmVlIGl0ZW0gc3R5bGVcclxuYXBwLWxpc3QtY2FyZCB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5zbGlkZS1jb250YWluZXIge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgcGFkZGluZy1yaWdodDogMTJweDtcclxuICBcclxuICAvLyAgbWFrZSB0aGUgbGlzdCB0aXRsZSB0cmltIG9uIG9uZSBsaW5lIGluIHRoZSBzbGlkZXIgb2YgdGhlIHRocmVlIGl0ZW1cclxuXHJcbiAgLmxpc3QtdGl0bGUge1xyXG4gICAgLXdlYmtpdC1saW5lLWNsYW1wOiAxO1xyXG4gIH1cclxuICAmLmFjdGlvbi1zbGlkZXIge1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAycHg7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAwICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG59XHJcbmlvbi1zbGlkZSAge1xyXG4gICY6bGFzdC1jaGlsZCB7XHJcbiAgICAuYWN0aW9uLXNsaWRlciB7XHJcbiAgICAgIG1hcmdpbi1yaWdodDogMCAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG4gICAgLnNsaWRlLWNvbnRhaW5lciB7XHJcbiAgICAgIHBhZGRpbmctcmlnaHQ6IDAgIWltcG9ydGFudDtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbi8vIHRleHQgd2l0aCBsZWZ0IGFuZCByaWdodCBsaW5lXHJcbi50ZXh0LXdpdGgtbGluZS1jb250YWluZXIge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBwYWRkaW5nOiAwIDIwcHg7XHJcbiAgcGFkZGluZy10b3A6IDIwcHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLXdoaXRlO1xyXG4gIC5saW5lLWxlZnQge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDFweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1ncmF5LTEzO1xyXG4gIH1cclxuICAubGluZS1yaWdodCB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMXB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLWdyYXktMTM7XHJcbiAgfVxyXG4gIC50eHQge1xyXG4gICAgZmxleC1zaHJpbms6IDA7XHJcbiAgICBwYWRkaW5nOiAwIDE2cHg7XHJcbiAgICB6LWluZGV4OiAxMDtcclxuICB9XHJcbn1cclxuXHJcbi8vY2VudGVyIHRoZSB0ZXh0IFxyXG5cclxuLmNlbnRlci10ZXh0IHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi8vIHNwaW5uZXIgc3R5bGVcclxuLnNwaW5uZXJIb2xkZXIge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgbWFyZ2luLXRvcDogODBweDtcclxuIGlvbi1zcGlubmVyIHtcclxuICAgY29sb3I6ICRjb2xvci1ncmVlbi01ICFpbXBvcnRhbnQ7XHJcbiB9XHJcbn1cclxuXHJcbi8vIHRoZSBwb2ludCBiZXR3ZWVuIHRoZSB0ZXh0IFxyXG4uYnVsbGV0LXBvaW50IHtcclxuICAgIGZvbnQtc2l6ZTogOXB4O1xyXG4gICAgbWFyZ2luLXJpZ2h0OiA4cHg7XHJcbiAgICBtYXJnaW4tbGVmdDogOHB4O1xyXG59XHJcblxyXG4vLyB0aGUgc3Bpbm5lciBvZiBpbmZpbml0ZSBzY3JvbGxcclxuLmluZmluaXRlLWxvYWRpbmcge1xyXG4gIG1hcmdpbjogMCAhaW1wb3J0YW50O1xyXG59XHJcbmlvbi1pbmZpbml0ZS1zY3JvbGwtY29udGVudCB7XHJcbiAgbWluLWhlaWdodDogNzBweDtcclxuICAmLmdyYXktYmFja2dyb3VuZCB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3Itd2hpdGUtZ3JheTtcclxuICB9XHJcbiAgJi5sb25nLWhlaWdodCB7XHJcbiAgICBtaW4taGVpZ2h0OiA3OXB4O1xyXG4gIH1cclxufVxyXG5pb24tc3Bpbm5lciB7XHJcbiAgY29sb3I6ICRjb2xvci1ncmVlbi01ICFpbXBvcnRhbnQ7XHJcbn1cclxuLm1haW5GYWJCdXR0b24ge1xyXG5pb24tZmFiLWJ1dHRvbiB7XHJcbiAgLS1iYWNrZ3JvdW5kOiAjMDBkMmJkO1xyXG4gIGhlaWdodDogNTBweDtcclxuICB3aWR0aDogNTBweDtcclxuICAtLWJhY2tncm91bmQtYWN0aXZhdGVkIDogIzAwZDJiZDtcclxufVxyXG5ib3R0b206IDIwcHggIWltcG9ydGFudDtcclxucmlnaHQ6IDIwcHggIWltcG9ydGFudDtcclxuaW1nIHtcclxuICBoZWlnaHQ6IDIxcHg7XHJcbn1cclxufVxyXG5cclxuaW9uLWFwcCB7XHJcbiAgbWFyZ2luLXRvcDogZW52KHNhZmUtYXJlYS1pbnNldC10b3ApO1xyXG4gIG1hcmdpbi1ib3R0b206IGVudihzYWZlLWFyZWEtaW5zZXQtYm90dG9tKTtcclxufVxyXG4uaGVhZGVyLWlvcyBpb24tdG9vbGJhcjpsYXN0LWNoaWxkIHsgICBcclxuICAtLWJvcmRlci13aWR0aDogMCAhaW1wb3J0YW50OyBcclxufSBcclxuXHJcbi8vIGJhciBpbnB1dCBpY29uIG5vdCBjZW50ZXJlZCBcclxuXHJcbi5pb3MgeyBcclxuICAvL25hdmJhciBpbnB1dCBzcGFjaW5nIFxyXG4gIGlvbi1oZWFkZXIgaW9uLXRvb2xiYXI6Zmlyc3QtY2hpbGQgeyAgIFxyXG4gICAgLS1wYWRkaW5nLXRvcDogMHB4ICFpbXBvcnRhbnQ7ICAgXHJcbiAgICAtLXBhZGRpbmctYm90dG9tOiAwcHggIWltcG9ydGFudDsgICBcclxuICAgIC0tcGFkZGluZy1zdGFydDogMHB4ICFpbXBvcnRhbnQ7ICAgXHJcbiAgICAtLXBhZGRpbmctZW5kOiAwcHggIWltcG9ydGFudDsgXHJcbiAgICBwYWRkaW5nOiAwICFpbXBvcnRhbnQ7XHJcbiAgfSBcclxuXHJcbiAgaW9uLWNvbnRlbnQgeyAgIFxyXG4gICAgLS1rZXlib2FyZC1vZmZzZXQ6IDBweCAhaW1wb3J0YW50OyBcclxuICB9IFxyXG4gIC5mci10b29sYmFyLmZyLXRvcCB7ICBcclxuICAgICBib3JkZXI6IDA7XHJcbiAgICAgIGhlaWdodDogNTdweCAhaW1wb3J0YW50OyBcclxuICAgICAgZGlzcGxheTogZmxleCAhaW1wb3J0YW50OyBcclxuICAgICAgYWxpZ24taXRlbXM6IFxyXG4gICAgICBjZW50ZXIgIWltcG9ydGFudDsgXHJcbiAgICAgIGJhY2tncm91bmQ6ICNmMGY1ZmYgIWltcG9ydGFudDtcclxuICAgICAgIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDEwcHggIWltcG9ydGFudDsgXHJcbiAgICAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMTBweCAhaW1wb3J0YW50OyBcclxuICAgICAgfVxyXG4gICAgICAgfVxyXG4gICAgICAgLmJsYWNrVG9vbGJhciB7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiAjMDAwMDAwO1xyXG4gICAgICAgIC5wYWdlcy10b3AtYmFyLXRpdGxlIHtcclxuICAgICAgICAgIGNvbG9yOiAkY29sb3Itd2hpdGUgIWltcG9ydGFudDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBpb24tZm9vdGVyIHtcclxuICAgICAgYmFja2dyb3VuZDogI2ZmZmZmZjtcclxuICB9XHJcblxyXG4gIC5pb3Mge1xyXG4gICAgaW9uLWZvb3RlciB7XHJcbiAgICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCAjRUNFRkYxICFpbXBvcnRhbnQ7IFxyXG4gICAgICAmLm5vLWJvcmRlciB7XHJcbiAgICAgICAgYm9yZGVyLXRvcDogMCAhaW1wb3J0YW50O1xyXG4gICAgICB9XHJcbiAgICAgICYubm8tYm9yZGVyLWZvb3Rlci10b3Age1xyXG4gICAgICAgIGJvcmRlci10b3A6IDAgIWltcG9ydGFudDtcclxuICAgICAgfVxyXG4gICAgfSBcclxufVxyXG5cclxuLnJlbW92ZS1jdXJzb3ItaW5wdXQge1xyXG4gIGlucHV0IHtcclxuICAgIGNhcmV0LWNvbG9yOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xyXG4gICAgcG9pbnRlci1ldmVudHM6IG5vbmU7XHJcblxyXG4gIH1cclxuXHJcbiAgdGV4dGFyZWEge1xyXG4gICAgY2FyZXQtY29sb3I6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XHJcbiAgICBwb2ludGVyLWV2ZW50czogbm9uZTtcclxuICB9XHJcbn0gXHJcblxyXG4uYWRkLWN1cnNvci1pbnB1dCB7XHJcbiAgaW5wdXQge1xyXG4gICAgY2FyZXQtY29sb3I6ICRjb2xvci1ncmVlbi01ICFpbXBvcnRhbnQ7XHJcbiAgICBwb2ludGVyLWV2ZW50czogYXV0bztcclxuICB9XHJcblxyXG4gIHRleHRhcmVhIHtcclxuICAgIGNhcmV0LWNvbG9yOiAkY29sb3ItZ3JlZW4gIWltcG9ydGFudDtcclxuICAgIHBvaW50ZXItZXZlbnRzOiBhdXRvO1xyXG4gIH1cclxufVxyXG4udG9hc3Qtd2l0aC1idXR0b24ge1xyXG4gIC0tYnV0dG9uLWNvbG9yOiAjMDBkMmJkO1xyXG59XHJcbi5pb3Mge1xyXG4gIC5mb290ZXItaW9zIGlvbi10b29sYmFyOmZpcnN0LWNoaWxkIHtcclxuICAgICAgcGFkZGluZzogIDAgIWltcG9ydGFudDtcclxuICAgIC0tYm9yZGVyLWNvbG9yIDojRDVFMEVBO1xyXG4gIH1cclxufVxyXG4uaW9zIHtcclxuICBpb24tZm9vdGVyIHtcclxuICAgICYuZml4ZWRzcGFjZWZvb3RlciB7XHJcbiAgICAgIHBhZGRpbmc6ICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiIsIi5jaGVja2JveCB7XHJcbiAgaGVpZ2h0OiA2MHB4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgJGNvbG9yLWdyYXktMTIgICA7XHJcbmltZyB7XHJcbiAgd2lkdGg6IDI0cHg7XHJcbiAgbWFyZ2luLXJpZ2h0OiAxOHB4O1xyXG59XHJcbiAgJjpsYXN0LWNoaWxkIHtcclxuICAgIGJvcmRlcjogMDtcclxuICB9XHJcbiAgLmNoZWNrYm94LWNvbnRhaW5lciB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gIH1cclxuICAuY29udGFpbmVyIHtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICB9XHJcbiAgLmNvbnRhaW5lciBpbnB1dCB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBvcGFjaXR5OiAwO1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgaGVpZ2h0OiAwO1xyXG4gICAgd2lkdGg6IDA7XHJcbiAgfVxyXG4gIC5jaGVja21hcmsge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiA0cHg7XHJcbiAgICByaWdodDogMDtcclxuICAgIGhlaWdodDogMjBweDtcclxuICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLXdoaXRlO1xyXG4gICAgYm9yZGVyOiAycHggc29saWQgJGNvbG9yLWdyYXktMTE7XHJcbiAgICBib3JkZXItcmFkaXVzOiAycHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgfVxyXG4gIC5jb250YWluZXIgaW5wdXQ6Y2hlY2tlZCB+IC5jaGVja21hcmsge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLXdoaXRlO1xyXG4gICAgYm9yZGVyOiAycHggc29saWQgJGNvbG9yLWdyZWVuLTU7XHJcbiAgfVxyXG4gIC5jaGVja21hcms6YWZ0ZXIge1xyXG4gICAgY29udGVudDogXCJcIjtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgfVxyXG4gIC5jb250YWluZXIgaW5wdXQ6Y2hlY2tlZCB+IC5jaGVja21hcms6YWZ0ZXIge1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgfVxyXG4gIC5jb250YWluZXIgLmNoZWNrbWFyazphZnRlciB7XHJcbiAgICBsZWZ0OiA1cHg7XHJcbiAgICB0b3A6IDJweDtcclxuICAgIHdpZHRoOiA0cHg7XHJcbiAgICBoZWlnaHQ6IDhweDtcclxuICAgIGJvcmRlcjogc29saWQgJGNvbG9yLWdyZWVuLTU7XHJcbiAgICBib3JkZXItd2lkdGg6IDAgMnB4IDJweCAwO1xyXG4gICAgdHJhbnNmb3JtOiByb3RhdGUoNDVkZWcpO1xyXG4gIH1cclxuICAudHh0IHtcclxuICAgIHdpZHRoOiA4MCU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gIH1cclxuICBpbnB1dFtkaXNhYmxlZF0gIHtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgfVxyXG59XHJcblxyXG4ubWFpbi1pbnB1dCB7XHJcbiAgbWFyZ2luLWJvdHRvbTogMTNweDtcclxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgJGNvbG9yLWdyYXktMTM7XHJcbiAgJi5uby1ib3JkZXIge1xyXG4gICAgYm9yZGVyOiAwO1xyXG4gIH1cclxuICAmLm5hdmJhci1pbnB1dCB7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBib3JkZXI6IDA7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMDtcclxuICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgfVxyXG4gIGFwcC1pbnB1dCB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcbiAgXHJcblxyXG4gIFxyXG5cclxuICAubWFpbi1pbnB1dC1jb250YWluZXIge1xyXG4gICAgbWFyZ2luLXRvcDogMTNweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgLmlucHV0LXRpdGxlIHtcclxuICAgICAgbWluLWhlaWdodDogMThweDtcclxuICAgICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgICAgJi5yZXF1aXJlZCB7XHJcbiAgICAgICAgJjo6YWZ0ZXIge1xyXG4gICAgICAgICAgY29udGVudDogXCIgKlwiO1xyXG4gICAgICAgICAgY29sb3I6IHJlZDtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIC5pbnB1dC1jb250YWluZXIge1xyXG4gICAgICBtYXJnaW4tdG9wOiA2cHg7XHJcbiAgICAgIC5pbnB1dCB7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gICAgICAgIC13ZWJraXQtYXBwZWFyYW5jZTogbm9uZTtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3ItZ3JheS0xNztcclxuICAgICAgICBib3gtc2hhZG93OiAwIDAgMTBweCAwIHJnYmEoMCwgMCwgMCwgMC4wMik7XHJcbiAgICAgICAgYm9yZGVyOiAwO1xyXG4gICAgICAgIGhlaWdodDogNDhweDtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBwYWRkaW5nOiAwIDE2cHg7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XHJcbiAgICAgICAgJjo6cGxhY2Vob2xkZXIge1xyXG4gICAgICAgICAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gICAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgICAgbGV0dGVyLXNwYWNpbmc6IDAuMnB4O1xyXG4gICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgICAgICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XHJcbiAgICAgICAgICAtd2Via2l0LWxpbmUtY2xhbXA6IDE7XHJcbiAgICAgICAgICAvKiBhdXRvcHJlZml4ZXI6IGlnbm9yZSBuZXh0ICovXHJcbiAgICAgICAgICAtd2Via2l0LWJveC1vcmllbnQ6IHZlcnRpY2FsO1xyXG4gICAgICAgICAgY29sb3I6ICRjb2xvci1ncmF5LTE1IDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgJi53aXRoLWxvZ28ge1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAmLmxlZnQge1xyXG4gICAgICAgICAgLmlucHV0IHtcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiA0MnB4O1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgLmxlZnQtbG9nbyB7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgd2lkdGg6IDIwcHg7XHJcbiAgICAgICAgICAgIGhlaWdodDogMjBweDtcclxuICAgICAgICAgICAgdG9wOiA1cHg7XHJcbiAgICAgICAgICAgIGxlZnQ6IDE2cHg7XHJcbiAgICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKDAsIDUwJSk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgICYucmlnaHQge1xyXG4gICAgICAgICAgLmlucHV0IHtcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4IDQ4cHggMCAxNnB4O1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgLnJpZ2h0LWxvZ28ge1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIHJpZ2h0OiAxNnB4O1xyXG4gICAgICAgICAgICB0b3A6IDIwJTtcclxuICAgICAgICAgICAgaW1nIHtcclxuICAgICAgICAgICAgICB3aWR0aDogMTZweDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuICAuZXJyb3ItbWVzc2FnZS1jb250YWluZXIgIHtcclxuICAgIGhlaWdodDogMTdweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgfVxyXG4gIC5lcnJvci1tZXNzYWdlIHtcclxuICAgIHBhZGRpbmctdG9wOiA2cHg7XHJcbiAgICBjb2xvcjogJGNvbG9yLW9yYW5nZSAhaW1wb3J0YW50O1xyXG4gICAgaGVpZ2h0OiAxN3B4O1xyXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBzcGFuIHtcclxuICAgICAgaGVpZ2h0OiAxN3B4O1xyXG4gICAgfVxyXG4gIH1cclxuICAuaW5saW5lLWlucHV0IHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAubWFpbi1pbnB1dC1jb250YWluZXIge1xyXG4gICAgICAmLmxlZnQtaXRlbSB7XHJcbiAgICAgICAgcGFkZGluZy1yaWdodDogOHB4O1xyXG4gICAgICB9XHJcbiAgICAgICYucmlnaHQtaXRlbSB7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiA4cHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgICYuc3BlY2lhbC13aWR0aC1pdGVtIHtcclxuICAgICAgLm1haW4taW5wdXQtY29udGFpbmVyIHtcclxuICAgICAgICBmbGV4LXNocmluazogMDtcclxuICAgICAgfVxyXG4gICAgICAubGVmdC1pdGVtIHtcclxuICAgICAgICB3aWR0aDogMTMwcHg7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICB9XHJcbiAgICAgIC5yaWdodC1pdGVtIHtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgYXBwLWJ1dHRvbiB7XHJcbiAgICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4gIC5pbnB1dC1jb250YWluZXIge1xyXG4gICAgbWFyZ2luLXRvcDogNnB4O1xyXG5cclxuICB9XHJcbiAgLnRleHRhcmVhLWNvbnRhaW5lciB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gICAgbWFyZ2luLXRvcDogNnB4O1xyXG4gIH1cclxuICAudGV4dGFyZWEge1xyXG4gICAgbWluLWhlaWdodDogMTIwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1ncmF5LTE3O1xyXG4gICAgYm94LXNoYWRvdzogMCAwIDEwcHggMCByZ2JhKDAsIDAsIDAsIDAuMDIpO1xyXG4gICAgYm9yZGVyOiAwO1xyXG4gICAgcmVzaXplOiBub25lO1xyXG4gICAgcGFkZGluZzogMTZweDtcclxuICAgIG1heC1oZWlnaHQ6IDIxMHB4O1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgJjo6cGxhY2Vob2xkZXIge1xyXG4gICAgICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWYgIWltcG9ydGFudDtcclxuICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgICAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgbGV0dGVyLXNwYWNpbmc6IDAuMnB4O1xyXG4gICAgICBjb2xvcjogcmdiKDE3NywgMTg4LCAxOTgpO1xyXG4gICAgfVxyXG4gICAgJi5ub01heGhlaWdodCB7XHJcbiAgICAgIG1heC1oZWlnaHQ6IG5vbmU7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuLy9jb21tZW50IGFuZCBzZW5kIGlucHV0XHJcbi5zZW5kLWlucHV0LWNvbnRhaW5lciB7XHJcbiAgcGFkZGluZzogMTJweCAyMHB4O1xyXG4uc2VuZC1pbnB1dC1pdGVtIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgbWF4LWhlaWdodDogMTM4cHg7XHJcbiAgaGVpZ2h0OiA0NHB4O1xyXG4gICAgLnNlbmQtaW5wdXQge1xyXG4gICAgICBoZWlnaHQ6IDQ0cHggIDtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLWdyYXktMTc7XHJcbiAgICAgIHJlc2l6ZTogbm9uZTtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgIGJvcmRlcjogMDtcclxuICAgICAgcGFkZGluZzogMTNweDtcclxuICAgICAgcGFkZGluZy1sZWZ0OiA0NnB4O1xyXG4gICAgICBwYWRkaW5nLXJpZ2h0OiA1NXB4O1xyXG4gICAgICBib3gtc2hhZG93OiAwIDAgMTBweCAwIHJnYmEoMCwwLDAsMC4wMik7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICAgICAgbWF4LWhlaWdodDogMTM4cHg7XHJcbiAgICAgICY6OnBsYWNlaG9sZGVye1xyXG4gICAgICAgIGNvbG9yOiAkY29sb3ItZ3JheS0xNTtcdFxyXG4gICAgICAgIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcdFxyXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcdFxyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHRcclxuICAgICAgICBsZXR0ZXItc3BhY2luZzogMC4ycHg7XHRcclxuICAgICAgICBsaW5lLWhlaWdodDogMTlweDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgLnNlbmQtaW1hZ2Uge1xyXG4gICAgICB3aWR0aDogMzJweDtcclxuICAgICAgaGVpZ2h0OiAzMnB4O1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIHRvcDogNnB4O1xyXG4gICAgICBsZWZ0OiA3cHg7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDdweDtcclxuICAgICAgbWF4LWhlaWdodDogMTM4cHg7XHJcbiAgICB9XHJcbiAgICAmLmhlaWdodC02MCB7XHJcbiAgICAgIGhlaWdodDogNTBweDtcclxuICAgICAgb3ZlcmZsb3c6IGF1dG87XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbi5sYWJlbC1yZXF1aXJlZCB7XHJcbiAgJjo6YWZ0ZXIge1xyXG4gICAgY29udGVudDogXCIgICpcIjtcclxuICAgIGNvbG9yOiByZWQ7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBtYXJnaW4tbGVmdDogMnB4O1xyXG4gIH1cclxufVxyXG5cclxuIiwiLmludml0ZS1jb250YWluZXIge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci13aGl0ZTtcclxuICBwYWRkaW5nOiAxMHB4IDIwcHg7XHJcbiAgcGFkZGluZy10b3A6IDA7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB6LWluZGV4OiAxMDtcclxuXHJcbiAgLnRpdGxlIHtcclxuICB9XHJcbiAgLnN1YnRpdGxlIHtcclxuICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgfVxyXG4gIC5pdGVtLWNvbnRhaW5lciB7XHJcbiAgICBtYXJnaW4tdG9wOiAxOHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAmLmNvbHVtbiB7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIH1cclxuICAgIC8vICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xyXG4gICAgLml0ZW0ge1xyXG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gICAgICAvLyAgIHBhZGRpbmc6IDAgMTNweDtcclxuICAgICAgbWFyZ2luLXRvcDogMjVweDtcclxuICAgICAgaW1nIHtcclxuICAgICAgICB3aWR0aDogNTVweDtcclxuICAgICAgICBoZWlnaHQ6IDU1cHg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICAgICAgICBvYmplY3QtZml0OiBjb3ZlcjtcclxuICAgICAgfVxyXG4gICAgICAudGl0bGUge1xyXG4gICAgICAgIHdpZHRoOiAxMDdweDtcclxuICAgICAgICAvLyBwYWRkaW5nOiAwIDEzcHg7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogOXB4O1xyXG4gICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICAgICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XHJcbiAgICAgICAgLXdlYmtpdC1saW5lLWNsYW1wOiAyO1xyXG4gICAgICAgIC8qIGF1dG9wcmVmaXhlcjogaWdub3JlIG5leHQgKi9cclxuICAgICAgICAtd2Via2l0LWJveC1vcmllbnQ6IHZlcnRpY2FsO1xyXG4gICAgICB9XHJcbiAgICAgIC5zdWJ0aXRsZSB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogM3B4O1xyXG4gICAgICB9XHJcbiAgICAgICYuYmlnLWl0ZW0ge1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDA7XHJcbiAgICAgICAgaW1nIHtcclxuICAgICAgICAgIHdpZHRoOiA4MHB4O1xyXG4gICAgICAgICAgaGVpZ2h0OiA4MHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuYmFkZ2V7XHJcbiAgICAgICAgICAgIHRvcDogNjdweDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgLmJhZGdlIHtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgdG9wOiA0MnB4O1xyXG4gICAgICAgIGxlZnQ6IDQzcHg7XHJcbiAgICAgICAgaW1nIHtcclxuICAgICAgICAgIHdpZHRoOiAyNHB4O1xyXG4gICAgICAgICAgaGVpZ2h0OiAyNHB4O1xyXG4gICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgJGNvbG9yLXdoaXRlO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuICAuZW5kLWl0ZW0ge1xyXG4gICAgbWFyZ2luLXRvcDogMzBweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxufVxyXG4iLCJAZm9udC1mYWNlIHtcclxuICAgIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJztcclxuICAgIHNyYzogdXJsKCcvYXNzZXRzL2ZvbnQvT3BlblNhbnMtUmVndWxhci50dGYnKVxyXG4gIH1cclxuICBAZm9udC1mYWNlIHtcclxuICAgIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJztcclxuICAgIHNyYzogdXJsKCcvYXNzZXRzL2ZvbnQvT3BlblNhbnMtU2VtaUJvbGQudHRmJyk7XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG4gIH0iLCIvLyBzcGFjaW5nXHJcblxyXG4uY29udGVudC1zcGFjZSB7XHJcbiAgLS1wYWRkaW5nLXN0YXJ0OiAyMHB4O1xyXG4gIC0tcGFkZGluZy1lbmQ6IDIwcHg7XHJcbiAgcGFkZGluZy1sZWZ0OiAyMHB4O1xyXG4gIHBhZGRpbmctcmlnaHQ6IDIwcHg7XHJcbiAgJi5ib3R0b20ge1xyXG4gICAgLS1wYWRkaW5nLWJvdHRvbTogMjBweDtcclxuICAgIHBhZGRpbmctYm90dG9tOiAyMHB4O1xyXG4gIH1cclxuICAvLyAmLmJvdHRvbS0xMCB7XHJcbiAgLy8gICAtLXBhZGRpbmctYm90dG9tOiAxMHB4O1xyXG4gIC8vICAgcGFkZGluZy1ib3R0b206IDEwcHg7XHJcbiAgLy8gfVxyXG4gICYuYm90dG9tLTMwIHtcclxuICAgIC0tcGFkZGluZy1ib3R0b206IDMwcHg7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMzBweDtcclxuICB9XHJcbiAgJi50b3Age1xyXG4gICAgLS1wYWRkaW5nLXRvcDogMjBweDtcclxuICAgIHBhZGRpbmctdG9wOiAyMHB4O1xyXG4gIH1cclxuICAmLnRvcC0xMCB7XHJcbiAgICAtLXBhZGRpbmctdG9wOiAxMHB4O1xyXG4gICAgcGFkZGluZy10b3A6IDEwcHg7XHJcbiAgfVxyXG4gICYudG9wLTIwIHtcclxuICAgIC0tcGFkZGluZy10b3A6IDIwcHg7XHJcbiAgICBwYWRkaW5nLXRvcDogMjBweDtcclxuICB9XHJcbiAgJi50b3AtMiB7XHJcbiAgICAtLXBhZGRpbmctdG9wOiAycHg7XHJcbiAgICBwYWRkaW5nLXRvcDogMnB4O1xyXG4gIH1cclxuICAmLnRvcC0zMCB7XHJcbiAgICAtLXBhZGRpbmctdG9wOiAzMHB4O1xyXG4gICAgcGFkZGluZy10b3A6IDMwcHg7XHJcbiAgfVxyXG4gICYucmlnaHQtMCB7XHJcbiAgICAtLXBhZGRpbmctcmlnaHQ6IDBweDtcclxuICAgIHBhZGRpbmctcmlnaHQ6IDBweDtcclxuICB9XHJcbiAgJi5jYXJkLWNvbnRhaW5lci1zZXBlcmF0b3Ige1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLXdoaXRlLWdyYXk7IFxyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIFxyXG4gICAgJi5zcGVjaWFsLW1hcmdpbiB7XHJcbiAgICAgIG1hcmdpbi10b3A6IC03cHg7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG4vL21hcmdpblxyXG4ubm8tbWFyZ2luIHtcclxuICBtYXJnaW46IDA7XHJcbn1cclxuXHJcbi8vdG9wIG1hcmdpblxyXG4ubmVnYXRpdmUtbWFyZ2luLTcge1xyXG4gIG1hcmdpbi10b3A6IC03cHg7XHJcbn1cclxuLm5vLW1hcmdpbi10b3Age1xyXG4gIG1hcmdpbi10b3A6IDBweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ubWFyZ2luLXRvcC0xe1xyXG4gIG1hcmdpbi10b3A6IDFweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ubWFyZ2luLXRvcC0ye1xyXG4gIG1hcmdpbi10b3A6IDJweCAhaW1wb3J0YW50O1xyXG59XHJcbi5tYXJnaW4tdG9wLTZ7XHJcbiAgbWFyZ2luLXRvcDogNnB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuLm1hcmdpbi10b3AtN3tcclxuICBtYXJnaW4tdG9wOiA3cHggIWltcG9ydGFudDtcclxufVxyXG4ubWFyZ2luLXRvcC0xMCB7XHJcbiAgbWFyZ2luLXRvcDogMTBweDtcclxufVxyXG4ubWFyZ2luLXRvcC0xMiB7XHJcbiAgbWFyZ2luLXRvcDogMTJweCAhaW1wb3J0YW50O1xyXG59XHJcbi5tYXJnaW4tdG9wLTEzIHtcclxuICBtYXJnaW4tdG9wOiAxM3B4ICFpbXBvcnRhbnQ7XHJcbn1cclxuLm1hcmdpbi10b3AtMTYge1xyXG4gIG1hcmdpbi10b3A6IDE2cHg7XHJcbn1cclxuLm1hcmdpbi10b3AtMTgge1xyXG4gIG1hcmdpbi10b3A6IDE4cHg7XHJcbn1cclxuLm1hcmdpbi10b3AtMjAge1xyXG4gIG1hcmdpbi10b3A6IDIwcHggIWltcG9ydGFudDtcclxufVxyXG4ubWFyZ2luLXRvcC0zMCB7XHJcbiAgbWFyZ2luLXRvcDogMzBweCAhaW1wb3J0YW50O1xyXG59XHJcbi5tYXJnaW4tdG9wLTQwIHtcclxuICBtYXJnaW4tdG9wOiA0MHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuLm1hcmdpbi10b3AtNTAge1xyXG4gIG1hcmdpbi10b3A6IDUwcHg7IFxyXG59XHJcblxyXG5cclxuLy9idXR0b20gbWFyZ2luXHJcbi5uby1tYXJnaW4tYm90dG9tIHtcclxuICBtYXJnaW4tYm90dG9tOiAwICFpbXBvcnRhbnQ7XHJcbn1cclxuLm1hcmdpbi1ib3R0b20tNntcclxuICBtYXJnaW4tYm90dG9tOiA2cHg7XHJcbn1cclxuLm1hcmdpbi1ib3R0b20tMTB7XHJcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxufVxyXG4ubWFyZ2luLWJvdHRvbS0xMiB7XHJcbiAgbWFyZ2luLWJvdHRvbTogMTJweDtcclxufVxyXG4ubWFyZ2luLWJvdHRvbS0xMyB7XHJcbiAgbWFyZ2luLWJvdHRvbTogMTNweDtcclxufVxyXG4ubWFyZ2luLWJvdHRvbS0xNCB7XHJcbiAgbWFyZ2luLWJvdHRvbTogMTRweCAhaW1wb3J0YW50O1xyXG59XHJcbi5tYXJnaW4tYm90dG9tLTE3IHtcclxuICBtYXJnaW4tYm90dG9tOiAxN3B4O1xyXG59XHJcbi5tYXJnaW4tYm90dG9tLTIwIHtcclxuICBtYXJnaW4tYm90dG9tOiAyMHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuLm1hcmdpbi1ib3R0b20tMzAge1xyXG4gIG1hcmdpbi1ib3R0b206IDMwcHggIWltcG9ydGFudDtcclxufVxyXG4ubWFyZ2luLWJvdHRvbS00MCB7XHJcbiAgbWFyZ2luLWJvdHRvbTogNDBweCAhaW1wb3J0YW50O1xyXG59XHJcbi5tYXJnaW4tYm90dG9tLTUwIHtcclxuICBtYXJnaW4tYm90dG9tOiA1MHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi8vIHJpZ2h0IG1hcmdpblxyXG4ubWFyZ2luLXJpZ2h0LTE0IHtcclxuICBtYXJnaW4tcmlnaHQ6IDE0cHg7XHJcbn1cclxuXHJcbi5tYXJnaW4tcmlnaHQtOCB7XHJcbiAgbWFyZ2luLXJpZ2h0OiA4cHg7XHJcbn1cclxuXHJcbi8vIHBhZGRpbmdcclxuLm5vLXBhZGRpbmcge1xyXG4gIHBhZGRpbmc6IDA7XHJcbn1cclxuLnBhZGRpbmctdG9wLTAge1xyXG4gIHBhZGRpbmctdG9wOiAwICFpbXBvcnRhbnQ7XHJcbn1cclxuLnBhZGRpbmctdG9wLTEge1xyXG4gIHBhZGRpbmctdG9wOiAxcHggIWltcG9ydGFudDtcclxufVxyXG4ucGFkZGluZy10b3AtMyB7XHJcbiAgcGFkZGluZy10b3A6IDNweCAhaW1wb3J0YW50O1xyXG59XHJcbi5wYWRkaW5nLXRvcC00IHtcclxuICBwYWRkaW5nLXRvcDogNHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuLnBhZGRpbmctdG9wLTEwIHtcclxuICAtLXBhZGRpbmctdG9wOiAxMHB4ICFpbXBvcnRhbnQ7XHJcbiAgcGFkZGluZy10b3A6IDEwcHggIWltcG9ydGFudDtcclxufVxyXG4ucGFkZGluZy10b3AtMjAge1xyXG4gIC0tcGFkZGluZy10b3A6IDIwcHggIWltcG9ydGFudDtcclxuICBwYWRkaW5nLXRvcDogMjBweCAhaW1wb3J0YW50O1xyXG59XHJcbi5wYWRkaW5nLXRvcC0zMCB7XHJcbiAgcGFkZGluZy10b3A6IDMwcHg7XHJcbn1cclxuXHJcbi5wYWRkaW5nLWJvdHRvbS00e1xyXG4gIHBhZGRpbmctYm90dG9tOiA0cHg7XHJcbn1cclxuLnBhZGRpbmctYm90dG9tLTggIHtcclxuICBwYWRkaW5nLWJvdHRvbTogOHB4O1xyXG59XHJcbi5wYWRkaW5nLWJvdHRvbS0xMCB7XHJcbiAgcGFkZGluZy1ib3R0b206IDEwcHg7XHJcbiAgLS1wYWRkaW5nLWJvdHRvbTogMTBweDtcclxufVxyXG4ucGFkZGluZy1ib3R0b20tMTMge1xyXG4gIHBhZGRpbmctYm90dG9tOiAxM3B4O1xyXG59XHJcbi5wYWRkaW5nLWJvdHRvbS0xNiAge1xyXG4gIHBhZGRpbmctYm90dG9tOiAxNnB4O1xyXG59XHJcbi5wYWRkaW5nLWJvdHRvbS0yMCB7XHJcbiAgcGFkZGluZy1ib3R0b206IDIwcHg7XHJcbn1cclxuLnBhZGRpbmctYm90dG9tLTMwIHtcclxuICBwYWRkaW5nLWJvdHRvbTogMzBweDtcclxufVxyXG4ucGFkZGluZy1ib3R0b20tNzAge1xyXG4gIC0tcGFkZGluZy1ib3R0b206IDcwcHg7XHJcbn1cclxuLnBhZGRpbmctYm90dG9tLTc5IHtcclxuICAtLXBhZGRpbmctYm90dG9tOiA3OXB4O1xyXG59XHJcbi5wYWRkaW5nLWJvdHRvbS0xMDAge1xyXG4gIC0tcGFkZGluZy1ib3R0b206IDEwMHB4O1xyXG59IiwiLy8gaW9uLXNlZ21lbnQtc2xpZGVzXHJcbi5zZWdtZW50LWNvbnRhaW5lciB7XHJcbiAgLy9jb250YWluZXIgb2YgIHRoZSBzZWdtZW50IGJhclxyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLXdoaXRlO1xyXG4gIG1hcmdpbi10b3A6IDVweDtcclxufVxyXG5pb24tc2VnbWVudCB7XHJcbiAgbWluLWhlaWdodDogNDFweDtcclxuICBoZWlnaHQgOiA0MXB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci13aGl0ZTtcclxuICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xyXG4gIG1hcmdpbi10b3A6IC00cHg7XHJcblxyXG4gICYud2l0aC1pY29uIHtcclxuICAgIGhlaWdodDogNjhweDtcclxuICAgIGlvbi1zZWdtZW50LWJ1dHRvbiB7XHJcbiAgICAgIGhlaWdodDogNTFweDtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbmlvbi1zZWdtZW50LWJ1dHRvbiB7XHJcbiAgbWluLWhlaWdodDogMzhweDtcclxuICBvcGFjaXR5OiAxO1xyXG4gIGxldHRlci1zcGFjaW5nOiAwLjJweDtcclxuICBsaW5lLWhlaWdodDogMjBweDtcclxuICBmb250LXdlaWdodDogNjAwO1xyXG4gIGZvbnQtc2l6ZTogMTVweDtcclxuICAtLWJhY2tncm91bmQtY2hlY2tlZDogI2ZmZmZmZjtcclxuICAtLWluZGljYXRvci1jb2xvcjogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcclxuICAtLWluZGljYXRvci1jb2xvci1jaGVja2VkOiAjMDBkMmJkICFpbXBvcnRhbnQ7XHJcbiAgLS1jb2xvcjogI2IxYmNjNjtcclxuICAtLWNvbG9yLWNoZWNrZWQ6ICMwMGQyYmQ7XHJcbiAgLS1yaXBwbGUtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gIGJvcmRlcjogMDtcclxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgJGNvbG9yLWdyYXktMTY7XHJcbiAgbWluLXdpZHRoOiAyNSU7XHJcbiAgbWF4LXdpZHRoOiBub25lO1xyXG4gIGJvcmRlci1yYWRpdXM6IDA7XHJcbiAgaGVpZ2h0OiAxMDAlICFpbXBvcnRhbnQ7XHJcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbn1cclxuXHJcbi5mb3VyLXNlZ21lbnQge1xyXG4gIGlvbi1zZWdtZW50LWJ1dHRvbiB7XHJcbiAgICB3aWR0aDogMjUlO1xyXG4gIH1cclxufVxyXG4udGhyZWUtc2VnbWVudCB7XHJcbiAgaW9uLXNlZ21lbnQtYnV0dG9uIHtcclxuICAgIHdpZHRoOiAzMy4zMyU7XHJcbiAgfVxyXG59XHJcbi50d28tc2VnbWVudCB7XHJcbiAgaW9uLXNlZ21lbnQtYnV0dG9uIHtcclxuICAgIHdpZHRoOiA1MCU7XHJcbiAgfVxyXG59XHJcbi8vIGRpcmVjdG9yeSBwYWdlIFxyXG5cclxuLmRpcmVjdG9yeSB7XHJcbiAgbWFyZ2luLXRvcCA6IDA7XHJcbiAgLnNlZ21lbnQtaWNvbiB7XHJcbiAgICBoZWlnaHQ6IDI4cHg7XHJcbiAgICB3aWR0aDogMjhweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDZweDtcclxuICAgIG1hcmdpbi10b3A6IDVweCAhaW1wb3J0YW50O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLWdyYXktMjI7XHJcbiAgICAtd2Via2l0LW1hc2stcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgICAtd2Via2l0LW1hc2stc2l6ZTogY29udGFpbjtcclxuICAgICYuZmlyc3Qge1xyXG4gICAgICAtd2Via2l0LW1hc2staW1hZ2U6IHVybChcIi4vYXNzZXRzL2ljb25zL2RpYXNwb3JhSWNvbi9QZW9wbGVfQWN0aXZlLnN2Z1wiKTtcclxuICAgIH1cclxuICAgICYuc2Vjb25kIHtcclxuICAgICAgLXdlYmtpdC1tYXNrLWltYWdlOiB1cmwoXCIuL2Fzc2V0cy9pY29ucy9kaWFzcG9yYUljb24vTmV0d29ya3NfQWN0aXZlLnN2Z1wiKTtcclxuICAgIH1cclxuICAgICYudGhpcmQge1xyXG4gICAgICAtd2Via2l0LW1hc2staW1hZ2U6IHVybChcIi4vYXNzZXRzL2ljb25zL2RpYXNwb3JhSWNvbi9CdXNpbmVzc19JbmFjdGl2ZS5zdmdcIik7XHJcbiAgICB9XHJcbiAgfVxyXG4gIFxyXG4gIC5zZWFyY2gtc2VnbWVudC1jaGVja2VkIHtcclxuICAgIGJvcmRlcjogMDtcclxuICAgIHBhZGRpbmctdG9wOiAxcHg7XHJcbiAgICAtLWluZGljYXRvci1jb2xvci1jaGVja2VkOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMnB4IHNvbGlkICRjb2xvci1ncmVlbi01O1xyXG4gICAgYmFja2dyb3VuZDogJGNvbG9yLXdoaXRlO1xyXG4gICAgJi5maXJzdCB7XHJcbiAgICAgIGNvbG9yOiAkY29sb3ItZ3JlZW4tNSAhaW1wb3J0YW50O1xyXG4gICAgICAuc2VnbWVudC1pY29uIHtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3ItZ3JlZW4tNTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgJi5zZWNvbmQge1xyXG4gICAgICBjb2xvcjogJGNvbG9yLWdyZWVuLTUgO1xyXG4gICAgICAuc2VnbWVudC1pY29uIHtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3ItZ3JlZW4tNTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgJi50aGlyZCB7XHJcbiAgICAgIGNvbG9yOiAgJGNvbG9yLWdyZWVuLTUgO1xyXG4gICAgICAuc2VnbWVudC1pY29uIHtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3ItZ3JlZW4tNTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgaW9uLXNlZ21lbnQge1xyXG4gICAgbWluLWhlaWdodDogMzlweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci13aGl0ZTtcclxuICAgIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XHJcbiAgICAmLndpdGgtaWNvbiB7XHJcbiAgICAgIGhlaWdodDogNjBweDtcclxuICAgICAgaW9uLXNlZ21lbnQtYnV0dG9uIHtcclxuICAgICAgICBoZWlnaHQ6IDUxcHg7XHJcbiAgICAgICAgXHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgaW9uLXNlZ21lbnQtYnV0dG9uIHtcclxuICAgIG1hcmdpbi10b3A6IDBweDtcclxuICB9XHJcbn1cclxuXHJcbi5zZWFyY2hTZWdtZW50IHtcclxuICBtYXJnaW4tdG9wOiAwO1xyXG4gIC5zZWdtZW50LWljb24ge1xyXG4gICAgaGVpZ2h0OiAyOHB4O1xyXG4gICAgd2lkdGg6IDI4cHg7XHJcbiAgICBtYXJnaW4tdG9wOiAzcHggIWltcG9ydGFudDtcclxuICAgIG1hcmdpbi1ib3R0b206IDZweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIC13ZWJraXQtbWFzay1yZXBlYXQ6IG5vLXJlcGVhdCAhaW1wb3J0YW50O1xyXG4gICAgLXdlYmtpdC1tYXNrLXNpemU6IGNvbnRhaW4gIWltcG9ydGFudDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1ncmF5LTIyO1xyXG5cclxuICAgICYuZmlyc3Qge1xyXG4gICAgICAtd2Via2l0LW1hc2staW1hZ2U6IHVybChcIi4vYXNzZXRzL2ljb25zL2RpYXNwb3JhSWNvbi9FdmVyeXRoaW5nX0luYWN0aXZlLnN2Z1wiKTtcclxuICAgIH1cclxuXHJcbiAgICAmLnNlY29uZCB7XHJcbiAgICAgIC13ZWJraXQtbWFzay1pbWFnZTogdXJsKFwiLi9hc3NldHMvaWNvbnMvZGlhc3BvcmFJY29uL1Blb3BsZV9BY3RpdmUuc3ZnXCIpO1xyXG4gICAgfVxyXG5cclxuICAgICYudGhpcmQge1xyXG4gICAgICAtd2Via2l0LW1hc2staW1hZ2U6IHVybChcIi4vYXNzZXRzL2ljb25zL2RpYXNwb3JhSWNvbi9OZXR3b3Jrc19BY3RpdmUuc3ZnXCIpO1xyXG4gICAgfVxyXG5cclxuICAgICYuZm91cnRoIHtcclxuICAgICAgLXdlYmtpdC1tYXNrLWltYWdlOiB1cmwoXCIuL2Fzc2V0cy9pY29ucy9kaWFzcG9yYUljb24vQnVzaW5lc3NfSW5hY3RpdmUuc3ZnXCIpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLnNlYXJjaC1zZWdtZW50LWNoZWNrZWQge1xyXG4gICAgYm9yZGVyOiAwO1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMnB4IHNvbGlkICRjb2xvci1ncmVlbi01O1xyXG4gICAgLS1pbmRpY2F0b3ItY29sb3ItY2hlY2tlZDogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcclxuICAgIHBhZGRpbmctdG9wOiAxcHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAkY29sb3Itd2hpdGU7XHJcblxyXG4gICAgLmZpcnN0IHtcclxuICAgICAgY29sb3I6ICAkY29sb3ItZ3JlZW4tNSA7XHJcbiAgICB9XHJcblxyXG4gICAgLnNlY29uZCB7XHJcbiAgICAgIGNvbG9yOiAkY29sb3ItZ3JlZW4tNTtcclxuICAgIH1cclxuXHJcbiAgICAudGhpcmQge1xyXG4gICAgICBjb2xvcjogJGNvbG9yLWdyZWVuLTUgO1xyXG4gICAgfVxyXG5cclxuICAgIC5mb3VydGgge1xyXG4gICAgICBjb2xvcjogICRjb2xvci1ncmVlbi01IDtcclxuICAgIH1cclxuICAgIFxyXG4gICAgJi5maXJzdCB7XHJcbiAgICAgIGJvcmRlci1jb2xvcjogICRjb2xvci1ncmVlbi01IDtcclxuICAgIH1cclxuICAgICYuc2Vjb25kIHtcclxuICAgICAgYm9yZGVyLWNvbG9yOiAkY29sb3ItZ3JlZW4tNTtcclxuICAgIH1cclxuICAgICYudGhpcmQge1xyXG4gICAgICBib3JkZXItY29sb3I6ICRjb2xvci1ncmVlbi01IDtcclxuICAgIH1cclxuICAgICYuZm91cnRoIHtcclxuICAgICAgYm9yZGVyLWNvbG9yOiAgJGNvbG9yLWdyZWVuLTUgO1xyXG4gICAgfVxyXG4gICAgLnNlZ21lbnQtaWNvbiB7XHJcbiAgICAgIGJhY2tncm91bmQ6ICRjb2xvci1ncmVlbi01O1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuIiwiLmdhbGxlcnkge1xyXG4gIHBhZGRpbmc6IDFweDtcclxuICAuZ2FsbGVyeS1jb250YWluZXIge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gICAgZmxleC13cmFwOiB3cmFwO1xyXG4gICAgLml0ZW0tY29udGFpbmVyIHtcclxuICAgICAgcGFkZGluZzogMXB4O1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgZmxleC1zaHJpbms6IDA7XHJcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xyXG4gICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gICAgICAuaXRlbSB7XHJcbiAgICAgICAgLy8gYmFja2dyb3VuZC1pbWFnZTogdXJsKGh0dHBzOi8vaW1hZ2VzLnBleGVscy5jb20vcGhvdG9zLzQxNDYxMi9wZXhlbHMtcGhvdG8tNDE0NjEyLmpwZWc/YXV0bz1jb21wcmVzcyZjcz10aW55c3JnYiZkcHI9MSZ3PTUwMCk7XHJcbiAgICAgICAgcGFkZGluZy10b3A6IDk5JTtcclxuICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gICAgICB9XHJcbiAgICAgIC5pY29uIHtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgd2lkdGg6IDUwcHg7XHJcbiAgICAgICAgdG9wOiA1MCU7XHJcbiAgICAgICAgbGVmdDogNTAlO1xyXG4gICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbi5nYWxsZXJ5LXZpZXctaW1hZ2Uge1xyXG4gIGhlaWdodDogMTAwJTtcclxuICBiYWNrZ3JvdW5kOiAkY29sb3ItYmxhY2s7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgLnZpZGVvLXBsYXllciB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcbiAgLmdhbGxlcnktdmlldy1pbWFnZS1jb250YWluZXIge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIHBhZGRpbmctYm90dG9tOiAyMHB4O1xyXG4gIH1cclxuICAuc2xpZGVyX192aWRlby1jb250YWluZXIge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogNTAlO1xyXG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoMCwgLTUwJSk7XHJcbiAgICAuc2xpZGVyX19pbWFnZSB7XHJcbiAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLnZpZGVvIHtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICB0b3A6IDUwJTtcclxuICAgICAgbGVmdDogNTAlO1xyXG4gICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuICAgICAgbWF4LXdpZHRoOiAxMDBweDtcclxuICAgIH1cclxuICB9XHJcbn1cclxuLm1lZGlhLWxpc3QtaXRlbSB7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLWJsYWNrO1xyXG4gIHBhZGRpbmctdG9wOiA1cHg7XHJcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIHJnYmEoMjE0LCAyMTQsIDIzMywgMC40Nik7XHJcbiAgLmxpc3QtaXRlbS1yb3ctdGl0bGUtbGFyZ2Uge1xyXG4gICAgY29sb3I6ICRjb2xvci13aGl0ZSAhaW1wb3J0YW50O1xyXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICB9XHJcbiAgLmxpc3QtaXRlbXMtbGVmdCB7XHJcbiAgICBoZWlnaHQ6IDA7XHJcbiAgICB3aWR0aDogMDtcclxuICB9XHJcbiAgYXBwLWxpc3Qge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG59XHJcbi5zbGlkZXJfX2NhcHRpb24ge1xyXG4gIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLWJsYWNrO1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBib3R0b206IDA7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjUpO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIC50cnVuY2F0ZWRUZXh0SG9sZGVyIHtcclxuICAgIG1heC1oZWlnaHQ6IDYwdmg7XHJcbiAgICBvdmVyZmxvdzogc2Nyb2xsO1xyXG4gICAgcGFkZGluZzogMTVweCAyMHB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDIxcHg7XHJcbiAgfVxyXG59XHJcbi5pbWFnZVZpZXdlci1jb250YWluZXIge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGlvbi1mb290ZXIge1xyXG4gICAgJjo6YmVmb3JlIHtcclxuICAgICAgYmFja2dyb3VuZC1pbWFnZTogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgICBoZWlnaHQ6IDAgIWltcG9ydGFudDtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLWJsYWNrO1xyXG4gICAgICBjb250ZW50OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5jb250ZW50LXNwYWNlIHtcclxuICAgIGJhY2tncm91bmQ6ICRjb2xvci1ibGFjaztcclxuICB9XHJcbn1cclxuLmRldGFpbHNHYWxsZXJ5VHlwZSB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LXdyYXA6IHdyYXA7XHJcblxyXG4gIGFwcC1tZWRpYS1jYXJkIHtcclxuICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gICAgd2lkdGg6IDMzLjMzJTtcclxuICB9XHJcbn1cclxuIiwiaW9uLW1vZGFsLmFjdGlvbi1zaGVldC1zdHlsZSB7XHJcbiAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICBpb24tY29udGVudCB7XHJcbiAgICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gIH1cclxuICAuc2hlZXQge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLXdoaXRlO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYm90dG9tOiAwO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBwYWRkaW5nOiAyMHB4O1xyXG4gICAgcGFkZGluZy10b3A6IDEycHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxNnB4IDE2cHggMCAwO1xyXG4gICAgLnZlcnRpY2FsLXNoZWV0IHtcclxuICAgICAgLnZlcnRpY2FsLXNoZWV0LWl0ZW0ge1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBoZWlnaHQ6IDYwcHg7XHJcbiAgICAgICAgaW1nIHtcclxuICAgICAgICAgIG1hcmdpbi1yaWdodDogMThweDtcclxuICAgICAgICAgIHdpZHRoOiAyNHB4O1xyXG4gICAgICAgICAgaGVpZ2h0OiAyNHB4O1xyXG4gICAgICAgICAgZmxleC1zaHJpbms6IDA7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC50eHQge1xyXG4gICAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICRjb2xvci1ncmF5LTEzO1xyXG4gICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICB9XHJcbiAgICAgICAgJjpsYXN0LWNoaWxkIHtcclxuICAgICAgICAgICAgLnR4dCB7XHJcbiAgICAgICAgICAgICAgICBib3JkZXI6IDA7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgJi5tYWluLXNoZWV0IHtcclxuICAgICAgICAgIGhlaWdodDogNzRweDtcclxuICAgICAgICAgIGltZyB7XHJcbiAgICAgICAgICAgIHdpZHRoOiAzMnB4O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMycHg7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5vcGFjaXR5LXNoZWV0IHtcclxuICAgIGhlaWdodDogMTAwdmg7XHJcbiAgfVxyXG59XHJcbi5jYW5jZWwtc2hlZXQge1xyXG4gIG1hcmdpbi10b3A6IDEycHg7XHJcbn1cclxuIiwiLmJhbm5lci1idXR0b24tYnV0dG9uLXN1YnRpdGxlXHJcbntcclxuXHRmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcblx0Zm9udC13ZWlnaHQ6IDYwMDtcclxuXHR0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuXHRmb250LXNpemU6IDE0cHg7XHJcblx0bGV0dGVyLXNwYWNpbmc6IDAuMnB4O1xyXG5cdGNvbG9yOiByZ2JhKDE3NywgMTg4LCAxOTgsIDEpO1xyXG59XHJcbi5idXR0b24tc21hbGwtZ3JheVxyXG57XHJcblx0Zm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG5cdGZvbnQtd2VpZ2h0OiA2MDA7IFxyXG5cdHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG5cdGZvbnQtc2l6ZTogMTRweDtcclxuXHRsZXR0ZXItc3BhY2luZzogMC4ycHg7XHJcblx0Y29sb3I6ICM3MDcwNzA7XHJcblx0Zm9udC1zaXplOiAxMXB4O1xyXG59XHJcbi5idXR0b24tc21hbGwtZ3JlZW5cclxue1xyXG5cdGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuXHRmb250LXdlaWdodDogNjAwOyBcclxuXHR0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuXHRmb250LXNpemU6IDE0cHggIWltcG9ydGFudDtcclxuXHRsZXR0ZXItc3BhY2luZzogMC4ycHg7XHJcblx0Y29sb3I6ICMwMEQyQkQ7XHJcblx0Zm9udC1zaXplOiAxMXB4O1xyXG5cdCYuYmxvY2sge1xyXG5cdFx0ZGlzcGxheTogYmxvY2s7XHJcblx0fVxyXG59XHJcblxyXG4uYnV0dG9uLXNtYWxsLXdoaXRlXHJcbntcclxuXHRmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcblx0Zm9udC13ZWlnaHQ6IDYwMDtcclxuXHR0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuXHRmb250LXNpemU6IDE0cHg7XHJcblx0bGV0dGVyLXNwYWNpbmc6IDAuMnB4O1xyXG5cdGNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDEpO1xyXG59XHJcbi5lbXB0eS1zdGF0ZS1ibG9jay1idXR0b24tdGl0bGVcclxue1xyXG5cdGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuXHRmb250LXdlaWdodDogNjAwO1xyXG5cdHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG5cdGZvbnQtc2l6ZTogMTRweDtcclxuXHRsZXR0ZXItc3BhY2luZzogMHB4O1xyXG5cdGNvbG9yOiAjZmZmZmZmO1xyXG59XHJcbi5idXR0b24tbGFyZ2Utd2hpdGVcclxue1xyXG5cdGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuXHRmb250LXdlaWdodDogNjAwO1xyXG5cdHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG5cdGZvbnQtc2l6ZTogMTZweDtcclxuXHRsZXR0ZXItc3BhY2luZzogMC4ycHg7XHJcblx0bGluZS1oZWlnaHQ6IDIycHg7XHJcblx0Y29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMSk7XHJcbn1cclxuLmJhbm5lci1idXR0b24tYnV0dG9uLXRpdGxlXHJcbntcclxuXHRmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcblx0Zm9udC13ZWlnaHQ6IDYwMDtcclxuXHR0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuXHRmb250LXNpemU6IDE1cHg7XHJcblx0bGV0dGVyLXNwYWNpbmc6IDAuMnB4O1xyXG5cdGNvbG9yOiAjMDBkMmJkO1xyXG59XHJcbi5idXR0b24tbGFyZ2UtZ3JlZW5cclxue1xyXG5cdGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuXHRmb250LXdlaWdodDogNjAwO1xyXG5cdHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG5cdGZvbnQtc2l6ZTogMTZweDtcclxuXHRsZXR0ZXItc3BhY2luZzogMC4ycHg7XHJcblx0bGluZS1oZWlnaHQ6IDIycHg7XHJcblx0Y29sb3I6ICMwMGQyYmQ7XHJcbn1cclxuIC5pb3Mge1xyXG5cdC5idXR0b24tbGFyZ2UtZ3JlZW5cclxuXHR7XHJcblx0XHRmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWYgIWltcG9ydGFudDtcclxuXHRcdGZvbnQtd2VpZ2h0OiA1MDA7XHJcblx0XHR0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuXHRcdGZvbnQtc2l6ZTogMTZweDtcclxuXHRcdGxldHRlci1zcGFjaW5nOiAwLjJweDtcclxuXHRcdGxpbmUtaGVpZ2h0OiAyMnB4O1xyXG5cdFx0Y29sb3I6ICMwMGQyYmQ7XHJcblx0fVxyXG5cdC5iYW5uZXItYnV0dG9uLWJ1dHRvbi10aXRsZXtcclxuXHRmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcblx0Zm9udC13ZWlnaHQ6IDYwMDtcclxuXHR0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuXHRmb250LXNpemU6IDE1cHg7XHJcblx0bGV0dGVyLXNwYWNpbmc6IDAuMnB4O1xyXG5cdGNvbG9yOiAjMDBkMmJkO1xyXG59XHJcbi5lbXB0eS1zdGF0ZS1ibG9jay1idXR0b24tdGl0bGVcclxue1xyXG5cdGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZiAhaW1wb3J0YW50ICFpbXBvcnRhbnQ7XHJcblx0Zm9udC13ZWlnaHQ6IDYwMDtcclxuXHR0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuXHRmb250LXNpemU6IDE0cHg7XHJcblx0bGV0dGVyLXNwYWNpbmc6IDBweDtcclxuXHRjb2xvcjogI2ZmZmZmZjtcclxufVxyXG4gfVxyXG4vL2xhbmRpbmcgcGFnZSBcclxuXHJcbi8vIC5wYWdlLWhlYWRlci1wYWdlLXRpdGxlLWxhcmdlIHtcclxuLy8gICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4vLyAgICAgZm9udC1mYW1pbHk6IFwiQXZlbmlyTmV4dFwiO1xyXG4vLyAgICAgZm9udC1zaXplOiAyNHB4O1xyXG4vLyAgICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuLy8gICAgIGxldHRlci1zcGFjaW5nOiAwLjJweDtcclxuLy8gICAgIGxpbmUtaGVpZ2h0OiAzM3B4O1xyXG4vLyAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4vLyAgIH1cclxuLy8gICAucGFnZS1oZWFkZXItcGFnZS1zdWJ0aXRsZS1sYXJnZSB7XHJcbi8vICAgICBjb2xvcjogI2ZmZmZmZjtcclxuLy8gICAgIGZvbnQtZmFtaWx5OiBcIkF2ZW5pck5leHRcIjtcclxuLy8gICAgIGZvbnQtc2l6ZTogMThweDtcclxuLy8gICAgIGxldHRlci1zcGFjaW5nOiAwLjJweDtcclxuLy8gICAgIGxpbmUtaGVpZ2h0OiAyNnB4O1xyXG4vLyAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4vLyAgIH1cclxuLy8gICAvLyB1c2VkIGluIGxvZ2luXHJcbi8vICAgLnBhZ2UtaGVhZGVyLXBhZ2UtdGl0bGUge1xyXG4vLyAgICAgY29sb3I6ICMzQTQxNDM7XHRcclxuLy8gICAgIGZvbnQtZmFtaWx5OiBcIkF2ZW5pck5leHRcIjtcdFxyXG4vLyAgICAgZm9udC1zaXplOiAxOHB4O1x0XHJcbi8vICAgICBmb250LXdlaWdodDogNjAwO1x0XHJcbi8vICAgICBsZXR0ZXItc3BhY2luZzogMC4ycHg7XHRcclxuLy8gICAgIGxpbmUtaGVpZ2h0OiAyNXB4O1x0XHJcbi8vICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbi8vIH1cclxuLy8gICAucGFnZS1oZWFkZXItcGFnZS1zdWJ0aXRsZSB7XHRcclxuLy8gICAgIGNvbG9yOiAjQjFCQ0M2O1x0XHJcbi8vICAgICBmb250LWZhbWlseTogXCJBdmVuaXJOZXh0XCI7XHRcclxuLy8gICAgIGZvbnQtc2l6ZTogMTNweDtcdFxyXG4vLyAgICAgZm9udC13ZWlnaHQ6IDUwMDtcdFxyXG4vLyAgICAgbGV0dGVyLXNwYWNpbmc6IDAuMnB4O1x0XHJcbi8vICAgICBsaW5lLWhlaWdodDogMThweDtcdFxyXG4vLyAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4vLyB9XHJcbiAgXHJcbi8vICAgLnBhZ2VzLWJsb2Nrcy1uZXdzLWRvdCB7XHJcbi8vICAgICBmb250LWZhbWlseTogXCInT3BlbiBTYW5zJywgc2Fucy1zZXJpZlwiO1xyXG4vLyAgICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuLy8gICAgIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4vLyAgICAgZm9udC1zaXplOiA4cHg7XHJcbi8vICAgICBsZXR0ZXItc3BhY2luZzogMHB4O1xyXG4vLyAgICAgY29sb3I6IHJnYmEoMTc3LCAxODgsIDE5OCwgMSk7XHJcbi8vICAgfVxyXG4gIFxyXG4vLyAgIC50ZXh0LWJvZHktZG90IHtcclxuLy8gICAgIGZvbnQtZmFtaWx5OiBcIidPcGVuIFNhbnMnLCBzYW5zLXNlcmlmXCI7XHJcbi8vICAgICBmb250LXdlaWdodDogNjAwO1xyXG4vLyAgICAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbi8vICAgICBmb250LXNpemU6IDhweDtcclxuLy8gICAgIGxldHRlci1zcGFjaW5nOiAwcHg7XHJcbi8vICAgICBjb2xvcjogcmdiYSg1OCwgNjUsIDY3LCAxKTtcclxuLy8gICB9XHJcbiAgXHJcbi8vICAgLnBhZ2VzLWNvbW1lbnQtZG90IHtcclxuLy8gICAgIGZvbnQtZmFtaWx5OiBcIidPcGVuIFNhbnMnLCBzYW5zLXNlcmlmXCI7XHJcbi8vICAgICBmb250LXdlaWdodDogNjAwO1xyXG4vLyAgICAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbi8vICAgICBmb250LXNpemU6IDhweDtcclxuLy8gICAgIGxldHRlci1zcGFjaW5nOiAwcHg7XHJcbi8vICAgICBjb2xvcjogcmdiYSgxNzcsIDE4OCwgMTk4LCAxKTtcclxuLy8gICB9XHJcbiAgXHJcbi8vICAgLnBvc3QtZG90IHtcclxuLy8gICAgIGZvbnQtZmFtaWx5OiBcIidPcGVuIFNhbnMnLCBzYW5zLXNlcmlmXCI7XHJcbi8vICAgICBmb250LXdlaWdodDogNjAwO1xyXG4vLyAgICAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbi8vICAgICBmb250LXNpemU6IDhweDtcclxuLy8gICAgIGxldHRlci1zcGFjaW5nOiAwcHg7XHJcbi8vICAgICBjb2xvcjogcmdiYSgxNzcsIDE4OCwgMTk4LCAxKTtcclxuLy8gICB9XHJcbiAgXHJcbi8vICAgLndlbGNvbWUtbG9jYXRpb25zLWFuZC1yb2xlcyB7XHJcbi8vICAgICBmb250LWZhbWlseTogXCInT3BlbiBTYW5zJywgc2Fucy1zZXJpZlwiO1xyXG4vLyAgICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuLy8gICAgIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4vLyAgICAgZm9udC1zaXplOiAxMXB4O1xyXG4vLyAgICAgbGV0dGVyLXNwYWNpbmc6IDAuMnB4O1xyXG4vLyAgICAgY29sb3I6IHJnYmEoMTMyLCAxNDMsIDE1NCwgMSk7XHJcbi8vICAgfVxyXG4gIFxyXG4gIFxyXG4iLCIvLyBBZGRlZCBCeSBBTEkgTUFIRElcclxuXHJcbi8vbGFuZGluZyBwYWdlIFxyXG5cclxuLnBhZ2UtaGVhZGVyLXBhZ2UtdGl0bGUtbGFyZ2Uge1xyXG4gIGNvbG9yOiAjZmZmZmZmO1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXNpemU6IDI0cHg7XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLnBhZ2UtaGVhZGVyLXBhZ2Utc3VidGl0bGUtbGFyZ2Uge1xyXG4gIGNvbG9yOiAjZmZmZmZmO1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXNpemU6IDE4cHg7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcbi8vIHVzZWQgaW4gbG9naW5cclxuLnBhZ2UtaGVhZGVyLXBhZ2UtdGl0bGUge1xyXG4gIGNvbG9yOiAjNzA3MDcwO1x0XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1x0XHJcbiAgZm9udC1zaXplOiAxOHB4O1x0XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcdFx0XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcbi5wYWdlLWhlYWRlci1wYWdlLXN1YnRpdGxlIHtcdFxyXG4gIGNvbG9yOiAkY29sb3ItZ3JheS0xNTtcdFxyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcdFxyXG4gIGZvbnQtc2l6ZTogMTNweDtcdFxyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHRcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5wYWdlcy1ibG9ja3MtbmV3cy1kb3Qge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNjAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogOHB4O1xyXG4gIGxldHRlci1zcGFjaW5nOiAwcHg7XHJcbiAgY29sb3I6IHJnYmEoMTc3LCAxODgsIDE5OCwgMSk7XHJcbn1cclxuXHJcbi50ZXh0LWJvZHktZG90IHtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBmb250LXNpemU6IDhweDtcclxuICBsZXR0ZXItc3BhY2luZzogMHB4O1xyXG4gIGNvbG9yOiByZ2JhKDU4LCA2NSwgNjcsIDEpO1xyXG59XHJcblxyXG4ucGFnZXMtY29tbWVudC1kb3Qge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNjAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogOHB4O1xyXG4gIGxldHRlci1zcGFjaW5nOiAwcHg7XHJcbiAgY29sb3I6IHJnYmEoMTc3LCAxODgsIDE5OCwgMSk7XHJcbn1cclxuXHJcbi5wb3N0LWRvdCB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiA4cHg7XHJcbiAgbGV0dGVyLXNwYWNpbmc6IDBweDtcclxuICBjb2xvcjogcmdiYSgxNzcsIDE4OCwgMTk4LCAxKTtcclxufVxyXG5cclxuLndlbGNvbWUtbG9jYXRpb25zLWFuZC1yb2xlcyB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxMXB4O1xyXG4gIFxyXG4gIGNvbG9yOiByZ2JhKDEzMiwgMTQzLCAxNTQsIDEpO1xyXG59XHJcblxyXG4ucGFnZXMtbWVzc2FnZXMtdGltZSB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxMXB4O1xyXG4gIGxldHRlci1zcGFjaW5nOiAwcHg7XHJcbiAgY29sb3I6IHJnYmEoMTc3LCAxODgsIDE5OCwgMSk7XHJcbn1cclxuXHJcbi5wb3N0LWxvY2F0aW9uIHtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBmb250LXNpemU6IDExcHg7XHJcbiAgXHJcbiAgY29sb3I6IHJnYmEoMTMyLCAxNDMsIDE1NCwgMSk7XHJcbn1cclxuXHJcbi5wcm9kdWN0LWNvdW50cnktb2YtYXZhaWxhYmlsaXR5IHtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBmb250LXNpemU6IDExcHg7XHJcbiAgY29sb3I6IHJnYmEoMTMyLCAxNDMsIDE1NCwgMSk7XHJcbn1cclxuXHJcbi5ib3R0b20tYmFyLWluYWN0aXZlIHtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBmb250LXNpemU6IDEycHg7XHJcbiAgbGV0dGVyLXNwYWNpbmc6IDAuMXB4O1xyXG4gIGNvbG9yOiByZ2JhKDEzMiwgMTQzLCAxNTQsIDEpO1xyXG4gIG1pbi1oZWlnaHQ6IDE0cHg7XHJcbn1cclxuXHJcbi5ib3R0b20tYmFyLWFjdGl2ZSB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxMnB4O1xyXG4gIFxyXG4gIGNvbG9yOiAjMDBkMmJkO1xyXG59XHJcblxyXG4ud2VsY29tZS1tYW5hZ2UtYWNjb3VudCB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxMXB4O1xyXG4gIFxyXG4gIGNvbG9yOiByZ2JhKDEzMiwgMTQzLCAxNTQsIDEpO1xyXG59XHJcblxyXG4ud2VsY29tZS11c2VyLXJlYWNoIHtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBmb250LXNpemU6IDEycHg7XHJcbiAgbGV0dGVyLXNwYWNpbmc6IDBweDtcclxuICBjb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAxKTtcclxufVxyXG5cclxuLmNyZWF0ZS1wb3N0LXBvc3QtdW5kZXIge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogMTJweDtcclxuICBcclxuICBjb2xvcjogcmdiYSgxNzcsIDE4OCwgMTk4LCAxKTtcclxufVxyXG5cclxuLmlucHV0LWluZm8tbWVzc2FnZSB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxMnB4O1xyXG4gIFxyXG4gIGNvbG9yOiByZ2JhKDE3NywgMTg4LCAxOTgsIDEpO1xyXG59XHJcblxyXG4udGV4dC1ib2R5LWhpbnQge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogMTJweDtcclxuICBsZXR0ZXItc3BhY2luZzogMC4xcHg7XHJcbiAgY29sb3I6IHJnYmEoMTc3LCAxODgsIDE5OCwgMSk7XHJcbn1cclxuXHJcbi5wYWdlcy1ibG9ja3MtbmV3cy1wb3N0ZWQtdW5kZXIge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxuICBcclxuICBjb2xvcjogcmdiYSgxMzIsIDE0MywgMTU0LCAxKTtcclxufVxyXG5cclxuLnBhZ2VzLWJsb2Nrcy1uZXdzLWRhdGUge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxuICBcclxuICBjb2xvcjogcmdiYSgxNzcsIDE4OCwgMTk4LCAxKTtcclxufVxyXG5cclxuLnBhZ2VzLWJsb2Nrcy1uZXdzLWF1dGhvciB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxM3B4O1xyXG4gIFxyXG4gIGNvbG9yOiByZ2JhKDE3NywgMTg4LCAxOTgsIDEpO1xyXG59XHJcblxyXG4uYmFubmVyLWhlYWRlci1tYWluLXN1YnRpdGxlIHtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBmb250LXNpemU6IDEzcHg7XHJcbiAgXHJcbiAgY29sb3I6IHJnYmEoMTMyLCAxNDMsIDE1NCwgMSk7XHJcbn1cclxuXHJcbi5lbnRpdHktZW50aXR5LWxvY2F0aW9uIHtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBmb250LXNpemU6IDEzcHg7XHJcbiAgXHJcbiAgY29sb3I6ICM3MDcwNzA7XHJcbn1cclxuXHJcbi5wYWdlcy1ldmVudHMtaG9zdCB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxM3B4O1xyXG4gIFxyXG4gIGNvbG9yOiByZ2JhKDE3NywgMTg4LCAxOTgsIDEpO1xyXG59XHJcblxyXG4ucGFnZXMtbm90aWZpY2F0aW9ucy1ub3RpZmljYXRpb24tdGltZSB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxM3B4O1xyXG4gIFxyXG4gIGNvbG9yOiByZ2JhKDE3NywgMTg4LCAxOTgsIDEpO1xyXG59XHJcblxyXG4ucGFnZXMtY29tbWVudC1saWtlcyB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxM3B4O1xyXG4gIFxyXG4gIGNvbG9yOiByZ2JhKDE3NywgMTg4LCAxOTgsIDEpO1xyXG59XHJcblxyXG4ucGFnZXMtY29tbWVudC1kYXRlIHtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBmb250LXNpemU6IDEzcHg7XHJcbiAgXHJcbiAgY29sb3I6IHJnYmEoMTc3LCAxODgsIDE5OCwgMSk7XHJcbn1cclxuXHJcbi5wb3N0LWRhdGUge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxuICBcclxuICBjb2xvcjogcmdiYSgxNzcsIDE4OCwgMTk4LCAxKTtcclxufVxyXG5cclxuLnBvc3QtcG9zdC1wb3N0ZWQtdW5kZXIge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxuICBcclxuICBjb2xvcjogcmdiYSgxMzIsIDE0MywgMTU0LCAxKTtcclxufVxyXG5cclxuLnBvc3QtcG9zdC1hdXRob3Ige1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxuICBjb2xvcjogIzcwNzA3MDtcclxufVxyXG4ubW9yZS10eHQge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxuICBjb2xvcjogIzcwNzA3MDtcclxufVxyXG5cclxuLnBvc3QtcG9zdC1saWtlIHtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBmb250LXNpemU6IDEzcHg7XHJcbiAgXHJcbiAgY29sb3I6IHJnYmEoMTMyLCAxNDMsIDE1NCwgMSk7XHJcbn1cclxuXHJcbi5wYWdlcy1tYW5hZ2Utc3VidGl0bGUge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxuICBcclxuICBjb2xvcjogcmdiYSgxNzcsIDE4OCwgMTk4LCAxKTtcclxufVxyXG5cclxuLmlucHV0LXRpdGxlIHtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBmb250LXNpemU6IDEzcHg7XHJcbiAgXHJcbiAgY29sb3I6IHJnYmEoMTc3LCAxODgsIDE5OCwgMSk7XHJcbn1cclxuXHJcbi5wYWdlcy10b3AtYmFyLXN0ZXBwZXIge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxuICBcclxuICBjb2xvcjogIzAwZDJiZDtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLnBhZ2VzLXRvcC1iYXItc3VidGl0bGUge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxuICBcclxuICBjb2xvcjogcmdiYSgxNzcsIDE4OCwgMTk4LCAxKTtcclxufVxyXG5cclxuLnBhZ2VzLXN1Yi1zZWN0aW9uLXN1Yi1zZWN0aW9uLWRlc2NyaXB0aW9uIHtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxNnB4O1xyXG4gIGxldHRlci1zcGFjaW5nOiAwLjNweDtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxuICBjb2xvcjogJGNvbG9yLWdyZWVuO1xyXG4gIGZvbnQtc2l6ZTogMThweDtcclxuICAvLyB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICAmLnNlY29uZC10aXRsZSB7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gIH1cclxufVxyXG5cclxuLnBhZ2VzLWltYWdlLXByZXZpZXctaW1hZ2UtZGF0ZSB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxM3B4O1xyXG4gIFxyXG4gIGNvbG9yOiByZ2JhKDE3NywgMTg4LCAxOTgsIDEpO1xyXG59XHJcblxyXG4uYmFubmVyLXRvcC1uZXR3b3JrZXItbmV0d29ya2VyLWludml0ZXMge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNjAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxuICBcclxuICBjb2xvcjogcmdiYSgyNTQsIDc2LCA3NSwgMSk7XHJcbn1cclxuXHJcbi53ZWxjb21lLW15cGFnZXMtYWN0aW9uIHtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBmb250LXNpemU6IDEzcHg7XHJcbiAgXHJcbiAgY29sb3I6ICMwMGQyYmQ7XHJcbn1cclxuXHJcbi5lbXB0eS1zdGF0ZS1wYWdlLXN1YnRpdGxlIHtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBmb250LXNpemU6IDEzcHg7XHJcbiAgXHJcbiAgY29sb3I6IHJnYmEoMTc3LCAxODgsIDE5OCwgMSk7XHJcbn1cclxuXHJcbi50ZXh0LWJvZHktc3VidGl0bGUge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxuICBcclxuICBjb2xvcjogcmdiYSgxMzIsIDE0MywgMTU0LCAxKTtcclxufVxyXG5cclxuLnBvc3QtcG9zdC1saWtlLWFjdGl2ZSB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxM3B4O1xyXG4gIFxyXG4gIGNvbG9yOiAjMDBkMmJkO1xyXG59XHJcblxyXG4udGFicy1zbWFsbC1pbmFjdGl2ZSB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxM3B4O1xyXG4gIFxyXG4gIGNvbG9yOiByZ2JhKDE3NywgMTg4LCAxOTgsIDEpO1xyXG59XHJcblxyXG4uY2FyZC1zdWJ0aXRsZSB7XHJcbiAgY29sb3I6ICM3MDcwNzA7XHJcbiAgZm9udC1zaXplOiAxMnB4O1xyXG4gIGxldHRlci1zcGFjaW5nOiAwLjJweDtcclxufVxyXG4ubGlzdC1zdWJ0aXRsZSB7XHJcbiAgY29sb3I6ICRjb2xvci1ibGFjay0xO1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxuICBsZXR0ZXItc3BhY2luZzogMC4ycHg7XHJcbiAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAmLmdyZWVuIHtcclxuICAgIGNvbG9yOiAkY29sb3ItZ3JlZW47XHJcbiAgfVxyXG4gICYuZ3JheSB7XHJcbiAgICBjb2xvcjogIzcwNzA3MDtcclxuICB9XHJcbiAgJi5ib2xkZXIge1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgIG1pbi1oZWlnaHQ6IDE4cHhcclxuICB9XHJcbn1cclxuXHJcbi50YWJzLXNtYWxsLWFjdGl2ZSB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxM3B4O1xyXG4gIFxyXG4gIGNvbG9yOiAjMDBkMmJkO1xyXG59XHJcblxyXG4udGFicy1zbWFsbC1uZXR3b3JrcyB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxM3B4O1xyXG4gIFxyXG4gIGNvbG9yOiByZ2JhKDI0LCA5MSwgMjIwLCAxKTtcclxufVxyXG5cclxuLnRhYnMtc21hbGwtYnVzaW5lc3Mge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNjAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxuICBcclxuICBjb2xvcjogcmdiYSg0MCwgNTMsIDE0NywgMSk7XHJcbn1cclxuXHJcbi50YWJzLXNtYWxsLWV2ZXJ5dGhpbmcge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNjAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxuICBcclxuICBjb2xvcjogcmdiYSgzNiwgNTMsIDg3LCAxKTtcclxufVxyXG5cclxuLmlucHV0LWhpbnQge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxuICBjb2xvcjogcmdiYSgxNzcsIDE4OCwgMTk4LCAxKTtcclxufVxyXG5cclxuLmVudGl0eS1lbnRpdHktdHlwZSB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gIFxyXG4gIGNvbG9yOiByZ2JhKDU4LCA2NSwgNjcsIDEpO1xyXG59XHJcblxyXG4udGV4dC1ib2R5LWxlZnQge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxuICBcclxuICBjb2xvcjogcmdiYSg1OCwgNjUsIDY3LCAxKTtcclxufVxyXG4uaW5wdXQtdGV4dCB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gIGNvbG9yOiAgJGNvbG9yLWdyYXktMjI7XHJcbiAgd29yZC1icmVhazogbm9ybWFsICFpbXBvcnRhbnQ7XHJcbiAgJi5ub3Qtc2VsZWN0ZWQge1xyXG4gICAgY29sb3I6ICRjb2xvci1ncmF5LTE1O1xyXG4gIH1cclxufVxyXG5cclxuLndlbGNvbWUtbXlwYWdlcy1lbnRpdHktbmFtZSB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gIFxyXG4gIGNvbG9yOiByZ2JhKDEzMiwgMTQzLCAxNTQsIDEpO1xyXG59XHJcblxyXG4ucGFnZXMtbWVzc2FnZXMtbWVzc2FnZSB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gIFxyXG4gIGNvbG9yOiByZ2JhKDU4LCA2NSwgNjcsIDEpO1xyXG59XHJcblxyXG4ucGFnZXMtbWVzc2FnZXMtaW5jb21pbmcge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxuICBcclxuICBjb2xvcjogcmdiYSg1OCwgNjUsIDY3LCAxKTtcclxufVxyXG5cclxuLnBhZ2VzLW1lc3NhZ2VzLW91dGdvaW5nIHtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBmb250LXNpemU6IDE0cHg7XHJcbiAgXHJcbiAgY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMSk7XHJcbn1cclxuXHJcbi53ZWxjb21lLXByaW1hcnktcm9sZSB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxMnB4O1xyXG4gIFxyXG4gIGNvbG9yOiByZ2JhKDEzMiwgMTQzLCAxNTQsIDEpO1xyXG59XHJcblxyXG4ucGFnZXMtY29tbWVudC11c2VyLW5hbWUge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNjAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxuICBcclxuICBjb2xvcjogcmdiYSg1OCwgNjUsIDY3LCAxKTtcclxufVxyXG5cclxuLnBhZ2VzLWNvbW1lbnQtY29tbWVudCB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gIFxyXG4gIGNvbG9yOiByZ2JhKDU4LCA2NSwgNjcsIDEpO1xyXG59XHJcblxyXG4ucG9zdC1ib2R5IHtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBmb250LXNpemU6IDE0cHg7XHJcbiAgXHJcbiAgY29sb3I6IHJnYmEoNTgsIDY1LCA2NywgMSk7XHJcbn1cclxuXHJcbi5jaGlwLWluYWN0aXZlIHtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBmb250LXNpemU6IDE0cHg7XHJcbiAgY29sb3I6IHJnYmEoMTMyLCAxNDMsIDE1NCwgMSk7XHJcbn1cclxuXHJcbi5jaGlwLWFjdGl2ZSB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gIGNvbG9yOiAjMDBkMmJkO1xyXG5cclxufVxyXG5cclxuLmNyZWF0ZS1wb3N0LWVudGl0eS1uYW1lIHtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBmb250LXNpemU6IDE0cHg7XHJcbiAgXHJcbiAgY29sb3I6ICMwMGQyYmQ7XHJcbn1cclxuXHJcbi5jcmVhdGUtcG9zdC1wb3N0LXR5cGUtc2VsZWN0ZWQge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNjAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxuICBcclxuICBjb2xvcjogIzAwZDJiZDtcclxufVxyXG5cclxuLmNyZWF0ZS1wb3N0LXBvc3QtdHlwZS1kZXNlbGVjdGVkIHtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBmb250LXNpemU6IDE0cHg7XHJcbiAgXHJcbiAgY29sb3I6IHJnYmEoMTMyLCAxNDMsIDE1NCwgMSk7XHJcbn1cclxuXHJcbi50ZXh0LWJvZHktY2VudGVyIHtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBmb250LXNpemU6IDE0cHg7XHJcbiAgXHJcbiAgY29sb3I6IHJnYmEoNTgsIDY1LCA2NywgMSk7XHJcbn1cclxuXHJcbi5kaWFsb2ctc3VidGl0bGUge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxuICBcclxuICBjb2xvcjogcmdiYSg1OCwgNjUsIDY3LCAxKTtcclxufVxyXG5cclxuLmVtcHR5LXN0YXRlLWJsb2NrLWJsb2NrLXRpdGxlIHtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBmb250LXNpemU6IDE0cHg7XHJcbiAgXHJcbiAgY29sb3I6IHJnYmEoMTMyLCAxNDMsIDE1NCwgMSk7XHJcbn1cclxuXHJcbi5wYWdlcy1pbWFnZS1wcmV2aWV3LWltYWdlLWNhcHRpb24ge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxuICBcclxuICBjb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAxKTtcclxufVxyXG5cclxuLnBhZ2VzLWltYWdlLXByZXZpZXctY3JvcHBlci1hY3Rpb25zIHtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBmb250LXNpemU6IDE0cHg7XHJcbiAgXHJcbiAgY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMSk7XHJcbn1cclxuXHJcbi5saXN0LWl0ZW0tcm93LXRpdGxlLXNtYWxsIHtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBmb250LXNpemU6IDE0cHg7XHJcbiAgXHJcbiAgY29sb3I6IHJnYmEoNTgsIDY1LCA2NywgMSk7XHJcbn1cclxuXHJcbi5saW5rcy1saW5rLWxlZnQge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxuICBcclxuICBjb2xvcjogIzAwZDJiZDtcclxufVxyXG5cclxuLmxpbmtzLWxpbmstY2VudGVyIHtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBmb250LXNpemU6IDE0cHg7XHJcbiAgXHJcbiAgY29sb3I6ICMwMGQyYmQ7XHJcbn1cclxuXHJcbi5wcm9kdWN0LW5hbWUge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxuICBcclxuICBjb2xvcjogcmdiYSg1OCwgNjUsIDY3LCAxKTtcclxufVxyXG5cclxuLnRhYnMtbGFyZ2UtcGVvcGxlIHtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBmb250LXNpemU6IDE1cHg7XHJcbiAgXHJcbiAgY29sb3I6ICMwMGQyYmQ7XHJcbn1cclxuXHJcbi50YWJzLWxhcmdlLWluYWN0aXZlIHtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBmb250LXNpemU6IDE1cHg7XHJcbiAgXHJcbiAgY29sb3I6IHJnYmEoMTc3LCAxODgsIDE5OCwgMSk7XHJcbn1cclxuXHJcbi50YWJzLWxhcmdlLW5ldHdvcmtzIHtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBmb250LXNpemU6IDE1cHg7XHJcbiAgXHJcbiAgY29sb3I6IHJnYmEoMjQsIDkxLCAyMjAsIDEpO1xyXG59XHJcblxyXG4udGFicy1sYXJnZS1idXNpbmVzcyB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxNXB4O1xyXG4gIFxyXG4gIGNvbG9yOiByZ2JhKDQwLCA1MywgMTQ3LCAxKTtcclxufVxyXG5cclxuLnRhYnMtbGFyZ2UtYWN0aXZlIHtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBmb250LXNpemU6IDE1cHg7XHJcbiAgXHJcbiAgY29sb3I6ICMwMGQyYmQ7XHJcbn1cclxuXHJcbi5wYWdlcy1ibG9ja3MtbmV3cy10aXRsZSB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxNnB4O1xyXG4gIFxyXG4gIGNvbG9yOiByZ2JhKDU4LCA2NSwgNjcsIDEpO1xyXG59XHJcblxyXG4ucGFnZXMtc3ViLXNlY3Rpb24tc3ViLXNlY3Rpb24tdGl0bGUge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZiAhaW1wb3J0YW50O1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgLy8gdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICBmb250LXNpemU6IDE2cHg7XHJcbiAgbGV0dGVyLXNwYWNpbmc6IDAuNnB4O1xyXG4gIGNvbG9yOiByZ2JhKDU4LCA2NSwgNjcsIDEpO1xyXG59XHJcbi5lbnRpdHktZW50aXR5LW5hbWUge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNjAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogMTZweDtcclxuICBjb2xvcjogcmdiYSg1OCwgNjUsIDY3LCAxKTtcclxufVxyXG5cclxuLnBhZ2VzLWV2ZW50cy10aXRsZSB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxNnB4O1xyXG4gIFxyXG4gIGNvbG9yOiByZ2JhKDU4LCA2NSwgNjcsIDEpO1xyXG59XHJcblxyXG4ucGFnZXMtZXZlbnRzLWRhdGUge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNjAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogMTZweDtcclxuICBcclxuICBjb2xvcjogIzAwZDJiZDtcclxufVxyXG5cclxuLnBhZ2VzLW1lc3NhZ2VzLXBlcnNvbi1uYW1lIHtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBmb250LXNpemU6IDE2cHg7XHJcbiAgXHJcbiAgY29sb3I6IHJnYmEoNTgsIDY1LCA2NywgMSk7XHJcbn1cclxuXHJcbi5wYWdlcy1ub3RpZmljYXRpb25zLW5vdGlmaWNhdGlvbi10aXRsZSB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxNnB4O1xyXG4gIFxyXG4gIGNvbG9yOiByZ2JhKDU4LCA2NSwgNjcsIDEpO1xyXG59XHJcblxyXG4ucG9zdC1wb3N0LXRpdGxlIHtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBmb250LXNpemU6IDE2cHg7XHJcbiAgXHJcbiAgY29sb3I6IHJnYmEoNTgsIDY1LCA2NywgMSk7XHJcbn1cclxuXHJcbi5wYWdlcy1tYW5hZ2UtdGl0bGUge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNjAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogMTZweDtcclxuICBcclxuICBjb2xvcjogcmdiYSg1OCwgNjUsIDY3LCAxKTtcclxufVxyXG5cclxuLnBhZ2VzLWltYWdlLXByZXZpZXctcGVyc29uLW5hbWUge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNjAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogMTZweDtcclxuICBcclxuICBjb2xvcjogcmdiYSgxNzcsIDE4OCwgMTk4LCAxKTtcclxufVxyXG5cclxuLmJhbm5lci10b3AtbmV0d29ya2VyLW5ldHdvcmtlci1uYW1lIHtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBmb250LXNpemU6IDE2cHg7XHJcbiAgXHJcbiAgY29sb3I6IHJnYmEoNTgsIDY1LCA2NywgMSk7XHJcbn1cclxuXHJcbi50ZXh0LWJvZHktdGl0bGUge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNjAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogMTZweDtcclxuICBcclxuICBjb2xvcjogcmdiYSg1OCwgNjUsIDY3LCAxKTtcclxufVxyXG5cclxuLmJhbm5lci1oZWFkZXItbWFpbi10aXRsZSB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxN3B4O1xyXG4gIFxyXG4gIGNvbG9yOiM3MDcwNzA7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi53ZWxjb21lLXVzZXItbmFtZSB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxOHB4O1xyXG4gIFxyXG4gIGNvbG9yOiAjNzA3MDcwO1xyXG59XHJcblxyXG4ucGFnZXMtdG9wLWJhci10aXRsZSB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxOHB4O1xyXG4gIFxyXG4gIGNvbG9yOiByZ2JhKDU4LCA2NSwgNjcsIDEpO1xyXG59XHJcblxyXG4ucGFnZXMtdG9wLXRpdGxlIHtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBcclxuICBjb2xvcjogcmdiYSg1OCwgNjUsIDY3LCAxKTtcclxufVxyXG5cclxuLmRpYWxvZy10aXRsZSB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxOHB4O1xyXG4gIFxyXG4gIGNvbG9yOiByZ2JhKDU4LCA2NSwgNjcsIDEpO1xyXG59XHJcblxyXG4uZW1wdHktc3RhdGUtcGFnZS10aXRsZSB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxOHB4O1xyXG4gIFxyXG4gIGNvbG9yOiByZ2JhKDU4LCA2NSwgNjcsIDEpO1xyXG59XHJcblxyXG4ucGFnZXMtdG9wLWJhci10aXRsZS13aGl0ZSB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxOHB4O1xyXG4gIFxyXG4gIGNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDEpO1xyXG59XHJcblxyXG4ucGFnZXMtYmxvY2tzLW5ld3MtYmxvY2stdGl0bGUge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNjAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogMjBweDtcclxuICBsZXR0ZXItc3BhY2luZzogMC4xOHB4O1xyXG4gIGNvbG9yOiByZ2JhKDI1NCwgNzQsIDczLCAxKTtcclxufVxyXG5cclxuLnBhZ2VzLWJsb2Nrcy1wZW9wbGUtYmxvY2stdGl0bGUge1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXdlaWdodDogNjAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGZvbnQtc2l6ZTogMjBweDtcclxuICBcclxuICBjb2xvcjogIzAwZDJiZDtcclxufVxyXG5cclxuLnBhZ2VzLWJsb2Nrcy1uZXR3b3Jrcy1ibG9jay10aXRsZSB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAyMHB4O1xyXG4gIFxyXG4gIGNvbG9yOiByZ2JhKDI0LCA5MSwgMjIwLCAxKTtcclxufVxyXG5cclxuLnBhZ2VzLWJsb2Nrcy1idXNpbmVzcy1ibG9jay10aXRsZSB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAyMHB4O1xyXG4gIGxldHRlci1zcGFjaW5nOiAwLjE4cHg7XHJcbiAgY29sb3I6IHJnYmEoNDAsIDUzLCAxNDcsIDEpO1xyXG59XHJcblxyXG4uc3ltYm9scy1ncm91cC10aXRsZSB7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgZm9udC1zaXplOiAyMHB4O1xyXG4gIGxldHRlci1zcGFjaW5nOiAwcHg7XHJcbiAgY29sb3I6IHJnYmEoMCwgMCwgMCwgMSk7XHJcbn1cclxuXHJcbi8vIG1pc3NpbmcgY2xhc3NlcyBcclxuXHJcbi5idXR0b24tbW9yZS10eHQge1x0XHJcbiAgY29sb3I6ICM2NTZENzY7XHRcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHRcclxuICBmb250LXNpemU6IDEycHg7XHRcclxuICBmb250LXdlaWdodDogNjAwO1x0XHJcbiAgbGV0dGVyLXNwYWNpbmc6IDAuNHB4O1x0XHJcbn1cclxuLmxpc3QtaXRlbS1yb3ctdGl0bGUtbGFyZ2Uge1x0XHJcbiAgY29sb3I6ICM3MDcwNzA7XHRcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHRcclxuICBmb250LXNpemU6IDE2cHg7XHRcclxuICBmb250LXdlaWdodDogNjAwO1x0XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuLm5ld3MtdGl0bGUge1xyXG4gIGNvbG9yOiAjNzA3MDcwO1x0XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1x0XHJcbiAgZm9udC1zaXplOiAxNHB4O1x0XHJcbiAgZm9udC13ZWlnaHQ6IDUwMDtcdFx0XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcbi5wYWdlcy1zdWItc2VjdGlvbi1zdWItc2VjdGlvbi1taW5pIHtcclxuICBjb2xvcjogJGNvbG9yLWdyYXktMTQ7XHRcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHRcclxuICBmb250LXNpemU6IDEzcHg7XHRcclxuICBmb250LXdlaWdodDogNTAwO1x0XHJcbn1cclxuLm5vdGlmaWNhdGlvbnMtY291bnQge1xyXG4gIGNvbG9yOiAkY29sb3Itd2hpdGU7XHRcdFxyXG4gIGZvbnQtc2l6ZTogOXB4O1x0XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHRcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgfVxyXG4gIC5wYWdlcy1pbWFnZS1wcmV2aWV3LWltYWdlLWNhcHQge1xyXG4gICAgY29sb3I6ICRjb2xvci13aGl0ZTtcdFxyXG4gICAgZm9udC1mYW1pbHk6IFwiQXZlbmlyIE5leHRcIjtcdFxyXG4gICAgZm9udC1zaXplOiAxNHB4O1x0XHJcbiAgICBmb250LXdlaWdodDogNTAwO1x0XHJcbiAgfVxyXG4gIC53ZWxjb21lLW15LXBhZ2VzLWVudGl0eS1uYW1lIHtcclxuICAgICAgY29sb3I6ICM3MDcwNzA7XHRcclxuICAgICAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1x0XHJcbiAgICAgIGZvbnQtc2l6ZTogMTRweDtcdFxyXG4gICAgICBmb250LXdlaWdodDogNjAwO1x0XHJcbiAgfVxyXG4gIC53ZWxjb21lLW15LXBhZ2VzLWFjdGlvbiB7XHRcclxuICAgIGNvbG9yOiAjMDBkMmJkO1x0XHJcbiAgICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHRcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcdFxyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcdFxyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuICAuZGlhc3BvcmEtaWQtY2FuLWhlbHAgIHtcclxuICAgIGNvbG9yOiAjNkY3NTc4O1x0XHJcbiAgICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHRcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcdFxyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcdFxyXG4gIH1cclxuICAudmlldy1hbGwtY29weSB7XHRcclxuICAgIGNvbG9yOiAjMDBEMkJEO1x0XHJcbiAgICBmb250LWZhbWlseTogXCJPcGVuIFNhbnNcIjtcdFxyXG4gICAgZm9udC1zaXplOiAxNnB4O1x0XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcdFxyXG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuNnB4O1x0XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5iYW5uZXItaGVhZGVyLW1haW4tdGl0bGUgIHtcclxuICAgICAgY29sb3I6ICRjb2xvci1ncmF5LTIyO1xyXG4gICAgICBmb250LXNpemU6IDE2cHg7XHRcclxuICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcdFxyXG4gICAgICBsZXR0ZXItc3BhY2luZzogMC4ycHg7XHRcclxuICAgICAgJi5zbWFsbCB7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1x0XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIFxyXG4gICAgLmJhbm5lci1oZWFkZXItbWFpbi1zdWJ0aXRsZSAge1xyXG4gICAgICBjb2xvcjogJGNvbG9yLWdyYXktMTQ7XHRcclxuICAgICAgZm9udC1zaXplOiAxM3B4O1x0XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHRcclxuICAgICAgbGV0dGVyLXNwYWNpbmc6IDAuMnB4O1x0XHJcbiAgICB9XHJcbiAiLCIud2VsY29tZS1oZWFkZXIge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBpbWcge1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDgycHg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMjRweDtcclxuICAgIH1cclxuICAgIC53ZWxjb21lLXR4dCB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogOTJweDtcclxuICAgICAgICAuZmlyc3QtaXRlbSB7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNzA3MDcwO1x0Zm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1x0XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMjJweDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcdFxyXG4gICAgICAgICAgICBsZXR0ZXItc3BhY2luZzogMC4ycHg7XHRcclxuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDMzcHg7XHRcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuc2Vjb25kLWl0ZW0ge1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxNnB4O1xyXG4gICAgICAgICAgICBjb2xvcjogIzcwNzA3MDtcdGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcdFxyXG4gICAgICAgICAgICBmb250LXNpemU6IDE4cHg7XHRcclxuICAgICAgICAgICAgbGV0dGVyLXNwYWNpbmc6IDAuMnB4O1x0XHJcbiAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyNnB4O1x0XHJcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG59XHJcbi53ZWxjb21lLXBhZ2UtYnRuIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJvdHRvbTogNTBweDtcclxuICAgIHdpZHRoOiBjYWxjKDEwMCUgLSA0MHB4KTtcclxuICAgIHBhZGRpbmc6IDAgNXB4O1xyXG4gICAgLnNlY29uZC1pdGVtIHtcclxuICAgICAgICBtYXJnaW4tdG9wOiAyNXB4O1xyXG4gICAgfVxyXG59IiwiLmxvZ2luLXR4dCB7XHJcbiAgLnRpdGxlIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgfVxyXG4gIC5zdWJ0aXRsZSB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gIH1cclxufVxyXG4uZGlhc3BvcmEtZm9vdGVyLXdpdGgtaW5saW5lLWJ0biB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBwYWRkaW5nOiAyMHB4O1xyXG4gIGFwcC1idXR0b24ge1xyXG4gICAgd2lkdGg6IGNhbGMoMzMuMzMlIC0gN3B4KTtcclxuICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxuICAgICY6bGFzdC1jaGlsZCB7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAwO1xyXG4gICAgfVxyXG4gIH1cclxuICAuYnRuIHtcclxuICAgIGhlaWdodDogNDFweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDdweDtcclxuICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxuICB9XHJcbiAgLnRleHQge1xyXG4gICAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmICFpbXBvcnRhbnQ7XHRcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcdFxyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcdFxyXG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuMnB4O1x0XHJcbiAgICBsaW5lLWhlaWdodDogMTlweDtcdFxyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxufVxyXG4uaW5saW5lLWxpbmstY29udGFpbmVyIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBtYXJnaW4tdG9wOiA0MHB4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAuaC1saW5lIHtcclxuICAgICAgICB3aWR0aDogMXB4O1xyXG4gICAgICAgIGhlaWdodDogMTVweDtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3ItZ3JheS0xMztcclxuICAgICAgICBtYXJnaW46IDJweCAyMHB4O1xyXG4gICAgfVxyXG59XHJcbiIsIi5zdGF0dXMtY29udGFpbmVyIHtcclxuICBtYXJnaW4tdG9wOiAxMDBweDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAuaW1hZ2Uge1xyXG4gICAgd2lkdGg6IDY0cHg7XHJcbiAgICBoZWlnaHQ6IDY0cHg7XHJcbiAgICBpbWcge1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG4gIH1cclxuICAudGl0bGUge1xyXG4gICAgbWFyZ2luLXRvcDogMjBweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuICAuc3VidGl0bGUge1xyXG4gICAgbWFyZ2luLXRvcDogOHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuICAuZW1wdHktYnV0dG9uIHtcclxuICAgIG1hcmdpbi10b3A6IDMwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcbn1cclxuXHJcbi5jYXJkLXN0YXR1cyB7XHJcbiAgLy8gYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICRjb2xvci1ncmF5LTEzO1xyXG4gIHBhZGRpbmctYm90dG9tOiAxMHB4O1xyXG4gICYubGFzdC1pdGVtLXN0YXR1cyB7XHJcbiAgICBib3JkZXI6IDA7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gIH1cclxuICAudGl0bGUge1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgZGlzcGxheTogLXdlYmtpdC1ib3g7XHJcbiAgICAtd2Via2l0LWxpbmUtY2xhbXA6IDM7XHJcbiAgICAvKiBhdXRvcHJlZml4ZXI6IGlnbm9yZSBuZXh0ICovXHJcbiAgICAtd2Via2l0LWJveC1vcmllbnQ6IHZlcnRpY2FsO1xyXG4gICAgcGFkZGluZzogMCAyMHB4O1xyXG4gIH1cclxuICAuY2FyZC1zdGF0dXMtY29udGFpbmVyIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgLnN0YXJ0LWl0ZW0ge1xyXG4gICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICBmbGV4LXNocmluazogMDtcclxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAuaW1hZ2Uge1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICAgICAgZmxleC1zaHJpbms6IDA7XHJcbiAgICAgICAgaW1nIHtcclxuICAgICAgICAgIGhlaWdodDogNDhweDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIC5lbmQtaXRlbSB7XHJcbiAgICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5zdGF0dXMtYnV0dG9uIHtcclxuICAgIHBhZGRpbmc6IDAgMzBweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiA0OHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLXdoaXRlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxufVxyXG4iLCIvL2lvcyBhbGVydFxyXG4uYWxlcnQtd3JhcHBlci5zYy1pb24tYWxlcnQtaW9ze1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTZweDtcclxuICAgIG1heC13aWR0aDogMzAwcHg7XHJcbn1cclxuLmFsZXJ0LXN1Yi10aXRsZS5zYy1pb24tYWxlcnQtaW9zLFxyXG4uYWxlcnQtdGl0bGUuc2MtaW9uLWFsZXJ0LWlvcyB7XHJcbiAgY29sb3I6ICAkY29sb3ItZ3JheS0yMjtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWYgIWltcG9ydGFudDtcclxuICBmb250LXNpemU6IDE4cHg7XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICBsZXR0ZXItc3BhY2luZzogMC4ycHg7XHJcbiAgbGluZS1oZWlnaHQ6IDI1cHg7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcbi5hbGVydC1oZWFkLnNjLWlvbi1hbGVydC1pb3MgKyAuYWxlcnQtbWVzc2FnZS5zYy1pb24tYWxlcnQtaW9zIHtcclxuICBjb2xvcjogJGNvbG9yLWdyYXktMjI7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmICFpbXBvcnRhbnQ7XHJcbiAgZm9udC1zaXplOiAxNXB4O1xyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgbGV0dGVyLXNwYWNpbmc6IDAuMnB4O1xyXG4gIGxpbmUtaGVpZ2h0OiAxOXB4O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBwYWRkaW5nLWJvdHRvbTogMjBweDtcclxufVxyXG4uYWxlcnQtYnV0dG9uLWdyb3VwLnNjLWlvbi1hbGVydC1pb3Mge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgcGFkZGluZy1yaWdodDogMjBweDtcclxuICBwYWRkaW5nLWxlZnQ6IDIwcHg7XHJcbiAgcGFkZGluZy1ib3R0b206IDIwcHg7XHJcbn1cclxuLmFsZXJ0LWJ1dHRvbi5zYy1pb24tYWxlcnQtaW9zIHtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlciAhaW1wb3J0YW50O1xyXG4gIHdpZHRoOiA1MCU7XHJcbiAgaGVpZ2h0OiA0OHB4O1xyXG4gIGJvcmRlcjogMDtcclxuICAmOmZpcnN0LWNoaWxkIHtcclxuICAgIHBhZGRpbmctbGVmdDogMDtcclxuICAgIG1hcmdpbi1yaWdodDogMDtcclxuICAgIHBhZGRpbmctcmlnaHQ6IDZweDtcclxuICB9XHJcbiAgJjpudGgtY2hpbGQoMikge1xyXG4gICAgcGFkZGluZy1sZWZ0OiA2cHg7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDA7XHJcbiAgICAuYWxlcnQtYnV0dG9uLWlubmVyLnNjLWlvbi1hbGVydC1pb3MsXHJcbiAgICAuYWxlcnQtdGFwcGFibGUuc2MtaW9uLWFsZXJ0LWlvcyB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogJGNvbG9yLW9yYW5nZTtcclxuICAgICAgICBjb2xvcjogJGNvbG9yLXdoaXRlO1xyXG4gICAgICAgIGJvcmRlcjogMDtcclxuICAgIH1cclxuICB9XHJcbn1cclxuLmFsZXJ0LWJ1dHRvbi1pbm5lci5zYy1pb24tYWxlcnQtaW9zLFxyXG4uYWxlcnQtdGFwcGFibGUuc2MtaW9uLWFsZXJ0LWlvcyB7XHJcbmJvcmRlcjogMXB4IHNvbGlkICRjb2xvci1ncmF5LTIyOyBcclxuICBjb2xvcjogJGNvbG9yLWdyYXktMjI7XHJcbiAgaGVpZ2h0OiA0OHB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICBmb250LXNpemU6IDE1cHg7XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICBsZXR0ZXItc3BhY2luZzogMC4ycHg7XHJcbiAgbGluZS1oZWlnaHQ6IDIycHg7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgcGFkZGluZzogMCA3cHg7XHJcbn1cclxuXHJcbi8vIGFuZHJvaWQgYWxlcnRcclxuLmFsZXJ0LXdyYXBwZXIuc2MtaW9uLWFsZXJ0LW1ke1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTZweDtcclxuICAgIG1heC13aWR0aDogMzAwcHg7XHJcbn1cclxuLmFsZXJ0LXN1Yi10aXRsZS5zYy1pb24tYWxlcnQtbWQsXHJcbi5hbGVydC10aXRsZS5zYy1pb24tYWxlcnQtbWQge1xyXG4gIGNvbG9yOiAgJGNvbG9yLWdyYXktMjI7XHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtc2l6ZTogMThweDtcclxuICBmb250LXdlaWdodDogNjAwO1xyXG4gIGxldHRlci1zcGFjaW5nOiAwLjJweDtcclxuICBsaW5lLWhlaWdodDogMjVweDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLmFsZXJ0LWhlYWQuc2MtaW9uLWFsZXJ0LW1kICsgLmFsZXJ0LW1lc3NhZ2Uuc2MtaW9uLWFsZXJ0LW1kIHtcclxuICBjb2xvcjogICRjb2xvci1ncmF5LTIyO1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBmb250LXNpemU6IDE0cHg7XHJcbiAgbGV0dGVyLXNwYWNpbmc6IDAuMnB4O1xyXG4gIGxpbmUtaGVpZ2h0OiAxOXB4O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBwYWRkaW5nLWJvdHRvbTogNHB4O1xyXG59XHJcbi5hbGVydC1idXR0b24tZ3JvdXAuc2MtaW9uLWFsZXJ0LW1kIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIHBhZGRpbmctcmlnaHQ6IDIwcHg7XHJcbiAgcGFkZGluZy1sZWZ0OiAyMHB4O1xyXG4gIHBhZGRpbmctYm90dG9tOiAxMXB4O1xyXG59XHJcbi5hbGVydC1idXR0b24uc2MtaW9uLWFsZXJ0LW1kIHtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlciAhaW1wb3J0YW50O1xyXG4gIHdpZHRoOiA1MCU7XHJcbiAgJjpmaXJzdC1jaGlsZCB7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDA7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDA7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiA2cHggIWltcG9ydGFudDtcclxuICB9XHJcbiAgJjpudGgtY2hpbGQoMikge1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAwO1xyXG4gICAgcGFkZGluZy1yaWdodDogMDtcclxuICAgIHBhZGRpbmctbGVmdDogNnB4ICFpbXBvcnRhbnQ7XHJcbiAgICAuYWxlcnQtYnV0dG9uLWlubmVyLnNjLWlvbi1hbGVydC1tZCxcclxuICAgIC5hbGVydC10YXBwYWJsZS5zYy1pb24tYWxlcnQtbWQge1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICRjb2xvci1vcmFuZ2U7XHJcbiAgICAgICAgY29sb3I6ICRjb2xvci13aGl0ZTtcclxuICAgICAgICBib3JkZXI6IDA7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbi5hbGVydC1idXR0b24taW5uZXIuc2MtaW9uLWFsZXJ0LW1kLFxyXG4uYWxlcnQtdGFwcGFibGUuc2MtaW9uLWFsZXJ0LW1kIHtcclxuICBoZWlnaHQ6IDQ4cHg7XHJcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gIGNvbG9yOiAkY29sb3ItZ3JheS0yMjtcclxuICBib3JkZXI6IDFweCBzb2xpZCAkY29sb3ItZ3JheS0yMjtcclxuICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC1zaXplOiAxNXB4O1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgbGV0dGVyLXNwYWNpbmc6IDAuMnB4O1xyXG4gIGxpbmUtaGVpZ2h0OiAyMnB4O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIHRleHQtdHJhbnNmb3JtIDogY2FwaXRhbGl6ZTtcclxuICBwYWRkaW5nIDogMCA3cHg7XHJcbn1cclxuLmNvbHVtbi1idXR0b24ge1xyXG4gIC5hbGVydC1idXR0b24tZ3JvdXAuc2MtaW9uLWFsZXJ0LW1kLCAgIC5hbGVydC1idXR0b24tZ3JvdXAuc2MtaW9uLWFsZXJ0LWlvc3tcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAuYWxlcnQtYnV0dG9uLnNjLWlvbi1hbGVydC1tZCwgLmFsZXJ0LWJ1dHRvbi5zYy1pb24tYWxlcnQtaW9zIHtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5hbGVydC1idXR0b24uc2MtaW9uLWFsZXJ0LW1kLCAgIC5hbGVydC1idXR0b24uc2MtaW9uLWFsZXJ0LWlvcyB7XHJcbiAgICAmOm50aC1jaGlsZCgyKSB7XHJcbiAgICAgIHBhZGRpbmctbGVmdDogMCAhaW1wb3J0YW50O1xyXG4gICAgICBwYWRkaW5nLXRvcDogMDtcclxuICAgICAgc3BhbiB7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLW9yYW5nZSAhaW1wb3J0YW50O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5hbGVydC1idXR0b24uc2MtaW9uLWFsZXJ0LW1kLCAuYWxlcnQtYnV0dG9uLnNjLWlvbi1hbGVydC1pb3N7XHJcbiAgICAmOm50aC1jaGlsZCgxKSB7XHJcbiAgICAgIHBhZGRpbmctcmlnaHQ6IDAgIWltcG9ydGFudDtcclxuICAgICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICAgICAgcGFkZGluZy1ib3R0b206IDA7XHJcbiAgICAgIHNwYW4ge1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci13aGl0ZSAhaW1wb3J0YW50O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbi5hbGVydC13cmFwcGVyLnNjLWlvbi1hbGVydC1tZCB7XHJcbiAgYm9yZGVyLXJhZGl1czogMTZweDtcclxuICB3aWR0aDogMTAwJTtcclxuICBtYXgtd2lkdGg6IGNhbGMoMTAwJSAtIDYwcHgpO1xyXG59XHJcbi5hbGVydC13cmFwcGVyLnNjLWlvbi1hbGVydC1pb3Mge1xyXG4gIGJvcmRlci1yYWRpdXM6IDE2cHg7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgbWF4LXdpZHRoOiBjYWxjKDEwMCUgLSA2MHB4KTtcclxufVxyXG4uZ3JlZW4tY29udHJvbGUge1xyXG4gIC5hbGVydC1pbnB1dC1ncm91cC5zYy1pb24tYWxlcnQtbWQsIC5hbGVydC1tZXNzYWdlLnNjLWlvbi1hbGVydC1tZCB7XHJcbiAgICAtd2Via2l0LXBhZGRpbmctc3RhcnQ6IDIwcHggIWltcG9ydGFudDtcclxuICAgIHBhZGRpbmctaW5saW5lLXN0YXJ0OiAyMHB4ICAhaW1wb3J0YW50O1xyXG4gICAgLXdlYmtpdC1wYWRkaW5nLWVuZDogMjBweCAgIWltcG9ydGFudDtcclxuICAgIHBhZGRpbmctaW5saW5lLWVuZDogMjBweCAgIWltcG9ydGFudDtcclxuICB9XHJcbiAgLmFsZXJ0LWlucHV0LXdyYXBwZXIuc2MtaW9uLWFsZXJ0LW1kIHtcclxuICAgaW5wdXQge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLWdyYXktMTc7XHJcbiAgICBwYWRkaW5nOiAxM3B4IDE2cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA4cHg7IFxyXG4gICAgYm9yZGVyLWJvdHRvbTogMDtcclxuICAgfVxyXG4gICAgaW5wdXQge1xyXG4gICAgICAmOjpwbGFjZWhvbGRlciB7XHJcbiAgICAgICAgY29sb3I6ICRjb2xvci1ncmF5LTE1O1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgICAgICBsZXR0ZXItc3BhY2luZzogMC4ycHg7IFxyXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAxOXB4O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5hbGVydC1pbnB1dC13cmFwcGVyLnNjLWlvbi1hbGVydC1pb3Mge1xyXG4gICAgaW5wdXQge1xyXG4gICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1ncmF5LTE3O1xyXG4gICAgIHBhZGRpbmc6IDEzcHggMTZweDtcclxuICAgICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbiAgICAgYm9yZGVyOiAwO1xyXG4gICAgfVxyXG4gICAgIGlucHV0IHtcclxuICAgICAgICY6OnBsYWNlaG9sZGVyIHtcclxuICAgICAgICAgY29sb3I6ICRjb2xvci1ncmF5LTE1O1xyXG4gICAgICAgICBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7XHJcbiAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgICAgICAgbGV0dGVyLXNwYWNpbmc6IDAuMnB4OyBcclxuICAgICAgICAgbGluZS1oZWlnaHQ6IDE5cHg7XHJcbiAgICAgICB9XHJcbiAgICAgfVxyXG4gICB9XHJcbiAgLmFsZXJ0LWlucHV0LWdyb3VwLnNjLWlvbi1hbGVydC1tZCwgLmFsZXJ0LW1lc3NhZ2Uuc2MtaW9uLWFsZXJ0LW1kIHtcclxuICAgIHBhZGRpbmctdG9wOiAwcHg7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMHB4O1xyXG4gIH1cclxuICAuYWxlcnQtYnV0dG9uLnNjLWlvbi1hbGVydC1pb3Mge1xyXG4gICAgJjpudGgtY2hpbGQoMikge1xyXG4gICAgICBwYWRkaW5nLWxlZnQ6IDhweDtcclxuICAgICAgbWFyZ2luLXJpZ2h0OiAwO1xyXG4gICAgICBzcGFuLmFsZXJ0LWJ1dHRvbi1pbm5lci5zYy1pb24tYWxlcnQtaW9zIHtcclxuICAgICAgICAgIGJhY2tncm91bmQ6ICRjb2xvci1ncmVlbi01O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5hbGVydC1idXR0b24uc2MtaW9uLWFsZXJ0LW1kICB7XHJcbiAgICAgICAgJjpudGgtY2hpbGQoMikge1xyXG4gICAgICBwYWRkaW5nLWxlZnQ6IDhweDtcclxuICAgICAgbWFyZ2luLXJpZ2h0OiAwO1xyXG4gICAgICBzcGFuLmFsZXJ0LWJ1dHRvbi1pbm5lci5zYy1pb24tYWxlcnQtbWR7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kOiAkY29sb3ItZ3JlZW4tNTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuICAmLmhpZGUtc3ViLWhlYWRlciB7XHJcbiAgICAuYWxlcnQtaGVhZC5zYy1pb24tYWxlcnQtaW9zICsgLmFsZXJ0LW1lc3NhZ2Uuc2MtaW9uLWFsZXJ0LWlvcyB7XHJcbiAgICAgIHBhZGRpbmctYm90dG9tIDogMCAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4uYWxlcnQtYnV0dG9uIHtcclxuICBpb24tcmlwcGxlLWVmZmVjdCB7XHJcbiAgICBkaXNwbGF5IDogbm9uZTtcclxuICB9XHJcbn1cclxuLmFsZXJ0LWlucHV0LWdyb3VwLnNjLWlvbi1hbGVydC1pb3MsIC5hbGVydC1tZXNzYWdlLnNjLWlvbi1hbGVydC1pb3Mge1xyXG4gIHBhZGRpbmc6ICAyMHB4ICFpbXBvcnRhbnQ7XHJcbiAgcGFkZGluZy10b3A6IDAgIWltcG9ydGFudDtcclxufVxyXG4uYWN0aXZhdGVkIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xyXG59XHJcbiIsIi5ib3JkZXItYm90dG9tIHtcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAkY29sb3ItZ3JheS0xMztcclxuICB9XHJcbiAgLm5vLWJvcmRlciB7XHJcbiAgICBib3JkZXI6IDA7XHJcbiAgfVxyXG4gIFxyXG4gIC5zZXBlcmF0ZS1ib3JkZXIge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDFweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1ncmF5LTEzO1xyXG4gIH1cclxuICAuc2VwZXJhdGUtYm9yZGVyLWFjdGlvbiB7XHJcbiAgICBiYWNrZ3JvdW5kOiAkY29sb3Itd2hpdGUtZ3JheTtcclxuICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICAuc2VwZXJhdGUtYm9yZGVyLWFjdGlvbi1ib2R5IHtcclxuICAgICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICRjb2xvci1ncmF5LTEzO1xyXG4gICAgICBoZWlnaHQ6IDQycHg7XHJcbiAgICAgIGJhY2tncm91bmQ6ICRjb2xvci13aGl0ZTtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgIHBhZGRpbmctdG9wOiAxOHB4O1xyXG4gICAgfVxyXG4gICAgaW1nIHtcclxuICAgICAgaGVpZ2h0OiAxM3B4O1xyXG4gICAgICB3aWR0aDogMTNweDtcclxuICAgICAgbWFyZ2luLWxlZnQ6IDdweDtcclxuICAgIH1cclxuICB9XHJcbiAgLm1haW4tYm9yZGVyIHtcclxuICAgIGhlaWdodDogNTNweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci13aGl0ZS1ncmF5O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgJjo6YWZ0ZXIge1xyXG4gICAgICBjb250ZW50OiBcIiBcIjtcclxuICAgICAgaGVpZ2h0OiAxNnB4O1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgYmFja2dyb3VuZDogJGNvbG9yLXdoaXRlO1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDIxcHg7XHJcbiAgICAgIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAyMXB4O1xyXG4gICAgICBib3gtc2hhZG93OiAwIDRweCA0cHggMCByZ2JhKDAsMCwwLDAuMDcpO1xyXG4gICAgICBsZWZ0OiAwO1xyXG4gICAgICB6LWluZGV4OiA1O1xyXG4gICAgfVxyXG4gICAgJjo6YmVmb3JlIHtcclxuICAgICAgY29udGVudDogXCIgXCI7XHJcbiAgICAgIGhlaWdodDogMTZweDtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgIGJhY2tncm91bmQ6ICRjb2xvci13aGl0ZTtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAyMXB4O1xyXG4gICAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMjFweDtcclxuICAgICAgYm90dG9tOiAwO1xyXG4gICAgICAvLyBib3gtc2hhZG93OiA0cHggLTNweCA0cHggMCByZ2JhKDAsIDAsIDAsIDAuMDcpO1xyXG4gICAgICBsZWZ0OiAwO1xyXG4gICAgfVxyXG4gICAgJi5uby1ib3JkZXItYm90dG9tLXJhZGl1cyB7XHJcbiAgICAgICY6OmFmdGVyIHtcclxuICAgICAgICBjb250ZW50OiBub25lO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAmLm5vLWJvcmRlci10b3AtcmFkaXVzIHtcclxuICAgICAgJjo6YmVmb3JlIHtcclxuICAgICAgICBjb250ZW50OiBub25lO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAmLnNtYWxsLWhlaWdodCB7XHJcbiAgICAgIGhlaWdodCA6IDM3cHg7XHJcbiAgICB9XHJcbiAgICAmLnNtYWxsLWhlaWdodC0yMCB7XHJcbiAgICAgIGhlaWdodDogMjBweDtcclxuICAgIH1cclxuICAgICYuc21hbGwtaGVpZ2h0LTIzIHtcclxuICAgICAgaGVpZ2h0OiAyM3B4OyBcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5yZW1vdmUtYWRkaXRpb25hbC1wYXJ0IHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIC5yZW1vdmUge1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBoZWlnaHQ6IDE2cHg7XHJcbiAgICAgICAgYmFja2dyb3VuZDogJGNvbG9yLXdoaXRlLWdyYXk7XHJcbiAgICAgICAgYm90dG9tOiAwO1xyXG4gICAgICAgICYuYmlnLWhlaWdodCB7XHJcbiAgICAgICAgICBoZWlnaHQ6IDEwMHB4O1xyXG4gICAgICAgICAgYm90dG9tOiAtMTAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiAgIiwiaW9uLWZvb3RlciB7XHJcbiAgICAmOjpiZWZvcmUge1xyXG4gICAgICBiYWNrZ3JvdW5kLWltYWdlOiBub25lICFpbXBvcnRhbnQ7XHJcbiAgICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCByZ2JhKDIxNCwgMjE0LCAyMzMsIDAuNDYpO1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3Itd2hpdGU7XHJcbiAgICB9XHJcbiAgICAmLm5vLWJvcmRlciB7XHJcbiAgICAgICY6OmJlZm9yZSB7XHJcbiAgICAgICAgYm9yZGVyLXRvcDogMDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuICBpb24taGVhZGVyIHtcclxuICAgICY6OmFmdGVyIHtcclxuICAgICAgYmFja2dyb3VuZC1pbWFnZTogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG4gIH0iLCIuZnItdG9vbGJhci5mci1tb2JpbGUuZnItdG9wLmZyLWJhc2ljIHtcclxuICAgIGhlaWdodDogNTdweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgYmFja2dyb3VuZDogJGNvbG9yLWdyYXktMTc7XHJcbiAgICBib3JkZXI6IDAgIWltcG9ydGFudDtcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjRDVFMEVBICFpbXBvcnRhbnQ7XHJcbiAgICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiA4cHg7XHJcbiAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogOHB4O1xyXG4gICAgY29sb3I6ICRjb2xvci1ncmVlbi01O1xyXG4gICAgaSB7XHJcbiAgICAgICAgZm9udDogbm9ybWFsIG5vcm1hbCBub3JtYWwgMTRweC8xIEZvbnRBd2Vzb21lICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgZm9udC1zaXplOiAxOHB4ICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcbn1cclxuLmZyLWNvbW1hbmQuZnItYnRuKy5mci1kcm9wZG93bi1tZW51IC5mci1kcm9wZG93bi13cmFwcGVyIC5mci1kcm9wZG93bi1jb250ZW50IHVsLmZyLWRyb3Bkb3duLWxpc3QgbGkgYS5mci1hY3RpdmUge1xyXG4gICAgYmFja2dyb3VuZDpyZ2JhKDEsIDE3NiwgMTg4LCAwLjIpICFpbXBvcnRhbnQ7XHJcbn1cclxuLmZyLWNvbW1hbmQuZnItYnRuLmZyLWFjdGl2ZSsuZnItZHJvcGRvd24tbWVudSB7XHJcbiAgICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgICBib3gtc2hhZG93OiAwIDNweCA2cHggcmdiYSgxLCAxNzYsIDE4OCwuMTYpLCAwIDJweCAycHggMXB4IHJnYmEoMSwgMTc2LCAxODgsLjE0KSAhaW1wb3J0YW50O1xyXG4gICAgdG9wOiA1MXB4ICFpbXBvcnRhbnQ7XHJcbiAgICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbiAgICAuZnItY29tbWFuZC5mci1idG4rLmZyLWRyb3Bkb3duLW1lbnUgLmZyLWRyb3Bkb3duLXdyYXBwZXIge1xyXG4gICAgICAgIGJhY2tncm91bmQ6cmdiYSgxLCAxNzYsIDE4OCwgMC4yKSAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG59XHJcbi5mci1ib3guZnItYmFzaWMuZnItdG9wIC5mci13cmFwcGVyIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1ncmF5LTE3O1xyXG4gICAgYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogOHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogOHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuLmZyLWJveC5mci1iYXNpYyAuZnItZWxlbWVudCB7XHJcbiAgICBjb2xvcjogJGNvbG9yLWdyYXktMTU7XHJcbiAgICBmb250LWZhbWlseTogXCJBdmVuaXIgTmV4dFwiO1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIGxldHRlci1zcGFjaW5nOiAwLjJweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAxOXB4OyBcclxuICAgIG1pbi1oZWlnaHQ6IDE1MHB4ICFpbXBvcnRhbnQ7IFxyXG59XHJcbi5mci1ib3ggLmZyLWNvdW50ZXIge1xyXG4gICAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xyXG59XHJcbi5mci10b29sYmFyIC5mci1jb21tYW5kLmZyLWJ0biwgLmZyLXBvcHVwIC5mci1jb21tYW5kLmZyLWJ0biB7XHJcbiAgICBjb2xvcjogIzlmYThiMyAhaW1wb3J0YW50O1xyXG59XHJcbi5mci10b29sYmFyIC5mci1jb21tYW5kLmZyLWJ0bi5mci1hY3RpdmUsIC5mci1wb3B1cCAuZnItY29tbWFuZC5mci1idG4uZnItYWN0aXZlIHtcclxuICAgIGNvbG9yOiAkY29sb3ItZ3JlZW4tNSAhaW1wb3J0YW50O1xyXG4gICAgYmFja2dyb3VuZDpyZ2JhKDEsIDE3NiwgMTg4LCAwLjIpICFpbXBvcnRhbnQ7XHJcbiAgICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbn1cclxuLmZyLXRvb2xiYXIgLmZyLWNvbW1hbmQuZnItYnRuLmZyLWRyb3Bkb3duOjphZnRlciwgLmZyLXBvcHVwIC5mci1jb21tYW5kLmZyLWJ0bi5mci1kcm9wZG93bjo6YWZ0ZXIge1xyXG4gICAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xyXG59XHJcbi5mci1wb3B1cCAuZnItYWN0aW9uLWJ1dHRvbnMgYnV0dG9uLmZyLWNvbW1hbmQge1xyXG4gICAgY29sb3I6ICRjb2xvci1ncmVlbi01ICFpbXBvcnRhbnQ7XHJcbn1cclxuLmZyLXBvcHVwIC5mci1jaGVja2JveC1saW5lIHtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbn1cclxuLmZyLXBvcHVwIHtcclxuICAgIGJvcmRlcjogMCAhaW1wb3J0YW50OyBcclxufVxyXG4uZnItcG9wdXAgLmZyLWJ1dHRvbnMge1xyXG4gIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcclxufVxyXG4uZnItcG9wdXAgLmZyLWFycm93IHtcclxuICAgIGJvcmRlci1ib3R0b206IDVweCBzb2xpZCAjOWZhOGIzICFpbXBvcnRhbnQ7XHJcbiAgICB0b3A6IC01cHggIWltcG9ydGFudDtcclxufVxyXG4uZnItcG9wdXAuZnItbW9iaWxlLmZyLWFjdGl2ZSB7XHJcbiAgICBib3gtc2hhZG93OiBub25lO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI0Q1RTBFQSAhaW1wb3J0YW50O1xyXG59XHJcbmJ1dHRvbiNsaW5rTGlzdC0xIHtcclxuICAgIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcclxufVxyXG4uZnItcG9wdXAgLmZyLWlucHV0LWxpbmUgaW5wdXRbdHlwZT10ZXh0XSwgLmZyLXBvcHVwIC5mci1pbnB1dC1saW5lIHRleHRhcmVhIHtcclxuICAgICY6Zm9jdXMge1xyXG4gICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAkY29sb3ItZ3JlZW4tNTtcclxuICAgICAgICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcbn1cclxuLmZyLXBvcHVwIC5mci1pbnB1dC1saW5lIGlucHV0LmZyLW5vdC1lbXB0eStsYWJlbCwgLmZyLXBvcHVwIC5mci1pbnB1dC1saW5lIHRleHRhcmVhLmZyLW5vdC1lbXB0eStsYWJlbCB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG59XHJcbi5mci1wb3B1cCAuZnItaW5wdXQtbGluZSBpbnB1dFt0eXBlPXRleHRdOmZvY3VzLCAuZnItcG9wdXAgLmZyLWlucHV0LWxpbmUgdGV4dGFyZWE6Zm9jdXMge1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMnB4IHNvbGlkICRjb2xvci1ncmVlbi01ICFpbXBvcnRhbnQ7XHJcbn1cclxuLmZyLXF1aWNrLWluc2VydC5mci12aXNpYmxlIHtcclxuICAgIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLmZyLXRvb2xiYXIgLmZyLWNvbW1hbmQuZnItYnRuIGksIC5mci1wb3B1cCAuZnItY29tbWFuZC5mci1idG4gaSwgLmZyLXRvb2xiYXIgLmZyLWNvbW1hbmQuZnItYnRuIHN2ZywgLmZyLXBvcHVwIC5mci1jb21tYW5kLmZyLWJ0biBzdmcge1xyXG4gZm9udDogbm9ybWFsIG5vcm1hbCBub3JtYWwgMTRweC8xIEZvbnRBd2Vzb21lICFpbXBvcnRhbnQ7XHJcbiBmb250LXNpemU6IDE4cHggIWltcG9ydGFudDsgICBcclxufVxyXG4uZnItYm94LmZyLWJhc2ljLmZyLXRvcCB7XHJcbiAgICBtYXJnaW4tdG9wOiA2cHg7XHJcbn0gXHJcbi5mci1wb3B1cCAuZnItaW5wdXQtbGluZSBpbnB1dCtsYWJlbCwgLmZyLXBvcHVwIC5mci1pbnB1dC1saW5lIHRleHRhcmVhK2xhYmVsIHtcclxuICAgIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcclxufVxyXG4uaW9zIHtcclxuICAgIC5mci13cmFwcGVyIC5mci1wbGFjZWhvbGRlciB7XHJcbiAgICAgICAgcGFkZGluZzogMTZweCAxNnB4IDIwcHggIWltcG9ydGFudDtcclxuICAgICAgICBmb250LXNpemU6IDE0cHggIWltcG9ydGFudDtcclxuICAgIH1cclxuICAgIC5mci10b29sYmFyIC5mci1jb21tYW5kLmZyLWJ0bi5mci1kcm9wZG93bi5mci1hY3RpdmU6aG92ZXIsIC5mci1wb3B1cCAuZnItY29tbWFuZC5mci1idG4uZnItZHJvcGRvd24uZnItYWN0aXZlOmhvdmVyLCAuZnItdG9vbGJhciAuZnItY29tbWFuZC5mci1idG4uZnItZHJvcGRvd24uZnItYWN0aXZlOmZvY3VzLCAuZnItcG9wdXAgLmZyLWNvbW1hbmQuZnItYnRuLmZyLWRyb3Bkb3duLmZyLWFjdGl2ZTpmb2N1cyB7XHJcbiAgICAgICAgaSB7XHJcbiAgICAgICAgICAgIGNvbG9yOiAkY29sb3ItZ3JlZW4tNSAhaW1wb3J0YW50O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4uZnItZWxlbWVudCB7XHJcbiAgICB1bCB7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAyMHB4ICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcbn1cclxuLmZyLXdyYXBwZXIuc2hvdy1wbGFjZWhvbGRlciAuZnItcGxhY2Vob2xkZXIge1xyXG4gICAgY29sb3I6ICRjb2xvci1ncmF5LTE1ICFpbXBvcnRhbnQgIDtcclxufVxyXG5cclxuIiwiLm1lc3NhZ2Uge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgLmxlZnQtaXRlbSB7XHJcbiAgICBmbGV4LXNocmluazogMDtcclxuICAgIGltZyB7XHJcbiAgICAgIHdpZHRoOiAzMnB4O1xyXG4gICAgICBoZWlnaHQ6IDMycHg7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDdweDtcclxuICAgICAgb2JqZWN0LWZpdDogY292ZXI7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5taWRkbGUtaXRlbSB7XHJcbiAgICBtYXJnaW4tbGVmdDogMTJweDtcclxuICAgIG1heC13aWR0aDogY2FsYygxMDAlIC0gODVweCk7XHJcbiAgICAuY29udGFpbmVyIHtcclxuICAgICAgbWFyZ2luLWJvdHRvbTogMzJweDtcclxuICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAudGV4dC1jb250YWluZXIge1xyXG4gICAgICAgIHBhZGRpbmc6IDEwcHggMTJweDtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3ItZ3JheS0xNztcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAyMHB4IDIwcHggMjBweCAwO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDRweDtcclxuICAgICAgfVxyXG4gICAgICAudGltZS1jb250YWluZXIge1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICBtaW4td2lkdGg6IDEyNXB4O1xyXG4gICAgICAgIG9wYWNpdHk6IDAuNztcclxuICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gICYubm90LW1lIHtcclxuICAgIGRpcmVjdGlvbjogcnRsO1xyXG4gICAgLm1pZGRsZS1pdGVtIHtcclxuICAgICAgbWFyZ2luOiAwO1xyXG4gICAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbiAgICAgIG1heC13aWR0aDogY2FsYygxMDAlIC0gODVweCk7XHJcbiAgICAgIC5jb250YWluZXIge1xyXG4gICAgICAgIC50ZXh0LWNvbnRhaW5lciB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3ItZ3JlZW4tNTtcclxuICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgICAgICBjb2xvcjogJGNvbG9yLXdoaXRlO1xyXG4gICAgICAgICAgYm9yZGVyLXJhZGl1czogMjBweCAyMHB4IDBweCAyMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmxlZnQtaXRlbSB7XHJcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgd2lkdGg6IDMycHg7XHJcbiAgICAgIGltZyB7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIGJvdHRvbTogMzJweDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iLCJhcHAtY2FyZC1jb21tZW50IHtcclxuICAmOmxhc3QtY2hpbGQge1xyXG4gICAgLmNhcmQtY29tbWVudC1jb250YWluZXIge1xyXG4gICAgICBib3JkZXItYm90dG9tOiAwO1xyXG4gICAgICBwYWRkaW5nLWJvdHRvbTogMDtcclxuICAgIH1cclxuICB9XHJcbn1cclxuLmxpc3QtYm94ZXMge1xyXG4gIGhlaWdodDogNTRweDtcclxuICBib3JkZXI6IDFweCBzb2xpZCAkY29sb3ItZ3JheS0xMztcclxuICBib3JkZXItbGVmdDogMDtcclxuICBib3JkZXItcmlnaHQ6IDA7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBwYWRkaW5nOiAwIDIwcHg7XHJcbiAgbWFyZ2luLXRvcDogMjBweDtcclxuICAubGlzdC1ib3hlcy1jb250YWluZXIge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuXHJcbiAgICAuaXRlbSB7XHJcbiAgICAgIHdpZHRoOiAzMy4zMyU7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgIGJvcmRlci1yaWdodDogMXB4IHNvbGlkICRjb2xvci1ncmF5LTEzO1xyXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgJi5zdGFydCB7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xyXG4gICAgICAgIGltZyB7XHJcbiAgICAgICAgICAvLyAmOmFjdGl2ZSB7XHJcbiAgICAgICAgICAvLyAgIHRyYW5zZm9ybTogc2NhbGUoMS4xKTtcclxuICAgICAgICAgIC8vIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgJi5lbmQge1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGltZyB7XHJcbiAgICAgICAgaGVpZ2h0OiAyMHB4O1xyXG4gICAgICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgICB9XHJcblxyXG4gICAgICBzcGFuIHtcclxuICAgICAgICBtYXJnaW4tbGVmdDogMTJweDtcclxuICAgICAgfVxyXG5cclxuICAgICAgJjpsYXN0LWNoaWxkIHtcclxuICAgICAgICBib3JkZXI6IDA7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbi5jYXJkLWNvbW1lbnQge1xyXG4gIHBhZGRpbmc6IDIwcHg7XHJcbiAgcGFkZGluZy1ib3R0b206IDA7XHJcbiAgLmNhcmQtY29tbWVudC1jb250YWluZXIge1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICRjb2xvci1ncmF5LTEzO1xyXG4gICAgcGFkZGluZy1ib3R0b206IDIwcHg7XHJcbiAgICAuZmlyc3QtaXRlbSB7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIC5sZWZ0IHtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG5cclxuICAgICAgICAubGVmdC1pdGVtIHtcclxuICAgICAgICAgIGltZyB7XHJcbiAgICAgICAgICAgIHdpZHRoOiAzMnB4O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMycHg7XHJcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDdweDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5yaWdodC1pdGVtIHtcclxuICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMnB4O1xyXG4gICAgICAgICAgLnNtYWxsIHtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogOHB4O1xyXG4gICAgICAgICAgICBsZXR0ZXItc3BhY2luZzogMC4ycHg7XHJcbiAgICAgICAgICAgIGNvbG9yOiAkY29sb3ItZ3JheS0xNTtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDZweDtcclxuICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiA2cHg7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC50b3Age1xyXG4gICAgICAgICAgbWFyZ2luLXRvcDogLTNweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLmJvdHRvbSB7XHJcbiAgICAgICAgICBtYXJnaW4tdG9wOiAtNHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgLnJpZ2h0IHtcclxuICAgICAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5zZWNvbmQtaXRlbSB7XHJcbiAgICAgIG1hcmdpbi10b3A6IDE2cHg7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiIsIi8vbmF2YmFyIHNrZWxldG9uXHJcblxyXG4vLyBtYWluIGNhcmQgcGFnZSBrZWxldG9uXHJcbioge1xyXG4gIG1hcmdpbjogMDtcclxuICBwYWRkaW5nOiAwO1xyXG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbn1cclxuLm1haW4tcHJvZmlsZS1za2VsZXRvbiB7XHJcbiAgYmFja2dyb3VuZDogJGNvbG9yLXdoaXRlO1xyXG4gIC5tYWluLXByb2ZpbGUtc2tlbGV0b24tY29udGFpbmVyIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAyMzBweDtcclxuICAgIGJhY2tncm91bmQ6ICRjb2xvci1ncmF5LTE3O1xyXG4gICAgcGFkZGluZzogMCAyMHB4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgLm1haW4tcHJvZmlsZS1za2VsZXRvbi1zZWFyY2gge1xyXG4gICAgICBwYWRkaW5nLXRvcDogMjBweDtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgLmZpcnN0LWl0ZW0ge1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGhlaWdodDogMzJweDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAkY29sb3Itd2hpdGU7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEzcHg7XHJcbiAgICAgIH1cclxuICAgICAgLndpdGhMZWZ0SWNvbiB7XHJcbiAgICAgICAgd2lkdGg6IDI0cHg7XHJcbiAgICAgICAgaGVpZ2h0OiAyNHB4O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDA7XHJcbiAgICAgICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi9hc3NldHMvaWNvbnMvZGlhc3BvcmFJY29uL0JhY2tfV2hpdGUuc3ZnXCIpO1xyXG4gICAgICB9XHJcbiAgICAgIC5zZWNvbmQtaXRlbSB7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgICAgICAuZmlyc3QtaXRlbS1pY29uIHtcclxuICAgICAgICAgIGhlaWdodDogMjRweDtcclxuICAgICAgICAgIHdpZHRoOiAyNHB4O1xyXG4gICAgICAgICAgYm9yZGVyLXJhZGl1czogN3B4O1xyXG4gICAgICAgICAgYmFja2dyb3VuZDogJGNvbG9yLXdoaXRlO1xyXG4gICAgICAgICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuc2Vjb25kLWl0ZW0taWNvbiB7XHJcbiAgICAgICAgICBoZWlnaHQ6IDE1cHg7XHJcbiAgICAgICAgICB3aWR0aDogMjhweDtcclxuICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDdweDtcclxuICAgICAgICAgIGJhY2tncm91bmQ6ICRjb2xvci13aGl0ZTtcclxuICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAmLnNtYWxsLWhlaWdodCB7XHJcbiAgICAgIHBhZGRpbmctYm90dG9tOiA1Ni4yNSU7XHJcbiAgICAgIGhlaWdodDogMDtcclxuICAgIH1cclxuICB9XHJcbiAgLm1haW4tcHJvZmlsZS1za2VsZXRvbi1ib2R5IHtcclxuICAgIHBhZGRpbmc6IDFweCAyMHB4O1xyXG4gICAgLm1haW4tcHJvZmlsZS1za2VsZXRvbi1ib2R5LWNvbnRhaW5lciB7XHJcbiAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICBiYWNrZ3JvdW5kOiAkY29sb3Itd2hpdGU7XHJcbiAgICAgIG1hcmdpbi10b3A6IC03OHB4O1xyXG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDEzcHg7XHJcbiAgICAgIHBhZGRpbmctdG9wOiA1M3B4O1xyXG4gICAgICBwYWRkaW5nLWJvdHRvbTogMTZweDtcclxuICAgICAgYm9yZGVyOiAxcHggc29saWQgJGNvbG9yLWdyYXktMTc7XHJcbiAgICAgIGJveC1zaGFkb3c6IDAgNXB4IDE2cHggMCByZ2JhKDIyOSwgMjM2LCAyNDUsIDAuMzUpO1xyXG4gICAgICAuaW1hZ2Uge1xyXG4gICAgICAgIHdpZHRoOiAxMDBweDtcclxuICAgICAgICBoZWlnaHQ6IDEwMHB4O1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICRjb2xvci13aGl0ZTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICAgICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIGxlZnQ6IDUwJTtcclxuICAgICAgICB0b3A6IC01MHB4O1xyXG4gICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIDApO1xyXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICRjb2xvci1ncmF5LTE3O1xyXG4gICAgICB9XHJcbiAgICAgIC50eHQtc2VrZWxldG9uLWNvbnRhaW5lciB7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgLnRpdGxlIHtcclxuICAgICAgICAgIHdpZHRoOiA1MCU7XHJcbiAgICAgICAgICBoZWlnaHQ6IDE0cHg7XHJcbiAgICAgICAgICBtYXJnaW4tdG9wOiAxMnB4O1xyXG4gICAgICAgICAgYmFja2dyb3VuZDogJGNvbG9yLWdyYXktMTc7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zdWJ0aXRsZSB7XHJcbiAgICAgICAgICB3aWR0aDogMzAlO1xyXG4gICAgICAgICAgaGVpZ2h0OiAxMXB4O1xyXG4gICAgICAgICAgbWFyZ2luLXRvcDogMTJweDtcclxuICAgICAgICAgIGJhY2tncm91bmQ6ICRjb2xvci1ncmF5LTE3O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICAuZGVzYy1jb250YWluZXIge1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMTZweDtcclxuICAgICAgICAuaXRlbSB7XHJcbiAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAuaW1hZ2UtZGVzYyB7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzJweDtcclxuICAgICAgICAgICAgd2lkdGg6IDMycHg7XHJcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLWdyYXktMTc7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICAuaW1hZ2UtdGl0bGUge1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDEwcHg7XHJcbiAgICAgICAgICAgIHdpZHRoOiA2MHB4O1xyXG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiAycHg7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1ncmF5LTE3O1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxMXB4O1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICAmLnNpbXBsZSB7XHJcbiAgICAgICAgcGFkZGluZy10b3A6IDA7XHJcbiAgICAgICAgbWFyZ2luOiAwO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDA7XHJcbiAgICAgICAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMTNweDtcclxuICAgICAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMTNweDtcclxuICAgICAgICBib3JkZXItdG9wOiAwO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAmOjphZnRlciB7XHJcbiAgICBjb250ZW50OiBcIiBcIjtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIHdpZHRoOiAzMHB4O1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYW5pbWF0aW9uLW5hbWU6IHNoYWRvdztcclxuICAgIGFuaW1hdGlvbi1kdXJhdGlvbjogM3M7XHJcbiAgICBhbmltYXRpb24taXRlcmF0aW9uLWNvdW50OiBpbmZpbml0ZTtcclxuICAgIG9wYWNpdHk6IDAuNTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDJweDtcclxuICAgIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgcmdiYSgyNTUsIDI1NSwgMjU1LCAxKSwgcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjIpKTtcclxuICAgIHRvcDogMDtcclxuICB9XHJcbn1cclxuLm1haW4tcHJvZmlsZS1za2VsZXRvbi1idXR0b24ge1xyXG4gIGhlaWdodDogNTBweDtcclxuICB3aWR0aDogMTAwJTtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIC5maXJzdC1idXR0b24ge1xyXG4gICAgd2lkdGg6IGNhbGMoNTAlICsgMnB4KTtcclxuICAgIGJvcmRlci1yaWdodDogMXB4IHNvbGlkICRjb2xvci1ncmF5LTEzO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIHBhZGRpbmctdG9wOiAxNnB4O1xyXG4gICAgLmxvZ28ge1xyXG4gICAgICB3aWR0aDogMjRweDtcclxuICAgICAgaGVpZ2h0OiAyNHB4O1xyXG4gICAgICBiYWNrZ3JvdW5kOiAjZjBmNWZmO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiA3cHg7XHJcbiAgICB9XHJcbiAgICAudGV4dCB7XHJcbiAgICAgIHdpZHRoOiA0MCU7XHJcbiAgICAgIGhlaWdodDogMTJweDtcclxuICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbiAgICAgIGJhY2tncm91bmQ6ICNmMGY1ZmY7XHJcbiAgICB9XHJcbiAgICAmOmxhc3QtY2hpbGQge1xyXG4gICAgICB3aWR0aDogNTAlO1xyXG4gICAgICBib3JkZXItcmlnaHQ6IDA7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbkBrZXlmcmFtZXMgc2hhZG93IHtcclxuICAwJSB7XHJcbiAgICBsZWZ0OiAwO1xyXG4gIH1cclxuICAxMDAlIHtcclxuICAgIGxlZnQ6IGNhbGMoMTAwJSAtIDMwcHgpO1xyXG4gIH1cclxufVxyXG5cclxuLmJhci1za2VsZXRvbiB7XHJcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDU2cHgpO1xyXG4gIGhlaWdodDogNDRweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3ItZ3JheS0xNztcclxuICBib3JkZXItcmFkaXVzOiA4cHg7XHJcblxyXG4gICY6OmFmdGVyIHtcclxuICAgIGNvbnRlbnQ6IFwiIFwiO1xyXG4gICAgd2lkdGg6IDQ0cHg7XHJcbiAgICBoZWlnaHQ6IDQ0cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3ItZ3JheS0xNztcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHJpZ2h0OiAyMHB4O1xyXG4gIH1cclxufVxyXG5cclxuLy8gcHJvZmlsZSBza2VsZXRvblxyXG5cclxuLnByb2ZpbGUtc2tlbGV0b24ge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci13aGl0ZTtcclxuICBwYWRkaW5nOiAyN3B4IDA7XHJcbiAgcGFkZGluZy1ib3R0b206IDA7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIC5wcm9maWxlLXNrZWxldG9uLWNvbnRhaW5lciB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIC5iYWNrLWJ1dHRvbiB7XHJcbiAgICAgIHdpZHRoOiAyNHB4O1xyXG4gICAgICBoZWlnaHQ6IDI0cHg7XHJcbiAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi4vYXNzZXRzL2ljb25zL2RpYXNwb3JhSWNvbi9CYWNrX0dyZXkuc3ZnXCIpO1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIGxlZnQ6IDA7XHJcbiAgICB9XHJcbiAgICAucHJvZmlsZS1za2VsZXRvbi1pbWFnZSB7XHJcbiAgICAgIGhlaWdodDogODBweDtcclxuICAgICAgd2lkdGg6IDgwcHg7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDE4cHg7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1ncmF5LTE3O1xyXG4gICAgfVxyXG4gICAgLnR4dC1jb250YWluZXIge1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgLnR4dCB7XHJcbiAgICAgICAgd2lkdGg6IDcwJTtcclxuICAgICAgICBoZWlnaHQ6IDE2cHg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMnB4O1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1ncmF5LTE3O1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDE2cHg7XHJcbiAgICAgICAgd2lkdGg6IDU5LjglO1xyXG4gICAgICAgICYuc3VidGl0bGUge1xyXG4gICAgICAgICAgaGVpZ2h0OiAxMHB4O1xyXG4gICAgICAgICAgd2lkdGg6IDI5LjklO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgJjo6YWZ0ZXIge1xyXG4gICAgICBjb250ZW50OiBcIiBcIjtcclxuICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICB3aWR0aDogMzBweDtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICBhbmltYXRpb24tbmFtZTogc2hhZG93O1xyXG4gICAgICBhbmltYXRpb24tZHVyYXRpb246IDNzO1xyXG4gICAgICBhbmltYXRpb24taXRlcmF0aW9uLWNvdW50OiBpbmZpbml0ZTtcclxuICAgICAgb3BhY2l0eTogMC41O1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAycHg7XHJcbiAgICAgIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgcmdiYSgyNTUsIDI1NSwgMjU1LCAxKSwgcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjIpKTtcclxuICAgICAgdG9wOiAwO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5Aa2V5ZnJhbWVzIHNoYWRvdyB7XHJcbiAgMCUge1xyXG4gICAgbGVmdDogMDtcclxuICB9XHJcbiAgMTAwJSB7XHJcbiAgICBsZWZ0OiBjYWxjKDEwMCUgLSAzMHB4KTtcclxuICB9XHJcbn1cclxuXHJcbi8vbGlzdC1kZXRhaWxzXHJcblxyXG4ubGlzdC1kZXRhaWxzIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB3aWR0aDogMTAwJTtcclxuICBtYXJnaW4tdG9wOiAyNHB4O1xyXG4gIC5saXN0LWRldGFpbHMtaXRlbSB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMjUlO1xyXG4gICAgLmxpc3QtaW1hZ2Uge1xyXG4gICAgICBoZWlnaHQ6IDMycHg7XHJcbiAgICAgIHdpZHRoOiAzMnB4O1xyXG4gICAgICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1ncmF5LTE3O1xyXG4gICAgfVxyXG4gICAgLmdhbGxlcnktbGlzdC1pbWFnZSB7XHJcbiAgICAgIGhlaWdodDogMjUwcHg7XHJcbiAgICAgIHdpZHRoOiAyNTBweDtcclxuICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3ItZ3JheS0xNztcclxuICAgIH1cclxuICAgIC5saXN0LXR4dCB7XHJcbiAgICAgIGhlaWdodDogMTBweDtcclxuICAgICAgd2lkdGg6IDYwcHg7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDJweDtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLWdyYXktMTc7XHJcbiAgICAgIG1hcmdpbi10b3A6IDExcHg7XHJcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIH1cclxuICB9XHJcbiAgJjo6YWZ0ZXIge1xyXG4gICAgY29udGVudDogXCIgXCI7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB3aWR0aDogMzBweDtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGFuaW1hdGlvbi1uYW1lOiBsaXN0U2hhZG93O1xyXG4gICAgYW5pbWF0aW9uLWR1cmF0aW9uOiAzcztcclxuICAgIGFuaW1hdGlvbi1pdGVyYXRpb24tY291bnQ6IGluZmluaXRlO1xyXG4gICAgb3BhY2l0eTogMC41O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMnB4O1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KFxyXG4gICAgICB0byByaWdodCxcclxuICAgICAgcmdiYSgyNTUsIDI1NSwgMjU1LCAxKSxcclxuICAgICAgcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjUpLFxyXG4gICAgICByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuMilcclxuICAgICk7XHJcbiAgfVxyXG4gICYuc21hbGwge1xyXG4gICAgbWFyZ2luLXRvcDogMTZweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgLmxpc3QtZGV0YWlscy1pdGVtIHtcclxuICAgICAgd2lkdGg6IDMzLjMzJTtcclxuICAgICAgLmxpc3QtaW1hZ2Uge1xyXG4gICAgICAgIGhlaWdodDogMjBweDtcclxuICAgICAgICB3aWR0aDogMjBweDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA0cHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuQGtleWZyYW1lcyBsaXN0U2hhZG93IHtcclxuICAwJSB7XHJcbiAgICBsZWZ0OiAwO1xyXG4gIH1cclxuICAxMDAlIHtcclxuICAgIGxlZnQ6IGNhbGMoMTAwJSAtIDEwcHgpO1xyXG4gIH1cclxufVxyXG5cclxuLy9saXN0LWxlZnQtaW1hZ2VcclxuLmxpc3QtbGVmdC1pbWFnZS1za2VsZXRvbiB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIG1hcmdpbi10b3A6IDIycHg7XHJcbiAgcGFkZGluZy1ib3R0b206IDIycHg7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgJGNvbG9yLWdyYXktMTc7XHJcbiAgLmxpc3QtbGVmdC1pbWFnZS1jb250YWluZXIge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIHBhZGRpbmc6IDAgMTZweDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogODBweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1ncmF5LTE3O1xyXG4gICAgYm9yZGVyLXJhZGl1czogOHB4IDhweCAwIDA7XHJcbiAgICAuaW1hZ2Uge1xyXG4gICAgICBoZWlnaHQ6IDQ4cHg7XHJcbiAgICAgIHdpZHRoOiA0OHB4O1xyXG4gICAgICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci13aGl0ZTtcclxuICAgICAgZmxleC1zaHJpbms6IDA7XHJcbiAgICB9XHJcbiAgICAudHh0IHtcclxuICAgICAgaGVpZ2h0OiAxMHB4O1xyXG4gICAgICB3aWR0aDogNjAlO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAycHg7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci13aGl0ZTtcclxuICAgICAgbWFyZ2luLWxlZnQ6IDEycHg7XHJcbiAgICB9XHJcbiAgICAmOjphZnRlciB7XHJcbiAgICAgIGNvbnRlbnQ6IFwiIFwiO1xyXG4gICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgIHdpZHRoOiAzMHB4O1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIGFuaW1hdGlvbi1uYW1lOiBzaGFkb3c7XHJcbiAgICAgIGFuaW1hdGlvbi1kdXJhdGlvbjogM3M7XHJcbiAgICAgIGFuaW1hdGlvbi1pdGVyYXRpb24tY291bnQ6IGluZmluaXRlO1xyXG4gICAgICBvcGFjaXR5OiAwLjU7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDJweDtcclxuICAgICAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KFxyXG4gICAgICAgIHRvIHJpZ2h0LFxyXG4gICAgICAgIHJnYmEoMjQwLCAyNDUsIDI1NSwgMSksXHJcbiAgICAgICAgcmdiYSgyNDAsIDI0NSwgMjU1LCAwLjUpLFxyXG4gICAgICAgIHJnYmEoMjQwLCAyNDUsIDI1NSwgMC4yKVxyXG4gICAgICApO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuLy8gc2tlbGV0b24gc2VwZXJhdG9yXHJcblxyXG4uc2VwZXJhdG9yLXNrZWxldG9uIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGhlaWdodDogNTBweDtcclxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2YxZjNmNTtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBwYWRkaW5nLWJvdHRvbTogMjBweDtcclxuICAubGVmdCB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIC5pbWFnZSB7XHJcbiAgICAgIGhlaWdodDogMzJweDtcclxuICAgICAgd2lkdGg6IDMycHg7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLWdyYXktMTc7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC50eHQge1xyXG4gICAgaGVpZ2h0OiAxNnB4O1xyXG4gICAgd2lkdGg6IDEwMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMnB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLWdyYXktMTc7XHJcbiAgICBtYXJnaW4tbGVmdDogMTJweDtcclxuICB9XHJcbiAgLnJpZ2h0IHtcclxuICAgIGhlaWdodDogMjhweDtcclxuICAgIHdpZHRoOiA3MXB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogN3B4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLWdyYXktMTc7XHJcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICB9XHJcbn1cclxuXHJcbi8vIGNhcmQgc2tlbGV0b25cclxuXHJcbi5jYXJkLXNrZWxldG9uIHtcclxuXHJcbiAgcGFkZGluZy1ib3R0b206IDI0cHg7XHJcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICRjb2xvci1ncmF5LTE3O1xyXG4gIC5jYXJkLWltYWdlIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgcGFkZGluZy1ib3R0b206IDU2LjI1JTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3ItZ3JheS0xNztcclxuICB9XHJcbiAgLmxpc3QtY29udGFpbmVyIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIC5saXN0LXdpdGgtaW1hZ2Uge1xyXG4gICAgICBoZWlnaHQ6IDIwcHg7XHJcbiAgICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgICBib3JkZXItcmFkaXVzOiA0cHg7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1ncmF5LTE3O1xyXG4gICAgICBtYXJnaW4tdG9wOiAxMnB4O1xyXG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICY6OmFmdGVyIHtcclxuICAgICAgICBjb250ZW50OiBcIiBcIjtcclxuICAgICAgICBoZWlnaHQ6IDEwcHg7XHJcbiAgICAgICAgd2lkdGg6IDE0MHB4O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDJweDtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3ItZ3JheS0xNztcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgbGVmdDogMjhweDtcclxuICAgICAgICB0b3A6IDUwJTtcclxuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgwLCAtNTAlKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmxpc3Qge1xyXG4gICAgICBtYXJnaW4tdG9wOiAxNnB4O1xyXG4gICAgICBoZWlnaHQ6IDEwcHg7XHJcbiAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAycHg7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1ncmF5LTE3O1xyXG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICY6OmFmdGVyIHtcclxuICAgICAgICBjb250ZW50OiBcIiBcIjtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMHB4O1xyXG4gICAgICAgIHdpZHRoOiA3Ny43JTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAycHg7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLWdyYXktMTc7XHJcbiAgICAgICAgdG9wOiAxNnB4O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAubGlzdC10d28tdGV4dCB7XHJcbiAgICAgIGhlaWdodDogMTBweDtcclxuICAgICAgd2lkdGg6IDQwcHg7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDJweDtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLWdyYXktMTc7XHJcbiAgICAgIG1hcmdpbi10b3A6IDMwcHg7XHJcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgJjo6YWZ0ZXIge1xyXG4gICAgICAgIGNvbnRlbnQ6IFwiIFwiO1xyXG4gICAgICAgIGhlaWdodDogMTBweDtcclxuICAgICAgICB3aWR0aDogMTAwcHg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMnB4O1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1ncmF5LTE3O1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICBsZWZ0OiA0OHB4O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAmOjphZnRlciB7XHJcbiAgICAgIGNvbnRlbnQ6IFwiIFwiO1xyXG4gICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgIHdpZHRoOiAzMHB4O1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIGFuaW1hdGlvbi1uYW1lOiBzaGFkb3c7XHJcbiAgICAgIGFuaW1hdGlvbi1kdXJhdGlvbjogM3M7XHJcbiAgICAgIGFuaW1hdGlvbi1pdGVyYXRpb24tY291bnQ6IGluZmluaXRlO1xyXG4gICAgICBvcGFjaXR5OiAwLjU7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDJweDtcclxuICAgICAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCByZ2JhKDI1NSwgMjU1LCAyNTUsIDEpLCByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuMikpO1xyXG4gICAgICB0b3A6IDA7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG4vLyBsaXN0IHdpdGggcmlnaHQgaW1hZ2VcclxuYXBwLXNrZWxldG9uIHtcclxuICAmOmxhc3QtY2hpbGQge1xyXG4gICAgLmxpc3QtcmlnaHQtaW1hZ2Uge1xyXG4gICAgICBib3JkZXItYm90dG9tOiAwO1xyXG4gICAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcclxuICAgIH1cclxuICB9XHJcbn1cclxuLmxpc3QtcmlnaHQtaW1hZ2Uge1xyXG4gIHBhZGRpbmc6IDIwcHggMDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAkY29sb3ItZ3JheS0xMztcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgLmxlZnQge1xyXG4gICAgd2lkdGg6IGNhbGMoMTAwJSAtIDk2cHgpO1xyXG4gICAgLmxpc3Qtd2l0aC1pbWFnZSB7XHJcbiAgICAgIGhlaWdodDogMjBweDtcclxuICAgICAgd2lkdGg6IDIwcHg7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDRweDtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLWdyYXktMTc7XHJcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgJjo6YWZ0ZXIge1xyXG4gICAgICAgIGNvbnRlbnQ6IFwiIFwiO1xyXG4gICAgICAgIGhlaWdodDogMTBweDtcclxuICAgICAgICB3aWR0aDogMTQwcHg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMnB4O1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1ncmF5LTE3O1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICBsZWZ0OiAyOHB4O1xyXG4gICAgICAgIHRvcDogNTAlO1xyXG4gICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKDAsIC01MCUpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAubGlzdCB7XHJcbiAgICAgIG1hcmdpbi10b3A6IDE2cHg7XHJcbiAgICAgIGhlaWdodDogMTBweDtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDJweDtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLWdyYXktMTc7XHJcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgJjo6YWZ0ZXIge1xyXG4gICAgICAgIGNvbnRlbnQ6IFwiIFwiO1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICBoZWlnaHQ6IDEwcHg7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMnB4O1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1ncmF5LTE3O1xyXG4gICAgICAgIHRvcDogMTZweDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmxpc3QtdHdvLXRleHQge1xyXG4gICAgICBoZWlnaHQ6IDEwcHg7XHJcbiAgICAgIHdpZHRoOiA0MHB4O1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAycHg7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1ncmF5LTE3O1xyXG4gICAgICBtYXJnaW4tdG9wOiAzMHB4O1xyXG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICY6OmFmdGVyIHtcclxuICAgICAgICBjb250ZW50OiBcIiBcIjtcclxuICAgICAgICBoZWlnaHQ6IDEwcHg7XHJcbiAgICAgICAgd2lkdGg6IDEwMHB4O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDJweDtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3ItZ3JheS0xNztcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgbGVmdDogNDhweDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuICAucmlnaHQge1xyXG4gICAgaGVpZ2h0OiA4MHB4O1xyXG4gICAgd2lkdGg6IDgwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxNnB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLWdyYXktMTc7XHJcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICB9XHJcbiAgJjo6YWZ0ZXIge1xyXG4gICAgY29udGVudDogXCIgXCI7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB3aWR0aDogMzBweDtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGFuaW1hdGlvbi1uYW1lOiBzaGFkb3c7XHJcbiAgICBhbmltYXRpb24tZHVyYXRpb246IDNzO1xyXG4gICAgYW5pbWF0aW9uLWl0ZXJhdGlvbi1jb3VudDogaW5maW5pdGU7XHJcbiAgICBvcGFjaXR5OiAwLjU7XHJcbiAgICBib3JkZXItcmFkaXVzOiAycHg7XHJcbiAgICB0b3A6IDA7XHJcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsIHJnYmEoMjU1LCAyNTUsIDI1NSwgMSksIHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4yKSk7XHJcbiAgfVxyXG4gIC8vICAgJjpsYXN0LWNoaWxkIHtcclxuICAvLyAgICAgYm9yZGVyLWJvdHRvbTogMDtcclxuICAvLyAgIH1cclxufVxyXG4vLyBsaXN0IG9mXHJcblxyXG4uc2xpZGUtbGlzdC1za2VsZXRvbiB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgcGFkZGluZy10b3A6IDEwcHg7XHJcbiAgLmxlZnQtaW1hZ2Uge1xyXG4gICAgaGVpZ2h0OiA1NHB4O1xyXG4gICAgd2lkdGg6IDU0cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gICAgZmxleC1zaHJpbms6IDA7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3ItZ3JheS0xNztcclxuICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgfVxyXG4gIC5taWRkbGUge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBtYXJnaW4tbGVmdDogMTBweDtcclxuICAgIHBhZGRpbmctYm90dG9tOiAxNnB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgJGNvbG9yLWdyYXktMTM7XHJcbiAgICAubGlzdC1jb250YWluZXIge1xyXG4gICAgICBwYWRkaW5nLXJpZ2h0OiAxOHB4O1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgLmxpc3Qge1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDMycHg7XHJcbiAgICAgICAgaGVpZ2h0OiAxMHB4O1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDJweDtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3ItZ3JheS0xNztcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgJjo6YWZ0ZXIge1xyXG4gICAgICAgICAgY29udGVudDogXCIgXCI7XHJcbiAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICBoZWlnaHQ6IDEwcHg7XHJcbiAgICAgICAgICB3aWR0aDogNTIlO1xyXG4gICAgICAgICAgYm9yZGVyLXJhZGl1czogMnB4O1xyXG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLWdyYXktMTc7XHJcbiAgICAgICAgICB0b3A6IC0xOXB4O1xyXG4gICAgICAgICAgbGVmdDogMDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgLmxpc3Qtd2l0aC1pbWFnZSB7XHJcbiAgICAgICAgaGVpZ2h0OiAyMHB4O1xyXG4gICAgICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDRweDtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3ItZ3JheS0xNztcclxuICAgICAgICBtYXJnaW4tdG9wOiA4cHg7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgICY6OmFmdGVyIHtcclxuICAgICAgICAgIGNvbnRlbnQ6IFwiIFwiO1xyXG4gICAgICAgICAgaGVpZ2h0OiAxMHB4O1xyXG4gICAgICAgICAgd2lkdGg6IDE0MHB4O1xyXG4gICAgICAgICAgYm9yZGVyLXJhZGl1czogMnB4O1xyXG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLWdyYXktMTc7XHJcbiAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICBsZWZ0OiAyOHB4O1xyXG4gICAgICAgICAgdG9wOiA1MCU7XHJcbiAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgwLCAtNTAlKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgLnJpZ2h0IHtcclxuICAgIGhlaWdodDogMjhweDtcclxuICAgIHdpZHRoOiAyOHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTRweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1ncmF5LTE3O1xyXG4gICAgZmxleC1zaHJpbms6IDA7XHJcbiAgfVxyXG4gICY6bGFzdC1jaGlsZCB7XHJcbiAgICAubWlkZGxlIHtcclxuICAgICAgYm9yZGVyLWJvdHRvbTogMDtcclxuICAgIH1cclxuICB9XHJcbiAgJjo6YWZ0ZXIge1xyXG4gICAgY29udGVudDogXCIgXCI7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB3aWR0aDogMzBweDtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGFuaW1hdGlvbi1uYW1lOiBzaGFkb3c7XHJcbiAgICBhbmltYXRpb24tZHVyYXRpb246IDNzO1xyXG4gICAgYW5pbWF0aW9uLWl0ZXJhdGlvbi1jb3VudDogaW5maW5pdGU7XHJcbiAgICBvcGFjaXR5OiAwLjU7XHJcbiAgICBib3JkZXItcmFkaXVzOiAycHg7XHJcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsIHJnYmEoMjU1LCAyNTUsIDI1NSwgMSksIHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4yKSk7XHJcbiAgICB0b3A6IDA7XHJcbiAgfVxyXG59XHJcblxyXG4vL3Njcm9sbCBidXR0b25cclxuXHJcbi5zY3JvbGwtYnV0dG9uLXNrZWxldG9uIHtcclxuICBwYWRkaW5nLWJvdHRvbTogMDtcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gIC5zY3JvbGwtYnV0dG9uLXNrZWxldG9uLWNvbnRhaW5lciB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAudGl0bGUge1xyXG4gICAgICBoZWlnaHQ6IDE2cHg7XHJcbiAgICAgIHdpZHRoOiA0OCU7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDJweDtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLWdyYXktMTc7XHJcbiAgICB9XHJcbiAgICAuYnV0dG9uLWNvbnRhaW5lciB7XHJcbiAgICAgIG1hcmdpbi10b3A6IDIzcHg7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIHBhZGRpbmctYm90dG9tOiAzMHB4O1xyXG4gICAgICAuYnV0dG9uIHtcclxuICAgICAgICBoZWlnaHQ6IDM5cHg7XHJcbiAgICAgICAgd2lkdGg6IDEwMHB4O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDdweDtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3ItZ3JheS0xNztcclxuICAgICAgICBmbGV4LXNocmluazogMDtcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgICY6OmFmdGVyIHtcclxuICAgICAgY29udGVudDogXCIgXCI7XHJcbiAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgd2lkdGg6IDMwcHg7XHJcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgYW5pbWF0aW9uLW5hbWU6IHNoYWRvdztcclxuICAgICAgYW5pbWF0aW9uLWR1cmF0aW9uOiAzcztcclxuICAgICAgYW5pbWF0aW9uLWl0ZXJhdGlvbi1jb3VudDogaW5maW5pdGU7XHJcbiAgICAgIG9wYWNpdHk6IDAuNTtcclxuICAgICAgYm9yZGVyLXJhZGl1czogMnB4O1xyXG4gICAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsIHJnYmEoMjU1LCAyNTUsIDI1NSwgMSksIHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4yKSk7XHJcbiAgICAgIHRvcDogMDtcclxuICAgIH1cclxuICB9XHJcbiAgJi53cmFwIHtcclxuICAgIC5idXR0b24tY29udGFpbmVyIHtcclxuICAgICAgZmxleC13cmFwOiB3cmFwO1xyXG4gICAgICBtYXJnaW4tdG9wOiAxMXB4O1xyXG4gICAgICAuYnV0dG9uIHtcclxuICAgICAgICBtYXJnaW4tdG9wOiAxMnB4O1xyXG4gICAgICAgICYud2lkdGgtODAge1xyXG4gICAgICAgICAgd2lkdGg6IDgwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbi5iaWctY292ZXItaW1hZ2Utc2tlbGV0b24ge1xyXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAkY29sb3ItZ3JheS0xNztcclxuICAuYmlnLWNvdmVyLWltYWdlLWNvbnRhaW5lciB7XHJcbiAgICBwYWRkaW5nOiAzMHB4IDA7XHJcbiAgICAuY292ZXItaW1hZ2Uge1xyXG4gICAgICBoZWlnaHQ6IDQzOXB4O1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3ItZ3JheS0xNztcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbi5nYWxsZXJ5LXNrZWxldG9uIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gIC5saXN0LWRldGFpbHMtaXRlbSB7XHJcbiAgICB3aWR0aDogMjUlO1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG4gICAgLmdhbGxlcnktbGlzdC1pbWFnZSB7XHJcbiAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICBwYWRkaW5nLXRvcDogMTAwJTtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLWdyYXktMTc7XHJcbiAgICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxuICAgICAgYm9yZGVyLXJhZGl1czogNnB4O1xyXG4gICAgfVxyXG4gICAgJjpsYXN0LWNoaWxkIHtcclxuICAgICAgbWFyZ2luLXJpZ2h0OiAwO1xyXG4gICAgfVxyXG4gIH1cclxuICAmOjphZnRlciB7XHJcbiAgICBjb250ZW50OiBcIiBcIjtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIHdpZHRoOiAzMHB4O1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYW5pbWF0aW9uLW5hbWU6IHNoYWRvdztcclxuICAgIGFuaW1hdGlvbi1kdXJhdGlvbjogM3M7XHJcbiAgICBhbmltYXRpb24taXRlcmF0aW9uLWNvdW50OiBpbmZpbml0ZTtcclxuICAgIG9wYWNpdHk6IDAuNTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDJweDtcclxuICAgIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgcmdiYSgyNTUsIDI1NSwgMjU1LCAxKSwgcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjIpKTtcclxuICAgIHRvcDogMDtcclxuICB9XHJcbn1cclxuLm1hcmtldC1za2VsZXRvbi1jb250YWluZXIge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgbWFyZ2luLXRvcDogMjBweDtcclxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgJGNvbG9yLWdyYXktMTM7XHJcbiAgLm1hcmtldC1pdGVtLXNrZWxldG9uIHtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICRjb2xvci1ncmF5LTEzO1xyXG4gICAgaGVpZ2h0OiAyMTFweDtcclxuICAgIHdpZHRoOiAxNTJweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDdweDtcclxuICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICAgICY6bGFzdC1jaGlsZCB7XHJcbiAgICAgIG1hcmdpbi1yaWdodDogMDtcclxuICAgIH1cclxuICAgIC5maXJzdC1pdGVtIHtcclxuICAgICAgaGVpZ2h0OiAxMzJweDtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1saWdodEJsdWU7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDdweDtcclxuICAgICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDA7XHJcbiAgICAgIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDA7XHJcbiAgICB9XHJcbiAgICAuc2Vjb25kLWl0ZW0ge1xyXG4gICAgICBwYWRkaW5nOiAxMHB4IDEzcHggMTNweCAxM3B4O1xyXG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgIC5maXJzdCB7XHJcbiAgICAgICAgaGVpZ2h0OiAxNHB4O1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1saWdodEJsdWU7XHJcbiAgICAgIH1cclxuICAgICAgLnNlY29uZCB7XHJcbiAgICAgICAgaGVpZ2h0OiAxNHB4O1xyXG4gICAgICAgIHdpZHRoOiA3MCU7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLWxpZ2h0Qmx1ZTtcclxuICAgICAgICBtYXJnaW4tdG9wOiA2cHg7XHJcbiAgICAgIH1cclxuICAgICAgJjo6YWZ0ZXIge1xyXG4gICAgICAgIGNvbnRlbnQ6IFwiIFwiO1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICB3aWR0aDogMzBweDtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgYW5pbWF0aW9uLW5hbWU6IHNoYWRvdztcclxuICAgICAgICBhbmltYXRpb24tZHVyYXRpb246IDNzO1xyXG4gICAgICAgIGFuaW1hdGlvbi1pdGVyYXRpb24tY291bnQ6IGluZmluaXRlO1xyXG4gICAgICAgIG9wYWNpdHk6IDAuNTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAycHg7XHJcbiAgICAgICAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCByZ2JhKDI1NSwgMjU1LCAyNTUsIDEpLCByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuMikpO1xyXG4gICAgICAgIHRvcDogMDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iLCIvLyBhcHBseSB0byBiZSBhbiBhbWJzc2Fkb3JcclxuXHJcbi5hcHBseVRvQW1iYXNzYWRyb3Ige1xyXG4gIHBhZGRpbmctYm90dG9tOiAzMHB4O1xyXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAkY29sb3ItZ3JheS0xMztcclxuICBwYWRkaW5nLWJvdHRvbTogMzBweDtcclxuICAudGl0bGUge1xyXG4gICAgbWFyZ2luLXRvcDogMzBweDtcclxuICB9XHJcbiAgLnN1YnRpdGxlIHtcclxuICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgfVxyXG59XHJcblxyXG4uYW1iLWNvbnRlbnQge1xyXG4gIHBhZGRpbmc6IDAgMjBweDtcclxuICBwYWRkaW5nLWJvdHRvbTogMjBweDtcclxuICAuaGVhZGVyLWFtYiB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMzBweDtcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAkY29sb3ItZ3JheS0xMztcclxuICAgIC50aXRsZSB7XHJcbiAgICAgIG1hcmdpbi10b3A6IDMwcHg7XHJcbiAgICB9XHJcbiAgICAuc3VidGl0bGUge1xyXG4gICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAzMHB4O1xyXG4gICAgfVxyXG4gICAgLmltYWdlIHtcclxuICAgICAgICB3aWR0aDogNjRweDtcclxuICAgIH1cclxuICB9XHJcbiAgLmxpc3Qge1xyXG4gICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxuICAgICAgbWFyZ2luLXRvcDogMzBweDtcclxuICAgICAgaW1nIHtcclxuICAgICAgICAgIG1hcmdpbi1yaWdodDogMTJweDtcclxuICAgICAgfVxyXG4gIH1cclxufVxyXG4iLCJpb24tdGFiLWJhciB7XHJcbiAgaGVpZ2h0OiA1MHB4O1xyXG59XHJcbmlvbi10YWItYnV0dG9uIHtcclxuICAtLXBhZGRpbmctc3RhcnQ6IDBweDtcclxuICAtLXBhZGRpbmctZW5kOiAwcHg7XHJcbiAgLS1yaXBwbGUtY29sb3I6ICMwMGQyYmQ7XHJcbn1cclxuXHJcbi50YWItYnV0dG9uLWljb24ge1xyXG4gICAgLy8gYmFja2dyb3VuZC1jb2xvcjogIzcwNzA3MCA7XHJcbiAgICAvLyAtd2Via2l0LW1hc2stc2l6ZTogMjBweCAhaW1wb3J0YW50O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgd2lkdGg6IDIwcHg7XHJcbiAgICBoZWlnaHQ6IDI0cHg7XHJcbiAgaW1nIHtcclxuICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgaGVpZ2h0OiAyNHB4O1xyXG4gICAgJi5hY3RpdmUge1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIGxlZnQ6IDA7XHJcbiAgICAgIHRvcDogMDtcclxuICAgICAgZGlzcGxheTogbm9uZTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuLmlvcyB7XHJcbiAgLnRhYi1idXR0b24taWNvbiB7XHJcbiAgICBoZWlnaHQ6IDI2cHg7XHJcbiAgfVxyXG59XHJcbi50YWItc2VsZWN0ZWQge1xyXG4gICAgLnRhYi1idXR0b24taWNvbiB7XHJcbiAgICAgICAgaW1nIHtcclxuICAgICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICAgICAgICAmLmFjdGl2ZSB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfSBcclxuICAgICAgLmJvdHRvbS1iYXItaW5hY3RpdmUge1xyXG4gICAgICAgICAgY29sb3I6ICAkY29sb3ItZ3JlZW4tNTtcclxuICAgICAgfVxyXG59XHJcbi50YWItY29udGFpbmVyLWljb24ge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAuaGFzTm90aWZpY2F0aW9uIHtcclxuICAgICYubWVzc2FnZSB7XHJcbiAgICAgIGhlaWdodDogMThweDtcclxuICAgICAgd2lkdGg6IDE4cHg7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgICAgYm9yZGVyOiAxLjVweCBzb2xpZCAjRkZGRkZGO1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3Itb3JhbmdlO1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICByaWdodDogLTZweDtcclxuICAgICAgdG9wOiAtNHB4O1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4uaWNvbi1jb250YWluZXIge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAuaGFzTm90aWZpY2F0aW9uIHtcclxuICAgICYuYmFyLW5vdGlmaWNhdGlvbiB7XHJcbiAgICAgIGhlaWdodDogMThweDtcclxuICAgICAgd2lkdGg6IDE4cHg7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgICAgYm9yZGVyOiAxLjVweCBzb2xpZCAjRkZGRkZGO1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3Itb3JhbmdlO1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICByaWdodDogLTZweDtcclxuICAgICAgdG9wOiAtNnB4O1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4uaW9zIHtcclxuICAudGFiLWNvbnRhaW5lci1pY29uIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIC5oYXNOb3RpZmljYXRpb24ge1xyXG4gICAgICAmLm1lc3NhZ2Uge1xyXG4gICAgICAgIHBhZGRpbmctbGVmdDogMXB4O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG4iLCIubm90aWZpY2F0aW9uLXRpdGxlIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBiYWNrZ3JvdW5kOiAkY29sb3Itd2hpdGU7XHJcbiAgei1pbmRleDogMTA7XHJcbn1cclxuLm5vdGlmaWNhdGlvbi10aXRsZS1jb250YWluZXIge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBwYWRkaW5nOiAyMHB4O1xyXG4gIC5ub3Qtc21hbGwtdGl0bGUtY29udGFpbmVyIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgICAubm90LXNtYWxsLXRpdGxlIHtcclxuICAgICAgbWFyZ2luLWxlZnQ6IDIwcHg7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbi5zZXR0aW5ncyB7XHJcbiAgICBwYWRkaW5nOiAwIDIwcHg7XHJcbiAgLnNldHRpbmctbm90IHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgJGNvbG9yLWdyYXktMTM7XHJcbiAgICAuc2V0dGluZy1jb250YWluZXIge1xyXG4gICAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgaGVpZ2h0OiAyMnB4O1xyXG4gICAgICBwYWRkaW5nLXJpZ2h0OiA4cHg7XHJcbiAgICAgIC5jb250YWluZXIge1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAzNXB4O1xyXG4gICAgICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gICAgICAgIHdpZHRoOiAxOHB4O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG4udW5yZWFkTm90aWZpY2F0aW9uIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3ItbGlnaHRCbHVlICFpbXBvcnRhbnQ7XHJcbn1cclxuIiwiLmFwcC1zdGF0dXMge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGhlaWdodDogMTAwJTtcclxuICBwYWRkaW5nOiAwIDIwcHg7XHJcbiAgLnRpdGxlIHtcclxuICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgfVxyXG4gIC5zdWJ0aXRsZSB7XHJcbiAgICBtYXJnaW4tdG9wOiA4cHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAzMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuICBhcHAtYnV0dG9uICB7XHJcbiAgICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxufVxyXG4iLCIuYWJvdXQge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIC5hYm91dC11cy1pbWFnZSB7XHJcbiAgICAgICAgd2lkdGg6IDQ5JTtcclxuICAgICAgICBwYWRkaW5nLXRvcDogNTAlO1xyXG4gICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcclxuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IDkwJTtcclxuICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gICAgfVxyXG59IiwiLy8gaHR0cDovL2lvbmljZnJhbWV3b3JrLmNvbS9kb2NzL3RoZW1pbmcvXHJcbkBpbXBvcnQgJ35AaW9uaWMvYW5ndWxhci9jc3MvY29yZS5jc3MnO1xyXG5AaW1wb3J0ICd+QGlvbmljL2FuZ3VsYXIvY3NzL25vcm1hbGl6ZS5jc3MnO1xyXG5AaW1wb3J0ICd+QGlvbmljL2FuZ3VsYXIvY3NzL3N0cnVjdHVyZS5jc3MnO1xyXG5AaW1wb3J0ICd+QGlvbmljL2FuZ3VsYXIvY3NzL3R5cG9ncmFwaHkuY3NzJztcclxuQGltcG9ydCAnfkBpb25pYy9hbmd1bGFyL2Nzcy9kaXNwbGF5LmNzcyc7XHJcbkBpbXBvcnQgJ35AaW9uaWMvYW5ndWxhci9jc3MvcGFkZGluZy5jc3MnO1xyXG5AaW1wb3J0ICd+QGlvbmljL2FuZ3VsYXIvY3NzL2Zsb2F0LWVsZW1lbnRzLmNzcyc7XHJcbkBpbXBvcnQgJ35AaW9uaWMvYW5ndWxhci9jc3MvdGV4dC1hbGlnbm1lbnQuY3NzJztcclxuQGltcG9ydCAnfkBpb25pYy9hbmd1bGFyL2Nzcy90ZXh0LXRyYW5zZm9ybWF0aW9uLmNzcyc7XHJcbkBpbXBvcnQgJ35AaW9uaWMvYW5ndWxhci9jc3MvZmxleC11dGlscy5jc3MnO1xyXG5AaW1wb3J0ICcuLi9zcmMvYXNzZXRzL3Njc3MvdmFyaWFibGUuc2Nzcyc7XHJcbkBpbXBvcnQgJy4uL3NyYy9hc3NldHMvc2Nzcy90eXBvZ3JhcGh5LnNjc3MnO1xyXG5AaW1wb3J0ICcuLi9zcmMvYXNzZXRzL3Njc3MvYnV0dG9uLnNjc3MnO1xyXG5AaW1wb3J0ICcuLi9zcmMvYXNzZXRzL3Njc3Mvc2VwYXJhdG9ycy5zY3NzJztcclxuQGltcG9ydCAnLi4vc3JjL2Fzc2V0cy9zY3NzL2xpc3Quc2Nzcyc7XHJcbkBpbXBvcnQgJy4uL3NyYy9hc3NldHMvc2Nzcy9uYXZiYXIuc2Nzcyc7XHJcbkBpbXBvcnQgJy4uL3NyYy9hc3NldHMvc2Nzcy9jYXJkLnNjc3MnOyBcclxuQGltcG9ydCAnLi4vc3JjL2Fzc2V0cy9zY3NzL2hpbnRzLnNjc3MnOyBcclxuQGltcG9ydCAnLi4vc3JjL2Fzc2V0cy9zY3NzL2dlbmVyYWwuc2Nzcyc7IFxyXG5AaW1wb3J0ICcuLi9zcmMvYXNzZXRzL3Njc3MvaW5wdXQuc2Nzcyc7IFxyXG5AaW1wb3J0ICcuLi9zcmMvYXNzZXRzL3Njc3MvaW52aXRlLnNjc3MnOyAgIFxyXG5AaW1wb3J0ICcuLi9zcmMvYXNzZXRzL3Njc3MvZm9udC5zY3NzJzsgXHJcbkBpbXBvcnQgJy4uL3NyYy9hc3NldHMvc2Nzcy9zcGFjaW5nLnNjc3MnOyBcclxuQGltcG9ydCAnLi4vc3JjL2Fzc2V0cy9zY3NzL3NlZ21lbnQtYnV0dG9uLnNjc3MnOyAgXHJcbkBpbXBvcnQgJy4uL3NyYy9hc3NldHMvc2Nzcy9nYWxsZXJ5LnNjc3MnOyAgXHJcbkBpbXBvcnQgJy4uL3NyYy9hc3NldHMvc2Nzcy9hY3Rpb25TaGVldC5zY3NzJzsgIFxyXG5AaW1wb3J0ICcuLi9zcmMvYXNzZXRzL3Njc3MvYnV0dG9uLXR5cG9ncmFwaHkuc2Nzcyc7ICBcclxuQGltcG9ydCAnLi4vc3JjL2Fzc2V0cy9zY3NzL2ludmlzaW9uLXR5cG9ncmFwaHkuc2Nzcyc7ICBcclxuQGltcG9ydCAnLi4vc3JjL2Fzc2V0cy9zY3NzL3BhZ2VzL2xhbmRpbmQtcGFnZS5zY3NzJzsgIFxyXG5AaW1wb3J0ICcuLi9zcmMvYXNzZXRzL3Njc3MvcGFnZXMvbG9naW4tcGFnZS5zY3NzJzsgIFxyXG5AaW1wb3J0ICcuLi9zcmMvYXNzZXRzL3Njc3MvZW1wdHktc3RhdGUuc2Nzcyc7ICBcclxuQGltcG9ydCAnLi4vc3JjL2Fzc2V0cy9zY3NzL2FsZXJ0LWNvbnRyb2wuc2Nzcyc7ICBcclxuQGltcG9ydCAnLi4vc3JjL2Fzc2V0cy9zY3NzL2JvcmRlcnMuc2Nzcyc7IFxyXG5AaW1wb3J0ICcuLi9zcmMvYXNzZXRzL3Njc3MvY29tcG9uZW50LnNjc3MnOyBcclxuQGltcG9ydCAnLi4vc3JjL2Fzc2V0cy9zY3NzL2VkaXRvci5zY3NzJzsgXHJcbkBpbXBvcnQgJy4uL3NyYy9hc3NldHMvc2Nzcy9jb252ZXJzYXRpb24uc2Nzcyc7IFxyXG5AaW1wb3J0ICcuLi9zcmMvYXNzZXRzL3Njc3MvY29tbWVudC5zY3NzJzsgXHJcbkBpbXBvcnQgJy4uL3NyYy9hc3NldHMvc2Nzcy9za2VsZXRvbi5zY3NzJzsgXHJcbkBpbXBvcnQgJy4uL3NyYy9hc3NldHMvc2Nzcy9hbWJhc3NhZG9yLnNjc3MnO1xyXG5AaW1wb3J0ICcuLi9zcmMvYXNzZXRzL3Njc3MvbWFpbi10YWJzLnNjc3MnO1xyXG5AaW1wb3J0ICcuLi9zcmMvYXNzZXRzL3Njc3Mvbm90aWZpY2F0aW9ucy5zY3NzJztcclxuQGltcG9ydCAnLi4vc3JjL2Fzc2V0cy9zY3NzL2FwcC1zdGF0dXMuc2Nzcyc7XHJcbkBpbXBvcnQgJy4uL3NyYy9hc3NldHMvc2Nzcy9hYm91dC5zY3NzJztcclxuQGltcG9ydCAnLi4vc3JjL2Fzc2V0cy9zY3NzL2ZvbnQtYXdlc29tZS5zY3NzJztcclxuLnN3aXBlci1jb250YWluZXItaG9yaXpvbnRhbD4uc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0cywgLnN3aXBlci1wYWdpbmF0aW9uLWN1c3RvbSwgLnN3aXBlci1wYWdpbmF0aW9uLWZyYWN0aW9uIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZSAhaW1wb3J0YW50O1xyXG4gICAgYm90dG9tOiAtNXB4O1xyXG4gICAgcGFkZGluZy10b3A6IDIwcHg7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMDtcclxuLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldCB7XHJcbiAgICBvcGFjaXR5OiAxO1xyXG59XHJcbiAgICAuc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0LWFjdGl2ZSB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogIzAwZDJiZDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5mci10b29sYmFyIC5mci1jb21tYW5kLmZyLWJ0biBzdmcgcGF0aCwgLmZyLXBvcHVwIC5mci1jb21tYW5kLmZyLWJ0biBzdmcgcGF0aCwgLmZyLW1vZGFsIC5mci1jb21tYW5kLmZyLWJ0biBzdmcgcGF0aCB7XHJcbiAgICAgIGZpbGw6ICM5ZmE4YjMgIWltcG9ydGFudDtcclxuICB9XHJcbiAgLmZyLXRvb2xiYXIgLmZyLXRhYnMgLmZyLWNvbW1hbmQuZnItYnRuLCAuZnItcG9wdXAgLmZyLXRhYnMgLmZyLWNvbW1hbmQuZnItYnRuLCAuZnItbW9kYWwgLmZyLXRhYnMgLmZyLWNvbW1hbmQuZnItYnRuIHtcclxuICAgICAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xyXG4gIH1cclxuICAuaW9zIHtcclxuICAgICAgaW9uLWZvb3RlciBpb24tdG9vbGJhcjpsYXN0LWNoaWxkIHtcclxuICAgICAgICAgIHBhZGRpbmc6IDAgICFpbXBvcnRhbnQ7XHJcbiAgICAgIH1cclxuICB9XHJcbiAgXHJcbiAgLmJsYWNrQm9keSB7XHJcbiAgICAuaW9zIHtcclxuICAgICAgICBpb24tZm9vdGVyIGlvbi10b29sYmFyOmxhc3QtY2hpbGQge1xyXG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogdmFyKC0taW9uLXNhZmUtYXJlYS1ib3R0b20sMCkgIWltcG9ydGFudDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5jZW50ZXItc2xpZGUge1xyXG4gICAgcGFkZGluZy1ib3R0b206IDEwcHg7XHJcbiAgLmNhcm91c2VsLWNlbGwge1xyXG4gICAgICB3aWR0aDogMTg2cHg7XHJcbiAgICAgIGhlaWdodDogMTQ2cHggO1xyXG4gICAgICBvcGFjaXR5OiAwLjU7XHJcbiAgICB9XHJcbiAgICAuc3dpcGVyLXNsaWRlLWFjdGl2ZSB7XHJcbiAgICAgIC5jYXJvdXNlbC1jZWxsIHtcclxuICAgICAgd2lkdGg6IDE4NnB4O1xyXG4gICAgICBoZWlnaHQ6IDE0NnB4IDtcclxuICAgICAgb3BhY2l0eTogMTtcclxuICAgICAgdHJhbnNmb3JtOiBzY2FsZSgxLjEpO1xyXG4gICAgICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gLjJzIGVhc2Utb3V0O1xyXG4gICAgICB9XHJcbiAgICAgIC5jYXJkLXRleHQge1xyXG4gICAgICAgICAgdG9wOiA0MnB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgIH1cclxuICB9XHJcbiAgLnN3aXBlci1zbGlkZSB7XHJcbiAgICAgIHdpZHRoOiAxODRweCAhaW1wb3J0YW50O1xyXG4gIH1cclxufVxyXG4uZmlsdGVyIHtcclxuICAgIGZpbHRlcjogdXJsKCNkdW90b25lKTtcclxufSJdfQ== */
/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5zY3NzIn0= *//*!
 * froala_editor v2.9.6 (https://www.froala.com/wysiwyg-editor)
 * License https://froala.com/wysiwyg-editor/terms/
 * Copyright 2014-2019 Froala Labs
 */

.clearfix::after{clear:both;display:block;content:&quot;&quot;;height:0}

.hide-by-clipping{position:absolute;width:1px;height:1px;padding:0;margin:-1px;overflow:hidden;clip:rect(0,0,0,0);border:0}

.fr-element,.fr-element:focus{outline:0 solid transparent}

.fr-box.fr-basic .fr-element{color:#000;padding:16px;box-sizing:border-box;overflow-x:auto;min-height:52px}

.fr-box.fr-basic.fr-rtl .fr-element{text-align:right}

.fr-element{background:0 0;position:relative;z-index:2;-webkit-user-select:auto}

.fr-element a{user-select:auto;-o-user-select:auto;-moz-user-select:auto;-khtml-user-select:auto;-webkit-user-select:auto;-ms-user-select:auto}

.fr-element.fr-disabled{user-select:none;-o-user-select:none;-moz-user-select:none;-khtml-user-select:none;-webkit-user-select:none;-ms-user-select:none}

.fr-element [contenteditable=true]{outline:0 solid transparent}

.fr-box a.fr-floating-btn{box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 1px 1px rgba(0,0,0,.16);border-radius:100%;-moz-border-radius:100%;-webkit-border-radius:100%;-moz-background-clip:padding;-webkit-background-clip:padding-box;background-clip:padding-box;height:32px;width:32px;background:#fff;color:#1e88e5;-webkit-transition:background .2s ease 0s,color .2s ease 0s,transform .2s ease 0s;-moz-transition:background .2s ease 0s,color .2s ease 0s,transform .2s ease 0s;-ms-transition:background .2s ease 0s,color .2s ease 0s,transform .2s ease 0s;-o-transition:background .2s ease 0s,color .2s ease 0s,transform .2s ease 0s;outline:0;left:0;top:0;line-height:32px;-webkit-transform:scale(0);-moz-transform:scale(0);-ms-transform:scale(0);-o-transform:scale(0);text-align:center;display:block;box-sizing:border-box;border:0}

.fr-box a.fr-floating-btn svg{-webkit-transition:transform .2s ease 0s;-moz-transition:transform .2s ease 0s;-ms-transition:transform .2s ease 0s;-o-transition:transform .2s ease 0s;fill:#1e88e5}

.fr-box a.fr-floating-btn i,.fr-box a.fr-floating-btn svg{font-size:14px;line-height:32px}

.fr-box a.fr-floating-btn.fr-btn+.fr-btn{margin-left:10px}

.fr-box a.fr-floating-btn:hover{background:#ebebeb;cursor:pointer}

.fr-box a.fr-floating-btn:hover svg{fill:#1e88e5}

.fr-box .fr-visible a.fr-floating-btn{-webkit-transform:scale(1);-moz-transform:scale(1);-ms-transform:scale(1);-o-transform:scale(1)}

iframe.fr-iframe{width:100%;border:0;position:relative;display:block;z-index:2;box-sizing:border-box}

.fr-wrapper{position:relative;z-index:1}

.fr-wrapper::after{clear:both;display:block;content:&quot;&quot;;height:0}

.fr-wrapper .fr-placeholder{position:absolute;font-size:12px;color:#aaa;z-index:1;display:none;top:0;left:0;right:0;overflow:hidden}

.fr-wrapper.show-placeholder .fr-placeholder{display:block}

.fr-wrapper ::-moz-selection{background:#b5d6fd;color:#000}

.fr-wrapper ::selection{background:#b5d6fd;color:#000}

.fr-box.fr-basic .fr-wrapper{background:#fff;border:0;border-top:0;top:0;left:0}

.fr-box.fr-basic.fr-top .fr-wrapper{border-top:0;border-radius:0 0 2px 2px;-moz-border-radius:0 0 2px 2px;-webkit-border-radius:0 0 2px 2px;-moz-background-clip:padding;-webkit-background-clip:padding-box;background-clip:padding-box;box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 1px 1px rgba(0,0,0,.16)}

.fr-box.fr-basic.fr-bottom .fr-wrapper{border-bottom:0;border-radius:2px 2px 0 0;-moz-border-radius:2px 2px 0 0;-webkit-border-radius:2px 2px 0 0;-moz-background-clip:padding;-webkit-background-clip:padding-box;background-clip:padding-box;box-shadow:0 -1px 3px rgba(0,0,0,.12),0 -1px 1px 1px rgba(0,0,0,.16)}

@media (min-width:992px){.fr-box.fr-document{min-width:21cm}.fr-box.fr-document .fr-wrapper{text-align:left;padding:30px;min-width:21cm;background:#EFEFEF}.fr-box.fr-document .fr-wrapper .fr-element{text-align:left;background:#FFF;width:21cm;margin:auto;min-height:26cm!important;padding:1cm 2cm;box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 1px 1px rgba(0,0,0,.16);overflow:visible;z-index:auto}.fr-box.fr-document .fr-wrapper .fr-element hr{margin-left:-2cm;margin-right:-2cm;background:#EFEFEF;height:1cm;outline:0;border:0}.fr-box.fr-document .fr-wrapper .fr-element img{z-index:1}}

.fr-tooltip{position:absolute;top:0;left:0;padding:0 8px;border-radius:2px;-moz-border-radius:2px;-webkit-border-radius:2px;-moz-background-clip:padding;-webkit-background-clip:padding-box;background-clip:padding-box;box-shadow:0 3px 6px rgba(0,0,0,.16),0 2px 2px 1px rgba(0,0,0,.14);background:#222;color:#fff;font-size:11px;line-height:22px;font-family:Arial,Helvetica,sans-serif;-webkit-transition:opacity .2s ease 0s;-moz-transition:opacity .2s ease 0s;-ms-transition:opacity .2s ease 0s;-o-transition:opacity .2s ease 0s;-webkit-opacity:0;-moz-opacity:0;opacity:0;-ms-filter:&quot;alpha(Opacity=0)&quot;;left:-3000px;user-select:none;-o-user-select:none;-moz-user-select:none;-khtml-user-select:none;-webkit-user-select:none;-ms-user-select:none;z-index:2147483647;text-rendering:optimizelegibility;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}

.fr-tooltip.fr-visible{-webkit-opacity:1;-moz-opacity:1;opacity:1;-ms-filter:&quot;alpha(Opacity=0)&quot;}

.fr-toolbar .fr-btn-wrap,.fr-popup .fr-btn-wrap{float:left;white-space:nowrap;position:relative}

.fr-toolbar .fr-btn-wrap.fr-hidden,.fr-popup .fr-btn-wrap.fr-hidden{display:none}

.fr-toolbar .fr-command.fr-btn,.fr-popup .fr-command.fr-btn{background:0 0;color:#222;-moz-outline:0;outline:0;border:0;line-height:1;cursor:pointer;text-align:left;margin:0 2px;-webkit-transition:background .2s ease 0s;-moz-transition:background .2s ease 0s;-ms-transition:background .2s ease 0s;-o-transition:background .2s ease 0s;border-radius:0;-moz-border-radius:0;-webkit-border-radius:0;-moz-background-clip:padding;-webkit-background-clip:padding-box;background-clip:padding-box;z-index:2;position:relative;box-sizing:border-box;text-decoration:none;user-select:none;-o-user-select:none;-moz-user-select:none;-khtml-user-select:none;-webkit-user-select:none;-ms-user-select:none;float:left;padding:0;width:38px;height:38px}

.fr-toolbar .fr-command.fr-btn::-moz-focus-inner,.fr-popup .fr-command.fr-btn::-moz-focus-inner{border:0;padding:0}

.fr-toolbar .fr-command.fr-btn.fr-btn-text,.fr-popup .fr-command.fr-btn.fr-btn-text{width:auto}

.fr-toolbar .fr-command.fr-btn i,.fr-popup .fr-command.fr-btn i,.fr-toolbar .fr-command.fr-btn svg,.fr-popup .fr-command.fr-btn svg{display:block;font-size:14px;width:14px;margin:12px;text-align:center;float:none}

.fr-toolbar .fr-command.fr-btn span.fr-sr-only,.fr-popup .fr-command.fr-btn span.fr-sr-only{position:absolute;width:1px;height:1px;padding:0;margin:-1px;overflow:hidden;clip:rect(0,0,0,0);border:0}

.fr-toolbar .fr-command.fr-btn span,.fr-popup .fr-command.fr-btn span{font-size:14px;display:block;line-height:17px;min-width:34px;float:left;text-overflow:ellipsis;overflow:hidden;white-space:nowrap;height:17px;font-weight:700;padding:0 2px}

.fr-toolbar .fr-command.fr-btn img,.fr-popup .fr-command.fr-btn img{margin:12px;width:14px}

.fr-toolbar .fr-command.fr-btn.fr-active,.fr-popup .fr-command.fr-btn.fr-active{color:#1e88e5;background:0 0}

.fr-toolbar .fr-command.fr-btn.fr-dropdown.fr-selection,.fr-popup .fr-command.fr-btn.fr-dropdown.fr-selection{width:auto}

.fr-toolbar .fr-command.fr-btn.fr-dropdown.fr-selection span,.fr-popup .fr-command.fr-btn.fr-dropdown.fr-selection span{font-weight:400}

.fr-toolbar .fr-command.fr-btn.fr-dropdown i,.fr-popup .fr-command.fr-btn.fr-dropdown i,.fr-toolbar .fr-command.fr-btn.fr-dropdown span,.fr-popup .fr-command.fr-btn.fr-dropdown span,.fr-toolbar .fr-command.fr-btn.fr-dropdown img,.fr-popup .fr-command.fr-btn.fr-dropdown img,.fr-toolbar .fr-command.fr-btn.fr-dropdown svg,.fr-popup .fr-command.fr-btn.fr-dropdown svg{margin-left:8px;margin-right:16px}

.fr-toolbar .fr-command.fr-btn.fr-dropdown.fr-active,.fr-popup .fr-command.fr-btn.fr-dropdown.fr-active{color:#222;background:#d6d6d6}

.fr-toolbar .fr-command.fr-btn.fr-dropdown.fr-active:hover,.fr-popup .fr-command.fr-btn.fr-dropdown.fr-active:hover,.fr-toolbar .fr-command.fr-btn.fr-dropdown.fr-active:focus,.fr-popup .fr-command.fr-btn.fr-dropdown.fr-active:focus{background:#d6d6d6!important;color:#222!important}

.fr-toolbar .fr-command.fr-btn.fr-dropdown.fr-active:hover::after,.fr-popup .fr-command.fr-btn.fr-dropdown.fr-active:hover::after,.fr-toolbar .fr-command.fr-btn.fr-dropdown.fr-active:focus::after,.fr-popup .fr-command.fr-btn.fr-dropdown.fr-active:focus::after{border-top-color:#222!important}

.fr-toolbar .fr-command.fr-btn.fr-dropdown::after,.fr-popup .fr-command.fr-btn.fr-dropdown::after{position:absolute;width:0;height:0;border-left:4px solid transparent;border-right:4px solid transparent;border-top:4px solid #222;right:4px;top:17px;content:&quot;&quot;}

.fr-toolbar .fr-command.fr-btn.fr-disabled,.fr-popup .fr-command.fr-btn.fr-disabled{color:#bdbdbd;cursor:default}

.fr-toolbar .fr-command.fr-btn.fr-disabled::after,.fr-popup .fr-command.fr-btn.fr-disabled::after{border-top-color:#bdbdbd!important}

.fr-toolbar .fr-command.fr-btn.fr-hidden,.fr-popup .fr-command.fr-btn.fr-hidden{display:none}

.fr-toolbar.fr-disabled .fr-btn,.fr-popup.fr-disabled .fr-btn,.fr-toolbar.fr-disabled .fr-btn.fr-active,.fr-popup.fr-disabled .fr-btn.fr-active{color:#bdbdbd}

.fr-toolbar.fr-disabled .fr-btn.fr-dropdown::after,.fr-popup.fr-disabled .fr-btn.fr-dropdown::after,.fr-toolbar.fr-disabled .fr-btn.fr-active.fr-dropdown::after,.fr-popup.fr-disabled .fr-btn.fr-active.fr-dropdown::after{border-top-color:#bdbdbd}

.fr-toolbar.fr-rtl .fr-command.fr-btn,.fr-popup.fr-rtl .fr-command.fr-btn,.fr-toolbar.fr-rtl .fr-btn-wrap,.fr-popup.fr-rtl .fr-btn-wrap{float:right}

.fr-toolbar.fr-inline>.fr-command.fr-btn:not(.fr-hidden),.fr-toolbar.fr-inline>.fr-btn-wrap:not(.fr-hidden){display:-webkit-inline-box;display:inline-flex;float:none}

.fr-desktop .fr-command:hover,.fr-desktop .fr-command:focus,.fr-desktop .fr-command.fr-btn-hover,.fr-desktop .fr-command.fr-expanded{outline:0;color:#222;background:#ebebeb}

.fr-desktop .fr-command:hover::after,.fr-desktop .fr-command:focus::after,.fr-desktop .fr-command.fr-btn-hover::after,.fr-desktop .fr-command.fr-expanded::after{border-top-color:#222!important}

.fr-desktop .fr-command.fr-selected{color:#222;background:#d6d6d6}

.fr-desktop .fr-command.fr-active:hover,.fr-desktop .fr-command.fr-active:focus,.fr-desktop .fr-command.fr-active.fr-btn-hover,.fr-desktop .fr-command.fr-active.fr-expanded{color:#1e88e5;background:#ebebeb}

.fr-desktop .fr-command.fr-active.fr-selected{color:#1e88e5;background:#d6d6d6}

.fr-desktop .fr-command.fr-disabled:hover,.fr-desktop .fr-command.fr-disabled:focus,.fr-desktop .fr-command.fr-disabled.fr-selected{background:0 0}

.fr-desktop.fr-disabled .fr-command:hover,.fr-desktop.fr-disabled .fr-command:focus,.fr-desktop.fr-disabled .fr-command.fr-selected{background:0 0}

.fr-toolbar.fr-mobile .fr-command.fr-blink,.fr-popup.fr-mobile .fr-command.fr-blink{background:0 0}

.fr-command.fr-btn.fr-options{width:16px;margin-left:-5px}

.fr-command.fr-btn.fr-options.fr-btn-hover,.fr-command.fr-btn.fr-options:hover,.fr-command.fr-btn.fr-options:focus{border-left:solid 1px #fafafa}

.fr-command.fr-btn+.fr-dropdown-menu{display:inline-block;position:absolute;right:auto;bottom:auto;height:auto;z-index:4;-webkit-overflow-scrolling:touch;overflow:hidden;zoom:1;border-radius:0 0 2px 2px;-moz-border-radius:0 0 2px 2px;-webkit-border-radius:0 0 2px 2px;-moz-background-clip:padding;-webkit-background-clip:padding-box;background-clip:padding-box}

.fr-command.fr-btn+.fr-dropdown-menu.test-height .fr-dropdown-wrapper{-webkit-transition:none;-moz-transition:none;-ms-transition:none;-o-transition:none;height:auto;max-height:275px}

.fr-command.fr-btn+.fr-dropdown-menu .fr-dropdown-wrapper{background:#fff;padding:0;margin:auto;display:inline-block;text-align:left;position:relative;box-sizing:border-box;-webkit-transition:max-height .2s ease 0s;-moz-transition:max-height .2s ease 0s;-ms-transition:max-height .2s ease 0s;-o-transition:max-height .2s ease 0s;margin-top:0;float:left;max-height:0;height:0;margin-top:0!important}

.fr-command.fr-btn+.fr-dropdown-menu .fr-dropdown-wrapper .fr-dropdown-content{overflow:auto;position:relative;max-height:275px}

.fr-command.fr-btn+.fr-dropdown-menu .fr-dropdown-wrapper .fr-dropdown-content ul.fr-dropdown-list{list-style-type:none;margin:0;padding:0}

.fr-command.fr-btn+.fr-dropdown-menu .fr-dropdown-wrapper .fr-dropdown-content ul.fr-dropdown-list li{padding:0;margin:0;font-size:15px}

.fr-command.fr-btn+.fr-dropdown-menu .fr-dropdown-wrapper .fr-dropdown-content ul.fr-dropdown-list li a{padding:0 24px;line-height:200%;display:block;cursor:pointer;white-space:nowrap;color:inherit;text-decoration:none}

.fr-command.fr-btn+.fr-dropdown-menu .fr-dropdown-wrapper .fr-dropdown-content ul.fr-dropdown-list li a.fr-active{background:#d6d6d6}

.fr-command.fr-btn+.fr-dropdown-menu .fr-dropdown-wrapper .fr-dropdown-content ul.fr-dropdown-list li a.fr-disabled{color:#bdbdbd;cursor:default}

.fr-command.fr-btn+.fr-dropdown-menu .fr-dropdown-wrapper .fr-dropdown-content ul.fr-dropdown-list li a .fr-shortcut{float:right;margin-left:32px;font-weight:700;-webkit-opacity:.75;-moz-opacity:.75;opacity:.75;-ms-filter:&quot;alpha(Opacity=0)&quot;}

.fr-command.fr-btn:not(.fr-active)+.fr-dropdown-menu{left:-3000px!important}

.fr-command.fr-btn.fr-active+.fr-dropdown-menu{display:inline-block;box-shadow:0 3px 6px rgba(0,0,0,.16),0 2px 2px 1px rgba(0,0,0,.14)}

.fr-command.fr-btn.fr-active+.fr-dropdown-menu .fr-dropdown-wrapper{height:auto;max-height:275px}

.fr-bottom>.fr-command.fr-btn+.fr-dropdown-menu{border-radius:2px 2px 0 0;-moz-border-radius:2px 2px 0 0;-webkit-border-radius:2px 2px 0 0;-moz-background-clip:padding;-webkit-background-clip:padding-box;background-clip:padding-box}

.fr-toolbar.fr-rtl .fr-dropdown-wrapper,.fr-popup.fr-rtl .fr-dropdown-wrapper{text-align:right!important}

body.prevent-scroll{overflow:hidden}

body.prevent-scroll.fr-mobile{position:fixed;-webkit-overflow-scrolling:touch}

.fr-modal{color:#222;font-family:Arial,Helvetica,sans-serif;position:fixed;overflow-x:auto;overflow-y:scroll;top:0;left:0;bottom:0;right:0;width:100%;z-index:2147483640;text-rendering:optimizelegibility;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;text-align:center;line-height:1.2}

.fr-modal.fr-middle .fr-modal-wrapper{margin-top:0;margin-bottom:0;margin-left:auto;margin-right:auto;top:50%;left:50%;-webkit-transform:translate(-50%,-50%);-moz-transform:translate(-50%,-50%);-ms-transform:translate(-50%,-50%);-o-transform:translate(-50%,-50%);position:absolute}

.fr-modal .fr-modal-wrapper{border-radius:2px;-moz-border-radius:2px;-webkit-border-radius:2px;-moz-background-clip:padding;-webkit-background-clip:padding-box;background-clip:padding-box;margin:20px auto;display:inline-block;background:#fff;min-width:300px;box-shadow:0 5px 8px rgba(0,0,0,.19),0 4px 3px 1px rgba(0,0,0,.14);border:0;border-top:5px solid #222;overflow:hidden;width:90%;position:relative}

@media (min-width:768px) and (max-width:991px){.fr-modal .fr-modal-wrapper{margin:30px auto;width:70%}}

@media (min-width:992px){.fr-modal .fr-modal-wrapper{margin:50px auto;width:960px}}

.fr-modal .fr-modal-wrapper .fr-modal-head{background:#fff;box-shadow:0 3px 6px rgba(0,0,0,.16),0 2px 2px 1px rgba(0,0,0,.14);border-bottom:0;overflow:hidden;position:absolute;width:100%;min-height:42px;z-index:3;-webkit-transition:height .2s ease 0s;-moz-transition:height .2s ease 0s;-ms-transition:height .2s ease 0s;-o-transition:height .2s ease 0s}

.fr-modal .fr-modal-wrapper .fr-modal-head .fr-modal-close{padding:12px;width:20px;font-size:30px;cursor:pointer;line-height:18px;color:#222;box-sizing:content-box;position:absolute;top:0;right:0;-webkit-transition:color .2s ease 0s;-moz-transition:color .2s ease 0s;-ms-transition:color .2s ease 0s;-o-transition:color .2s ease 0s}

.fr-modal .fr-modal-wrapper .fr-modal-head h4{font-size:18px;padding:12px 10px;margin:0;font-weight:400;line-height:18px;display:inline-block;float:left}

.fr-modal .fr-modal-wrapper div.fr-modal-body{height:100%;min-height:150px;overflow-y:auto;padding-bottom:10px}

.fr-modal .fr-modal-wrapper div.fr-modal-body:focus{outline:0}

.fr-modal .fr-modal-wrapper div.fr-modal-body button.fr-command{height:36px;line-height:1;color:#1e88e5;padding:10px;cursor:pointer;text-decoration:none;border:0;background:0 0;font-size:16px;outline:0;-webkit-transition:background .2s ease 0s;-moz-transition:background .2s ease 0s;-ms-transition:background .2s ease 0s;-o-transition:background .2s ease 0s;border-radius:2px;-moz-border-radius:2px;-webkit-border-radius:2px;-moz-background-clip:padding;-webkit-background-clip:padding-box;background-clip:padding-box}

.fr-modal .fr-modal-wrapper div.fr-modal-body button.fr-command+button{margin-left:24px}

.fr-modal .fr-modal-wrapper div.fr-modal-body button.fr-command:hover,.fr-modal .fr-modal-wrapper div.fr-modal-body button.fr-command:focus{background:#ebebeb;color:#1e88e5}

.fr-modal .fr-modal-wrapper div.fr-modal-body button.fr-command:active{background:#d6d6d6;color:#1e88e5}

.fr-modal .fr-modal-wrapper div.fr-modal-body button::-moz-focus-inner{border:0}

.fr-desktop .fr-modal-wrapper .fr-modal-head i:hover{background:#ebebeb}

.fr-overlay{position:fixed;top:0;bottom:0;left:0;right:0;background:#000;-webkit-opacity:.5;-moz-opacity:.5;opacity:.5;-ms-filter:&quot;alpha(Opacity=0)&quot;;z-index:2147483639}

.fr-popup{position:absolute;display:none;color:#222;background:#fff;box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 1px 1px rgba(0,0,0,.16);border-radius:2px;-moz-border-radius:2px;-webkit-border-radius:2px;-moz-background-clip:padding;-webkit-background-clip:padding-box;background-clip:padding-box;font-family:Arial,Helvetica,sans-serif;box-sizing:border-box;user-select:none;-o-user-select:none;-moz-user-select:none;-khtml-user-select:none;-webkit-user-select:none;-ms-user-select:none;margin-top:10px;z-index:2147483635;text-align:left;border:0;border-top:5px solid #222;text-rendering:optimizelegibility;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;line-height:1.2}

.fr-popup .fr-input-focus{background:#f5f5f5}

.fr-popup.fr-above{margin-top:-10px;border-top:0;border-bottom:5px solid #222;box-shadow:0 -1px 3px rgba(0,0,0,.12),0 -1px 1px 1px rgba(0,0,0,.16)}

.fr-popup.fr-active{display:block}

.fr-popup.fr-hidden{-webkit-opacity:0;-moz-opacity:0;opacity:0;-ms-filter:&quot;alpha(Opacity=0)&quot;}

.fr-popup.fr-empty{display:none!important}

.fr-popup .fr-hs{display:block!important}

.fr-popup .fr-hs.fr-hidden{display:none!important}

.fr-popup .fr-input-line{position:relative;padding:8px 0}

.fr-popup .fr-input-line input[type=text],.fr-popup .fr-input-line textarea{width:100%;margin:0 0 1px;border:0;border-bottom:solid 1px #bdbdbd;color:#222;font-size:14px;padding:6px 0 2px;background:rgba(0,0,0,0);position:relative;z-index:2;box-sizing:border-box}

.fr-popup .fr-input-line input[type=text]:focus,.fr-popup .fr-input-line textarea:focus{border-bottom:solid 2px #1e88e5;margin-bottom:0}

.fr-popup .fr-input-line input+label,.fr-popup .fr-input-line textarea+label{position:absolute;top:0;left:0;font-size:12px;color:rgba(0,0,0,0);-webkit-transition:color .2s ease 0s;-moz-transition:color .2s ease 0s;-ms-transition:color .2s ease 0s;-o-transition:color .2s ease 0s;z-index:3;width:100%;display:block;background:#fff}

.fr-popup .fr-input-line input.fr-not-empty:focus+label,.fr-popup .fr-input-line textarea.fr-not-empty:focus+label{color:#1e88e5}

.fr-popup .fr-input-line input.fr-not-empty+label,.fr-popup .fr-input-line textarea.fr-not-empty+label{color:gray}

.fr-popup input,.fr-popup textarea{user-select:text;-o-user-select:text;-moz-user-select:text;-khtml-user-select:text;-webkit-user-select:text;-ms-user-select:text;border-radius:0;-moz-border-radius:0;-webkit-border-radius:0;-moz-background-clip:padding;-webkit-background-clip:padding-box;background-clip:padding-box;outline:0}

.fr-popup textarea{resize:none}

.fr-popup .fr-buttons{box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 1px 1px rgba(0,0,0,.16);padding:0 2px;white-space:nowrap;line-height:0;border-bottom:0}

.fr-popup .fr-buttons::after{clear:both;display:block;content:&quot;&quot;;height:0}

.fr-popup .fr-buttons .fr-btn{display:inline-block;float:none}

.fr-popup .fr-buttons .fr-btn i{float:left}

.fr-popup .fr-buttons .fr-separator{display:inline-block;float:none}

.fr-popup .fr-layer{width:225px;box-sizing:border-box;margin:10px;display:none}

@media (min-width:768px){.fr-popup .fr-layer{width:300px}}

.fr-popup .fr-layer.fr-active{display:inline-block}

.fr-popup .fr-action-buttons{z-index:7;height:36px;text-align:right}

.fr-popup .fr-action-buttons button.fr-command{height:36px;line-height:1;color:#1e88e5;padding:10px;cursor:pointer;text-decoration:none;border:0;background:0 0;font-size:16px;outline:0;-webkit-transition:background .2s ease 0s;-moz-transition:background .2s ease 0s;-ms-transition:background .2s ease 0s;-o-transition:background .2s ease 0s;border-radius:2px;-moz-border-radius:2px;-webkit-border-radius:2px;-moz-background-clip:padding;-webkit-background-clip:padding-box;background-clip:padding-box}

.fr-popup .fr-action-buttons button.fr-command+button{margin-left:24px}

.fr-popup .fr-action-buttons button.fr-command:hover,.fr-popup .fr-action-buttons button.fr-command:focus{background:#ebebeb;color:#1e88e5}

.fr-popup .fr-action-buttons button.fr-command:active{background:#d6d6d6;color:#1e88e5}

.fr-popup .fr-action-buttons button::-moz-focus-inner{border:0}

.fr-popup .fr-checkbox{position:relative;display:inline-block;width:16px;height:16px;line-height:1;box-sizing:content-box;vertical-align:middle}

.fr-popup .fr-checkbox svg{margin-left:2px;margin-top:2px;display:none;width:10px;height:10px}

.fr-popup .fr-checkbox span{border:solid 1px #222;border-radius:2px;-moz-border-radius:2px;-webkit-border-radius:2px;-moz-background-clip:padding;-webkit-background-clip:padding-box;background-clip:padding-box;width:16px;height:16px;display:inline-block;position:relative;z-index:1;box-sizing:border-box;-webkit-transition:background .2s ease 0s,border-color .2s ease 0s;-moz-transition:background .2s ease 0s,border-color .2s ease 0s;-ms-transition:background .2s ease 0s,border-color .2s ease 0s;-o-transition:background .2s ease 0s,border-color .2s ease 0s}

.fr-popup .fr-checkbox input{position:absolute;z-index:2;-webkit-opacity:0;-moz-opacity:0;opacity:0;-ms-filter:&quot;alpha(Opacity=0)&quot;;border:0 none;cursor:pointer;height:16px;margin:0;padding:0;width:16px;top:1px;left:1px}

.fr-popup .fr-checkbox input:checked+span{background:#1e88e5;border-color:#1e88e5}

.fr-popup .fr-checkbox input:checked+span svg{display:block}

.fr-popup .fr-checkbox input:focus+span{border-color:#1e88e5}

.fr-popup .fr-checkbox-line{font-size:14px;line-height:1.4px;margin-top:10px}

.fr-popup .fr-checkbox-line label{cursor:pointer;margin:0 5px;vertical-align:middle}

.fr-popup.fr-rtl{direction:rtl;text-align:right}

.fr-popup.fr-rtl .fr-action-buttons{text-align:left}

.fr-popup.fr-rtl .fr-input-line input+label,.fr-popup.fr-rtl .fr-input-line textarea+label{left:auto;right:0}

.fr-popup.fr-rtl .fr-buttons .fr-separator.fr-vs{float:right}

.fr-popup .fr-arrow{width:0;height:0;border-left:5px solid transparent;border-right:5px solid transparent;border-bottom:5px solid #222;position:absolute;top:-9px;left:50%;margin-left:-5px;display:inline-block}

.fr-popup.fr-above .fr-arrow{top:auto;bottom:-9px;border-bottom:0;border-top:5px solid #222}

.fr-text-edit-layer{width:250px;box-sizing:border-box;display:block!important}

.fr-toolbar{color:#222;background:#fff;position:relative;z-index:4;font-family:Arial,Helvetica,sans-serif;box-sizing:border-box;user-select:none;-o-user-select:none;-moz-user-select:none;-khtml-user-select:none;-webkit-user-select:none;-ms-user-select:none;padding:0 2px;border-radius:2px;-moz-border-radius:2px;-webkit-border-radius:2px;-moz-background-clip:padding;-webkit-background-clip:padding-box;background-clip:padding-box;box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 1px 1px rgba(0,0,0,.16);text-align:left;border:0;border-top:5px solid #222;text-rendering:optimizelegibility;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;line-height:1.2}

.fr-toolbar::after{clear:both;display:block;content:&quot;&quot;;height:0}

.fr-toolbar.fr-rtl{text-align:right}

.fr-toolbar.fr-inline{display:none;white-space:nowrap;position:absolute;margin-top:10px}

.fr-toolbar.fr-inline .fr-arrow{width:0;height:0;border-left:5px solid transparent;border-right:5px solid transparent;border-bottom:5px solid #222;position:absolute;top:-9px;left:50%;margin-left:-5px;display:inline-block}

.fr-toolbar.fr-inline.fr-above{margin-top:-10px;box-shadow:0 -1px 3px rgba(0,0,0,.12),0 -1px 1px 1px rgba(0,0,0,.16);border-bottom:5px solid #222;border-top:0}

.fr-toolbar.fr-inline.fr-above .fr-arrow{top:auto;bottom:-9px;border-bottom:0;border-top-color:inherit;border-top-style:solid;border-top-width:5px}

.fr-toolbar.fr-top{top:0;border-radius:2px 2px 0 0;-moz-border-radius:2px 2px 0 0;-webkit-border-radius:2px 2px 0 0;-moz-background-clip:padding;-webkit-background-clip:padding-box;background-clip:padding-box;box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 1px 1px rgba(0,0,0,.16)}

.fr-toolbar.fr-bottom{bottom:0;border-radius:0 0 2px 2px;-moz-border-radius:0 0 2px 2px;-webkit-border-radius:0 0 2px 2px;-moz-background-clip:padding;-webkit-background-clip:padding-box;background-clip:padding-box;box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 1px 1px rgba(0,0,0,.16)}

.fr-separator{background:#ebebeb;display:block;vertical-align:top;float:left}

.fr-separator+.fr-separator{display:none}

.fr-separator.fr-vs{height:34px;width:1px;margin:2px}

.fr-separator.fr-hs{clear:both;height:1px;width:calc(100% - (2 * 2px));margin:0 2px}

.fr-separator.fr-hidden{display:none!important}

.fr-rtl .fr-separator{float:right}

.fr-toolbar.fr-inline .fr-separator.fr-hs{float:none}

.fr-toolbar.fr-inline .fr-separator.fr-vs{float:none;display:inline-block}

.fr-visibility-helper{display:none;margin-left:0!important}

@media (min-width:768px){.fr-visibility-helper{margin-left:1px!important}}

@media (min-width:992px){.fr-visibility-helper{margin-left:2px!important}}

@media (min-width:1200px){.fr-visibility-helper{margin-left:3px!important}}

.fr-opacity-0{-webkit-opacity:0;-moz-opacity:0;opacity:0;-ms-filter:&quot;alpha(Opacity=0)&quot;}

.fr-box{position:relative}

.fr-sticky{position:-webkit-sticky;position:-moz-sticky;position:-ms-sticky;position:-o-sticky;position:sticky}

.fr-sticky-off{position:relative}

.fr-sticky-on{position:fixed}

.fr-sticky-on.fr-sticky-ios{position:absolute;left:0;right:0;width:auto!important}

.fr-sticky-dummy{display:none}

.fr-sticky-on+.fr-sticky-dummy,.fr-sticky-box>.fr-sticky-dummy{display:block}

span.fr-sr-only{position:absolute;width:1px;height:1px;padding:0;margin:-1px;overflow:hidden;clip:rect(0,0,0,0);border:0}

.clearfix::after{clear:both;display:block;content:&quot;&quot;;height:0}

.hide-by-clipping{position:absolute;width:1px;height:1px;padding:0;margin:-1px;overflow:hidden;clip:rect(0,0,0,0);border:0}

.fr-box .fr-counter{position:absolute;bottom:0;padding:5px;right:0;color:#ccc;content:attr(data-chars);font-size:15px;font-family:&quot;Times New Roman&quot;,Georgia,Serif;z-index:1;background:#fff;border-top:solid 1px #ebebeb;border-left:solid 1px #ebebeb;border-radius:2px 0 0;-moz-border-radius:2px 0 0;-webkit-border-radius:2px 0 0;-moz-background-clip:padding;-webkit-background-clip:padding-box;background-clip:padding-box}

.fr-box.fr-rtl .fr-counter{left:0;right:auto;border-left:0;border-right:solid 1px #ebebeb;border-radius:0 2px 0 0;-moz-border-radius:0 2px 0 0;-webkit-border-radius:0 2px 0 0;-moz-background-clip:padding;-webkit-background-clip:padding-box;background-clip:padding-box}

.fr-box.fr-code-view .fr-counter{display:none}

.clearfix::after{clear:both;display:block;content:&quot;&quot;;height:0}

.hide-by-clipping{position:absolute;width:1px;height:1px;padding:0;margin:-1px;overflow:hidden;clip:rect(0,0,0,0);border:0}

textarea.fr-code{display:none;width:100%;resize:none;-moz-resize:none;-webkit-resize:none;box-sizing:border-box;border:0;padding:10px;margin:0;font-family:&quot;Courier New&quot;,monospace;font-size:14px;background:#fff;color:#000;outline:0}

.fr-box.fr-rtl textarea.fr-code{direction:rtl}

.fr-box .CodeMirror{display:none}

.fr-box.fr-code-view textarea.fr-code{display:block}

.fr-box.fr-code-view.fr-inline{box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 1px 1px rgba(0,0,0,.16)}

.fr-box.fr-code-view .fr-element,.fr-box.fr-code-view .fr-placeholder,.fr-box.fr-code-view .fr-iframe{display:none}

.fr-box.fr-code-view .CodeMirror{display:block}

.fr-box.fr-inline.fr-code-view .fr-command.fr-btn.html-switch{display:block}

.fr-box.fr-inline .fr-command.fr-btn.html-switch{position:absolute;top:0;right:0;box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 1px 1px rgba(0,0,0,.16);display:none;background:#fff;color:#222;-moz-outline:0;outline:0;border:0;line-height:1;cursor:pointer;text-align:left;padding:12px;-webkit-transition:background .2s ease 0s;-moz-transition:background .2s ease 0s;-ms-transition:background .2s ease 0s;-o-transition:background .2s ease 0s;border-radius:0;-moz-border-radius:0;-webkit-border-radius:0;-moz-background-clip:padding;-webkit-background-clip:padding-box;background-clip:padding-box;z-index:2;box-sizing:border-box;text-decoration:none;user-select:none;-o-user-select:none;-moz-user-select:none;-khtml-user-select:none;-webkit-user-select:none;-ms-user-select:none}

.fr-box.fr-inline .fr-command.fr-btn.html-switch i{font-size:14px;width:14px;text-align:center}

.fr-box.fr-inline .fr-command.fr-btn.html-switch.fr-desktop:hover{background:#ebebeb}

.clearfix::after{clear:both;display:block;content:&quot;&quot;;height:0}

.hide-by-clipping{position:absolute;width:1px;height:1px;padding:0;margin:-1px;overflow:hidden;clip:rect(0,0,0,0);border:0}

.fr-popup .fr-colors-tabs{box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 1px 1px rgba(0,0,0,.16);margin-bottom:5px;line-height:16px;margin-left:-2px;margin-right:-2px}

.fr-popup .fr-colors-tabs .fr-colors-tab{display:inline-block;width:50%;cursor:pointer;text-align:center;color:#222;font-size:13px;padding:8px 0;position:relative}

.fr-popup .fr-colors-tabs .fr-colors-tab:hover,.fr-popup .fr-colors-tabs .fr-colors-tab:focus{color:#1e88e5}

.fr-popup .fr-colors-tabs .fr-colors-tab[data-param1=background]::after{position:absolute;bottom:0;left:0;width:100%;height:2px;background:#1e88e5;content:'';-webkit-transition:transform .2s ease 0s;-moz-transition:transform .2s ease 0s;-ms-transition:transform .2s ease 0s;-o-transition:transform .2s ease 0s}

.fr-popup .fr-colors-tabs .fr-colors-tab.fr-selected-tab{color:#1e88e5}

.fr-popup .fr-colors-tabs .fr-colors-tab.fr-selected-tab[data-param1=text]~[data-param1=background]::after{-webkit-transform:translate3d(-100%,0,0);-moz-transform:translate3d(-100%,0,0);-ms-transform:translate3d(-100%,0,0);-o-transform:translate3d(-100%,0,0)}

.fr-popup .fr-color-hex-layer{width:100%;margin:0;padding:10px}

.fr-popup .fr-color-hex-layer .fr-input-line{float:left;width:calc(100% - 50px);padding:8px 0 0}

.fr-popup .fr-color-hex-layer .fr-action-buttons{float:right;width:50px}

.fr-popup .fr-color-hex-layer .fr-action-buttons button.fr-command{background-color:#1e88e5;color:#FFF!important;border-radius:2px;-moz-border-radius:2px;-webkit-border-radius:2px;-moz-background-clip:padding;-webkit-background-clip:padding-box;background-clip:padding-box;font-size:13px;height:32px}

.fr-popup .fr-color-hex-layer .fr-action-buttons button.fr-command:hover{background-color:#166dba;color:#FFF}

.fr-popup .fr-separator+.fr-colors-tabs{box-shadow:none;margin-left:2px;margin-right:2px}

.fr-popup .fr-color-set{line-height:0;display:none}

.fr-popup .fr-color-set.fr-selected-set{display:block}

.fr-popup .fr-color-set>span{display:inline-block;width:32px;height:32px;position:relative;z-index:1}

.fr-popup .fr-color-set>span>i,.fr-popup .fr-color-set>span>svg{text-align:center;line-height:32px;height:32px;width:32px;font-size:13px;position:absolute;bottom:0;cursor:default;left:0}

.fr-popup .fr-color-set>span .fr-selected-color{color:#fff;font-family:FontAwesome;font-size:13px;font-weight:400;line-height:32px;position:absolute;top:0;bottom:0;right:0;left:0;text-align:center;cursor:default}

.fr-popup .fr-color-set>span:hover,.fr-popup .fr-color-set>span:focus{outline:1px solid #222;z-index:2}

.fr-rtl .fr-popup .fr-colors-tabs .fr-colors-tab.fr-selected-tab[data-param1=text]~[data-param1=background]::after{-webkit-transform:translate3d(100%,0,0);-moz-transform:translate3d(100%,0,0);-ms-transform:translate3d(100%,0,0);-o-transform:translate3d(100%,0,0)}

.clearfix::after{clear:both;display:block;content:&quot;&quot;;height:0}

.hide-by-clipping{position:absolute;width:1px;height:1px;padding:0;margin:-1px;overflow:hidden;clip:rect(0,0,0,0);border:0}

.fr-drag-helper{background:#1e88e5;height:2px;margin-top:-1px;-webkit-opacity:.2;-moz-opacity:.2;opacity:.2;-ms-filter:&quot;alpha(Opacity=0)&quot;;position:absolute;z-index:2147483640;display:none}

.fr-drag-helper.fr-visible{display:block}

.fr-dragging{-webkit-opacity:.4;-moz-opacity:.4;opacity:.4;-ms-filter:&quot;alpha(Opacity=0)&quot;}

.clearfix::after{clear:both;display:block;content:&quot;&quot;;height:0}

.hide-by-clipping{position:absolute;width:1px;height:1px;padding:0;margin:-1px;overflow:hidden;clip:rect(0,0,0,0);border:0}

.fr-popup .fr-emoticon{display:inline-block;font-size:20px;width:20px;padding:5px;line-height:1;cursor:default;font-weight:400;font-family:&quot;Apple Color Emoji&quot;,&quot;Segoe UI Emoji&quot;,NotoColorEmoji,&quot;Segoe UI Symbol&quot;,&quot;Android Emoji&quot;,EmojiSymbols;box-sizing:content-box}

.fr-popup .fr-emoticon img{height:20px}

.fr-popup .fr-link:focus{outline:0;background:#ebebeb}

.clearfix::after{clear:both;display:block;content:&quot;&quot;;height:0}

.hide-by-clipping{position:absolute;width:1px;height:1px;padding:0;margin:-1px;overflow:hidden;clip:rect(0,0,0,0);border:0}

.fr-popup .fr-file-upload-layer{border:dashed 2px #bdbdbd;padding:25px 0;position:relative;font-size:14px;letter-spacing:1px;line-height:140%;box-sizing:border-box;text-align:center}

.fr-popup .fr-file-upload-layer:hover{background:#ebebeb}

.fr-popup .fr-file-upload-layer.fr-drop{background:#ebebeb;border-color:#1e88e5}

.fr-popup .fr-file-upload-layer .fr-form{-webkit-opacity:0;-moz-opacity:0;opacity:0;-ms-filter:&quot;alpha(Opacity=0)&quot;;position:absolute;top:0;bottom:0;left:0;right:0;z-index:2147483640;overflow:hidden;margin:0!important;padding:0!important;width:100%!important}

.fr-popup .fr-file-upload-layer .fr-form input{cursor:pointer;position:absolute;right:0;top:0;bottom:0;width:500%;height:100%;margin:0;font-size:400px}

.fr-popup .fr-file-progress-bar-layer{box-sizing:border-box}

.fr-popup .fr-file-progress-bar-layer>h3{font-size:16px;margin:10px 0;font-weight:400}

.fr-popup .fr-file-progress-bar-layer>div.fr-action-buttons{display:none}

.fr-popup .fr-file-progress-bar-layer>div.fr-loader{background:#bcdbf7;height:10px;width:100%;margin-top:20px;overflow:hidden;position:relative}

.fr-popup .fr-file-progress-bar-layer>div.fr-loader span{display:block;height:100%;width:0;background:#1e88e5;-webkit-transition:width .2s ease 0s;-moz-transition:width .2s ease 0s;-ms-transition:width .2s ease 0s;-o-transition:width .2s ease 0s}

.fr-popup .fr-file-progress-bar-layer>div.fr-loader.fr-indeterminate span{width:30%!important;position:absolute;top:0;-webkit-animation:loading 2s linear infinite;animation:loading 2s linear infinite}

.fr-popup .fr-file-progress-bar-layer.fr-error>div.fr-loader{display:none}

.fr-popup .fr-file-progress-bar-layer.fr-error>div.fr-action-buttons{display:block}

@keyframes loading{from{left:-25%}to{left:100%}}

@-webkit-keyframes loading{from{left:-25%}to{left:100%}}

body.fr-fullscreen{overflow:hidden;height:100%;width:100%;position:fixed}

.fr-box.fr-fullscreen{margin:0!important;position:fixed;top:0;left:0;bottom:0;right:0;z-index:2147483630!important;width:auto!important}

.fr-box.fr-fullscreen .fr-toolbar.fr-top{top:0!important}

.fr-box.fr-fullscreen .fr-toolbar.fr-bottom{bottom:0!important}

.fr-fullscreen-wrapper{z-index:2147483640!important;width:100%!important;margin:0!important;padding:0!important;overflow:visible!important}

.clearfix::after{clear:both;display:block;content:&quot;&quot;;height:0}

.hide-by-clipping{position:absolute;width:1px;height:1px;padding:0;margin:-1px;overflow:hidden;clip:rect(0,0,0,0);border:0}

.fr-modal .fr-modal-wrapper .fr-modal-body .fr-help-modal{text-align:left;padding:20px 20px 10px}

.fr-modal .fr-modal-wrapper .fr-modal-body .fr-help-modal table{border-collapse:collapse;font-size:14px;line-height:1.5;width:100%}

.fr-modal .fr-modal-wrapper .fr-modal-body .fr-help-modal table+table{margin-top:20px}

.fr-modal .fr-modal-wrapper .fr-modal-body .fr-help-modal table tr{border:0}

.fr-modal .fr-modal-wrapper .fr-modal-body .fr-help-modal table th,.fr-modal .fr-modal-wrapper .fr-modal-body .fr-help-modal table td{padding:6px 0 4px}

.fr-modal .fr-modal-wrapper .fr-modal-body .fr-help-modal table tbody tr{border-bottom:solid 1px #ebebeb}

.fr-modal .fr-modal-wrapper .fr-modal-body .fr-help-modal table tbody td:first-child{width:60%;color:#646464}

.fr-modal .fr-modal-wrapper .fr-modal-body .fr-help-modal table tbody td:nth-child(n+2){letter-spacing:.5px}

.clearfix::after{clear:both;display:block;content:&quot;&quot;;height:0}

.hide-by-clipping{position:absolute;width:1px;height:1px;padding:0;margin:-1px;overflow:hidden;clip:rect(0,0,0,0);border:0}

.fr-element img{cursor:pointer}

.fr-image-resizer{position:absolute;border:solid 1px #1e88e5;display:none;user-select:none;-o-user-select:none;-moz-user-select:none;-khtml-user-select:none;-webkit-user-select:none;-ms-user-select:none;box-sizing:content-box}

.fr-image-resizer.fr-active{display:block}

.fr-image-resizer .fr-handler{display:block;position:absolute;background:#1e88e5;border:solid 1px #fff;z-index:4;box-sizing:border-box}

.fr-image-resizer .fr-handler.fr-hnw{cursor:nw-resize}

.fr-image-resizer .fr-handler.fr-hne{cursor:ne-resize}

.fr-image-resizer .fr-handler.fr-hsw{cursor:sw-resize}

.fr-image-resizer .fr-handler.fr-hse{cursor:se-resize}

.fr-image-resizer .fr-handler{width:12px;height:12px}

.fr-image-resizer .fr-handler.fr-hnw{left:-6px;top:-6px}

.fr-image-resizer .fr-handler.fr-hne{right:-6px;top:-6px}

.fr-image-resizer .fr-handler.fr-hsw{left:-6px;bottom:-6px}

.fr-image-resizer .fr-handler.fr-hse{right:-6px;bottom:-6px}

@media (min-width:1200px){.fr-image-resizer .fr-handler{width:10px;height:10px}.fr-image-resizer .fr-handler.fr-hnw{left:-5px;top:-5px}.fr-image-resizer .fr-handler.fr-hne{right:-5px;top:-5px}.fr-image-resizer .fr-handler.fr-hsw{left:-5px;bottom:-5px}.fr-image-resizer .fr-handler.fr-hse{right:-5px;bottom:-5px}}

.fr-image-overlay{position:fixed;top:0;left:0;bottom:0;right:0;z-index:2147483640;display:none}

.fr-popup .fr-image-upload-layer{border:dashed 2px #bdbdbd;padding:25px 0;position:relative;font-size:14px;letter-spacing:1px;line-height:140%;text-align:center}

.fr-popup .fr-image-upload-layer:hover{background:#ebebeb}

.fr-popup .fr-image-upload-layer.fr-drop{background:#ebebeb;border-color:#1e88e5}

.fr-popup .fr-image-upload-layer .fr-form{-webkit-opacity:0;-moz-opacity:0;opacity:0;-ms-filter:&quot;alpha(Opacity=0)&quot;;position:absolute;top:0;bottom:0;left:0;right:0;z-index:2147483640;overflow:hidden;margin:0!important;padding:0!important;width:100%!important}

.fr-popup .fr-image-upload-layer .fr-form input{cursor:pointer;position:absolute;right:0;top:0;bottom:0;width:500%;height:100%;margin:0;font-size:400px}

.fr-popup .fr-image-progress-bar-layer>h3{font-size:16px;margin:10px 0;font-weight:400}

.fr-popup .fr-image-progress-bar-layer>div.fr-action-buttons{display:none}

.fr-popup .fr-image-progress-bar-layer>div.fr-loader{background:#bcdbf7;height:10px;width:100%;margin-top:20px;overflow:hidden;position:relative}

.fr-popup .fr-image-progress-bar-layer>div.fr-loader span{display:block;height:100%;width:0;background:#1e88e5;-webkit-transition:width .2s ease 0s;-moz-transition:width .2s ease 0s;-ms-transition:width .2s ease 0s;-o-transition:width .2s ease 0s}

.fr-popup .fr-image-progress-bar-layer>div.fr-loader.fr-indeterminate span{width:30%!important;position:absolute;top:0;-webkit-animation:loading 2s linear infinite;animation:loading 2s linear infinite}

.fr-popup .fr-image-progress-bar-layer.fr-error>div.fr-loader{display:none}

.fr-popup .fr-image-progress-bar-layer.fr-error>div.fr-action-buttons{display:block}

.fr-image-size-layer .fr-image-group .fr-input-line{width:calc(50% - 5px);display:inline-block}

.fr-image-size-layer .fr-image-group .fr-input-line+.fr-input-line{margin-left:10px}

.fr-uploading{-webkit-opacity:.4;-moz-opacity:.4;opacity:.4;-ms-filter:&quot;alpha(Opacity=0)&quot;}

@keyframes loading{from{left:-25%}to{left:100%}}

@-webkit-keyframes loading{from{left:-25%}to{left:100%}}

.clearfix::after{clear:both;display:block;content:&quot;&quot;;height:0}

.hide-by-clipping{position:absolute;width:1px;height:1px;padding:0;margin:-1px;overflow:hidden;clip:rect(0,0,0,0);border:0}

.fr-modal-head .fr-modal-head-line::after{clear:both;display:block;content:&quot;&quot;;height:0}

.fr-modal-head .fr-modal-head-line i.fr-modal-more,.fr-modal-head .fr-modal-head-line svg.fr-modal-more{float:left;opacity:1;-webkit-transition:padding .2s ease 0s,width .2s ease 0s,opacity .2s ease 0s;-moz-transition:padding .2s ease 0s,width .2s ease 0s,opacity .2s ease 0s;-ms-transition:padding .2s ease 0s,width .2s ease 0s,opacity .2s ease 0s;-o-transition:padding .2s ease 0s,width .2s ease 0s,opacity .2s ease 0s;padding:12px}

.fr-modal-head .fr-modal-head-line i.fr-modal-more.fr-not-available,.fr-modal-head .fr-modal-head-line svg.fr-modal-more.fr-not-available{opacity:0;width:0;padding:12px 0}

.fr-modal-head .fr-modal-tags{display:none;text-align:left}

.fr-modal-head .fr-modal-tags a{display:inline-block;opacity:0;padding:6px 8px;margin:8px 0 8px 8px;text-decoration:none;border-radius:2px;-moz-border-radius:2px;-webkit-border-radius:2px;-moz-background-clip:padding;-webkit-background-clip:padding-box;background-clip:padding-box;color:#1e88e5;-webkit-transition:opacity .2s ease 0s,background .2s ease 0s;-moz-transition:opacity .2s ease 0s,background .2s ease 0s;-ms-transition:opacity .2s ease 0s,background .2s ease 0s;-o-transition:opacity .2s ease 0s,background .2s ease 0s;cursor:pointer}

.fr-modal-head .fr-modal-tags a:focus{outline:0}

.fr-modal-head .fr-modal-tags a.fr-selected-tag{background:#d6d6d6}

div.fr-modal-body .fr-preloader{display:block;margin:50px auto}

div.fr-modal-body div.fr-image-list{text-align:center;margin:0 10px;padding:0}

div.fr-modal-body div.fr-image-list::after{clear:both;display:block;content:&quot;&quot;;height:0}

div.fr-modal-body div.fr-image-list .fr-list-column{float:left;width:calc((100% - 10px) / 2)}

@media (min-width:768px) and (max-width:1199px){div.fr-modal-body div.fr-image-list .fr-list-column{width:calc((100% - 20px) / 3)}}

@media (min-width:1200px){div.fr-modal-body div.fr-image-list .fr-list-column{width:calc((100% - 30px) / 4)}}

div.fr-modal-body div.fr-image-list .fr-list-column+.fr-list-column{margin-left:10px}

div.fr-modal-body div.fr-image-list div.fr-image-container{position:relative;width:100%;display:block;box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 1px 1px rgba(0,0,0,.16);border-radius:2px;-moz-border-radius:2px;-webkit-border-radius:2px;-moz-background-clip:padding;-webkit-background-clip:padding-box;background-clip:padding-box;overflow:hidden}

div.fr-modal-body div.fr-image-list div.fr-image-container:first-child{margin-top:10px}

div.fr-modal-body div.fr-image-list div.fr-image-container+div{margin-top:10px}

div.fr-modal-body div.fr-image-list div.fr-image-container.fr-image-deleting::after{position:absolute;-webkit-opacity:.5;-moz-opacity:.5;opacity:.5;-ms-filter:&quot;alpha(Opacity=0)&quot;;-webkit-transition:opacity .2s ease 0s;-moz-transition:opacity .2s ease 0s;-ms-transition:opacity .2s ease 0s;-o-transition:opacity .2s ease 0s;background:#000;content:&quot;&quot;;top:0;left:0;bottom:0;right:0;z-index:2}

div.fr-modal-body div.fr-image-list div.fr-image-container.fr-image-deleting::before{content:attr(data-deleting);color:#fff;top:0;left:0;bottom:0;right:0;margin:auto;position:absolute;z-index:3;font-size:15px;height:20px}

div.fr-modal-body div.fr-image-list div.fr-image-container.fr-empty{height:95px;background:#ccc;z-index:1}

div.fr-modal-body div.fr-image-list div.fr-image-container.fr-empty::after{position:absolute;margin:auto;top:0;bottom:0;left:0;right:0;content:attr(data-loading);display:inline-block;height:20px}

div.fr-modal-body div.fr-image-list div.fr-image-container img{width:100%;vertical-align:middle;position:relative;z-index:2;-webkit-opacity:1;-moz-opacity:1;opacity:1;-ms-filter:&quot;alpha(Opacity=0)&quot;;-webkit-transition:opacity .2s ease 0s,filter .2s ease 0s;-moz-transition:opacity .2s ease 0s,filter .2s ease 0s;-ms-transition:opacity .2s ease 0s,filter .2s ease 0s;-o-transition:opacity .2s ease 0s,filter .2s ease 0s;-webkit-transform:translateZ(0);-moz-transform:translateZ(0);-ms-transform:translateZ(0);-o-transform:translateZ(0)}

div.fr-modal-body div.fr-image-list div.fr-image-container.fr-mobile-selected img{-webkit-opacity:.75;-moz-opacity:.75;opacity:.75;-ms-filter:&quot;alpha(Opacity=0)&quot;}

div.fr-modal-body div.fr-image-list div.fr-image-container.fr-mobile-selected .fr-delete-img,div.fr-modal-body div.fr-image-list div.fr-image-container.fr-mobile-selected .fr-insert-img{display:inline-block}

div.fr-modal-body div.fr-image-list div.fr-image-container .fr-delete-img,div.fr-modal-body div.fr-image-list div.fr-image-container .fr-insert-img{display:none;top:50%;border-radius:100%;-moz-border-radius:100%;-webkit-border-radius:100%;-moz-background-clip:padding;-webkit-background-clip:padding-box;background-clip:padding-box;-webkit-transition:background .2s ease 0s,color .2s ease 0s;-moz-transition:background .2s ease 0s,color .2s ease 0s;-ms-transition:background .2s ease 0s,color .2s ease 0s;-o-transition:background .2s ease 0s,color .2s ease 0s;box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 1px 1px rgba(0,0,0,.16);position:absolute;cursor:pointer;margin:0;width:36px;height:36px;line-height:36px;text-decoration:none;z-index:3}

div.fr-modal-body div.fr-image-list div.fr-image-container .fr-delete-img{background:#b8312f;color:#fff;left:50%;-webkit-transform:translateY(-50%) translateX(25%);-moz-transform:translateY(-50%) translateX(25%);-ms-transform:translateY(-50%) translateX(25%);-o-transform:translateY(-50%) translateX(25%)}

div.fr-modal-body div.fr-image-list div.fr-image-container .fr-insert-img{background:#fff;color:#1e88e5;left:50%;-webkit-transform:translateY(-50%) translateX(-125%);-moz-transform:translateY(-50%) translateX(-125%);-ms-transform:translateY(-50%) translateX(-125%);-o-transform:translateY(-50%) translateX(-125%)}

.fr-desktop .fr-modal-wrapper .fr-modal-head .fr-modal-tags a:hover{background:#ebebeb}

.fr-desktop .fr-modal-wrapper .fr-modal-head .fr-modal-tags a.fr-selected-tag{background:#d6d6d6}

.fr-desktop .fr-modal-wrapper div.fr-modal-body div.fr-image-list div.fr-image-container:hover img{-webkit-opacity:.75;-moz-opacity:.75;opacity:.75;-ms-filter:&quot;alpha(Opacity=0)&quot;}

.fr-desktop .fr-modal-wrapper div.fr-modal-body div.fr-image-list div.fr-image-container:hover .fr-delete-img,.fr-desktop .fr-modal-wrapper div.fr-modal-body div.fr-image-list div.fr-image-container:hover .fr-insert-img{display:inline-block}

.fr-desktop .fr-modal-wrapper div.fr-modal-body div.fr-image-list div.fr-image-container .fr-delete-img:hover{background:#bf4644;color:#fff}

.fr-desktop .fr-modal-wrapper div.fr-modal-body div.fr-image-list div.fr-image-container .fr-insert-img:hover{background:#ebebeb}

.clearfix::after{clear:both;display:block;content:&quot;&quot;;height:0}

.hide-by-clipping{position:absolute;width:1px;height:1px;padding:0;margin:-1px;overflow:hidden;clip:rect(0,0,0,0);border:0}

.fr-line-breaker{cursor:text;border-top:1px solid #1e88e5;position:fixed;z-index:2;display:none}

.fr-line-breaker.fr-visible{display:block}

.fr-line-breaker a.fr-floating-btn{position:absolute;left:calc(50% - (32px / 2));top:-16px}

.clearfix::after{clear:both;display:block;content:&quot;&quot;;height:0}

.hide-by-clipping{position:absolute;width:1px;height:1px;padding:0;margin:-1px;overflow:hidden;clip:rect(0,0,0,0);border:0}

.fr-quick-insert{position:absolute;z-index:2147483639;white-space:nowrap;padding-right:5px;margin-left:-5px;box-sizing:content-box}

.fr-quick-insert.fr-on a.fr-floating-btn svg{-webkit-transform:rotate(135deg);-moz-transform:rotate(135deg);-ms-transform:rotate(135deg);-o-transform:rotate(135deg)}

.fr-quick-insert.fr-hidden{display:none}

.fr-qi-helper{position:absolute;z-index:3;padding-left:16px;white-space:nowrap}

.fr-qi-helper a.fr-btn.fr-floating-btn{text-align:center;display:inline-block;color:#222;-webkit-opacity:0;-moz-opacity:0;opacity:0;-ms-filter:&quot;alpha(Opacity=0)&quot;;-webkit-transform:scale(0);-moz-transform:scale(0);-ms-transform:scale(0);-o-transform:scale(0)}

.fr-qi-helper a.fr-btn.fr-floating-btn.fr-size-1{-webkit-opacity:1;-moz-opacity:1;opacity:1;-ms-filter:&quot;alpha(Opacity=0)&quot;;-webkit-transform:scale(1);-moz-transform:scale(1);-ms-transform:scale(1);-o-transform:scale(1)}

.clearfix::after{clear:both;display:block;content:&quot;&quot;;height:0}

.hide-by-clipping{position:absolute;width:1px;height:1px;padding:0;margin:-1px;overflow:hidden;clip:rect(0,0,0,0);border:0}

.fr-modal .fr-modal-wrapper .fr-modal-body .fr-special-characters-modal{text-align:left;padding:20px 20px 10px}

.fr-modal .fr-modal-wrapper .fr-modal-body .fr-special-characters-modal .fr-special-characters-list{margin-bottom:20px}

.fr-modal .fr-modal-wrapper .fr-modal-body .fr-special-characters-modal .fr-special-characters-title{font-weight:700;font-size:14px;padding:6px 0 4px;margin:0 0 5px}

.fr-modal .fr-modal-wrapper .fr-modal-body .fr-special-characters-modal .fr-special-character{display:inline-block;font-size:16px;width:20px;height:20px;padding:5px;line-height:20px;cursor:default;font-weight:400;box-sizing:content-box;text-align:center;border:1px solid #ccc;margin:-1px 0 0 -1px}

.clearfix::after{clear:both;display:block;content:&quot;&quot;;height:0}

.hide-by-clipping{position:absolute;width:1px;height:1px;padding:0;margin:-1px;overflow:hidden;clip:rect(0,0,0,0);border:0}

.fr-element table td.fr-selected-cell,.fr-element table th.fr-selected-cell{border:1px double #1e88e5}

.fr-element table tr{user-select:none;-o-user-select:none;-moz-user-select:none;-khtml-user-select:none;-webkit-user-select:none;-ms-user-select:none}

.fr-element table td,.fr-element table th{user-select:text;-o-user-select:text;-moz-user-select:text;-khtml-user-select:text;-webkit-user-select:text;-ms-user-select:text}

.fr-element .fr-no-selection table td,.fr-element .fr-no-selection table th{user-select:none;-o-user-select:none;-moz-user-select:none;-khtml-user-select:none;-webkit-user-select:none;-ms-user-select:none}

.fr-table-resizer{cursor:col-resize;position:absolute;z-index:3;display:none}

.fr-table-resizer.fr-moving{z-index:2}

.fr-table-resizer div{-webkit-opacity:0;-moz-opacity:0;opacity:0;-ms-filter:&quot;alpha(Opacity=0)&quot;;border-right:1px solid #1e88e5}

.fr-no-selection{user-select:none;-o-user-select:none;-moz-user-select:none;-khtml-user-select:none;-webkit-user-select:none;-ms-user-select:none}

.fr-popup .fr-table-colors-hex-layer{width:100%;margin:0;padding:10px}

.fr-popup .fr-table-colors-hex-layer .fr-input-line{float:left;width:calc(100% - 50px);padding:8px 0 0}

.fr-popup .fr-table-colors-hex-layer .fr-action-buttons{float:right;width:50px}

.fr-popup .fr-table-colors-hex-layer .fr-action-buttons button{background-color:#1e88e5;color:#FFF;border-radius:2px;-moz-border-radius:2px;-webkit-border-radius:2px;-moz-background-clip:padding;-webkit-background-clip:padding-box;background-clip:padding-box;font-size:13px;height:32px}

.fr-popup .fr-table-colors-hex-layer .fr-action-buttons button:hover{background-color:#166dba;color:#FFF}

.fr-popup .fr-table-size .fr-table-size-info{text-align:center;font-size:14px;padding:8px}

.fr-popup .fr-table-size .fr-select-table-size{line-height:0;padding:0 5px 5px;white-space:nowrap}

.fr-popup .fr-table-size .fr-select-table-size>span{display:inline-block;padding:0 4px 4px 0;background:0 0}

.fr-popup .fr-table-size .fr-select-table-size>span>span{display:inline-block;width:18px;height:18px;border:1px solid #ddd}

.fr-popup .fr-table-size .fr-select-table-size>span.hover{background:0 0}

.fr-popup .fr-table-size .fr-select-table-size>span.hover>span{background:rgba(30,136,229,.3);border:solid 1px #1e88e5}

.fr-popup .fr-table-size .fr-select-table-size .new-line::after{clear:both;display:block;content:&quot;&quot;;height:0}

.fr-popup.fr-above .fr-table-size .fr-select-table-size>span{display:inline-block!important}

.fr-popup .fr-table-colors-buttons{margin-bottom:5px}

.fr-popup .fr-table-colors{line-height:0;display:block}

.fr-popup .fr-table-colors>span{display:inline-block;width:32px;height:32px;position:relative;z-index:1}

.fr-popup .fr-table-colors>span>i{text-align:center;line-height:32px;height:32px;width:32px;font-size:13px;position:absolute;bottom:0;cursor:default;left:0}

.fr-popup .fr-table-colors>span:focus{outline:1px solid #222;z-index:2}

.fr-popup.fr-desktop .fr-table-size .fr-select-table-size>span>span{width:12px;height:12px}

.fr-insert-helper{position:absolute;z-index:9999;white-space:nowrap}

.clearfix::after{clear:both;display:block;content:&quot;&quot;;height:0}

.hide-by-clipping{position:absolute;width:1px;height:1px;padding:0;margin:-1px;overflow:hidden;clip:rect(0,0,0,0);border:0}

.fr-element .fr-video{user-select:none;-o-user-select:none;-moz-user-select:none;-khtml-user-select:none;-webkit-user-select:none;-ms-user-select:none}

.fr-element .fr-video::after{position:absolute;content:'';z-index:1;top:0;left:0;right:0;bottom:0;cursor:pointer;display:block;background:rgba(0,0,0,0)}

.fr-element .fr-video.fr-active>*{z-index:2;position:relative}

.fr-element .fr-video>*{box-sizing:content-box;max-width:100%;border:0}

.fr-box .fr-video-resizer{position:absolute;border:solid 1px #1e88e5;display:none;user-select:none;-o-user-select:none;-moz-user-select:none;-khtml-user-select:none;-webkit-user-select:none;-ms-user-select:none}

.fr-box .fr-video-resizer.fr-active{display:block}

.fr-box .fr-video-resizer .fr-handler{display:block;position:absolute;background:#1e88e5;border:solid 1px #fff;z-index:4;box-sizing:border-box}

.fr-box .fr-video-resizer .fr-handler.fr-hnw{cursor:nw-resize}

.fr-box .fr-video-resizer .fr-handler.fr-hne{cursor:ne-resize}

.fr-box .fr-video-resizer .fr-handler.fr-hsw{cursor:sw-resize}

.fr-box .fr-video-resizer .fr-handler.fr-hse{cursor:se-resize}

.fr-box .fr-video-resizer .fr-handler{width:12px;height:12px}

.fr-box .fr-video-resizer .fr-handler.fr-hnw{left:-6px;top:-6px}

.fr-box .fr-video-resizer .fr-handler.fr-hne{right:-6px;top:-6px}

.fr-box .fr-video-resizer .fr-handler.fr-hsw{left:-6px;bottom:-6px}

.fr-box .fr-video-resizer .fr-handler.fr-hse{right:-6px;bottom:-6px}

@media (min-width:1200px){.fr-box .fr-video-resizer .fr-handler{width:10px;height:10px}.fr-box .fr-video-resizer .fr-handler.fr-hnw{left:-5px;top:-5px}.fr-box .fr-video-resizer .fr-handler.fr-hne{right:-5px;top:-5px}.fr-box .fr-video-resizer .fr-handler.fr-hsw{left:-5px;bottom:-5px}.fr-box .fr-video-resizer .fr-handler.fr-hse{right:-5px;bottom:-5px}}

.fr-popup .fr-video-size-layer .fr-video-group .fr-input-line{width:calc(50% - 5px);display:inline-block}

.fr-popup .fr-video-size-layer .fr-video-group .fr-input-line+.fr-input-line{margin-left:10px}

.fr-popup .fr-video-upload-layer{border:dashed 2px #bdbdbd;padding:25px 0;position:relative;font-size:14px;letter-spacing:1px;line-height:140%;text-align:center}

.fr-popup .fr-video-upload-layer:hover{background:#ebebeb}

.fr-popup .fr-video-upload-layer.fr-drop{background:#ebebeb;border-color:#1e88e5}

.fr-popup .fr-video-upload-layer .fr-form{-webkit-opacity:0;-moz-opacity:0;opacity:0;-ms-filter:&quot;alpha(Opacity=0)&quot;;position:absolute;top:0;bottom:0;left:0;right:0;z-index:2147483640;overflow:hidden;margin:0!important;padding:0!important;width:100%!important}

.fr-popup .fr-video-upload-layer .fr-form input{cursor:pointer;position:absolute;right:0;top:0;bottom:0;width:500%;height:100%;margin:0;font-size:400px}

.fr-popup .fr-video-progress-bar-layer>h3{font-size:16px;margin:10px 0;font-weight:400}

.fr-popup .fr-video-progress-bar-layer>div.fr-action-buttons{display:none}

.fr-popup .fr-video-progress-bar-layer>div.fr-loader{background:#bcdbf7;height:10px;width:100%;margin-top:20px;overflow:hidden;position:relative}

.fr-popup .fr-video-progress-bar-layer>div.fr-loader span{display:block;height:100%;width:0;background:#1e88e5;-webkit-transition:width .2s ease 0s;-moz-transition:width .2s ease 0s;-ms-transition:width .2s ease 0s;-o-transition:width .2s ease 0s}

.fr-popup .fr-video-progress-bar-layer>div.fr-loader.fr-indeterminate span{width:30%!important;position:absolute;top:0;-webkit-animation:loading 2s linear infinite;animation:loading 2s linear infinite}

.fr-popup .fr-video-progress-bar-layer.fr-error>div.fr-loader{display:none}

.fr-popup .fr-video-progress-bar-layer.fr-error>div.fr-action-buttons{display:block}

.fr-video-overlay{position:fixed;top:0;left:0;bottom:0;right:0;z-index:2147483640;display:none}
/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9mcm9hbGEtZWRpdG9yL2Nzcy9mcm9hbGFfZWRpdG9yLnBrZ2QubWluLmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7OztFQUlFOztBQUVGLGlCQUFpQixVQUFVLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxRQUFROztBQUFDLGtCQUFrQixpQkFBaUIsQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLGtCQUFrQixDQUFDLFFBQVE7O0FBQUMsOEJBQThCLDJCQUEyQjs7QUFBQyw2QkFBNkIsVUFBVSxDQUFDLFlBQVksQ0FBMEQscUJBQXFCLENBQUMsZUFBZSxDQUFDLGVBQWU7O0FBQUMsb0NBQW9DLGdCQUFnQjs7QUFBQyxZQUFZLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsd0JBQXdCOztBQUFDLGNBQWMsZ0JBQWdCLENBQUMsbUJBQW1CLENBQUMscUJBQXFCLENBQUMsdUJBQXVCLENBQUMsd0JBQXdCLENBQUMsb0JBQW9COztBQUFDLHdCQUF3QixnQkFBZ0IsQ0FBQyxtQkFBbUIsQ0FBQyxxQkFBcUIsQ0FBQyx1QkFBdUIsQ0FBQyx3QkFBd0IsQ0FBQyxvQkFBb0I7O0FBQUMsbUNBQW1DLDJCQUEyQjs7QUFBQywwQkFBNkssa0VBQWtFLENBQUMsa0JBQWtCLENBQUMsdUJBQXVCLENBQUMsMEJBQTBCLENBQUMsNEJBQTRCLENBQUMsbUNBQW1DLENBQUMsMkJBQTJCLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLGlGQUFpRixDQUFDLDhFQUE4RSxDQUFDLDZFQUE2RSxDQUFDLDRFQUE0RSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLGdCQUFnQixDQUFDLDBCQUEwQixDQUFDLHVCQUF1QixDQUFDLHNCQUFzQixDQUFDLHFCQUFxQixDQUFDLGlCQUFpQixDQUFDLGFBQWEsQ0FBMEQscUJBQXFCLENBQUMsUUFBUTs7QUFBQyw4QkFBOEIsd0NBQXdDLENBQUMscUNBQXFDLENBQUMsb0NBQW9DLENBQUMsbUNBQW1DLENBQUMsWUFBWTs7QUFBQywwREFBMEQsY0FBYyxDQUFDLGdCQUFnQjs7QUFBQyx5Q0FBeUMsZ0JBQWdCOztBQUFDLGdDQUFnQyxrQkFBa0IsQ0FBQyxjQUFjOztBQUFDLG9DQUFvQyxZQUFZOztBQUFDLHNDQUFzQywwQkFBMEIsQ0FBQyx1QkFBdUIsQ0FBQyxzQkFBc0IsQ0FBQyxxQkFBcUI7O0FBQUMsaUJBQWlCLFVBQVUsQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBMEQscUJBQXFCOztBQUFDLFlBQVksaUJBQWlCLENBQUMsU0FBUzs7QUFBQyxtQkFBbUIsVUFBVSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsUUFBUTs7QUFBQyw0QkFBNEIsaUJBQWlCLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLGVBQWU7O0FBQUMsNkNBQTZDLGFBQWE7O0FBQUMsNkJBQTZCLGtCQUFrQixDQUFDLFVBQVU7O0FBQUMsd0JBQXdCLGtCQUFrQixDQUFDLFVBQVU7O0FBQUMsNkJBQTZCLGVBQWUsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxNQUFNOztBQUFDLG9DQUFvQyxZQUFZLENBQUMseUJBQXlCLENBQUMsOEJBQThCLENBQUMsaUNBQWlDLENBQUMsNEJBQTRCLENBQUMsbUNBQW1DLENBQUMsMkJBQTJCLENBQW9KLGtFQUFrRTs7QUFBQyx1Q0FBdUMsZUFBZSxDQUFDLHlCQUF5QixDQUFDLDhCQUE4QixDQUFDLGlDQUFpQyxDQUFDLDRCQUE0QixDQUFDLG1DQUFtQyxDQUFDLDJCQUEyQixDQUF3SixvRUFBb0U7O0FBQUMseUJBQXlCLG9CQUFvQixjQUFjLENBQUMsZ0NBQWdDLGVBQWUsQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLGtCQUFrQixDQUFDLDRDQUE0QyxlQUFlLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMseUJBQXlCLENBQUMsZUFBZSxDQUFvSixrRUFBa0UsQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLENBQUMsK0NBQStDLGdCQUFnQixDQUFDLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLGdEQUFnRCxTQUFTLENBQUM7O0FBQUMsWUFBWSxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxzQkFBc0IsQ0FBQyx5QkFBeUIsQ0FBQyw0QkFBNEIsQ0FBQyxtQ0FBbUMsQ0FBQywyQkFBMkIsQ0FBb0osa0VBQWtFLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsc0NBQXNDLENBQUMsc0NBQXNDLENBQUMsbUNBQW1DLENBQUMsa0NBQWtDLENBQUMsaUNBQWlDLENBQUMsaUJBQWlCLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyw2QkFBNkIsQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsbUJBQW1CLENBQUMscUJBQXFCLENBQUMsdUJBQXVCLENBQUMsd0JBQXdCLENBQUMsb0JBQW9CLENBQUMsa0JBQWtCLENBQUMsaUNBQWlDLENBQUMsa0NBQWtDLENBQUMsaUNBQWlDOztBQUFDLHVCQUF1QixpQkFBaUIsQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLDZCQUE2Qjs7QUFBQyxnREFBZ0QsVUFBVSxDQUFDLGtCQUFrQixDQUFDLGlCQUFpQjs7QUFBQyxvRUFBb0UsWUFBWTs7QUFBQyw0REFBNEQsY0FBYyxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMseUNBQXlDLENBQUMsc0NBQXNDLENBQUMscUNBQXFDLENBQUMsb0NBQW9DLENBQUMsZUFBZSxDQUFDLG9CQUFvQixDQUFDLHVCQUF1QixDQUFDLDRCQUE0QixDQUFDLG1DQUFtQyxDQUFDLDJCQUEyQixDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBMEQscUJBQXFCLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLENBQUMsbUJBQW1CLENBQUMscUJBQXFCLENBQUMsdUJBQXVCLENBQUMsd0JBQXdCLENBQUMsb0JBQW9CLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsV0FBVzs7QUFBQyxnR0FBZ0csUUFBUSxDQUFDLFNBQVM7O0FBQUMsb0ZBQW9GLFVBQVU7O0FBQUMsb0lBQW9JLGFBQWEsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVOztBQUFDLDRGQUE0RixpQkFBaUIsQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLGtCQUFrQixDQUFDLFFBQVE7O0FBQUMsc0VBQXNFLGNBQWMsQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxzQkFBc0IsQ0FBQyxlQUFlLENBQUMsa0JBQWtCLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxhQUFhOztBQUFDLG9FQUFvRSxXQUFXLENBQUMsVUFBVTs7QUFBQyxnRkFBZ0YsYUFBYSxDQUFDLGNBQWM7O0FBQUMsOEdBQThHLFVBQVU7O0FBQUMsd0hBQXdILGVBQWU7O0FBQUMsOFdBQThXLGVBQWUsQ0FBQyxpQkFBaUI7O0FBQUMsd0dBQXdHLFVBQVUsQ0FBQyxrQkFBa0I7O0FBQUMsd09BQXdPLDRCQUE0QixDQUFDLG9CQUFvQjs7QUFBQyxvUUFBb1EsK0JBQStCOztBQUFDLGtHQUFrRyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLGlDQUFpQyxDQUFDLGtDQUFrQyxDQUFDLHlCQUF5QixDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsVUFBVTs7QUFBQyxvRkFBb0YsYUFBYSxDQUFDLGNBQWM7O0FBQUMsa0dBQWtHLGtDQUFrQzs7QUFBQyxnRkFBZ0YsWUFBWTs7QUFBQyxnSkFBZ0osYUFBYTs7QUFBQyw0TkFBNE4sd0JBQXdCOztBQUFDLHdJQUF3SSxXQUFXOztBQUFDLDRHQUFtSywwQkFBbUIsQ0FBbkIsbUJBQW1CLENBQUMsVUFBVTs7QUFBQyxxSUFBcUksU0FBUyxDQUFDLFVBQVUsQ0FBQyxrQkFBa0I7O0FBQUMsaUtBQWlLLCtCQUErQjs7QUFBQyxvQ0FBb0MsVUFBVSxDQUFDLGtCQUFrQjs7QUFBQyw2S0FBNkssYUFBYSxDQUFDLGtCQUFrQjs7QUFBQyw4Q0FBOEMsYUFBYSxDQUFDLGtCQUFrQjs7QUFBQyxvSUFBb0ksY0FBYzs7QUFBQyxvSUFBb0ksY0FBYzs7QUFBQyxvRkFBb0YsY0FBYzs7QUFBQyw4QkFBOEIsVUFBVSxDQUFDLGdCQUFnQjs7QUFBQyxtSEFBbUgsNkJBQTZCOztBQUFDLHFDQUFxQyxvQkFBb0IsQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsZ0NBQWdDLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyx5QkFBeUIsQ0FBQyw4QkFBOEIsQ0FBQyxpQ0FBaUMsQ0FBQyw0QkFBNEIsQ0FBQyxtQ0FBbUMsQ0FBQywyQkFBMkI7O0FBQUMsc0VBQXNFLHVCQUF1QixDQUFDLG9CQUFvQixDQUFDLG1CQUFtQixDQUFDLGtCQUFrQixDQUFDLFdBQVcsQ0FBQyxnQkFBZ0I7O0FBQUMsMERBQTBELGVBQWUsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLG9CQUFvQixDQUFDLGVBQWUsQ0FBQyxpQkFBaUIsQ0FBMEQscUJBQXFCLENBQUMseUNBQXlDLENBQUMsc0NBQXNDLENBQUMscUNBQXFDLENBQUMsb0NBQW9DLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLHNCQUFzQjs7QUFBQywrRUFBK0UsYUFBYSxDQUFDLGlCQUFpQixDQUFDLGdCQUFnQjs7QUFBQyxtR0FBbUcsb0JBQW9CLENBQUMsUUFBUSxDQUFDLFNBQVM7O0FBQUMsc0dBQXNHLFNBQVMsQ0FBQyxRQUFRLENBQUMsY0FBYzs7QUFBQyx3R0FBd0csY0FBYyxDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUMsYUFBYSxDQUFDLG9CQUFvQjs7QUFBQyxrSEFBa0gsa0JBQWtCOztBQUFDLG9IQUFvSCxhQUFhLENBQUMsY0FBYzs7QUFBQyxxSEFBcUgsV0FBVyxDQUFDLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsNkJBQTZCOztBQUFDLHFEQUFxRCxzQkFBc0I7O0FBQUMsK0NBQStDLG9CQUFvQixDQUFvSixrRUFBa0U7O0FBQUMsb0VBQW9FLFdBQVcsQ0FBQyxnQkFBZ0I7O0FBQUMsZ0RBQWdELHlCQUF5QixDQUFDLDhCQUE4QixDQUFDLGlDQUFpQyxDQUFDLDRCQUE0QixDQUFDLG1DQUFtQyxDQUFDLDJCQUEyQjs7QUFBQyw4RUFBOEUsMEJBQTBCOztBQUFDLG9CQUFvQixlQUFlOztBQUFDLDhCQUE4QixjQUFjLENBQUMsZ0NBQWdDOztBQUFDLFVBQVUsVUFBVSxDQUFDLHNDQUFzQyxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxpQ0FBaUMsQ0FBQyxrQ0FBa0MsQ0FBQyxpQ0FBaUMsQ0FBQyxpQkFBaUIsQ0FBQyxlQUFlOztBQUFDLHNDQUFzQyxZQUFZLENBQUMsZUFBZSxDQUFDLGdCQUFnQixDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsc0NBQXNDLENBQUMsbUNBQW1DLENBQUMsa0NBQWtDLENBQUMsaUNBQWlDLENBQUMsaUJBQWlCOztBQUFDLDRCQUE0QixpQkFBaUIsQ0FBQyxzQkFBc0IsQ0FBQyx5QkFBeUIsQ0FBQyw0QkFBNEIsQ0FBQyxtQ0FBbUMsQ0FBQywyQkFBMkIsQ0FBQyxnQkFBZ0IsQ0FBQyxvQkFBb0IsQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFvSixrRUFBa0UsQ0FBQyxRQUFRLENBQUMseUJBQXlCLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQyxpQkFBaUI7O0FBQUMsK0NBQStDLDRCQUE0QixnQkFBZ0IsQ0FBQyxTQUFTLENBQUM7O0FBQUMseUJBQXlCLDRCQUE0QixnQkFBZ0IsQ0FBQyxXQUFXLENBQUM7O0FBQUMsMkNBQTJDLGVBQWUsQ0FBb0osa0VBQWtFLENBQUMsZUFBZSxDQUFDLGVBQWUsQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQyxxQ0FBcUMsQ0FBQyxrQ0FBa0MsQ0FBQyxpQ0FBaUMsQ0FBQyxnQ0FBZ0M7O0FBQUMsMkRBQTJELFlBQVksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLENBQTRELHNCQUFzQixDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsb0NBQW9DLENBQUMsaUNBQWlDLENBQUMsZ0NBQWdDLENBQUMsK0JBQStCOztBQUFDLDhDQUE4QyxjQUFjLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxvQkFBb0IsQ0FBQyxVQUFVOztBQUFDLDhDQUE4QyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLG1CQUFtQjs7QUFBQyxvREFBb0QsU0FBUzs7QUFBQyxnRUFBZ0UsV0FBVyxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMseUNBQXlDLENBQUMsc0NBQXNDLENBQUMscUNBQXFDLENBQUMsb0NBQW9DLENBQUMsaUJBQWlCLENBQUMsc0JBQXNCLENBQUMseUJBQXlCLENBQUMsNEJBQTRCLENBQUMsbUNBQW1DLENBQUMsMkJBQTJCOztBQUFDLHVFQUF1RSxnQkFBZ0I7O0FBQUMsNElBQTRJLGtCQUFrQixDQUFDLGFBQWE7O0FBQUMsdUVBQXVFLGtCQUFrQixDQUFDLGFBQWE7O0FBQUMsdUVBQXVFLFFBQVE7O0FBQUMscURBQXFELGtCQUFrQjs7QUFBQyxZQUFZLGNBQWMsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsZUFBZSxDQUFDLGtCQUFrQixDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsNkJBQTZCLENBQUMsa0JBQWtCOztBQUFDLFVBQVUsaUJBQWlCLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQW9KLGtFQUFrRSxDQUFDLGlCQUFpQixDQUFDLHNCQUFzQixDQUFDLHlCQUF5QixDQUFDLDRCQUE0QixDQUFDLG1DQUFtQyxDQUFDLDJCQUEyQixDQUFDLHNDQUFzQyxDQUEwRCxxQkFBcUIsQ0FBQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsQ0FBQyxxQkFBcUIsQ0FBQyx1QkFBdUIsQ0FBQyx3QkFBd0IsQ0FBQyxvQkFBb0IsQ0FBQyxlQUFlLENBQUMsa0JBQWtCLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyx5QkFBeUIsQ0FBQyxpQ0FBaUMsQ0FBQyxrQ0FBa0MsQ0FBQyxpQ0FBaUMsQ0FBQyxlQUFlOztBQUFDLDBCQUEwQixrQkFBa0I7O0FBQUMsbUJBQW1CLGdCQUFnQixDQUFDLFlBQVksQ0FBQyw0QkFBNEIsQ0FBd0osb0VBQW9FOztBQUFDLG9CQUFvQixhQUFhOztBQUFDLG9CQUFvQixpQkFBaUIsQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLDZCQUE2Qjs7QUFBQyxtQkFBbUIsc0JBQXNCOztBQUFDLGlCQUFpQix1QkFBdUI7O0FBQUMsMkJBQTJCLHNCQUFzQjs7QUFBQyx5QkFBeUIsaUJBQWlCLENBQUMsYUFBYTs7QUFBQyw0RUFBNEUsVUFBVSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsK0JBQStCLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyx3QkFBd0IsQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQTBELHFCQUFxQjs7QUFBQyx3RkFBd0YsK0JBQStCLENBQUMsZUFBZTs7QUFBQyw2RUFBNkUsaUJBQWlCLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsbUJBQW1CLENBQUMsb0NBQW9DLENBQUMsaUNBQWlDLENBQUMsZ0NBQWdDLENBQUMsK0JBQStCLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsZUFBZTs7QUFBQyxtSEFBbUgsYUFBYTs7QUFBQyx1R0FBdUcsVUFBVTs7QUFBQyxtQ0FBbUMsZ0JBQWdCLENBQUMsbUJBQW1CLENBQUMscUJBQXFCLENBQUMsdUJBQXVCLENBQUMsd0JBQXdCLENBQUMsb0JBQW9CLENBQUMsZUFBZSxDQUFDLG9CQUFvQixDQUFDLHVCQUF1QixDQUFDLDRCQUE0QixDQUFDLG1DQUFtQyxDQUFDLDJCQUEyQixDQUFDLFNBQVM7O0FBQUMsbUJBQW1CLFdBQVc7O0FBQUMsc0JBQXlLLGtFQUFrRSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhLENBQUMsZUFBZTs7QUFBQyw2QkFBNkIsVUFBVSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsUUFBUTs7QUFBQyw4QkFBOEIsb0JBQW9CLENBQUMsVUFBVTs7QUFBQyxnQ0FBZ0MsVUFBVTs7QUFBQyxvQ0FBb0Msb0JBQW9CLENBQUMsVUFBVTs7QUFBQyxvQkFBb0IsV0FBVyxDQUEwRCxxQkFBcUIsQ0FBQyxXQUFXLENBQUMsWUFBWTs7QUFBQyx5QkFBeUIsb0JBQW9CLFdBQVcsQ0FBQzs7QUFBQyw4QkFBOEIsb0JBQW9COztBQUFDLDZCQUE2QixTQUFTLENBQUMsV0FBVyxDQUFDLGdCQUFnQjs7QUFBQywrQ0FBK0MsV0FBVyxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMseUNBQXlDLENBQUMsc0NBQXNDLENBQUMscUNBQXFDLENBQUMsb0NBQW9DLENBQUMsaUJBQWlCLENBQUMsc0JBQXNCLENBQUMseUJBQXlCLENBQUMsNEJBQTRCLENBQUMsbUNBQW1DLENBQUMsMkJBQTJCOztBQUFDLHNEQUFzRCxnQkFBZ0I7O0FBQUMsMEdBQTBHLGtCQUFrQixDQUFDLGFBQWE7O0FBQUMsc0RBQXNELGtCQUFrQixDQUFDLGFBQWE7O0FBQUMsc0RBQXNELFFBQVE7O0FBQUMsdUJBQXVCLGlCQUFpQixDQUFDLG9CQUFvQixDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUE0RCxzQkFBc0IsQ0FBQyxxQkFBcUI7O0FBQUMsMkJBQTJCLGVBQWUsQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxXQUFXOztBQUFDLDRCQUE0QixxQkFBcUIsQ0FBQyxpQkFBaUIsQ0FBQyxzQkFBc0IsQ0FBQyx5QkFBeUIsQ0FBQyw0QkFBNEIsQ0FBQyxtQ0FBbUMsQ0FBQywyQkFBMkIsQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLG9CQUFvQixDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBMEQscUJBQXFCLENBQUMsa0VBQWtFLENBQUMsK0RBQStELENBQUMsOERBQThELENBQUMsNkRBQTZEOztBQUFDLDZCQUE2QixpQkFBaUIsQ0FBQyxTQUFTLENBQUMsaUJBQWlCLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyw2QkFBNkIsQ0FBQyxhQUFhLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsUUFBUTs7QUFBQywwQ0FBMEMsa0JBQWtCLENBQUMsb0JBQW9COztBQUFDLDhDQUE4QyxhQUFhOztBQUFDLHdDQUF3QyxvQkFBb0I7O0FBQUMsNEJBQTRCLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxlQUFlOztBQUFDLGtDQUFrQyxjQUFjLENBQUMsWUFBWSxDQUFDLHFCQUFxQjs7QUFBQyxpQkFBaUIsYUFBYSxDQUFDLGdCQUFnQjs7QUFBQyxvQ0FBb0MsZUFBZTs7QUFBQywyRkFBMkYsU0FBUyxDQUFDLE9BQU87O0FBQUMsaURBQWlELFdBQVc7O0FBQUMsb0JBQW9CLE9BQU8sQ0FBQyxRQUFRLENBQUMsaUNBQWlDLENBQUMsa0NBQWtDLENBQUMsNEJBQTRCLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxvQkFBb0I7O0FBQUMsNkJBQTZCLFFBQVEsQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLHlCQUF5Qjs7QUFBQyxvQkFBb0IsV0FBVyxDQUEwRCxxQkFBcUIsQ0FBQyx1QkFBdUI7O0FBQUMsWUFBWSxVQUFVLENBQUMsZUFBZSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxzQ0FBc0MsQ0FBMEQscUJBQXFCLENBQUMsZ0JBQWdCLENBQUMsbUJBQW1CLENBQUMscUJBQXFCLENBQUMsdUJBQXVCLENBQUMsd0JBQXdCLENBQUMsb0JBQW9CLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLHNCQUFzQixDQUFDLHlCQUF5QixDQUFDLDRCQUE0QixDQUFDLG1DQUFtQyxDQUFDLDJCQUEyQixDQUFvSixrRUFBa0UsQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLHlCQUF5QixDQUFDLGlDQUFpQyxDQUFDLGtDQUFrQyxDQUFDLGlDQUFpQyxDQUFDLGVBQWU7O0FBQUMsbUJBQW1CLFVBQVUsQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLFFBQVE7O0FBQUMsbUJBQW1CLGdCQUFnQjs7QUFBQyxzQkFBc0IsWUFBWSxDQUFDLGtCQUFrQixDQUFDLGlCQUFpQixDQUFDLGVBQWU7O0FBQUMsZ0NBQWdDLE9BQU8sQ0FBQyxRQUFRLENBQUMsaUNBQWlDLENBQUMsa0NBQWtDLENBQUMsNEJBQTRCLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxvQkFBb0I7O0FBQUMsK0JBQStCLGdCQUFnQixDQUF3SixvRUFBb0UsQ0FBQyw0QkFBNEIsQ0FBQyxZQUFZOztBQUFDLHlDQUF5QyxRQUFRLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyx3QkFBd0IsQ0FBQyxzQkFBc0IsQ0FBQyxvQkFBb0I7O0FBQUMsbUJBQW1CLEtBQUssQ0FBQyx5QkFBeUIsQ0FBQyw4QkFBOEIsQ0FBQyxpQ0FBaUMsQ0FBQyw0QkFBNEIsQ0FBQyxtQ0FBbUMsQ0FBQywyQkFBMkIsQ0FBb0osa0VBQWtFOztBQUFDLHNCQUFzQixRQUFRLENBQUMseUJBQXlCLENBQUMsOEJBQThCLENBQUMsaUNBQWlDLENBQUMsNEJBQTRCLENBQUMsbUNBQW1DLENBQUMsMkJBQTJCLENBQW9KLGtFQUFrRTs7QUFBQyxjQUFjLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBQyxVQUFVOztBQUFDLDRCQUE0QixZQUFZOztBQUFDLG9CQUFvQixXQUFXLENBQUMsU0FBUyxDQUFDLFVBQVU7O0FBQUMsb0JBQW9CLFVBQVUsQ0FBQyxVQUFVLENBQUMsNEJBQTRCLENBQUMsWUFBWTs7QUFBQyx3QkFBd0Isc0JBQXNCOztBQUFDLHNCQUFzQixXQUFXOztBQUFDLDBDQUEwQyxVQUFVOztBQUFDLDBDQUEwQyxVQUFVLENBQUMsb0JBQW9COztBQUFDLHNCQUFzQixZQUFZLENBQUMsdUJBQXVCOztBQUFDLHlCQUF5QixzQkFBc0IseUJBQXlCLENBQUM7O0FBQUMseUJBQXlCLHNCQUFzQix5QkFBeUIsQ0FBQzs7QUFBQywwQkFBMEIsc0JBQXNCLHlCQUF5QixDQUFDOztBQUFDLGNBQWMsaUJBQWlCLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyw2QkFBNkI7O0FBQUMsUUFBUSxpQkFBaUI7O0FBQUMsV0FBVyx1QkFBdUIsQ0FBQyxvQkFBb0IsQ0FBQyxtQkFBbUIsQ0FBQyxrQkFBa0IsQ0FBQyxlQUFlOztBQUFDLGVBQWUsaUJBQWlCOztBQUFDLGNBQWMsY0FBYzs7QUFBQyw0QkFBNEIsaUJBQWlCLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxvQkFBb0I7O0FBQUMsaUJBQWlCLFlBQVk7O0FBQUMsK0RBQStELGFBQWE7O0FBQUMsZ0JBQWdCLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsa0JBQWtCLENBQUMsUUFBUTs7QUFBQyxpQkFBaUIsVUFBVSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsUUFBUTs7QUFBQyxrQkFBa0IsaUJBQWlCLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxRQUFROztBQUFDLG9CQUFvQixpQkFBaUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsd0JBQXdCLENBQUMsY0FBYyxDQUFDLDJDQUEyQyxDQUFDLFNBQVMsQ0FBQyxlQUFlLENBQUMsNEJBQTRCLENBQUMsNkJBQTZCLENBQUMscUJBQXFCLENBQUMsMEJBQTBCLENBQUMsNkJBQTZCLENBQUMsNEJBQTRCLENBQUMsbUNBQW1DLENBQUMsMkJBQTJCOztBQUFDLDJCQUEyQixNQUFNLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyw4QkFBOEIsQ0FBQyx1QkFBdUIsQ0FBQyw0QkFBNEIsQ0FBQywrQkFBK0IsQ0FBQyw0QkFBNEIsQ0FBQyxtQ0FBbUMsQ0FBQywyQkFBMkI7O0FBQUMsaUNBQWlDLFlBQVk7O0FBQUMsaUJBQWlCLFVBQVUsQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLFFBQVE7O0FBQUMsa0JBQWtCLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsa0JBQWtCLENBQUMsUUFBUTs7QUFBQyxpQkFBaUIsWUFBWSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsbUJBQW1CLENBQTBELHFCQUFxQixDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLG1DQUFtQyxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLFNBQVM7O0FBQUMsZ0NBQWdDLGFBQWE7O0FBQUMsb0JBQW9CLFlBQVk7O0FBQUMsc0NBQXNDLGFBQWE7O0FBQUMsK0JBQWtMLGtFQUFrRTs7QUFBQyxzR0FBc0csWUFBWTs7QUFBQyxpQ0FBaUMsYUFBYTs7QUFBQyw4REFBOEQsYUFBYTs7QUFBQyxpREFBaUQsaUJBQWlCLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBb0osa0VBQWtFLENBQUMsWUFBWSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLHlDQUF5QyxDQUFDLHNDQUFzQyxDQUFDLHFDQUFxQyxDQUFDLG9DQUFvQyxDQUFDLGVBQWUsQ0FBQyxvQkFBb0IsQ0FBQyx1QkFBdUIsQ0FBQyw0QkFBNEIsQ0FBQyxtQ0FBbUMsQ0FBQywyQkFBMkIsQ0FBQyxTQUFTLENBQTBELHFCQUFxQixDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixDQUFDLG1CQUFtQixDQUFDLHFCQUFxQixDQUFDLHVCQUF1QixDQUFDLHdCQUF3QixDQUFDLG9CQUFvQjs7QUFBQyxtREFBbUQsY0FBYyxDQUFDLFVBQVUsQ0FBQyxpQkFBaUI7O0FBQUMsa0VBQWtFLGtCQUFrQjs7QUFBQyxpQkFBaUIsVUFBVSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsUUFBUTs7QUFBQyxrQkFBa0IsaUJBQWlCLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxRQUFROztBQUFDLDBCQUE2SyxrRUFBa0UsQ0FBQyxpQkFBaUIsQ0FBQyxnQkFBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQyxpQkFBaUI7O0FBQUMseUNBQXlDLG9CQUFvQixDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsaUJBQWlCOztBQUFDLDhGQUE4RixhQUFhOztBQUFDLHdFQUF3RSxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsa0JBQWtCLENBQUMsVUFBVSxDQUFDLHdDQUF3QyxDQUFDLHFDQUFxQyxDQUFDLG9DQUFvQyxDQUFDLG1DQUFtQzs7QUFBQyx5REFBeUQsYUFBYTs7QUFBQywyR0FBMkcsd0NBQXdDLENBQUMscUNBQXFDLENBQUMsb0NBQW9DLENBQUMsbUNBQW1DOztBQUFDLDhCQUE4QixVQUFVLENBQUMsUUFBUSxDQUFDLFlBQVk7O0FBQUMsNkNBQTZDLFVBQVUsQ0FBQyx1QkFBdUIsQ0FBQyxlQUFlOztBQUFDLGlEQUFpRCxXQUFXLENBQUMsVUFBVTs7QUFBQyxtRUFBbUUsd0JBQXdCLENBQUMsb0JBQW9CLENBQUMsaUJBQWlCLENBQUMsc0JBQXNCLENBQUMseUJBQXlCLENBQUMsNEJBQTRCLENBQUMsbUNBQW1DLENBQUMsMkJBQTJCLENBQUMsY0FBYyxDQUFDLFdBQVc7O0FBQUMseUVBQXlFLHdCQUF3QixDQUFDLFVBQVU7O0FBQUMsd0NBQXFGLGVBQWUsQ0FBQyxlQUFlLENBQUMsZ0JBQWdCOztBQUFDLHdCQUF3QixhQUFhLENBQUMsWUFBWTs7QUFBQyx3Q0FBd0MsYUFBYTs7QUFBQyw2QkFBNkIsb0JBQW9CLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTOztBQUFDLGdFQUFnRSxpQkFBaUIsQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLE1BQU07O0FBQUMsZ0RBQWdELFVBQVUsQ0FBQyx1QkFBdUIsQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLGdCQUFnQixDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjOztBQUFDLHNFQUFzRSxzQkFBc0IsQ0FBQyxTQUFTOztBQUFDLG1IQUFtSCx1Q0FBdUMsQ0FBQyxvQ0FBb0MsQ0FBQyxtQ0FBbUMsQ0FBQyxrQ0FBa0M7O0FBQUMsaUJBQWlCLFVBQVUsQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLFFBQVE7O0FBQUMsa0JBQWtCLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsa0JBQWtCLENBQUMsUUFBUTs7QUFBQyxnQkFBZ0Isa0JBQWtCLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLDZCQUE2QixDQUFDLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLFlBQVk7O0FBQUMsMkJBQTJCLGFBQWE7O0FBQUMsYUFBYSxrQkFBa0IsQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLDZCQUE2Qjs7QUFBQyxpQkFBaUIsVUFBVSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsUUFBUTs7QUFBQyxrQkFBa0IsaUJBQWlCLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxRQUFROztBQUFDLHVCQUF1QixvQkFBb0IsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyw4R0FBOEcsQ0FBNEQsc0JBQXNCOztBQUFDLDJCQUEyQixXQUFXOztBQUFDLHlCQUF5QixTQUFTLENBQUMsa0JBQWtCOztBQUFDLGlCQUFpQixVQUFVLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxRQUFROztBQUFDLGtCQUFrQixpQkFBaUIsQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLGtCQUFrQixDQUFDLFFBQVE7O0FBQUMsZ0NBQWdDLHlCQUF5QixDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUMsZ0JBQWdCLENBQTBELHFCQUFxQixDQUFDLGlCQUFpQjs7QUFBQyxzQ0FBc0Msa0JBQWtCOztBQUFDLHdDQUF3QyxrQkFBa0IsQ0FBQyxvQkFBb0I7O0FBQUMseUNBQXlDLGlCQUFpQixDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsNkJBQTZCLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxtQkFBbUIsQ0FBQyxvQkFBb0I7O0FBQUMsK0NBQStDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxlQUFlOztBQUFDLHNDQUErRixxQkFBcUI7O0FBQUMseUNBQXlDLGNBQWMsQ0FBQyxhQUFhLENBQUMsZUFBZTs7QUFBQyw0REFBNEQsWUFBWTs7QUFBQyxvREFBb0Qsa0JBQWtCLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLGlCQUFpQjs7QUFBQyx5REFBeUQsYUFBYSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUMsb0NBQW9DLENBQUMsaUNBQWlDLENBQUMsZ0NBQWdDLENBQUMsK0JBQStCOztBQUFDLDBFQUEwRSxtQkFBbUIsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsNENBQTRDLENBQW1GLG9DQUFvQzs7QUFBQyw2REFBNkQsWUFBWTs7QUFBQyxxRUFBcUUsYUFBYTs7QUFBQyxtQkFBbUIsS0FBSyxTQUFTLENBQUMsR0FBRyxTQUFTLENBQUM7O0FBQUMsMkJBQTJCLEtBQUssU0FBUyxDQUFDLEdBQUcsU0FBUyxDQUFDOztBQUF5RyxtQkFBbUIsZUFBZSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsY0FBYzs7QUFBQyxzQkFBc0Isa0JBQWtCLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyw0QkFBNEIsQ0FBQyxvQkFBb0I7O0FBQUMseUNBQXlDLGVBQWU7O0FBQUMsNENBQTRDLGtCQUFrQjs7QUFBQyx1QkFBdUIsNEJBQTRCLENBQUMsb0JBQW9CLENBQUMsa0JBQWtCLENBQUMsbUJBQW1CLENBQUMsMEJBQTBCOztBQUFDLGlCQUFpQixVQUFVLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxRQUFROztBQUFDLGtCQUFrQixpQkFBaUIsQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLGtCQUFrQixDQUFDLFFBQVE7O0FBQUMsMERBQTBELGVBQWUsQ0FBQyxzQkFBc0I7O0FBQUMsZ0VBQWdFLHdCQUF3QixDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsVUFBVTs7QUFBQyxzRUFBc0UsZUFBZTs7QUFBQyxtRUFBbUUsUUFBUTs7QUFBQyxzSUFBc0ksaUJBQWlCOztBQUFDLHlFQUF5RSwrQkFBK0I7O0FBQUMscUZBQXFGLFNBQVMsQ0FBQyxhQUFhOztBQUFDLHdGQUF3RixtQkFBbUI7O0FBQUMsaUJBQWlCLFVBQVUsQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLFFBQVE7O0FBQUMsa0JBQWtCLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsa0JBQWtCLENBQUMsUUFBUTs7QUFBQyxnQkFBZ0IsY0FBYzs7QUFBQyxrQkFBa0IsaUJBQWlCLENBQUMsd0JBQXdCLENBQUMsWUFBWSxDQUFDLGdCQUFnQixDQUFDLG1CQUFtQixDQUFDLHFCQUFxQixDQUFDLHVCQUF1QixDQUFDLHdCQUF3QixDQUFDLG9CQUFvQixDQUE0RCxzQkFBc0I7O0FBQUMsNEJBQTRCLGFBQWE7O0FBQUMsOEJBQThCLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxrQkFBa0IsQ0FBQyxxQkFBcUIsQ0FBQyxTQUFTLENBQTBELHFCQUFxQjs7QUFBQyxxQ0FBcUMsZ0JBQWdCOztBQUFDLHFDQUFxQyxnQkFBZ0I7O0FBQUMscUNBQXFDLGdCQUFnQjs7QUFBQyxxQ0FBcUMsZ0JBQWdCOztBQUFDLDhCQUE4QixVQUFVLENBQUMsV0FBVzs7QUFBQyxxQ0FBcUMsU0FBUyxDQUFDLFFBQVE7O0FBQUMscUNBQXFDLFVBQVUsQ0FBQyxRQUFROztBQUFDLHFDQUFxQyxTQUFTLENBQUMsV0FBVzs7QUFBQyxxQ0FBcUMsVUFBVSxDQUFDLFdBQVc7O0FBQUMsMEJBQTBCLDhCQUE4QixVQUFVLENBQUMsV0FBVyxDQUFDLHFDQUFxQyxTQUFTLENBQUMsUUFBUSxDQUFDLHFDQUFxQyxVQUFVLENBQUMsUUFBUSxDQUFDLHFDQUFxQyxTQUFTLENBQUMsV0FBVyxDQUFDLHFDQUFxQyxVQUFVLENBQUMsV0FBVyxDQUFDOztBQUFDLGtCQUFrQixjQUFjLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLFlBQVk7O0FBQUMsaUNBQWlDLHlCQUF5QixDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUMsZ0JBQWdCLENBQUMsaUJBQWlCOztBQUFDLHVDQUF1QyxrQkFBa0I7O0FBQUMseUNBQXlDLGtCQUFrQixDQUFDLG9CQUFvQjs7QUFBQywwQ0FBMEMsaUJBQWlCLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyw2QkFBNkIsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUMsZUFBZSxDQUFDLGtCQUFrQixDQUFDLG1CQUFtQixDQUFDLG9CQUFvQjs7QUFBQyxnREFBZ0QsY0FBYyxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLGVBQWU7O0FBQUMsMENBQTBDLGNBQWMsQ0FBQyxhQUFhLENBQUMsZUFBZTs7QUFBQyw2REFBNkQsWUFBWTs7QUFBQyxxREFBcUQsa0JBQWtCLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLGlCQUFpQjs7QUFBQywwREFBMEQsYUFBYSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUMsb0NBQW9DLENBQUMsaUNBQWlDLENBQUMsZ0NBQWdDLENBQUMsK0JBQStCOztBQUFDLDJFQUEyRSxtQkFBbUIsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsNENBQTRDLENBQW1GLG9DQUFvQzs7QUFBQyw4REFBOEQsWUFBWTs7QUFBQyxzRUFBc0UsYUFBYTs7QUFBQyxvREFBb0QscUJBQXFCLENBQUMsb0JBQW9COztBQUFDLG1FQUFtRSxnQkFBZ0I7O0FBQUMsY0FBYyxrQkFBa0IsQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLDZCQUE2Qjs7QUFBQyxtQkFBbUIsS0FBSyxTQUFTLENBQUMsR0FBRyxTQUFTLENBQUM7O0FBQUMsMkJBQTJCLEtBQUssU0FBUyxDQUFDLEdBQUcsU0FBUyxDQUFDOztBQUF5RyxpQkFBaUIsVUFBVSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsUUFBUTs7QUFBQyxrQkFBa0IsaUJBQWlCLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxRQUFROztBQUFDLDBDQUEwQyxVQUFVLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxRQUFROztBQUFDLHdHQUF3RyxVQUFVLENBQUMsU0FBUyxDQUFDLDRFQUE0RSxDQUFDLHlFQUF5RSxDQUFDLHdFQUF3RSxDQUFDLHVFQUF1RSxDQUFDLFlBQVk7O0FBQUMsMElBQTBJLFNBQVMsQ0FBQyxPQUFPLENBQUMsY0FBYzs7QUFBQyw4QkFBOEIsWUFBWSxDQUFDLGVBQWU7O0FBQUMsZ0NBQWdDLG9CQUFvQixDQUFDLFNBQVMsQ0FBQyxlQUFlLENBQUMsb0JBQW9CLENBQUMsb0JBQW9CLENBQUMsaUJBQWlCLENBQUMsc0JBQXNCLENBQUMseUJBQXlCLENBQUMsNEJBQTRCLENBQUMsbUNBQW1DLENBQUMsMkJBQTJCLENBQUMsYUFBYSxDQUFDLDZEQUE2RCxDQUFDLDBEQUEwRCxDQUFDLHlEQUF5RCxDQUFDLHdEQUF3RCxDQUFDLGNBQWM7O0FBQUMsc0NBQXNDLFNBQVM7O0FBQUMsZ0RBQWdELGtCQUFrQjs7QUFBQyxnQ0FBZ0MsYUFBYSxDQUFDLGdCQUFnQjs7QUFBQyxvQ0FBb0MsaUJBQWlCLENBQUMsYUFBYSxDQUFDLFNBQVM7O0FBQUMsMkNBQTJDLFVBQVUsQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLFFBQVE7O0FBQUMsb0RBQW9ELFVBQVUsQ0FBQyw2QkFBNkI7O0FBQUMsZ0RBQWdELG9EQUFvRCw2QkFBNkIsQ0FBQzs7QUFBQywwQkFBMEIsb0RBQW9ELDZCQUE2QixDQUFDOztBQUFDLG9FQUFvRSxnQkFBZ0I7O0FBQUMsMkRBQTJELGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQW9KLGtFQUFrRSxDQUFDLGlCQUFpQixDQUFDLHNCQUFzQixDQUFDLHlCQUF5QixDQUFDLDRCQUE0QixDQUFDLG1DQUFtQyxDQUFDLDJCQUEyQixDQUFDLGVBQWU7O0FBQUMsdUVBQXVFLGVBQWU7O0FBQUMsK0RBQStELGVBQWU7O0FBQUMsb0ZBQW9GLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsNkJBQTZCLENBQUMsc0NBQXNDLENBQUMsbUNBQW1DLENBQUMsa0NBQWtDLENBQUMsaUNBQWlDLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsU0FBUzs7QUFBQyxxRkFBcUYsMkJBQTJCLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxXQUFXOztBQUFDLG9FQUFvRSxXQUFXLENBQUMsZUFBZSxDQUFDLFNBQVM7O0FBQUMsMkVBQTJFLGlCQUFpQixDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsMEJBQTBCLENBQUMsb0JBQW9CLENBQUMsV0FBVzs7QUFBQywrREFBK0QsVUFBVSxDQUFDLHFCQUFxQixDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLDZCQUE2QixDQUFDLHlEQUF5RCxDQUFDLHNEQUFzRCxDQUFDLHFEQUFxRCxDQUFDLG9EQUFvRCxDQUFDLCtCQUErQixDQUFDLDRCQUE0QixDQUFDLDJCQUEyQixDQUFDLDBCQUEwQjs7QUFBQyxrRkFBa0YsbUJBQW1CLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxDQUFDLDZCQUE2Qjs7QUFBQywwTEFBMEwsb0JBQW9COztBQUFDLG9KQUFvSixZQUFZLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLHVCQUF1QixDQUFDLDBCQUEwQixDQUFDLDRCQUE0QixDQUFDLG1DQUFtQyxDQUFDLDJCQUEyQixDQUFDLDJEQUEyRCxDQUFDLHdEQUF3RCxDQUFDLHVEQUF1RCxDQUFDLHNEQUFzRCxDQUFvSixrRUFBa0UsQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsb0JBQW9CLENBQUMsU0FBUzs7QUFBQywwRUFBMEUsa0JBQWtCLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxrREFBa0QsQ0FBQywrQ0FBK0MsQ0FBQyw4Q0FBOEMsQ0FBQyw2Q0FBNkM7O0FBQUMsMEVBQTBFLGVBQWUsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLG9EQUFvRCxDQUFDLGlEQUFpRCxDQUFDLGdEQUFnRCxDQUFDLCtDQUErQzs7QUFBQyxvRUFBb0Usa0JBQWtCOztBQUFDLDhFQUE4RSxrQkFBa0I7O0FBQUMsbUdBQW1HLG1CQUFtQixDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQyw2QkFBNkI7O0FBQUMsNE5BQTROLG9CQUFvQjs7QUFBQyw4R0FBOEcsa0JBQWtCLENBQUMsVUFBVTs7QUFBQyw4R0FBOEcsa0JBQWtCOztBQUFDLGlCQUFpQixVQUFVLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxRQUFROztBQUFDLGtCQUFrQixpQkFBaUIsQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLGtCQUFrQixDQUFDLFFBQVE7O0FBQUMsaUJBQWlCLFdBQVcsQ0FBQyw0QkFBNEIsQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLFlBQVk7O0FBQUMsNEJBQTRCLGFBQWE7O0FBQUMsbUNBQW1DLGlCQUFpQixDQUFDLDJCQUEyQixDQUFDLFNBQVM7O0FBQUMsaUJBQWlCLFVBQVUsQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLFFBQVE7O0FBQUMsa0JBQWtCLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsa0JBQWtCLENBQUMsUUFBUTs7QUFBQyxpQkFBaUIsaUJBQWlCLENBQUMsa0JBQWtCLENBQUMsa0JBQWtCLENBQUMsaUJBQWlCLENBQUMsZ0JBQWdCLENBQTRELHNCQUFzQjs7QUFBQyw2Q0FBNkMsZ0NBQWdDLENBQUMsNkJBQTZCLENBQUMsNEJBQTRCLENBQUMsMkJBQTJCOztBQUFDLDJCQUEyQixZQUFZOztBQUFDLGNBQWMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLGlCQUFpQixDQUFDLGtCQUFrQjs7QUFBQyx1Q0FBdUMsaUJBQWlCLENBQUMsb0JBQW9CLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsNkJBQTZCLENBQUMsMEJBQTBCLENBQUMsdUJBQXVCLENBQUMsc0JBQXNCLENBQUMscUJBQXFCOztBQUFDLGlEQUFpRCxpQkFBaUIsQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLDZCQUE2QixDQUFDLDBCQUEwQixDQUFDLHVCQUF1QixDQUFDLHNCQUFzQixDQUFDLHFCQUFxQjs7QUFBQyxpQkFBaUIsVUFBVSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsUUFBUTs7QUFBQyxrQkFBa0IsaUJBQWlCLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxRQUFROztBQUFDLHdFQUF3RSxlQUFlLENBQUMsc0JBQXNCOztBQUFDLG9HQUFvRyxrQkFBa0I7O0FBQUMscUdBQXFHLGVBQWUsQ0FBQyxjQUFjLENBQUMsaUJBQWlCLENBQUMsY0FBYzs7QUFBQyw4RkFBOEYsb0JBQW9CLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQTRELHNCQUFzQixDQUFDLGlCQUFpQixDQUFDLHFCQUFxQixDQUFDLG9CQUFvQjs7QUFBQyxpQkFBaUIsVUFBVSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsUUFBUTs7QUFBQyxrQkFBa0IsaUJBQWlCLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxRQUFROztBQUFDLDRFQUE0RSx5QkFBeUI7O0FBQUMscUJBQXFCLGdCQUFnQixDQUFDLG1CQUFtQixDQUFDLHFCQUFxQixDQUFDLHVCQUF1QixDQUFDLHdCQUF3QixDQUFDLG9CQUFvQjs7QUFBQywwQ0FBMEMsZ0JBQWdCLENBQUMsbUJBQW1CLENBQUMscUJBQXFCLENBQUMsdUJBQXVCLENBQUMsd0JBQXdCLENBQUMsb0JBQW9COztBQUFDLDRFQUE0RSxnQkFBZ0IsQ0FBQyxtQkFBbUIsQ0FBQyxxQkFBcUIsQ0FBQyx1QkFBdUIsQ0FBQyx3QkFBd0IsQ0FBQyxvQkFBb0I7O0FBQUMsa0JBQWtCLGlCQUFpQixDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxZQUFZOztBQUFDLDRCQUE0QixTQUFTOztBQUFDLHNCQUFzQixpQkFBaUIsQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLDZCQUE2QixDQUFDLDhCQUE4Qjs7QUFBQyxpQkFBaUIsZ0JBQWdCLENBQUMsbUJBQW1CLENBQUMscUJBQXFCLENBQUMsdUJBQXVCLENBQUMsd0JBQXdCLENBQUMsb0JBQW9COztBQUFDLHFDQUFxQyxVQUFVLENBQUMsUUFBUSxDQUFDLFlBQVk7O0FBQUMsb0RBQW9ELFVBQVUsQ0FBQyx1QkFBdUIsQ0FBQyxlQUFlOztBQUFDLHdEQUF3RCxXQUFXLENBQUMsVUFBVTs7QUFBQywrREFBK0Qsd0JBQXdCLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDLHNCQUFzQixDQUFDLHlCQUF5QixDQUFDLDRCQUE0QixDQUFDLG1DQUFtQyxDQUFDLDJCQUEyQixDQUFDLGNBQWMsQ0FBQyxXQUFXOztBQUFDLHFFQUFxRSx3QkFBd0IsQ0FBQyxVQUFVOztBQUFDLDZDQUE2QyxpQkFBaUIsQ0FBQyxjQUFjLENBQUMsV0FBVzs7QUFBQywrQ0FBK0MsYUFBYSxDQUFDLGlCQUFpQixDQUFDLGtCQUFrQjs7QUFBQyxvREFBb0Qsb0JBQW9CLENBQUMsbUJBQW1CLENBQUMsY0FBYzs7QUFBQyx5REFBeUQsb0JBQW9CLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxxQkFBcUI7O0FBQUMsMERBQTBELGNBQWM7O0FBQUMsK0RBQStELDhCQUE4QixDQUFDLHdCQUF3Qjs7QUFBQyxnRUFBZ0UsVUFBVSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsUUFBUTs7QUFBQyw2REFBNkQsOEJBQThCOztBQUFDLG1DQUFtQyxpQkFBaUI7O0FBQUMsMkJBQTJCLGFBQWEsQ0FBQyxhQUFhOztBQUFDLGdDQUFnQyxvQkFBb0IsQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLFNBQVM7O0FBQUMsa0NBQWtDLGlCQUFpQixDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsTUFBTTs7QUFBQyxzQ0FBc0Msc0JBQXNCLENBQUMsU0FBUzs7QUFBQyxvRUFBb0UsVUFBVSxDQUFDLFdBQVc7O0FBQUMsa0JBQWtCLGlCQUFpQixDQUFDLFlBQVksQ0FBQyxrQkFBa0I7O0FBQUMsaUJBQWlCLFVBQVUsQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLFFBQVE7O0FBQUMsa0JBQWtCLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsa0JBQWtCLENBQUMsUUFBUTs7QUFBQyxzQkFBc0IsZ0JBQWdCLENBQUMsbUJBQW1CLENBQUMscUJBQXFCLENBQUMsdUJBQXVCLENBQUMsd0JBQXdCLENBQUMsb0JBQW9COztBQUFDLDZCQUE2QixpQkFBaUIsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLHdCQUF3Qjs7QUFBQyxrQ0FBa0MsU0FBUyxDQUFDLGlCQUFpQjs7QUFBQyx3QkFBbUYsc0JBQXNCLENBQUMsY0FBYyxDQUFDLFFBQVE7O0FBQUMsMEJBQTBCLGlCQUFpQixDQUFDLHdCQUF3QixDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsQ0FBQyxxQkFBcUIsQ0FBQyx1QkFBdUIsQ0FBQyx3QkFBd0IsQ0FBQyxvQkFBb0I7O0FBQUMsb0NBQW9DLGFBQWE7O0FBQUMsc0NBQXNDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxrQkFBa0IsQ0FBQyxxQkFBcUIsQ0FBQyxTQUFTLENBQTBELHFCQUFxQjs7QUFBQyw2Q0FBNkMsZ0JBQWdCOztBQUFDLDZDQUE2QyxnQkFBZ0I7O0FBQUMsNkNBQTZDLGdCQUFnQjs7QUFBQyw2Q0FBNkMsZ0JBQWdCOztBQUFDLHNDQUFzQyxVQUFVLENBQUMsV0FBVzs7QUFBQyw2Q0FBNkMsU0FBUyxDQUFDLFFBQVE7O0FBQUMsNkNBQTZDLFVBQVUsQ0FBQyxRQUFROztBQUFDLDZDQUE2QyxTQUFTLENBQUMsV0FBVzs7QUFBQyw2Q0FBNkMsVUFBVSxDQUFDLFdBQVc7O0FBQUMsMEJBQTBCLHNDQUFzQyxVQUFVLENBQUMsV0FBVyxDQUFDLDZDQUE2QyxTQUFTLENBQUMsUUFBUSxDQUFDLDZDQUE2QyxVQUFVLENBQUMsUUFBUSxDQUFDLDZDQUE2QyxTQUFTLENBQUMsV0FBVyxDQUFDLDZDQUE2QyxVQUFVLENBQUMsV0FBVyxDQUFDOztBQUFDLDhEQUE4RCxxQkFBcUIsQ0FBQyxvQkFBb0I7O0FBQUMsNkVBQTZFLGdCQUFnQjs7QUFBQyxpQ0FBaUMseUJBQXlCLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxnQkFBZ0IsQ0FBQyxpQkFBaUI7O0FBQUMsdUNBQXVDLGtCQUFrQjs7QUFBQyx5Q0FBeUMsa0JBQWtCLENBQUMsb0JBQW9COztBQUFDLDBDQUEwQyxpQkFBaUIsQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLDZCQUE2QixDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxlQUFlLENBQUMsa0JBQWtCLENBQUMsbUJBQW1CLENBQUMsb0JBQW9COztBQUFDLGdEQUFnRCxjQUFjLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsZUFBZTs7QUFBQywwQ0FBMEMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxlQUFlOztBQUFDLDZEQUE2RCxZQUFZOztBQUFDLHFEQUFxRCxrQkFBa0IsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxlQUFlLENBQUMsaUJBQWlCOztBQUFDLDBEQUEwRCxhQUFhLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxvQ0FBb0MsQ0FBQyxpQ0FBaUMsQ0FBQyxnQ0FBZ0MsQ0FBQywrQkFBK0I7O0FBQUMsMkVBQTJFLG1CQUFtQixDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyw0Q0FBNEMsQ0FBbUYsb0NBQW9DOztBQUFDLDhEQUE4RCxZQUFZOztBQUFDLHNFQUFzRSxhQUFhOztBQUFDLGtCQUFrQixjQUFjLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLFlBQVkiLCJmaWxlIjoibm9kZV9tb2R1bGVzL2Zyb2FsYS1lZGl0b3IvY3NzL2Zyb2FsYV9lZGl0b3IucGtnZC5taW4uY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyohXG4gKiBmcm9hbGFfZWRpdG9yIHYyLjkuNiAoaHR0cHM6Ly93d3cuZnJvYWxhLmNvbS93eXNpd3lnLWVkaXRvcilcbiAqIExpY2Vuc2UgaHR0cHM6Ly9mcm9hbGEuY29tL3d5c2l3eWctZWRpdG9yL3Rlcm1zL1xuICogQ29weXJpZ2h0IDIwMTQtMjAxOSBGcm9hbGEgTGFic1xuICovXG5cbi5jbGVhcmZpeDo6YWZ0ZXJ7Y2xlYXI6Ym90aDtkaXNwbGF5OmJsb2NrO2NvbnRlbnQ6XCJcIjtoZWlnaHQ6MH0uaGlkZS1ieS1jbGlwcGluZ3twb3NpdGlvbjphYnNvbHV0ZTt3aWR0aDoxcHg7aGVpZ2h0OjFweDtwYWRkaW5nOjA7bWFyZ2luOi0xcHg7b3ZlcmZsb3c6aGlkZGVuO2NsaXA6cmVjdCgwLDAsMCwwKTtib3JkZXI6MH0uZnItZWxlbWVudCwuZnItZWxlbWVudDpmb2N1c3tvdXRsaW5lOjAgc29saWQgdHJhbnNwYXJlbnR9LmZyLWJveC5mci1iYXNpYyAuZnItZWxlbWVudHtjb2xvcjojMDAwO3BhZGRpbmc6MTZweDstd2Via2l0LWJveC1zaXppbmc6Ym9yZGVyLWJveDstbW96LWJveC1zaXppbmc6Ym9yZGVyLWJveDtib3gtc2l6aW5nOmJvcmRlci1ib3g7b3ZlcmZsb3cteDphdXRvO21pbi1oZWlnaHQ6NTJweH0uZnItYm94LmZyLWJhc2ljLmZyLXJ0bCAuZnItZWxlbWVudHt0ZXh0LWFsaWduOnJpZ2h0fS5mci1lbGVtZW50e2JhY2tncm91bmQ6MCAwO3Bvc2l0aW9uOnJlbGF0aXZlO3otaW5kZXg6Mjstd2Via2l0LXVzZXItc2VsZWN0OmF1dG99LmZyLWVsZW1lbnQgYXt1c2VyLXNlbGVjdDphdXRvOy1vLXVzZXItc2VsZWN0OmF1dG87LW1vei11c2VyLXNlbGVjdDphdXRvOy1raHRtbC11c2VyLXNlbGVjdDphdXRvOy13ZWJraXQtdXNlci1zZWxlY3Q6YXV0bzstbXMtdXNlci1zZWxlY3Q6YXV0b30uZnItZWxlbWVudC5mci1kaXNhYmxlZHt1c2VyLXNlbGVjdDpub25lOy1vLXVzZXItc2VsZWN0Om5vbmU7LW1vei11c2VyLXNlbGVjdDpub25lOy1raHRtbC11c2VyLXNlbGVjdDpub25lOy13ZWJraXQtdXNlci1zZWxlY3Q6bm9uZTstbXMtdXNlci1zZWxlY3Q6bm9uZX0uZnItZWxlbWVudCBbY29udGVudGVkaXRhYmxlPXRydWVde291dGxpbmU6MCBzb2xpZCB0cmFuc3BhcmVudH0uZnItYm94IGEuZnItZmxvYXRpbmctYnRuey13ZWJraXQtYm94LXNoYWRvdzowIDFweCAzcHggcmdiYSgwLDAsMCwuMTIpLDAgMXB4IDFweCAxcHggcmdiYSgwLDAsMCwuMTYpOy1tb3otYm94LXNoYWRvdzowIDFweCAzcHggcmdiYSgwLDAsMCwuMTIpLDAgMXB4IDFweCAxcHggcmdiYSgwLDAsMCwuMTYpO2JveC1zaGFkb3c6MCAxcHggM3B4IHJnYmEoMCwwLDAsLjEyKSwwIDFweCAxcHggMXB4IHJnYmEoMCwwLDAsLjE2KTtib3JkZXItcmFkaXVzOjEwMCU7LW1vei1ib3JkZXItcmFkaXVzOjEwMCU7LXdlYmtpdC1ib3JkZXItcmFkaXVzOjEwMCU7LW1vei1iYWNrZ3JvdW5kLWNsaXA6cGFkZGluZzstd2Via2l0LWJhY2tncm91bmQtY2xpcDpwYWRkaW5nLWJveDtiYWNrZ3JvdW5kLWNsaXA6cGFkZGluZy1ib3g7aGVpZ2h0OjMycHg7d2lkdGg6MzJweDtiYWNrZ3JvdW5kOiNmZmY7Y29sb3I6IzFlODhlNTstd2Via2l0LXRyYW5zaXRpb246YmFja2dyb3VuZCAuMnMgZWFzZSAwcyxjb2xvciAuMnMgZWFzZSAwcyx0cmFuc2Zvcm0gLjJzIGVhc2UgMHM7LW1vei10cmFuc2l0aW9uOmJhY2tncm91bmQgLjJzIGVhc2UgMHMsY29sb3IgLjJzIGVhc2UgMHMsdHJhbnNmb3JtIC4ycyBlYXNlIDBzOy1tcy10cmFuc2l0aW9uOmJhY2tncm91bmQgLjJzIGVhc2UgMHMsY29sb3IgLjJzIGVhc2UgMHMsdHJhbnNmb3JtIC4ycyBlYXNlIDBzOy1vLXRyYW5zaXRpb246YmFja2dyb3VuZCAuMnMgZWFzZSAwcyxjb2xvciAuMnMgZWFzZSAwcyx0cmFuc2Zvcm0gLjJzIGVhc2UgMHM7b3V0bGluZTowO2xlZnQ6MDt0b3A6MDtsaW5lLWhlaWdodDozMnB4Oy13ZWJraXQtdHJhbnNmb3JtOnNjYWxlKDApOy1tb3otdHJhbnNmb3JtOnNjYWxlKDApOy1tcy10cmFuc2Zvcm06c2NhbGUoMCk7LW8tdHJhbnNmb3JtOnNjYWxlKDApO3RleHQtYWxpZ246Y2VudGVyO2Rpc3BsYXk6YmxvY2s7LXdlYmtpdC1ib3gtc2l6aW5nOmJvcmRlci1ib3g7LW1vei1ib3gtc2l6aW5nOmJvcmRlci1ib3g7Ym94LXNpemluZzpib3JkZXItYm94O2JvcmRlcjowfS5mci1ib3ggYS5mci1mbG9hdGluZy1idG4gc3Zney13ZWJraXQtdHJhbnNpdGlvbjp0cmFuc2Zvcm0gLjJzIGVhc2UgMHM7LW1vei10cmFuc2l0aW9uOnRyYW5zZm9ybSAuMnMgZWFzZSAwczstbXMtdHJhbnNpdGlvbjp0cmFuc2Zvcm0gLjJzIGVhc2UgMHM7LW8tdHJhbnNpdGlvbjp0cmFuc2Zvcm0gLjJzIGVhc2UgMHM7ZmlsbDojMWU4OGU1fS5mci1ib3ggYS5mci1mbG9hdGluZy1idG4gaSwuZnItYm94IGEuZnItZmxvYXRpbmctYnRuIHN2Z3tmb250LXNpemU6MTRweDtsaW5lLWhlaWdodDozMnB4fS5mci1ib3ggYS5mci1mbG9hdGluZy1idG4uZnItYnRuKy5mci1idG57bWFyZ2luLWxlZnQ6MTBweH0uZnItYm94IGEuZnItZmxvYXRpbmctYnRuOmhvdmVye2JhY2tncm91bmQ6I2ViZWJlYjtjdXJzb3I6cG9pbnRlcn0uZnItYm94IGEuZnItZmxvYXRpbmctYnRuOmhvdmVyIHN2Z3tmaWxsOiMxZTg4ZTV9LmZyLWJveCAuZnItdmlzaWJsZSBhLmZyLWZsb2F0aW5nLWJ0bnstd2Via2l0LXRyYW5zZm9ybTpzY2FsZSgxKTstbW96LXRyYW5zZm9ybTpzY2FsZSgxKTstbXMtdHJhbnNmb3JtOnNjYWxlKDEpOy1vLXRyYW5zZm9ybTpzY2FsZSgxKX1pZnJhbWUuZnItaWZyYW1le3dpZHRoOjEwMCU7Ym9yZGVyOjA7cG9zaXRpb246cmVsYXRpdmU7ZGlzcGxheTpibG9jazt6LWluZGV4OjI7LXdlYmtpdC1ib3gtc2l6aW5nOmJvcmRlci1ib3g7LW1vei1ib3gtc2l6aW5nOmJvcmRlci1ib3g7Ym94LXNpemluZzpib3JkZXItYm94fS5mci13cmFwcGVye3Bvc2l0aW9uOnJlbGF0aXZlO3otaW5kZXg6MX0uZnItd3JhcHBlcjo6YWZ0ZXJ7Y2xlYXI6Ym90aDtkaXNwbGF5OmJsb2NrO2NvbnRlbnQ6XCJcIjtoZWlnaHQ6MH0uZnItd3JhcHBlciAuZnItcGxhY2Vob2xkZXJ7cG9zaXRpb246YWJzb2x1dGU7Zm9udC1zaXplOjEycHg7Y29sb3I6I2FhYTt6LWluZGV4OjE7ZGlzcGxheTpub25lO3RvcDowO2xlZnQ6MDtyaWdodDowO292ZXJmbG93OmhpZGRlbn0uZnItd3JhcHBlci5zaG93LXBsYWNlaG9sZGVyIC5mci1wbGFjZWhvbGRlcntkaXNwbGF5OmJsb2NrfS5mci13cmFwcGVyIDo6LW1vei1zZWxlY3Rpb257YmFja2dyb3VuZDojYjVkNmZkO2NvbG9yOiMwMDB9LmZyLXdyYXBwZXIgOjpzZWxlY3Rpb257YmFja2dyb3VuZDojYjVkNmZkO2NvbG9yOiMwMDB9LmZyLWJveC5mci1iYXNpYyAuZnItd3JhcHBlcntiYWNrZ3JvdW5kOiNmZmY7Ym9yZGVyOjA7Ym9yZGVyLXRvcDowO3RvcDowO2xlZnQ6MH0uZnItYm94LmZyLWJhc2ljLmZyLXRvcCAuZnItd3JhcHBlcntib3JkZXItdG9wOjA7Ym9yZGVyLXJhZGl1czowIDAgMnB4IDJweDstbW96LWJvcmRlci1yYWRpdXM6MCAwIDJweCAycHg7LXdlYmtpdC1ib3JkZXItcmFkaXVzOjAgMCAycHggMnB4Oy1tb3otYmFja2dyb3VuZC1jbGlwOnBhZGRpbmc7LXdlYmtpdC1iYWNrZ3JvdW5kLWNsaXA6cGFkZGluZy1ib3g7YmFja2dyb3VuZC1jbGlwOnBhZGRpbmctYm94Oy13ZWJraXQtYm94LXNoYWRvdzowIDFweCAzcHggcmdiYSgwLDAsMCwuMTIpLDAgMXB4IDFweCAxcHggcmdiYSgwLDAsMCwuMTYpOy1tb3otYm94LXNoYWRvdzowIDFweCAzcHggcmdiYSgwLDAsMCwuMTIpLDAgMXB4IDFweCAxcHggcmdiYSgwLDAsMCwuMTYpO2JveC1zaGFkb3c6MCAxcHggM3B4IHJnYmEoMCwwLDAsLjEyKSwwIDFweCAxcHggMXB4IHJnYmEoMCwwLDAsLjE2KX0uZnItYm94LmZyLWJhc2ljLmZyLWJvdHRvbSAuZnItd3JhcHBlcntib3JkZXItYm90dG9tOjA7Ym9yZGVyLXJhZGl1czoycHggMnB4IDAgMDstbW96LWJvcmRlci1yYWRpdXM6MnB4IDJweCAwIDA7LXdlYmtpdC1ib3JkZXItcmFkaXVzOjJweCAycHggMCAwOy1tb3otYmFja2dyb3VuZC1jbGlwOnBhZGRpbmc7LXdlYmtpdC1iYWNrZ3JvdW5kLWNsaXA6cGFkZGluZy1ib3g7YmFja2dyb3VuZC1jbGlwOnBhZGRpbmctYm94Oy13ZWJraXQtYm94LXNoYWRvdzowIC0xcHggM3B4IHJnYmEoMCwwLDAsLjEyKSwwIC0xcHggMXB4IDFweCByZ2JhKDAsMCwwLC4xNik7LW1vei1ib3gtc2hhZG93OjAgLTFweCAzcHggcmdiYSgwLDAsMCwuMTIpLDAgLTFweCAxcHggMXB4IHJnYmEoMCwwLDAsLjE2KTtib3gtc2hhZG93OjAgLTFweCAzcHggcmdiYSgwLDAsMCwuMTIpLDAgLTFweCAxcHggMXB4IHJnYmEoMCwwLDAsLjE2KX1AbWVkaWEgKG1pbi13aWR0aDo5OTJweCl7LmZyLWJveC5mci1kb2N1bWVudHttaW4td2lkdGg6MjFjbX0uZnItYm94LmZyLWRvY3VtZW50IC5mci13cmFwcGVye3RleHQtYWxpZ246bGVmdDtwYWRkaW5nOjMwcHg7bWluLXdpZHRoOjIxY207YmFja2dyb3VuZDojRUZFRkVGfS5mci1ib3guZnItZG9jdW1lbnQgLmZyLXdyYXBwZXIgLmZyLWVsZW1lbnR7dGV4dC1hbGlnbjpsZWZ0O2JhY2tncm91bmQ6I0ZGRjt3aWR0aDoyMWNtO21hcmdpbjphdXRvO21pbi1oZWlnaHQ6MjZjbSFpbXBvcnRhbnQ7cGFkZGluZzoxY20gMmNtOy13ZWJraXQtYm94LXNoYWRvdzowIDFweCAzcHggcmdiYSgwLDAsMCwuMTIpLDAgMXB4IDFweCAxcHggcmdiYSgwLDAsMCwuMTYpOy1tb3otYm94LXNoYWRvdzowIDFweCAzcHggcmdiYSgwLDAsMCwuMTIpLDAgMXB4IDFweCAxcHggcmdiYSgwLDAsMCwuMTYpO2JveC1zaGFkb3c6MCAxcHggM3B4IHJnYmEoMCwwLDAsLjEyKSwwIDFweCAxcHggMXB4IHJnYmEoMCwwLDAsLjE2KTtvdmVyZmxvdzp2aXNpYmxlO3otaW5kZXg6YXV0b30uZnItYm94LmZyLWRvY3VtZW50IC5mci13cmFwcGVyIC5mci1lbGVtZW50IGhye21hcmdpbi1sZWZ0Oi0yY207bWFyZ2luLXJpZ2h0Oi0yY207YmFja2dyb3VuZDojRUZFRkVGO2hlaWdodDoxY207b3V0bGluZTowO2JvcmRlcjowfS5mci1ib3guZnItZG9jdW1lbnQgLmZyLXdyYXBwZXIgLmZyLWVsZW1lbnQgaW1ne3otaW5kZXg6MX19LmZyLXRvb2x0aXB7cG9zaXRpb246YWJzb2x1dGU7dG9wOjA7bGVmdDowO3BhZGRpbmc6MCA4cHg7Ym9yZGVyLXJhZGl1czoycHg7LW1vei1ib3JkZXItcmFkaXVzOjJweDstd2Via2l0LWJvcmRlci1yYWRpdXM6MnB4Oy1tb3otYmFja2dyb3VuZC1jbGlwOnBhZGRpbmc7LXdlYmtpdC1iYWNrZ3JvdW5kLWNsaXA6cGFkZGluZy1ib3g7YmFja2dyb3VuZC1jbGlwOnBhZGRpbmctYm94Oy13ZWJraXQtYm94LXNoYWRvdzowIDNweCA2cHggcmdiYSgwLDAsMCwuMTYpLDAgMnB4IDJweCAxcHggcmdiYSgwLDAsMCwuMTQpOy1tb3otYm94LXNoYWRvdzowIDNweCA2cHggcmdiYSgwLDAsMCwuMTYpLDAgMnB4IDJweCAxcHggcmdiYSgwLDAsMCwuMTQpO2JveC1zaGFkb3c6MCAzcHggNnB4IHJnYmEoMCwwLDAsLjE2KSwwIDJweCAycHggMXB4IHJnYmEoMCwwLDAsLjE0KTtiYWNrZ3JvdW5kOiMyMjI7Y29sb3I6I2ZmZjtmb250LXNpemU6MTFweDtsaW5lLWhlaWdodDoyMnB4O2ZvbnQtZmFtaWx5OkFyaWFsLEhlbHZldGljYSxzYW5zLXNlcmlmOy13ZWJraXQtdHJhbnNpdGlvbjpvcGFjaXR5IC4ycyBlYXNlIDBzOy1tb3otdHJhbnNpdGlvbjpvcGFjaXR5IC4ycyBlYXNlIDBzOy1tcy10cmFuc2l0aW9uOm9wYWNpdHkgLjJzIGVhc2UgMHM7LW8tdHJhbnNpdGlvbjpvcGFjaXR5IC4ycyBlYXNlIDBzOy13ZWJraXQtb3BhY2l0eTowOy1tb3otb3BhY2l0eTowO29wYWNpdHk6MDstbXMtZmlsdGVyOlwiYWxwaGEoT3BhY2l0eT0wKVwiO2xlZnQ6LTMwMDBweDt1c2VyLXNlbGVjdDpub25lOy1vLXVzZXItc2VsZWN0Om5vbmU7LW1vei11c2VyLXNlbGVjdDpub25lOy1raHRtbC11c2VyLXNlbGVjdDpub25lOy13ZWJraXQtdXNlci1zZWxlY3Q6bm9uZTstbXMtdXNlci1zZWxlY3Q6bm9uZTt6LWluZGV4OjIxNDc0ODM2NDc7dGV4dC1yZW5kZXJpbmc6b3B0aW1pemVsZWdpYmlsaXR5Oy13ZWJraXQtZm9udC1zbW9vdGhpbmc6YW50aWFsaWFzZWQ7LW1vei1vc3gtZm9udC1zbW9vdGhpbmc6Z3JheXNjYWxlfS5mci10b29sdGlwLmZyLXZpc2libGV7LXdlYmtpdC1vcGFjaXR5OjE7LW1vei1vcGFjaXR5OjE7b3BhY2l0eToxOy1tcy1maWx0ZXI6XCJhbHBoYShPcGFjaXR5PTApXCJ9LmZyLXRvb2xiYXIgLmZyLWJ0bi13cmFwLC5mci1wb3B1cCAuZnItYnRuLXdyYXB7ZmxvYXQ6bGVmdDt3aGl0ZS1zcGFjZTpub3dyYXA7cG9zaXRpb246cmVsYXRpdmV9LmZyLXRvb2xiYXIgLmZyLWJ0bi13cmFwLmZyLWhpZGRlbiwuZnItcG9wdXAgLmZyLWJ0bi13cmFwLmZyLWhpZGRlbntkaXNwbGF5Om5vbmV9LmZyLXRvb2xiYXIgLmZyLWNvbW1hbmQuZnItYnRuLC5mci1wb3B1cCAuZnItY29tbWFuZC5mci1idG57YmFja2dyb3VuZDowIDA7Y29sb3I6IzIyMjstbW96LW91dGxpbmU6MDtvdXRsaW5lOjA7Ym9yZGVyOjA7bGluZS1oZWlnaHQ6MTtjdXJzb3I6cG9pbnRlcjt0ZXh0LWFsaWduOmxlZnQ7bWFyZ2luOjAgMnB4Oy13ZWJraXQtdHJhbnNpdGlvbjpiYWNrZ3JvdW5kIC4ycyBlYXNlIDBzOy1tb3otdHJhbnNpdGlvbjpiYWNrZ3JvdW5kIC4ycyBlYXNlIDBzOy1tcy10cmFuc2l0aW9uOmJhY2tncm91bmQgLjJzIGVhc2UgMHM7LW8tdHJhbnNpdGlvbjpiYWNrZ3JvdW5kIC4ycyBlYXNlIDBzO2JvcmRlci1yYWRpdXM6MDstbW96LWJvcmRlci1yYWRpdXM6MDstd2Via2l0LWJvcmRlci1yYWRpdXM6MDstbW96LWJhY2tncm91bmQtY2xpcDpwYWRkaW5nOy13ZWJraXQtYmFja2dyb3VuZC1jbGlwOnBhZGRpbmctYm94O2JhY2tncm91bmQtY2xpcDpwYWRkaW5nLWJveDt6LWluZGV4OjI7cG9zaXRpb246cmVsYXRpdmU7LXdlYmtpdC1ib3gtc2l6aW5nOmJvcmRlci1ib3g7LW1vei1ib3gtc2l6aW5nOmJvcmRlci1ib3g7Ym94LXNpemluZzpib3JkZXItYm94O3RleHQtZGVjb3JhdGlvbjpub25lO3VzZXItc2VsZWN0Om5vbmU7LW8tdXNlci1zZWxlY3Q6bm9uZTstbW96LXVzZXItc2VsZWN0Om5vbmU7LWtodG1sLXVzZXItc2VsZWN0Om5vbmU7LXdlYmtpdC11c2VyLXNlbGVjdDpub25lOy1tcy11c2VyLXNlbGVjdDpub25lO2Zsb2F0OmxlZnQ7cGFkZGluZzowO3dpZHRoOjM4cHg7aGVpZ2h0OjM4cHh9LmZyLXRvb2xiYXIgLmZyLWNvbW1hbmQuZnItYnRuOjotbW96LWZvY3VzLWlubmVyLC5mci1wb3B1cCAuZnItY29tbWFuZC5mci1idG46Oi1tb3otZm9jdXMtaW5uZXJ7Ym9yZGVyOjA7cGFkZGluZzowfS5mci10b29sYmFyIC5mci1jb21tYW5kLmZyLWJ0bi5mci1idG4tdGV4dCwuZnItcG9wdXAgLmZyLWNvbW1hbmQuZnItYnRuLmZyLWJ0bi10ZXh0e3dpZHRoOmF1dG99LmZyLXRvb2xiYXIgLmZyLWNvbW1hbmQuZnItYnRuIGksLmZyLXBvcHVwIC5mci1jb21tYW5kLmZyLWJ0biBpLC5mci10b29sYmFyIC5mci1jb21tYW5kLmZyLWJ0biBzdmcsLmZyLXBvcHVwIC5mci1jb21tYW5kLmZyLWJ0biBzdmd7ZGlzcGxheTpibG9jaztmb250LXNpemU6MTRweDt3aWR0aDoxNHB4O21hcmdpbjoxMnB4O3RleHQtYWxpZ246Y2VudGVyO2Zsb2F0Om5vbmV9LmZyLXRvb2xiYXIgLmZyLWNvbW1hbmQuZnItYnRuIHNwYW4uZnItc3Itb25seSwuZnItcG9wdXAgLmZyLWNvbW1hbmQuZnItYnRuIHNwYW4uZnItc3Itb25seXtwb3NpdGlvbjphYnNvbHV0ZTt3aWR0aDoxcHg7aGVpZ2h0OjFweDtwYWRkaW5nOjA7bWFyZ2luOi0xcHg7b3ZlcmZsb3c6aGlkZGVuO2NsaXA6cmVjdCgwLDAsMCwwKTtib3JkZXI6MH0uZnItdG9vbGJhciAuZnItY29tbWFuZC5mci1idG4gc3BhbiwuZnItcG9wdXAgLmZyLWNvbW1hbmQuZnItYnRuIHNwYW57Zm9udC1zaXplOjE0cHg7ZGlzcGxheTpibG9jaztsaW5lLWhlaWdodDoxN3B4O21pbi13aWR0aDozNHB4O2Zsb2F0OmxlZnQ7dGV4dC1vdmVyZmxvdzplbGxpcHNpcztvdmVyZmxvdzpoaWRkZW47d2hpdGUtc3BhY2U6bm93cmFwO2hlaWdodDoxN3B4O2ZvbnQtd2VpZ2h0OjcwMDtwYWRkaW5nOjAgMnB4fS5mci10b29sYmFyIC5mci1jb21tYW5kLmZyLWJ0biBpbWcsLmZyLXBvcHVwIC5mci1jb21tYW5kLmZyLWJ0biBpbWd7bWFyZ2luOjEycHg7d2lkdGg6MTRweH0uZnItdG9vbGJhciAuZnItY29tbWFuZC5mci1idG4uZnItYWN0aXZlLC5mci1wb3B1cCAuZnItY29tbWFuZC5mci1idG4uZnItYWN0aXZle2NvbG9yOiMxZTg4ZTU7YmFja2dyb3VuZDowIDB9LmZyLXRvb2xiYXIgLmZyLWNvbW1hbmQuZnItYnRuLmZyLWRyb3Bkb3duLmZyLXNlbGVjdGlvbiwuZnItcG9wdXAgLmZyLWNvbW1hbmQuZnItYnRuLmZyLWRyb3Bkb3duLmZyLXNlbGVjdGlvbnt3aWR0aDphdXRvfS5mci10b29sYmFyIC5mci1jb21tYW5kLmZyLWJ0bi5mci1kcm9wZG93bi5mci1zZWxlY3Rpb24gc3BhbiwuZnItcG9wdXAgLmZyLWNvbW1hbmQuZnItYnRuLmZyLWRyb3Bkb3duLmZyLXNlbGVjdGlvbiBzcGFue2ZvbnQtd2VpZ2h0OjQwMH0uZnItdG9vbGJhciAuZnItY29tbWFuZC5mci1idG4uZnItZHJvcGRvd24gaSwuZnItcG9wdXAgLmZyLWNvbW1hbmQuZnItYnRuLmZyLWRyb3Bkb3duIGksLmZyLXRvb2xiYXIgLmZyLWNvbW1hbmQuZnItYnRuLmZyLWRyb3Bkb3duIHNwYW4sLmZyLXBvcHVwIC5mci1jb21tYW5kLmZyLWJ0bi5mci1kcm9wZG93biBzcGFuLC5mci10b29sYmFyIC5mci1jb21tYW5kLmZyLWJ0bi5mci1kcm9wZG93biBpbWcsLmZyLXBvcHVwIC5mci1jb21tYW5kLmZyLWJ0bi5mci1kcm9wZG93biBpbWcsLmZyLXRvb2xiYXIgLmZyLWNvbW1hbmQuZnItYnRuLmZyLWRyb3Bkb3duIHN2ZywuZnItcG9wdXAgLmZyLWNvbW1hbmQuZnItYnRuLmZyLWRyb3Bkb3duIHN2Z3ttYXJnaW4tbGVmdDo4cHg7bWFyZ2luLXJpZ2h0OjE2cHh9LmZyLXRvb2xiYXIgLmZyLWNvbW1hbmQuZnItYnRuLmZyLWRyb3Bkb3duLmZyLWFjdGl2ZSwuZnItcG9wdXAgLmZyLWNvbW1hbmQuZnItYnRuLmZyLWRyb3Bkb3duLmZyLWFjdGl2ZXtjb2xvcjojMjIyO2JhY2tncm91bmQ6I2Q2ZDZkNn0uZnItdG9vbGJhciAuZnItY29tbWFuZC5mci1idG4uZnItZHJvcGRvd24uZnItYWN0aXZlOmhvdmVyLC5mci1wb3B1cCAuZnItY29tbWFuZC5mci1idG4uZnItZHJvcGRvd24uZnItYWN0aXZlOmhvdmVyLC5mci10b29sYmFyIC5mci1jb21tYW5kLmZyLWJ0bi5mci1kcm9wZG93bi5mci1hY3RpdmU6Zm9jdXMsLmZyLXBvcHVwIC5mci1jb21tYW5kLmZyLWJ0bi5mci1kcm9wZG93bi5mci1hY3RpdmU6Zm9jdXN7YmFja2dyb3VuZDojZDZkNmQ2IWltcG9ydGFudDtjb2xvcjojMjIyIWltcG9ydGFudH0uZnItdG9vbGJhciAuZnItY29tbWFuZC5mci1idG4uZnItZHJvcGRvd24uZnItYWN0aXZlOmhvdmVyOjphZnRlciwuZnItcG9wdXAgLmZyLWNvbW1hbmQuZnItYnRuLmZyLWRyb3Bkb3duLmZyLWFjdGl2ZTpob3Zlcjo6YWZ0ZXIsLmZyLXRvb2xiYXIgLmZyLWNvbW1hbmQuZnItYnRuLmZyLWRyb3Bkb3duLmZyLWFjdGl2ZTpmb2N1czo6YWZ0ZXIsLmZyLXBvcHVwIC5mci1jb21tYW5kLmZyLWJ0bi5mci1kcm9wZG93bi5mci1hY3RpdmU6Zm9jdXM6OmFmdGVye2JvcmRlci10b3AtY29sb3I6IzIyMiFpbXBvcnRhbnR9LmZyLXRvb2xiYXIgLmZyLWNvbW1hbmQuZnItYnRuLmZyLWRyb3Bkb3duOjphZnRlciwuZnItcG9wdXAgLmZyLWNvbW1hbmQuZnItYnRuLmZyLWRyb3Bkb3duOjphZnRlcntwb3NpdGlvbjphYnNvbHV0ZTt3aWR0aDowO2hlaWdodDowO2JvcmRlci1sZWZ0OjRweCBzb2xpZCB0cmFuc3BhcmVudDtib3JkZXItcmlnaHQ6NHB4IHNvbGlkIHRyYW5zcGFyZW50O2JvcmRlci10b3A6NHB4IHNvbGlkICMyMjI7cmlnaHQ6NHB4O3RvcDoxN3B4O2NvbnRlbnQ6XCJcIn0uZnItdG9vbGJhciAuZnItY29tbWFuZC5mci1idG4uZnItZGlzYWJsZWQsLmZyLXBvcHVwIC5mci1jb21tYW5kLmZyLWJ0bi5mci1kaXNhYmxlZHtjb2xvcjojYmRiZGJkO2N1cnNvcjpkZWZhdWx0fS5mci10b29sYmFyIC5mci1jb21tYW5kLmZyLWJ0bi5mci1kaXNhYmxlZDo6YWZ0ZXIsLmZyLXBvcHVwIC5mci1jb21tYW5kLmZyLWJ0bi5mci1kaXNhYmxlZDo6YWZ0ZXJ7Ym9yZGVyLXRvcC1jb2xvcjojYmRiZGJkIWltcG9ydGFudH0uZnItdG9vbGJhciAuZnItY29tbWFuZC5mci1idG4uZnItaGlkZGVuLC5mci1wb3B1cCAuZnItY29tbWFuZC5mci1idG4uZnItaGlkZGVue2Rpc3BsYXk6bm9uZX0uZnItdG9vbGJhci5mci1kaXNhYmxlZCAuZnItYnRuLC5mci1wb3B1cC5mci1kaXNhYmxlZCAuZnItYnRuLC5mci10b29sYmFyLmZyLWRpc2FibGVkIC5mci1idG4uZnItYWN0aXZlLC5mci1wb3B1cC5mci1kaXNhYmxlZCAuZnItYnRuLmZyLWFjdGl2ZXtjb2xvcjojYmRiZGJkfS5mci10b29sYmFyLmZyLWRpc2FibGVkIC5mci1idG4uZnItZHJvcGRvd246OmFmdGVyLC5mci1wb3B1cC5mci1kaXNhYmxlZCAuZnItYnRuLmZyLWRyb3Bkb3duOjphZnRlciwuZnItdG9vbGJhci5mci1kaXNhYmxlZCAuZnItYnRuLmZyLWFjdGl2ZS5mci1kcm9wZG93bjo6YWZ0ZXIsLmZyLXBvcHVwLmZyLWRpc2FibGVkIC5mci1idG4uZnItYWN0aXZlLmZyLWRyb3Bkb3duOjphZnRlcntib3JkZXItdG9wLWNvbG9yOiNiZGJkYmR9LmZyLXRvb2xiYXIuZnItcnRsIC5mci1jb21tYW5kLmZyLWJ0biwuZnItcG9wdXAuZnItcnRsIC5mci1jb21tYW5kLmZyLWJ0biwuZnItdG9vbGJhci5mci1ydGwgLmZyLWJ0bi13cmFwLC5mci1wb3B1cC5mci1ydGwgLmZyLWJ0bi13cmFwe2Zsb2F0OnJpZ2h0fS5mci10b29sYmFyLmZyLWlubGluZT4uZnItY29tbWFuZC5mci1idG46bm90KC5mci1oaWRkZW4pLC5mci10b29sYmFyLmZyLWlubGluZT4uZnItYnRuLXdyYXA6bm90KC5mci1oaWRkZW4pe2Rpc3BsYXk6LXdlYmtpdC1pbmxpbmUtZmxleDtkaXNwbGF5Oi1tcy1pbmxpbmUtZmxleGJveDtkaXNwbGF5OmlubGluZS1mbGV4O2Zsb2F0Om5vbmV9LmZyLWRlc2t0b3AgLmZyLWNvbW1hbmQ6aG92ZXIsLmZyLWRlc2t0b3AgLmZyLWNvbW1hbmQ6Zm9jdXMsLmZyLWRlc2t0b3AgLmZyLWNvbW1hbmQuZnItYnRuLWhvdmVyLC5mci1kZXNrdG9wIC5mci1jb21tYW5kLmZyLWV4cGFuZGVke291dGxpbmU6MDtjb2xvcjojMjIyO2JhY2tncm91bmQ6I2ViZWJlYn0uZnItZGVza3RvcCAuZnItY29tbWFuZDpob3Zlcjo6YWZ0ZXIsLmZyLWRlc2t0b3AgLmZyLWNvbW1hbmQ6Zm9jdXM6OmFmdGVyLC5mci1kZXNrdG9wIC5mci1jb21tYW5kLmZyLWJ0bi1ob3Zlcjo6YWZ0ZXIsLmZyLWRlc2t0b3AgLmZyLWNvbW1hbmQuZnItZXhwYW5kZWQ6OmFmdGVye2JvcmRlci10b3AtY29sb3I6IzIyMiFpbXBvcnRhbnR9LmZyLWRlc2t0b3AgLmZyLWNvbW1hbmQuZnItc2VsZWN0ZWR7Y29sb3I6IzIyMjtiYWNrZ3JvdW5kOiNkNmQ2ZDZ9LmZyLWRlc2t0b3AgLmZyLWNvbW1hbmQuZnItYWN0aXZlOmhvdmVyLC5mci1kZXNrdG9wIC5mci1jb21tYW5kLmZyLWFjdGl2ZTpmb2N1cywuZnItZGVza3RvcCAuZnItY29tbWFuZC5mci1hY3RpdmUuZnItYnRuLWhvdmVyLC5mci1kZXNrdG9wIC5mci1jb21tYW5kLmZyLWFjdGl2ZS5mci1leHBhbmRlZHtjb2xvcjojMWU4OGU1O2JhY2tncm91bmQ6I2ViZWJlYn0uZnItZGVza3RvcCAuZnItY29tbWFuZC5mci1hY3RpdmUuZnItc2VsZWN0ZWR7Y29sb3I6IzFlODhlNTtiYWNrZ3JvdW5kOiNkNmQ2ZDZ9LmZyLWRlc2t0b3AgLmZyLWNvbW1hbmQuZnItZGlzYWJsZWQ6aG92ZXIsLmZyLWRlc2t0b3AgLmZyLWNvbW1hbmQuZnItZGlzYWJsZWQ6Zm9jdXMsLmZyLWRlc2t0b3AgLmZyLWNvbW1hbmQuZnItZGlzYWJsZWQuZnItc2VsZWN0ZWR7YmFja2dyb3VuZDowIDB9LmZyLWRlc2t0b3AuZnItZGlzYWJsZWQgLmZyLWNvbW1hbmQ6aG92ZXIsLmZyLWRlc2t0b3AuZnItZGlzYWJsZWQgLmZyLWNvbW1hbmQ6Zm9jdXMsLmZyLWRlc2t0b3AuZnItZGlzYWJsZWQgLmZyLWNvbW1hbmQuZnItc2VsZWN0ZWR7YmFja2dyb3VuZDowIDB9LmZyLXRvb2xiYXIuZnItbW9iaWxlIC5mci1jb21tYW5kLmZyLWJsaW5rLC5mci1wb3B1cC5mci1tb2JpbGUgLmZyLWNvbW1hbmQuZnItYmxpbmt7YmFja2dyb3VuZDowIDB9LmZyLWNvbW1hbmQuZnItYnRuLmZyLW9wdGlvbnN7d2lkdGg6MTZweDttYXJnaW4tbGVmdDotNXB4fS5mci1jb21tYW5kLmZyLWJ0bi5mci1vcHRpb25zLmZyLWJ0bi1ob3ZlciwuZnItY29tbWFuZC5mci1idG4uZnItb3B0aW9uczpob3ZlciwuZnItY29tbWFuZC5mci1idG4uZnItb3B0aW9uczpmb2N1c3tib3JkZXItbGVmdDpzb2xpZCAxcHggI2ZhZmFmYX0uZnItY29tbWFuZC5mci1idG4rLmZyLWRyb3Bkb3duLW1lbnV7ZGlzcGxheTppbmxpbmUtYmxvY2s7cG9zaXRpb246YWJzb2x1dGU7cmlnaHQ6YXV0bztib3R0b206YXV0bztoZWlnaHQ6YXV0bzt6LWluZGV4OjQ7LXdlYmtpdC1vdmVyZmxvdy1zY3JvbGxpbmc6dG91Y2g7b3ZlcmZsb3c6aGlkZGVuO3pvb206MTtib3JkZXItcmFkaXVzOjAgMCAycHggMnB4Oy1tb3otYm9yZGVyLXJhZGl1czowIDAgMnB4IDJweDstd2Via2l0LWJvcmRlci1yYWRpdXM6MCAwIDJweCAycHg7LW1vei1iYWNrZ3JvdW5kLWNsaXA6cGFkZGluZzstd2Via2l0LWJhY2tncm91bmQtY2xpcDpwYWRkaW5nLWJveDtiYWNrZ3JvdW5kLWNsaXA6cGFkZGluZy1ib3h9LmZyLWNvbW1hbmQuZnItYnRuKy5mci1kcm9wZG93bi1tZW51LnRlc3QtaGVpZ2h0IC5mci1kcm9wZG93bi13cmFwcGVyey13ZWJraXQtdHJhbnNpdGlvbjpub25lOy1tb3otdHJhbnNpdGlvbjpub25lOy1tcy10cmFuc2l0aW9uOm5vbmU7LW8tdHJhbnNpdGlvbjpub25lO2hlaWdodDphdXRvO21heC1oZWlnaHQ6Mjc1cHh9LmZyLWNvbW1hbmQuZnItYnRuKy5mci1kcm9wZG93bi1tZW51IC5mci1kcm9wZG93bi13cmFwcGVye2JhY2tncm91bmQ6I2ZmZjtwYWRkaW5nOjA7bWFyZ2luOmF1dG87ZGlzcGxheTppbmxpbmUtYmxvY2s7dGV4dC1hbGlnbjpsZWZ0O3Bvc2l0aW9uOnJlbGF0aXZlOy13ZWJraXQtYm94LXNpemluZzpib3JkZXItYm94Oy1tb3otYm94LXNpemluZzpib3JkZXItYm94O2JveC1zaXppbmc6Ym9yZGVyLWJveDstd2Via2l0LXRyYW5zaXRpb246bWF4LWhlaWdodCAuMnMgZWFzZSAwczstbW96LXRyYW5zaXRpb246bWF4LWhlaWdodCAuMnMgZWFzZSAwczstbXMtdHJhbnNpdGlvbjptYXgtaGVpZ2h0IC4ycyBlYXNlIDBzOy1vLXRyYW5zaXRpb246bWF4LWhlaWdodCAuMnMgZWFzZSAwczttYXJnaW4tdG9wOjA7ZmxvYXQ6bGVmdDttYXgtaGVpZ2h0OjA7aGVpZ2h0OjA7bWFyZ2luLXRvcDowIWltcG9ydGFudH0uZnItY29tbWFuZC5mci1idG4rLmZyLWRyb3Bkb3duLW1lbnUgLmZyLWRyb3Bkb3duLXdyYXBwZXIgLmZyLWRyb3Bkb3duLWNvbnRlbnR7b3ZlcmZsb3c6YXV0bztwb3NpdGlvbjpyZWxhdGl2ZTttYXgtaGVpZ2h0OjI3NXB4fS5mci1jb21tYW5kLmZyLWJ0bisuZnItZHJvcGRvd24tbWVudSAuZnItZHJvcGRvd24td3JhcHBlciAuZnItZHJvcGRvd24tY29udGVudCB1bC5mci1kcm9wZG93bi1saXN0e2xpc3Qtc3R5bGUtdHlwZTpub25lO21hcmdpbjowO3BhZGRpbmc6MH0uZnItY29tbWFuZC5mci1idG4rLmZyLWRyb3Bkb3duLW1lbnUgLmZyLWRyb3Bkb3duLXdyYXBwZXIgLmZyLWRyb3Bkb3duLWNvbnRlbnQgdWwuZnItZHJvcGRvd24tbGlzdCBsaXtwYWRkaW5nOjA7bWFyZ2luOjA7Zm9udC1zaXplOjE1cHh9LmZyLWNvbW1hbmQuZnItYnRuKy5mci1kcm9wZG93bi1tZW51IC5mci1kcm9wZG93bi13cmFwcGVyIC5mci1kcm9wZG93bi1jb250ZW50IHVsLmZyLWRyb3Bkb3duLWxpc3QgbGkgYXtwYWRkaW5nOjAgMjRweDtsaW5lLWhlaWdodDoyMDAlO2Rpc3BsYXk6YmxvY2s7Y3Vyc29yOnBvaW50ZXI7d2hpdGUtc3BhY2U6bm93cmFwO2NvbG9yOmluaGVyaXQ7dGV4dC1kZWNvcmF0aW9uOm5vbmV9LmZyLWNvbW1hbmQuZnItYnRuKy5mci1kcm9wZG93bi1tZW51IC5mci1kcm9wZG93bi13cmFwcGVyIC5mci1kcm9wZG93bi1jb250ZW50IHVsLmZyLWRyb3Bkb3duLWxpc3QgbGkgYS5mci1hY3RpdmV7YmFja2dyb3VuZDojZDZkNmQ2fS5mci1jb21tYW5kLmZyLWJ0bisuZnItZHJvcGRvd24tbWVudSAuZnItZHJvcGRvd24td3JhcHBlciAuZnItZHJvcGRvd24tY29udGVudCB1bC5mci1kcm9wZG93bi1saXN0IGxpIGEuZnItZGlzYWJsZWR7Y29sb3I6I2JkYmRiZDtjdXJzb3I6ZGVmYXVsdH0uZnItY29tbWFuZC5mci1idG4rLmZyLWRyb3Bkb3duLW1lbnUgLmZyLWRyb3Bkb3duLXdyYXBwZXIgLmZyLWRyb3Bkb3duLWNvbnRlbnQgdWwuZnItZHJvcGRvd24tbGlzdCBsaSBhIC5mci1zaG9ydGN1dHtmbG9hdDpyaWdodDttYXJnaW4tbGVmdDozMnB4O2ZvbnQtd2VpZ2h0OjcwMDstd2Via2l0LW9wYWNpdHk6Ljc1Oy1tb3otb3BhY2l0eTouNzU7b3BhY2l0eTouNzU7LW1zLWZpbHRlcjpcImFscGhhKE9wYWNpdHk9MClcIn0uZnItY29tbWFuZC5mci1idG46bm90KC5mci1hY3RpdmUpKy5mci1kcm9wZG93bi1tZW51e2xlZnQ6LTMwMDBweCFpbXBvcnRhbnR9LmZyLWNvbW1hbmQuZnItYnRuLmZyLWFjdGl2ZSsuZnItZHJvcGRvd24tbWVudXtkaXNwbGF5OmlubGluZS1ibG9jazstd2Via2l0LWJveC1zaGFkb3c6MCAzcHggNnB4IHJnYmEoMCwwLDAsLjE2KSwwIDJweCAycHggMXB4IHJnYmEoMCwwLDAsLjE0KTstbW96LWJveC1zaGFkb3c6MCAzcHggNnB4IHJnYmEoMCwwLDAsLjE2KSwwIDJweCAycHggMXB4IHJnYmEoMCwwLDAsLjE0KTtib3gtc2hhZG93OjAgM3B4IDZweCByZ2JhKDAsMCwwLC4xNiksMCAycHggMnB4IDFweCByZ2JhKDAsMCwwLC4xNCl9LmZyLWNvbW1hbmQuZnItYnRuLmZyLWFjdGl2ZSsuZnItZHJvcGRvd24tbWVudSAuZnItZHJvcGRvd24td3JhcHBlcntoZWlnaHQ6YXV0bzttYXgtaGVpZ2h0OjI3NXB4fS5mci1ib3R0b20+LmZyLWNvbW1hbmQuZnItYnRuKy5mci1kcm9wZG93bi1tZW51e2JvcmRlci1yYWRpdXM6MnB4IDJweCAwIDA7LW1vei1ib3JkZXItcmFkaXVzOjJweCAycHggMCAwOy13ZWJraXQtYm9yZGVyLXJhZGl1czoycHggMnB4IDAgMDstbW96LWJhY2tncm91bmQtY2xpcDpwYWRkaW5nOy13ZWJraXQtYmFja2dyb3VuZC1jbGlwOnBhZGRpbmctYm94O2JhY2tncm91bmQtY2xpcDpwYWRkaW5nLWJveH0uZnItdG9vbGJhci5mci1ydGwgLmZyLWRyb3Bkb3duLXdyYXBwZXIsLmZyLXBvcHVwLmZyLXJ0bCAuZnItZHJvcGRvd24td3JhcHBlcnt0ZXh0LWFsaWduOnJpZ2h0IWltcG9ydGFudH1ib2R5LnByZXZlbnQtc2Nyb2xse292ZXJmbG93OmhpZGRlbn1ib2R5LnByZXZlbnQtc2Nyb2xsLmZyLW1vYmlsZXtwb3NpdGlvbjpmaXhlZDstd2Via2l0LW92ZXJmbG93LXNjcm9sbGluZzp0b3VjaH0uZnItbW9kYWx7Y29sb3I6IzIyMjtmb250LWZhbWlseTpBcmlhbCxIZWx2ZXRpY2Esc2Fucy1zZXJpZjtwb3NpdGlvbjpmaXhlZDtvdmVyZmxvdy14OmF1dG87b3ZlcmZsb3cteTpzY3JvbGw7dG9wOjA7bGVmdDowO2JvdHRvbTowO3JpZ2h0OjA7d2lkdGg6MTAwJTt6LWluZGV4OjIxNDc0ODM2NDA7dGV4dC1yZW5kZXJpbmc6b3B0aW1pemVsZWdpYmlsaXR5Oy13ZWJraXQtZm9udC1zbW9vdGhpbmc6YW50aWFsaWFzZWQ7LW1vei1vc3gtZm9udC1zbW9vdGhpbmc6Z3JheXNjYWxlO3RleHQtYWxpZ246Y2VudGVyO2xpbmUtaGVpZ2h0OjEuMn0uZnItbW9kYWwuZnItbWlkZGxlIC5mci1tb2RhbC13cmFwcGVye21hcmdpbi10b3A6MDttYXJnaW4tYm90dG9tOjA7bWFyZ2luLWxlZnQ6YXV0bzttYXJnaW4tcmlnaHQ6YXV0bzt0b3A6NTAlO2xlZnQ6NTAlOy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZSgtNTAlLC01MCUpOy1tb3otdHJhbnNmb3JtOnRyYW5zbGF0ZSgtNTAlLC01MCUpOy1tcy10cmFuc2Zvcm06dHJhbnNsYXRlKC01MCUsLTUwJSk7LW8tdHJhbnNmb3JtOnRyYW5zbGF0ZSgtNTAlLC01MCUpO3Bvc2l0aW9uOmFic29sdXRlfS5mci1tb2RhbCAuZnItbW9kYWwtd3JhcHBlcntib3JkZXItcmFkaXVzOjJweDstbW96LWJvcmRlci1yYWRpdXM6MnB4Oy13ZWJraXQtYm9yZGVyLXJhZGl1czoycHg7LW1vei1iYWNrZ3JvdW5kLWNsaXA6cGFkZGluZzstd2Via2l0LWJhY2tncm91bmQtY2xpcDpwYWRkaW5nLWJveDtiYWNrZ3JvdW5kLWNsaXA6cGFkZGluZy1ib3g7bWFyZ2luOjIwcHggYXV0bztkaXNwbGF5OmlubGluZS1ibG9jaztiYWNrZ3JvdW5kOiNmZmY7bWluLXdpZHRoOjMwMHB4Oy13ZWJraXQtYm94LXNoYWRvdzowIDVweCA4cHggcmdiYSgwLDAsMCwuMTkpLDAgNHB4IDNweCAxcHggcmdiYSgwLDAsMCwuMTQpOy1tb3otYm94LXNoYWRvdzowIDVweCA4cHggcmdiYSgwLDAsMCwuMTkpLDAgNHB4IDNweCAxcHggcmdiYSgwLDAsMCwuMTQpO2JveC1zaGFkb3c6MCA1cHggOHB4IHJnYmEoMCwwLDAsLjE5KSwwIDRweCAzcHggMXB4IHJnYmEoMCwwLDAsLjE0KTtib3JkZXI6MDtib3JkZXItdG9wOjVweCBzb2xpZCAjMjIyO292ZXJmbG93OmhpZGRlbjt3aWR0aDo5MCU7cG9zaXRpb246cmVsYXRpdmV9QG1lZGlhIChtaW4td2lkdGg6NzY4cHgpIGFuZCAobWF4LXdpZHRoOjk5MXB4KXsuZnItbW9kYWwgLmZyLW1vZGFsLXdyYXBwZXJ7bWFyZ2luOjMwcHggYXV0bzt3aWR0aDo3MCV9fUBtZWRpYSAobWluLXdpZHRoOjk5MnB4KXsuZnItbW9kYWwgLmZyLW1vZGFsLXdyYXBwZXJ7bWFyZ2luOjUwcHggYXV0bzt3aWR0aDo5NjBweH19LmZyLW1vZGFsIC5mci1tb2RhbC13cmFwcGVyIC5mci1tb2RhbC1oZWFke2JhY2tncm91bmQ6I2ZmZjstd2Via2l0LWJveC1zaGFkb3c6MCAzcHggNnB4IHJnYmEoMCwwLDAsLjE2KSwwIDJweCAycHggMXB4IHJnYmEoMCwwLDAsLjE0KTstbW96LWJveC1zaGFkb3c6MCAzcHggNnB4IHJnYmEoMCwwLDAsLjE2KSwwIDJweCAycHggMXB4IHJnYmEoMCwwLDAsLjE0KTtib3gtc2hhZG93OjAgM3B4IDZweCByZ2JhKDAsMCwwLC4xNiksMCAycHggMnB4IDFweCByZ2JhKDAsMCwwLC4xNCk7Ym9yZGVyLWJvdHRvbTowO292ZXJmbG93OmhpZGRlbjtwb3NpdGlvbjphYnNvbHV0ZTt3aWR0aDoxMDAlO21pbi1oZWlnaHQ6NDJweDt6LWluZGV4OjM7LXdlYmtpdC10cmFuc2l0aW9uOmhlaWdodCAuMnMgZWFzZSAwczstbW96LXRyYW5zaXRpb246aGVpZ2h0IC4ycyBlYXNlIDBzOy1tcy10cmFuc2l0aW9uOmhlaWdodCAuMnMgZWFzZSAwczstby10cmFuc2l0aW9uOmhlaWdodCAuMnMgZWFzZSAwc30uZnItbW9kYWwgLmZyLW1vZGFsLXdyYXBwZXIgLmZyLW1vZGFsLWhlYWQgLmZyLW1vZGFsLWNsb3Nle3BhZGRpbmc6MTJweDt3aWR0aDoyMHB4O2ZvbnQtc2l6ZTozMHB4O2N1cnNvcjpwb2ludGVyO2xpbmUtaGVpZ2h0OjE4cHg7Y29sb3I6IzIyMjstd2Via2l0LWJveC1zaXppbmc6Y29udGVudC1ib3g7LW1vei1ib3gtc2l6aW5nOmNvbnRlbnQtYm94O2JveC1zaXppbmc6Y29udGVudC1ib3g7cG9zaXRpb246YWJzb2x1dGU7dG9wOjA7cmlnaHQ6MDstd2Via2l0LXRyYW5zaXRpb246Y29sb3IgLjJzIGVhc2UgMHM7LW1vei10cmFuc2l0aW9uOmNvbG9yIC4ycyBlYXNlIDBzOy1tcy10cmFuc2l0aW9uOmNvbG9yIC4ycyBlYXNlIDBzOy1vLXRyYW5zaXRpb246Y29sb3IgLjJzIGVhc2UgMHN9LmZyLW1vZGFsIC5mci1tb2RhbC13cmFwcGVyIC5mci1tb2RhbC1oZWFkIGg0e2ZvbnQtc2l6ZToxOHB4O3BhZGRpbmc6MTJweCAxMHB4O21hcmdpbjowO2ZvbnQtd2VpZ2h0OjQwMDtsaW5lLWhlaWdodDoxOHB4O2Rpc3BsYXk6aW5saW5lLWJsb2NrO2Zsb2F0OmxlZnR9LmZyLW1vZGFsIC5mci1tb2RhbC13cmFwcGVyIGRpdi5mci1tb2RhbC1ib2R5e2hlaWdodDoxMDAlO21pbi1oZWlnaHQ6MTUwcHg7b3ZlcmZsb3cteTphdXRvO3BhZGRpbmctYm90dG9tOjEwcHh9LmZyLW1vZGFsIC5mci1tb2RhbC13cmFwcGVyIGRpdi5mci1tb2RhbC1ib2R5OmZvY3Vze291dGxpbmU6MH0uZnItbW9kYWwgLmZyLW1vZGFsLXdyYXBwZXIgZGl2LmZyLW1vZGFsLWJvZHkgYnV0dG9uLmZyLWNvbW1hbmR7aGVpZ2h0OjM2cHg7bGluZS1oZWlnaHQ6MTtjb2xvcjojMWU4OGU1O3BhZGRpbmc6MTBweDtjdXJzb3I6cG9pbnRlcjt0ZXh0LWRlY29yYXRpb246bm9uZTtib3JkZXI6MDtiYWNrZ3JvdW5kOjAgMDtmb250LXNpemU6MTZweDtvdXRsaW5lOjA7LXdlYmtpdC10cmFuc2l0aW9uOmJhY2tncm91bmQgLjJzIGVhc2UgMHM7LW1vei10cmFuc2l0aW9uOmJhY2tncm91bmQgLjJzIGVhc2UgMHM7LW1zLXRyYW5zaXRpb246YmFja2dyb3VuZCAuMnMgZWFzZSAwczstby10cmFuc2l0aW9uOmJhY2tncm91bmQgLjJzIGVhc2UgMHM7Ym9yZGVyLXJhZGl1czoycHg7LW1vei1ib3JkZXItcmFkaXVzOjJweDstd2Via2l0LWJvcmRlci1yYWRpdXM6MnB4Oy1tb3otYmFja2dyb3VuZC1jbGlwOnBhZGRpbmc7LXdlYmtpdC1iYWNrZ3JvdW5kLWNsaXA6cGFkZGluZy1ib3g7YmFja2dyb3VuZC1jbGlwOnBhZGRpbmctYm94fS5mci1tb2RhbCAuZnItbW9kYWwtd3JhcHBlciBkaXYuZnItbW9kYWwtYm9keSBidXR0b24uZnItY29tbWFuZCtidXR0b257bWFyZ2luLWxlZnQ6MjRweH0uZnItbW9kYWwgLmZyLW1vZGFsLXdyYXBwZXIgZGl2LmZyLW1vZGFsLWJvZHkgYnV0dG9uLmZyLWNvbW1hbmQ6aG92ZXIsLmZyLW1vZGFsIC5mci1tb2RhbC13cmFwcGVyIGRpdi5mci1tb2RhbC1ib2R5IGJ1dHRvbi5mci1jb21tYW5kOmZvY3Vze2JhY2tncm91bmQ6I2ViZWJlYjtjb2xvcjojMWU4OGU1fS5mci1tb2RhbCAuZnItbW9kYWwtd3JhcHBlciBkaXYuZnItbW9kYWwtYm9keSBidXR0b24uZnItY29tbWFuZDphY3RpdmV7YmFja2dyb3VuZDojZDZkNmQ2O2NvbG9yOiMxZTg4ZTV9LmZyLW1vZGFsIC5mci1tb2RhbC13cmFwcGVyIGRpdi5mci1tb2RhbC1ib2R5IGJ1dHRvbjo6LW1vei1mb2N1cy1pbm5lcntib3JkZXI6MH0uZnItZGVza3RvcCAuZnItbW9kYWwtd3JhcHBlciAuZnItbW9kYWwtaGVhZCBpOmhvdmVye2JhY2tncm91bmQ6I2ViZWJlYn0uZnItb3ZlcmxheXtwb3NpdGlvbjpmaXhlZDt0b3A6MDtib3R0b206MDtsZWZ0OjA7cmlnaHQ6MDtiYWNrZ3JvdW5kOiMwMDA7LXdlYmtpdC1vcGFjaXR5Oi41Oy1tb3otb3BhY2l0eTouNTtvcGFjaXR5Oi41Oy1tcy1maWx0ZXI6XCJhbHBoYShPcGFjaXR5PTApXCI7ei1pbmRleDoyMTQ3NDgzNjM5fS5mci1wb3B1cHtwb3NpdGlvbjphYnNvbHV0ZTtkaXNwbGF5Om5vbmU7Y29sb3I6IzIyMjtiYWNrZ3JvdW5kOiNmZmY7LXdlYmtpdC1ib3gtc2hhZG93OjAgMXB4IDNweCByZ2JhKDAsMCwwLC4xMiksMCAxcHggMXB4IDFweCByZ2JhKDAsMCwwLC4xNik7LW1vei1ib3gtc2hhZG93OjAgMXB4IDNweCByZ2JhKDAsMCwwLC4xMiksMCAxcHggMXB4IDFweCByZ2JhKDAsMCwwLC4xNik7Ym94LXNoYWRvdzowIDFweCAzcHggcmdiYSgwLDAsMCwuMTIpLDAgMXB4IDFweCAxcHggcmdiYSgwLDAsMCwuMTYpO2JvcmRlci1yYWRpdXM6MnB4Oy1tb3otYm9yZGVyLXJhZGl1czoycHg7LXdlYmtpdC1ib3JkZXItcmFkaXVzOjJweDstbW96LWJhY2tncm91bmQtY2xpcDpwYWRkaW5nOy13ZWJraXQtYmFja2dyb3VuZC1jbGlwOnBhZGRpbmctYm94O2JhY2tncm91bmQtY2xpcDpwYWRkaW5nLWJveDtmb250LWZhbWlseTpBcmlhbCxIZWx2ZXRpY2Esc2Fucy1zZXJpZjstd2Via2l0LWJveC1zaXppbmc6Ym9yZGVyLWJveDstbW96LWJveC1zaXppbmc6Ym9yZGVyLWJveDtib3gtc2l6aW5nOmJvcmRlci1ib3g7dXNlci1zZWxlY3Q6bm9uZTstby11c2VyLXNlbGVjdDpub25lOy1tb3otdXNlci1zZWxlY3Q6bm9uZTsta2h0bWwtdXNlci1zZWxlY3Q6bm9uZTstd2Via2l0LXVzZXItc2VsZWN0Om5vbmU7LW1zLXVzZXItc2VsZWN0Om5vbmU7bWFyZ2luLXRvcDoxMHB4O3otaW5kZXg6MjE0NzQ4MzYzNTt0ZXh0LWFsaWduOmxlZnQ7Ym9yZGVyOjA7Ym9yZGVyLXRvcDo1cHggc29saWQgIzIyMjt0ZXh0LXJlbmRlcmluZzpvcHRpbWl6ZWxlZ2liaWxpdHk7LXdlYmtpdC1mb250LXNtb290aGluZzphbnRpYWxpYXNlZDstbW96LW9zeC1mb250LXNtb290aGluZzpncmF5c2NhbGU7bGluZS1oZWlnaHQ6MS4yfS5mci1wb3B1cCAuZnItaW5wdXQtZm9jdXN7YmFja2dyb3VuZDojZjVmNWY1fS5mci1wb3B1cC5mci1hYm92ZXttYXJnaW4tdG9wOi0xMHB4O2JvcmRlci10b3A6MDtib3JkZXItYm90dG9tOjVweCBzb2xpZCAjMjIyOy13ZWJraXQtYm94LXNoYWRvdzowIC0xcHggM3B4IHJnYmEoMCwwLDAsLjEyKSwwIC0xcHggMXB4IDFweCByZ2JhKDAsMCwwLC4xNik7LW1vei1ib3gtc2hhZG93OjAgLTFweCAzcHggcmdiYSgwLDAsMCwuMTIpLDAgLTFweCAxcHggMXB4IHJnYmEoMCwwLDAsLjE2KTtib3gtc2hhZG93OjAgLTFweCAzcHggcmdiYSgwLDAsMCwuMTIpLDAgLTFweCAxcHggMXB4IHJnYmEoMCwwLDAsLjE2KX0uZnItcG9wdXAuZnItYWN0aXZle2Rpc3BsYXk6YmxvY2t9LmZyLXBvcHVwLmZyLWhpZGRlbnstd2Via2l0LW9wYWNpdHk6MDstbW96LW9wYWNpdHk6MDtvcGFjaXR5OjA7LW1zLWZpbHRlcjpcImFscGhhKE9wYWNpdHk9MClcIn0uZnItcG9wdXAuZnItZW1wdHl7ZGlzcGxheTpub25lIWltcG9ydGFudH0uZnItcG9wdXAgLmZyLWhze2Rpc3BsYXk6YmxvY2shaW1wb3J0YW50fS5mci1wb3B1cCAuZnItaHMuZnItaGlkZGVue2Rpc3BsYXk6bm9uZSFpbXBvcnRhbnR9LmZyLXBvcHVwIC5mci1pbnB1dC1saW5le3Bvc2l0aW9uOnJlbGF0aXZlO3BhZGRpbmc6OHB4IDB9LmZyLXBvcHVwIC5mci1pbnB1dC1saW5lIGlucHV0W3R5cGU9dGV4dF0sLmZyLXBvcHVwIC5mci1pbnB1dC1saW5lIHRleHRhcmVhe3dpZHRoOjEwMCU7bWFyZ2luOjAgMCAxcHg7Ym9yZGVyOjA7Ym9yZGVyLWJvdHRvbTpzb2xpZCAxcHggI2JkYmRiZDtjb2xvcjojMjIyO2ZvbnQtc2l6ZToxNHB4O3BhZGRpbmc6NnB4IDAgMnB4O2JhY2tncm91bmQ6cmdiYSgwLDAsMCwwKTtwb3NpdGlvbjpyZWxhdGl2ZTt6LWluZGV4OjI7LXdlYmtpdC1ib3gtc2l6aW5nOmJvcmRlci1ib3g7LW1vei1ib3gtc2l6aW5nOmJvcmRlci1ib3g7Ym94LXNpemluZzpib3JkZXItYm94fS5mci1wb3B1cCAuZnItaW5wdXQtbGluZSBpbnB1dFt0eXBlPXRleHRdOmZvY3VzLC5mci1wb3B1cCAuZnItaW5wdXQtbGluZSB0ZXh0YXJlYTpmb2N1c3tib3JkZXItYm90dG9tOnNvbGlkIDJweCAjMWU4OGU1O21hcmdpbi1ib3R0b206MH0uZnItcG9wdXAgLmZyLWlucHV0LWxpbmUgaW5wdXQrbGFiZWwsLmZyLXBvcHVwIC5mci1pbnB1dC1saW5lIHRleHRhcmVhK2xhYmVse3Bvc2l0aW9uOmFic29sdXRlO3RvcDowO2xlZnQ6MDtmb250LXNpemU6MTJweDtjb2xvcjpyZ2JhKDAsMCwwLDApOy13ZWJraXQtdHJhbnNpdGlvbjpjb2xvciAuMnMgZWFzZSAwczstbW96LXRyYW5zaXRpb246Y29sb3IgLjJzIGVhc2UgMHM7LW1zLXRyYW5zaXRpb246Y29sb3IgLjJzIGVhc2UgMHM7LW8tdHJhbnNpdGlvbjpjb2xvciAuMnMgZWFzZSAwczt6LWluZGV4OjM7d2lkdGg6MTAwJTtkaXNwbGF5OmJsb2NrO2JhY2tncm91bmQ6I2ZmZn0uZnItcG9wdXAgLmZyLWlucHV0LWxpbmUgaW5wdXQuZnItbm90LWVtcHR5OmZvY3VzK2xhYmVsLC5mci1wb3B1cCAuZnItaW5wdXQtbGluZSB0ZXh0YXJlYS5mci1ub3QtZW1wdHk6Zm9jdXMrbGFiZWx7Y29sb3I6IzFlODhlNX0uZnItcG9wdXAgLmZyLWlucHV0LWxpbmUgaW5wdXQuZnItbm90LWVtcHR5K2xhYmVsLC5mci1wb3B1cCAuZnItaW5wdXQtbGluZSB0ZXh0YXJlYS5mci1ub3QtZW1wdHkrbGFiZWx7Y29sb3I6Z3JheX0uZnItcG9wdXAgaW5wdXQsLmZyLXBvcHVwIHRleHRhcmVhe3VzZXItc2VsZWN0OnRleHQ7LW8tdXNlci1zZWxlY3Q6dGV4dDstbW96LXVzZXItc2VsZWN0OnRleHQ7LWtodG1sLXVzZXItc2VsZWN0OnRleHQ7LXdlYmtpdC11c2VyLXNlbGVjdDp0ZXh0Oy1tcy11c2VyLXNlbGVjdDp0ZXh0O2JvcmRlci1yYWRpdXM6MDstbW96LWJvcmRlci1yYWRpdXM6MDstd2Via2l0LWJvcmRlci1yYWRpdXM6MDstbW96LWJhY2tncm91bmQtY2xpcDpwYWRkaW5nOy13ZWJraXQtYmFja2dyb3VuZC1jbGlwOnBhZGRpbmctYm94O2JhY2tncm91bmQtY2xpcDpwYWRkaW5nLWJveDtvdXRsaW5lOjB9LmZyLXBvcHVwIHRleHRhcmVhe3Jlc2l6ZTpub25lfS5mci1wb3B1cCAuZnItYnV0dG9uc3std2Via2l0LWJveC1zaGFkb3c6MCAxcHggM3B4IHJnYmEoMCwwLDAsLjEyKSwwIDFweCAxcHggMXB4IHJnYmEoMCwwLDAsLjE2KTstbW96LWJveC1zaGFkb3c6MCAxcHggM3B4IHJnYmEoMCwwLDAsLjEyKSwwIDFweCAxcHggMXB4IHJnYmEoMCwwLDAsLjE2KTtib3gtc2hhZG93OjAgMXB4IDNweCByZ2JhKDAsMCwwLC4xMiksMCAxcHggMXB4IDFweCByZ2JhKDAsMCwwLC4xNik7cGFkZGluZzowIDJweDt3aGl0ZS1zcGFjZTpub3dyYXA7bGluZS1oZWlnaHQ6MDtib3JkZXItYm90dG9tOjB9LmZyLXBvcHVwIC5mci1idXR0b25zOjphZnRlcntjbGVhcjpib3RoO2Rpc3BsYXk6YmxvY2s7Y29udGVudDpcIlwiO2hlaWdodDowfS5mci1wb3B1cCAuZnItYnV0dG9ucyAuZnItYnRue2Rpc3BsYXk6aW5saW5lLWJsb2NrO2Zsb2F0Om5vbmV9LmZyLXBvcHVwIC5mci1idXR0b25zIC5mci1idG4gaXtmbG9hdDpsZWZ0fS5mci1wb3B1cCAuZnItYnV0dG9ucyAuZnItc2VwYXJhdG9ye2Rpc3BsYXk6aW5saW5lLWJsb2NrO2Zsb2F0Om5vbmV9LmZyLXBvcHVwIC5mci1sYXllcnt3aWR0aDoyMjVweDstd2Via2l0LWJveC1zaXppbmc6Ym9yZGVyLWJveDstbW96LWJveC1zaXppbmc6Ym9yZGVyLWJveDtib3gtc2l6aW5nOmJvcmRlci1ib3g7bWFyZ2luOjEwcHg7ZGlzcGxheTpub25lfUBtZWRpYSAobWluLXdpZHRoOjc2OHB4KXsuZnItcG9wdXAgLmZyLWxheWVye3dpZHRoOjMwMHB4fX0uZnItcG9wdXAgLmZyLWxheWVyLmZyLWFjdGl2ZXtkaXNwbGF5OmlubGluZS1ibG9ja30uZnItcG9wdXAgLmZyLWFjdGlvbi1idXR0b25ze3otaW5kZXg6NztoZWlnaHQ6MzZweDt0ZXh0LWFsaWduOnJpZ2h0fS5mci1wb3B1cCAuZnItYWN0aW9uLWJ1dHRvbnMgYnV0dG9uLmZyLWNvbW1hbmR7aGVpZ2h0OjM2cHg7bGluZS1oZWlnaHQ6MTtjb2xvcjojMWU4OGU1O3BhZGRpbmc6MTBweDtjdXJzb3I6cG9pbnRlcjt0ZXh0LWRlY29yYXRpb246bm9uZTtib3JkZXI6MDtiYWNrZ3JvdW5kOjAgMDtmb250LXNpemU6MTZweDtvdXRsaW5lOjA7LXdlYmtpdC10cmFuc2l0aW9uOmJhY2tncm91bmQgLjJzIGVhc2UgMHM7LW1vei10cmFuc2l0aW9uOmJhY2tncm91bmQgLjJzIGVhc2UgMHM7LW1zLXRyYW5zaXRpb246YmFja2dyb3VuZCAuMnMgZWFzZSAwczstby10cmFuc2l0aW9uOmJhY2tncm91bmQgLjJzIGVhc2UgMHM7Ym9yZGVyLXJhZGl1czoycHg7LW1vei1ib3JkZXItcmFkaXVzOjJweDstd2Via2l0LWJvcmRlci1yYWRpdXM6MnB4Oy1tb3otYmFja2dyb3VuZC1jbGlwOnBhZGRpbmc7LXdlYmtpdC1iYWNrZ3JvdW5kLWNsaXA6cGFkZGluZy1ib3g7YmFja2dyb3VuZC1jbGlwOnBhZGRpbmctYm94fS5mci1wb3B1cCAuZnItYWN0aW9uLWJ1dHRvbnMgYnV0dG9uLmZyLWNvbW1hbmQrYnV0dG9ue21hcmdpbi1sZWZ0OjI0cHh9LmZyLXBvcHVwIC5mci1hY3Rpb24tYnV0dG9ucyBidXR0b24uZnItY29tbWFuZDpob3ZlciwuZnItcG9wdXAgLmZyLWFjdGlvbi1idXR0b25zIGJ1dHRvbi5mci1jb21tYW5kOmZvY3Vze2JhY2tncm91bmQ6I2ViZWJlYjtjb2xvcjojMWU4OGU1fS5mci1wb3B1cCAuZnItYWN0aW9uLWJ1dHRvbnMgYnV0dG9uLmZyLWNvbW1hbmQ6YWN0aXZle2JhY2tncm91bmQ6I2Q2ZDZkNjtjb2xvcjojMWU4OGU1fS5mci1wb3B1cCAuZnItYWN0aW9uLWJ1dHRvbnMgYnV0dG9uOjotbW96LWZvY3VzLWlubmVye2JvcmRlcjowfS5mci1wb3B1cCAuZnItY2hlY2tib3h7cG9zaXRpb246cmVsYXRpdmU7ZGlzcGxheTppbmxpbmUtYmxvY2s7d2lkdGg6MTZweDtoZWlnaHQ6MTZweDtsaW5lLWhlaWdodDoxOy13ZWJraXQtYm94LXNpemluZzpjb250ZW50LWJveDstbW96LWJveC1zaXppbmc6Y29udGVudC1ib3g7Ym94LXNpemluZzpjb250ZW50LWJveDt2ZXJ0aWNhbC1hbGlnbjptaWRkbGV9LmZyLXBvcHVwIC5mci1jaGVja2JveCBzdmd7bWFyZ2luLWxlZnQ6MnB4O21hcmdpbi10b3A6MnB4O2Rpc3BsYXk6bm9uZTt3aWR0aDoxMHB4O2hlaWdodDoxMHB4fS5mci1wb3B1cCAuZnItY2hlY2tib3ggc3Bhbntib3JkZXI6c29saWQgMXB4ICMyMjI7Ym9yZGVyLXJhZGl1czoycHg7LW1vei1ib3JkZXItcmFkaXVzOjJweDstd2Via2l0LWJvcmRlci1yYWRpdXM6MnB4Oy1tb3otYmFja2dyb3VuZC1jbGlwOnBhZGRpbmc7LXdlYmtpdC1iYWNrZ3JvdW5kLWNsaXA6cGFkZGluZy1ib3g7YmFja2dyb3VuZC1jbGlwOnBhZGRpbmctYm94O3dpZHRoOjE2cHg7aGVpZ2h0OjE2cHg7ZGlzcGxheTppbmxpbmUtYmxvY2s7cG9zaXRpb246cmVsYXRpdmU7ei1pbmRleDoxOy13ZWJraXQtYm94LXNpemluZzpib3JkZXItYm94Oy1tb3otYm94LXNpemluZzpib3JkZXItYm94O2JveC1zaXppbmc6Ym9yZGVyLWJveDstd2Via2l0LXRyYW5zaXRpb246YmFja2dyb3VuZCAuMnMgZWFzZSAwcyxib3JkZXItY29sb3IgLjJzIGVhc2UgMHM7LW1vei10cmFuc2l0aW9uOmJhY2tncm91bmQgLjJzIGVhc2UgMHMsYm9yZGVyLWNvbG9yIC4ycyBlYXNlIDBzOy1tcy10cmFuc2l0aW9uOmJhY2tncm91bmQgLjJzIGVhc2UgMHMsYm9yZGVyLWNvbG9yIC4ycyBlYXNlIDBzOy1vLXRyYW5zaXRpb246YmFja2dyb3VuZCAuMnMgZWFzZSAwcyxib3JkZXItY29sb3IgLjJzIGVhc2UgMHN9LmZyLXBvcHVwIC5mci1jaGVja2JveCBpbnB1dHtwb3NpdGlvbjphYnNvbHV0ZTt6LWluZGV4OjI7LXdlYmtpdC1vcGFjaXR5OjA7LW1vei1vcGFjaXR5OjA7b3BhY2l0eTowOy1tcy1maWx0ZXI6XCJhbHBoYShPcGFjaXR5PTApXCI7Ym9yZGVyOjAgbm9uZTtjdXJzb3I6cG9pbnRlcjtoZWlnaHQ6MTZweDttYXJnaW46MDtwYWRkaW5nOjA7d2lkdGg6MTZweDt0b3A6MXB4O2xlZnQ6MXB4fS5mci1wb3B1cCAuZnItY2hlY2tib3ggaW5wdXQ6Y2hlY2tlZCtzcGFue2JhY2tncm91bmQ6IzFlODhlNTtib3JkZXItY29sb3I6IzFlODhlNX0uZnItcG9wdXAgLmZyLWNoZWNrYm94IGlucHV0OmNoZWNrZWQrc3BhbiBzdmd7ZGlzcGxheTpibG9ja30uZnItcG9wdXAgLmZyLWNoZWNrYm94IGlucHV0OmZvY3VzK3NwYW57Ym9yZGVyLWNvbG9yOiMxZTg4ZTV9LmZyLXBvcHVwIC5mci1jaGVja2JveC1saW5le2ZvbnQtc2l6ZToxNHB4O2xpbmUtaGVpZ2h0OjEuNHB4O21hcmdpbi10b3A6MTBweH0uZnItcG9wdXAgLmZyLWNoZWNrYm94LWxpbmUgbGFiZWx7Y3Vyc29yOnBvaW50ZXI7bWFyZ2luOjAgNXB4O3ZlcnRpY2FsLWFsaWduOm1pZGRsZX0uZnItcG9wdXAuZnItcnRse2RpcmVjdGlvbjpydGw7dGV4dC1hbGlnbjpyaWdodH0uZnItcG9wdXAuZnItcnRsIC5mci1hY3Rpb24tYnV0dG9uc3t0ZXh0LWFsaWduOmxlZnR9LmZyLXBvcHVwLmZyLXJ0bCAuZnItaW5wdXQtbGluZSBpbnB1dCtsYWJlbCwuZnItcG9wdXAuZnItcnRsIC5mci1pbnB1dC1saW5lIHRleHRhcmVhK2xhYmVse2xlZnQ6YXV0bztyaWdodDowfS5mci1wb3B1cC5mci1ydGwgLmZyLWJ1dHRvbnMgLmZyLXNlcGFyYXRvci5mci12c3tmbG9hdDpyaWdodH0uZnItcG9wdXAgLmZyLWFycm93e3dpZHRoOjA7aGVpZ2h0OjA7Ym9yZGVyLWxlZnQ6NXB4IHNvbGlkIHRyYW5zcGFyZW50O2JvcmRlci1yaWdodDo1cHggc29saWQgdHJhbnNwYXJlbnQ7Ym9yZGVyLWJvdHRvbTo1cHggc29saWQgIzIyMjtwb3NpdGlvbjphYnNvbHV0ZTt0b3A6LTlweDtsZWZ0OjUwJTttYXJnaW4tbGVmdDotNXB4O2Rpc3BsYXk6aW5saW5lLWJsb2NrfS5mci1wb3B1cC5mci1hYm92ZSAuZnItYXJyb3d7dG9wOmF1dG87Ym90dG9tOi05cHg7Ym9yZGVyLWJvdHRvbTowO2JvcmRlci10b3A6NXB4IHNvbGlkICMyMjJ9LmZyLXRleHQtZWRpdC1sYXllcnt3aWR0aDoyNTBweDstd2Via2l0LWJveC1zaXppbmc6Ym9yZGVyLWJveDstbW96LWJveC1zaXppbmc6Ym9yZGVyLWJveDtib3gtc2l6aW5nOmJvcmRlci1ib3g7ZGlzcGxheTpibG9jayFpbXBvcnRhbnR9LmZyLXRvb2xiYXJ7Y29sb3I6IzIyMjtiYWNrZ3JvdW5kOiNmZmY7cG9zaXRpb246cmVsYXRpdmU7ei1pbmRleDo0O2ZvbnQtZmFtaWx5OkFyaWFsLEhlbHZldGljYSxzYW5zLXNlcmlmOy13ZWJraXQtYm94LXNpemluZzpib3JkZXItYm94Oy1tb3otYm94LXNpemluZzpib3JkZXItYm94O2JveC1zaXppbmc6Ym9yZGVyLWJveDt1c2VyLXNlbGVjdDpub25lOy1vLXVzZXItc2VsZWN0Om5vbmU7LW1vei11c2VyLXNlbGVjdDpub25lOy1raHRtbC11c2VyLXNlbGVjdDpub25lOy13ZWJraXQtdXNlci1zZWxlY3Q6bm9uZTstbXMtdXNlci1zZWxlY3Q6bm9uZTtwYWRkaW5nOjAgMnB4O2JvcmRlci1yYWRpdXM6MnB4Oy1tb3otYm9yZGVyLXJhZGl1czoycHg7LXdlYmtpdC1ib3JkZXItcmFkaXVzOjJweDstbW96LWJhY2tncm91bmQtY2xpcDpwYWRkaW5nOy13ZWJraXQtYmFja2dyb3VuZC1jbGlwOnBhZGRpbmctYm94O2JhY2tncm91bmQtY2xpcDpwYWRkaW5nLWJveDstd2Via2l0LWJveC1zaGFkb3c6MCAxcHggM3B4IHJnYmEoMCwwLDAsLjEyKSwwIDFweCAxcHggMXB4IHJnYmEoMCwwLDAsLjE2KTstbW96LWJveC1zaGFkb3c6MCAxcHggM3B4IHJnYmEoMCwwLDAsLjEyKSwwIDFweCAxcHggMXB4IHJnYmEoMCwwLDAsLjE2KTtib3gtc2hhZG93OjAgMXB4IDNweCByZ2JhKDAsMCwwLC4xMiksMCAxcHggMXB4IDFweCByZ2JhKDAsMCwwLC4xNik7dGV4dC1hbGlnbjpsZWZ0O2JvcmRlcjowO2JvcmRlci10b3A6NXB4IHNvbGlkICMyMjI7dGV4dC1yZW5kZXJpbmc6b3B0aW1pemVsZWdpYmlsaXR5Oy13ZWJraXQtZm9udC1zbW9vdGhpbmc6YW50aWFsaWFzZWQ7LW1vei1vc3gtZm9udC1zbW9vdGhpbmc6Z3JheXNjYWxlO2xpbmUtaGVpZ2h0OjEuMn0uZnItdG9vbGJhcjo6YWZ0ZXJ7Y2xlYXI6Ym90aDtkaXNwbGF5OmJsb2NrO2NvbnRlbnQ6XCJcIjtoZWlnaHQ6MH0uZnItdG9vbGJhci5mci1ydGx7dGV4dC1hbGlnbjpyaWdodH0uZnItdG9vbGJhci5mci1pbmxpbmV7ZGlzcGxheTpub25lO3doaXRlLXNwYWNlOm5vd3JhcDtwb3NpdGlvbjphYnNvbHV0ZTttYXJnaW4tdG9wOjEwcHh9LmZyLXRvb2xiYXIuZnItaW5saW5lIC5mci1hcnJvd3t3aWR0aDowO2hlaWdodDowO2JvcmRlci1sZWZ0OjVweCBzb2xpZCB0cmFuc3BhcmVudDtib3JkZXItcmlnaHQ6NXB4IHNvbGlkIHRyYW5zcGFyZW50O2JvcmRlci1ib3R0b206NXB4IHNvbGlkICMyMjI7cG9zaXRpb246YWJzb2x1dGU7dG9wOi05cHg7bGVmdDo1MCU7bWFyZ2luLWxlZnQ6LTVweDtkaXNwbGF5OmlubGluZS1ibG9ja30uZnItdG9vbGJhci5mci1pbmxpbmUuZnItYWJvdmV7bWFyZ2luLXRvcDotMTBweDstd2Via2l0LWJveC1zaGFkb3c6MCAtMXB4IDNweCByZ2JhKDAsMCwwLC4xMiksMCAtMXB4IDFweCAxcHggcmdiYSgwLDAsMCwuMTYpOy1tb3otYm94LXNoYWRvdzowIC0xcHggM3B4IHJnYmEoMCwwLDAsLjEyKSwwIC0xcHggMXB4IDFweCByZ2JhKDAsMCwwLC4xNik7Ym94LXNoYWRvdzowIC0xcHggM3B4IHJnYmEoMCwwLDAsLjEyKSwwIC0xcHggMXB4IDFweCByZ2JhKDAsMCwwLC4xNik7Ym9yZGVyLWJvdHRvbTo1cHggc29saWQgIzIyMjtib3JkZXItdG9wOjB9LmZyLXRvb2xiYXIuZnItaW5saW5lLmZyLWFib3ZlIC5mci1hcnJvd3t0b3A6YXV0bztib3R0b206LTlweDtib3JkZXItYm90dG9tOjA7Ym9yZGVyLXRvcC1jb2xvcjppbmhlcml0O2JvcmRlci10b3Atc3R5bGU6c29saWQ7Ym9yZGVyLXRvcC13aWR0aDo1cHh9LmZyLXRvb2xiYXIuZnItdG9we3RvcDowO2JvcmRlci1yYWRpdXM6MnB4IDJweCAwIDA7LW1vei1ib3JkZXItcmFkaXVzOjJweCAycHggMCAwOy13ZWJraXQtYm9yZGVyLXJhZGl1czoycHggMnB4IDAgMDstbW96LWJhY2tncm91bmQtY2xpcDpwYWRkaW5nOy13ZWJraXQtYmFja2dyb3VuZC1jbGlwOnBhZGRpbmctYm94O2JhY2tncm91bmQtY2xpcDpwYWRkaW5nLWJveDstd2Via2l0LWJveC1zaGFkb3c6MCAxcHggM3B4IHJnYmEoMCwwLDAsLjEyKSwwIDFweCAxcHggMXB4IHJnYmEoMCwwLDAsLjE2KTstbW96LWJveC1zaGFkb3c6MCAxcHggM3B4IHJnYmEoMCwwLDAsLjEyKSwwIDFweCAxcHggMXB4IHJnYmEoMCwwLDAsLjE2KTtib3gtc2hhZG93OjAgMXB4IDNweCByZ2JhKDAsMCwwLC4xMiksMCAxcHggMXB4IDFweCByZ2JhKDAsMCwwLC4xNil9LmZyLXRvb2xiYXIuZnItYm90dG9te2JvdHRvbTowO2JvcmRlci1yYWRpdXM6MCAwIDJweCAycHg7LW1vei1ib3JkZXItcmFkaXVzOjAgMCAycHggMnB4Oy13ZWJraXQtYm9yZGVyLXJhZGl1czowIDAgMnB4IDJweDstbW96LWJhY2tncm91bmQtY2xpcDpwYWRkaW5nOy13ZWJraXQtYmFja2dyb3VuZC1jbGlwOnBhZGRpbmctYm94O2JhY2tncm91bmQtY2xpcDpwYWRkaW5nLWJveDstd2Via2l0LWJveC1zaGFkb3c6MCAxcHggM3B4IHJnYmEoMCwwLDAsLjEyKSwwIDFweCAxcHggMXB4IHJnYmEoMCwwLDAsLjE2KTstbW96LWJveC1zaGFkb3c6MCAxcHggM3B4IHJnYmEoMCwwLDAsLjEyKSwwIDFweCAxcHggMXB4IHJnYmEoMCwwLDAsLjE2KTtib3gtc2hhZG93OjAgMXB4IDNweCByZ2JhKDAsMCwwLC4xMiksMCAxcHggMXB4IDFweCByZ2JhKDAsMCwwLC4xNil9LmZyLXNlcGFyYXRvcntiYWNrZ3JvdW5kOiNlYmViZWI7ZGlzcGxheTpibG9jazt2ZXJ0aWNhbC1hbGlnbjp0b3A7ZmxvYXQ6bGVmdH0uZnItc2VwYXJhdG9yKy5mci1zZXBhcmF0b3J7ZGlzcGxheTpub25lfS5mci1zZXBhcmF0b3IuZnItdnN7aGVpZ2h0OjM0cHg7d2lkdGg6MXB4O21hcmdpbjoycHh9LmZyLXNlcGFyYXRvci5mci1oc3tjbGVhcjpib3RoO2hlaWdodDoxcHg7d2lkdGg6Y2FsYygxMDAlIC0gKDIgKiAycHgpKTttYXJnaW46MCAycHh9LmZyLXNlcGFyYXRvci5mci1oaWRkZW57ZGlzcGxheTpub25lIWltcG9ydGFudH0uZnItcnRsIC5mci1zZXBhcmF0b3J7ZmxvYXQ6cmlnaHR9LmZyLXRvb2xiYXIuZnItaW5saW5lIC5mci1zZXBhcmF0b3IuZnItaHN7ZmxvYXQ6bm9uZX0uZnItdG9vbGJhci5mci1pbmxpbmUgLmZyLXNlcGFyYXRvci5mci12c3tmbG9hdDpub25lO2Rpc3BsYXk6aW5saW5lLWJsb2NrfS5mci12aXNpYmlsaXR5LWhlbHBlcntkaXNwbGF5Om5vbmU7bWFyZ2luLWxlZnQ6MCFpbXBvcnRhbnR9QG1lZGlhIChtaW4td2lkdGg6NzY4cHgpey5mci12aXNpYmlsaXR5LWhlbHBlcnttYXJnaW4tbGVmdDoxcHghaW1wb3J0YW50fX1AbWVkaWEgKG1pbi13aWR0aDo5OTJweCl7LmZyLXZpc2liaWxpdHktaGVscGVye21hcmdpbi1sZWZ0OjJweCFpbXBvcnRhbnR9fUBtZWRpYSAobWluLXdpZHRoOjEyMDBweCl7LmZyLXZpc2liaWxpdHktaGVscGVye21hcmdpbi1sZWZ0OjNweCFpbXBvcnRhbnR9fS5mci1vcGFjaXR5LTB7LXdlYmtpdC1vcGFjaXR5OjA7LW1vei1vcGFjaXR5OjA7b3BhY2l0eTowOy1tcy1maWx0ZXI6XCJhbHBoYShPcGFjaXR5PTApXCJ9LmZyLWJveHtwb3NpdGlvbjpyZWxhdGl2ZX0uZnItc3RpY2t5e3Bvc2l0aW9uOi13ZWJraXQtc3RpY2t5O3Bvc2l0aW9uOi1tb3otc3RpY2t5O3Bvc2l0aW9uOi1tcy1zdGlja3k7cG9zaXRpb246LW8tc3RpY2t5O3Bvc2l0aW9uOnN0aWNreX0uZnItc3RpY2t5LW9mZntwb3NpdGlvbjpyZWxhdGl2ZX0uZnItc3RpY2t5LW9ue3Bvc2l0aW9uOmZpeGVkfS5mci1zdGlja3ktb24uZnItc3RpY2t5LWlvc3twb3NpdGlvbjphYnNvbHV0ZTtsZWZ0OjA7cmlnaHQ6MDt3aWR0aDphdXRvIWltcG9ydGFudH0uZnItc3RpY2t5LWR1bW15e2Rpc3BsYXk6bm9uZX0uZnItc3RpY2t5LW9uKy5mci1zdGlja3ktZHVtbXksLmZyLXN0aWNreS1ib3g+LmZyLXN0aWNreS1kdW1teXtkaXNwbGF5OmJsb2NrfXNwYW4uZnItc3Itb25seXtwb3NpdGlvbjphYnNvbHV0ZTt3aWR0aDoxcHg7aGVpZ2h0OjFweDtwYWRkaW5nOjA7bWFyZ2luOi0xcHg7b3ZlcmZsb3c6aGlkZGVuO2NsaXA6cmVjdCgwLDAsMCwwKTtib3JkZXI6MH0uY2xlYXJmaXg6OmFmdGVye2NsZWFyOmJvdGg7ZGlzcGxheTpibG9jaztjb250ZW50OlwiXCI7aGVpZ2h0OjB9LmhpZGUtYnktY2xpcHBpbmd7cG9zaXRpb246YWJzb2x1dGU7d2lkdGg6MXB4O2hlaWdodDoxcHg7cGFkZGluZzowO21hcmdpbjotMXB4O292ZXJmbG93OmhpZGRlbjtjbGlwOnJlY3QoMCwwLDAsMCk7Ym9yZGVyOjB9LmZyLWJveCAuZnItY291bnRlcntwb3NpdGlvbjphYnNvbHV0ZTtib3R0b206MDtwYWRkaW5nOjVweDtyaWdodDowO2NvbG9yOiNjY2M7Y29udGVudDphdHRyKGRhdGEtY2hhcnMpO2ZvbnQtc2l6ZToxNXB4O2ZvbnQtZmFtaWx5OlwiVGltZXMgTmV3IFJvbWFuXCIsR2VvcmdpYSxTZXJpZjt6LWluZGV4OjE7YmFja2dyb3VuZDojZmZmO2JvcmRlci10b3A6c29saWQgMXB4ICNlYmViZWI7Ym9yZGVyLWxlZnQ6c29saWQgMXB4ICNlYmViZWI7Ym9yZGVyLXJhZGl1czoycHggMCAwOy1tb3otYm9yZGVyLXJhZGl1czoycHggMCAwOy13ZWJraXQtYm9yZGVyLXJhZGl1czoycHggMCAwOy1tb3otYmFja2dyb3VuZC1jbGlwOnBhZGRpbmc7LXdlYmtpdC1iYWNrZ3JvdW5kLWNsaXA6cGFkZGluZy1ib3g7YmFja2dyb3VuZC1jbGlwOnBhZGRpbmctYm94fS5mci1ib3guZnItcnRsIC5mci1jb3VudGVye2xlZnQ6MDtyaWdodDphdXRvO2JvcmRlci1sZWZ0OjA7Ym9yZGVyLXJpZ2h0OnNvbGlkIDFweCAjZWJlYmViO2JvcmRlci1yYWRpdXM6MCAycHggMCAwOy1tb3otYm9yZGVyLXJhZGl1czowIDJweCAwIDA7LXdlYmtpdC1ib3JkZXItcmFkaXVzOjAgMnB4IDAgMDstbW96LWJhY2tncm91bmQtY2xpcDpwYWRkaW5nOy13ZWJraXQtYmFja2dyb3VuZC1jbGlwOnBhZGRpbmctYm94O2JhY2tncm91bmQtY2xpcDpwYWRkaW5nLWJveH0uZnItYm94LmZyLWNvZGUtdmlldyAuZnItY291bnRlcntkaXNwbGF5Om5vbmV9LmNsZWFyZml4OjphZnRlcntjbGVhcjpib3RoO2Rpc3BsYXk6YmxvY2s7Y29udGVudDpcIlwiO2hlaWdodDowfS5oaWRlLWJ5LWNsaXBwaW5ne3Bvc2l0aW9uOmFic29sdXRlO3dpZHRoOjFweDtoZWlnaHQ6MXB4O3BhZGRpbmc6MDttYXJnaW46LTFweDtvdmVyZmxvdzpoaWRkZW47Y2xpcDpyZWN0KDAsMCwwLDApO2JvcmRlcjowfXRleHRhcmVhLmZyLWNvZGV7ZGlzcGxheTpub25lO3dpZHRoOjEwMCU7cmVzaXplOm5vbmU7LW1vei1yZXNpemU6bm9uZTstd2Via2l0LXJlc2l6ZTpub25lOy13ZWJraXQtYm94LXNpemluZzpib3JkZXItYm94Oy1tb3otYm94LXNpemluZzpib3JkZXItYm94O2JveC1zaXppbmc6Ym9yZGVyLWJveDtib3JkZXI6MDtwYWRkaW5nOjEwcHg7bWFyZ2luOjA7Zm9udC1mYW1pbHk6XCJDb3VyaWVyIE5ld1wiLG1vbm9zcGFjZTtmb250LXNpemU6MTRweDtiYWNrZ3JvdW5kOiNmZmY7Y29sb3I6IzAwMDtvdXRsaW5lOjB9LmZyLWJveC5mci1ydGwgdGV4dGFyZWEuZnItY29kZXtkaXJlY3Rpb246cnRsfS5mci1ib3ggLkNvZGVNaXJyb3J7ZGlzcGxheTpub25lfS5mci1ib3guZnItY29kZS12aWV3IHRleHRhcmVhLmZyLWNvZGV7ZGlzcGxheTpibG9ja30uZnItYm94LmZyLWNvZGUtdmlldy5mci1pbmxpbmV7LXdlYmtpdC1ib3gtc2hhZG93OjAgMXB4IDNweCByZ2JhKDAsMCwwLC4xMiksMCAxcHggMXB4IDFweCByZ2JhKDAsMCwwLC4xNik7LW1vei1ib3gtc2hhZG93OjAgMXB4IDNweCByZ2JhKDAsMCwwLC4xMiksMCAxcHggMXB4IDFweCByZ2JhKDAsMCwwLC4xNik7Ym94LXNoYWRvdzowIDFweCAzcHggcmdiYSgwLDAsMCwuMTIpLDAgMXB4IDFweCAxcHggcmdiYSgwLDAsMCwuMTYpfS5mci1ib3guZnItY29kZS12aWV3IC5mci1lbGVtZW50LC5mci1ib3guZnItY29kZS12aWV3IC5mci1wbGFjZWhvbGRlciwuZnItYm94LmZyLWNvZGUtdmlldyAuZnItaWZyYW1le2Rpc3BsYXk6bm9uZX0uZnItYm94LmZyLWNvZGUtdmlldyAuQ29kZU1pcnJvcntkaXNwbGF5OmJsb2NrfS5mci1ib3guZnItaW5saW5lLmZyLWNvZGUtdmlldyAuZnItY29tbWFuZC5mci1idG4uaHRtbC1zd2l0Y2h7ZGlzcGxheTpibG9ja30uZnItYm94LmZyLWlubGluZSAuZnItY29tbWFuZC5mci1idG4uaHRtbC1zd2l0Y2h7cG9zaXRpb246YWJzb2x1dGU7dG9wOjA7cmlnaHQ6MDstd2Via2l0LWJveC1zaGFkb3c6MCAxcHggM3B4IHJnYmEoMCwwLDAsLjEyKSwwIDFweCAxcHggMXB4IHJnYmEoMCwwLDAsLjE2KTstbW96LWJveC1zaGFkb3c6MCAxcHggM3B4IHJnYmEoMCwwLDAsLjEyKSwwIDFweCAxcHggMXB4IHJnYmEoMCwwLDAsLjE2KTtib3gtc2hhZG93OjAgMXB4IDNweCByZ2JhKDAsMCwwLC4xMiksMCAxcHggMXB4IDFweCByZ2JhKDAsMCwwLC4xNik7ZGlzcGxheTpub25lO2JhY2tncm91bmQ6I2ZmZjtjb2xvcjojMjIyOy1tb3otb3V0bGluZTowO291dGxpbmU6MDtib3JkZXI6MDtsaW5lLWhlaWdodDoxO2N1cnNvcjpwb2ludGVyO3RleHQtYWxpZ246bGVmdDtwYWRkaW5nOjEycHg7LXdlYmtpdC10cmFuc2l0aW9uOmJhY2tncm91bmQgLjJzIGVhc2UgMHM7LW1vei10cmFuc2l0aW9uOmJhY2tncm91bmQgLjJzIGVhc2UgMHM7LW1zLXRyYW5zaXRpb246YmFja2dyb3VuZCAuMnMgZWFzZSAwczstby10cmFuc2l0aW9uOmJhY2tncm91bmQgLjJzIGVhc2UgMHM7Ym9yZGVyLXJhZGl1czowOy1tb3otYm9yZGVyLXJhZGl1czowOy13ZWJraXQtYm9yZGVyLXJhZGl1czowOy1tb3otYmFja2dyb3VuZC1jbGlwOnBhZGRpbmc7LXdlYmtpdC1iYWNrZ3JvdW5kLWNsaXA6cGFkZGluZy1ib3g7YmFja2dyb3VuZC1jbGlwOnBhZGRpbmctYm94O3otaW5kZXg6Mjstd2Via2l0LWJveC1zaXppbmc6Ym9yZGVyLWJveDstbW96LWJveC1zaXppbmc6Ym9yZGVyLWJveDtib3gtc2l6aW5nOmJvcmRlci1ib3g7dGV4dC1kZWNvcmF0aW9uOm5vbmU7dXNlci1zZWxlY3Q6bm9uZTstby11c2VyLXNlbGVjdDpub25lOy1tb3otdXNlci1zZWxlY3Q6bm9uZTsta2h0bWwtdXNlci1zZWxlY3Q6bm9uZTstd2Via2l0LXVzZXItc2VsZWN0Om5vbmU7LW1zLXVzZXItc2VsZWN0Om5vbmV9LmZyLWJveC5mci1pbmxpbmUgLmZyLWNvbW1hbmQuZnItYnRuLmh0bWwtc3dpdGNoIGl7Zm9udC1zaXplOjE0cHg7d2lkdGg6MTRweDt0ZXh0LWFsaWduOmNlbnRlcn0uZnItYm94LmZyLWlubGluZSAuZnItY29tbWFuZC5mci1idG4uaHRtbC1zd2l0Y2guZnItZGVza3RvcDpob3ZlcntiYWNrZ3JvdW5kOiNlYmViZWJ9LmNsZWFyZml4OjphZnRlcntjbGVhcjpib3RoO2Rpc3BsYXk6YmxvY2s7Y29udGVudDpcIlwiO2hlaWdodDowfS5oaWRlLWJ5LWNsaXBwaW5ne3Bvc2l0aW9uOmFic29sdXRlO3dpZHRoOjFweDtoZWlnaHQ6MXB4O3BhZGRpbmc6MDttYXJnaW46LTFweDtvdmVyZmxvdzpoaWRkZW47Y2xpcDpyZWN0KDAsMCwwLDApO2JvcmRlcjowfS5mci1wb3B1cCAuZnItY29sb3JzLXRhYnN7LXdlYmtpdC1ib3gtc2hhZG93OjAgMXB4IDNweCByZ2JhKDAsMCwwLC4xMiksMCAxcHggMXB4IDFweCByZ2JhKDAsMCwwLC4xNik7LW1vei1ib3gtc2hhZG93OjAgMXB4IDNweCByZ2JhKDAsMCwwLC4xMiksMCAxcHggMXB4IDFweCByZ2JhKDAsMCwwLC4xNik7Ym94LXNoYWRvdzowIDFweCAzcHggcmdiYSgwLDAsMCwuMTIpLDAgMXB4IDFweCAxcHggcmdiYSgwLDAsMCwuMTYpO21hcmdpbi1ib3R0b206NXB4O2xpbmUtaGVpZ2h0OjE2cHg7bWFyZ2luLWxlZnQ6LTJweDttYXJnaW4tcmlnaHQ6LTJweH0uZnItcG9wdXAgLmZyLWNvbG9ycy10YWJzIC5mci1jb2xvcnMtdGFie2Rpc3BsYXk6aW5saW5lLWJsb2NrO3dpZHRoOjUwJTtjdXJzb3I6cG9pbnRlcjt0ZXh0LWFsaWduOmNlbnRlcjtjb2xvcjojMjIyO2ZvbnQtc2l6ZToxM3B4O3BhZGRpbmc6OHB4IDA7cG9zaXRpb246cmVsYXRpdmV9LmZyLXBvcHVwIC5mci1jb2xvcnMtdGFicyAuZnItY29sb3JzLXRhYjpob3ZlciwuZnItcG9wdXAgLmZyLWNvbG9ycy10YWJzIC5mci1jb2xvcnMtdGFiOmZvY3Vze2NvbG9yOiMxZTg4ZTV9LmZyLXBvcHVwIC5mci1jb2xvcnMtdGFicyAuZnItY29sb3JzLXRhYltkYXRhLXBhcmFtMT1iYWNrZ3JvdW5kXTo6YWZ0ZXJ7cG9zaXRpb246YWJzb2x1dGU7Ym90dG9tOjA7bGVmdDowO3dpZHRoOjEwMCU7aGVpZ2h0OjJweDtiYWNrZ3JvdW5kOiMxZTg4ZTU7Y29udGVudDonJzstd2Via2l0LXRyYW5zaXRpb246dHJhbnNmb3JtIC4ycyBlYXNlIDBzOy1tb3otdHJhbnNpdGlvbjp0cmFuc2Zvcm0gLjJzIGVhc2UgMHM7LW1zLXRyYW5zaXRpb246dHJhbnNmb3JtIC4ycyBlYXNlIDBzOy1vLXRyYW5zaXRpb246dHJhbnNmb3JtIC4ycyBlYXNlIDBzfS5mci1wb3B1cCAuZnItY29sb3JzLXRhYnMgLmZyLWNvbG9ycy10YWIuZnItc2VsZWN0ZWQtdGFie2NvbG9yOiMxZTg4ZTV9LmZyLXBvcHVwIC5mci1jb2xvcnMtdGFicyAuZnItY29sb3JzLXRhYi5mci1zZWxlY3RlZC10YWJbZGF0YS1wYXJhbTE9dGV4dF1+W2RhdGEtcGFyYW0xPWJhY2tncm91bmRdOjphZnRlcnstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGUzZCgtMTAwJSwwLDApOy1tb3otdHJhbnNmb3JtOnRyYW5zbGF0ZTNkKC0xMDAlLDAsMCk7LW1zLXRyYW5zZm9ybTp0cmFuc2xhdGUzZCgtMTAwJSwwLDApOy1vLXRyYW5zZm9ybTp0cmFuc2xhdGUzZCgtMTAwJSwwLDApfS5mci1wb3B1cCAuZnItY29sb3ItaGV4LWxheWVye3dpZHRoOjEwMCU7bWFyZ2luOjA7cGFkZGluZzoxMHB4fS5mci1wb3B1cCAuZnItY29sb3ItaGV4LWxheWVyIC5mci1pbnB1dC1saW5le2Zsb2F0OmxlZnQ7d2lkdGg6Y2FsYygxMDAlIC0gNTBweCk7cGFkZGluZzo4cHggMCAwfS5mci1wb3B1cCAuZnItY29sb3ItaGV4LWxheWVyIC5mci1hY3Rpb24tYnV0dG9uc3tmbG9hdDpyaWdodDt3aWR0aDo1MHB4fS5mci1wb3B1cCAuZnItY29sb3ItaGV4LWxheWVyIC5mci1hY3Rpb24tYnV0dG9ucyBidXR0b24uZnItY29tbWFuZHtiYWNrZ3JvdW5kLWNvbG9yOiMxZTg4ZTU7Y29sb3I6I0ZGRiFpbXBvcnRhbnQ7Ym9yZGVyLXJhZGl1czoycHg7LW1vei1ib3JkZXItcmFkaXVzOjJweDstd2Via2l0LWJvcmRlci1yYWRpdXM6MnB4Oy1tb3otYmFja2dyb3VuZC1jbGlwOnBhZGRpbmc7LXdlYmtpdC1iYWNrZ3JvdW5kLWNsaXA6cGFkZGluZy1ib3g7YmFja2dyb3VuZC1jbGlwOnBhZGRpbmctYm94O2ZvbnQtc2l6ZToxM3B4O2hlaWdodDozMnB4fS5mci1wb3B1cCAuZnItY29sb3ItaGV4LWxheWVyIC5mci1hY3Rpb24tYnV0dG9ucyBidXR0b24uZnItY29tbWFuZDpob3ZlcntiYWNrZ3JvdW5kLWNvbG9yOiMxNjZkYmE7Y29sb3I6I0ZGRn0uZnItcG9wdXAgLmZyLXNlcGFyYXRvcisuZnItY29sb3JzLXRhYnN7LXdlYmtpdC1ib3gtc2hhZG93Om5vbmU7LW1vei1ib3gtc2hhZG93Om5vbmU7Ym94LXNoYWRvdzpub25lO21hcmdpbi1sZWZ0OjJweDttYXJnaW4tcmlnaHQ6MnB4fS5mci1wb3B1cCAuZnItY29sb3Itc2V0e2xpbmUtaGVpZ2h0OjA7ZGlzcGxheTpub25lfS5mci1wb3B1cCAuZnItY29sb3Itc2V0LmZyLXNlbGVjdGVkLXNldHtkaXNwbGF5OmJsb2NrfS5mci1wb3B1cCAuZnItY29sb3Itc2V0PnNwYW57ZGlzcGxheTppbmxpbmUtYmxvY2s7d2lkdGg6MzJweDtoZWlnaHQ6MzJweDtwb3NpdGlvbjpyZWxhdGl2ZTt6LWluZGV4OjF9LmZyLXBvcHVwIC5mci1jb2xvci1zZXQ+c3Bhbj5pLC5mci1wb3B1cCAuZnItY29sb3Itc2V0PnNwYW4+c3Zne3RleHQtYWxpZ246Y2VudGVyO2xpbmUtaGVpZ2h0OjMycHg7aGVpZ2h0OjMycHg7d2lkdGg6MzJweDtmb250LXNpemU6MTNweDtwb3NpdGlvbjphYnNvbHV0ZTtib3R0b206MDtjdXJzb3I6ZGVmYXVsdDtsZWZ0OjB9LmZyLXBvcHVwIC5mci1jb2xvci1zZXQ+c3BhbiAuZnItc2VsZWN0ZWQtY29sb3J7Y29sb3I6I2ZmZjtmb250LWZhbWlseTpGb250QXdlc29tZTtmb250LXNpemU6MTNweDtmb250LXdlaWdodDo0MDA7bGluZS1oZWlnaHQ6MzJweDtwb3NpdGlvbjphYnNvbHV0ZTt0b3A6MDtib3R0b206MDtyaWdodDowO2xlZnQ6MDt0ZXh0LWFsaWduOmNlbnRlcjtjdXJzb3I6ZGVmYXVsdH0uZnItcG9wdXAgLmZyLWNvbG9yLXNldD5zcGFuOmhvdmVyLC5mci1wb3B1cCAuZnItY29sb3Itc2V0PnNwYW46Zm9jdXN7b3V0bGluZToxcHggc29saWQgIzIyMjt6LWluZGV4OjJ9LmZyLXJ0bCAuZnItcG9wdXAgLmZyLWNvbG9ycy10YWJzIC5mci1jb2xvcnMtdGFiLmZyLXNlbGVjdGVkLXRhYltkYXRhLXBhcmFtMT10ZXh0XX5bZGF0YS1wYXJhbTE9YmFja2dyb3VuZF06OmFmdGVyey13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZTNkKDEwMCUsMCwwKTstbW96LXRyYW5zZm9ybTp0cmFuc2xhdGUzZCgxMDAlLDAsMCk7LW1zLXRyYW5zZm9ybTp0cmFuc2xhdGUzZCgxMDAlLDAsMCk7LW8tdHJhbnNmb3JtOnRyYW5zbGF0ZTNkKDEwMCUsMCwwKX0uY2xlYXJmaXg6OmFmdGVye2NsZWFyOmJvdGg7ZGlzcGxheTpibG9jaztjb250ZW50OlwiXCI7aGVpZ2h0OjB9LmhpZGUtYnktY2xpcHBpbmd7cG9zaXRpb246YWJzb2x1dGU7d2lkdGg6MXB4O2hlaWdodDoxcHg7cGFkZGluZzowO21hcmdpbjotMXB4O292ZXJmbG93OmhpZGRlbjtjbGlwOnJlY3QoMCwwLDAsMCk7Ym9yZGVyOjB9LmZyLWRyYWctaGVscGVye2JhY2tncm91bmQ6IzFlODhlNTtoZWlnaHQ6MnB4O21hcmdpbi10b3A6LTFweDstd2Via2l0LW9wYWNpdHk6LjI7LW1vei1vcGFjaXR5Oi4yO29wYWNpdHk6LjI7LW1zLWZpbHRlcjpcImFscGhhKE9wYWNpdHk9MClcIjtwb3NpdGlvbjphYnNvbHV0ZTt6LWluZGV4OjIxNDc0ODM2NDA7ZGlzcGxheTpub25lfS5mci1kcmFnLWhlbHBlci5mci12aXNpYmxle2Rpc3BsYXk6YmxvY2t9LmZyLWRyYWdnaW5ney13ZWJraXQtb3BhY2l0eTouNDstbW96LW9wYWNpdHk6LjQ7b3BhY2l0eTouNDstbXMtZmlsdGVyOlwiYWxwaGEoT3BhY2l0eT0wKVwifS5jbGVhcmZpeDo6YWZ0ZXJ7Y2xlYXI6Ym90aDtkaXNwbGF5OmJsb2NrO2NvbnRlbnQ6XCJcIjtoZWlnaHQ6MH0uaGlkZS1ieS1jbGlwcGluZ3twb3NpdGlvbjphYnNvbHV0ZTt3aWR0aDoxcHg7aGVpZ2h0OjFweDtwYWRkaW5nOjA7bWFyZ2luOi0xcHg7b3ZlcmZsb3c6aGlkZGVuO2NsaXA6cmVjdCgwLDAsMCwwKTtib3JkZXI6MH0uZnItcG9wdXAgLmZyLWVtb3RpY29ue2Rpc3BsYXk6aW5saW5lLWJsb2NrO2ZvbnQtc2l6ZToyMHB4O3dpZHRoOjIwcHg7cGFkZGluZzo1cHg7bGluZS1oZWlnaHQ6MTtjdXJzb3I6ZGVmYXVsdDtmb250LXdlaWdodDo0MDA7Zm9udC1mYW1pbHk6XCJBcHBsZSBDb2xvciBFbW9qaVwiLFwiU2Vnb2UgVUkgRW1vamlcIixOb3RvQ29sb3JFbW9qaSxcIlNlZ29lIFVJIFN5bWJvbFwiLFwiQW5kcm9pZCBFbW9qaVwiLEVtb2ppU3ltYm9sczstd2Via2l0LWJveC1zaXppbmc6Y29udGVudC1ib3g7LW1vei1ib3gtc2l6aW5nOmNvbnRlbnQtYm94O2JveC1zaXppbmc6Y29udGVudC1ib3h9LmZyLXBvcHVwIC5mci1lbW90aWNvbiBpbWd7aGVpZ2h0OjIwcHh9LmZyLXBvcHVwIC5mci1saW5rOmZvY3Vze291dGxpbmU6MDtiYWNrZ3JvdW5kOiNlYmViZWJ9LmNsZWFyZml4OjphZnRlcntjbGVhcjpib3RoO2Rpc3BsYXk6YmxvY2s7Y29udGVudDpcIlwiO2hlaWdodDowfS5oaWRlLWJ5LWNsaXBwaW5ne3Bvc2l0aW9uOmFic29sdXRlO3dpZHRoOjFweDtoZWlnaHQ6MXB4O3BhZGRpbmc6MDttYXJnaW46LTFweDtvdmVyZmxvdzpoaWRkZW47Y2xpcDpyZWN0KDAsMCwwLDApO2JvcmRlcjowfS5mci1wb3B1cCAuZnItZmlsZS11cGxvYWQtbGF5ZXJ7Ym9yZGVyOmRhc2hlZCAycHggI2JkYmRiZDtwYWRkaW5nOjI1cHggMDtwb3NpdGlvbjpyZWxhdGl2ZTtmb250LXNpemU6MTRweDtsZXR0ZXItc3BhY2luZzoxcHg7bGluZS1oZWlnaHQ6MTQwJTstd2Via2l0LWJveC1zaXppbmc6Ym9yZGVyLWJveDstbW96LWJveC1zaXppbmc6Ym9yZGVyLWJveDtib3gtc2l6aW5nOmJvcmRlci1ib3g7dGV4dC1hbGlnbjpjZW50ZXJ9LmZyLXBvcHVwIC5mci1maWxlLXVwbG9hZC1sYXllcjpob3ZlcntiYWNrZ3JvdW5kOiNlYmViZWJ9LmZyLXBvcHVwIC5mci1maWxlLXVwbG9hZC1sYXllci5mci1kcm9we2JhY2tncm91bmQ6I2ViZWJlYjtib3JkZXItY29sb3I6IzFlODhlNX0uZnItcG9wdXAgLmZyLWZpbGUtdXBsb2FkLWxheWVyIC5mci1mb3Jtey13ZWJraXQtb3BhY2l0eTowOy1tb3otb3BhY2l0eTowO29wYWNpdHk6MDstbXMtZmlsdGVyOlwiYWxwaGEoT3BhY2l0eT0wKVwiO3Bvc2l0aW9uOmFic29sdXRlO3RvcDowO2JvdHRvbTowO2xlZnQ6MDtyaWdodDowO3otaW5kZXg6MjE0NzQ4MzY0MDtvdmVyZmxvdzpoaWRkZW47bWFyZ2luOjAhaW1wb3J0YW50O3BhZGRpbmc6MCFpbXBvcnRhbnQ7d2lkdGg6MTAwJSFpbXBvcnRhbnR9LmZyLXBvcHVwIC5mci1maWxlLXVwbG9hZC1sYXllciAuZnItZm9ybSBpbnB1dHtjdXJzb3I6cG9pbnRlcjtwb3NpdGlvbjphYnNvbHV0ZTtyaWdodDowO3RvcDowO2JvdHRvbTowO3dpZHRoOjUwMCU7aGVpZ2h0OjEwMCU7bWFyZ2luOjA7Zm9udC1zaXplOjQwMHB4fS5mci1wb3B1cCAuZnItZmlsZS1wcm9ncmVzcy1iYXItbGF5ZXJ7LXdlYmtpdC1ib3gtc2l6aW5nOmJvcmRlci1ib3g7LW1vei1ib3gtc2l6aW5nOmJvcmRlci1ib3g7Ym94LXNpemluZzpib3JkZXItYm94fS5mci1wb3B1cCAuZnItZmlsZS1wcm9ncmVzcy1iYXItbGF5ZXI+aDN7Zm9udC1zaXplOjE2cHg7bWFyZ2luOjEwcHggMDtmb250LXdlaWdodDo0MDB9LmZyLXBvcHVwIC5mci1maWxlLXByb2dyZXNzLWJhci1sYXllcj5kaXYuZnItYWN0aW9uLWJ1dHRvbnN7ZGlzcGxheTpub25lfS5mci1wb3B1cCAuZnItZmlsZS1wcm9ncmVzcy1iYXItbGF5ZXI+ZGl2LmZyLWxvYWRlcntiYWNrZ3JvdW5kOiNiY2RiZjc7aGVpZ2h0OjEwcHg7d2lkdGg6MTAwJTttYXJnaW4tdG9wOjIwcHg7b3ZlcmZsb3c6aGlkZGVuO3Bvc2l0aW9uOnJlbGF0aXZlfS5mci1wb3B1cCAuZnItZmlsZS1wcm9ncmVzcy1iYXItbGF5ZXI+ZGl2LmZyLWxvYWRlciBzcGFue2Rpc3BsYXk6YmxvY2s7aGVpZ2h0OjEwMCU7d2lkdGg6MDtiYWNrZ3JvdW5kOiMxZTg4ZTU7LXdlYmtpdC10cmFuc2l0aW9uOndpZHRoIC4ycyBlYXNlIDBzOy1tb3otdHJhbnNpdGlvbjp3aWR0aCAuMnMgZWFzZSAwczstbXMtdHJhbnNpdGlvbjp3aWR0aCAuMnMgZWFzZSAwczstby10cmFuc2l0aW9uOndpZHRoIC4ycyBlYXNlIDBzfS5mci1wb3B1cCAuZnItZmlsZS1wcm9ncmVzcy1iYXItbGF5ZXI+ZGl2LmZyLWxvYWRlci5mci1pbmRldGVybWluYXRlIHNwYW57d2lkdGg6MzAlIWltcG9ydGFudDtwb3NpdGlvbjphYnNvbHV0ZTt0b3A6MDstd2Via2l0LWFuaW1hdGlvbjpsb2FkaW5nIDJzIGxpbmVhciBpbmZpbml0ZTstbW96LWFuaW1hdGlvbjpsb2FkaW5nIDJzIGxpbmVhciBpbmZpbml0ZTstby1hbmltYXRpb246bG9hZGluZyAycyBsaW5lYXIgaW5maW5pdGU7YW5pbWF0aW9uOmxvYWRpbmcgMnMgbGluZWFyIGluZmluaXRlfS5mci1wb3B1cCAuZnItZmlsZS1wcm9ncmVzcy1iYXItbGF5ZXIuZnItZXJyb3I+ZGl2LmZyLWxvYWRlcntkaXNwbGF5Om5vbmV9LmZyLXBvcHVwIC5mci1maWxlLXByb2dyZXNzLWJhci1sYXllci5mci1lcnJvcj5kaXYuZnItYWN0aW9uLWJ1dHRvbnN7ZGlzcGxheTpibG9ja31Aa2V5ZnJhbWVzIGxvYWRpbmd7ZnJvbXtsZWZ0Oi0yNSV9dG97bGVmdDoxMDAlfX1ALXdlYmtpdC1rZXlmcmFtZXMgbG9hZGluZ3tmcm9te2xlZnQ6LTI1JX10b3tsZWZ0OjEwMCV9fUAtbW96LWtleWZyYW1lcyBsb2FkaW5ne2Zyb217bGVmdDotMjUlfXRve2xlZnQ6MTAwJX19QC1vLWtleWZyYW1lcyBsb2FkaW5ne2Zyb217bGVmdDotMjUlfXRve2xlZnQ6MTAwJX19Ym9keS5mci1mdWxsc2NyZWVue292ZXJmbG93OmhpZGRlbjtoZWlnaHQ6MTAwJTt3aWR0aDoxMDAlO3Bvc2l0aW9uOmZpeGVkfS5mci1ib3guZnItZnVsbHNjcmVlbnttYXJnaW46MCFpbXBvcnRhbnQ7cG9zaXRpb246Zml4ZWQ7dG9wOjA7bGVmdDowO2JvdHRvbTowO3JpZ2h0OjA7ei1pbmRleDoyMTQ3NDgzNjMwIWltcG9ydGFudDt3aWR0aDphdXRvIWltcG9ydGFudH0uZnItYm94LmZyLWZ1bGxzY3JlZW4gLmZyLXRvb2xiYXIuZnItdG9we3RvcDowIWltcG9ydGFudH0uZnItYm94LmZyLWZ1bGxzY3JlZW4gLmZyLXRvb2xiYXIuZnItYm90dG9te2JvdHRvbTowIWltcG9ydGFudH0uZnItZnVsbHNjcmVlbi13cmFwcGVye3otaW5kZXg6MjE0NzQ4MzY0MCFpbXBvcnRhbnQ7d2lkdGg6MTAwJSFpbXBvcnRhbnQ7bWFyZ2luOjAhaW1wb3J0YW50O3BhZGRpbmc6MCFpbXBvcnRhbnQ7b3ZlcmZsb3c6dmlzaWJsZSFpbXBvcnRhbnR9LmNsZWFyZml4OjphZnRlcntjbGVhcjpib3RoO2Rpc3BsYXk6YmxvY2s7Y29udGVudDpcIlwiO2hlaWdodDowfS5oaWRlLWJ5LWNsaXBwaW5ne3Bvc2l0aW9uOmFic29sdXRlO3dpZHRoOjFweDtoZWlnaHQ6MXB4O3BhZGRpbmc6MDttYXJnaW46LTFweDtvdmVyZmxvdzpoaWRkZW47Y2xpcDpyZWN0KDAsMCwwLDApO2JvcmRlcjowfS5mci1tb2RhbCAuZnItbW9kYWwtd3JhcHBlciAuZnItbW9kYWwtYm9keSAuZnItaGVscC1tb2RhbHt0ZXh0LWFsaWduOmxlZnQ7cGFkZGluZzoyMHB4IDIwcHggMTBweH0uZnItbW9kYWwgLmZyLW1vZGFsLXdyYXBwZXIgLmZyLW1vZGFsLWJvZHkgLmZyLWhlbHAtbW9kYWwgdGFibGV7Ym9yZGVyLWNvbGxhcHNlOmNvbGxhcHNlO2ZvbnQtc2l6ZToxNHB4O2xpbmUtaGVpZ2h0OjEuNTt3aWR0aDoxMDAlfS5mci1tb2RhbCAuZnItbW9kYWwtd3JhcHBlciAuZnItbW9kYWwtYm9keSAuZnItaGVscC1tb2RhbCB0YWJsZSt0YWJsZXttYXJnaW4tdG9wOjIwcHh9LmZyLW1vZGFsIC5mci1tb2RhbC13cmFwcGVyIC5mci1tb2RhbC1ib2R5IC5mci1oZWxwLW1vZGFsIHRhYmxlIHRye2JvcmRlcjowfS5mci1tb2RhbCAuZnItbW9kYWwtd3JhcHBlciAuZnItbW9kYWwtYm9keSAuZnItaGVscC1tb2RhbCB0YWJsZSB0aCwuZnItbW9kYWwgLmZyLW1vZGFsLXdyYXBwZXIgLmZyLW1vZGFsLWJvZHkgLmZyLWhlbHAtbW9kYWwgdGFibGUgdGR7cGFkZGluZzo2cHggMCA0cHh9LmZyLW1vZGFsIC5mci1tb2RhbC13cmFwcGVyIC5mci1tb2RhbC1ib2R5IC5mci1oZWxwLW1vZGFsIHRhYmxlIHRib2R5IHRye2JvcmRlci1ib3R0b206c29saWQgMXB4ICNlYmViZWJ9LmZyLW1vZGFsIC5mci1tb2RhbC13cmFwcGVyIC5mci1tb2RhbC1ib2R5IC5mci1oZWxwLW1vZGFsIHRhYmxlIHRib2R5IHRkOmZpcnN0LWNoaWxke3dpZHRoOjYwJTtjb2xvcjojNjQ2NDY0fS5mci1tb2RhbCAuZnItbW9kYWwtd3JhcHBlciAuZnItbW9kYWwtYm9keSAuZnItaGVscC1tb2RhbCB0YWJsZSB0Ym9keSB0ZDpudGgtY2hpbGQobisyKXtsZXR0ZXItc3BhY2luZzouNXB4fS5jbGVhcmZpeDo6YWZ0ZXJ7Y2xlYXI6Ym90aDtkaXNwbGF5OmJsb2NrO2NvbnRlbnQ6XCJcIjtoZWlnaHQ6MH0uaGlkZS1ieS1jbGlwcGluZ3twb3NpdGlvbjphYnNvbHV0ZTt3aWR0aDoxcHg7aGVpZ2h0OjFweDtwYWRkaW5nOjA7bWFyZ2luOi0xcHg7b3ZlcmZsb3c6aGlkZGVuO2NsaXA6cmVjdCgwLDAsMCwwKTtib3JkZXI6MH0uZnItZWxlbWVudCBpbWd7Y3Vyc29yOnBvaW50ZXJ9LmZyLWltYWdlLXJlc2l6ZXJ7cG9zaXRpb246YWJzb2x1dGU7Ym9yZGVyOnNvbGlkIDFweCAjMWU4OGU1O2Rpc3BsYXk6bm9uZTt1c2VyLXNlbGVjdDpub25lOy1vLXVzZXItc2VsZWN0Om5vbmU7LW1vei11c2VyLXNlbGVjdDpub25lOy1raHRtbC11c2VyLXNlbGVjdDpub25lOy13ZWJraXQtdXNlci1zZWxlY3Q6bm9uZTstbXMtdXNlci1zZWxlY3Q6bm9uZTstd2Via2l0LWJveC1zaXppbmc6Y29udGVudC1ib3g7LW1vei1ib3gtc2l6aW5nOmNvbnRlbnQtYm94O2JveC1zaXppbmc6Y29udGVudC1ib3h9LmZyLWltYWdlLXJlc2l6ZXIuZnItYWN0aXZle2Rpc3BsYXk6YmxvY2t9LmZyLWltYWdlLXJlc2l6ZXIgLmZyLWhhbmRsZXJ7ZGlzcGxheTpibG9jaztwb3NpdGlvbjphYnNvbHV0ZTtiYWNrZ3JvdW5kOiMxZTg4ZTU7Ym9yZGVyOnNvbGlkIDFweCAjZmZmO3otaW5kZXg6NDstd2Via2l0LWJveC1zaXppbmc6Ym9yZGVyLWJveDstbW96LWJveC1zaXppbmc6Ym9yZGVyLWJveDtib3gtc2l6aW5nOmJvcmRlci1ib3h9LmZyLWltYWdlLXJlc2l6ZXIgLmZyLWhhbmRsZXIuZnItaG53e2N1cnNvcjpudy1yZXNpemV9LmZyLWltYWdlLXJlc2l6ZXIgLmZyLWhhbmRsZXIuZnItaG5le2N1cnNvcjpuZS1yZXNpemV9LmZyLWltYWdlLXJlc2l6ZXIgLmZyLWhhbmRsZXIuZnItaHN3e2N1cnNvcjpzdy1yZXNpemV9LmZyLWltYWdlLXJlc2l6ZXIgLmZyLWhhbmRsZXIuZnItaHNle2N1cnNvcjpzZS1yZXNpemV9LmZyLWltYWdlLXJlc2l6ZXIgLmZyLWhhbmRsZXJ7d2lkdGg6MTJweDtoZWlnaHQ6MTJweH0uZnItaW1hZ2UtcmVzaXplciAuZnItaGFuZGxlci5mci1obnd7bGVmdDotNnB4O3RvcDotNnB4fS5mci1pbWFnZS1yZXNpemVyIC5mci1oYW5kbGVyLmZyLWhuZXtyaWdodDotNnB4O3RvcDotNnB4fS5mci1pbWFnZS1yZXNpemVyIC5mci1oYW5kbGVyLmZyLWhzd3tsZWZ0Oi02cHg7Ym90dG9tOi02cHh9LmZyLWltYWdlLXJlc2l6ZXIgLmZyLWhhbmRsZXIuZnItaHNle3JpZ2h0Oi02cHg7Ym90dG9tOi02cHh9QG1lZGlhIChtaW4td2lkdGg6MTIwMHB4KXsuZnItaW1hZ2UtcmVzaXplciAuZnItaGFuZGxlcnt3aWR0aDoxMHB4O2hlaWdodDoxMHB4fS5mci1pbWFnZS1yZXNpemVyIC5mci1oYW5kbGVyLmZyLWhud3tsZWZ0Oi01cHg7dG9wOi01cHh9LmZyLWltYWdlLXJlc2l6ZXIgLmZyLWhhbmRsZXIuZnItaG5le3JpZ2h0Oi01cHg7dG9wOi01cHh9LmZyLWltYWdlLXJlc2l6ZXIgLmZyLWhhbmRsZXIuZnItaHN3e2xlZnQ6LTVweDtib3R0b206LTVweH0uZnItaW1hZ2UtcmVzaXplciAuZnItaGFuZGxlci5mci1oc2V7cmlnaHQ6LTVweDtib3R0b206LTVweH19LmZyLWltYWdlLW92ZXJsYXl7cG9zaXRpb246Zml4ZWQ7dG9wOjA7bGVmdDowO2JvdHRvbTowO3JpZ2h0OjA7ei1pbmRleDoyMTQ3NDgzNjQwO2Rpc3BsYXk6bm9uZX0uZnItcG9wdXAgLmZyLWltYWdlLXVwbG9hZC1sYXllcntib3JkZXI6ZGFzaGVkIDJweCAjYmRiZGJkO3BhZGRpbmc6MjVweCAwO3Bvc2l0aW9uOnJlbGF0aXZlO2ZvbnQtc2l6ZToxNHB4O2xldHRlci1zcGFjaW5nOjFweDtsaW5lLWhlaWdodDoxNDAlO3RleHQtYWxpZ246Y2VudGVyfS5mci1wb3B1cCAuZnItaW1hZ2UtdXBsb2FkLWxheWVyOmhvdmVye2JhY2tncm91bmQ6I2ViZWJlYn0uZnItcG9wdXAgLmZyLWltYWdlLXVwbG9hZC1sYXllci5mci1kcm9we2JhY2tncm91bmQ6I2ViZWJlYjtib3JkZXItY29sb3I6IzFlODhlNX0uZnItcG9wdXAgLmZyLWltYWdlLXVwbG9hZC1sYXllciAuZnItZm9ybXstd2Via2l0LW9wYWNpdHk6MDstbW96LW9wYWNpdHk6MDtvcGFjaXR5OjA7LW1zLWZpbHRlcjpcImFscGhhKE9wYWNpdHk9MClcIjtwb3NpdGlvbjphYnNvbHV0ZTt0b3A6MDtib3R0b206MDtsZWZ0OjA7cmlnaHQ6MDt6LWluZGV4OjIxNDc0ODM2NDA7b3ZlcmZsb3c6aGlkZGVuO21hcmdpbjowIWltcG9ydGFudDtwYWRkaW5nOjAhaW1wb3J0YW50O3dpZHRoOjEwMCUhaW1wb3J0YW50fS5mci1wb3B1cCAuZnItaW1hZ2UtdXBsb2FkLWxheWVyIC5mci1mb3JtIGlucHV0e2N1cnNvcjpwb2ludGVyO3Bvc2l0aW9uOmFic29sdXRlO3JpZ2h0OjA7dG9wOjA7Ym90dG9tOjA7d2lkdGg6NTAwJTtoZWlnaHQ6MTAwJTttYXJnaW46MDtmb250LXNpemU6NDAwcHh9LmZyLXBvcHVwIC5mci1pbWFnZS1wcm9ncmVzcy1iYXItbGF5ZXI+aDN7Zm9udC1zaXplOjE2cHg7bWFyZ2luOjEwcHggMDtmb250LXdlaWdodDo0MDB9LmZyLXBvcHVwIC5mci1pbWFnZS1wcm9ncmVzcy1iYXItbGF5ZXI+ZGl2LmZyLWFjdGlvbi1idXR0b25ze2Rpc3BsYXk6bm9uZX0uZnItcG9wdXAgLmZyLWltYWdlLXByb2dyZXNzLWJhci1sYXllcj5kaXYuZnItbG9hZGVye2JhY2tncm91bmQ6I2JjZGJmNztoZWlnaHQ6MTBweDt3aWR0aDoxMDAlO21hcmdpbi10b3A6MjBweDtvdmVyZmxvdzpoaWRkZW47cG9zaXRpb246cmVsYXRpdmV9LmZyLXBvcHVwIC5mci1pbWFnZS1wcm9ncmVzcy1iYXItbGF5ZXI+ZGl2LmZyLWxvYWRlciBzcGFue2Rpc3BsYXk6YmxvY2s7aGVpZ2h0OjEwMCU7d2lkdGg6MDtiYWNrZ3JvdW5kOiMxZTg4ZTU7LXdlYmtpdC10cmFuc2l0aW9uOndpZHRoIC4ycyBlYXNlIDBzOy1tb3otdHJhbnNpdGlvbjp3aWR0aCAuMnMgZWFzZSAwczstbXMtdHJhbnNpdGlvbjp3aWR0aCAuMnMgZWFzZSAwczstby10cmFuc2l0aW9uOndpZHRoIC4ycyBlYXNlIDBzfS5mci1wb3B1cCAuZnItaW1hZ2UtcHJvZ3Jlc3MtYmFyLWxheWVyPmRpdi5mci1sb2FkZXIuZnItaW5kZXRlcm1pbmF0ZSBzcGFue3dpZHRoOjMwJSFpbXBvcnRhbnQ7cG9zaXRpb246YWJzb2x1dGU7dG9wOjA7LXdlYmtpdC1hbmltYXRpb246bG9hZGluZyAycyBsaW5lYXIgaW5maW5pdGU7LW1vei1hbmltYXRpb246bG9hZGluZyAycyBsaW5lYXIgaW5maW5pdGU7LW8tYW5pbWF0aW9uOmxvYWRpbmcgMnMgbGluZWFyIGluZmluaXRlO2FuaW1hdGlvbjpsb2FkaW5nIDJzIGxpbmVhciBpbmZpbml0ZX0uZnItcG9wdXAgLmZyLWltYWdlLXByb2dyZXNzLWJhci1sYXllci5mci1lcnJvcj5kaXYuZnItbG9hZGVye2Rpc3BsYXk6bm9uZX0uZnItcG9wdXAgLmZyLWltYWdlLXByb2dyZXNzLWJhci1sYXllci5mci1lcnJvcj5kaXYuZnItYWN0aW9uLWJ1dHRvbnN7ZGlzcGxheTpibG9ja30uZnItaW1hZ2Utc2l6ZS1sYXllciAuZnItaW1hZ2UtZ3JvdXAgLmZyLWlucHV0LWxpbmV7d2lkdGg6Y2FsYyg1MCUgLSA1cHgpO2Rpc3BsYXk6aW5saW5lLWJsb2NrfS5mci1pbWFnZS1zaXplLWxheWVyIC5mci1pbWFnZS1ncm91cCAuZnItaW5wdXQtbGluZSsuZnItaW5wdXQtbGluZXttYXJnaW4tbGVmdDoxMHB4fS5mci11cGxvYWRpbmd7LXdlYmtpdC1vcGFjaXR5Oi40Oy1tb3otb3BhY2l0eTouNDtvcGFjaXR5Oi40Oy1tcy1maWx0ZXI6XCJhbHBoYShPcGFjaXR5PTApXCJ9QGtleWZyYW1lcyBsb2FkaW5ne2Zyb217bGVmdDotMjUlfXRve2xlZnQ6MTAwJX19QC13ZWJraXQta2V5ZnJhbWVzIGxvYWRpbmd7ZnJvbXtsZWZ0Oi0yNSV9dG97bGVmdDoxMDAlfX1ALW1vei1rZXlmcmFtZXMgbG9hZGluZ3tmcm9te2xlZnQ6LTI1JX10b3tsZWZ0OjEwMCV9fUAtby1rZXlmcmFtZXMgbG9hZGluZ3tmcm9te2xlZnQ6LTI1JX10b3tsZWZ0OjEwMCV9fS5jbGVhcmZpeDo6YWZ0ZXJ7Y2xlYXI6Ym90aDtkaXNwbGF5OmJsb2NrO2NvbnRlbnQ6XCJcIjtoZWlnaHQ6MH0uaGlkZS1ieS1jbGlwcGluZ3twb3NpdGlvbjphYnNvbHV0ZTt3aWR0aDoxcHg7aGVpZ2h0OjFweDtwYWRkaW5nOjA7bWFyZ2luOi0xcHg7b3ZlcmZsb3c6aGlkZGVuO2NsaXA6cmVjdCgwLDAsMCwwKTtib3JkZXI6MH0uZnItbW9kYWwtaGVhZCAuZnItbW9kYWwtaGVhZC1saW5lOjphZnRlcntjbGVhcjpib3RoO2Rpc3BsYXk6YmxvY2s7Y29udGVudDpcIlwiO2hlaWdodDowfS5mci1tb2RhbC1oZWFkIC5mci1tb2RhbC1oZWFkLWxpbmUgaS5mci1tb2RhbC1tb3JlLC5mci1tb2RhbC1oZWFkIC5mci1tb2RhbC1oZWFkLWxpbmUgc3ZnLmZyLW1vZGFsLW1vcmV7ZmxvYXQ6bGVmdDtvcGFjaXR5OjE7LXdlYmtpdC10cmFuc2l0aW9uOnBhZGRpbmcgLjJzIGVhc2UgMHMsd2lkdGggLjJzIGVhc2UgMHMsb3BhY2l0eSAuMnMgZWFzZSAwczstbW96LXRyYW5zaXRpb246cGFkZGluZyAuMnMgZWFzZSAwcyx3aWR0aCAuMnMgZWFzZSAwcyxvcGFjaXR5IC4ycyBlYXNlIDBzOy1tcy10cmFuc2l0aW9uOnBhZGRpbmcgLjJzIGVhc2UgMHMsd2lkdGggLjJzIGVhc2UgMHMsb3BhY2l0eSAuMnMgZWFzZSAwczstby10cmFuc2l0aW9uOnBhZGRpbmcgLjJzIGVhc2UgMHMsd2lkdGggLjJzIGVhc2UgMHMsb3BhY2l0eSAuMnMgZWFzZSAwcztwYWRkaW5nOjEycHh9LmZyLW1vZGFsLWhlYWQgLmZyLW1vZGFsLWhlYWQtbGluZSBpLmZyLW1vZGFsLW1vcmUuZnItbm90LWF2YWlsYWJsZSwuZnItbW9kYWwtaGVhZCAuZnItbW9kYWwtaGVhZC1saW5lIHN2Zy5mci1tb2RhbC1tb3JlLmZyLW5vdC1hdmFpbGFibGV7b3BhY2l0eTowO3dpZHRoOjA7cGFkZGluZzoxMnB4IDB9LmZyLW1vZGFsLWhlYWQgLmZyLW1vZGFsLXRhZ3N7ZGlzcGxheTpub25lO3RleHQtYWxpZ246bGVmdH0uZnItbW9kYWwtaGVhZCAuZnItbW9kYWwtdGFncyBhe2Rpc3BsYXk6aW5saW5lLWJsb2NrO29wYWNpdHk6MDtwYWRkaW5nOjZweCA4cHg7bWFyZ2luOjhweCAwIDhweCA4cHg7dGV4dC1kZWNvcmF0aW9uOm5vbmU7Ym9yZGVyLXJhZGl1czoycHg7LW1vei1ib3JkZXItcmFkaXVzOjJweDstd2Via2l0LWJvcmRlci1yYWRpdXM6MnB4Oy1tb3otYmFja2dyb3VuZC1jbGlwOnBhZGRpbmc7LXdlYmtpdC1iYWNrZ3JvdW5kLWNsaXA6cGFkZGluZy1ib3g7YmFja2dyb3VuZC1jbGlwOnBhZGRpbmctYm94O2NvbG9yOiMxZTg4ZTU7LXdlYmtpdC10cmFuc2l0aW9uOm9wYWNpdHkgLjJzIGVhc2UgMHMsYmFja2dyb3VuZCAuMnMgZWFzZSAwczstbW96LXRyYW5zaXRpb246b3BhY2l0eSAuMnMgZWFzZSAwcyxiYWNrZ3JvdW5kIC4ycyBlYXNlIDBzOy1tcy10cmFuc2l0aW9uOm9wYWNpdHkgLjJzIGVhc2UgMHMsYmFja2dyb3VuZCAuMnMgZWFzZSAwczstby10cmFuc2l0aW9uOm9wYWNpdHkgLjJzIGVhc2UgMHMsYmFja2dyb3VuZCAuMnMgZWFzZSAwcztjdXJzb3I6cG9pbnRlcn0uZnItbW9kYWwtaGVhZCAuZnItbW9kYWwtdGFncyBhOmZvY3Vze291dGxpbmU6MH0uZnItbW9kYWwtaGVhZCAuZnItbW9kYWwtdGFncyBhLmZyLXNlbGVjdGVkLXRhZ3tiYWNrZ3JvdW5kOiNkNmQ2ZDZ9ZGl2LmZyLW1vZGFsLWJvZHkgLmZyLXByZWxvYWRlcntkaXNwbGF5OmJsb2NrO21hcmdpbjo1MHB4IGF1dG99ZGl2LmZyLW1vZGFsLWJvZHkgZGl2LmZyLWltYWdlLWxpc3R7dGV4dC1hbGlnbjpjZW50ZXI7bWFyZ2luOjAgMTBweDtwYWRkaW5nOjB9ZGl2LmZyLW1vZGFsLWJvZHkgZGl2LmZyLWltYWdlLWxpc3Q6OmFmdGVye2NsZWFyOmJvdGg7ZGlzcGxheTpibG9jaztjb250ZW50OlwiXCI7aGVpZ2h0OjB9ZGl2LmZyLW1vZGFsLWJvZHkgZGl2LmZyLWltYWdlLWxpc3QgLmZyLWxpc3QtY29sdW1ue2Zsb2F0OmxlZnQ7d2lkdGg6Y2FsYygoMTAwJSAtIDEwcHgpIC8gMil9QG1lZGlhIChtaW4td2lkdGg6NzY4cHgpIGFuZCAobWF4LXdpZHRoOjExOTlweCl7ZGl2LmZyLW1vZGFsLWJvZHkgZGl2LmZyLWltYWdlLWxpc3QgLmZyLWxpc3QtY29sdW1ue3dpZHRoOmNhbGMoKDEwMCUgLSAyMHB4KSAvIDMpfX1AbWVkaWEgKG1pbi13aWR0aDoxMjAwcHgpe2Rpdi5mci1tb2RhbC1ib2R5IGRpdi5mci1pbWFnZS1saXN0IC5mci1saXN0LWNvbHVtbnt3aWR0aDpjYWxjKCgxMDAlIC0gMzBweCkgLyA0KX19ZGl2LmZyLW1vZGFsLWJvZHkgZGl2LmZyLWltYWdlLWxpc3QgLmZyLWxpc3QtY29sdW1uKy5mci1saXN0LWNvbHVtbnttYXJnaW4tbGVmdDoxMHB4fWRpdi5mci1tb2RhbC1ib2R5IGRpdi5mci1pbWFnZS1saXN0IGRpdi5mci1pbWFnZS1jb250YWluZXJ7cG9zaXRpb246cmVsYXRpdmU7d2lkdGg6MTAwJTtkaXNwbGF5OmJsb2NrOy13ZWJraXQtYm94LXNoYWRvdzowIDFweCAzcHggcmdiYSgwLDAsMCwuMTIpLDAgMXB4IDFweCAxcHggcmdiYSgwLDAsMCwuMTYpOy1tb3otYm94LXNoYWRvdzowIDFweCAzcHggcmdiYSgwLDAsMCwuMTIpLDAgMXB4IDFweCAxcHggcmdiYSgwLDAsMCwuMTYpO2JveC1zaGFkb3c6MCAxcHggM3B4IHJnYmEoMCwwLDAsLjEyKSwwIDFweCAxcHggMXB4IHJnYmEoMCwwLDAsLjE2KTtib3JkZXItcmFkaXVzOjJweDstbW96LWJvcmRlci1yYWRpdXM6MnB4Oy13ZWJraXQtYm9yZGVyLXJhZGl1czoycHg7LW1vei1iYWNrZ3JvdW5kLWNsaXA6cGFkZGluZzstd2Via2l0LWJhY2tncm91bmQtY2xpcDpwYWRkaW5nLWJveDtiYWNrZ3JvdW5kLWNsaXA6cGFkZGluZy1ib3g7b3ZlcmZsb3c6aGlkZGVufWRpdi5mci1tb2RhbC1ib2R5IGRpdi5mci1pbWFnZS1saXN0IGRpdi5mci1pbWFnZS1jb250YWluZXI6Zmlyc3QtY2hpbGR7bWFyZ2luLXRvcDoxMHB4fWRpdi5mci1tb2RhbC1ib2R5IGRpdi5mci1pbWFnZS1saXN0IGRpdi5mci1pbWFnZS1jb250YWluZXIrZGl2e21hcmdpbi10b3A6MTBweH1kaXYuZnItbW9kYWwtYm9keSBkaXYuZnItaW1hZ2UtbGlzdCBkaXYuZnItaW1hZ2UtY29udGFpbmVyLmZyLWltYWdlLWRlbGV0aW5nOjphZnRlcntwb3NpdGlvbjphYnNvbHV0ZTstd2Via2l0LW9wYWNpdHk6LjU7LW1vei1vcGFjaXR5Oi41O29wYWNpdHk6LjU7LW1zLWZpbHRlcjpcImFscGhhKE9wYWNpdHk9MClcIjstd2Via2l0LXRyYW5zaXRpb246b3BhY2l0eSAuMnMgZWFzZSAwczstbW96LXRyYW5zaXRpb246b3BhY2l0eSAuMnMgZWFzZSAwczstbXMtdHJhbnNpdGlvbjpvcGFjaXR5IC4ycyBlYXNlIDBzOy1vLXRyYW5zaXRpb246b3BhY2l0eSAuMnMgZWFzZSAwcztiYWNrZ3JvdW5kOiMwMDA7Y29udGVudDpcIlwiO3RvcDowO2xlZnQ6MDtib3R0b206MDtyaWdodDowO3otaW5kZXg6Mn1kaXYuZnItbW9kYWwtYm9keSBkaXYuZnItaW1hZ2UtbGlzdCBkaXYuZnItaW1hZ2UtY29udGFpbmVyLmZyLWltYWdlLWRlbGV0aW5nOjpiZWZvcmV7Y29udGVudDphdHRyKGRhdGEtZGVsZXRpbmcpO2NvbG9yOiNmZmY7dG9wOjA7bGVmdDowO2JvdHRvbTowO3JpZ2h0OjA7bWFyZ2luOmF1dG87cG9zaXRpb246YWJzb2x1dGU7ei1pbmRleDozO2ZvbnQtc2l6ZToxNXB4O2hlaWdodDoyMHB4fWRpdi5mci1tb2RhbC1ib2R5IGRpdi5mci1pbWFnZS1saXN0IGRpdi5mci1pbWFnZS1jb250YWluZXIuZnItZW1wdHl7aGVpZ2h0Ojk1cHg7YmFja2dyb3VuZDojY2NjO3otaW5kZXg6MX1kaXYuZnItbW9kYWwtYm9keSBkaXYuZnItaW1hZ2UtbGlzdCBkaXYuZnItaW1hZ2UtY29udGFpbmVyLmZyLWVtcHR5OjphZnRlcntwb3NpdGlvbjphYnNvbHV0ZTttYXJnaW46YXV0bzt0b3A6MDtib3R0b206MDtsZWZ0OjA7cmlnaHQ6MDtjb250ZW50OmF0dHIoZGF0YS1sb2FkaW5nKTtkaXNwbGF5OmlubGluZS1ibG9jaztoZWlnaHQ6MjBweH1kaXYuZnItbW9kYWwtYm9keSBkaXYuZnItaW1hZ2UtbGlzdCBkaXYuZnItaW1hZ2UtY29udGFpbmVyIGltZ3t3aWR0aDoxMDAlO3ZlcnRpY2FsLWFsaWduOm1pZGRsZTtwb3NpdGlvbjpyZWxhdGl2ZTt6LWluZGV4OjI7LXdlYmtpdC1vcGFjaXR5OjE7LW1vei1vcGFjaXR5OjE7b3BhY2l0eToxOy1tcy1maWx0ZXI6XCJhbHBoYShPcGFjaXR5PTApXCI7LXdlYmtpdC10cmFuc2l0aW9uOm9wYWNpdHkgLjJzIGVhc2UgMHMsZmlsdGVyIC4ycyBlYXNlIDBzOy1tb3otdHJhbnNpdGlvbjpvcGFjaXR5IC4ycyBlYXNlIDBzLGZpbHRlciAuMnMgZWFzZSAwczstbXMtdHJhbnNpdGlvbjpvcGFjaXR5IC4ycyBlYXNlIDBzLGZpbHRlciAuMnMgZWFzZSAwczstby10cmFuc2l0aW9uOm9wYWNpdHkgLjJzIGVhc2UgMHMsZmlsdGVyIC4ycyBlYXNlIDBzOy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVooMCk7LW1vei10cmFuc2Zvcm06dHJhbnNsYXRlWigwKTstbXMtdHJhbnNmb3JtOnRyYW5zbGF0ZVooMCk7LW8tdHJhbnNmb3JtOnRyYW5zbGF0ZVooMCl9ZGl2LmZyLW1vZGFsLWJvZHkgZGl2LmZyLWltYWdlLWxpc3QgZGl2LmZyLWltYWdlLWNvbnRhaW5lci5mci1tb2JpbGUtc2VsZWN0ZWQgaW1ney13ZWJraXQtb3BhY2l0eTouNzU7LW1vei1vcGFjaXR5Oi43NTtvcGFjaXR5Oi43NTstbXMtZmlsdGVyOlwiYWxwaGEoT3BhY2l0eT0wKVwifWRpdi5mci1tb2RhbC1ib2R5IGRpdi5mci1pbWFnZS1saXN0IGRpdi5mci1pbWFnZS1jb250YWluZXIuZnItbW9iaWxlLXNlbGVjdGVkIC5mci1kZWxldGUtaW1nLGRpdi5mci1tb2RhbC1ib2R5IGRpdi5mci1pbWFnZS1saXN0IGRpdi5mci1pbWFnZS1jb250YWluZXIuZnItbW9iaWxlLXNlbGVjdGVkIC5mci1pbnNlcnQtaW1ne2Rpc3BsYXk6aW5saW5lLWJsb2NrfWRpdi5mci1tb2RhbC1ib2R5IGRpdi5mci1pbWFnZS1saXN0IGRpdi5mci1pbWFnZS1jb250YWluZXIgLmZyLWRlbGV0ZS1pbWcsZGl2LmZyLW1vZGFsLWJvZHkgZGl2LmZyLWltYWdlLWxpc3QgZGl2LmZyLWltYWdlLWNvbnRhaW5lciAuZnItaW5zZXJ0LWltZ3tkaXNwbGF5Om5vbmU7dG9wOjUwJTtib3JkZXItcmFkaXVzOjEwMCU7LW1vei1ib3JkZXItcmFkaXVzOjEwMCU7LXdlYmtpdC1ib3JkZXItcmFkaXVzOjEwMCU7LW1vei1iYWNrZ3JvdW5kLWNsaXA6cGFkZGluZzstd2Via2l0LWJhY2tncm91bmQtY2xpcDpwYWRkaW5nLWJveDtiYWNrZ3JvdW5kLWNsaXA6cGFkZGluZy1ib3g7LXdlYmtpdC10cmFuc2l0aW9uOmJhY2tncm91bmQgLjJzIGVhc2UgMHMsY29sb3IgLjJzIGVhc2UgMHM7LW1vei10cmFuc2l0aW9uOmJhY2tncm91bmQgLjJzIGVhc2UgMHMsY29sb3IgLjJzIGVhc2UgMHM7LW1zLXRyYW5zaXRpb246YmFja2dyb3VuZCAuMnMgZWFzZSAwcyxjb2xvciAuMnMgZWFzZSAwczstby10cmFuc2l0aW9uOmJhY2tncm91bmQgLjJzIGVhc2UgMHMsY29sb3IgLjJzIGVhc2UgMHM7LXdlYmtpdC1ib3gtc2hhZG93OjAgMXB4IDNweCByZ2JhKDAsMCwwLC4xMiksMCAxcHggMXB4IDFweCByZ2JhKDAsMCwwLC4xNik7LW1vei1ib3gtc2hhZG93OjAgMXB4IDNweCByZ2JhKDAsMCwwLC4xMiksMCAxcHggMXB4IDFweCByZ2JhKDAsMCwwLC4xNik7Ym94LXNoYWRvdzowIDFweCAzcHggcmdiYSgwLDAsMCwuMTIpLDAgMXB4IDFweCAxcHggcmdiYSgwLDAsMCwuMTYpO3Bvc2l0aW9uOmFic29sdXRlO2N1cnNvcjpwb2ludGVyO21hcmdpbjowO3dpZHRoOjM2cHg7aGVpZ2h0OjM2cHg7bGluZS1oZWlnaHQ6MzZweDt0ZXh0LWRlY29yYXRpb246bm9uZTt6LWluZGV4OjN9ZGl2LmZyLW1vZGFsLWJvZHkgZGl2LmZyLWltYWdlLWxpc3QgZGl2LmZyLWltYWdlLWNvbnRhaW5lciAuZnItZGVsZXRlLWltZ3tiYWNrZ3JvdW5kOiNiODMxMmY7Y29sb3I6I2ZmZjtsZWZ0OjUwJTstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVZKC01MCUpIHRyYW5zbGF0ZVgoMjUlKTstbW96LXRyYW5zZm9ybTp0cmFuc2xhdGVZKC01MCUpIHRyYW5zbGF0ZVgoMjUlKTstbXMtdHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTUwJSkgdHJhbnNsYXRlWCgyNSUpOy1vLXRyYW5zZm9ybTp0cmFuc2xhdGVZKC01MCUpIHRyYW5zbGF0ZVgoMjUlKX1kaXYuZnItbW9kYWwtYm9keSBkaXYuZnItaW1hZ2UtbGlzdCBkaXYuZnItaW1hZ2UtY29udGFpbmVyIC5mci1pbnNlcnQtaW1ne2JhY2tncm91bmQ6I2ZmZjtjb2xvcjojMWU4OGU1O2xlZnQ6NTAlOy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTUwJSkgdHJhbnNsYXRlWCgtMTI1JSk7LW1vei10cmFuc2Zvcm06dHJhbnNsYXRlWSgtNTAlKSB0cmFuc2xhdGVYKC0xMjUlKTstbXMtdHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTUwJSkgdHJhbnNsYXRlWCgtMTI1JSk7LW8tdHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTUwJSkgdHJhbnNsYXRlWCgtMTI1JSl9LmZyLWRlc2t0b3AgLmZyLW1vZGFsLXdyYXBwZXIgLmZyLW1vZGFsLWhlYWQgLmZyLW1vZGFsLXRhZ3MgYTpob3ZlcntiYWNrZ3JvdW5kOiNlYmViZWJ9LmZyLWRlc2t0b3AgLmZyLW1vZGFsLXdyYXBwZXIgLmZyLW1vZGFsLWhlYWQgLmZyLW1vZGFsLXRhZ3MgYS5mci1zZWxlY3RlZC10YWd7YmFja2dyb3VuZDojZDZkNmQ2fS5mci1kZXNrdG9wIC5mci1tb2RhbC13cmFwcGVyIGRpdi5mci1tb2RhbC1ib2R5IGRpdi5mci1pbWFnZS1saXN0IGRpdi5mci1pbWFnZS1jb250YWluZXI6aG92ZXIgaW1ney13ZWJraXQtb3BhY2l0eTouNzU7LW1vei1vcGFjaXR5Oi43NTtvcGFjaXR5Oi43NTstbXMtZmlsdGVyOlwiYWxwaGEoT3BhY2l0eT0wKVwifS5mci1kZXNrdG9wIC5mci1tb2RhbC13cmFwcGVyIGRpdi5mci1tb2RhbC1ib2R5IGRpdi5mci1pbWFnZS1saXN0IGRpdi5mci1pbWFnZS1jb250YWluZXI6aG92ZXIgLmZyLWRlbGV0ZS1pbWcsLmZyLWRlc2t0b3AgLmZyLW1vZGFsLXdyYXBwZXIgZGl2LmZyLW1vZGFsLWJvZHkgZGl2LmZyLWltYWdlLWxpc3QgZGl2LmZyLWltYWdlLWNvbnRhaW5lcjpob3ZlciAuZnItaW5zZXJ0LWltZ3tkaXNwbGF5OmlubGluZS1ibG9ja30uZnItZGVza3RvcCAuZnItbW9kYWwtd3JhcHBlciBkaXYuZnItbW9kYWwtYm9keSBkaXYuZnItaW1hZ2UtbGlzdCBkaXYuZnItaW1hZ2UtY29udGFpbmVyIC5mci1kZWxldGUtaW1nOmhvdmVye2JhY2tncm91bmQ6I2JmNDY0NDtjb2xvcjojZmZmfS5mci1kZXNrdG9wIC5mci1tb2RhbC13cmFwcGVyIGRpdi5mci1tb2RhbC1ib2R5IGRpdi5mci1pbWFnZS1saXN0IGRpdi5mci1pbWFnZS1jb250YWluZXIgLmZyLWluc2VydC1pbWc6aG92ZXJ7YmFja2dyb3VuZDojZWJlYmVifS5jbGVhcmZpeDo6YWZ0ZXJ7Y2xlYXI6Ym90aDtkaXNwbGF5OmJsb2NrO2NvbnRlbnQ6XCJcIjtoZWlnaHQ6MH0uaGlkZS1ieS1jbGlwcGluZ3twb3NpdGlvbjphYnNvbHV0ZTt3aWR0aDoxcHg7aGVpZ2h0OjFweDtwYWRkaW5nOjA7bWFyZ2luOi0xcHg7b3ZlcmZsb3c6aGlkZGVuO2NsaXA6cmVjdCgwLDAsMCwwKTtib3JkZXI6MH0uZnItbGluZS1icmVha2Vye2N1cnNvcjp0ZXh0O2JvcmRlci10b3A6MXB4IHNvbGlkICMxZTg4ZTU7cG9zaXRpb246Zml4ZWQ7ei1pbmRleDoyO2Rpc3BsYXk6bm9uZX0uZnItbGluZS1icmVha2VyLmZyLXZpc2libGV7ZGlzcGxheTpibG9ja30uZnItbGluZS1icmVha2VyIGEuZnItZmxvYXRpbmctYnRue3Bvc2l0aW9uOmFic29sdXRlO2xlZnQ6Y2FsYyg1MCUgLSAoMzJweCAvIDIpKTt0b3A6LTE2cHh9LmNsZWFyZml4OjphZnRlcntjbGVhcjpib3RoO2Rpc3BsYXk6YmxvY2s7Y29udGVudDpcIlwiO2hlaWdodDowfS5oaWRlLWJ5LWNsaXBwaW5ne3Bvc2l0aW9uOmFic29sdXRlO3dpZHRoOjFweDtoZWlnaHQ6MXB4O3BhZGRpbmc6MDttYXJnaW46LTFweDtvdmVyZmxvdzpoaWRkZW47Y2xpcDpyZWN0KDAsMCwwLDApO2JvcmRlcjowfS5mci1xdWljay1pbnNlcnR7cG9zaXRpb246YWJzb2x1dGU7ei1pbmRleDoyMTQ3NDgzNjM5O3doaXRlLXNwYWNlOm5vd3JhcDtwYWRkaW5nLXJpZ2h0OjVweDttYXJnaW4tbGVmdDotNXB4Oy13ZWJraXQtYm94LXNpemluZzpjb250ZW50LWJveDstbW96LWJveC1zaXppbmc6Y29udGVudC1ib3g7Ym94LXNpemluZzpjb250ZW50LWJveH0uZnItcXVpY2staW5zZXJ0LmZyLW9uIGEuZnItZmxvYXRpbmctYnRuIHN2Z3std2Via2l0LXRyYW5zZm9ybTpyb3RhdGUoMTM1ZGVnKTstbW96LXRyYW5zZm9ybTpyb3RhdGUoMTM1ZGVnKTstbXMtdHJhbnNmb3JtOnJvdGF0ZSgxMzVkZWcpOy1vLXRyYW5zZm9ybTpyb3RhdGUoMTM1ZGVnKX0uZnItcXVpY2staW5zZXJ0LmZyLWhpZGRlbntkaXNwbGF5Om5vbmV9LmZyLXFpLWhlbHBlcntwb3NpdGlvbjphYnNvbHV0ZTt6LWluZGV4OjM7cGFkZGluZy1sZWZ0OjE2cHg7d2hpdGUtc3BhY2U6bm93cmFwfS5mci1xaS1oZWxwZXIgYS5mci1idG4uZnItZmxvYXRpbmctYnRue3RleHQtYWxpZ246Y2VudGVyO2Rpc3BsYXk6aW5saW5lLWJsb2NrO2NvbG9yOiMyMjI7LXdlYmtpdC1vcGFjaXR5OjA7LW1vei1vcGFjaXR5OjA7b3BhY2l0eTowOy1tcy1maWx0ZXI6XCJhbHBoYShPcGFjaXR5PTApXCI7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGUoMCk7LW1vei10cmFuc2Zvcm06c2NhbGUoMCk7LW1zLXRyYW5zZm9ybTpzY2FsZSgwKTstby10cmFuc2Zvcm06c2NhbGUoMCl9LmZyLXFpLWhlbHBlciBhLmZyLWJ0bi5mci1mbG9hdGluZy1idG4uZnItc2l6ZS0xey13ZWJraXQtb3BhY2l0eToxOy1tb3otb3BhY2l0eToxO29wYWNpdHk6MTstbXMtZmlsdGVyOlwiYWxwaGEoT3BhY2l0eT0wKVwiOy13ZWJraXQtdHJhbnNmb3JtOnNjYWxlKDEpOy1tb3otdHJhbnNmb3JtOnNjYWxlKDEpOy1tcy10cmFuc2Zvcm06c2NhbGUoMSk7LW8tdHJhbnNmb3JtOnNjYWxlKDEpfS5jbGVhcmZpeDo6YWZ0ZXJ7Y2xlYXI6Ym90aDtkaXNwbGF5OmJsb2NrO2NvbnRlbnQ6XCJcIjtoZWlnaHQ6MH0uaGlkZS1ieS1jbGlwcGluZ3twb3NpdGlvbjphYnNvbHV0ZTt3aWR0aDoxcHg7aGVpZ2h0OjFweDtwYWRkaW5nOjA7bWFyZ2luOi0xcHg7b3ZlcmZsb3c6aGlkZGVuO2NsaXA6cmVjdCgwLDAsMCwwKTtib3JkZXI6MH0uZnItbW9kYWwgLmZyLW1vZGFsLXdyYXBwZXIgLmZyLW1vZGFsLWJvZHkgLmZyLXNwZWNpYWwtY2hhcmFjdGVycy1tb2RhbHt0ZXh0LWFsaWduOmxlZnQ7cGFkZGluZzoyMHB4IDIwcHggMTBweH0uZnItbW9kYWwgLmZyLW1vZGFsLXdyYXBwZXIgLmZyLW1vZGFsLWJvZHkgLmZyLXNwZWNpYWwtY2hhcmFjdGVycy1tb2RhbCAuZnItc3BlY2lhbC1jaGFyYWN0ZXJzLWxpc3R7bWFyZ2luLWJvdHRvbToyMHB4fS5mci1tb2RhbCAuZnItbW9kYWwtd3JhcHBlciAuZnItbW9kYWwtYm9keSAuZnItc3BlY2lhbC1jaGFyYWN0ZXJzLW1vZGFsIC5mci1zcGVjaWFsLWNoYXJhY3RlcnMtdGl0bGV7Zm9udC13ZWlnaHQ6NzAwO2ZvbnQtc2l6ZToxNHB4O3BhZGRpbmc6NnB4IDAgNHB4O21hcmdpbjowIDAgNXB4fS5mci1tb2RhbCAuZnItbW9kYWwtd3JhcHBlciAuZnItbW9kYWwtYm9keSAuZnItc3BlY2lhbC1jaGFyYWN0ZXJzLW1vZGFsIC5mci1zcGVjaWFsLWNoYXJhY3RlcntkaXNwbGF5OmlubGluZS1ibG9jaztmb250LXNpemU6MTZweDt3aWR0aDoyMHB4O2hlaWdodDoyMHB4O3BhZGRpbmc6NXB4O2xpbmUtaGVpZ2h0OjIwcHg7Y3Vyc29yOmRlZmF1bHQ7Zm9udC13ZWlnaHQ6NDAwOy13ZWJraXQtYm94LXNpemluZzpjb250ZW50LWJveDstbW96LWJveC1zaXppbmc6Y29udGVudC1ib3g7Ym94LXNpemluZzpjb250ZW50LWJveDt0ZXh0LWFsaWduOmNlbnRlcjtib3JkZXI6MXB4IHNvbGlkICNjY2M7bWFyZ2luOi0xcHggMCAwIC0xcHh9LmNsZWFyZml4OjphZnRlcntjbGVhcjpib3RoO2Rpc3BsYXk6YmxvY2s7Y29udGVudDpcIlwiO2hlaWdodDowfS5oaWRlLWJ5LWNsaXBwaW5ne3Bvc2l0aW9uOmFic29sdXRlO3dpZHRoOjFweDtoZWlnaHQ6MXB4O3BhZGRpbmc6MDttYXJnaW46LTFweDtvdmVyZmxvdzpoaWRkZW47Y2xpcDpyZWN0KDAsMCwwLDApO2JvcmRlcjowfS5mci1lbGVtZW50IHRhYmxlIHRkLmZyLXNlbGVjdGVkLWNlbGwsLmZyLWVsZW1lbnQgdGFibGUgdGguZnItc2VsZWN0ZWQtY2VsbHtib3JkZXI6MXB4IGRvdWJsZSAjMWU4OGU1fS5mci1lbGVtZW50IHRhYmxlIHRye3VzZXItc2VsZWN0Om5vbmU7LW8tdXNlci1zZWxlY3Q6bm9uZTstbW96LXVzZXItc2VsZWN0Om5vbmU7LWtodG1sLXVzZXItc2VsZWN0Om5vbmU7LXdlYmtpdC11c2VyLXNlbGVjdDpub25lOy1tcy11c2VyLXNlbGVjdDpub25lfS5mci1lbGVtZW50IHRhYmxlIHRkLC5mci1lbGVtZW50IHRhYmxlIHRoe3VzZXItc2VsZWN0OnRleHQ7LW8tdXNlci1zZWxlY3Q6dGV4dDstbW96LXVzZXItc2VsZWN0OnRleHQ7LWtodG1sLXVzZXItc2VsZWN0OnRleHQ7LXdlYmtpdC11c2VyLXNlbGVjdDp0ZXh0Oy1tcy11c2VyLXNlbGVjdDp0ZXh0fS5mci1lbGVtZW50IC5mci1uby1zZWxlY3Rpb24gdGFibGUgdGQsLmZyLWVsZW1lbnQgLmZyLW5vLXNlbGVjdGlvbiB0YWJsZSB0aHt1c2VyLXNlbGVjdDpub25lOy1vLXVzZXItc2VsZWN0Om5vbmU7LW1vei11c2VyLXNlbGVjdDpub25lOy1raHRtbC11c2VyLXNlbGVjdDpub25lOy13ZWJraXQtdXNlci1zZWxlY3Q6bm9uZTstbXMtdXNlci1zZWxlY3Q6bm9uZX0uZnItdGFibGUtcmVzaXplcntjdXJzb3I6Y29sLXJlc2l6ZTtwb3NpdGlvbjphYnNvbHV0ZTt6LWluZGV4OjM7ZGlzcGxheTpub25lfS5mci10YWJsZS1yZXNpemVyLmZyLW1vdmluZ3t6LWluZGV4OjJ9LmZyLXRhYmxlLXJlc2l6ZXIgZGl2ey13ZWJraXQtb3BhY2l0eTowOy1tb3otb3BhY2l0eTowO29wYWNpdHk6MDstbXMtZmlsdGVyOlwiYWxwaGEoT3BhY2l0eT0wKVwiO2JvcmRlci1yaWdodDoxcHggc29saWQgIzFlODhlNX0uZnItbm8tc2VsZWN0aW9ue3VzZXItc2VsZWN0Om5vbmU7LW8tdXNlci1zZWxlY3Q6bm9uZTstbW96LXVzZXItc2VsZWN0Om5vbmU7LWtodG1sLXVzZXItc2VsZWN0Om5vbmU7LXdlYmtpdC11c2VyLXNlbGVjdDpub25lOy1tcy11c2VyLXNlbGVjdDpub25lfS5mci1wb3B1cCAuZnItdGFibGUtY29sb3JzLWhleC1sYXllcnt3aWR0aDoxMDAlO21hcmdpbjowO3BhZGRpbmc6MTBweH0uZnItcG9wdXAgLmZyLXRhYmxlLWNvbG9ycy1oZXgtbGF5ZXIgLmZyLWlucHV0LWxpbmV7ZmxvYXQ6bGVmdDt3aWR0aDpjYWxjKDEwMCUgLSA1MHB4KTtwYWRkaW5nOjhweCAwIDB9LmZyLXBvcHVwIC5mci10YWJsZS1jb2xvcnMtaGV4LWxheWVyIC5mci1hY3Rpb24tYnV0dG9uc3tmbG9hdDpyaWdodDt3aWR0aDo1MHB4fS5mci1wb3B1cCAuZnItdGFibGUtY29sb3JzLWhleC1sYXllciAuZnItYWN0aW9uLWJ1dHRvbnMgYnV0dG9ue2JhY2tncm91bmQtY29sb3I6IzFlODhlNTtjb2xvcjojRkZGO2JvcmRlci1yYWRpdXM6MnB4Oy1tb3otYm9yZGVyLXJhZGl1czoycHg7LXdlYmtpdC1ib3JkZXItcmFkaXVzOjJweDstbW96LWJhY2tncm91bmQtY2xpcDpwYWRkaW5nOy13ZWJraXQtYmFja2dyb3VuZC1jbGlwOnBhZGRpbmctYm94O2JhY2tncm91bmQtY2xpcDpwYWRkaW5nLWJveDtmb250LXNpemU6MTNweDtoZWlnaHQ6MzJweH0uZnItcG9wdXAgLmZyLXRhYmxlLWNvbG9ycy1oZXgtbGF5ZXIgLmZyLWFjdGlvbi1idXR0b25zIGJ1dHRvbjpob3ZlcntiYWNrZ3JvdW5kLWNvbG9yOiMxNjZkYmE7Y29sb3I6I0ZGRn0uZnItcG9wdXAgLmZyLXRhYmxlLXNpemUgLmZyLXRhYmxlLXNpemUtaW5mb3t0ZXh0LWFsaWduOmNlbnRlcjtmb250LXNpemU6MTRweDtwYWRkaW5nOjhweH0uZnItcG9wdXAgLmZyLXRhYmxlLXNpemUgLmZyLXNlbGVjdC10YWJsZS1zaXple2xpbmUtaGVpZ2h0OjA7cGFkZGluZzowIDVweCA1cHg7d2hpdGUtc3BhY2U6bm93cmFwfS5mci1wb3B1cCAuZnItdGFibGUtc2l6ZSAuZnItc2VsZWN0LXRhYmxlLXNpemU+c3BhbntkaXNwbGF5OmlubGluZS1ibG9jaztwYWRkaW5nOjAgNHB4IDRweCAwO2JhY2tncm91bmQ6MCAwfS5mci1wb3B1cCAuZnItdGFibGUtc2l6ZSAuZnItc2VsZWN0LXRhYmxlLXNpemU+c3Bhbj5zcGFue2Rpc3BsYXk6aW5saW5lLWJsb2NrO3dpZHRoOjE4cHg7aGVpZ2h0OjE4cHg7Ym9yZGVyOjFweCBzb2xpZCAjZGRkfS5mci1wb3B1cCAuZnItdGFibGUtc2l6ZSAuZnItc2VsZWN0LXRhYmxlLXNpemU+c3Bhbi5ob3ZlcntiYWNrZ3JvdW5kOjAgMH0uZnItcG9wdXAgLmZyLXRhYmxlLXNpemUgLmZyLXNlbGVjdC10YWJsZS1zaXplPnNwYW4uaG92ZXI+c3BhbntiYWNrZ3JvdW5kOnJnYmEoMzAsMTM2LDIyOSwuMyk7Ym9yZGVyOnNvbGlkIDFweCAjMWU4OGU1fS5mci1wb3B1cCAuZnItdGFibGUtc2l6ZSAuZnItc2VsZWN0LXRhYmxlLXNpemUgLm5ldy1saW5lOjphZnRlcntjbGVhcjpib3RoO2Rpc3BsYXk6YmxvY2s7Y29udGVudDpcIlwiO2hlaWdodDowfS5mci1wb3B1cC5mci1hYm92ZSAuZnItdGFibGUtc2l6ZSAuZnItc2VsZWN0LXRhYmxlLXNpemU+c3BhbntkaXNwbGF5OmlubGluZS1ibG9jayFpbXBvcnRhbnR9LmZyLXBvcHVwIC5mci10YWJsZS1jb2xvcnMtYnV0dG9uc3ttYXJnaW4tYm90dG9tOjVweH0uZnItcG9wdXAgLmZyLXRhYmxlLWNvbG9yc3tsaW5lLWhlaWdodDowO2Rpc3BsYXk6YmxvY2t9LmZyLXBvcHVwIC5mci10YWJsZS1jb2xvcnM+c3BhbntkaXNwbGF5OmlubGluZS1ibG9jazt3aWR0aDozMnB4O2hlaWdodDozMnB4O3Bvc2l0aW9uOnJlbGF0aXZlO3otaW5kZXg6MX0uZnItcG9wdXAgLmZyLXRhYmxlLWNvbG9ycz5zcGFuPml7dGV4dC1hbGlnbjpjZW50ZXI7bGluZS1oZWlnaHQ6MzJweDtoZWlnaHQ6MzJweDt3aWR0aDozMnB4O2ZvbnQtc2l6ZToxM3B4O3Bvc2l0aW9uOmFic29sdXRlO2JvdHRvbTowO2N1cnNvcjpkZWZhdWx0O2xlZnQ6MH0uZnItcG9wdXAgLmZyLXRhYmxlLWNvbG9ycz5zcGFuOmZvY3Vze291dGxpbmU6MXB4IHNvbGlkICMyMjI7ei1pbmRleDoyfS5mci1wb3B1cC5mci1kZXNrdG9wIC5mci10YWJsZS1zaXplIC5mci1zZWxlY3QtdGFibGUtc2l6ZT5zcGFuPnNwYW57d2lkdGg6MTJweDtoZWlnaHQ6MTJweH0uZnItaW5zZXJ0LWhlbHBlcntwb3NpdGlvbjphYnNvbHV0ZTt6LWluZGV4Ojk5OTk7d2hpdGUtc3BhY2U6bm93cmFwfS5jbGVhcmZpeDo6YWZ0ZXJ7Y2xlYXI6Ym90aDtkaXNwbGF5OmJsb2NrO2NvbnRlbnQ6XCJcIjtoZWlnaHQ6MH0uaGlkZS1ieS1jbGlwcGluZ3twb3NpdGlvbjphYnNvbHV0ZTt3aWR0aDoxcHg7aGVpZ2h0OjFweDtwYWRkaW5nOjA7bWFyZ2luOi0xcHg7b3ZlcmZsb3c6aGlkZGVuO2NsaXA6cmVjdCgwLDAsMCwwKTtib3JkZXI6MH0uZnItZWxlbWVudCAuZnItdmlkZW97dXNlci1zZWxlY3Q6bm9uZTstby11c2VyLXNlbGVjdDpub25lOy1tb3otdXNlci1zZWxlY3Q6bm9uZTsta2h0bWwtdXNlci1zZWxlY3Q6bm9uZTstd2Via2l0LXVzZXItc2VsZWN0Om5vbmU7LW1zLXVzZXItc2VsZWN0Om5vbmV9LmZyLWVsZW1lbnQgLmZyLXZpZGVvOjphZnRlcntwb3NpdGlvbjphYnNvbHV0ZTtjb250ZW50OicnO3otaW5kZXg6MTt0b3A6MDtsZWZ0OjA7cmlnaHQ6MDtib3R0b206MDtjdXJzb3I6cG9pbnRlcjtkaXNwbGF5OmJsb2NrO2JhY2tncm91bmQ6cmdiYSgwLDAsMCwwKX0uZnItZWxlbWVudCAuZnItdmlkZW8uZnItYWN0aXZlPip7ei1pbmRleDoyO3Bvc2l0aW9uOnJlbGF0aXZlfS5mci1lbGVtZW50IC5mci12aWRlbz4qey13ZWJraXQtYm94LXNpemluZzpjb250ZW50LWJveDstbW96LWJveC1zaXppbmc6Y29udGVudC1ib3g7Ym94LXNpemluZzpjb250ZW50LWJveDttYXgtd2lkdGg6MTAwJTtib3JkZXI6MH0uZnItYm94IC5mci12aWRlby1yZXNpemVye3Bvc2l0aW9uOmFic29sdXRlO2JvcmRlcjpzb2xpZCAxcHggIzFlODhlNTtkaXNwbGF5Om5vbmU7dXNlci1zZWxlY3Q6bm9uZTstby11c2VyLXNlbGVjdDpub25lOy1tb3otdXNlci1zZWxlY3Q6bm9uZTsta2h0bWwtdXNlci1zZWxlY3Q6bm9uZTstd2Via2l0LXVzZXItc2VsZWN0Om5vbmU7LW1zLXVzZXItc2VsZWN0Om5vbmV9LmZyLWJveCAuZnItdmlkZW8tcmVzaXplci5mci1hY3RpdmV7ZGlzcGxheTpibG9ja30uZnItYm94IC5mci12aWRlby1yZXNpemVyIC5mci1oYW5kbGVye2Rpc3BsYXk6YmxvY2s7cG9zaXRpb246YWJzb2x1dGU7YmFja2dyb3VuZDojMWU4OGU1O2JvcmRlcjpzb2xpZCAxcHggI2ZmZjt6LWluZGV4OjQ7LXdlYmtpdC1ib3gtc2l6aW5nOmJvcmRlci1ib3g7LW1vei1ib3gtc2l6aW5nOmJvcmRlci1ib3g7Ym94LXNpemluZzpib3JkZXItYm94fS5mci1ib3ggLmZyLXZpZGVvLXJlc2l6ZXIgLmZyLWhhbmRsZXIuZnItaG53e2N1cnNvcjpudy1yZXNpemV9LmZyLWJveCAuZnItdmlkZW8tcmVzaXplciAuZnItaGFuZGxlci5mci1obmV7Y3Vyc29yOm5lLXJlc2l6ZX0uZnItYm94IC5mci12aWRlby1yZXNpemVyIC5mci1oYW5kbGVyLmZyLWhzd3tjdXJzb3I6c3ctcmVzaXplfS5mci1ib3ggLmZyLXZpZGVvLXJlc2l6ZXIgLmZyLWhhbmRsZXIuZnItaHNle2N1cnNvcjpzZS1yZXNpemV9LmZyLWJveCAuZnItdmlkZW8tcmVzaXplciAuZnItaGFuZGxlcnt3aWR0aDoxMnB4O2hlaWdodDoxMnB4fS5mci1ib3ggLmZyLXZpZGVvLXJlc2l6ZXIgLmZyLWhhbmRsZXIuZnItaG53e2xlZnQ6LTZweDt0b3A6LTZweH0uZnItYm94IC5mci12aWRlby1yZXNpemVyIC5mci1oYW5kbGVyLmZyLWhuZXtyaWdodDotNnB4O3RvcDotNnB4fS5mci1ib3ggLmZyLXZpZGVvLXJlc2l6ZXIgLmZyLWhhbmRsZXIuZnItaHN3e2xlZnQ6LTZweDtib3R0b206LTZweH0uZnItYm94IC5mci12aWRlby1yZXNpemVyIC5mci1oYW5kbGVyLmZyLWhzZXtyaWdodDotNnB4O2JvdHRvbTotNnB4fUBtZWRpYSAobWluLXdpZHRoOjEyMDBweCl7LmZyLWJveCAuZnItdmlkZW8tcmVzaXplciAuZnItaGFuZGxlcnt3aWR0aDoxMHB4O2hlaWdodDoxMHB4fS5mci1ib3ggLmZyLXZpZGVvLXJlc2l6ZXIgLmZyLWhhbmRsZXIuZnItaG53e2xlZnQ6LTVweDt0b3A6LTVweH0uZnItYm94IC5mci12aWRlby1yZXNpemVyIC5mci1oYW5kbGVyLmZyLWhuZXtyaWdodDotNXB4O3RvcDotNXB4fS5mci1ib3ggLmZyLXZpZGVvLXJlc2l6ZXIgLmZyLWhhbmRsZXIuZnItaHN3e2xlZnQ6LTVweDtib3R0b206LTVweH0uZnItYm94IC5mci12aWRlby1yZXNpemVyIC5mci1oYW5kbGVyLmZyLWhzZXtyaWdodDotNXB4O2JvdHRvbTotNXB4fX0uZnItcG9wdXAgLmZyLXZpZGVvLXNpemUtbGF5ZXIgLmZyLXZpZGVvLWdyb3VwIC5mci1pbnB1dC1saW5le3dpZHRoOmNhbGMoNTAlIC0gNXB4KTtkaXNwbGF5OmlubGluZS1ibG9ja30uZnItcG9wdXAgLmZyLXZpZGVvLXNpemUtbGF5ZXIgLmZyLXZpZGVvLWdyb3VwIC5mci1pbnB1dC1saW5lKy5mci1pbnB1dC1saW5le21hcmdpbi1sZWZ0OjEwcHh9LmZyLXBvcHVwIC5mci12aWRlby11cGxvYWQtbGF5ZXJ7Ym9yZGVyOmRhc2hlZCAycHggI2JkYmRiZDtwYWRkaW5nOjI1cHggMDtwb3NpdGlvbjpyZWxhdGl2ZTtmb250LXNpemU6MTRweDtsZXR0ZXItc3BhY2luZzoxcHg7bGluZS1oZWlnaHQ6MTQwJTt0ZXh0LWFsaWduOmNlbnRlcn0uZnItcG9wdXAgLmZyLXZpZGVvLXVwbG9hZC1sYXllcjpob3ZlcntiYWNrZ3JvdW5kOiNlYmViZWJ9LmZyLXBvcHVwIC5mci12aWRlby11cGxvYWQtbGF5ZXIuZnItZHJvcHtiYWNrZ3JvdW5kOiNlYmViZWI7Ym9yZGVyLWNvbG9yOiMxZTg4ZTV9LmZyLXBvcHVwIC5mci12aWRlby11cGxvYWQtbGF5ZXIgLmZyLWZvcm17LXdlYmtpdC1vcGFjaXR5OjA7LW1vei1vcGFjaXR5OjA7b3BhY2l0eTowOy1tcy1maWx0ZXI6XCJhbHBoYShPcGFjaXR5PTApXCI7cG9zaXRpb246YWJzb2x1dGU7dG9wOjA7Ym90dG9tOjA7bGVmdDowO3JpZ2h0OjA7ei1pbmRleDoyMTQ3NDgzNjQwO292ZXJmbG93OmhpZGRlbjttYXJnaW46MCFpbXBvcnRhbnQ7cGFkZGluZzowIWltcG9ydGFudDt3aWR0aDoxMDAlIWltcG9ydGFudH0uZnItcG9wdXAgLmZyLXZpZGVvLXVwbG9hZC1sYXllciAuZnItZm9ybSBpbnB1dHtjdXJzb3I6cG9pbnRlcjtwb3NpdGlvbjphYnNvbHV0ZTtyaWdodDowO3RvcDowO2JvdHRvbTowO3dpZHRoOjUwMCU7aGVpZ2h0OjEwMCU7bWFyZ2luOjA7Zm9udC1zaXplOjQwMHB4fS5mci1wb3B1cCAuZnItdmlkZW8tcHJvZ3Jlc3MtYmFyLWxheWVyPmgze2ZvbnQtc2l6ZToxNnB4O21hcmdpbjoxMHB4IDA7Zm9udC13ZWlnaHQ6NDAwfS5mci1wb3B1cCAuZnItdmlkZW8tcHJvZ3Jlc3MtYmFyLWxheWVyPmRpdi5mci1hY3Rpb24tYnV0dG9uc3tkaXNwbGF5Om5vbmV9LmZyLXBvcHVwIC5mci12aWRlby1wcm9ncmVzcy1iYXItbGF5ZXI+ZGl2LmZyLWxvYWRlcntiYWNrZ3JvdW5kOiNiY2RiZjc7aGVpZ2h0OjEwcHg7d2lkdGg6MTAwJTttYXJnaW4tdG9wOjIwcHg7b3ZlcmZsb3c6aGlkZGVuO3Bvc2l0aW9uOnJlbGF0aXZlfS5mci1wb3B1cCAuZnItdmlkZW8tcHJvZ3Jlc3MtYmFyLWxheWVyPmRpdi5mci1sb2FkZXIgc3BhbntkaXNwbGF5OmJsb2NrO2hlaWdodDoxMDAlO3dpZHRoOjA7YmFja2dyb3VuZDojMWU4OGU1Oy13ZWJraXQtdHJhbnNpdGlvbjp3aWR0aCAuMnMgZWFzZSAwczstbW96LXRyYW5zaXRpb246d2lkdGggLjJzIGVhc2UgMHM7LW1zLXRyYW5zaXRpb246d2lkdGggLjJzIGVhc2UgMHM7LW8tdHJhbnNpdGlvbjp3aWR0aCAuMnMgZWFzZSAwc30uZnItcG9wdXAgLmZyLXZpZGVvLXByb2dyZXNzLWJhci1sYXllcj5kaXYuZnItbG9hZGVyLmZyLWluZGV0ZXJtaW5hdGUgc3Bhbnt3aWR0aDozMCUhaW1wb3J0YW50O3Bvc2l0aW9uOmFic29sdXRlO3RvcDowOy13ZWJraXQtYW5pbWF0aW9uOmxvYWRpbmcgMnMgbGluZWFyIGluZmluaXRlOy1tb3otYW5pbWF0aW9uOmxvYWRpbmcgMnMgbGluZWFyIGluZmluaXRlOy1vLWFuaW1hdGlvbjpsb2FkaW5nIDJzIGxpbmVhciBpbmZpbml0ZTthbmltYXRpb246bG9hZGluZyAycyBsaW5lYXIgaW5maW5pdGV9LmZyLXBvcHVwIC5mci12aWRlby1wcm9ncmVzcy1iYXItbGF5ZXIuZnItZXJyb3I+ZGl2LmZyLWxvYWRlcntkaXNwbGF5Om5vbmV9LmZyLXBvcHVwIC5mci12aWRlby1wcm9ncmVzcy1iYXItbGF5ZXIuZnItZXJyb3I+ZGl2LmZyLWFjdGlvbi1idXR0b25ze2Rpc3BsYXk6YmxvY2t9LmZyLXZpZGVvLW92ZXJsYXl7cG9zaXRpb246Zml4ZWQ7dG9wOjA7bGVmdDowO2JvdHRvbTowO3JpZ2h0OjA7ei1pbmRleDoyMTQ3NDgzNjQwO2Rpc3BsYXk6bm9uZX0iXX0= *//* Slider */
.slick-slider {
  position: relative;
  display: block;
  box-sizing: border-box;
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  touch-action: pan-y;
  -webkit-tap-highlight-color: transparent; }
.slick-list {
  position: relative;
  overflow: hidden;
  display: block;
  margin: 0;
  padding: 0; }
.slick-list:focus {
    outline: none; }
.slick-list.dragging {
    cursor: pointer;
    cursor: hand; }
.slick-slider .slick-track,
.slick-slider .slick-list {
  -webkit-transform: translate3d(0, 0, 0);
  transform: translate3d(0, 0, 0); }
.slick-track {
  position: relative;
  left: 0;
  top: 0;
  display: block;
  margin-left: auto;
  margin-right: auto; }
.slick-track:before, .slick-track:after {
    content: &quot;&quot;;
    display: table; }
.slick-track:after {
    clear: both; }
.slick-loading .slick-track {
    visibility: hidden; }
.slick-slide {
  float: left;
  height: 100%;
  min-height: 1px;
  display: none; }
[dir=&quot;rtl&quot;] .slick-slide {
    float: right; }
.slick-slide img {
    display: block; }
.slick-slide.slick-loading img {
    display: none; }
.slick-slide.dragging img {
    pointer-events: none; }
.slick-initialized .slick-slide {
    display: block; }
.slick-loading .slick-slide {
    visibility: hidden; }
.slick-vertical .slick-slide {
    display: block;
    height: auto;
    border: 1px solid transparent; }
.slick-arrow.slick-hidden {
  display: none; }

/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9zbGljay1jYXJvdXNlbC9zbGljay9DOlxcVXNlcnNcXGFibGF5YmVsXFxEZXNrdG9wXFxEaWFzcG9yYSB2NFxcRGlhc3BvcmEgdjggMjAyMCAtIENvZGUgcHVzaC9ub2RlX21vZHVsZXNcXHNsaWNrLWNhcm91c2VsXFxzbGlja1xcc2xpY2suc2NzcyIsIm5vZGVfbW9kdWxlcy9zbGljay1jYXJvdXNlbC9zbGljay9zbGljay5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLFdBQUE7QUFFQTtFQUNJLGtCQUFrQjtFQUNsQixjQUFjO0VBQ2Qsc0JBQXNCO0VBQ3RCLDJCQUEyQjtFQUMzQix5QkFBeUI7RUFFekIsc0JBQXNCO0VBQ3RCLHFCQUFxQjtFQUNyQixpQkFBaUI7RUFFakIsbUJBQW1CO0VBQ25CLHdDQUF3QyxFQUFBO0FBRTVDO0VBQ0ksa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsU0FBUztFQUNULFVBQVUsRUFBQTtBQUxkO0lBUVEsYUFBYSxFQUFBO0FBUnJCO0lBWVEsZUFBZTtJQUNmLFlBQVksRUFBQTtBQUdwQjs7RUFFSSx1Q0FBdUM7RUFJdkMsK0JBQStCLEVBQUE7QUFHbkM7RUFDSSxrQkFBa0I7RUFDbEIsT0FBTztFQUNQLE1BQU07RUFDTixjQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLGtCQUFrQixFQUFBO0FBTnRCO0lBVVEsV0FBVztJQUNYLGNBQWMsRUFBQTtBQVh0QjtJQWVRLFdBQVcsRUFBQTtBQUdmO0lBQ0ksa0JBQWtCLEVBQUE7QUFHMUI7RUFDSSxXQUFXO0VBQ1gsWUFBWTtFQUNaLGVBQWU7RUFXZixhQUFhLEVBQUE7QUN0QmY7SURhTSxZQUFZLEVBQUE7QUFMcEI7SUFRUSxjQUFjLEVBQUE7QUFSdEI7SUFXUSxhQUFhLEVBQUE7QUFYckI7SUFpQlEsb0JBQW9CLEVBQUE7QUFHeEI7SUFDSSxjQUFjLEVBQUE7QUFHbEI7SUFDSSxrQkFBa0IsRUFBQTtBQUd0QjtJQUNJLGNBQWM7SUFDZCxZQUFZO0lBQ1osNkJBQTZCLEVBQUE7QUFHckM7RUFDSSxhQUFhLEVBQUEiLCJmaWxlIjoibm9kZV9tb2R1bGVzL3NsaWNrLWNhcm91c2VsL3NsaWNrL3NsaWNrLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiBTbGlkZXIgKi9cblxuLnNsaWNrLXNsaWRlciB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgLXdlYmtpdC10b3VjaC1jYWxsb3V0OiBub25lO1xuICAgIC13ZWJraXQtdXNlci1zZWxlY3Q6IG5vbmU7XG4gICAgLWtodG1sLXVzZXItc2VsZWN0OiBub25lO1xuICAgIC1tb3otdXNlci1zZWxlY3Q6IG5vbmU7XG4gICAgLW1zLXVzZXItc2VsZWN0OiBub25lO1xuICAgIHVzZXItc2VsZWN0OiBub25lO1xuICAgIC1tcy10b3VjaC1hY3Rpb246IHBhbi15O1xuICAgIHRvdWNoLWFjdGlvbjogcGFuLXk7XG4gICAgLXdlYmtpdC10YXAtaGlnaGxpZ2h0LWNvbG9yOiB0cmFuc3BhcmVudDtcbn1cbi5zbGljay1saXN0IHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBtYXJnaW46IDA7XG4gICAgcGFkZGluZzogMDtcblxuICAgICY6Zm9jdXMge1xuICAgICAgICBvdXRsaW5lOiBub25lO1xuICAgIH1cblxuICAgICYuZHJhZ2dpbmcge1xuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgICAgIGN1cnNvcjogaGFuZDtcbiAgICB9XG59XG4uc2xpY2stc2xpZGVyIC5zbGljay10cmFjayxcbi5zbGljay1zbGlkZXIgLnNsaWNrLWxpc3Qge1xuICAgIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGUzZCgwLCAwLCAwKTtcbiAgICAtbW96LXRyYW5zZm9ybTogdHJhbnNsYXRlM2QoMCwgMCwgMCk7XG4gICAgLW1zLXRyYW5zZm9ybTogdHJhbnNsYXRlM2QoMCwgMCwgMCk7XG4gICAgLW8tdHJhbnNmb3JtOiB0cmFuc2xhdGUzZCgwLCAwLCAwKTtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZTNkKDAsIDAsIDApO1xufVxuXG4uc2xpY2stdHJhY2sge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBsZWZ0OiAwO1xuICAgIHRvcDogMDtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG5cbiAgICAmOmJlZm9yZSxcbiAgICAmOmFmdGVyIHtcbiAgICAgICAgY29udGVudDogXCJcIjtcbiAgICAgICAgZGlzcGxheTogdGFibGU7XG4gICAgfVxuXG4gICAgJjphZnRlciB7XG4gICAgICAgIGNsZWFyOiBib3RoO1xuICAgIH1cblxuICAgIC5zbGljay1sb2FkaW5nICYge1xuICAgICAgICB2aXNpYmlsaXR5OiBoaWRkZW47XG4gICAgfVxufVxuLnNsaWNrLXNsaWRlIHtcbiAgICBmbG9hdDogbGVmdDtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgbWluLWhlaWdodDogMXB4O1xuICAgIFtkaXI9XCJydGxcIl0gJiB7XG4gICAgICAgIGZsb2F0OiByaWdodDtcbiAgICB9XG4gICAgaW1nIHtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgfVxuICAgICYuc2xpY2stbG9hZGluZyBpbWcge1xuICAgICAgICBkaXNwbGF5OiBub25lO1xuICAgIH1cblxuICAgIGRpc3BsYXk6IG5vbmU7XG5cbiAgICAmLmRyYWdnaW5nIGltZyB7XG4gICAgICAgIHBvaW50ZXItZXZlbnRzOiBub25lO1xuICAgIH1cblxuICAgIC5zbGljay1pbml0aWFsaXplZCAmIHtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgfVxuXG4gICAgLnNsaWNrLWxvYWRpbmcgJiB7XG4gICAgICAgIHZpc2liaWxpdHk6IGhpZGRlbjtcbiAgICB9XG5cbiAgICAuc2xpY2stdmVydGljYWwgJiB7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICBoZWlnaHQ6IGF1dG87XG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkIHRyYW5zcGFyZW50O1xuICAgIH1cbn1cbi5zbGljay1hcnJvdy5zbGljay1oaWRkZW4ge1xuICAgIGRpc3BsYXk6IG5vbmU7XG59XG4iLCIvKiBTbGlkZXIgKi9cbi5zbGljay1zbGlkZXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAtd2Via2l0LXRvdWNoLWNhbGxvdXQ6IG5vbmU7XG4gIC13ZWJraXQtdXNlci1zZWxlY3Q6IG5vbmU7XG4gIC1raHRtbC11c2VyLXNlbGVjdDogbm9uZTtcbiAgLW1vei11c2VyLXNlbGVjdDogbm9uZTtcbiAgLW1zLXVzZXItc2VsZWN0OiBub25lO1xuICB1c2VyLXNlbGVjdDogbm9uZTtcbiAgLW1zLXRvdWNoLWFjdGlvbjogcGFuLXk7XG4gIHRvdWNoLWFjdGlvbjogcGFuLXk7XG4gIC13ZWJraXQtdGFwLWhpZ2hsaWdodC1jb2xvcjogdHJhbnNwYXJlbnQ7IH1cblxuLnNsaWNrLWxpc3Qge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBtYXJnaW46IDA7XG4gIHBhZGRpbmc6IDA7IH1cbiAgLnNsaWNrLWxpc3Q6Zm9jdXMge1xuICAgIG91dGxpbmU6IG5vbmU7IH1cbiAgLnNsaWNrLWxpc3QuZHJhZ2dpbmcge1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBjdXJzb3I6IGhhbmQ7IH1cblxuLnNsaWNrLXNsaWRlciAuc2xpY2stdHJhY2ssXG4uc2xpY2stc2xpZGVyIC5zbGljay1saXN0IHtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZTNkKDAsIDAsIDApO1xuICAtbW96LXRyYW5zZm9ybTogdHJhbnNsYXRlM2QoMCwgMCwgMCk7XG4gIC1tcy10cmFuc2Zvcm06IHRyYW5zbGF0ZTNkKDAsIDAsIDApO1xuICAtby10cmFuc2Zvcm06IHRyYW5zbGF0ZTNkKDAsIDAsIDApO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZTNkKDAsIDAsIDApOyB9XG5cbi5zbGljay10cmFjayB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbGVmdDogMDtcbiAgdG9wOiAwO1xuICBkaXNwbGF5OiBibG9jaztcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gIG1hcmdpbi1yaWdodDogYXV0bzsgfVxuICAuc2xpY2stdHJhY2s6YmVmb3JlLCAuc2xpY2stdHJhY2s6YWZ0ZXIge1xuICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgZGlzcGxheTogdGFibGU7IH1cbiAgLnNsaWNrLXRyYWNrOmFmdGVyIHtcbiAgICBjbGVhcjogYm90aDsgfVxuICAuc2xpY2stbG9hZGluZyAuc2xpY2stdHJhY2sge1xuICAgIHZpc2liaWxpdHk6IGhpZGRlbjsgfVxuXG4uc2xpY2stc2xpZGUge1xuICBmbG9hdDogbGVmdDtcbiAgaGVpZ2h0OiAxMDAlO1xuICBtaW4taGVpZ2h0OiAxcHg7XG4gIGRpc3BsYXk6IG5vbmU7IH1cbiAgW2Rpcj1cInJ0bFwiXSAuc2xpY2stc2xpZGUge1xuICAgIGZsb2F0OiByaWdodDsgfVxuICAuc2xpY2stc2xpZGUgaW1nIHtcbiAgICBkaXNwbGF5OiBibG9jazsgfVxuICAuc2xpY2stc2xpZGUuc2xpY2stbG9hZGluZyBpbWcge1xuICAgIGRpc3BsYXk6IG5vbmU7IH1cbiAgLnNsaWNrLXNsaWRlLmRyYWdnaW5nIGltZyB7XG4gICAgcG9pbnRlci1ldmVudHM6IG5vbmU7IH1cbiAgLnNsaWNrLWluaXRpYWxpemVkIC5zbGljay1zbGlkZSB7XG4gICAgZGlzcGxheTogYmxvY2s7IH1cbiAgLnNsaWNrLWxvYWRpbmcgLnNsaWNrLXNsaWRlIHtcbiAgICB2aXNpYmlsaXR5OiBoaWRkZW47IH1cbiAgLnNsaWNrLXZlcnRpY2FsIC5zbGljay1zbGlkZSB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgaGVpZ2h0OiBhdXRvO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkIHRyYW5zcGFyZW50OyB9XG5cbi5zbGljay1hcnJvdy5zbGljay1oaWRkZW4ge1xuICBkaXNwbGF5OiBub25lOyB9XG4iXX0= */@charset &quot;UTF-8&quot;;
/* Slider */
.slick-loading .slick-list {
  background: #fff url('ajax-loader.gif') center center no-repeat; }
/* Icons */
@font-face {
  font-family: &quot;slick&quot;;
  src: url('slick.eot');
  src: url('slick.eot?#iefix') format(&quot;embedded-opentype&quot;), url('slick.woff') format(&quot;woff&quot;), url('slick.ttf') format(&quot;truetype&quot;), url('slick.svg#slick') format(&quot;svg&quot;);
  font-weight: normal;
  font-style: normal; }
/* Arrows */
.slick-prev,
.slick-next {
  position: absolute;
  display: block;
  height: 20px;
  width: 20px;
  line-height: 0px;
  font-size: 0px;
  cursor: pointer;
  background: transparent;
  color: transparent;
  top: 50%;
  -webkit-transform: translate(0, -50%);
  transform: translate(0, -50%);
  padding: 0;
  border: none;
  outline: none; }
.slick-prev:hover, .slick-prev:focus,
  .slick-next:hover,
  .slick-next:focus {
    outline: none;
    background: transparent;
    color: transparent; }
.slick-prev:hover:before, .slick-prev:focus:before,
    .slick-next:hover:before,
    .slick-next:focus:before {
      opacity: 1; }
.slick-prev.slick-disabled:before,
  .slick-next.slick-disabled:before {
    opacity: 0.25; }
.slick-prev:before,
  .slick-next:before {
    font-family: &quot;slick&quot;;
    font-size: 20px;
    line-height: 1;
    color: white;
    opacity: 0.75;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale; }
.slick-prev {
  left: -25px; }
[dir=&quot;rtl&quot;] .slick-prev {
    left: auto;
    right: -25px; }
.slick-prev:before {
    content: &quot;←&quot;; }
[dir=&quot;rtl&quot;] .slick-prev:before {
      content: &quot;→&quot;; }
.slick-next {
  right: -25px; }
[dir=&quot;rtl&quot;] .slick-next {
    left: -25px;
    right: auto; }
.slick-next:before {
    content: &quot;→&quot;; }
[dir=&quot;rtl&quot;] .slick-next:before {
      content: &quot;←&quot;; }
/* Dots */
.slick-dotted.slick-slider {
  margin-bottom: 30px; }
.slick-dots {
  position: absolute;
  bottom: -25px;
  list-style: none;
  display: block;
  text-align: center;
  padding: 0;
  margin: 0;
  width: 100%; }
.slick-dots li {
    position: relative;
    display: inline-block;
    height: 20px;
    width: 20px;
    margin: 0 5px;
    padding: 0;
    cursor: pointer; }
.slick-dots li button {
      border: 0;
      background: transparent;
      display: block;
      height: 20px;
      width: 20px;
      outline: none;
      line-height: 0px;
      font-size: 0px;
      color: transparent;
      padding: 5px;
      cursor: pointer; }
.slick-dots li button:hover, .slick-dots li button:focus {
        outline: none; }
.slick-dots li button:hover:before, .slick-dots li button:focus:before {
          opacity: 1; }
.slick-dots li button:before {
        position: absolute;
        top: 0;
        left: 0;
        content: &quot;•&quot;;
        width: 20px;
        height: 20px;
        font-family: &quot;slick&quot;;
        font-size: 6px;
        line-height: 20px;
        text-align: center;
        color: black;
        opacity: 0.25;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale; }
.slick-dots li.slick-active button:before {
      color: black;
      opacity: 0.75; }

/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9zbGljay1jYXJvdXNlbC9zbGljay9zbGljay10aGVtZS5zY3NzIiwibm9kZV9tb2R1bGVzL3NsaWNrLWNhcm91c2VsL3NsaWNrL0M6XFxVc2Vyc1xcYWJsYXliZWxcXERlc2t0b3BcXERpYXNwb3JhIHY0XFxEaWFzcG9yYSB2OCAyMDIwIC0gQ29kZSBwdXNoL25vZGVfbW9kdWxlc1xcc2xpY2stY2Fyb3VzZWxcXHNsaWNrXFxzbGljay10aGVtZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLGdCQUFnQjtBQ3lDaEIsV0FBQTtBQUdJO0VBQ0ksK0RBQTJFLEVBQUE7QUFJbkYsVUFBQTtBQUVJO0VBQ0ksb0JBQW9CO0VBQ3BCLHFCQWhCb0M7RUFpQnBDLHFLQUFpTjtFQUNqTixtQkFBbUI7RUFDbkIsa0JBQWtCLEVBQUE7QUFJMUIsV0FBQTtBQUVBOztFQUVJLGtCQUFrQjtFQUNsQixjQUFjO0VBQ2QsWUFBWTtFQUNaLFdBQVc7RUFDWCxnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLGVBQWU7RUFDZix1QkFBdUI7RUFDdkIsa0JBQWtCO0VBQ2xCLFFBQVE7RUFDUixxQ0FBcUM7RUFFckMsNkJBQTZCO0VBQzdCLFVBQVU7RUFDVixZQUFZO0VBQ1osYUFBYSxFQUFBO0FBakJqQjs7O0lBbUJRLGFBQWE7SUFDYix1QkFBdUI7SUFDdkIsa0JBQWtCLEVBQUE7QUFyQjFCOzs7TUF1QlksVUFqRWMsRUFBQTtBQTBDMUI7O0lBMkJRLGFBcEV1QixFQUFBO0FBeUMvQjs7SUE4QlEsb0JBbEZtQjtJQW1GbkIsZUFBZTtJQUNmLGNBQWM7SUFDZCxZQW5GaUI7SUFvRmpCLGFBN0VvQjtJQThFcEIsbUNBQW1DO0lBQ25DLGtDQUFrQyxFQUFBO0FBSTFDO0VBQ0ksV0FBVyxFQUFBO0FEOUNiO0lDZ0RNLFVBQVU7SUFDVixZQUFZLEVBQUE7QUFKcEI7SUFPUSxZQTlGZSxFQUFPO0FEK0MxQjtNQ2lEUSxZQS9GVyxFQUFPO0FBb0c5QjtFQUNJLFlBQVksRUFBQTtBRGxEZDtJQ29ETSxXQUFXO0lBQ1gsV0FBVyxFQUFBO0FBSm5CO0lBT1EsWUEzR2UsRUFBTztBRHdEMUI7TUNxRFEsWUE5R1csRUFBTztBQW1IOUIsU0FBQTtBQUVBO0VBQ0ksbUJBQW1CLEVBQUE7QUFHdkI7RUFDSSxrQkFBa0I7RUFDbEIsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLFVBQVU7RUFDVixTQUFTO0VBQ1QsV0FBVyxFQUFBO0FBUmY7SUFVUSxrQkFBa0I7SUFDbEIscUJBQXFCO0lBQ3JCLFlBQVk7SUFDWixXQUFXO0lBQ1gsYUFBYTtJQUNiLFVBQVU7SUFDVixlQUFlLEVBQUE7QUFoQnZCO01Ba0JZLFNBQVM7TUFDVCx1QkFBdUI7TUFDdkIsY0FBYztNQUNkLFlBQVk7TUFDWixXQUFXO01BQ1gsYUFBYTtNQUNiLGdCQUFnQjtNQUNoQixjQUFjO01BQ2Qsa0JBQWtCO01BQ2xCLFlBQVk7TUFDWixlQUFlLEVBQUE7QUE1QjNCO1FBOEJnQixhQUFhLEVBQUE7QUE5QjdCO1VBZ0NvQixVQXBKTSxFQUFBO0FBb0gxQjtRQW9DZ0Isa0JBQWtCO1FBQ2xCLE1BQU07UUFDTixPQUFPO1FBQ1AsWUE5Sk07UUErSk4sV0FBVztRQUNYLFlBQVk7UUFDWixvQkF4S1c7UUF5S1gsY0FqS0k7UUFrS0osaUJBQWlCO1FBQ2pCLGtCQUFrQjtRQUNsQixZQXpLTztRQTBLUCxhQWxLZTtRQW1LZixtQ0FBbUM7UUFDbkMsa0NBQWtDLEVBQUE7QUFqRGxEO01BcURZLFlBaExXO01BaUxYLGFBM0tnQixFQUFBIiwiZmlsZSI6Im5vZGVfbW9kdWxlcy9zbGljay1jYXJvdXNlbC9zbGljay9zbGljay10aGVtZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGNoYXJzZXQgXCJVVEYtOFwiO1xuLyogU2xpZGVyICovXG4uc2xpY2stbG9hZGluZyAuc2xpY2stbGlzdCB7XG4gIGJhY2tncm91bmQ6ICNmZmYgdXJsKFwiLi9hamF4LWxvYWRlci5naWZcIikgY2VudGVyIGNlbnRlciBuby1yZXBlYXQ7IH1cblxuLyogSWNvbnMgKi9cbkBmb250LWZhY2Uge1xuICBmb250LWZhbWlseTogXCJzbGlja1wiO1xuICBzcmM6IHVybChcIi4vZm9udHMvc2xpY2suZW90XCIpO1xuICBzcmM6IHVybChcIi4vZm9udHMvc2xpY2suZW90PyNpZWZpeFwiKSBmb3JtYXQoXCJlbWJlZGRlZC1vcGVudHlwZVwiKSwgdXJsKFwiLi9mb250cy9zbGljay53b2ZmXCIpIGZvcm1hdChcIndvZmZcIiksIHVybChcIi4vZm9udHMvc2xpY2sudHRmXCIpIGZvcm1hdChcInRydWV0eXBlXCIpLCB1cmwoXCIuL2ZvbnRzL3NsaWNrLnN2ZyNzbGlja1wiKSBmb3JtYXQoXCJzdmdcIik7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIGZvbnQtc3R5bGU6IG5vcm1hbDsgfVxuXG4vKiBBcnJvd3MgKi9cbi5zbGljay1wcmV2LFxuLnNsaWNrLW5leHQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBoZWlnaHQ6IDIwcHg7XG4gIHdpZHRoOiAyMHB4O1xuICBsaW5lLWhlaWdodDogMHB4O1xuICBmb250LXNpemU6IDBweDtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgY29sb3I6IHRyYW5zcGFyZW50O1xuICB0b3A6IDUwJTtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZSgwLCAtNTAlKTtcbiAgLW1zLXRyYW5zZm9ybTogdHJhbnNsYXRlKDAsIC01MCUpO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgwLCAtNTAlKTtcbiAgcGFkZGluZzogMDtcbiAgYm9yZGVyOiBub25lO1xuICBvdXRsaW5lOiBub25lOyB9XG4gIC5zbGljay1wcmV2OmhvdmVyLCAuc2xpY2stcHJldjpmb2N1cyxcbiAgLnNsaWNrLW5leHQ6aG92ZXIsXG4gIC5zbGljay1uZXh0OmZvY3VzIHtcbiAgICBvdXRsaW5lOiBub25lO1xuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgIGNvbG9yOiB0cmFuc3BhcmVudDsgfVxuICAgIC5zbGljay1wcmV2OmhvdmVyOmJlZm9yZSwgLnNsaWNrLXByZXY6Zm9jdXM6YmVmb3JlLFxuICAgIC5zbGljay1uZXh0OmhvdmVyOmJlZm9yZSxcbiAgICAuc2xpY2stbmV4dDpmb2N1czpiZWZvcmUge1xuICAgICAgb3BhY2l0eTogMTsgfVxuICAuc2xpY2stcHJldi5zbGljay1kaXNhYmxlZDpiZWZvcmUsXG4gIC5zbGljay1uZXh0LnNsaWNrLWRpc2FibGVkOmJlZm9yZSB7XG4gICAgb3BhY2l0eTogMC4yNTsgfVxuICAuc2xpY2stcHJldjpiZWZvcmUsXG4gIC5zbGljay1uZXh0OmJlZm9yZSB7XG4gICAgZm9udC1mYW1pbHk6IFwic2xpY2tcIjtcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgbGluZS1oZWlnaHQ6IDE7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIG9wYWNpdHk6IDAuNzU7XG4gICAgLXdlYmtpdC1mb250LXNtb290aGluZzogYW50aWFsaWFzZWQ7XG4gICAgLW1vei1vc3gtZm9udC1zbW9vdGhpbmc6IGdyYXlzY2FsZTsgfVxuXG4uc2xpY2stcHJldiB7XG4gIGxlZnQ6IC0yNXB4OyB9XG4gIFtkaXI9XCJydGxcIl0gLnNsaWNrLXByZXYge1xuICAgIGxlZnQ6IGF1dG87XG4gICAgcmlnaHQ6IC0yNXB4OyB9XG4gIC5zbGljay1wcmV2OmJlZm9yZSB7XG4gICAgY29udGVudDogXCLihpBcIjsgfVxuICAgIFtkaXI9XCJydGxcIl0gLnNsaWNrLXByZXY6YmVmb3JlIHtcbiAgICAgIGNvbnRlbnQ6IFwi4oaSXCI7IH1cblxuLnNsaWNrLW5leHQge1xuICByaWdodDogLTI1cHg7IH1cbiAgW2Rpcj1cInJ0bFwiXSAuc2xpY2stbmV4dCB7XG4gICAgbGVmdDogLTI1cHg7XG4gICAgcmlnaHQ6IGF1dG87IH1cbiAgLnNsaWNrLW5leHQ6YmVmb3JlIHtcbiAgICBjb250ZW50OiBcIuKGklwiOyB9XG4gICAgW2Rpcj1cInJ0bFwiXSAuc2xpY2stbmV4dDpiZWZvcmUge1xuICAgICAgY29udGVudDogXCLihpBcIjsgfVxuXG4vKiBEb3RzICovXG4uc2xpY2stZG90dGVkLnNsaWNrLXNsaWRlciB7XG4gIG1hcmdpbi1ib3R0b206IDMwcHg7IH1cblxuLnNsaWNrLWRvdHMge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvdHRvbTogLTI1cHg7XG4gIGxpc3Qtc3R5bGU6IG5vbmU7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBhZGRpbmc6IDA7XG4gIG1hcmdpbjogMDtcbiAgd2lkdGg6IDEwMCU7IH1cbiAgLnNsaWNrLWRvdHMgbGkge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgaGVpZ2h0OiAyMHB4O1xuICAgIHdpZHRoOiAyMHB4O1xuICAgIG1hcmdpbjogMCA1cHg7XG4gICAgcGFkZGluZzogMDtcbiAgICBjdXJzb3I6IHBvaW50ZXI7IH1cbiAgICAuc2xpY2stZG90cyBsaSBidXR0b24ge1xuICAgICAgYm9yZGVyOiAwO1xuICAgICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgIGhlaWdodDogMjBweDtcbiAgICAgIHdpZHRoOiAyMHB4O1xuICAgICAgb3V0bGluZTogbm9uZTtcbiAgICAgIGxpbmUtaGVpZ2h0OiAwcHg7XG4gICAgICBmb250LXNpemU6IDBweDtcbiAgICAgIGNvbG9yOiB0cmFuc3BhcmVudDtcbiAgICAgIHBhZGRpbmc6IDVweDtcbiAgICAgIGN1cnNvcjogcG9pbnRlcjsgfVxuICAgICAgLnNsaWNrLWRvdHMgbGkgYnV0dG9uOmhvdmVyLCAuc2xpY2stZG90cyBsaSBidXR0b246Zm9jdXMge1xuICAgICAgICBvdXRsaW5lOiBub25lOyB9XG4gICAgICAgIC5zbGljay1kb3RzIGxpIGJ1dHRvbjpob3ZlcjpiZWZvcmUsIC5zbGljay1kb3RzIGxpIGJ1dHRvbjpmb2N1czpiZWZvcmUge1xuICAgICAgICAgIG9wYWNpdHk6IDE7IH1cbiAgICAgIC5zbGljay1kb3RzIGxpIGJ1dHRvbjpiZWZvcmUge1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIHRvcDogMDtcbiAgICAgICAgbGVmdDogMDtcbiAgICAgICAgY29udGVudDogXCLigKJcIjtcbiAgICAgICAgd2lkdGg6IDIwcHg7XG4gICAgICAgIGhlaWdodDogMjBweDtcbiAgICAgICAgZm9udC1mYW1pbHk6IFwic2xpY2tcIjtcbiAgICAgICAgZm9udC1zaXplOiA2cHg7XG4gICAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIGNvbG9yOiBibGFjaztcbiAgICAgICAgb3BhY2l0eTogMC4yNTtcbiAgICAgICAgLXdlYmtpdC1mb250LXNtb290aGluZzogYW50aWFsaWFzZWQ7XG4gICAgICAgIC1tb3otb3N4LWZvbnQtc21vb3RoaW5nOiBncmF5c2NhbGU7IH1cbiAgICAuc2xpY2stZG90cyBsaS5zbGljay1hY3RpdmUgYnV0dG9uOmJlZm9yZSB7XG4gICAgICBjb2xvcjogYmxhY2s7XG4gICAgICBvcGFjaXR5OiAwLjc1OyB9XG4iLCJAY2hhcnNldCBcIlVURi04XCI7XG5cbi8vIERlZmF1bHQgVmFyaWFibGVzXG5cbi8vIFNsaWNrIGljb24gZW50aXR5IGNvZGVzIG91dHB1dHMgdGhlIGZvbGxvd2luZ1xuLy8gXCJcXDIxOTBcIiBvdXRwdXRzIGFzY2lpIGNoYXJhY3RlciBcIuKGkFwiXG4vLyBcIlxcMjE5MlwiIG91dHB1dHMgYXNjaWkgY2hhcmFjdGVyIFwi4oaSXCJcbi8vIFwiXFwyMDIyXCIgb3V0cHV0cyBhc2NpaSBjaGFyYWN0ZXIgXCLigKJcIlxuXG4kc2xpY2stZm9udC1wYXRoOiBcIi4vZm9udHMvXCIgIWRlZmF1bHQ7XG4kc2xpY2stZm9udC1mYW1pbHk6IFwic2xpY2tcIiAhZGVmYXVsdDtcbiRzbGljay1sb2FkZXItcGF0aDogXCIuL1wiICFkZWZhdWx0O1xuJHNsaWNrLWFycm93LWNvbG9yOiB3aGl0ZSAhZGVmYXVsdDtcbiRzbGljay1kb3QtY29sb3I6IGJsYWNrICFkZWZhdWx0O1xuJHNsaWNrLWRvdC1jb2xvci1hY3RpdmU6ICRzbGljay1kb3QtY29sb3IgIWRlZmF1bHQ7XG4kc2xpY2stcHJldi1jaGFyYWN0ZXI6IFwiXFwyMTkwXCIgIWRlZmF1bHQ7XG4kc2xpY2stbmV4dC1jaGFyYWN0ZXI6IFwiXFwyMTkyXCIgIWRlZmF1bHQ7XG4kc2xpY2stZG90LWNoYXJhY3RlcjogXCJcXDIwMjJcIiAhZGVmYXVsdDtcbiRzbGljay1kb3Qtc2l6ZTogNnB4ICFkZWZhdWx0O1xuJHNsaWNrLW9wYWNpdHktZGVmYXVsdDogMC43NSAhZGVmYXVsdDtcbiRzbGljay1vcGFjaXR5LW9uLWhvdmVyOiAxICFkZWZhdWx0O1xuJHNsaWNrLW9wYWNpdHktbm90LWFjdGl2ZTogMC4yNSAhZGVmYXVsdDtcblxuQGZ1bmN0aW9uIHNsaWNrLWltYWdlLXVybCgkdXJsKSB7XG4gICAgQGlmIGZ1bmN0aW9uLWV4aXN0cyhpbWFnZS11cmwpIHtcbiAgICAgICAgQHJldHVybiBpbWFnZS11cmwoJHVybCk7XG4gICAgfVxuICAgIEBlbHNlIHtcbiAgICAgICAgQHJldHVybiB1cmwoJHNsaWNrLWxvYWRlci1wYXRoICsgJHVybCk7XG4gICAgfVxufVxuXG5AZnVuY3Rpb24gc2xpY2stZm9udC11cmwoJHVybCkge1xuICAgIEBpZiBmdW5jdGlvbi1leGlzdHMoZm9udC11cmwpIHtcbiAgICAgICAgQHJldHVybiBmb250LXVybCgkdXJsKTtcbiAgICB9XG4gICAgQGVsc2Uge1xuICAgICAgICBAcmV0dXJuIHVybCgkc2xpY2stZm9udC1wYXRoICsgJHVybCk7XG4gICAgfVxufVxuXG4vKiBTbGlkZXIgKi9cblxuLnNsaWNrLWxpc3Qge1xuICAgIC5zbGljay1sb2FkaW5nICYge1xuICAgICAgICBiYWNrZ3JvdW5kOiAjZmZmIHNsaWNrLWltYWdlLXVybChcImFqYXgtbG9hZGVyLmdpZlwiKSBjZW50ZXIgY2VudGVyIG5vLXJlcGVhdDtcbiAgICB9XG59XG5cbi8qIEljb25zICovXG5AaWYgJHNsaWNrLWZvbnQtZmFtaWx5ID09IFwic2xpY2tcIiB7XG4gICAgQGZvbnQtZmFjZSB7XG4gICAgICAgIGZvbnQtZmFtaWx5OiBcInNsaWNrXCI7XG4gICAgICAgIHNyYzogc2xpY2stZm9udC11cmwoXCJzbGljay5lb3RcIik7XG4gICAgICAgIHNyYzogc2xpY2stZm9udC11cmwoXCJzbGljay5lb3Q/I2llZml4XCIpIGZvcm1hdChcImVtYmVkZGVkLW9wZW50eXBlXCIpLCBzbGljay1mb250LXVybChcInNsaWNrLndvZmZcIikgZm9ybWF0KFwid29mZlwiKSwgc2xpY2stZm9udC11cmwoXCJzbGljay50dGZcIikgZm9ybWF0KFwidHJ1ZXR5cGVcIiksIHNsaWNrLWZvbnQtdXJsKFwic2xpY2suc3ZnI3NsaWNrXCIpIGZvcm1hdChcInN2Z1wiKTtcbiAgICAgICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgICAgICAgZm9udC1zdHlsZTogbm9ybWFsO1xuICAgIH1cbn1cblxuLyogQXJyb3dzICovXG5cbi5zbGljay1wcmV2LFxuLnNsaWNrLW5leHQge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBoZWlnaHQ6IDIwcHg7XG4gICAgd2lkdGg6IDIwcHg7XG4gICAgbGluZS1oZWlnaHQ6IDBweDtcbiAgICBmb250LXNpemU6IDBweDtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgY29sb3I6IHRyYW5zcGFyZW50O1xuICAgIHRvcDogNTAlO1xuICAgIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGUoMCwgLTUwJSk7XG4gICAgLW1zLXRyYW5zZm9ybTogdHJhbnNsYXRlKDAsIC01MCUpO1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKDAsIC01MCUpO1xuICAgIHBhZGRpbmc6IDA7XG4gICAgYm9yZGVyOiBub25lO1xuICAgIG91dGxpbmU6IG5vbmU7XG4gICAgJjpob3ZlciwgJjpmb2N1cyB7XG4gICAgICAgIG91dGxpbmU6IG5vbmU7XG4gICAgICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgICAgICBjb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgICAgICY6YmVmb3JlIHtcbiAgICAgICAgICAgIG9wYWNpdHk6ICRzbGljay1vcGFjaXR5LW9uLWhvdmVyO1xuICAgICAgICB9XG4gICAgfVxuICAgICYuc2xpY2stZGlzYWJsZWQ6YmVmb3JlIHtcbiAgICAgICAgb3BhY2l0eTogJHNsaWNrLW9wYWNpdHktbm90LWFjdGl2ZTtcbiAgICB9XG4gICAgJjpiZWZvcmUge1xuICAgICAgICBmb250LWZhbWlseTogJHNsaWNrLWZvbnQtZmFtaWx5O1xuICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgIGxpbmUtaGVpZ2h0OiAxO1xuICAgICAgICBjb2xvcjogJHNsaWNrLWFycm93LWNvbG9yO1xuICAgICAgICBvcGFjaXR5OiAkc2xpY2stb3BhY2l0eS1kZWZhdWx0O1xuICAgICAgICAtd2Via2l0LWZvbnQtc21vb3RoaW5nOiBhbnRpYWxpYXNlZDtcbiAgICAgICAgLW1vei1vc3gtZm9udC1zbW9vdGhpbmc6IGdyYXlzY2FsZTtcbiAgICB9XG59XG5cbi5zbGljay1wcmV2IHtcbiAgICBsZWZ0OiAtMjVweDtcbiAgICBbZGlyPVwicnRsXCJdICYge1xuICAgICAgICBsZWZ0OiBhdXRvO1xuICAgICAgICByaWdodDogLTI1cHg7XG4gICAgfVxuICAgICY6YmVmb3JlIHtcbiAgICAgICAgY29udGVudDogJHNsaWNrLXByZXYtY2hhcmFjdGVyO1xuICAgICAgICBbZGlyPVwicnRsXCJdICYge1xuICAgICAgICAgICAgY29udGVudDogJHNsaWNrLW5leHQtY2hhcmFjdGVyO1xuICAgICAgICB9XG4gICAgfVxufVxuXG4uc2xpY2stbmV4dCB7XG4gICAgcmlnaHQ6IC0yNXB4O1xuICAgIFtkaXI9XCJydGxcIl0gJiB7XG4gICAgICAgIGxlZnQ6IC0yNXB4O1xuICAgICAgICByaWdodDogYXV0bztcbiAgICB9XG4gICAgJjpiZWZvcmUge1xuICAgICAgICBjb250ZW50OiAkc2xpY2stbmV4dC1jaGFyYWN0ZXI7XG4gICAgICAgIFtkaXI9XCJydGxcIl0gJiB7XG4gICAgICAgICAgICBjb250ZW50OiAkc2xpY2stcHJldi1jaGFyYWN0ZXI7XG4gICAgICAgIH1cbiAgICB9XG59XG5cbi8qIERvdHMgKi9cblxuLnNsaWNrLWRvdHRlZC5zbGljay1zbGlkZXIge1xuICAgIG1hcmdpbi1ib3R0b206IDMwcHg7XG59XG5cbi5zbGljay1kb3RzIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYm90dG9tOiAtMjVweDtcbiAgICBsaXN0LXN0eWxlOiBub25lO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBwYWRkaW5nOiAwO1xuICAgIG1hcmdpbjogMDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBsaSB7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgICAgICBoZWlnaHQ6IDIwcHg7XG4gICAgICAgIHdpZHRoOiAyMHB4O1xuICAgICAgICBtYXJnaW46IDAgNXB4O1xuICAgICAgICBwYWRkaW5nOiAwO1xuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgICAgIGJ1dHRvbiB7XG4gICAgICAgICAgICBib3JkZXI6IDA7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgaGVpZ2h0OiAyMHB4O1xuICAgICAgICAgICAgd2lkdGg6IDIwcHg7XG4gICAgICAgICAgICBvdXRsaW5lOiBub25lO1xuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDBweDtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMHB4O1xuICAgICAgICAgICAgY29sb3I6IHRyYW5zcGFyZW50O1xuICAgICAgICAgICAgcGFkZGluZzogNXB4O1xuICAgICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICAgICAgICAgJjpob3ZlciwgJjpmb2N1cyB7XG4gICAgICAgICAgICAgICAgb3V0bGluZTogbm9uZTtcbiAgICAgICAgICAgICAgICAmOmJlZm9yZSB7XG4gICAgICAgICAgICAgICAgICAgIG9wYWNpdHk6ICRzbGljay1vcGFjaXR5LW9uLWhvdmVyO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgICY6YmVmb3JlIHtcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICAgICAgdG9wOiAwO1xuICAgICAgICAgICAgICAgIGxlZnQ6IDA7XG4gICAgICAgICAgICAgICAgY29udGVudDogJHNsaWNrLWRvdC1jaGFyYWN0ZXI7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDIwcHg7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAyMHB4O1xuICAgICAgICAgICAgICAgIGZvbnQtZmFtaWx5OiAkc2xpY2stZm9udC1mYW1pbHk7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAkc2xpY2stZG90LXNpemU7XG4gICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgICAgICAgIGNvbG9yOiAkc2xpY2stZG90LWNvbG9yO1xuICAgICAgICAgICAgICAgIG9wYWNpdHk6ICRzbGljay1vcGFjaXR5LW5vdC1hY3RpdmU7XG4gICAgICAgICAgICAgICAgLXdlYmtpdC1mb250LXNtb290aGluZzogYW50aWFsaWFzZWQ7XG4gICAgICAgICAgICAgICAgLW1vei1vc3gtZm9udC1zbW9vdGhpbmc6IGdyYXlzY2FsZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAmLnNsaWNrLWFjdGl2ZSBidXR0b246YmVmb3JlIHtcbiAgICAgICAgICAgIGNvbG9yOiAkc2xpY2stZG90LWNvbG9yLWFjdGl2ZTtcbiAgICAgICAgICAgIG9wYWNpdHk6ICRzbGljay1vcGFjaXR5LWRlZmF1bHQ7XG4gICAgICAgIH1cbiAgICB9XG59XG4iXX0= */[_nghost-csx-c0] {
      display: flex;
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;

      flex-direction: column;

      width: 100%;
      height: 100%;

      contain: layout size style;
      z-index: $z-index-page-container;
    }
    .tabs-inner[_ngcontent-csx-c0] {
      position: relative;

      flex: 1;

      contain: layout size style;
    }ion-content[_ngcontent-csx-c1] {
  --background: #f0f0f0; }

ion-slides.center-slide[_ngcontent-csx-c1] {
  overflow: visible; }
/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGFicy1wYWdlL2hvbWUvQzpcXFVzZXJzXFxhYmxheWJlbFxcRGVza3RvcFxcRGlhc3BvcmEgdjRcXERpYXNwb3JhIHY4IDIwMjAgLSBDb2RlIHB1c2gvc3JjXFxhcHBcXHBhZ2VzXFx0YWJzLXBhZ2VcXGhvbWVcXGhvbWUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kscUJBQWEsRUFBQTs7QUFFakI7RUFFSSxpQkFBaUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3RhYnMtcGFnZS9ob21lL2hvbWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjZjBmMGYwO1xyXG59XHJcbmlvbi1zbGlkZXMge1xyXG4gICAmLmNlbnRlci1zbGlkZSB7XHJcbiAgICBvdmVyZmxvdzogdmlzaWJsZTtcclxuICAgfVxyXG59XHJcblxyXG4iXX0= */svg[_ngcontent-csx-c3] {
  position: absolute; }

.filter[_ngcontent-csx-c3] {
  -webkit-filter: url(#duotone);
          filter: url(#duotone); }
/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9lbnRpdHktY29tcG9uZW50cy9lbnRpdHktY29tbW9uLWhlYWRlci9DOlxcVXNlcnNcXGFibGF5YmVsXFxEZXNrdG9wXFxEaWFzcG9yYSB2NFxcRGlhc3BvcmEgdjggMjAyMCAtIENvZGUgcHVzaC9zcmNcXGFwcFxcY29tcG9uZW50c1xcZW50aXR5LWNvbXBvbmVudHNcXGVudGl0eS1jb21tb24taGVhZGVyXFxlbnRpdHktY29tbW9uLWhlYWRlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFrQixFQUFBOztBQUV0QjtFQUNJLDZCQUFvQjtVQUFwQixxQkFBb0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZW50aXR5LWNvbXBvbmVudHMvZW50aXR5LWNvbW1vbi1oZWFkZXIvZW50aXR5LWNvbW1vbi1oZWFkZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJzdmcge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG59XHJcbi5maWx0ZXIge1xyXG4gICAgZmlsdGVyOnVybCgjZHVvdG9uZSk7XHJcbn0iXX0= */
/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvc2tlbGV0b24vc2tlbGV0b24uY29tcG9uZW50LnNjc3MifQ== */
/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY2FyZC1jb21wb25lbnRzL3BhZ2UtY2FyZC9wYWdlLWNhcmQuY29tcG9uZW50LnNjc3MifQ== */[_nghost-csx-c12] {
        border-radius: '50%';
      }
/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvc2VjdGlvbi1jb21wb25lbnRzL3NlY3Rpb24tdGl0bGUvc2VjdGlvbi10aXRsZS5jb21wb25lbnQuc2NzcyJ9 */
/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY2FyZC1jb21wb25lbnRzL21haW4tY2FyZC9tYWluLWNhcmQuY29tcG9uZW50LnNjc3MifQ== */
/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY2FyZC1jb21wb25lbnRzL2xpc3QtY2FyZC1yaWdodC1pbWFnZS9saXN0LWNhcmQtcmlnaHQtaW1hZ2UuY29tcG9uZW50LnNjc3MifQ== */
/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY2FyZC1jb21wb25lbnRzL2NhcmQtc2VhcmNoL2NhcmQtc2VhcmNoLmNvbXBvbmVudC5zY3NzIn0= */
/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvc2VjdGlvbi1jb21wb25lbnRzL3NlY3Rpb24tZXhwbG9yZS9zZWN0aW9uLWV4cGxvcmUuY29tcG9uZW50LnNjc3MifQ== */
/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvcmUvYXBwLWJ1dHRvbi9hcHAtYnV0dG9uLmNvbXBvbmVudC5zY3NzIn0= */#katalon{font-family:monospace;font-size:13px;background-color:rgba(0,0,0,.7);position:fixed;top:0;left:0;right:0;display:block;z-index:999999999;line-height: normal} #katalon div{padding:0;margin:0;color:#fff;} #katalon kbd{display:inline-block;padding:3px 5px;font:13px Consolas,&quot;Liberation Mono&quot;,Menlo,Courier,monospace;line-height:10px;color:#555;vertical-align:middle;background-color:#fcfcfc;border:1px solid #ccc;border-bottom-color:#bbb;border-radius:3px;box-shadow:inset 0 -1px 0 #bbb;font-weight: bold} div#katalon-spy_elementInfoDiv {color: lightblue; padding: 0px 5px 5px} div#katalon-spy_instructionDiv {padding: 5px 5px 2.5px}ion-content[_ngcontent-csx-c14] {
  --background: #f0f0f0;
  --padding-bottom: 10px; }
/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGFicy1wYWdlL3Byb2ZpbGUvQzpcXFVzZXJzXFxhYmxheWJlbFxcRGVza3RvcFxcRGlhc3BvcmEgdjRcXERpYXNwb3JhIHY4IDIwMjAgLSBDb2RlIHB1c2gvc3JjXFxhcHBcXHBhZ2VzXFx0YWJzLXBhZ2VcXHByb2ZpbGVcXHByb2ZpbGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kscUJBQWE7RUFDYixzQkFBaUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3RhYnMtcGFnZS9wcm9maWxlL3Byb2ZpbGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjZjBmMGYwO1xyXG4gICAgLS1wYWRkaW5nLWJvdHRvbTogMTBweDtcclxufSJdfQ== */
/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZW50aXR5LWNvbXBvbmVudHMvZW50aXR5LWRldGFpbHMtaW5mby9lbnRpdHktZGV0YWlscy1pbmZvLmNvbXBvbmVudC5zY3NzIn0= */.show-more-link[_ngcontent-csx-c20] {
  font-size: 13px;
  margin: 3px 0 0 10px;
  color: #00BDA5; }

.truncatedTextHolder[_ngcontent-csx-c20] {
  font-size: 14px; }
/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29yZS9hcHAtdHVuY2F0ZS10ZXh0L0M6XFxVc2Vyc1xcYWJsYXliZWxcXERlc2t0b3BcXERpYXNwb3JhIHY0XFxEaWFzcG9yYSB2OCAyMDIwIC0gQ29kZSBwdXNoL3NyY1xcYXBwXFxjb3JlXFxhcHAtdHVuY2F0ZS10ZXh0XFxhcHAtdHVuY2F0ZS10ZXh0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZUFBZTtFQUNmLG9CQUFvQjtFQUNwQixjQUFjLEVBQUE7O0FBRWxCO0VBQ0ksZUFDSixFQUFBIiwiZmlsZSI6InNyYy9hcHAvY29yZS9hcHAtdHVuY2F0ZS10ZXh0L2FwcC10dW5jYXRlLXRleHQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc2hvdy1tb3JlLWxpbmsgIHtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgIG1hcmdpbjogM3B4IDAgMCAxMHB4O1xyXG4gICAgY29sb3I6ICMwMEJEQTU7XHJcbn1cclxuLnRydW5jYXRlZFRleHRIb2xkZXIge1xyXG4gICAgZm9udC1zaXplIDogMTRweFxyXG59Il19 */
/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZW1wdHktc3RhdGVzL2VtcHR5LXN0YXRlcy5jb21wb25lbnQuc2NzcyJ9 */
/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY2FyZC1jb21wb25lbnRzL2xpc3QtY2FyZC9saXN0LWNhcmQuY29tcG9uZW50LnNjc3MifQ== */
/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYm90dG9tLWFjdGlvbi9ib3R0b20tYWN0aW9uLmNvbXBvbmVudC5zY3NzIn0= */
/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FjdGlvbi1wYWdlcy9zdGF0dXMvc3RhdHVzLnBhZ2Uuc2NzcyJ9 */
/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvcmUvYXBwLWJhci9hcHAtYmFyLmNvbXBvbmVudC5zY3NzIn0= */
/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvcmUvYXBwLWlucHV0L2FwcC1pbnB1dC5jb21wb25lbnQuc2NzcyJ9 */
/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvcmUvYXBwLWVkaXRvci9hcHAtZWRpdG9yLmNvbXBvbmVudC5zY3NzIn0= */.no-scroll-content[_ngcontent-csx-c25] {
  --overfolow: hidden
; }
/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9tb2RhbC9jYW1lcmEvQzpcXFVzZXJzXFxhYmxheWJlbFxcRGVza3RvcFxcRGlhc3BvcmEgdjRcXERpYXNwb3JhIHY4IDIwMjAgLSBDb2RlIHB1c2gvc3JjXFxhcHBcXGNvbXBvbmVudHNcXG1vZGFsXFxjYW1lcmFcXGNhbWVyYS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJO0FBQWEsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbW9kYWwvY2FtZXJhL2NhbWVyYS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5uby1zY3JvbGwtY29udGVudCB7XHJcbiAgICAtLW92ZXJmb2xvdyA6IGhpZGRlblxyXG59Il19 */ion-content[_ngcontent-csx-c26] {
  --background: #000000; }

.cropper-container[_ngcontent-csx-c26] {
  display: -webkit-box;
  display: flex;
  -webkit-box-align: center;
          align-items: center;
  height: calc(100% - 61px); }
/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9tb2RhbC9pbWFnZS1jcm9wcGVyL0M6XFxVc2Vyc1xcYWJsYXliZWxcXERlc2t0b3BcXERpYXNwb3JhIHY0XFxEaWFzcG9yYSB2OCAyMDIwIC0gQ29kZSBwdXNoL3NyY1xcYXBwXFxjb21wb25lbnRzXFxtb2RhbFxcaW1hZ2UtY3JvcHBlclxcaW1hZ2UtY3JvcHBlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHFCQUFhLEVBQUE7O0FBRWpCO0VBQ0ksb0JBQWE7RUFBYixhQUFhO0VBQ2IseUJBQW1CO1VBQW5CLG1CQUFtQjtFQUNuQix5QkFBeUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbW9kYWwvaW1hZ2UtY3JvcHBlci9pbWFnZS1jcm9wcGVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjMDAwMDAwO1xyXG59XHJcbi5jcm9wcGVyLWNvbnRhaW5lciB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGhlaWdodDogY2FsYygxMDAlIC0gNjFweCk7XHJcbn0iXX0= */:host{display:block}.cropper img{max-width:100%;max-height:100%;height:auto}.cropper-wrapper{position:relative;min-height:80px}.cropper-wrapper .loading-block{position:absolute;top:0;left:0;width:100%;height:100%}.cropper-wrapper .loading-block .spinner{width:31px;height:31px;margin:0 auto;border:2px solid rgba(97,100,193,.98);border-radius:50%;border-left-color:transparent;border-right-color:transparent;-webkit-animation:425ms linear infinite cssload-spin;position:absolute;top:calc(50% - 15px);left:calc(50% - 15px);animation:425ms linear infinite cssload-spin}@-webkit-keyframes cssload-spin{to{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}@keyframes cssload-spin{to{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}.cropper-container{direction:ltr;font-size:0;line-height:0;position:relative;touch-action:none;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.cropper-container img{display:block;height:100%;image-orientation:0deg;max-height:none!important;max-width:none!important;min-height:0!important;min-width:0!important;width:100%}.cropper-canvas,.cropper-crop-box,.cropper-drag-box,.cropper-modal,.cropper-wrap-box{bottom:0;left:0;position:absolute;right:0;top:0}.cropper-canvas,.cropper-wrap-box{overflow:hidden}.cropper-drag-box{background-color:#fff;opacity:0}.cropper-modal{background-color:#000;opacity:.5}.cropper-view-box{display:block;height:100%;outline:#39f solid 1px;overflow:hidden;width:100%}.cropper-dashed{border:0 dashed #eee;display:block;opacity:.5;position:absolute}.cropper-dashed.dashed-h{border-bottom-width:1px;border-top-width:1px;height:calc(100% / 3);left:0;top:calc(100% / 3);width:100%}.cropper-dashed.dashed-v{border-left-width:1px;border-right-width:1px;height:100%;left:calc(100% / 3);top:0;width:calc(100% / 3)}.cropper-center{display:block;height:0;left:50%;opacity:.75;position:absolute;top:50%;width:0}.cropper-center:after,.cropper-center:before{background-color:#eee;content:' ';display:block;position:absolute}.cropper-center:before{height:1px;left:-3px;top:0;width:7px}.cropper-center:after{height:7px;left:0;top:-3px;width:1px}.cropper-face,.cropper-line,.cropper-point{display:block;height:100%;opacity:.1;position:absolute;width:100%}.cropper-face{background-color:#fff;left:0;top:0}.cropper-line{background-color:#39f}.cropper-line.line-e{cursor:ew-resize;right:-3px;top:0;width:5px}.cropper-line.line-n{cursor:ns-resize;height:5px;left:0;top:-3px}.cropper-line.line-w{cursor:ew-resize;left:-3px;top:0;width:5px}.cropper-line.line-s{bottom:-3px;cursor:ns-resize;height:5px;left:0}.cropper-point{background-color:#39f;height:5px;opacity:.75;width:5px}.cropper-point.point-e{cursor:ew-resize;margin-top:-3px;right:-3px;top:50%}.cropper-point.point-n{cursor:ns-resize;left:50%;margin-left:-3px;top:-3px}.cropper-point.point-w{cursor:ew-resize;left:-3px;margin-top:-3px;top:50%}.cropper-point.point-s{bottom:-3px;cursor:s-resize;left:50%;margin-left:-3px}.cropper-point.point-ne{cursor:nesw-resize;right:-3px;top:-3px}.cropper-point.point-nw{cursor:nwse-resize;left:-3px;top:-3px}.cropper-point.point-sw{bottom:-3px;cursor:nesw-resize;left:-3px}.cropper-point.point-se{bottom:-3px;cursor:nwse-resize;height:20px;opacity:1;right:-3px;width:20px}@media (min-width:768px){.cropper-point.point-se{height:15px;width:15px}}@media (min-width:992px){.cropper-point.point-se{height:10px;width:10px}}@media (min-width:1200px){.cropper-point.point-se{height:5px;opacity:.75;width:5px}}.cropper-point.point-se:before{background-color:#39f;bottom:-50%;content:' ';display:block;height:200%;opacity:0;position:absolute;right:-50%;width:200%}.cropper-invisible{opacity:0}.cropper-bg{background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAQMAAAAlPW0iAAAAA3NCSVQICAjb4U/gAAAABlBMVEXMzMz////TjRV2AAAACXBIWXMAAArrAAAK6wGCiw1aAAAAHHRFWHRTb2Z0d2FyZQBBZG9iZSBGaXJld29ya3MgQ1M26LyyjAAAABFJREFUCJlj+M/AgBVhF/0PAH6/D/HkDxOGAAAAAElFTkSuQmCC)}.cropper-hide{display:block;height:0;position:absolute;width:0}.cropper-hidden{display:none!important}.cropper-move{cursor:move}.cropper-crop{cursor:crosshair}.cropper-disabled .cropper-drag-box,.cropper-disabled .cropper-face,.cropper-disabled .cropper-line,.cropper-disabled .cropper-point{cursor:not-allowed}#katalon{font-family:monospace;font-size:13px;background-color:rgba(0,0,0,.7);position:fixed;top:0;left:0;right:0;display:block;z-index:999999999;line-height: normal} #katalon div{padding:0;margin:0;color:#fff;} #katalon kbd{display:inline-block;padding:3px 5px;font:13px Consolas,&quot;Liberation Mono&quot;,Menlo,Courier,monospace;line-height:10px;color:#555;vertical-align:middle;background-color:#fcfcfc;border:1px solid #ccc;border-bottom-color:#bbb;border-radius:3px;box-shadow:inset 0 -1px 0 #bbb;font-weight: bold} div#katalon-spy_elementInfoDiv {color: lightblue; padding: 0px 5px 5px} div#katalon-spy_instructionDiv {padding: 5px 5px 2.5px}#katalon{font-family:monospace;font-size:13px;background-color:rgba(0,0,0,.7);position:fixed;top:0;left:0;right:0;display:block;z-index:999999999;line-height: normal} #katalon div{padding:0;margin:0;color:#fff;} #katalon kbd{display:inline-block;padding:3px 5px;font:13px Consolas,&quot;Liberation Mono&quot;,Menlo,Courier,monospace;line-height:10px;color:#555;vertical-align:middle;background-color:#fcfcfc;border:1px solid #ccc;border-bottom-color:#bbb;border-radius:3px;box-shadow:inset 0 -1px 0 #bbb;font-weight: bold} div#katalon-spy_elementInfoDiv {color: lightblue; padding: 0px 5px 5px} div#katalon-spy_instructionDiv {padding: 5px 5px 2.5px}

  1 NN Welcome, new_firstName_1583402331087TucumanJasper N FounderBusinessNetways_Organization1583220732467 N PostInviteShareNetways_Organization1583221188791 N PostInviteShareNetways_Organization1583402618113 N PostInviteShareNetways_Organization1583402791615 N PostInviteShareNetways_Organization1583403033614 N PostInviteShareNewsFrom pages you followMoreRabih Keyrouz Rocks Paris in Latest Show1mo•by Simon KhouryDiasporaID HeadlinesFofoo 1mo•by Simon Khoury DiasporaID HeadlinesFeatureeeedd 1mo•by Simon Khoury PeopleYou should followMoreStephanie Ghazal President, Tollab Sin El Fil Montreal Rami Hatoum Rami Hatoum, Association Libanaise de l’Université de Montréal Beirut Montreal Nick Kahwaji Honorary Consul, Honorary Consulate of Lebanon in Vancouver Abra Port Moody  FZ Fadi Ziadeh Ambassador, Embassy of Lebanon in Canada Beirut Ottawa Simon Khoury Ambassador, Embassy of Lebanon in Ukraine Jbeil (Byblos) Beirut Reem Ghaziri Ambassador, Embassy of Lebanon in Turkey Tripoli Adelaide  NP Nati Pati President, Taxes Aaba Airdrie  CA CCICL Admin Administrator, Chamber of commerce and industry Canada- Lebanon - CCICL Montreal Natacha Kubeiah last Founder, Parfum Beirut Aabeidat Explore By RoleLooking for a mayor, a mukhtar, or a town ambassador to contact? Start here. Mayors  Community Ambassadors  Mukhtars  Elected Members 1 NN new_firstName_1583402331087 new_lastName_1583402331087 Employee, Netways_Organization1583220732467 Set Primary RoleTucumanJasper N FounderBusinessManageShareAboutInfo, sector and contact detailsEditnew_about_1583402331087AnimationAdd a Professionnew_email1583402331087@gmail.com Private+1123456 PrivateSocial Media PeopleFollowing &amp; followed by new_firstName_1583402331087See All JI Johnny Ibrahim Ambassador, Embajada del Líbano en ArgentinaBeirutBuenos Aires View AllNetworksnew_firstName_1583402331087’s communities &amp; orgsEdit N Netways_Organization1583220732467 EmployeeAaba N Netways_Organization1583221188791 EmployeeAaba N Netways_Organization1583402618113 FounderAaba N Netways_Organization1583402791615 EmployeeAaba N Netways_Organization1583403033614 FounderAabaBusinessCompanies, Startups, FreelancersAre you in charge of a company or startup? Work in one?Add Your Business RoleUpdatesnew_firstName_1583402331087’s recent posts &amp; eventsPosts and events you added to your profile or communities appear hereCreate Post Under Your ProfileMedia GalleryPhotos &amp; videosShow sights, achievements or projects with images and videosAdd Photos or VideosHomeDirectoryMarketMessagesProfile NN  Posting under new_firstName_1583402331087 new_lastName_1583402331087 Publish Upload a photo for your post (optional) Post TitleDescriptionBoldItalicFullscreenUnordered ListDefaultCircleDiscSquareInsert ImageInsert VideoInsert Linkfsf0 Advanced issue found▲1What’s happening3
  




Insert Image (Ctrl+P)id(&quot;htmlTag&quot;)</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;htmlTag&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//html[@id='htmlTag']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//html</value>
   </webElementXpaths>
</WebElementEntity>
