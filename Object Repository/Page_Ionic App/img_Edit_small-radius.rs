<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img_Edit_small-radius</name>
   <tag></tag>
   <elementGuidId>b0018256-6277-4858-9bf2-37d93b4d7d10</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//body[@id='body-id']/app-root/ion-app/ion-router-outlet/ng-component/ion-tabs/div/ion-router-outlet/app-profile/ion-content/div[3]/ion-slides/div/ion-slide/div/app-list-card/div/div/div/div/img</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>small-radius</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://diasporamediastaging.blob.core.windows.net/municipality/286193b7-0304-e711-80e1-00155d003d3b/municipality-cover-image-1.png</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;body-id&quot;)/app-root[1]/ion-app[@class=&quot;md ion-page hydrated&quot;]/ion-router-outlet[@class=&quot;hydrated&quot;]/ng-component[@class=&quot;ion-page&quot;]/ion-tabs[1]/div[@class=&quot;tabs-inner&quot;]/ion-router-outlet[@class=&quot;hydrated&quot;]/app-profile[@class=&quot;ion-page&quot;]/ion-content[@class=&quot;md hydrated&quot;]/div[@class=&quot;card-container-with-border&quot;]/ion-slides[@class=&quot;content-space bottom-10 md slides-md swiper-container hydrated swiper-container-initialized swiper-container-horizontal swiper-container-autoheight&quot;]/div[@class=&quot;swiper-wrapper&quot;]/ion-slide[@class=&quot;slide md swiper-slide swiper-zoom-container hydrated swiper-slide-active&quot;]/div[@class=&quot;slide-container&quot;]/app-list-card[1]/div[1]/div[@class=&quot;list&quot;]/div[@class=&quot;list-container&quot;]/div[@class=&quot;list-items-left&quot;]/img[@class=&quot;small-radius&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//body[@id='body-id']/app-root/ion-app/ion-router-outlet/ng-component/ion-tabs/div/ion-router-outlet/app-profile/ion-content/div[3]/ion-slides/div/ion-slide/div/app-list-card/div/div/div/div/img</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:img</name>
      <type>Main</type>
      <value>(//img[contains(@src,'https://diasporamediastaging.blob.core.windows.net/municipality/286193b7-0304-e711-80e1-00155d003d3b/municipality-cover-image-1.png')])[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//app-list-card/div/div/div/div/img</value>
   </webElementXpaths>
</WebElementEntity>
