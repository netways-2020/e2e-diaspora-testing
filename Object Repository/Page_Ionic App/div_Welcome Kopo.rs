<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Welcome Kopo</name>
   <tag></tag>
   <elementGuidId>2b87d815-5923-4864-b103-1c1253c15fec</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//body[@id='body-id']/app-root/ion-app/ion-router-outlet/ng-component/ion-tabs/div/ion-router-outlet/app-home/ion-content/div/app-entity-common-header/div/div/div[2]/div/div[2]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-body-title welcome-user-name</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Welcome, Kopo</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;body-id&quot;)/app-root[1]/ion-app[@class=&quot;md ion-page hydrated&quot;]/ion-router-outlet[@class=&quot;hydrated&quot;]/ng-component[@class=&quot;ion-page&quot;]/ion-tabs[1]/div[@class=&quot;tabs-inner&quot;]/ion-router-outlet[@class=&quot;hydrated&quot;]/app-home[@class=&quot;ion-page&quot;]/ion-content[@class=&quot;md hydrated&quot;]/div[@class=&quot;card-container-with-border no-border-top-radius&quot;]/app-entity-common-header[1]/div[1]/div[@class=&quot;big-height main-card&quot;]/div[@class=&quot;content-space&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;card-body-container&quot;]/div[@class=&quot;card-body-title welcome-user-name&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//body[@id='body-id']/app-root/ion-app/ion-router-outlet/ng-component/ion-tabs/div/ion-router-outlet/app-home/ion-content/div/app-entity-common-header/div/div/div[2]/div/div[2]/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Aaba'])[1]/preceding::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Avellaneda'])[1]/preceding::div[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Welcome, Kopo']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/div</value>
   </webElementXpaths>
</WebElementEntity>
