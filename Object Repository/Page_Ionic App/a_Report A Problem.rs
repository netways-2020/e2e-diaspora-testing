<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Report A Problem</name>
   <tag></tag>
   <elementGuidId>7fe7f7e6-59ed-4d38-a74a-f0481b30c2b0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//ion-modal[@id='ion-overlay-8']/div/app-action-sheet/ion-content/div/div[2]/div/div/a[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>vertical-sheet-item</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Report A Problem</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ion-overlay-8&quot;)/div[@class=&quot;modal-wrapper md sc-ion-modal-md&quot;]/app-action-sheet[@class=&quot;ion-page&quot;]/ion-content[@class=&quot;no-scroll-content md hydrated&quot;]/div[@class=&quot;action-sheet&quot;]/div[@class=&quot;sheet&quot;]/div[@class=&quot;action-sheet-vertical no-border&quot;]/div[@class=&quot;vertical-sheet&quot;]/a[@class=&quot;vertical-sheet-item&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ion-modal[@id='ion-overlay-8']/div/app-action-sheet/ion-content/div/div[2]/div/div/a[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='More'])[6]/following::a[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Photos &amp; videos'])[2]/following::a[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[1]/preceding::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[2]</value>
   </webElementXpaths>
</WebElementEntity>
