<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Set Primary Role</name>
   <tag></tag>
   <elementGuidId>f8cce24f-e008-410d-8838-fde79658d2f6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//body[@id='body-id']/app-root/ion-app/ion-router-outlet/ng-component/ion-tabs/div/ion-router-outlet/app-profile/ion-content/div/div/app-entity-common-header/div/div/div[2]/div/div[2]/div[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-body-subtitle-with-image</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Set Primary Role</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;body-id&quot;)/app-root[1]/ion-app[@class=&quot;md ion-page hydrated&quot;]/ion-router-outlet[@class=&quot;hydrated&quot;]/ng-component[@class=&quot;ion-page&quot;]/ion-tabs[1]/div[@class=&quot;tabs-inner&quot;]/ion-router-outlet[@class=&quot;hydrated&quot;]/app-profile[@class=&quot;ion-page&quot;]/ion-content[@class=&quot;md hydrated&quot;]/div[@class=&quot;card-container-with-border no-border-top-radius&quot;]/div[1]/app-entity-common-header[1]/div[1]/div[@class=&quot;main-card&quot;]/div[@class=&quot;content-space&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;card-body-container&quot;]/div[@class=&quot;card-body-subtitle-with-image&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//body[@id='body-id']/app-root/ion-app/ion-router-outlet/ng-component/ion-tabs/div/ion-router-outlet/app-profile/ion-content/div/div/app-entity-common-header/div/div/div[2]/div/div[2]/div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='founder, Netways 1582984572908'])[1]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Automation Testing'])[1]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Beirut'])[1]/preceding::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='founder, Netways 1582990732186'])[1]/following::div[1]</value>
   </webElementXpaths>
</WebElementEntity>
