<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Manage Details</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>f9a1cadd-2e0e-4665-a802-908b1966fcfb</testSuiteGuid>
   <testCaseLink>
      <guid>2f5ad534-e23e-4b5c-bf89-cba3d3da5f73</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/My Profile/My User Info/Check_Publisher_User</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>241bf03b-afc1-4588-a89f-dfc65f7fd4e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/My Profile/My User Info/My_Profile_Actions_Section</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a88b3ddf-b714-4b45-99af-5f41d606d100</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/My Profile/My User Info/My_Profile_Edit_Information</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>82da4459-a656-4b4d-8eae-21f9b83e28f8</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>758e9475-9812-47ce-a46a-73644dc6c6be</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e0cf07e4-a87d-4ed4-84c7-1910680383b0</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>51bd0d2f-c793-4afe-8481-8605333623f9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>da114d51-595d-44af-b69b-3a89cb667bba</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>54fe9da2-80b4-4eb6-a4a3-0fd2e48fc30b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>53f334ac-1ea4-4df3-889a-25ac4df44def</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>0944378a-c1ca-4a01-b616-dd01cc142933</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5b9cc310-499c-4f3b-8e20-2a1e5fb37a59</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7607f93e-a404-4a9c-8ec5-5848b7da6003</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>2b68bab1-90cf-4c25-8720-2ab7dbf7683d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/My Profile/My User Info/My_Profile_Edit_Privacy</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>427cff0d-d483-4028-997a-875b72b9bd0b</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>6142ae3a-d22f-4ff2-8ce4-654c546b0d57</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/My Profile/My User Info/My_Profile_Empty_State</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>78fdf2aa-f6da-4073-88f4-df1a586cd848</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/My Profile/My User Info/My_Profile_Empty_State_Outside_Lebanon</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4ae8df5c-1574-4d80-b87e-c1a8acd9b475</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/My Profile/My User Info/My_Profile_Update_User_Image</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>058b6e75-f187-48be-af1c-cd77ef534681</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/My Profile/Follower-Following/My_Profile_Manage_Followers</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b4e432fa-d020-435f-9f24-40b90e4c5446</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/My Profile/Follower-Following/My_Profile_Manage_Following</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>660cfcdb-1dae-4ebe-939e-962cc4d10476</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/Visitor/Visitor User Info/Visitor_Profile_Actions_Section</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>63b5624c-ac09-4a9c-a021-df8447dcb84c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/Visitor/Visitor User Info/Visitor_Profile_Empty_state</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8549bede-d268-4cea-b8cb-f7d876a12ae9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/Visitor/Visitor User Info/Visitor_Report_User</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c4ac9cad-df98-4410-8818-2406c117b4f5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/Empty State/Profile_All_Empty_State</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
