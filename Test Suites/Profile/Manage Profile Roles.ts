<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Manage Profile Roles</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>811f4f3f-f38c-45ef-b93c-090da2b5a28c</testSuiteGuid>
   <testCaseLink>
      <guid>d7ee808d-848e-4677-95a2-0b1011485691</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Profile/My Profile/Diplomatic Role/My_Profile_Manage_Diplomatic_Role</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>56c0a93a-77d4-4238-99a2-e6459a5380cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Profile/My Profile/Diplomatic Role/My_Profile_Set_Diplomatic_As_Primary</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>86dd611f-e86b-473b-8658-0b19b6ad16a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Profile/My Profile/Business Role/My_Profile_Manage_Business_Role</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0e10bd05-d0ce-41f9-903e-3a429c0dbbe0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Profile/My Profile/Business Role/My_Profile_Set_Businesss_Role_As_Primary</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>36651375-4a53-4213-b3cc-2165f3cba3c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Profile/My Profile/Organization Role/My_Profile_Manage_Organization_Role</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>468f498d-6417-416e-88ce-1684606265fa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/My Profile/Organization Role/My_Profile_Set_Organization_Role_As_Primary</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2488d927-c3c4-482c-8aea-c47e7e3f926b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Profile/My Profile/Self Employeed Role/My_Profile_Manage_Self_Employeed_Role</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
