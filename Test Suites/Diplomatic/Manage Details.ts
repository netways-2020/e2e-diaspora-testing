<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Manage Details</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>2d9ba398-98a9-49b9-83bb-bb4c13691c04</testSuiteGuid>
   <testCaseLink>
      <guid>4840e09c-2597-4d4d-9636-801d845a2912</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Diplomatic/President or Admin/Ambassadorial Network/Manage_Ambassadorial_Network</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ced769f1-bc2f-4c60-b43f-ad54ef149321</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Diplomatic/President or Admin/Embassy President Info/Embassy_Actions_Section</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bc473dd3-a30d-4af2-8528-bb85a05738f1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Diplomatic/President or Admin/Embassy President Info/Embassy_Edit_Information</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4772d93a-e391-430f-b899-24045f4587fb</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5baba40b-6f6c-4c6a-9fe2-54afb2217ea5</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7e69964e-7381-404f-99a0-fb7e7ec65ce4</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e8b5c995-00bb-4e77-b35c-23c748f8cb05</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b9119b57-7bbc-459d-ba39-17e8a70b2ebf</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5a8ef9a5-dfb2-4393-8243-697cac5f7f4a</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e45a3aad-1491-4913-ade5-ebe806aecac3</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ed4a9a08-8b0e-497b-8653-b961bfc6e81f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>54b5b48a-1f16-46f0-a592-611612616563</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>210b7496-50c2-4bfc-aec5-8cb1eea6d893</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Diplomatic/President or Admin/Embassy President Info/Embassy_Empty_State</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>264dcca2-f46c-4a47-90b9-65276a6b112a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Diplomatic/President or Admin/Embassy President Info/Embassy_Update_Image</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3b9696c0-46b1-43ca-a20e-8892577f229c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Diplomatic/President or Admin/Key Staff/Accept_Member</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>92d012cd-b795-462c-9a03-926ae1d6b50f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Diplomatic/President or Admin/Key Staff/Admin</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6e011787-b5a1-46f1-971d-f08e0765a25a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Diplomatic/President or Admin/Key Staff/Reject_Member</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4186acd2-a52c-4389-a060-0b553b18efd2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Diplomatic/President or Admin/Support Staff/Manage_Support_Staff</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b4f52d29-766e-46f6-9d2c-f621d4766e15</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Diplomatic/President or Admin/Trade Office/Manage_Trade_Office</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
