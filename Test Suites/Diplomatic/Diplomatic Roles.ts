<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Diplomatic Roles</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>d442bfa6-09c4-457c-b4c9-e93764133e1e</testSuiteGuid>
   <testCaseLink>
      <guid>fee981d4-09b5-4491-a1e5-22e58e71c2e1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Diplomatic/Diplomat And Key Staff/Diplomat And Key Staff Info/Embassy_Diplomat_And_KeyStaff_Empty_State</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cb341e4c-b575-40e5-89b2-1eb504cb67ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Diplomatic/Diplomat And Key Staff/Media/Diplomat And Key Staff_Media</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>51a98109-c963-4a73-aeec-198c14839b1e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Diplomatic/Diplomat And Key Staff/Media/Diplomat And Key Staff_Media_Youtube</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3f440e26-fea1-499d-aabf-03f9ac7694c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Diplomatic/Diplomat And Key Staff/Updates/Diplomat And Key Staff_Events</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c3d3b8b7-008b-425e-9d1f-8b7a7b4e065e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Diplomatic/Diplomat And Key Staff/Updates/Diplomat And Key Staff_News</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1f1f1870-45c7-4469-88f3-69f91f0dd33c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Diplomatic/Diplomatic Member/Diplomatic Member Info/Diplomatic_Member_Empty_State</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5e61c908-eb61-4d2b-93e0-909ef4bda02e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Diplomatic/Diplomatic Member/Media/Diplomatic_Member_Media</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7e764aab-ea48-49b3-bacb-b417d45481fe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Diplomatic/Diplomatic Member/Media/Diplomatic_Member_Youtube</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ef143a69-bc88-4a0a-a891-871a35aa24b8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Diplomatic/Diplomatic Member/Updates/Diplomatic_Member_Events</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>37638741-7940-4b66-8194-1330e7151c6a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Diplomatic/Diplomatic Member/Updates/Diplomatic_Member_News</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d0793c80-003b-42a4-9a38-821d1cc32f1d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Diplomatic/Embassy_Location_With_Same_User_City/Embassy_Location_With_Same_User_City</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8d7fded1-762b-4285-9c55-a5baf06b5ce6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Diplomatic/Embassy_Location_With_Same_User_City/Login_For_Belgium_Embassy</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>052cf07e-0133-49ff-8aa1-1f4072b924d1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Diplomatic/Support Staff/Media/Diplomatic_Support_Staff_Youtube</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>babe4221-4027-49eb-93b6-62f9f945ed82</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Diplomatic/Support Staff/Media/Diplomatic_Support_Staff_Media</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>23afe502-a37c-4339-a9db-b46a99d618b1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Diplomatic/Support Staff/Support Staff Info/Embassy_Support_Staff_Empty_State</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6b2f2535-e381-4219-8d70-1ec781e221b1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Diplomatic/Support Staff/Updates/Diplomatic_Support_Staff_Events</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fbcb73de-7578-4153-8bf9-c698838b6e83</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Diplomatic/Support Staff/Updates/Diplomatic_Support_Staff_News</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e1993d93-2d0e-4b60-8fca-3c5afec853b2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Diplomatic/Visitor/Embassy Visitor Info/Embassy_Visitor_Actions_Section</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>73d512bd-bf78-44ae-b16e-21b25ff413d8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Diplomatic/Visitor/Embassy Visitor Info/Embassy_Visitor_Empty_state</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>75419230-0c1f-4a7b-a23f-97ff4ea4cbc1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Diplomatic/Visitor/Embassy Visitor Info/Embassy_Visitor_Report</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8e7321ce-e3ee-4641-8d0b-fe21e2209f67</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Diplomatic/Check Roles/Check Diplomatic Admin Role</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d3207ab9-9298-467f-afad-6dfc8ce696d9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Diplomatic/Check Roles/Check Diplomatic Members And Admin List After Remove Admin Role</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4ee8f1c2-dc07-4e09-a0bd-281b002bfc0b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Diplomatic/Check Roles/Check Diplomatic Role</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1ee08c58-3601-48fa-bd59-d7b68a732321</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Diplomatic/Check Roles/Check Remove Office Member Role</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d40d0260-1467-4075-b4d5-6f3d1add5092</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Diplomatic/Check Roles/Check Diplomatic Followers</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
