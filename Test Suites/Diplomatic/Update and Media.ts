<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Update and Media</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>1b3fe2e5-e4ba-47d6-b694-4ae74ded25f5</testSuiteGuid>
   <testCaseLink>
      <guid>4afaeddc-be34-4720-b7eb-e85fa1609901</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Diplomatic/President or Admin/Updates/Diplomatic_Events</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e5628433-b480-4640-97c5-ec490ed5a1e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Diplomatic/President or Admin/Updates/Diplomatic_News</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c4231bf3-ec44-4d9d-937e-7279ac1381d2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Diplomatic/President or Admin/Media/Diplomatic_Media</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>943fb61f-d565-4d70-9bb7-2563accd13e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Diplomatic/President or Admin/Media/Diplomatic_Media_Youtube</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
