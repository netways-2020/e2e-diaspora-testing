<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Search</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>6173ee1c-a7cb-42fc-a3f2-b4c825f8903c</testSuiteGuid>
   <testCaseLink>
      <guid>b6fa5b55-b59e-47b1-98c8-098f424aa1f4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Search/Search_businesses_entity_filter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>89ac8da1-c996-438c-ae8a-d769a714eb40</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Search/Search_Created_Entities</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>af8195b4-dab4-42d9-8252-1901c66e16e6</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>51b2b3fc-1447-42b0-aa5a-a0c0c116d99e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>668c961a-05e5-4181-b005-a20c5326f7ba</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>034407df-64a8-486e-b3c4-97a06404800a</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>9ef836e2-b155-4d14-b40b-415495e00dca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Search/Search_entities</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f5943828-80ff-4c6b-b09d-966c628f197d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Search/Search_Everything_Check_Result</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cae6279d-4df6-480c-925b-76104795c60c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Search/Search_everything_entity_filter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e081868e-ec8b-4b28-ae23-cc10f7507f47</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Search/Search_network_entity_filter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1a51fd14-ecfb-4061-a391-c7b4a9686c26</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Search/Search_offers</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4445d734-a521-443b-8e02-51a22d508687</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Search/Search_peoples_entity_filter</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
