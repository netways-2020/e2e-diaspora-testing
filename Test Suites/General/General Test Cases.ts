<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>General Test Cases</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>a9e83fd8-55f7-4c2a-b2ee-809702011e6f</testSuiteGuid>
   <testCaseLink>
      <guid>793b9f5a-b421-4f5b-b29a-8c9d7b1d9776</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Notifications/Check_Notifications</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>889281a0-6e8a-428e-b539-2723d954d35e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Notifications/Check_Notification_Badge</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6190a2cc-e31d-4743-8724-21e91e600945</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5d9f0eb4-d031-4da7-9bdf-b76d6de8b42d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>2f15dcdb-a48b-47b1-9952-36d7faec9b5a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Notifications/Manage_Notifications</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8e13fd5a-f6f2-44dd-bcfd-738f4567e663</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Notifications/notifications_empty_state</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>167cfbbe-a6d2-49b6-8cf9-421fad3b06d2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Messages/Check_Messages</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>9b80b45f-3c48-4055-a10e-a3434b0eddc0</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>126b7014-f186-4459-9eea-9b5eccdfd5b7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ea062eee-d7d2-42d6-80bb-1306f1f9ecce</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>9d21bb0f-59a0-463b-9d9d-fd5c2fdedd4e</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>64a16790-e4e2-4e8a-901b-a0fc5b9da754</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Messages/Check_Messages_Town</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>032c32a2-21e3-4eff-9c79-767176b01a75</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6f14ad91-ce4a-426e-a5d5-2bada67baed2</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>441b1898-4ae3-4e30-b973-f30c2ffdc395</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Messages/Check_Message_Badge_Decrement</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>bc68cf39-900f-482a-a504-a01981c096a3</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>62d8bba5-dd94-4d79-bcbe-b1674cb2534a</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>9d7907a8-a384-4519-89e6-6a1568155393</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Messages/messages_empty_state/empty_state_conversation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>795f01ee-8314-4428-a46a-5f46162096c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Messages/messages_empty_state/empty_state_message</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c0f2fab1-3395-49dd-b907-7def97b30c9d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MarketPlace/Manage_Market_Place</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d7e7db10-082c-4a67-b04b-da06f0c71022</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MarketPlace/Register_User_For_MarketPlace</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c7545bf1-8902-44b9-97e6-6c718a9c89f9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Card/card_empty_satate/save_card_empty_state</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4da357c0-9093-4875-b63e-39aa6235bfde</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Card/Contact_Seller</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b3f20a80-e2de-4ad5-981f-8cb4f7c11896</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Card/Like_Comment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>378fce97-f89d-41de-a3b0-749ecf9380c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Card/Save_Cards</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d895e0f5-947d-4082-9ba4-2607ac4ba18f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/test cover image entities/test cover image entities</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>244d8508-cd30-49de-b484-d2d85c12cb35</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Manage Account/manage-account</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
