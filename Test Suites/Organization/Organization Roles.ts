<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Organization Roles</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>76065f43-75f7-45bb-89b5-70ac38b1cd91</testSuiteGuid>
   <testCaseLink>
      <guid>76c414ef-bddd-4187-b4bd-4d398c4a2f0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Organization/Board Of Directors/Media/Organization_Board_Of_Direcotrs_Media</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d1c177c7-94b7-4466-9735-fbd8de4fb2e3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Organization/Board Of Directors/Media/Organization_Board_Of_Direcotrs_Media_Youtube</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5808f741-8891-4b6b-aa1d-8dc9ec83a753</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Organization/Board Of Directors/Organization Board of directors Info/Organization_Board_Of_Direcotrs_Empty_State</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1cefdb70-49c7-481e-9990-e29b62cd7744</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Organization/Board Of Directors/Updates/Organization_Board_Of_directors_Events</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9ec070b7-734d-497c-97b1-5151b44b8e4f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Organization/Board Of Directors/Updates/Organization_Board_Of_directors_News</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7b9db6c3-c2cd-41b1-99a8-b19e6cae7223</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Organization/Visitor/Organization Visitor Info/Organization_Visitor_Actions_Section</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8a4847ff-1e16-41af-bac3-3d34bb448532</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Organization/Visitor/Organization Visitor Info/Organization_Visitor_Claim_Organization</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9b41af95-8357-445b-8f22-394187a62b35</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Organization/Visitor/Organization Visitor Info/Organization_Visitor_Empty_state</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>729d0c56-2ed0-4607-b3a4-95f2a48b2975</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Organization/Visitor/Organization Visitor Info/Organization_Visitor_Report</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>db5dce7e-8b5e-45cd-833a-d5db8f916f22</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Organization/Member/Media/Organization_Member_Media</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6b0b05e4-4543-4db7-9c43-e02dd8a1abee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Organization/Member/Media/Organization_Member_Youtube</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>379f79b3-30d2-4540-8e3b-985a22472e35</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Organization/Member/Organization Member Info/Organization_Member_Empty_State</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0e460f81-4cce-4cdf-8d3c-57e84307a90a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Organization/Member/Updates/Organization_Member_Events</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cb4772f8-5083-4f04-aac9-fd3b715e4b1e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Organization/Member/Updates/Organization_Member_News</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5d584710-84ef-4425-8ffd-e76fc2795dbf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Organization/Committee Or Board Member/Media/Organization_Committee_or_Board_Member_Media</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fd21aaf4-68b4-497e-8e64-2e88f56d4569</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Organization/Committee Or Board Member/Media/Organization_Committee_or_Board_Member_Media_Youtube</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e8d1beca-1e86-4c61-bae5-38095f52d2a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Organization/Committee Or Board Member/Organization Committe or Board Member Info/Organization_Committee_or_Board_Member_Empty_State</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3cf69c36-cf7d-45f7-8182-e1c724222dce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Organization/Committee Or Board Member/Updates/Organization_Committee_or_Board_Member_Events</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f7247381-6792-45a5-9b3d-777ce33ba6cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Organization/Committee Or Board Member/Updates/Organization_Committee_or_Board_Member_News</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a1e8f0df-223b-474d-94e1-825ceae154f1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Check Roles/Check Organization Admin Role</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>02204a3a-45e5-4632-8087-42b99ae8023a</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>f204e1e1-a16b-4f9d-808e-29284ac2642d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Check Roles/Check Organization Members And Admin List After Remove Admin Role</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c937a7d2-0739-4afe-8301-78fe4e22b14f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>14d024fb-ae16-4a5f-bf83-36b35b2d2b62</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Check Roles/Check Organization Role</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fa93a3d4-5735-4e1e-b2a0-006757d72229</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Check Roles/Check Remove Committee Member Role</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0f597b45-c316-4178-8259-bd03fccfabcd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Check Mentor Hub Organization Role/Check Mentor Hub organization</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>963ee6f0-355b-45c3-952a-68b9e6398645</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Check Roles/Check Organization Followers</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
