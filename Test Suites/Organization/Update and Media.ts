<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Update and Media</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>164fcd34-5c9a-46fb-b5d7-033445fdf81e</testSuiteGuid>
   <testCaseLink>
      <guid>46ae30d4-c262-4c1b-ba09-d63470aea737</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Founder or admin/Media/Organization_Media</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>318f9ab4-8ea9-453d-b619-55000cada7f8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Founder or admin/Media/Organization_Media_Youtube</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dcbbc733-e4aa-40f2-a10a-39205a44efd2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Founder or admin/Updates/Organization_Events</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>14229ddd-9939-45c0-b3fb-565b9b46a954</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Founder or admin/Updates/Organization_News</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
