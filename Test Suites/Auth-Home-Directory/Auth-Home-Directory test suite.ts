<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Auth-Home-Directory test suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>45b84c4e-3fbf-4e22-a995-6a403b4ac113</testSuiteGuid>
   <testCaseLink>
      <guid>37366a6f-e6f2-4e75-9ac0-e8801852a2f3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Authorization/loginWithEmail</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>594730c1-8472-4a8a-a40d-54371aee408f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5147c952-0a5b-40fc-85fb-f20902d2bfbc</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>1aabe31b-89cf-45df-bbcf-0a3b7f968062</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>1d139cbc-cf2a-498f-b66d-cbcc9f93b7cc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Authorization/loginWithPhone</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0711bc28-ba73-4bca-a7b4-a179c92c59c6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Authorization/registration</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>bee1631a-62cb-488a-8014-b65cb6a8f55d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>8d29902c-9197-4d7f-b0ac-0510d25b91b7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6460803e-8c5f-4ab5-86df-f85edf36e3ee</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7d9ab354-fa63-47cc-b788-9848b3c0db00</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6fa1604b-7ee7-4595-b147-f4875222e04a</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e79e06a4-d619-41d5-8fd9-1db61355f70e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Home/business-network-people-business-support-sections</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4feed9fa-98f3-4214-bb27-206bf57765dc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Home/empty state/home_empty_state</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>93e513e3-2de9-4377-8e64-99dadf76bf58</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Home/home-explore-listing</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3535c5f1-8df0-4a25-bd3f-2fe4bc1ba9da</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Home/home_check-Main-Header-And-New-Section</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>42a5ebdf-4508-4a93-926c-5db9042dc708</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Directory/empty state/directory_empty_state</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b0bc41f0-a460-40d9-962c-e79195d838bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Directory/section in tabs/Business Tab</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ae04ebb3-64b2-4819-84fe-eb42ad47193f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Directory/section in tabs/Networks Tab</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3b3d54ca-2a64-4e08-985b-9629dfa989d6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Directory/section in tabs/People Tab</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0d253142-3e12-4b06-b07c-5d8356e75895</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Home/check-delete-news</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>12e1f94b-b3c9-4480-a723-73c9f43c4d4e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Directory/check-delete-event</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
