<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Update and Media</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>df47238a-e81a-4ff4-a84e-1cadef35334d</testSuiteGuid>
   <testCaseLink>
      <guid>286207cf-fa3d-484b-8239-bfe83d9012b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Company/Founder or admin/Media/Company_Media</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9f8f2258-f300-4fe2-8e2d-fc1c28889809</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Company/Founder or admin/Media/Company_Media_Youtube</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6f6cefda-ebdc-42c4-9842-73531cb5b8cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Company/Founder or admin/Updates/Company_Events</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c222edcd-59b8-4ec1-b677-eb14d96572e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Company/Founder or admin/Updates/Company_News</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
