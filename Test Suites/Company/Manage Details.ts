<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Manage Details</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ec7a104e-d8fa-449a-94ca-7eb475336eaa</testSuiteGuid>
   <testCaseLink>
      <guid>60112b14-4ba1-4a50-ab9c-faf8cf221ab9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Company/Founder or admin/Company Founder or Admin Info/Company_Edit_Information</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3441292a-4525-4ce8-93d1-080f276acf55</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a157545a-0d3a-4920-9e5f-4e16184c04a6</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>9efc4293-9006-44a1-9b42-8d527d4f8762</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>aac2932f-9e0e-4c71-b386-952dbda8b95b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>95ae7fb9-e3e9-4c24-ac02-a4e78aaa7d85</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b93cab2f-05cf-4f5d-921a-9d90c332575f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>24827012-91da-4f0e-8d3d-db3629ca5ff7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>63243e5f-af0c-4960-bb84-889e7109f9aa</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e8cc6f94-1b5e-4648-bfba-a8dd6e8a58e9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d6cf2506-c425-4480-9191-4b4eeddf6a89</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>06bfe03d-0347-4d72-ab20-2fcce09fd6c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Company/Founder or admin/Company Founder or Admin Info/Company_Update_Image</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cdd350ff-9462-4ecd-baa6-978dc1924fae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Company/Founder or admin/Company Founder or Admin Info/My_Company_Action_Section</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e947d01c-ed05-4eaa-9ae7-b83120516859</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Company/Founder or admin/Company Founder or Admin Info/My_Company_Empty_State</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d58c287d-5bd4-4c92-919f-4a5ebe37bdeb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Company/Founder or admin/Market Place/Company_Manage_Franchise</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>824d0665-50cb-4587-a384-d745e8349a37</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Company/Founder or admin/Market Place/Company_Manage_Product</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>da1dced6-6608-4eaa-829c-09f84bdf3ee8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Company/Founder or admin/Market Place/Company_Manage_Service</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f0b0e2dc-9b2b-4b96-9df2-82b2ea162f44</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Company/Founder or admin/Team/Accept_Member</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c558c5c1-4ca9-4439-9afe-98d5d985f46f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Company/Founder or admin/Team/Admin</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2375d127-415c-4449-9a03-9b945274b769</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Company/Founder or admin/Team/Reject_Member</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f43b1f5e-2416-4c4e-b2bc-ce99d4327892</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Company/Founder or admin/Branches/Company_Manage_Branches</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>81a1e4d8-0f6f-4a0d-8d15-b953f710e85a</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a8c21c52-1c01-4104-8764-29190f06b131</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Company/Founder or admin/Affiliation/Company_Manage_Affiliation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5807b02a-80ef-4e41-99ff-011c0fb67ffc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Company/Empty State/Company_All_Empty_State</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ed4ce215-c8d1-4b89-b989-dc80d430527f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Company/Founder or admin/Authorization for company/Login_For_Company_With_Image</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
