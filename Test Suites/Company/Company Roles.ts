<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Company Roles</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>6aaa079e-fe8b-4f94-882a-0f33e665ebaf</testSuiteGuid>
   <testCaseLink>
      <guid>ab7842f2-9237-4287-aff6-d9d75577a1d6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Company/Advisor/Company Advisor Info/Company_Advisor_Empty_State</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6eff1b54-94a7-4ef1-b71c-159c5c36643c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Company/Advisor/Updates/Company_Advisor_Events</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bc8d1087-8aeb-41fa-922b-9f887b7ede6d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Company/Advisor/Updates/Company_Advisor_News</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c0a05d73-5e32-4647-a809-f368708e48ae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Company/Employee/Company Employee Info/Company_Employee_Empty_State</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2ded95f5-dba7-40a3-bd5d-9746a2eac648</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Company/Employee/Updates/Company_Employee_Events</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>de7209aa-92ce-4cb5-93a8-0658db4a45f9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Company/Employee/Updates/Company_Employee_News</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b1c8f61c-5cfe-445a-ac66-c7c51a0b4276</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Company/Mentor/Company Mentor Info/Company_Mentor_Empty_State</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>513b03d4-f19e-49da-8554-7f780d1017af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Company/Mentor/Updates/Company_Mentor_Events</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>19e9d5ac-e98d-4c3c-ad9a-4f6c99b287a4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Company/Mentor/Updates/Company_Mentor_News</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a9c4d3b0-a5ec-4712-96d9-4bde2b6e67ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Company/Visitor/Company Visitor Info/Company_Visitor_Claim_Company</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1b3eafe6-45f6-4ceb-82bd-7bd945d80d3d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Company/Visitor/Company Visitor Info/Company_Visitor_Empty_state</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8f2a9b29-9bd3-425a-92b7-22804511f0bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Company/Visitor/Company Visitor Info/Company_Visitor_Report</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>37dcc681-4129-4444-accd-b06149914bae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Company/Visitor/Company Visitor Info/Visitor_Company_Action_Section</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c02e3883-b9ef-4d74-8fbb-74df4f46bca4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Company/Check Roles/Check Company Admin Role</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>141c36ce-571c-4587-a42f-e2131f465984</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>991b33cf-54ac-45f0-84a1-df90c06d820b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Company/Check Roles/Check Company Members And Admin List After Remove Admin Role</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>46fbb8f3-f298-47bd-b312-9a1310fc66fb</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>1cec27f9-df4a-47ca-aea0-c4d9bc90cd60</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Company/Check Roles/Check Company Role</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f7753436-d163-4b0a-bf65-43e3478034cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Company/Check Roles/Check Company Followers</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
