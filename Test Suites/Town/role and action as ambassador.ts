<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>role and action as ambassador</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>7eedb7f1-e59d-49cd-8d31-2f23c9e63b99</testSuiteGuid>
   <testCaseLink>
      <guid>1966849b-5703-482b-b46b-3647c1d60260</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/ambassador/check_etimology_town_in_this_municipality</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f91c1287-bbe4-488e-b0cb-feb5d4a44627</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/ambassador/edit_information_as_ambassador</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2ac77d41-da9d-4b82-81e2-8e247dc9e5fd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/ambassador/invite_follow_message_share_report_user_as_ambassador</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>37531079-92f2-44eb-92b0-9d66c17596d2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/ambassador/municipality_mukhtar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8109c98d-8ce7-4b2d-86c7-1f83a86d9499</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/ambassador/town_administrator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>23483d78-4e1b-422c-99ef-3c48516ac4a7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/ambassador/town_ambassador_section</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c30f9da2-5dfe-4375-bc99-6b4ce9f52109</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/ambassador/town_member</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eaba8102-fbc9-41e5-a3b6-09ecf6f317eb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/ambassador/update/add_edit_delete_like_save_event_as_ambassador</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bddb149f-ff6b-491f-9c88-026d5918efb3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/ambassador/update/add_edit_delete_like_save_news</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>96ceb47a-8602-4595-9c68-aae6f897b1fa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/ambassador/update/check_edit_delete_comment_access</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ec2959fd-8d9b-4061-ba70-3e4826a60853</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/ambassador/update/update_empty_town/add_edit_delete_like_save_news</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d64cde89-8c2e-4b14-9f5e-11e94cd8ef12</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/ambassador/zmedia/add_delete_media</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>379b87ff-cd91-47dd-86a0-06c2575b614a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/ambassador/zmedia/add_delete_youtube_video</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>15f14614-f181-47f9-82d5-010c6323622f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/ambassador/zmedia/media_empty_town/add_media_delete</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
