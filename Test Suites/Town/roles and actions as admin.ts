<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>roles and actions as admin</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>c1e1942c-1c1b-4e8c-8cfd-46d90622550a</testSuiteGuid>
   <testCaseLink>
      <guid>bb344aa9-62af-4b43-b24c-09465febb4e4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/admin/check_etimology_town_in_this_municipality</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>19cf10fb-4b8c-4c4c-b1d9-41faea28c65b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/admin/edit_information_as_admin</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>776aeca2-f9f8-49ed-b12d-e97c01102a61</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/admin/invite_follow_message_share_report_user_as_admin</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c6a3d811-3ee8-4d91-bed4-3d19ebd34c69</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/admin/municipality_mukhtar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fe1b6293-99ee-4818-8c01-37d6f49e3f85</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/admin/town_administrator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>24d5c10b-036f-46d9-a29a-f3235e05efac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/admin/town_ambassador_section</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>980cf9c4-9300-4c77-b65e-679bed6c55ba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/admin/town_member</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a2586d85-3888-42a9-be46-c59723a2eb8c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/admin/update/add_edit_delete_like_save_event_as_admin</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>070f3cb1-3358-4cd2-a26b-dc495e74120a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/admin/update/add_edit_delete_like_save_news_as_admin</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8d94ce7c-742d-4d5c-9964-4ad28f8d42df</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/admin/update/check_edit_delete_comment_access</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e1691b3c-82d5-4358-add2-d806465d0142</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/admin/update/update_empty_town/add_edit_delete_like_save_news_as_admin</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f2f249b1-11a3-4b03-987e-cbe6073e98d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/admin/zmedia/add_delete_media</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7c41a566-4272-4c7c-9fcd-d265e0f56708</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/admin/zmedia/add_delete_youtube_video</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7fb689c3-69a5-4a14-9d6a-bcca6884871d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/admin/zmedia/media_empty_town/add_media_delete</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
