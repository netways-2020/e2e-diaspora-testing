<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>role and action as my town</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>88a9980c-582d-4831-9b19-158df4b20407</testSuiteGuid>
   <testCaseLink>
      <guid>7ca5e3bd-cfa7-427f-928f-29e9670f5171</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Town/roles and actions/my town/apply_to_be_ambassador</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bd7b599c-c7b9-42d4-8eab-51b4492fdbce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Town/roles and actions/my town/edit_information_as_my_town</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e4ac6b93-008b-4e96-b2a8-4deaf776de13</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/my town/invite_follow_message_share_report_user_as_myTown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d727adab-74f9-4c75-9bf7-c6a615f427d2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/my town/municipality_mukhtar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2503e52c-3c55-4f5a-a723-7ae8387a2d9a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/my town/town_administrator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>db653daf-812a-48c6-9484-7d95a5b95f6a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/my town/town_ambassador_section</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5dee09d6-3bc1-4605-8000-7fd05564add8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/my town/town_member</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7c457577-6026-4b6d-86c1-0878ccda8b8b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/my town/update/add_edit_delete_like_save_event_as_myTown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7b8c516c-285d-460d-adb3-2e8609111c61</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/my town/update/add_edit_delete_like_save_news_as_myTown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8c062957-7136-42fc-ba37-40e6f269c1c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/my town/update/check_edit_delete_comment_access</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b59e5982-2f7a-426a-acc3-a36076ee9883</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/my town/update/update_empty_town/add_edit_delete_like_save_news_as_myTown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>47897f99-a8f7-4268-8bd4-83db50848f9b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/my town/zmedia/add_delete_media</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4c0cc5be-36e9-4ae5-b2b7-2254aa006a52</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/my town/zmedia/add_delete_youtube_video</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bcee58f5-df57-4f12-8f8c-c8244eaab59b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/my town/zmedia/check delete image access</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>766b6cca-9c63-4844-af0b-0cd138f564a7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/my town/zmedia/media_empty_town/add_media_delete</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
