<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>role and action as visitor</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>9ced1798-9f02-41a2-a16a-170fcf384677</testSuiteGuid>
   <testCaseLink>
      <guid>17756dd6-e136-4455-991f-290c7ecbc1bf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/visitor/edit_information_as_visitor</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aae160f5-0880-4dda-a317-676d1a0e88d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/visitor/invite_follow_message_share_report_user_as_myTown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9ff156eb-734f-48be-b5df-e6e0da4035a5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/visitor/municipality_mukhtar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eb9cc767-c29d-481c-9e94-b46eb0d25518</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/visitor/town_administrator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c53e1e21-6459-4da3-bc85-b92f9ac7fc6a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/visitor/town_ambassador_section</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>155386d3-baa0-4416-a5a2-a4695cac3f6b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/visitor/town_member</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>835b9fdc-ae62-4964-9837-56b538e958c6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/visitor/update/add_edit_delete_like_save_news_as_myTown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eef794ed-c141-4e9c-80f5-a8301a5d600a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/visitor/update/check_edit_delete_comment_access</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7fafeede-2610-4164-8b4d-1f0b7694d13a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/visitor/update/update_empty_town/add_edit_delete_like_save_news_as_visitor</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b0c7526c-7186-4c0e-a302-e7692279b486</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/visitor/zmedia/add_delete_media</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9de6eb38-1337-431f-b67f-1bbf73a1f1a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/visitor/zmedia/add_delete_youtube_video</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>40d64ce6-201a-4209-8e87-858e2093555d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/visitor/zmedia/check delete image access</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8f08e42d-8a26-4ad2-93a1-24a3ed8c24f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/visitor/zmedia/media_empty_town/add_media_delete</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f5e7eb27-3b31-4778-975d-350fb7610c47</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/visitor/ztown_follow_new_town/folow_new_town</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
