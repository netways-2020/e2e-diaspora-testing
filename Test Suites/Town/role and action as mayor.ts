<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>role and action as mayor</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>35b732b2-1954-4640-9094-131c40eeefed</testSuiteGuid>
   <testCaseLink>
      <guid>a2f03da7-ded0-4453-8a72-87bcb1fb0467</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/mayor/check_etimology_town_in_this_municipality</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ebf285b4-d47d-438d-b93c-26216c4738c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/mayor/edit_information_as_admin</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6fad5c81-8480-4aba-84df-a396a79768f4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/mayor/invite_follow_message_share_report_user_as_mayor</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>faf6c8a8-1fc9-429a-97d0-04e081b38bdd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/mayor/municipality_mukhtar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e4aad203-3f0a-4735-b7e9-7538bf88ad29</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/mayor/town_administrator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b0dd8724-45bc-4a81-bd2a-79d59404cd0a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/mayor/town_ambassador_section</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b1050886-56bc-4501-b068-03a097eff4a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/mayor/town_member</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eed3e9ad-03e1-4f64-9808-d7a819e062d6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/mayor/update/add_edit_delete_like_save_event_as_mayor</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>687fe7ce-fad8-4390-96a8-107f63cd3fbf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/mayor/update/add_edit_delete_like_save_news_as_mayor</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a5b6d887-3890-4247-b3aa-828a5304436b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/mayor/update/check_edit_delete_comment_access</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>156afd91-d407-4153-b0c0-c0ff4610d5bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/mayor/update/update_empty_town/add_edit_delete_like_save_news_as_mayor</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>85b39d72-81f2-4bd6-bdfb-edfb0f3ff1ac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/mayor/zmedia/add_delete_media</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fca2843a-a9aa-43b9-aaae-9f5425692d18</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/mayor/zmedia/add_delete_youtube_video</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>955dc016-0bde-41fb-9970-151c03e7ecb3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Town/roles and actions/mayor/zmedia/media_empty_town/add_media_delete</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
