import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
 
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Search/Login_For_Search'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.delay(2);

TestObject everything_allPeople_entity = TestHelper.getObjectById("//div[@id='All People_everything_search_entity']")
TestObject everything_companies_entity = TestHelper.getObjectById("//div[@id='Companies_everything_search_entity']")
TestObject everything_startups_entity = TestHelper.getObjectById("//div[@id='Startups_everything_search_entity']")
TestObject everything_social_enterprises_entity = TestHelper.getObjectById("//div[@id='Social Enterprises_everything_search_entity']")
TestObject everything_products_entity = TestHelper.getObjectById("//div[@id='Products_everything_search_entity']")
TestObject everything_services_entity = TestHelper.getObjectById("//div[@id='Services_everything_search_entity']")
TestObject everything_franchises_entity = TestHelper.getObjectById("//div[@id='Franchises_everything_search_entity']")
TestObject everything_organizations_entity = TestHelper.getObjectById("//div[@id='Organizations_everything_search_entity']")
TestObject everything_hometowns_entity = TestHelper.getObjectById("//div[@id='Hometowns_everything_search_entity']")
TestObject everything_diplomatic_missions_entity = TestHelper.getObjectById("//div[@id='Diplomatic Missions_everything_search_entity']")
TestObject everything_news_entity = TestHelper.getObjectById("//div[@id='News_everything_search_entity']")
TestObject everything_events_entity = TestHelper.getObjectById("//div[@id='Events_everything_search_entity']")


boolean everything_allPeople_entity_exist = TestHelper.isElementPresent(everything_allPeople_entity);
boolean everything_companies_entity_exist = TestHelper.isElementPresent(everything_companies_entity);
boolean everything_startups_entity_exist = TestHelper.isElementPresent(everything_startups_entity);
boolean everything_social_enterprises_entity_exist = TestHelper.isElementPresent(everything_social_enterprises_entity);
boolean everything_products_entity_exist = TestHelper.isElementPresent(everything_products_entity);
boolean everything_services_entity_exist = TestHelper.isElementPresent(everything_services_entity);
boolean everything_franchises_entity_exist = TestHelper.isElementPresent(everything_franchises_entity);
boolean everything_organizations_entity_exist = TestHelper.isElementPresent(everything_organizations_entity);
boolean everything_hometowns_entity_exist = TestHelper.isElementPresent(everything_hometowns_entity);
boolean everything_diplomatic_missions_entity_exist = TestHelper.isElementPresent(everything_diplomatic_missions_entity);
boolean everything_news_entity_exist = TestHelper.isElementPresent(everything_news_entity);
boolean everything_events_entity_exist = TestHelper.isElementPresent(everything_events_entity);


checkAllPeopleFilter(everything_allPeople_entity_exist, everything_allPeople_entity);
checkCompaniesFilter(everything_companies_entity_exist, everything_companies_entity);
checkStartupFilter(everything_startups_entity_exist, everything_startups_entity);
checkSocialEnterpriseFilter(everything_social_enterprises_entity_exist, everything_social_enterprises_entity);
checkProductsFilter(everything_products_entity_exist, everything_products_entity);
checkServiceFilter(everything_services_entity_exist, everything_services_entity);
checkFranchisesFilter(everything_franchises_entity_exist, everything_franchises_entity);
checkOrgnizationFilter(everything_organizations_entity_exist, everything_organizations_entity);
checkHometownFilter(everything_hometowns_entity_exist, everything_hometowns_entity);
checkDiplomaticFilter(everything_diplomatic_missions_entity_exist, everything_diplomatic_missions_entity);
checkNewsFilter(everything_news_entity_exist, everything_news_entity);
checkEventsFilter(everything_events_entity_exist, everything_events_entity);
WebUI.delay(3);
WebUI.closeBrowser();

// Check All People Filter
def checkAllPeopleFilter(boolean everything_allPeople_entity_exist, TestObject everything_allPeople_entity) {
	if(!everything_allPeople_entity_exist) {
		TestHelper.thrownException("Filter peoples not exist");
 	} 
	
	TestHelper.clickButton(everything_allPeople_entity);
	SearchFilterHelper.checkMainPeopleFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkAllPeopleFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}

// Check Companies Filter
def checkCompaniesFilter(boolean everything_companies_entity_exist, TestObject everything_companies_entity) {
	if(!everything_companies_entity_exist) {
		TestHelper.thrownException("Filter companies not exist");
	}
	
	TestHelper.clickButton(everything_companies_entity);
	SearchFilterHelper.checkMainCompaniesFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkCompaniesFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}

// Check Startup Filter
def checkStartupFilter(boolean everything_startups_entity_exist, TestObject everything_startups_entity) {
	if(!everything_startups_entity_exist) {
		TestHelper.thrownException("Filter startup not exist");
	 }
	
	TestHelper.clickButton(everything_startups_entity);
	SearchFilterHelper.checkMainCompaniesFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkCompaniesFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}

// Check Social Enterprise Filter
def checkSocialEnterpriseFilter(boolean everything_social_enterprises_entity_exist, TestObject everything_social_enterprises_entity) {
	if(!everything_social_enterprises_entity_exist) {
		TestHelper.thrownException("Filter social enterprise not exist");
	 }
	
	TestHelper.clickButton(everything_social_enterprises_entity);
	SearchFilterHelper.checkMainCompaniesFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkCompaniesFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}

// Check products Filter
def checkProductsFilter(boolean everything_products_entity_exist, TestObject everything_products_entity) {
	if(!everything_products_entity_exist) {
		TestHelper.thrownException("Filter social enterprise not exist");
	 }
	
	TestHelper.clickButton(everything_products_entity);
	SearchFilterHelper.checkMainNeedsFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkNeedsFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}

// Check Services Filter
def checkServiceFilter(boolean everything_services_entity_exist, TestObject everything_services_entity) {
	if(!everything_services_entity_exist) {
		TestHelper.thrownException("Filter service not exist");
	 }
	
	TestHelper.clickButton(everything_services_entity);
	SearchFilterHelper.checkMainNeedsFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkNeedsFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}

// Check Franchise Filter
def checkFranchisesFilter(boolean everything_franchises_entity_exist, TestObject everything_franchises_entity) {
	if(!everything_franchises_entity_exist) {
		TestHelper.thrownException("Filter franchise not exist");
	 }
		
	TestHelper.clickButton(everything_franchises_entity);
	SearchFilterHelper.checkMainNeedsFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkNeedsFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}

// Check Organization Filter
def checkOrgnizationFilter(boolean everything_organizations_entity_exist, TestObject everything_organizations_entity) {
	if(!everything_organizations_entity_exist) {
		TestHelper.thrownException("Filter organization not exist");
	 }
	
	TestHelper.clickButton(everything_organizations_entity);
	SearchFilterHelper.checkMainOrganizationFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkOrganizationFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}

// Check Hometown Filter
def checkHometownFilter(boolean everything_hometowns_entity_exist, TestObject everything_hometowns_entity) {
	if(!everything_hometowns_entity_exist) {
		TestHelper.thrownException("Filter organization not exist");
	 }
	
	TestHelper.clickButton(everything_hometowns_entity);
	SearchFilterHelper.checkMainHometownsFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkHometownsFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}

// Check Diplomatic Filter
def checkDiplomaticFilter(boolean everything_diplomatic_missions_entity_exist, TestObject everything_diplomatic_missions_entity) {
	if(!everything_diplomatic_missions_entity_exist) {
		TestHelper.thrownException("Filter diplomatic not exist");
	 }
	
	TestHelper.clickButton(everything_diplomatic_missions_entity);
	SearchFilterHelper.checkMainDiplomaticFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkDiplomaticFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}

// Check News Filter
def checkNewsFilter(boolean everything_news_entity_exist, TestObject everything_news_entity) {
	if(!everything_news_entity_exist) {
		TestHelper.thrownException("Filter diplomatic not exist");
	 }
	
	TestHelper.clickButton(everything_news_entity);
	SearchFilterHelper.checkMainNewsFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkNewsFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}

// Check Event Filter
def checkEventsFilter(boolean everything_events_entity_exist, TestObject everything_events_entity) {
	if(!everything_events_entity_exist) {
		TestHelper.thrownException("Filter events not exist");
	 }
	
	TestHelper.clickButton(everything_events_entity);
	SearchFilterHelper.checkMainEventsFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkEventsFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}

 
