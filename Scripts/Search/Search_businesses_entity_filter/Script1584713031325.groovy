import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Search/Login_For_Search'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.delay(2);
TestObject businessTab = TestHelper.getObjectById("//ion-segment-button[@id='search-page-business-tab']")
TestHelper.clickButton(businessTab);

TestObject businesses_companies_entity = TestHelper.getObjectById("//div[@id='Companies_businesses_search_entity']")
TestObject businesses_startups_entity = TestHelper.getObjectById("//div[@id='Startups_businesses_search_entity']")
TestObject businesses_social_enterprises_entity = TestHelper.getObjectById("//div[@id='Social Enterprises_businesses_search_entity']")
TestObject businesses_products_entity = TestHelper.getObjectById("//div[@id='Products_businesses_search_entity']")
TestObject businesses_services_entity = TestHelper.getObjectById("//div[@id='Services_businesses_search_entity']")
TestObject businesses_franchises_entity = TestHelper.getObjectById("//div[@id='Franchises_businesses_search_entity']")

boolean businesses_companies_entity_exist = TestHelper.isElementPresent(businesses_companies_entity);
boolean businesses_startups_entity_exist = TestHelper.isElementPresent(businesses_startups_entity);
boolean businesses_social_enterprises_entity_exist = TestHelper.isElementPresent(businesses_social_enterprises_entity);
boolean businesses_products_entity_exist = TestHelper.isElementPresent(businesses_products_entity);
boolean businesses_services_entity_exist = TestHelper.isElementPresent(businesses_services_entity);
boolean businesses_franchises_entity_exist = TestHelper.isElementPresent(businesses_franchises_entity);
 
checkCompaniesFilter(businesses_companies_entity_exist, businesses_companies_entity);
checkStartupFilter(businesses_startups_entity_exist, businesses_startups_entity);
checkSocialEnterpriseFilter(businesses_social_enterprises_entity_exist, businesses_social_enterprises_entity);
checkProductsFilter(businesses_products_entity_exist, businesses_products_entity);
checkServiceFilter(businesses_services_entity_exist, businesses_services_entity);
checkFranchisesFilter(businesses_franchises_entity_exist, businesses_franchises_entity);

WebUI.delay(2);
WebUI.closeBrowser();

// Check Companies Filter
def checkCompaniesFilter(boolean businesses_companies_entity_exist, TestObject businesses_companies_entity) {
	if(!businesses_companies_entity_exist) {
		TestHelper.thrownException("Filter companies not exist");
	}
	
	TestHelper.clickButton(businesses_companies_entity);
	SearchFilterHelper.checkMainCompaniesFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkCompaniesFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}

// Check Startup Filter
def checkStartupFilter(boolean businesses_startups_entity_exist, TestObject businesses_startups_entity) {
	if(!businesses_startups_entity_exist) {
		TestHelper.thrownException("Filter startup not exist");
	 }
	
	TestHelper.clickButton(businesses_startups_entity);
	SearchFilterHelper.checkMainCompaniesFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkCompaniesFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}

// Check Social Enterprise Filter
def checkSocialEnterpriseFilter(boolean businesses_social_enterprises_entity_exist, TestObject businesses_social_enterprises_entity) {
	if(!businesses_social_enterprises_entity_exist) {
		TestHelper.thrownException("Filter social enterprise not exist");
	 }
	
	TestHelper.clickButton(businesses_social_enterprises_entity);
	SearchFilterHelper.checkMainCompaniesFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkCompaniesFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}

// Check products Filter
def checkProductsFilter(boolean businesses_products_entity_exist, TestObject businesses_products_entity) {
	if(!businesses_products_entity_exist) {
		TestHelper.thrownException("Filter social enterprise not exist");
	 }
	
	TestHelper.clickButton(businesses_products_entity);
	SearchFilterHelper.checkMainNeedsFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkNeedsFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}

// Check Services Filter
def checkServiceFilter(boolean businesses_services_entity_exist, TestObject businesses_services_entity) {
	if(!businesses_services_entity_exist) {
		TestHelper.thrownException("Filter service not exist");
	 }
	
	TestHelper.clickButton(businesses_services_entity);
	SearchFilterHelper.checkMainNeedsFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkNeedsFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}

// Check Franchise Filter
def checkFranchisesFilter(boolean businesses_franchises_entity_exist, TestObject businesses_franchises_entity) {
	if(!businesses_franchises_entity_exist) {
		TestHelper.thrownException("Filter franchise not exist");
	 }
		
	TestHelper.clickButton(businesses_franchises_entity);
	SearchFilterHelper.checkMainNeedsFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkNeedsFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}
