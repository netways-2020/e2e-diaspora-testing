import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Search/Login_For_Search'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.delay(2);
TestObject networkTab = TestHelper.getObjectById("//ion-segment-button[@id='search-page-network-tab']")
TestHelper.clickButton(networkTab);

TestObject networks_organizations_entity = TestHelper.getObjectById("//div[@id='Organizations_networks_search_entity']")
TestObject networks_hometowns_entity = TestHelper.getObjectById("//div[@id='Hometowns_networks_search_entity']")
TestObject networks_diplomatics_entity = TestHelper.getObjectById("//div[@id='Diplomatic Missions_networks_search_entity']")
TestObject networks_news_entity = TestHelper.getObjectById("//div[@id='News_networks_search_entity']")
TestObject networks_events_entity = TestHelper.getObjectById("//div[@id='Events_networks_search_entity']")

boolean networks_organizations_entity_exist = TestHelper.isElementPresent(networks_organizations_entity);
boolean networks_hometowns_entity_exist = TestHelper.isElementPresent(networks_hometowns_entity);
boolean networks_diplomatics_entity_exist = TestHelper.isElementPresent(networks_diplomatics_entity);
boolean networks_news_entity_exist = TestHelper.isElementPresent(networks_news_entity);
boolean networks_events_entity_exist = TestHelper.isElementPresent(networks_events_entity);

checkOrgnizationFilter(networks_organizations_entity_exist, networks_organizations_entity);
checkHometownFilter(networks_hometowns_entity_exist, networks_hometowns_entity);
checkDiplomaticFilter(networks_diplomatics_entity_exist, networks_diplomatics_entity);
checkNewsFilter(networks_news_entity_exist, networks_news_entity);
checkEventsFilter(networks_events_entity_exist, networks_events_entity);
WebUI.delay(3);
WebUI.closeBrowser();


// Check Organization Filter
def checkOrgnizationFilter(boolean networks_organizations_entity_exist, TestObject networks_organizations_entity) {
	if(!networks_organizations_entity_exist) {
		TestHelper.thrownException("Filter organization not exist");
	 }
	
	TestHelper.clickButton(networks_organizations_entity);
	SearchFilterHelper.checkMainOrganizationFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkOrganizationFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}

// Check Hometown Filter
def checkHometownFilter(boolean networks_hometowns_entity_exist, TestObject networks_hometowns_entity) {
	if(!networks_hometowns_entity_exist) {
		TestHelper.thrownException("Filter organization not exist");
	 }
	
	TestHelper.clickButton(networks_hometowns_entity);
	SearchFilterHelper.checkMainHometownsFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkHometownsFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}

// Check Diplomatic Filter
def checkDiplomaticFilter(boolean networks_diplomatics_entity_exist, TestObject networks_diplomatics_entity) {
	if(!networks_diplomatics_entity_exist) {
		TestHelper.thrownException("Filter diplomatic not exist");
	 }
	
	TestHelper.clickButton(networks_diplomatics_entity);
	SearchFilterHelper.checkMainDiplomaticFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkDiplomaticFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}

// Check News Filter
def checkNewsFilter(boolean networks_news_entity_exist, TestObject networks_news_entity) {
	if(!networks_news_entity_exist) {
		TestHelper.thrownException("Filter diplomatic not exist");
	 }
	
	TestHelper.clickButton(networks_news_entity);
	SearchFilterHelper.checkMainNewsFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkNewsFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}

// Check Event Filter
def checkEventsFilter(boolean networks_events_entity_exist, TestObject networks_events_entity) {
	if(!networks_events_entity_exist) {
		TestHelper.thrownException("Filter events not exist");
	 }
	
	TestHelper.clickButton(networks_events_entity);
	SearchFilterHelper.checkMainEventsFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkEventsFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}

 