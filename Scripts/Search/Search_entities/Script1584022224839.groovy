import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Search/Login_For_Search'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2);
TestObject everythingTab = TestHelper.getObjectById("//ion-segment-button[@id='search-page-everything-tab']")
TestObject peoplesTab = TestHelper.getObjectById("//ion-segment-button[@id='search-page-peoples-tab']")
TestObject networkTab = TestHelper.getObjectById("//ion-segment-button[@id='search-page-network-tab']")
TestObject businessTab = TestHelper.getObjectById("//ion-segment-button[@id='search-page-business-tab']")

// Check Entities
checkEverythingEntities(everythingTab);
checkPeoplesEntities(peoplesTab);
checkNetworksEntities(networkTab);
checkBusinessesEntities(businessTab);
WebUI.delay(3);
WebUI.closeBrowser();

def checkEverythingEntities(TestObject everythingTab) {
	boolean allEntitiesExist = true;
	TestHelper.clickButton(everythingTab);
	
	TestObject everything_allPeople_entity = TestHelper.getObjectById("//div[@id='All People_everything_search_entity']")
	TestObject everything_companies_entity = TestHelper.getObjectById("//div[@id='Companies_everything_search_entity']")
	TestObject everything_startups_entity = TestHelper.getObjectById("//div[@id='Startups_everything_search_entity']")
	TestObject everything_social_enterprises_entity = TestHelper.getObjectById("//div[@id='Social Enterprises_everything_search_entity']")
	TestObject everything_products_entity = TestHelper.getObjectById("//div[@id='Products_everything_search_entity']")
	TestObject everything_services_entity = TestHelper.getObjectById("//div[@id='Services_everything_search_entity']")
	TestObject everything_franchises_entity = TestHelper.getObjectById("//div[@id='Franchises_everything_search_entity']")
	TestObject everything_organizations_entity = TestHelper.getObjectById("//div[@id='Organizations_everything_search_entity']") 
	TestObject everything_hometowns_entity = TestHelper.getObjectById("//div[@id='Hometowns_everything_search_entity']")
	TestObject everything_diplomatic_missions_entity = TestHelper.getObjectById("//div[@id='Diplomatic Missions_everything_search_entity']")
	TestObject everything_news_entity = TestHelper.getObjectById("//div[@id='News_everything_search_entity']")
	TestObject everything_events_entity = TestHelper.getObjectById("//div[@id='Events_everything_search_entity']")
	 
	boolean everything_allPeople_entity_exist = TestHelper.isElementPresent(everything_allPeople_entity);
	boolean everything_companies_entity_exist = TestHelper.isElementPresent(everything_companies_entity);
	boolean everything_startups_entity_exist = TestHelper.isElementPresent(everything_startups_entity);
	boolean everything_social_enterprises_entity_exist = TestHelper.isElementPresent(everything_social_enterprises_entity);
	boolean everything_products_entity_exist = TestHelper.isElementPresent(everything_products_entity);
	boolean everything_services_entity_exist = TestHelper.isElementPresent(everything_services_entity);
	boolean everything_franchises_entity_exist = TestHelper.isElementPresent(everything_franchises_entity);
	boolean everything_organizations_entity_exist = TestHelper.isElementPresent(everything_organizations_entity);
	boolean everything_hometowns_entity_exist = TestHelper.isElementPresent(everything_hometowns_entity);
	boolean everything_diplomatic_missions_entity_exist = TestHelper.isElementPresent(everything_diplomatic_missions_entity);
	boolean everything_news_entity_exist = TestHelper.isElementPresent(everything_news_entity);
	boolean everything_events_entity_exist = TestHelper.isElementPresent(everything_events_entity);
	
	if(!everything_allPeople_entity_exist) {
		allEntitiesExist = false;
	} else if(!everything_companies_entity_exist) {
		allEntitiesExist = false;
	} else if(!everything_startups_entity_exist) {
		allEntitiesExist = false;
	} else if(!everything_social_enterprises_entity_exist) {
		allEntitiesExist = false;
	} else if(!everything_products_entity_exist) {
		allEntitiesExist = false;
	} else if(!everything_services_entity_exist) {
		allEntitiesExist = false;
	} else if(!everything_franchises_entity_exist) {
		allEntitiesExist = false;
	} else if(!everything_organizations_entity_exist) {
		allEntitiesExist = false;
	} else if(!everything_hometowns_entity_exist) {
		allEntitiesExist = false;
	} else if(!everything_diplomatic_missions_entity_exist) {
		allEntitiesExist = false;
	} else if(!everything_news_entity_exist) {
		allEntitiesExist = false;	
	} else if(!everything_events_entity_exist) {
		allEntitiesExist = false;
	} 
	 
	if(allEntitiesExist) {
		println("all everything entities exists");
	} else {
		TestHelper.thrownException("Not all everything entities exists");
	}
}

def checkPeoplesEntities(TestObject peoplesTab) {
	boolean peoplesEntitiesExist = true;
	TestHelper.clickButton(peoplesTab);
	
	TestObject peoples_allPeople_entity = TestHelper.getObjectById("//div[@id='All People_peoples_search_entity']")
	TestObject peoples_town_officials_entity = TestHelper.getObjectById("//div[@id='Town Officials_peoples_search_entity']")
	TestObject peoples_diplomats_entity = TestHelper.getObjectById("//div[@id='Diplomats & Diplomatic Staff_peoples_search_entity']")
	TestObject peoples_company_leadership_entity = TestHelper.getObjectById("//div[@id='Company Leadership_peoples_search_entity']")
	TestObject peoples_employees_entity = TestHelper.getObjectById("//div[@id='Employees_peoples_search_entity']")
	TestObject peoples_professionals_entity = TestHelper.getObjectById("//div[@id='Professionals_peoples_search_entity']")
	TestObject peoples_leaderships_entity = TestHelper.getObjectById("//div[@id='Organization Leadership_peoples_search_entity']")
	TestObject peoples_town_ambassadors_entity = TestHelper.getObjectById("//div[@id='Town Ambassadors_peoples_search_entity']")
	TestObject peoples_mentors_peoples_entity = TestHelper.getObjectById("//div[@id='Mentors_peoples_search_entity']")
	TestObject peoples_advisors_peoples_entity = TestHelper.getObjectById("//div[@id='Advisors_peoples_search_entity']")
	 
	boolean peoples_allPeople_entity_exist = TestHelper.isElementPresent(peoples_allPeople_entity);
	boolean peoples_town_officials_entity_exist = TestHelper.isElementPresent(peoples_town_officials_entity);
	boolean peoples_diplomats_entity_exist = TestHelper.isElementPresent(peoples_diplomats_entity);
	boolean peoples_company_leadership_entity_exist = TestHelper.isElementPresent(peoples_company_leadership_entity);
	boolean peoples_employees_entity_exist =  TestHelper.isElementPresent(peoples_employees_entity);
	boolean peoples_professionals_entity_exist = TestHelper.isElementPresent(peoples_professionals_entity);
	boolean peoples_leaderships_entity_exist = TestHelper.isElementPresent(peoples_leaderships_entity);
	boolean peoples_town_ambassadors_entity_exist = TestHelper.isElementPresent(peoples_town_ambassadors_entity);
	boolean peoples_mentors_peoples_entity_exist = TestHelper.isElementPresent(peoples_mentors_peoples_entity);
	boolean peoples_advisors_peoples_entity_exist = TestHelper.isElementPresent(peoples_advisors_peoples_entity);
	
	
	if(!peoples_allPeople_entity_exist) {
		peoplesEntitiesExist = false;
	} else if(!peoples_town_officials_entity_exist) {
		peoplesEntitiesExist = false;
	} else if(!peoples_diplomats_entity_exist) {
		peoplesEntitiesExist = false;
	} else if(!peoples_company_leadership_entity_exist) {
		peoplesEntitiesExist = false;
	} else if(!peoples_employees_entity_exist) {
		peoplesEntitiesExist = false;
	} else if(!peoples_professionals_entity_exist) {
		peoplesEntitiesExist = false;
	} else if(!peoples_leaderships_entity_exist) {
		peoplesEntitiesExist = false;
	} else if(!peoples_town_ambassadors_entity_exist) {
		peoplesEntitiesExist = false;
	} else if(!peoples_mentors_peoples_entity_exist) {
		peoplesEntitiesExist = false;
	} else if(!peoples_advisors_peoples_entity_exist) {
		peoplesEntitiesExist = false;
	}   
	 
	if(peoplesEntitiesExist) {
		println("All peoples entities exists");
	} else {
		TestHelper.thrownException("Not all peoples entities exists");
	}
}

def checkNetworksEntities(TestObject networkTab ) {
	boolean networksEntitiesExist = true;
	TestHelper.clickButton(networkTab);
	
	TestObject networks_organizations_entity = TestHelper.getObjectById("//div[@id='Organizations_networks_search_entity']")
	TestObject networks_hometowns_entity = TestHelper.getObjectById("//div[@id='Hometowns_networks_search_entity']")
	TestObject networks_diplomatics_entity = TestHelper.getObjectById("//div[@id='Diplomatic Missions_networks_search_entity']")
	TestObject networks_news_entity = TestHelper.getObjectById("//div[@id='News_networks_search_entity']")
	TestObject networks_events_entity = TestHelper.getObjectById("//div[@id='Events_networks_search_entity']")

	boolean networks_organizations_entity_exist = TestHelper.isElementPresent(networks_organizations_entity);
	boolean networks_hometowns_entity_exist = TestHelper.isElementPresent(networks_hometowns_entity);
	boolean networks_diplomatics_entity_exist = TestHelper.isElementPresent(networks_diplomatics_entity);
	boolean networks_news_entity_exist = TestHelper.isElementPresent(networks_news_entity);
	boolean networks_events_entity_exist = TestHelper.isElementPresent(networks_events_entity);
	
	if(!networks_organizations_entity_exist) {
		networksEntitiesExist = false;
	} else if(!networks_hometowns_entity_exist) {
		networksEntitiesExist = false;
	} else if(!networks_diplomatics_entity_exist) {
		networksEntitiesExist = false;
	} else if(!networks_news_entity_exist) {
		networksEntitiesExist = false;
	} else if(!networks_events_entity_exist) {
		networksEntitiesExist = false;
	}
	
	if(networksEntitiesExist) {
		println("All networks entities exists");
	} else {
		TestHelper.thrownException("Not all networks entities exists");
	}
}

def checkBusinessesEntities(TestObject businessTab) {
	boolean businessesEntitiesExist = true;
	TestHelper.clickButton(businessTab);
	
	TestObject businesses_companies_entity = TestHelper.getObjectById("//div[@id='Companies_businesses_search_entity']")
	TestObject businesses_startups_entity = TestHelper.getObjectById("//div[@id='Startups_businesses_search_entity']")
	TestObject businesses_social_enterprises_entity = TestHelper.getObjectById("//div[@id='Social Enterprises_businesses_search_entity']")
	TestObject businesses_products_entity = TestHelper.getObjectById("//div[@id='Products_businesses_search_entity']")
	TestObject businesses_services_entity = TestHelper.getObjectById("//div[@id='Services_businesses_search_entity']")
	TestObject businesses_franchises_entity = TestHelper.getObjectById("//div[@id='Franchises_businesses_search_entity']")
	
	boolean businesses_companies_entity_exist = TestHelper.isElementPresent(businesses_companies_entity);
	boolean businesses_startups_entity_exist = TestHelper.isElementPresent(businesses_startups_entity);
	boolean businesses_social_enterprises_entity_exist = TestHelper.isElementPresent(businesses_social_enterprises_entity);
	boolean businesses_products_entity_exist = TestHelper.isElementPresent(businesses_products_entity);
	boolean businesses_services_entity_exist = TestHelper.isElementPresent(businesses_services_entity);
	boolean businesses_franchises_entity_exist = TestHelper.isElementPresent(businesses_franchises_entity);
	
	if(!businesses_companies_entity_exist) {
		businessesEntitiesExist = false;
	} else if(!businesses_startups_entity_exist) {
		businessesEntitiesExist = false;
	} else if(!businesses_social_enterprises_entity_exist) {
		businessesEntitiesExist = false;
	} else if(!businesses_products_entity_exist) {
		businessesEntitiesExist = false;
	} else if(!businesses_services_entity_exist) {
		businessesEntitiesExist = false;
	} else if(!businesses_franchises_entity_exist) {
		businessesEntitiesExist = false;
	}
	
	if(businessesEntitiesExist) {
		println("All businesses entities exists");
	} else {
		TestHelper.thrownException("Not all businesses entities exists");
	}
}
