import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

String searchKeyword = 'a';

WebUI.callTestCase(findTestCase('Search/Login_For_Search'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.delay(2);



searchByKeywords(searchKeyword);
checkEverythingsResults();
checkPeoplesResults();
checkNetworksResults();
checkBusinessesResults();

WebUI.delay(2);
WebUI.closeBrowser();

def searchByKeywords(String keyword) {
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='search-entity-input-action']"), keyword)
	WebUI.delay(5);
}

// Everything results
def checkEverythingsResults() {
	checkSearchEverythingsResultsTitle();
	checkSearchEverythingsResultsActions();
	checkSearchEverythingsResultsCards();
}
 
def checkSearchEverythingsResultsTitle() {
	TestObject peoples_search_result_title = TestHelper.getObjectById("//div[@id='People_everything_search_result_title']")
	TestObject companies_search_result_title = TestHelper.getObjectById("//div[@id='Companies_everything_search_result_title']")
	TestObject startups_search_result_title = TestHelper.getObjectById("//div[@id='Startups_everything_search_result_title']")
	TestObject social_enterprises_search_result_title = TestHelper.getObjectById("//div[@id='Social Enterprises_everything_search_result_title']")
	TestObject products_search_result_title = TestHelper.getObjectById("//div[@id='Products_everything_search_result_title']")
	TestObject services_search_result_title = TestHelper.getObjectById("//div[@id='Services_everything_search_result_title']")
	TestObject franchises_search_result_title = TestHelper.getObjectById("//div[@id='Franchises_everything_search_result_title']")
	TestObject organizations_search_result_title = TestHelper.getObjectById("//div[@id='Organizations_everything_search_result_title']")
	TestObject hometowns_search_result_title = TestHelper.getObjectById("//div[@id='Hometowns_everything_search_result_title']")
	TestObject diplomatics_search_result_title = TestHelper.getObjectById("//div[@id='Diplomatic Missions_everything_search_result_title']")
	TestObject news_search_result_title = TestHelper.getObjectById("//div[@id='News_everything_search_result_title']")
	TestObject events_search_result_title = TestHelper.getObjectById("//div[@id='Events_everything_search_result_title']")
	 
	boolean peoples_search_result_title_exist = TestHelper.isElementPresent(peoples_search_result_title);
	boolean companies_search_result_title_exist = TestHelper.isElementPresent(companies_search_result_title);
	boolean startups_search_result_title_exist = TestHelper.isElementPresent(startups_search_result_title);
	boolean social_enterprises_search_result_title_exist = TestHelper.isElementPresent(social_enterprises_search_result_title);
	boolean products_search_result_title_exist = TestHelper.isElementPresent(products_search_result_title);
	boolean services_search_result_title_exist = TestHelper.isElementPresent(services_search_result_title);
	boolean franchises_search_result_title_exist = TestHelper.isElementPresent(franchises_search_result_title);
	boolean organizations_search_result_title_exist = TestHelper.isElementPresent(organizations_search_result_title);
	boolean hometowns_search_result_title_exist = TestHelper.isElementPresent(hometowns_search_result_title);
	boolean diplomatics_search_result_title_exist = TestHelper.isElementPresent(diplomatics_search_result_title);
	boolean news_search_result_title_exist = TestHelper.isElementPresent(news_search_result_title);
	boolean events_search_result_title_exist = TestHelper.isElementPresent(events_search_result_title);
	 
	if(!peoples_search_result_title_exist) {
		TestHelper.thrownException("No peoples title found");
	} else
	if(!companies_search_result_title_exist) {
		TestHelper.thrownException("No companies title found");
	} 
	
	else if(!startups_search_result_title_exist) {
		TestHelper.thrownException("No startups title found");
	} 
	
	else if(!social_enterprises_search_result_title_exist) {
		TestHelper.thrownException("No social enterprises title found");
	}
	else if(!products_search_result_title_exist) {
		TestHelper.thrownException("No products title found");
	} else if(!services_search_result_title_exist) {
		TestHelper.thrownException("No services title found");
	} else if(!franchises_search_result_title_exist) {
		TestHelper.thrownException("No franchises title found");
	} else if(!organizations_search_result_title_exist) {
		TestHelper.thrownException("No organizations title found");
	} else if(!hometowns_search_result_title_exist) {
		TestHelper.thrownException("No hometowns title found");
	}
	else if(!diplomatics_search_result_title_exist) {
		TestHelper.thrownException("No diplomatics title found");
	}
	else if(!news_search_result_title_exist) {
		TestHelper.thrownException("No news title found");
	} else if(!events_search_result_title_exist) {
		TestHelper.thrownException("No events title found");
	}
}

def checkSearchEverythingsResultsActions() {
	TestObject peoples_search_result_more_action = TestHelper.getObjectById("//div[@id='People_everything_search_result_more_action']")
	TestObject companies_search_result_more_action = TestHelper.getObjectById("//div[@id='Companies_everything_search_result_more_action']")
	TestObject startups_search_result_more_action = TestHelper.getObjectById("//div[@id='Startups_everything_search_result_more_action']")
	TestObject social_enterprises_search_result_more_action = TestHelper.getObjectById("//div[@id='Social Enterprises_everything_search_result_more_action']")
	TestObject products_search_result_more_action = TestHelper.getObjectById("//div[@id='Products_everything_search_result_more_action']")
	TestObject services_search_result_more_action = TestHelper.getObjectById("//div[@id='Services_everything_search_result_more_action']")
	TestObject franchises_search_result_more_action = TestHelper.getObjectById("//div[@id='Franchises_everything_search_result_more_action']")
	TestObject organizations_search_result_more_action = TestHelper.getObjectById("//div[@id='Organizations_everything_search_result_more_action']")
	TestObject hometowns_search_result_more_action = TestHelper.getObjectById("//div[@id='Hometowns_everything_search_result_more_action']")
	TestObject diplomatics_search_result_more_action = TestHelper.getObjectById("//div[@id='Diplomatic Missions_everything_search_result_more_action']")
	TestObject news_search_result_more_action = TestHelper.getObjectById("//div[@id='News_everything_search_result_more_action']")
	TestObject events_search_result_more_action = TestHelper.getObjectById("//div[@id='Events_everything_search_result_more_action']")
	 
	boolean peoples_search_result_more_action_exist = TestHelper.isElementPresent(peoples_search_result_more_action);
	boolean companies_search_result_more_action_exist = TestHelper.isElementPresent(companies_search_result_more_action);
	boolean startups_search_result_more_action_exist = TestHelper.isElementPresent(startups_search_result_more_action);
	boolean social_enterprises_search_result_more_action_exist = TestHelper.isElementPresent(social_enterprises_search_result_more_action);
	boolean products_search_result_more_action_exist = TestHelper.isElementPresent(products_search_result_more_action);
	boolean services_search_result_more_action_exist = TestHelper.isElementPresent(services_search_result_more_action);
	boolean franchises_search_result_more_action_exist = TestHelper.isElementPresent(franchises_search_result_more_action);
	boolean organizations_search_result_more_action_exist = TestHelper.isElementPresent(organizations_search_result_more_action);
	boolean hometowns_search_result_more_action_exist = TestHelper.isElementPresent(hometowns_search_result_more_action);
	boolean diplomatics_search_result_more_action_exist = TestHelper.isElementPresent(diplomatics_search_result_more_action);
	boolean news_search_result_more_action_exist = TestHelper.isElementPresent(news_search_result_more_action);
	boolean events_search_result_more_action_exist = TestHelper.isElementPresent(events_search_result_more_action);
	 
	if(!peoples_search_result_more_action_exist) {
		TestHelper.thrownException("No peoples more action found");
	} else {
		SearchFilterHelper.checkFilterEntitiesResult(peoples_search_result_more_action, "Everything People")
	}
	
	if(!companies_search_result_more_action_exist) {
		TestHelper.thrownException("No companies more action found");
	} else {
		SearchFilterHelper.checkFilterEntitiesResult(companies_search_result_more_action, "Everything Companies")
	}
	
	if(!startups_search_result_more_action_exist) {
		TestHelper.thrownException("No startups more action found");
	} else {
		SearchFilterHelper.checkFilterEntitiesResult(startups_search_result_more_action, "Everything Startups")
	} 
			
	if(!social_enterprises_search_result_more_action_exist) {
		TestHelper.thrownException("No social enterprises more action found");
	} else {
		SearchFilterHelper.checkFilterEntitiesResult(social_enterprises_search_result_more_action, "Everything Social enterprises")
	}
	 
	if(!products_search_result_more_action_exist) {
		TestHelper.thrownException("No products more action found");
	} else {
		SearchFilterHelper.checkFilterEntitiesResult(products_search_result_more_action, "Everything Products")
	}
	
	if(!services_search_result_more_action_exist) {
		TestHelper.thrownException("No services more action found");
	} else {
		SearchFilterHelper.checkFilterEntitiesResult(services_search_result_more_action, "Everything Services")
	}
	
	if(!franchises_search_result_more_action_exist) {
		TestHelper.thrownException("No franchises more action found");
	} else {
		SearchFilterHelper.checkFilterEntitiesResult(franchises_search_result_more_action, "Everything Franchises")
	}
	
	if(!organizations_search_result_more_action_exist) {
		TestHelper.thrownException("No organizations more action found");
	} else {
		SearchFilterHelper.checkFilterEntitiesResult(organizations_search_result_more_action, "Everything Organizations")
	}
	
	if(!hometowns_search_result_more_action_exist) {
		TestHelper.thrownException("No hometowns more action found");
	} else {
		SearchFilterHelper.checkFilterEntitiesResult(hometowns_search_result_more_action, "Everything Hometown")
	}
	
	if(!diplomatics_search_result_more_action_exist) {
		TestHelper.thrownException("No diplomatics more action found");
	} else {
		SearchFilterHelper.checkFilterEntitiesResult(diplomatics_search_result_more_action, "Everything Diplomatic")
	}
	
	if(!news_search_result_more_action_exist) {
		TestHelper.thrownException("No news more action found");
	} else {
		SearchFilterHelper.checkFilterEntitiesResult(news_search_result_more_action, "Everything News")
	} 
	
	if(!events_search_result_more_action_exist) {
		TestHelper.thrownException("No events more action found");
	} else {
		SearchFilterHelper.checkFilterEntitiesResult(events_search_result_more_action, "Everything Events")
	}
}
 
def checkSearchEverythingsResultsCards() {
	int peoples_search_result_count = TestHelper.countElementByClassName("People_everything_search_result_cards")
	int companies_search_result_count = TestHelper.countElementByClassName("Companies_everything_search_result_cards")
	int startups_search_result_count = TestHelper.countElementByClassName("Startups_everything_search_result_cards")
	int social_enterprises_search_result_count = TestHelper.countElementByClassName("Social Enterprises_everything_search_result_cards")
	int products_search_result_count = TestHelper.countElementByClassName("Products_everything_search_result_cards")
	int services_search_result_count = TestHelper.countElementByClassName("Services_everything_search_result_cards")
	int franchises_search_result_count = TestHelper.countElementByClassName("Franchises_everything_search_result_cards")
	int organizations_search_result_count = TestHelper.countElementByClassName("Organizations_everything_search_result_cards")
	int hometowns_search_result_count = TestHelper.countElementByClassName("Hometowns_everything_search_result_cards")
	int diplomatics_search_result_count = TestHelper.countElementByClassName("Diplomatic Missions_everything_search_result_cards")
	int news_search_result_count =  TestHelper.countElementByClassName("News_everything_search_result_cards")
	int events_search_result_count = TestHelper.countElementByClassName("Events_everything_search_result_cards")
	 
  
	if(peoples_search_result_count < 0) {
		TestHelper.thrownException("No peoples card found");
	} else if(companies_search_result_count < 0) {
		TestHelper.thrownException("No companies card found");
	} 
	else if(startups_search_result_count < 0) {
		TestHelper.thrownException("No startups card found");
	} 
	else if(social_enterprises_search_result_count < 0) {
		TestHelper.thrownException("No social enterprises card found");
	} 
	else if(products_search_result_count < 0) {
		TestHelper.thrownException("No products card found");
	} else if(services_search_result_count < 0) {
		TestHelper.thrownException("No services card found");
	} else if(franchises_search_result_count < 0) {
		TestHelper.thrownException("No franchises card found");
	} else if(organizations_search_result_count < 0) {
		TestHelper.thrownException("No organizations card found");
	} else if(hometowns_search_result_count < 0) {
		TestHelper.thrownException("No hometows card found");
	}
	else if(diplomatics_search_result_count < 0) {
		TestHelper.thrownException("No diplomatics card found");
	}
	else if(news_search_result_count < 0) {
		TestHelper.thrownException("No news card found");
	} else if(events_search_result_count < 0) {
		TestHelper.thrownException("No events card found");
	}
}
  
// Peoples results
def checkPeoplesResults() { 
	TestObject peoplesTab = TestHelper.getObjectById("//ion-segment-button[@id='search-page-peoples-tab']")
	TestHelper.clickButton(peoplesTab)
	checkSearchPeoplesResultsTitle();
	checkSearchPeoplesResultsActions();
	checkSearchPeoplesResultsCards();
}

def checkSearchPeoplesResultsTitle() {
	TestObject peoples_search_result_title = TestHelper.getObjectById("//div[@id='People_peoples_search_result_title']")
	boolean peoples_search_result_title_exist = TestHelper.isElementPresent(peoples_search_result_title);
 
	if(!peoples_search_result_title_exist) {
		TestHelper.thrownException("No peoples title found");
	}  
}
 
def checkSearchPeoplesResultsActions() {
	TestObject peoples_search_result_more_action = TestHelper.getObjectById("//div[@id='People_peoples_search_result_more_action']")
	boolean peoples_search_result_more_action_exist = TestHelper.isElementPresent(peoples_search_result_more_action);
 	 
	if(!peoples_search_result_more_action_exist) {
		TestHelper.thrownException("No peoples more action found");
	} else {
		SearchFilterHelper.checkFilterEntitiesResult(peoples_search_result_more_action, "Peoples Sections People")
	} 
}
 
def checkSearchPeoplesResultsCards() {
	int peoples_search_result_count = TestHelper.countElementByClassName("People_peoples_search_result_cards")   
	if(peoples_search_result_count < 0) {
		TestHelper.thrownException("No peoples card found");
	} 
}


// Networks results
def checkNetworksResults() {
	TestObject networkTab = TestHelper.getObjectById("//ion-segment-button[@id='search-page-network-tab']")
	TestHelper.clickButton(networkTab)
	
	checkSearchNetworksResultsTitle();
	checkSearchNetworksResultsActions();
	checkSearchNetworksResultsCards();
}

def checkSearchNetworksResultsTitle() {
	TestObject organizations_search_result_title = TestHelper.getObjectById("//div[@id='Organizations_networks_search_result_title']")
	TestObject hometowns_search_result_title = TestHelper.getObjectById("//div[@id='Hometowns_networks_search_result_title']")
	TestObject diplomatics_search_result_title = TestHelper.getObjectById("//div[@id='Diplomatic Missions_networks_search_result_title']")
	TestObject news_search_result_title = TestHelper.getObjectById("//div[@id='News_networks_search_result_title']")
	TestObject events_search_result_title = TestHelper.getObjectById("//div[@id='Events_networks_search_result_title']")
	
	boolean organizations_search_result_title_exist = TestHelper.isElementPresent(organizations_search_result_title);
	boolean hometowns_search_result_title_exist = TestHelper.isElementPresent(hometowns_search_result_title);
	boolean diplomatics_search_result_title_exist = TestHelper.isElementPresent(diplomatics_search_result_title);
	boolean news_search_result_title_exist = TestHelper.isElementPresent(news_search_result_title);
	boolean events_search_result_title_exist = TestHelper.isElementPresent(events_search_result_title);
 
	if(!organizations_search_result_title_exist) {
		TestHelper.thrownException("No organizations title found");
	} else if(!hometowns_search_result_title_exist) {
		TestHelper.thrownException("No hometowns title found");
	}
	else if(!diplomatics_search_result_title_exist) {
		TestHelper.thrownException("No diplomatics title found");
	}
	else if(!news_search_result_title_exist) {
		TestHelper.thrownException("No news title found");
	} else if(!events_search_result_title_exist) {
		TestHelper.thrownException("No events title found");
	}
}
 
def checkSearchNetworksResultsActions() {
 		TestObject organizations_search_result_more_action = TestHelper.getObjectById("//div[@id='Organizations_networks_search_result_more_action']")
		TestObject hometowns_search_result_more_action = TestHelper.getObjectById("//div[@id='Hometowns_networks_search_result_more_action']")
		TestObject diplomatics_search_result_more_action = TestHelper.getObjectById("//div[@id='Diplomatic Missions_networks_search_result_more_action']")
		TestObject news_search_result_more_action = TestHelper.getObjectById("//div[@id='News_networks_search_result_more_action']")
		TestObject events_search_result_more_action = TestHelper.getObjectById("//div[@id='Events_networks_search_result_more_action']")
		 
 		boolean organizations_search_result_more_action_exist = TestHelper.isElementPresent(organizations_search_result_more_action);
		boolean hometowns_search_result_more_action_exist = TestHelper.isElementPresent(hometowns_search_result_more_action);
		boolean diplomatics_search_result_more_action_exist = TestHelper.isElementPresent(diplomatics_search_result_more_action);
		boolean news_search_result_more_action_exist = TestHelper.isElementPresent(news_search_result_more_action);
		boolean events_search_result_more_action_exist = TestHelper.isElementPresent(events_search_result_more_action);
		 
	 		 
		if(!organizations_search_result_more_action_exist) {
			TestHelper.thrownException("No organizations more action found");
		} else {
			SearchFilterHelper.checkFilterEntitiesResult(organizations_search_result_more_action, "Everything Organizations")
		}
		
		if(!hometowns_search_result_more_action_exist) {
			TestHelper.thrownException("No hometowns more action found");
		} else {
			SearchFilterHelper.checkFilterEntitiesResult(hometowns_search_result_more_action, "Everything Hometown")
		}
		
		if(!diplomatics_search_result_more_action_exist) {
			TestHelper.thrownException("No diplomatics more action found");
		} else {
			SearchFilterHelper.checkFilterEntitiesResult(diplomatics_search_result_more_action, "Everything Diplomatic")
		}
		
		if(!news_search_result_more_action_exist) {
			TestHelper.thrownException("No news more action found");
		} else {
			SearchFilterHelper.checkFilterEntitiesResult(news_search_result_more_action, "Everything News")
		}
		
		if(!events_search_result_more_action_exist) {
			TestHelper.thrownException("No events more action found");
		} else {
			SearchFilterHelper.checkFilterEntitiesResult(events_search_result_more_action, "Everything Events")
		}
}
 
def checkSearchNetworksResultsCards() {
 	int organizations_search_result_count = TestHelper.countElementByClassName("Organizations_networks_search_result_cards")
	int hometowns_search_result_count = TestHelper.countElementByClassName("Hometowns_networks_search_result_cards")
	int diplomatics_search_result_count = TestHelper.countElementByClassName("Diplomatic Missions_networks_search_result_cards")
	int news_search_result_count =  TestHelper.countElementByClassName("News_networks_search_result_cards")
	int events_search_result_count = TestHelper.countElementByClassName("Events_networks_search_result_cards")
	 
  
	if(organizations_search_result_count < 0) {
		TestHelper.thrownException("No organizations card found");
	} else if(hometowns_search_result_count < 0) {
		TestHelper.thrownException("No hometows card found");
	}
	else if(diplomatics_search_result_count < 0) {
		TestHelper.thrownException("No diplomatics card found");
	}
	else if(news_search_result_count < 0) {
		TestHelper.thrownException("No news card found");
	} else if(events_search_result_count < 0) {
		TestHelper.thrownException("No events card found");
	}
}


// Business results
def checkBusinessesResults() {
	TestObject businessTab = TestHelper.getObjectById("//ion-segment-button[@id='search-page-business-tab']")
	TestHelper.clickButton(businessTab)
	
	checkSearchBusinessesResultsTitle();
	checkSearchBusinessesResultsActions();
	checkSearchBusinessesResultsCards();
}

def checkSearchBusinessesResultsTitle() {
	TestObject companies_search_result_title = TestHelper.getObjectById("//div[@id='Companies_businesses_search_result_title']")
	TestObject startups_search_result_title = TestHelper.getObjectById("//div[@id='Startups_everything_search_result_title']")
	TestObject social_enterprises_search_result_title = TestHelper.getObjectById("//div[@id='Social Enterprises_businesses_search_result_title']")
	TestObject products_search_result_title = TestHelper.getObjectById("//div[@id='Products_businesses_search_result_title']")
	TestObject services_search_result_title = TestHelper.getObjectById("//div[@id='Services_businesses_search_result_title']")
	TestObject franchises_search_result_title = TestHelper.getObjectById("//div[@id='Franchises_businesses_search_result_title']")

	boolean companies_search_result_title_exist = TestHelper.isElementPresent(companies_search_result_title);
	boolean startups_search_result_title_exist = TestHelper.isElementPresent(startups_search_result_title);
	boolean social_enterprises_search_result_title_exist = TestHelper.isElementPresent(social_enterprises_search_result_title);
	boolean products_search_result_title_exist = TestHelper.isElementPresent(products_search_result_title);
	boolean services_search_result_title_exist = TestHelper.isElementPresent(services_search_result_title);
	boolean franchises_search_result_title_exist = TestHelper.isElementPresent(franchises_search_result_title);

	if(!companies_search_result_title_exist) {
		TestHelper.thrownException("No companies title found");
	} 
	else if(!startups_search_result_title_exist) {
		TestHelper.thrownException("No startups title found");
	} 
	else if(!social_enterprises_search_result_title_exist) {
		TestHelper.thrownException("No social enterprises title found");
	}
	else if(!products_search_result_title_exist) {
		TestHelper.thrownException("No products title found");
	} else if(!services_search_result_title_exist) {
		TestHelper.thrownException("No services title found");
	} else if(!franchises_search_result_title_exist) {
		TestHelper.thrownException("No franchises title found");
	} 
}
 
def checkSearchBusinessesResultsActions() {
 		TestObject companies_search_result_more_action = TestHelper.getObjectById("//div[@id='Companies_businesses_search_result_more_action']")
		TestObject startups_search_result_more_action = TestHelper.getObjectById("//div[@id='Startups_businesses_search_result_more_action']")
		TestObject social_enterprises_search_result_more_action = TestHelper.getObjectById("//div[@id='Social Enterprises_businesses_search_result_more_action']")
		TestObject products_search_result_more_action = TestHelper.getObjectById("//div[@id='Products_businesses_search_result_more_action']")
		TestObject services_search_result_more_action = TestHelper.getObjectById("//div[@id='Services_businesses_search_result_more_action']")
		TestObject franchises_search_result_more_action = TestHelper.getObjectById("//div[@id='Franchises_businesses_search_result_more_action']")
   
 		boolean companies_search_result_more_action_exist = TestHelper.isElementPresent(companies_search_result_more_action);
		boolean startups_search_result_more_action_exist = TestHelper.isElementPresent(startups_search_result_more_action);
		boolean social_enterprises_search_result_more_action_exist = TestHelper.isElementPresent(social_enterprises_search_result_more_action);
		boolean products_search_result_more_action_exist = TestHelper.isElementPresent(products_search_result_more_action);
		boolean services_search_result_more_action_exist = TestHelper.isElementPresent(services_search_result_more_action);
		boolean franchises_search_result_more_action_exist = TestHelper.isElementPresent(franchises_search_result_more_action);
	 		 
 		if(!companies_search_result_more_action_exist) {
			TestHelper.thrownException("No companies more action found");
		} else {
			SearchFilterHelper.checkFilterEntitiesResult(companies_search_result_more_action, "Everything Companies")
		}
	
		if(!startups_search_result_more_action_exist) {
			TestHelper.thrownException("No startups more action found");
		} else {
			SearchFilterHelper.checkFilterEntitiesResult(startups_search_result_more_action, "Everything Startups")
		}
	
		if(!social_enterprises_search_result_more_action_exist) {
			TestHelper.thrownException("No social enterprises more action found");
		} else {
			SearchFilterHelper.checkFilterEntitiesResult(social_enterprises_search_result_more_action, "Everything Social enterprises")
		}
		 
		if(!products_search_result_more_action_exist) {
			TestHelper.thrownException("No products more action found");
		} else {
			SearchFilterHelper.checkFilterEntitiesResult(products_search_result_more_action, "Everything Products")
		}
		
		if(!services_search_result_more_action_exist) {
			TestHelper.thrownException("No services more action found");
		} else {
			SearchFilterHelper.checkFilterEntitiesResult(services_search_result_more_action, "Everything Services")
		}
		
		if(!franchises_search_result_more_action_exist) {
			TestHelper.thrownException("No franchises more action found");
		} else {
			SearchFilterHelper.checkFilterEntitiesResult(franchises_search_result_more_action, "Everything Franchises")
		}
		 
}
 
def checkSearchBusinessesResultsCards() {
 	int companies_search_result_count = TestHelper.countElementByClassName("Companies_businesses_search_result_cards")
	int startups_search_result_count = TestHelper.countElementByClassName("Startups_businesses_search_result_cards")
	int social_enterprises_search_result_count = TestHelper.countElementByClassName("Social Enterprises_businesses_search_result_cards")
	int products_search_result_count = TestHelper.countElementByClassName("Products_businesses_search_result_cards")
	int services_search_result_count = TestHelper.countElementByClassName("Services_businesses_search_result_cards")
	int franchises_search_result_count = TestHelper.countElementByClassName("Franchises_businesses_search_result_cards")
 
	if(companies_search_result_count < 0) {
		TestHelper.thrownException("No companies card found");
	} 
	else if(startups_search_result_count < 0) {
		TestHelper.thrownException("No startups card found");
	} 
	else if(social_enterprises_search_result_count < 0) {
		TestHelper.thrownException("No social enterprises card found");
	} 
	else if(products_search_result_count < 0) {
		TestHelper.thrownException("No products card found");
	} else if(services_search_result_count < 0) {
		TestHelper.thrownException("No services card found");
	} else if(franchises_search_result_count < 0) {
		TestHelper.thrownException("No franchises card found");
	} 
}


