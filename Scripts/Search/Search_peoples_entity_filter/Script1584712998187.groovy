import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable


WebUI.callTestCase(findTestCase('Search/Login_For_Search'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.delay(2);
TestObject peoplesTab = TestHelper.getObjectById("//ion-segment-button[@id='search-page-peoples-tab']")
TestHelper.clickButton(peoplesTab);

TestObject peoples_allPeople_entity = TestHelper.getObjectById("//div[@id='All People_peoples_search_entity']")
TestObject peoples_town_officials_entity = TestHelper.getObjectById("//div[@id='Town Officials_peoples_search_entity']")
TestObject peoples_diplomats_entity = TestHelper.getObjectById("//div[@id='Diplomats & Diplomatic Staff_peoples_search_entity']")
TestObject peoples_company_leadership_entity = TestHelper.getObjectById("//div[@id='Company Leadership_peoples_search_entity']")
TestObject peoples_employees_entity = TestHelper.getObjectById("//div[@id='Employees_peoples_search_entity']")
TestObject peoples_professionals_entity = TestHelper.getObjectById("//div[@id='Professionals_peoples_search_entity']")
TestObject peoples_leaderships_entity = TestHelper.getObjectById("//div[@id='Organization Leadership_peoples_search_entity']")
TestObject peoples_town_ambassadors_entity = TestHelper.getObjectById("//div[@id='Town Ambassadors_peoples_search_entity']")
TestObject peoples_mentors_peoples_entity = TestHelper.getObjectById("//div[@id='Mentors_peoples_search_entity']")
TestObject peoples_advisors_peoples_entity = TestHelper.getObjectById("//div[@id='Advisors_peoples_search_entity']")
 
boolean peoples_allPeople_entity_exist = TestHelper.isElementPresent(peoples_allPeople_entity);
boolean peoples_town_officials_entity_exist = TestHelper.isElementPresent(peoples_town_officials_entity);
boolean peoples_diplomats_entity_exist = TestHelper.isElementPresent(peoples_diplomats_entity);
boolean peoples_company_leadership_entity_exist = TestHelper.isElementPresent(peoples_company_leadership_entity);
boolean peoples_employees_entity_exist =  TestHelper.isElementPresent(peoples_employees_entity);
boolean peoples_professionals_entity_exist = TestHelper.isElementPresent(peoples_professionals_entity);
boolean peoples_leaderships_entity_exist = TestHelper.isElementPresent(peoples_leaderships_entity);
boolean peoples_town_ambassadors_entity_exist = TestHelper.isElementPresent(peoples_town_ambassadors_entity);
boolean peoples_mentors_peoples_entity_exist = TestHelper.isElementPresent(peoples_mentors_peoples_entity);
boolean peoples_advisors_peoples_entity_exist = TestHelper.isElementPresent(peoples_advisors_peoples_entity);

checkAllPeoplesFilter(peoples_allPeople_entity_exist, peoples_allPeople_entity);
checkTownOfficialsFilter(peoples_town_officials_entity_exist, peoples_town_officials_entity);
checkDiplomatsFilter(peoples_diplomats_entity_exist, peoples_diplomats_entity);
checkCompanyLeadershipFilter(peoples_company_leadership_entity_exist, peoples_company_leadership_entity);
checkEmployeesFilter(peoples_employees_entity_exist, peoples_employees_entity);
checkProfessionalsFilter(peoples_professionals_entity_exist, peoples_professionals_entity);
checkOrganizationLeadershipsFilter(peoples_leaderships_entity_exist, peoples_leaderships_entity);
checkTownAmbassadorsFilter(peoples_town_ambassadors_entity_exist, peoples_town_ambassadors_entity);
checkMentorsFilter(peoples_mentors_peoples_entity_exist, peoples_mentors_peoples_entity);
checkAdvisorsFilter(peoples_advisors_peoples_entity_exist, peoples_advisors_peoples_entity);
WebUI.delay(3);
WebUI.closeBrowser();

// Check All People Filter
def checkAllPeoplesFilter(boolean everything_allPeople_entity_exist, TestObject everything_allPeople_entity) {
	if(!everything_allPeople_entity_exist) {
		TestHelper.thrownException("Filter peoples not exist");
	 }
	
	TestHelper.clickButton(everything_allPeople_entity);
	SearchFilterHelper.checkMainPeopleFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkAllPeopleFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}

// Check Town Official Filter
def checkTownOfficialsFilter(boolean peoples_town_officials_entity_exist, TestObject peoples_town_officials_entity) {
	if(!peoples_town_officials_entity_exist) {
		TestHelper.thrownException("Filter town official not exist");
	 }
	
	TestHelper.clickButton(peoples_town_officials_entity);
	SearchFilterHelper.checkMainTownOfficialFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkTownOfficialFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}

// Check Diplomats Filter
def checkDiplomatsFilter(boolean peoples_diplomats_entity_exist, TestObject peoples_diplomats_entity) {
		if(!peoples_diplomats_entity_exist) {
		TestHelper.thrownException("Filter Diplomats not exist");
	 }
	
	TestHelper.clickButton(peoples_diplomats_entity);
	SearchFilterHelper.checkMainDiplomatsFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkDiplomatsFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}

// Check Company leadership Filter
def checkCompanyLeadershipFilter(boolean peoples_company_leadership_entity_exist, TestObject peoples_company_leadership_entity) {
		if(!peoples_company_leadership_entity_exist) {
		TestHelper.thrownException("Filter company leadership not exist");
	 }
	
	TestHelper.clickButton(peoples_company_leadership_entity);
	SearchFilterHelper.checkMainCompanyLeadershipFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkCompanyLeadershipFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}

// Check peoples employees Filter
def checkEmployeesFilter(boolean peoples_employees_entity_exist, TestObject peoples_employees_entity) {
		if(!peoples_employees_entity_exist) {
		TestHelper.thrownException("Filter peoples employees not exist");
	 }
	
	TestHelper.clickButton(peoples_employees_entity);
	SearchFilterHelper.checkMainCompanyLeadershipFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkCompanyLeadershipFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}

// Check Professionals Filter
def checkProfessionalsFilter(boolean peoples_professionals_entity_exist, TestObject peoples_professionals_entity) {
	if(!peoples_professionals_entity_exist) {
		TestHelper.thrownException("Filter professionals not exist");
	 }
	
	TestHelper.clickButton(peoples_professionals_entity);
	SearchFilterHelper.checkMainPeopleFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkAllPeopleFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}

// Check Leaderships Filter
def checkOrganizationLeadershipsFilter(boolean peoples_leaderships_entity_exist, TestObject peoples_leaderships_entity) {
	if(!peoples_leaderships_entity_exist) {
		TestHelper.thrownException("Filter leaderships not exist");
	 }
		
	TestHelper.clickButton(peoples_leaderships_entity);
	SearchFilterHelper.checkMainOrganizationLeadershipFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkOrganizationLeadershipFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}

// Check Town Ambassadors Filter
def checkTownAmbassadorsFilter(boolean peoples_town_ambassadors_entity_exist, TestObject peoples_town_ambassadors_entity) {
	if(!peoples_town_ambassadors_entity_exist) {
		TestHelper.thrownException("Filter town ambassadors not exist");
	 }
	
	TestHelper.clickButton(peoples_town_ambassadors_entity);
	SearchFilterHelper.checkMainPeopleFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkAllPeopleFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}

// Check Mentors Filter
def checkMentorsFilter(boolean peoples_mentors_peoples_entity_exist, TestObject peoples_mentors_peoples_entity) {
	if(!peoples_mentors_peoples_entity_exist) {
		TestHelper.thrownException("Filter mentors not exist");
	 }
	
	TestHelper.clickButton(peoples_mentors_peoples_entity);
	SearchFilterHelper.checkMainPeopleFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkAllPeopleFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}

// Check Advisors Filter
def checkAdvisorsFilter(boolean peoples_advisors_peoples_entity_exist, TestObject peoples_advisors_peoples_entity) {
	if(!peoples_advisors_peoples_entity_exist) {
		TestHelper.thrownException("Filter advisors not exist");
	 }
	
	TestHelper.clickButton(peoples_advisors_peoples_entity);
	SearchFilterHelper.checkMainPeopleFilter();
	SearchFilterHelper.checkMainFilterAction();
	SearchFilterHelper.checkAllPeopleFilter();
	SearchFilterHelper.closeFilterComponentsAndGoBackToSearchPage();
}
