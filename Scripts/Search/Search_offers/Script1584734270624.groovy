import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

long now = new Date().getTime();
String companyName = 'Netways' + new Date().getTime()
String roleName = "Founder"

String offerProduct_Title = 'new product title ' + now;
String offerProduct_Description = 'new product description ' + now;
 
String offerService_Title = 'new service title ' + now;
String offerService_Description = 'new service description ' + now;

String offerFranchise_Title = 'new franchise title ' + now;
String offerFranchise_Description = 'new franchise description ' + now;


WebUI.callTestCase(findTestCase('MarketPlace/Register_User_For_MarketPlace'), [:], FailureHandling.STOP_ON_FAILURE)

// Check if has companies
TestObject noCompanies_emptyStateObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-companies-added-empty-state']")
boolean noCompanies_emptyStateObjectPresent = TestHelper.isElementPresent(noCompanies_emptyStateObject)


if (noCompanies_emptyStateObjectPresent) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='add_company_action']"));
	UserRoleHelper.createCompany(companyName, roleName)
	WebUI.delay(2);
	checkOfferEmptyStateIsExist();
	
	addProduct(offerProduct_Title, offerProduct_Description);	
	WebUI.delay(2);
	
	addService(offerService_Title, offerService_Description);
	WebUI.delay(2);

	addFranchise(offerFranchise_Title, offerFranchise_Description);
	WebUI.delay(2);
	
	// Go to searhc page
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='home_tab_id']"))
	TestHelper.clickButton(TestHelper.getObjectById("//input[@id='search-input-action']"));
	
	// Check offer added
	checkAddedOfferExistInSearch(offerProduct_Title, offerProduct_Description);
	checkAddedServiceExistInSearch(offerService_Title, offerService_Description)
	checkAddedFranchiseExistInSearch(offerFranchise_Title, offerFranchise_Description);
	
	WebUI.delay(2);
	WebUI.closeBrowser();

} else {
	checkOfferEmptyStateIsExist();
}

def checkOfferEmptyStateIsExist() {
	TestObject noOffer_emptyStateObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='add_new_offer_action']")
	boolean noOffer_emptyStateObjectPresent = TestHelper.isElementPresent(noOffer_emptyStateObject)

	if(!noOffer_emptyStateObjectPresent) {
		TestHelper.thrownException("add offer action not present");
	}
}

// Add offers
def addProduct(String prodcutTitle, String productDescription) {	
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='add_new_offer_action']"));
	TestHelper.clickButton(TestHelper.getObjectById("//a[@id='List a Product_user_action']"))
	UserActionsHelper.addNeedOrOffer(prodcutTitle, productDescription);
}

def addService(String serviceTitle, String serviceDescription) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='add_new_offer_action']"));
	TestHelper.clickButton(TestHelper.getObjectById("//a[@id='List a Service_user_action']"))
	UserActionsHelper.addNeedOrOffer(serviceTitle, serviceDescription);
}

def addFranchise(String franchiseTitle, String franchiseDescription) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='add_new_offer_action']"));
	TestHelper.clickButton(TestHelper.getObjectById("//a[@id='List a Franchise_user_action']"))
	UserActionsHelper.addNeedOrOffer(franchiseTitle, franchiseDescription);
}

// Check offer entities in search
def checkAddedOfferExistInSearch(String prodcutTitle, String productDescription) {
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='search-entity-input-action']"), prodcutTitle);
	WebUI.delay(5);
	
	TestObject products_search_result_title = TestHelper.getObjectById("//div[@id='Products_everything_search_result_title']")
	TestObject offerObject = TestHelper.getObjectById("//span[@id='" + prodcutTitle + "_offer_id']")
	boolean offerObjectPresent = TestHelper.isElementPresent(offerObject)

	if (!offerObjectPresent) {
		TestHelper.thrownException("Offer added not exist in the search results");
	}
	
}

def checkAddedServiceExistInSearch(String serviceTitle, String serviceDescription) {
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='search-entity-input-action']"), serviceTitle);
	WebUI.delay(5);
	
	TestObject services_search_result_title = TestHelper.getObjectById("//div[@id='Services_everything_search_result_title']")
	TestObject serviceObject = TestHelper.getObjectById("//span[@id='" + serviceTitle + "_offer_id']")
	boolean serviceObjectPresent = TestHelper.isElementPresent(serviceObject)

	if (!serviceObjectPresent) {
		TestHelper.thrownException("Service added not exist in the search results");
	}
}

def checkAddedFranchiseExistInSearch(String franchiseTitle, String franchiseDescription) {
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='search-entity-input-action']"), franchiseTitle);
	WebUI.delay(5);
	
	
	TestObject franchises_search_result_title = TestHelper.getObjectById("//div[@id='Franchises_everything_search_result_title']")
	TestObject franchiseObject = TestHelper.getObjectById("//span[@id='" + franchiseTitle + "_offer_id']")
	boolean franchiseObjectPresent = TestHelper.isElementPresent(franchiseObject)

	if (!franchiseObjectPresent) {
		TestHelper.thrownException("Franchise added not exist in the search results");
	}
}