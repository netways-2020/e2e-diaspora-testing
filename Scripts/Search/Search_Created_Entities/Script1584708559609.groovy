import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement

long date = new Date().getTime();

// New User
String registeredEmail = 'netways_user' + date + '@gmail.com'
String password = 'cvW8qx4B2o3F4VwP/kNsqA=='
String firstName = 'First_Name_For_Search' + date;
String lastName = 'Last_Name_For_Search' + date;
String city = 'Hermel';
String fullName = firstName + ' ' + lastName;

// Organization and company
String organizationName = 'Netways_Organization' + date;
String companyName = 'Netways' + date;
String roleName = "Founder"


TestHelper.registerNewUser(registeredEmail, password, firstName, lastName, city, true);
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_tab_id']"));
  
TestHelper.goToManageEntity();

checkOrganization(organizationName, roleName);
checkCompany(companyName, roleName);

checkCreatedCompanyExistInSearch(companyName);
checkCreatedOrganizationExistInSearch(organizationName);
checkCreatedProfileExistInSearch(fullName);

WebUI.delay(2);
WebUI.closeBrowser();

// Check Organization
def checkOrganization (String organizationName, String roleName) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Network Roles_manage_entity_action']"));
	TestObject emptyStateObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-network-role-empty-state']")
	boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)

	if (emptyStateObjectPresent == true) {
		println("hasn't network roles")
 
		TestObject emptyStateAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-network-role-empty-state-button']")
		TestHelper.clickButton(emptyStateAction)

		// Create organization, add get created organization url
		UserRoleHelper.createOrganization(organizationName, roleName);
		 
		TestObject manageNetworkBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_network_role_back_action']")
		WebUI.waitForElementPresent(manageNetworkBackButton, 5)

		getOrganizationName(organizationName, roleName);
	} else {
		println("has network roles")
		TestObject addNetworkRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='add-new-network-role-action']")
		TestHelper.clickButton(addNetworkRoleAction)
		
		// Create organization, add get created organization url
		UserRoleHelper.createOrganization(organizationName, roleName);
		TestObject manageNetworkBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_network_role_back_action']")
		WebUI.waitForElementPresent(manageNetworkBackButton, 5)
		getOrganizationName(organizationName, roleName);
	}
}

def getOrganizationName(String organizationName, String roleName) {
	String networkRoleId = organizationName + '-' + roleName + '-role';
	String networkRoleIdXpath = "//div[@id='"+networkRoleId+"']"
	TestObject networkRoleObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, networkRoleIdXpath)
	TestHelper.clickButton(networkRoleObject);
	 
	WebUI.delay(6);
	organizationUrl = WebUI.getUrl();	 
	WebElement organizationNameObject = TestHelper.getItem('organization-name-title', 0)
	selectedOrganizationName = organizationNameObject.text;
	
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='organization_back_button']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_network_role_back_action']"));
}

// Check Company
def checkCompany(String companyName, String roleName) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Business Roles_manage_entity_action']"));
	
	TestObject emptyStateObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-business-role-empty-state\']')
	boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)

	if (emptyStateObjectPresent == true) {
		println('hasn\'t business roles')

		TestObject emptyStateAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-business-role-empty-state-button\']')
		WebUI.click(emptyStateAction)
		createNewCompany(companyName, roleName)

	} else {
		println('has business roles')

		TestObject addBusinessRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'add-new-business-role-action\']')
		WebUI.click(addBusinessRoleAction)
		createNewCompany(companyName, roleName)
	}
}
 
def createNewCompany(String companyName, String roleName) {
	UserRoleHelper.createCompany(companyName, roleName)
	WebUI.delay(2);
	String companyTarget = companyName + '-' + roleName + '-role'
	TestObject selectedCompany = TestHelper.getObjectById("//div[@id='"+companyTarget+"']")
	TestHelper.clickButton(selectedCompany);
	companyUrl = WebUI.getUrl();
	WebElement companyNameObject = TestHelper.getItem('company-name-title', 0)
	selectedCompanyName = companyNameObject.text;
	
	
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='company_back_button']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_business_role_back_action']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-User-back-button']"))
	TestHelper.clickButton(TestHelper.getObjectById("//input[@id='profile-search-input-action']"))
}


// Check created entities in search
def checkCreatedCompanyExistInSearch(String companyName) {
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='search-entity-input-action']"), companyName);
	WebUI.delay(5);
	
	TestObject createdCompanyObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='title_name_" + companyName + "']")
	boolean createdCompanyObjectPresent = TestHelper.isElementPresent(createdCompanyObject)

	if (!createdCompanyObjectPresent) {
		TestHelper.thrownException("Created company not exist in the search results");
	}
	
}

def checkCreatedOrganizationExistInSearch(String organizationName) {
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='search-entity-input-action']"), organizationName);
	WebUI.delay(5);
	
	TestObject createdOrganizationObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='title_name_" + organizationName + "']")
	boolean createdOrganizationObjectPresent = TestHelper.isElementPresent(createdOrganizationObject)

	if (!createdOrganizationObjectPresent) {
		TestHelper.thrownException("Created organization not exist in the search results");
	}
}

def checkCreatedProfileExistInSearch(String profileName) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='search-page-back-button']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_manage_accounts']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='logout-id']"))
	TestHelper.loginWithDiasporaUser();
	TestHelper.clickButton(TestHelper.getObjectById("//input[@id='search-input-action']"))
	
	
	
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='search-entity-input-action']"), profileName);
	WebUI.delay(5);
	
	TestObject createdProfileObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='title_name_" + profileName + "']")
	boolean createdProfileObjectPresent = TestHelper.isElementPresent(createdProfileObject)

	if (!createdProfileObjectPresent) {
		TestHelper.thrownException("Created profile not exist in the search results");
	}
}

