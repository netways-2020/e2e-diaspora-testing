import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

// Founder User
long date = new Date().getTime();
String registeredEmail = 'netways_user' + date + '@gmail.com'
String password = 'cvW8qx4B2o3F4VwP/kNsqA==';
String firstName = 'FirstName_' + date;
String lastName = 'LastName_' + date;
String memberName = firstName + ' ' + lastName;
String city = 'Hermel';

String organizationName = 'Lebanese Mentors Hub';
String employeeRoleName = 'Engineer';
String updatedEmployeeRoleName = 'Soccer Player';

createProfileAndJoinOrganization(organizationName, employeeRoleName, updatedEmployeeRoleName, registeredEmail, password, firstName, lastName, city)

WebUI.delay(2);
WebUI.closeBrowser();


def createProfileAndJoinOrganization(String organizationName, String employeeRoleName, String updatedEmployeeRoleName, String registeredEmail, 
	String password, String firstName, String lastName, String city) {
	TestHelper.registerNewUser(registeredEmail, password, firstName, lastName, city, true)
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_tab_id']"))
	TestHelper.goToManageEntity()
	TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'Network Roles_manage_entity_action\']'))
	TestObject emptyStateObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-network-role-empty-state\']')
	boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)
	
	if (emptyStateObjectPresent == true) {
		println('hasn\'t networks roles')
	
		TestObject emptyStateAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-network-role-empty-state-button\']')
		WebUI.click(emptyStateAction)
		joinMentorOrganization(organizationName, employeeRoleName)
		editMentorOrganizationRole(organizationName, updatedEmployeeRoleName);
	
	} else {
		println('has network roles')
	
		TestObject addOrganizationRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'add-new-network-role-action\']')
		WebUI.click(addOrganizationRoleAction)
		joinMentorOrganization(organizationName, employeeRoleName);
		editMentorOrganizationRole(organizationName, updatedEmployeeRoleName);
	}
}


def joinMentorOrganization(String organizationName, String employeeRoleName) {
	TestObject searchForOrganizationInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='searchForOrganizationInput']")
	TestHelper.setInputValue(searchForOrganizationInput, organizationName)

	String organizationId = organizationName + '_search_for_organization';
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='"+organizationId+"']"))
	
	UserRoleHelper.joinMentorOrganization();
	String organizationTarget = organizationName +'-Mentor-role'
	TestObject selectedOrganization = TestHelper.getObjectById("//div[@id='"+organizationTarget+"']")
	TestHelper.clickButton(selectedOrganization);
	
	WebUI.delay(2);
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='organization_back_button']"));
	}

def editMentorOrganizationRole(String organizationName, String updatedEmployeeRoleName) {
	String organizationId = organizationName + '-options-icon';
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='"+organizationId+"']"))
 
	TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Edit Role_user_action']"))
	
	UserRoleHelper.editMentorOrganization();
	
	String organizationTarget = organizationName +'-Mentor-role'
	TestObject selectedOrganization = TestHelper.getObjectById("//div[@id='"+organizationTarget+"']")
	TestHelper.clickButton(selectedOrganization);
	
	WebUI.delay(2);
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='organization_back_button']"));
	
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_network_role_back_action']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-User-back-button']"));
}


