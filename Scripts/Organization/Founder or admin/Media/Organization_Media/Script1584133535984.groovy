import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import org.junit.After

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement


// Login and go to organization 
WebUI.callTestCase(findTestCase('Organization/Founder or admin/Authorization for organization/Login_For_Organization'), [:], FailureHandling.STOP_ON_FAILURE)

 
TestHelper.getObjectById("//div[@id='organization_media']")
TestObject emptyStateOrganizationGallery = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-organization-media-empty-state']")
boolean emptyStateOrganizationGalleryExist = TestHelper.isElementPresent(emptyStateOrganizationGallery)

long now = new Date().getTime();
String imageCaption = 'caption' + now;

if (emptyStateOrganizationGalleryExist) {
	println("Hasn't images");
	WebUI.click(emptyStateOrganizationGallery)
	
	UserActionsHelper.addMedia(imageCaption);	
	
	TestObject addPhotoAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='organization_manage_media']")
	WebUI.click(addPhotoAction)
	
	checkIfImageAddedSuccessfully(true, 0); 
	removeImage();
} else { 
	println("Has images");

	TestObject emptyStateAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='organization_manage_media']")
	WebUI.click(emptyStateAction)
 	
	int imageCountBeforAdd = TestHelper.countElementByClassName('gallery-container');
 	
	TestObject addPhotoAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='addPhotoAction']")
	WebUI.click(addPhotoAction)

	UserActionsHelper.addMedia(imageCaption);
	WebUI.waitForElementPresent(addPhotoAction, 10)
	 
	checkIfImageAddedSuccessfully(false, imageCountBeforAdd);
	removeImage()
}

def removeImage() { 
	int imageCountBeforAdd = TestHelper.countElementByClassName('gallery-container');
	WebElement element = TestHelper.getItem('gallery-container', 0);
	element.click();
	
	TestHelper.checkImageLoaded("image_detail_id");

	TestObject imageCaption = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='image-caption-id']")
	boolean imageCaptionExist = TestHelper.isElementPresent(imageCaption);

	if(imageCaptionExist) {
		print("Image caption exist");
	} else {
		TestHelper.thrownException("Image caption not exist");
	}
	
	TestObject removeAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='galler_image_id']")
	boolean removeActionExist = TestHelper.isElementPresent(removeAction);
 
	if(removeActionExist) {
		TestHelper.clickButton(removeAction)
		UserActionsHelper.removeMedia();
		this.checkIfImageRemovedSuccessfully(imageCountBeforAdd);
	} else {
		TestHelper.thrownException("Doesn't have the privilege tot delete an image");
	}	
}


def checkIfImageAddedSuccessfully(boolean firstImageAdded, int imageCountBeforAdd) {
	int imageCount = TestHelper.countElementByClassName('gallery-container');
	 
	if(firstImageAdded) {
		if(imageCount == 1) {
			println("Image added successfully");
		} else {
			TestHelper.thrownException("Image not added successfully");
		}
	} else { 
	   if((imageCountBeforAdd + 1) == imageCount) {
		   println("Image added successfully");
	   } else {
		   TestHelper.thrownException("Image not added successfully");
	   }
	}
}


def checkIfImageRemovedSuccessfully(int imageCountBeforAdd) {
	int imageCount = TestHelper.countElementByClassName('gallery-container');
	
	if((imageCountBeforAdd - 1) == imageCount) {
	   println("Image deleted successfully");
	   WebUI.closeBrowser();
   } else {
	   TestHelper.thrownException("Image not deleted successfully");
   } 
}
 