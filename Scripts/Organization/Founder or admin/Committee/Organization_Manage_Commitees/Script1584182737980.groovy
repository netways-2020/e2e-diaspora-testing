import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import org.openqa.selenium.WebElement as WebElement

long currentDate =  new Date().getTime();
String organizationName = 'Netways_Organization' + currentDate;
String profileName;
String committeeName = 'Netways_Organization_Committee_' + currentDate;
String newCommitteeName = 'New_Netways_Organization_Committee_' + currentDate;

String roleName = 'Founder';

// Login and go to my organization
WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebElement profileNameObject = TestHelper.getItem('profile-name-title', 0)
profileName = profileNameObject.text;

TestHelper.goToManageEntity()
TestObject manageNetworkRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Network Roles_manage_entity_action\']')
TestHelper.clickButton(manageNetworkRoleAction)

goToOrganization(organizationName, roleName);

// Organization info
WebElement organizationNameObject = TestHelper.getItem('organization-name-title', 0)
organizationName = organizationNameObject.text;
  
TestHelper.goToOrganizationManageEntity();
TestObject manageOrganizationCommitteesAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Committees_manage_entity_action']")
TestHelper.clickButton(manageOrganizationCommitteesAction)

// Check if user has committees
TestObject emptyStateObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-organization-committees-empty-state']")
boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)


if (emptyStateObjectPresent) {
	println("hasn't committees")
	TestObject emptyStateAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-organization-committees-empty-state-button']")
	WebUI.click(emptyStateAction)
	
	// add committee
	addCommittee(committeeName);
	
	// join committee
	joinCommittee(committeeName, roleName, profileName)
 
	// add committee member
	addCommitteeMember(committeeName, profileName);
	
	// rename committee
	renameCommittee(newCommitteeName);
	
	// check committee in organization page
	checkCommitteeInOrganizationPage(newCommitteeName);

	// delete committee
	deleteCommittee(newCommitteeName);
	
} else {
	println("has committees")
	TestObject addCommitteeAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='add-new-committee-action']")
	TestHelper.clickButton(addCommitteeAction)
	
	// add committee
	addCommittee(committeeName);
	
	// join committee
	joinCommittee(committeeName, roleName, profileName)
	
	// add committee member
	addCommitteeMember(committeeName, profileName);
	
	// rename committee
	renameCommittee(newCommitteeName);
	
	// check committee in organization page
	checkCommitteeInOrganizationPage(newCommitteeName);
	
	// delete committee
	deleteCommittee(newCommitteeName);
}


def goToOrganization(String organizationName, String roleName) {
//	int organizationsSize = TestHelper.countElementByClassName('manage_user_network_role')
//	if (organizationsSize > 0) {
//		WebElement selectedOrganization = TestHelper.getItem('manage_user_network_role', 0)
//		selectedOrganization.click()
//	} else {
		TestObject emptyStateObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-network-role-empty-state\']')
		boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)
	
		if (emptyStateObjectPresent == true) {
			println('hasn\'t networks roles')
	
			TestObject emptyStateAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-network-role-empty-state-button\']')
			WebUI.click(emptyStateAction)
			createNewOrganization(organizationName, roleName)
	
		} else {
			println('has network roles')
	
			TestObject addOrganizationRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'add-new-network-role-action\']')
			WebUI.click(addOrganizationRoleAction)
			createNewOrganization(organizationName, roleName)
		}
//	}
}
	
def createNewOrganization(String organizationName, String roleName) {
	UserRoleHelper.createOrganization(organizationName, roleName)
	WebUI.delay(2);
	String organizationTarget = organizationName + '-' + roleName + '-role'
	TestObject selectedOrganization = TestHelper.getObjectById("//div[@id='"+organizationTarget+"']")
	TestHelper.clickButton(selectedOrganization);
}

def addCommittee(String committeeName) {
	TestObject committeeNameInput = TestHelper.getObjectById("//input[@id='committee-name-input']");
	TestHelper.setInputValue(committeeNameInput, committeeName);
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='add-committee-name-action']"));
	WebUI.delay(2);
	
	String committeeId = committeeName + '_org_committee';
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='" + committeeId +"']"));
}

def joinCommittee(String committeeName, String roleName, String profileName) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='join-committee-member-empty-state-button']"));
	UserRoleHelper.joinOrganizationCommitee(roleName);

	WebUI.delay(3);
//	String committeeId = committeeName + '_org_committee';
//	TestHelper.clickButton("//div[@id='" + committeeId + "']");
	
	
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='search-committee-member-input']"), profileName);
	WebUI.delay(2);
	
	// View Profile
		String userAction = profileName + '_committee_member_action';
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='" + userAction + "']"));
		TestHelper.clickButton(TestHelper.getObjectById("//a[@id='View Profile_user_action']"));
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_backIcon_id']"));
			
	
	// Remove member
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='" + userAction + "']"));
		TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Remove Member_user_action']"));
		WebUI.delay(2);
		WebElement deleteMemberConfirmation = TestHelper.getItem('remove-committee-member-confirmation', 0)
		deleteMemberConfirmation.click();
		WebUI.delay(1);
}

def addCommitteeMember(String committeeName, String profileName) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-committee-details-action']"));
	TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Add Members_user_action']"));
	
	
	// Invite member
	TestHelper.clickButton(TestHelper.getObjectById("//ion-segment-button[@id='join_committee_member_tab']"));
	
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='invite_committee_member_first_name']"), 'Ali');
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='invite_committee_member_last_name']"), 'Blaybel');
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='invite_committee_member_email']"), 'blaybel.2010@gmail.com');
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='invite_committee_member_action']"));
	
	// add member to committee
	TestHelper.clickButton(TestHelper.getObjectById("//ion-segment-button[@id='add_committee_member_tab']"));
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='add-committee-member-input-search']"), profileName);
	WebUI.delay(2);
		
	String add_member_id = profileName + '_join_committee';
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='" + add_member_id + "']"));
	WebUI.delay(2);	
	
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='add_committee_member_back_action']"));
	
	String current_member_id = profileName + '_committee_member_action';
	boolean currentMemberExist = TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='" + current_member_id + "']"))	
	
	if(!currentMemberExist) {
		TestHelper.thrownException("User not added as committee member");
	}
}

def renameCommittee(String newCommitteeName) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-committee-details-action']"));
	TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Rename Committee_user_action']"));

	TestObject committeeNameInput = TestHelper.getObjectById("//input[@id='committee-name-input']");
	TestHelper.setInputValue(committeeNameInput, newCommitteeName);
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='add-committee-name-action']"));
	WebUI.delay(2);

	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_committee_members_back_action']"));
	
	String new_committee_id = newCommitteeName + '_org_committee';
	boolean newCommitteeExist = TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='" + new_committee_id + "']"))
	
	if(!newCommitteeExist) {
		TestHelper.thrownException("Committe name not update successfully");
	}	
}

def checkCommitteeInOrganizationPage(String newCommitteeName) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_committee_back_action']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-Organization-back-button']"));
	
	TestObject committeeSection = TestHelper.getObjectById("//div[@id='organization_committee']");
	
	String new_committee_id = newCommitteeName + '_main_org_committee';
	boolean newCommitteeExist = TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='" + new_committee_id + "']"))
	
	if(!newCommitteeExist) {
		TestHelper.thrownException("Committe not exist in the organization page");
	}
	
	TestHelper.goToOrganizationManageEntity();
	TestObject manageOrganizationCommitteesAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Committees_manage_entity_action']")
	TestHelper.clickButton(manageOrganizationCommitteesAction)
	
}

def deleteCommittee(String newCommitteeName) {
	String new_committee_id = newCommitteeName + '_org_committee';
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='" + new_committee_id + "']"))

	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-committee-details-action']"));
	TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Delete Committee_user_action']"));
	WebUI.delay(2);
	
	WebElement removeCommitteeConfirmation = TestHelper.getItem('remove-committee-confirmation', 0)
	removeCommitteeConfirmation.click();
	boolean newCommitteeExist = TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='" + new_committee_id + "']"))
	
	if(newCommitteeExist) {
		TestHelper.thrownException("Committe name not update successfully");
	} else {
		print("Test Complete");
		WebUI.closeBrowser();
	}
}
