import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import static org.junit.Assert.*

import org.junit.After
import org.junit.Test
import org.openqa.selenium.WebElement as WebElement
import org.stringtemplate.v4.compiler.STParser.compoundElement_return

String companyName = 'Netways' + new Date().getTime();
String organizationName = 'Netways_Organization' + new Date().getTime()
String roleName = "Founder"
String companyUrl, organizationUrl;
String selectedCompanyName, selectedOrganizationName;

 
// Login and go to my profile tab
WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Login'), [:], FailureHandling.STOP_ON_FAILURE)
 
TestHelper.goToManageEntity();
TestObject manageBusinessRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Business Roles_manage_entity_action']")
TestHelper.clickButton(manageBusinessRoleAction)

checkCompany(companyName, roleName);
checkOrganization(organizationName, roleName)
addAffiliation();
acceptAffiliation();
checkIfCompanyAffiliationExistInOrganizationAffiliations();
checkIfCompanyExistInOrganizationProfile();
removeAffilition();

WebUI.delay(2);
WebUI.closeBrowser();

// Check Company 
def checkCompany (String companyName, String roleName) { 
//	int companiesSize = TestHelper.countElementByClassName('manage_user_business_role')
//	if (companiesSize > 0) {
//		WebElement selectedCompany = TestHelper.getItem('manage_user_business_role', 0)
//		selectedCompany.click();
//		
//		WebUI.delay(6);
//		companyUrl = WebUI.getUrl();
//		WebElement companyNameObject = TestHelper.getItem('company-name-title', 0)
//		selectedCompanyName = companyNameObject.text;
//		
//		TestObject companyBackButton = TestHelper.getObjectById("//div[@id='company_back_button']");
//		TestHelper.clickButton(companyBackButton);
//		
//	} else {
		TestObject emptyStateObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-business-role-empty-state']")
		boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)
	
		if (emptyStateObjectPresent == true) {
			println("hasn't businesses roles")
	 
			TestObject emptyStateAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-business-role-empty-state-button']")
			TestHelper.clickButton(emptyStateAction)

			// Create company, add get created company url
			UserRoleHelper.createCompany(companyName, roleName);
			 
			TestObject manageBusinessBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_business_role_back_action']")
			WebUI.waitForElementPresent(manageBusinessBackButton, 5)
			
			getCompanyName(companyName, roleName);
		} else {
			println("has businesses roles") 
			TestObject addBusinessRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='add-new-business-role-action']")
			TestHelper.clickButton(addBusinessRoleAction)
			
			// Create company, add get created company url
			UserRoleHelper.createCompany(companyName, roleName);
			TestObject manageBusinessBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_business_role_back_action']")
			WebUI.waitForElementPresent(manageBusinessBackButton, 5)
			getCompanyName(companyName, roleName);
		}
//	} 
}

def getCompanyName(String companyName, String roleName) { 
	String companyRoleId = companyName + '-' + roleName + '-role';
	String companyRoleIdXpath = "//div[@id='"+companyRoleId+"']"
	TestObject companyRoleObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, companyRoleIdXpath)
	TestHelper.clickButton(companyRoleObject);
	 
	WebUI.delay(6);
	TestObject companyBackButton = TestHelper.getObjectById("//div[@id='company_back_button']");
 	companyUrl = WebUI.getUrl();
	 
	WebElement companyNameObject = TestHelper.getItem('company-name-title', 0)
	selectedCompanyName = companyNameObject.text;
	TestHelper.clickButton(companyBackButton);
}

// Check Organization
def checkOrganization(String organizationName, String roleName) {
	TestObject manageBusinessBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_business_role_back_action']")
	TestHelper.clickButton(manageBusinessBackButton);
	 
	TestObject manageNetworksRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Network Roles_manage_entity_action\']')
	TestHelper.clickButton(manageNetworksRoleAction)
//	int organizationsSize = TestHelper.countElementByClassName('manage_user_network_role')
//	if (organizationsSize > 0) {
//		WebElement selectedOrganization = TestHelper.getItem('manage_user_network_role', 0) 
//		selectedOrganization.click()  
//		organizationUrl = WebUI.getUrl(); 
//		
//		WebElement organizationNameObject = TestHelper.getItem('organization-name-title', 0)
//		selectedOrganizationName = organizationNameObject.text;
//		
//	} else {
		TestObject emptyStateObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-network-role-empty-state\']')
	
		boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)
	
		if (emptyStateObjectPresent == true) {
			println('hasn\'t networks roles')
	
			TestObject emptyStateAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-network-role-empty-state-button\']')
			WebUI.click(emptyStateAction)
			createNewOrganization(organizationName, roleName)
	
		} else {
			println('has networks roles')
	
			TestObject addBusinessRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'add-new-network-role-action\']')
			WebUI.click(addBusinessRoleAction)
			createNewOrganization(organizationName, roleName)
		}
//	} 
}
 
def createNewOrganization(String organizationName, String roleName) {
	UserRoleHelper.createOrganization(organizationName, roleName)
	WebUI.delay(2);
	String organizationTarget = organizationName + '-' + roleName + '-role'
	TestObject selectedOrganization = TestHelper.getObjectById("//div[@id='"+organizationTarget+"']")
	TestHelper.clickButton(selectedOrganization);
	organizationUrl = WebUI.getUrl(); 
	WebElement organizationNameObject = TestHelper.getItem('organization-name-title', 0)
	selectedOrganizationName = organizationNameObject.text;
}

// Add Affiliation
def addAffiliation() {
	TestHelper.goToOrganizationManageEntity();
	TestObject manageAffiliationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Member Companies_manage_entity_action']")
	TestHelper.clickButton(manageAffiliationAction)
	
	TestObject requestedAffiliation = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-pending-company-affiliation-request-empty-state']")
	boolean requestedAffiliationPresent = TestHelper.isElementPresent(requestedAffiliation)

	if (requestedAffiliationPresent) {
		TestHelper.clickButton(requestedAffiliation);
				
		TestObject searchForCompany = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='searchForCompanyInput']")
		TestHelper.setInputValue(searchForCompany, selectedCompanyName)
		WebUI.delay(1);

		String companyAction = selectedCompanyName + '_filter_company_action';
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='" + companyAction + "']"));
		
		WebElement linkConfirmation = TestHelper.getItem('link_company_affiliation_confirmation', 0)
		linkConfirmation.click();
 		
		WebUI.delay(4);
		WebUI.navigateToUrl(companyUrl)
	} else {
		TestHelper.thrownException("Company already has affiliation");
	}
}

def acceptAffiliation() { 
	TestHelper.goToCompanyManageEntity();
	TestObject manageOrganizationMember = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Member Of_manage_entity_action']")
	TestHelper.clickButton(manageOrganizationMember)

	WebUI.delay(2);

	String organizationId = selectedOrganizationName + '_pending_org_affiliation';
	TestObject organizationObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + organizationId + "']")
	boolean organizationObjectExist = TestHelper.isElementPresent(organizationObject);
	
	if(organizationObjectExist) {
		String organizationAction = selectedOrganizationName + '_manage_pending_org_affiliation';
		TestObject organizationActionObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + organizationAction + "']")
		TestHelper.clickButton(organizationActionObject);
	
		// Accept Organization Request
		TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Approve Request_user_action']"));
		WebUI.delay(3);
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_organization_affiliation_back_action']"));
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-Company-back-button']"));
	} else {
		TestHelper.thrownException("Organization not exist");
	}

}	

def checkIfCompanyAffiliationExistInOrganizationAffiliations() {
	WebUI.navigateToUrl(organizationUrl);
	TestHelper.goToOrganizationManageEntity();
	TestObject manageAffiliationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Member Companies_manage_entity_action']")	
	TestHelper.clickButton(manageAffiliationAction)

	// Check if company exist in the current tab
	TestHelper.clickButton(TestHelper.getObjectById( "//ion-segment-button[@id='manage_company_current_affiliation']"))
	String affiliationCompany = selectedCompanyName + '_current_company_affiliation';
	TestObject affiliationCompanyObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + affiliationCompany + "']")
	boolean affiliationCompanyObjectExist = TestHelper.isElementPresent(affiliationCompanyObject);
	
	if(!affiliationCompanyObjectExist) {
		TestHelper.thrownException("Affiliation company not present in the organization affiliations");
	}
	
	
	// Feature Company
	String companyAction = selectedCompanyName + '_manage_current_company_affiliation';
	TestHelper.clickButton(TestHelper.getObjectById( "//div[@id='" + companyAction + "']"))
	TestHelper.clickButton(TestHelper.getObjectById( "//a[@id='Set As Primary_user_action']"));
	WebUI.delay(2);
	
	String featuredCompany = selectedCompanyName + '_featured';
	TestObject featuredCompanyObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + featuredCompany + "']")
	boolean featuredCompanyObjectExist = TestHelper.isElementPresent(featuredCompanyObject);
	
	if(featuredCompanyObjectExist) {		
		// Unfeature Company
		TestHelper.clickButton(TestHelper.getObjectById( "//div[@id='" + companyAction + "']"))
		TestHelper.clickButton(TestHelper.getObjectById( "//a[@id='Remove From Primary_user_action']"))
		WebUI.delay(2);
		boolean unfeaturedCompanyObjectExist = TestHelper.isElementPresent(featuredCompanyObject);
		
		if(unfeaturedCompanyObjectExist) {
			TestHelper.thrownException("company still set as featured");
		}
	} else {
		TestHelper.thrownException("Company not set as featured");
	}
}

def checkIfCompanyExistInOrganizationProfile() {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_company_affiliation_back_action']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-Organization-back-button']"))
	
	TestObject affiliationSection = TestHelper.getObjectById("//div[@id='affiliation-section']")
	boolean affiliationSectionExist = TestHelper.isElementPresent(affiliationSection);
	
	if(affiliationSectionExist) {
		String mainCompany = selectedCompanyName + '_main_page_company_affiliation';
		TestObject mainCompanyObject = TestHelper.getObjectById("//div[@id='" + mainCompany + "']")
		boolean mainCompanyObjectExist = TestHelper.isElementPresent(mainCompanyObject);
		
		if(!mainCompanyObjectExist) {
			TestHelper.thrownException("Affiliation not exist in the main page");	
		}	
	} else {
		TestHelper.thrownException("Affiliation company exist in the organization profile");
	}	
}

def removeAffilition() {
	TestHelper.goToOrganizationManageEntity();
	TestObject manageAffiliationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Member Companies_manage_entity_action']")
	TestHelper.clickButton(manageAffiliationAction)

	// Check if company exist in the current tab
	TestHelper.clickButton(TestHelper.getObjectById( "//ion-segment-button[@id='manage_company_pending_affiliation']"))

	// Remove affiliation
	String companyAction = selectedCompanyName + '_manage_current_company_affiliation';
	TestHelper.clickButton(TestHelper.getObjectById( "//div[@id='" + companyAction + "']"))
	TestHelper.clickButton(TestHelper.getObjectById( "//a[@id='Remove Company_user_action']"))
	WebElement removeConfirmation = TestHelper.getItem('removeCurrentCompanyAffiliation', 0)
	removeConfirmation.click();
	
	WebUI.delay(2);

	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_company_affiliation_back_action']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-Organization-back-button']"))
	
	TestObject affiliationSection = TestHelper.getObjectById("//div[@id='affiliation-section']")
	boolean affiliationSectionExist = TestHelper.isElementPresent(affiliationSection);
	
	if(affiliationSectionExist) {
		String mainCompany = selectedCompanyName + '_main_page_company_affiliation';
		TestObject mainCompanyObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + mainCompany + "']")
		boolean mainCompanyObjectExist = TestHelper.isElementPresent(mainCompanyObject);
		
		if(!mainCompanyObjectExist) {
			println("Test has been passed");
			WebUI.closeBrowser();
		} else {
			TestHelper.thrownException("Affiliation not exist in the main page");
		}
		
	} else {
		TestHelper.thrownException("Affiliation company exist in the company profile");
	}
}



