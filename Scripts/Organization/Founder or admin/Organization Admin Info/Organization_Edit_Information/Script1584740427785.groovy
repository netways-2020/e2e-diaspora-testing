import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import java.util.List
import java.util.Map

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable


// Define Varibales
TestObject organizationName_input, description_textArea, organizationTypeButton, organizationCategoryButton;
TestObject headquarterCountry_button, headquarterCity_button, address_input;
TestObject phoneCountryCode_button, phoneNumber_input, contactEmail_input, poBox_input;
TestObject website_input, facebook_input, linkedin_input, instagram_input, twitter_input;

List<String> countries = TestHelper.prepareCountries();
Map<String, List<String>> cities = TestHelper.prepareCities();
 
 
// New Value
long newDate =  new Date().getTime();

// Country and city
String selectedHeadquarterCountry = TestHelper.getRandomValueFromList(countries);
List<String> citiesOfHeadquarterCountry = cities.get(selectedHeadquarterCountry)
		
String selectedHeadquarterCity =  TestHelper.getRandomValueFromList(citiesOfHeadquarterCountry);
 
  
// Login and go to my organization
WebUI.callTestCase(findTestCase('Organization/Founder or admin/Authorization for organization/Login_For_Organization'), [:], FailureHandling.STOP_ON_FAILURE)

TestHelper.goToOrganizationManageEntity();
TestObject manageBasicInformationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Organization Information_manage_entity_action']")
TestHelper.clickButton(manageBasicInformationAction)


// Edit Organization and check values
prepareEditOrganizationFields();
changeOrganizationInfoValue(countries, cities, newDate, selectedHeadquarterCountry, selectedHeadquarterCity);
checkEditedOrganizationValue(newDate, selectedHeadquarterCountry, selectedHeadquarterCity);

//// Clear Edited Fields
TestHelper.goToOrganizationManageEntity();
TestHelper.clickButton(manageBasicInformationAction)
clearOrganizationFields(selectedHeadquarterCountry);
OrganizationHelper.checkOrganizationGeneralInfoFieldsEmptyState();
WebUI.closeBrowser();


def prepareEditOrganizationFields() {
	// About Info
	organizationName_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='organization-name-id']")
	description_textArea = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//textarea[@id='about-section']")
	address_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='organization-address-id']")

	organizationTypeButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='organization-type-id']")
	organizationCategoryButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='organization-category-id']")
	 
	// Place Info
	headquarterCountry_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='organization-headquarter-country']")
	headquarterCity_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='organization-headquarter-city']")
 	 
	// Contact Info
	phoneCountryCode_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='organization-phone-country-code']")
	phoneNumber_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='organization-phone-number']")
	contactEmail_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='organization-contact-email']")
	poBox_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='pobox-input']")
	
	// Social Media
	website_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='organization-website']")
	facebook_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='organization-social-media-facebook']")
	linkedin_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='organization-social-media-linkedin']")
	instagram_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='organization-social-media-instagram']")
	twitter_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='organization-social-media-twitter']")
}

// Edit Fields
def changeOrganizationInfoValue(List<String> countries, Map<String, List<String>> cities, long newDate, String selectedHeadquarterCountry, String selectedHeadquarterCity) {
	
	// User info
		TestHelper.setInputValue(organizationName_input, organizationName + newDate)
		TestHelper.setInputValue(description_textArea, newAbout + newDate)
		
	// Organization type
		TestHelper.clickButton(organizationTypeButton)
		TestObject clubType = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Club_item']")
		WebUI.click(clubType)
		WebUI.delay(1)
		
	// Organization category
		TestHelper.clickButton(organizationCategoryButton)
		TestObject beautyCategory = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Beauty_item']")
		WebUI.click(beautyCategory)
		
	// Headquarter Country/City
		WebUI.delay(2)
		TestHelper.clickButton(headquarterCountry_button)
		TestObject headquarterCountrySearchInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='country-search-input']")
		TestHelper.setInputValue(headquarterCountrySearchInput, selectedHeadquarterCountry)
		TestObject headquarterCountryObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+selectedHeadquarterCountry+"']")
		WebUI.delay(2.5)
		WebUI.click(headquarterCountryObject)
		
		TestHelper.clickButton(headquarterCity_button)
		TestObject headquarterCitySearchInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='city-search-input']")
		TestHelper.setInputValue(headquarterCitySearchInput, selectedHeadquarterCity)
		TestObject headquarterCityObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+selectedHeadquarterCity+"']")
		WebUI.delay(1)
		WebUI.click(headquarterCityObject)
		
	// Address
		TestHelper.setInputValue(address_input, newAddress + newDate)
		
		  
	// Contact Info
		WebUI.delay(2)
		
		TestHelper.clickButton(phoneCountryCode_button)
		TestObject phoneCountrySearchInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='country-search-input']")
		TestHelper.setInputValue(phoneCountrySearchInput, selectedHeadquarterCountry)
		TestObject phoneCountryObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+selectedHeadquarterCountry+"']")
		WebUI.delay(1)
		WebUI.click(phoneCountryObject)
		
		TestHelper.setInputValue(phoneNumber_input, '123456')
 
	// Email
		TestHelper.setInputValue(contactEmail_input, newEmail + newDate + '@gmail.com')
		TestHelper.setInputValue(website_input, newWebsite + newDate)
		TestHelper.setInputValue(poBox_input, '123456')
		
	// Social Media
		WebUI.delay(1);
		TestHelper.setInputValue(facebook_input, newFacebook + newDate)
		TestHelper.setInputValue(linkedin_input, newLinkedIn + newDate)
		TestHelper.setInputValue(instagram_input, newInstagram + newDate)
		TestHelper.setInputValue(twitter_input, newTwitter + newDate)
		
		TestObject saveOrganizationInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='save_organization_info']")
		TestHelper.clickButton(saveOrganizationInfo);
		WebUI.delay(3)
		
		TestObject manageEntity = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-Organization-back-button']")
		TestHelper.clickButton(manageEntity);
}

def checkEditedOrganizationValue(long newDate, String selectedHeadquarterCountry, String selectedHeadquarterCity) {
	WebUI.delay(1)
	boolean successfullyEdited = true;
	
	String headquarterCountry = TestHelper.getCountryIso(selectedHeadquarterCountry);
	String countryIsoCode = TestHelper.getCountryIso(selectedHeadquarterCountry);
	String phoneCountryCode = TestHelper.getCountryCode(selectedHeadquarterCountry);
	 
		
	 // Prepare IDs
		// Basic Info
			String edited_organization_name_id = 'organization_title_' + (organizationName + newDate);
			String edited_organization_description_id = 'organization_introduction_id_' + (newAbout + newDate);
			String edited_organization_address_id = 'organization_address_id_' + (newAddress + newDate);
  
		// Countries
			String edited_country_headquarter  = 'country_' + countryIsoCode;
			String edited_city_headquarter  = 'country_' + countryIsoCode + '_tag_label' ;
			
		 // Contact Info
			String edited_phone_id = 'organization_contact_phone_id_'+ phoneCountryCode + '123456';
			String edited_email_id = 'organization_contact_email_id_'+ (newEmail + newDate) + '@gmail.com';
			String edited_pobox_id = 'organization_pobox_id_'+  '123456';
			
		// Social Media
			String edited_website_id  = 'organization_contact_website_id_' + newWebsite  + newDate;
			String edited_facebook_id  = "@id='organization_facebook_id'  and  @alt= '" + newFacebook  + newDate +"'";
			String edited_linkedin_id  = "@id='organization_linkedin_id'  and  @alt= '" + newLinkedIn  + newDate +"'";
			String edited_twitter_id   = "@id='organization_twitter_id'   and  @alt= '" + newTwitter   + newDate +"'";
			String edited_instagram_id = "@id='organization_instagram_id' and  @alt= '" + newInstagram + newDate +"'";
			
	// Prepare Objects
		// Basic Info
			TestObject edited_organization_name_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + edited_organization_name_id +"']");
			TestObject edited_about_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + edited_organization_description_id +"']");
			TestObject edited_address_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + edited_organization_address_id +"']")
			
		
		// Countries Info
			TestObject edited_organization_country_headquarter = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//img[@id='" + edited_country_headquarter +"']");
			TestObject edited_organization_city_headquarter = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='" + edited_city_headquarter +"']");
			 
			 
		// Contact Info
			TestObject edited_email_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + edited_email_id +"']")
			TestObject edited_phone_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + edited_phone_id +"']")
			TestObject edited_pobox_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + edited_pobox_id +"']")
			TestObject edited_website_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + edited_website_id +"']")
			
		// Social Media
			TestObject edited_facebook_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//img["+ edited_facebook_id +"]")
			TestObject edited_linkedin_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//img["+ edited_linkedin_id +"]")
			TestObject edited_twitter_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,  "//img["+ edited_twitter_id  +"]")
			TestObject edited_instagram_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//img["+ edited_instagram_id +"]")
			
			
	// Check Object Exist
		if(!TestHelper.isElementPresent(edited_organization_name_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_about_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_address_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_organization_country_headquarter)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_organization_city_headquarter)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_email_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_phone_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_pobox_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_website_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_facebook_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_linkedin_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_twitter_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_instagram_object)) {
			successfullyEdited = false;
		}
 
 
	if(successfullyEdited) {
	  println("Organization Updated Successfully");
	} else {
	  TestHelper.thrownException("Organization Not Updated")
	}
	
}
	
// Reset Fields
def clearOrganizationFields(String selectedCountryOfResidence) {
	
	TestHelper.setInputValue(description_textArea, " ");
	WebUI.delay(1);
	TestHelper.setInputValue(address_input, " ");
	
	
	// Contact Info
	WebUI.delay(1);
	TestHelper.clickButton(phoneCountryCode_button)
	TestObject phoneCountrySearchInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='country-search-input']")
	TestHelper.setInputValue(phoneCountrySearchInput, selectedCountryOfResidence)
	TestObject phoneCountryObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+selectedCountryOfResidence+"']")
	WebUI.delay(1)
	WebUI.click(phoneCountryObject)

	
	TestHelper.setInputValue(contactEmail_input, " ");
//	TestHelper.setInputValue(poBox_input, null);
	
	// Social Media
	WebUI.delay(1);
	TestHelper.setInputValue(website_input, " ");
	WebUI.delay(1);
	TestHelper.setInputValue(facebook_input, " ");
	WebUI.delay(1);
	TestHelper.setInputValue(linkedin_input, " ");
	WebUI.delay(1);
	TestHelper.setInputValue(instagram_input, " ");
	WebUI.delay(1);
	TestHelper.setInputValue(twitter_input, " ");
		
	TestObject saveCompanyInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='save_organization_info']")
	TestHelper.clickButton(saveCompanyInfo);
	WebUI.delay(3)
	
	TestObject manageEntity = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-Organization-back-button']")
	TestHelper.clickButton(manageEntity);
}

