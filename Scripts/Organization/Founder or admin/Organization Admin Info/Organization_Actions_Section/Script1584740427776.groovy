import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Login_With_User_Has_Full_Information'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.navigateToUrl(TestHelper.getBlaybel_organizationUrl());

checkOrganizationActions();
WebUI.closeBrowser();

def checkOrganizationActions() {
	TestObject about_title_object = TestHelper.getObjectById("//div[@id='organization_about_title_id']")
	TestObject about_subTitle_object = TestHelper.getObjectById("//div[@id='organization_about_subTitle_id']")
	TestObject about_more_object = TestHelper.getObjectById("//div[@id='organization_about_more_id']")
	
	if(TestHelper.isElementPresent(about_title_object) && TestHelper.isElementPresent(about_subTitle_object) && TestHelper.isElementPresent(about_more_object)) {
		String aboutTitle = WebUI.getText(about_title_object);
		String aboutSubTitle = WebUI.getText(about_subTitle_object);
		String aboutMore = WebUI.getText(about_more_object);
		 
		if(!aboutTitle.equals("About") || !aboutSubTitle.equals("Info, sector and contact details") || !aboutMore.equals("Edit")) {
			TestHelper.thrownException("Invalid about title and subtitle")
		}
		
		TestHelper.clickButton(about_more_object);
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='edit-organization-back-button']"));
	} else {
		TestHelper.thrownException("Invalid sections data found");
	}

	TestObject affiliation_title_object = TestHelper.getObjectById("//div[@id='organization_affiliation_title_id']")
	TestObject affiliation_subTitle_object = TestHelper.getObjectById("//div[@id='organization_affiliation_subTitle_id']")
	TestObject affiliation_more_object = TestHelper.getObjectById("//div[@id='organization_affiliation_more_id']")
	
	if(TestHelper.isElementPresent(affiliation_title_object) && TestHelper.isElementPresent(affiliation_subTitle_object) && TestHelper.isElementPresent(affiliation_more_object)) {
		String affiliationTitle = WebUI.getText(affiliation_title_object);
		String affiliationSubTitle = WebUI.getText(affiliation_subTitle_object);
		String affiliationMore = WebUI.getText(affiliation_more_object);
		
		 if(!affiliationTitle.equals("Member Companies") || !affiliationSubTitle.equals("Affiliated to this org") || !affiliationMore.equals("Edit")) {
			TestHelper.thrownException("Invalid affiliation title and subtitle")
		}
		 
		 TestHelper.clickButton(affiliation_more_object);
		 TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_company_affiliation_back_action']"));
	} else {
		TestHelper.thrownException("Invalid sections data found");
	}

	
	TestObject members_title_object = TestHelper.getObjectById("//div[@id='organization_members_title_id']")
	TestObject members_subTitle_object = TestHelper.getObjectById("//div[@id='organization_members_subTitle_id']")
	TestObject members_more_object = TestHelper.getObjectById("//div[@id='organization_members_more_id']")
	
	if(TestHelper.isElementPresent(members_title_object) && TestHelper.isElementPresent(members_subTitle_object) && TestHelper.isElementPresent(members_more_object)) {
		String membersTitle = WebUI.getText(members_title_object);
		String membersSubTitle = WebUI.getText(members_subTitle_object);
		String membersMore = WebUI.getText(members_more_object);
		
		if(!membersTitle.equals("Members") || !membersSubTitle.equals("Members of this organization") || !membersMore.equals("Edit")) {
			TestHelper.thrownException("Invalid members title and subtitle")
		}
		
		TestHelper.clickButton(members_more_object);
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-organization-member-back-button']"));
	} else {
		TestHelper.thrownException("Invalid sections data found");
	}

	
	TestObject leaderships_title_object = TestHelper.getObjectById("//div[@id='organization_leadership_title_id']")
	TestObject leaderships_subTitle_object = TestHelper.getObjectById("//div[@id='organization_leadership_subTitle_id']")
	
	if(TestHelper.isElementPresent(leaderships_title_object) && TestHelper.isElementPresent(leaderships_subTitle_object)) {
		String leadershipsTitle = WebUI.getText(leaderships_title_object);
		String leadershipsSubTitle = WebUI.getText(leaderships_subTitle_object);

		if(!leadershipsTitle.equals("Leadership") && !leadershipsSubTitle.equals("Leaders & management")) {
			TestHelper.thrownException("Invalid leaderships title and subtitle")
		}
		
		TestObject leaderships_more_object = TestHelper.getObjectById("//div[@id='organization_leadership_more_id']")
		if(TestHelper.isElementPresent(leaderships_more_object)) {
			TestHelper.clickButton(leaderships_more_object);
			TestHelper.clickButton(TestHelper.getObjectById("//div[@id='view-organization-board-members-back-button']"));
		}
	} else {
		TestHelper.thrownException("Invalid sections data found");
	}

	
	TestObject chapters_title_object = TestHelper.getObjectById("//div[@id='organization_chapters_title_id']")
	TestObject chapters_subTitle_object = TestHelper.getObjectById("//div[@id='organization_chapters_subTitle_id']")
	TestObject chapters_more_object = TestHelper.getObjectById("//div[@id='organization_chapters_more_id']")
	
	if(TestHelper.isElementPresent(chapters_title_object) && TestHelper.isElementPresent(chapters_subTitle_object) && TestHelper.isElementPresent(chapters_more_object)) {
		String chaptersTitle = WebUI.getText(chapters_title_object);
		String chaptersSubTitle = WebUI.getText(chapters_subTitle_object);
		String chaptersMore = WebUI.getText(chapters_more_object);
		
		if(!chaptersTitle.equals("Chapters") || !chaptersSubTitle.equals("Chapters subtitle") || !chaptersMore.equals("Edit")) {
			TestHelper.thrownException("Invalid chapters title and subtitle")
		}

		TestHelper.clickButton(chapters_more_object);
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-organization-chapters-back-button']"));
	} else {
		TestHelper.thrownException("Invalid sections data found");
	}

	TestObject committees_title_object = TestHelper.getObjectById("//div[@id='organization_committees_title_id']")
	TestObject committess_subTitle_object = TestHelper.getObjectById("//div[@id='organization_committees_subTitle_id']")
	TestObject committess_more_object = TestHelper.getObjectById("//div[@id='organization_committees_more_id']")
	
	if(TestHelper.isElementPresent(committees_title_object) && TestHelper.isElementPresent(committess_subTitle_object) && TestHelper.isElementPresent(committess_more_object)) {
		String committeesTitle = WebUI.getText(committees_title_object);
		String committeesSubTitle = WebUI.getText(committess_subTitle_object);
		String committeesMore = WebUI.getText(committess_more_object);
		
		if(!committeesTitle.equals("Committees") || !committeesSubTitle.equals("Committees & members") || !committeesMore.equals("Edit")) {
			TestHelper.thrownException("Invalid committees title and subtitle")
		}
		
		TestHelper.clickButton(committess_more_object);
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_committee_back_action']"));
	} else {
		TestHelper.thrownException("Invalid sections data found");
	}

	TestObject updates_title_object = TestHelper.getObjectById("//div[@id='organization_updates_title_id']")
	TestObject updates_subTitle_object = TestHelper.getObjectById("//div[@id='organization_updates_subTitle_id']")
	TestObject updates_more_object = TestHelper.getObjectById("//div[@id='organization_manage_updates']")
	
	if(TestHelper.isElementPresent(updates_title_object) && TestHelper.isElementPresent(updates_subTitle_object) && TestHelper.isElementPresent(updates_more_object)) {
		String updatesTitle = WebUI.getText(updates_title_object);
		String updatesSubTitle = WebUI.getText(updates_subTitle_object);
		String updateMore = WebUI.getText(updates_more_object);
		
		if(!updatesTitle.equals("Updates") || !updatesSubTitle.equals("Posts & discussions") || !updateMore.equals("Edit")) {
			TestHelper.thrownException("Invalid updates title and subtitle")
		}

		TestHelper.clickButton(updates_more_object);
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back_button_cardListing']"));
	} else {
		TestHelper.thrownException("Invalid sections data found");
	}

	TestObject media_title_object = TestHelper.getObjectById("//div[@id='organization_media_title_id']")
	TestObject media_subTitle_object = TestHelper.getObjectById("//div[@id='organization_media_subTitle_id']")
	TestObject media_more_object = TestHelper.getObjectById("//div[@id='organization_manage_media']")
		
	if(TestHelper.isElementPresent(media_title_object) && TestHelper.isElementPresent(media_subTitle_object) && TestHelper.isElementPresent(media_more_object)) {
		String mediaTitle = WebUI.getText(media_title_object);
		String mediaSubTitle = WebUI.getText(media_subTitle_object);
		String mediaMore = WebUI.getText(media_more_object);
		
		if(!mediaTitle.equals("Media Gallery") || !mediaSubTitle.equals("Photos & videos") || !mediaMore.equals("Edit")) {
			TestHelper.thrownException("Invalid media title and subtitle")
		}
		
		TestHelper.clickButton(media_more_object);
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back_button_gallery']"));
	} else {
		TestHelper.thrownException("Invalid sections data found");
	}
}



