import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import java.util.List as List
import java.util.Map as Map
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Organization/Founder or admin/Authorization for organization/Login_For_Organization'), [:], FailureHandling.STOP_ON_FAILURE)

checkThatTheOrganizationHasntImage()

TestHelper.goToOrganizationManageEntity()
TestObject manageBasicInformationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Organization Information_manage_entity_action']")
TestHelper.clickButton(manageBasicInformationAction)

changeOrganizationImage()
checkThatTheOrganizationHasImage()  

def checkThatTheOrganizationHasntImage() {
    TestObject organizationAvatar = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//ngx-avatar[@id=\'Organization_hasnt_image\']')

    if (!(TestHelper.isElementPresent(organizationAvatar))) {
        TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'organization_back_button\']'))
        UserRoleHelper.createOrganizationAndGoToTarget();
        checkThatTheOrganizationHasntImage()
    }
}

def changeOrganizationImage() {
	String userPath = System.getProperty("user.dir")
	String imagePath = userPath + "//Image//test_image.png"

    TestObject changeImageAction = TestHelper.getObjectById('//div[@id=\'change_image_action\']')
    TestHelper.clickButton(changeImageAction)
	
    TestObject changeProfileImageAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//input[@id=\'image_gallery_action\']')
    WebUI.uploadFile(changeProfileImageAction, imagePath)
 
   TestObject selectImageAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'cropper_select_image\']')
    TestHelper.clickButton(selectImageAction)

	WebUI.delay(2);
    TestObject saveOrganizationInfo = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//button[@id=\'save_organization_info\']')
    TestHelper.clickButton(saveOrganizationInfo)
    WebUI.delay(3)

    TestObject manageEntity = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'manage-Organization-back-button\']')
    TestHelper.clickButton(manageEntity)
}

def checkThatTheOrganizationHasImage() {
    TestObject organizationAvatar = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//img[@id=\'Organization_has_image\']')

    if (!(TestHelper.isElementPresent(organizationAvatar))) {
        TestHelper.thrownException('Organization image not updated')
    } else {
        println('Image has been updated successfully')
		WebUI.closeBrowser();
    }
}

