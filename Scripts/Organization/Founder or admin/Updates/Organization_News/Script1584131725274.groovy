import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.testobject.ConditionType


WebUI.callTestCase(findTestCase('Organization/Founder or admin/Authorization for organization/Login_For_Organization'), [:], FailureHandling.STOP_ON_FAILURE)

String organizationUrl = WebUI.getUrl();
WebElement organizationNameObject = TestHelper.getItem('organization-name-title', 0)
String organizationName = organizationNameObject.text;

TestHelper.getObjectById("//div[@id='organization_updates']")
  
TestObject emptyStateOrganizationUpdates = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-organization-updates-empty-state']")
boolean emptyStateOrganizationUpdatesExist = TestHelper.isElementPresent(emptyStateOrganizationUpdates)

long now = new Date().getTime();
String postTitle = 'New post title add now_' + now;
String postDescription = 'New post description add now_' + now;
  
if (emptyStateOrganizationUpdatesExist) {
	println("Hasn't udpates");
	 WebUI.click(emptyStateOrganizationUpdates)
	
	TestObject create_news_action_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Create Post_user_action']")
	WebUI.click(create_news_action_button)
	 
	UserActionsHelper.addPost(postTitle, postDescription);
	TestHelper.verifyCardExist(organizationName, postTitle, 'organization', true);
	
	// Verify Card Features
	verifyCardFeature(postTitle, postDescription, organizationName);

} else {
	println("Has udpates");

	WebUI.navigateToUrl(TestHelper.getHomeUrl());
	
	WebUI.delay(1);
	
	TestObject home_tab = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='home_tab_id']")
	WebUI.click(home_tab)
	
	TestObject user_action_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//img[@id='user_action_button']")
	WebUI.click(user_action_button)
	
	TestObject create_news_action_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Create Post_user_action']")
	WebUI.click(create_news_action_button)

	TestObject changeTargetAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='change-target-id']")
	WebUI.click(changeTargetAction)
 
	TestObject selectedTarget = TestHelper.getObjectById("//div[@id='" +organizationName+ "_target']")
	WebUI.click(selectedTarget)
 
	UserActionsHelper.addPost(postTitle, postDescription)
	WebUI.delay(3);
	TestObject profile_tab = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='profile_tab_id']")
	WebUI.waitForElementPresent(profile_tab, 5)
	
	WebUI.navigateToUrl(organizationUrl);
	TestHelper.verifyCardExist(organizationName, postTitle, 'organization', true);
	
	// Verify Card Features
	verifyCardFeature(postTitle, postDescription, organizationName);

}

 
def verifyCardFeature(String postTitle, String postDescription, String organizationName) {
	TestObject manageCardObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='organization_manage_updates']")
	boolean manageCardObjectExist = TestHelper.isElementPresent(manageCardObject);
	if(!manageCardObjectExist) {
		TestHelper.thrownException("Manage card button not exist");
	}
	
	TestHelper.clickButton(manageCardObject);
	String cartId = postTitle + '_descriptionId';
	TestHelper.clickButton(TestHelper.getObjectById("//span[@id='" + cartId + "']"));
	
	String newPostTitle =  "new " + postTitle;
	String newPostDescription =  "new " + postDescription;
	
	TestHelper.checkPostCardFeatures(postTitle, postDescription, newPostTitle, newPostDescription, organizationName);
	

	WebUI.delay(3);
	String newCartId = newPostTitle + '_descriptionId';
	TestObject deleteCardObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='" + newCartId + "']")
	boolean deleteCardObjectExist = TestHelper.isElementPresent(deleteCardObject);
	
	if(deleteCardObjectExist) {
		TestHelper.thrownException("Card not deleted still exist in the card list");
	} else {
		checkCardRemovedFromMainPage(postDescription);
	}
}

def checkCardRemovedFromMainPage(String cardDescriptionId) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back_button_cardListing']"))
	TestObject cardDescriptionObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+ cardDescriptionId  + "_id']")
	
	boolean cardDescriptionObjectExist = TestHelper.isElementPresent(cardDescriptionObject)

	if(cardDescriptionObjectExist) {
		TestHelper.thrownException("Card not deleted from main page")
	} else {
		println("Test Complete");
		WebUI.closeBrowser();
	}

}




