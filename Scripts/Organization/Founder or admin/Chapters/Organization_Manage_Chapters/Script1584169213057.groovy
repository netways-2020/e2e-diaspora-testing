import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement

String parentOrganizationName = 'Netways_Organization' + new Date().getTime()
String roleName = 'Founder'

WebUI.callTestCase(findTestCase('Organization/Founder or admin/Authorization for organization/Login_For_Organization'), [:], FailureHandling.STOP_ON_FAILURE)

WebElement organizationNameObject = TestHelper.getItem('organization-name-title', 0)
String currentOrganizationName = organizationNameObject.text
String currentOrganizationUrl = WebUI.getUrl()
TestHelper.goToOrganizationManageEntity();

TestObject manageOrganizationChapters = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Chapters_manage_entity_action\']')
TestHelper.clickButton(manageOrganizationChapters)

// Outgoing request
addOrganizationParentAndCheckOutgoingRequest(parentOrganizationName, roleName)

// Current request
addOrganizationParentAndCheckCurrentRequest(parentOrganizationName, roleName)

// Incomming request
acceptIncommingRequest(TestHelper.getLebanedRedCross_OrganizationName(), currentOrganizationName)

rejectIncommingRequest(TestHelper.getLebanedRedCross_OrganizationName(), currentOrganizationName, currentOrganizationUrl) 

WebUI.delay(2);
WebUI.closeBrowser();

// Create Organization
// Check outgoing request
// Check if created Organization exist in the outgoing request
// Check Outgoing Request	
// Withdraw Request
// Create Organization
// Check current request
// Check Parent Organization Exist
// Unlink From Parent Organization
// Logout
// Remove parent branch
// Check Parent Organization Exist
// Unlink From Parent Organization
// Check if current Organization exist in the incomming request
// Accept Incomming Request
// Logout
// Check if current Organization exist in the incomming request
// Accept Incomming Request
// Create Organization
// Check current request
// Check Parent Organization Exist

def addOrganizationParentAndCheckOutgoingRequest(String parentOrganizationName, String roleName) {
    TestObject outgoingRequestTab = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//ion-segment-button[@id=\'manage_organization_chapters_outgoing_request\']')

    TestHelper.clickButton(outgoingRequestTab)
    WebUI.delay(1)
	
    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage_organization_chapters_add_parent_organization_outgoing_empty_state\']'))
    UserRoleHelper.createOrganization(parentOrganizationName, roleName)

    WebUI.delay(2)

    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage-organization-chapters-back-button\']'))
    TestObject manageOrganizationChapters = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Chapters_manage_entity_action\']')
    TestHelper.clickButton(manageOrganizationChapters)
    TestHelper.clickButton(outgoingRequestTab)

    WebUI.delay(1)
	
    String organizationId = parentOrganizationName + '_outgoing_request'
    TestObject organizationIdObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + organizationId) +  '\']')

    boolean organizationIdObjectExist = TestHelper.isElementPresent(organizationIdObject)

    if (!(organizationIdObjectExist)) {
        TestHelper.thrownException('Parent organization not exist in the outgoing request')
    }
    
    String organizationId_outgoingRequest = parentOrganizationName + '_outgoing_request_action'

    TestObject outgoingRequestAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + organizationId_outgoingRequest) + '\']')

    TestHelper.clickButton(outgoingRequestAction)

    TestHelper.clickButton(TestHelper.getObjectById('//a[@id=\'Withdraw Request_user_action\']'))

    WebUI.delay(2)

    organizationIdObjectExist = TestHelper.isElementPresent(organizationIdObject)

    if (organizationIdObjectExist) {
        TestHelper.thrownException('Erro withdraw request: Parent organization still exist in the outgoing request')
    }
}

def addOrganizationParentAndCheckCurrentRequest(String parentOrganizationName, String roleName) {
    TestObject currentRequestTab = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//ion-segment-button[@id=\'manage_organization_chapters_current_request\']')
    TestHelper.clickButton(currentRequestTab)

    WebUI.delay(1)

    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage_organization_chapters_add_parent_organization_current_empty_state\']'))
    UserRoleHelper.searchForOrganizationAndClickAction(parentOrganizationName)

    WebUI.delay(2)

    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage-organization-chapters-back-button\']'))
    TestObject manageOrganizationChapters = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Chapters_manage_entity_action\']')
    TestHelper.clickButton(manageOrganizationChapters)
    TestHelper.clickButton(currentRequestTab)

    WebUI.delay(1)

    TestObject parentObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + parentOrganizationName) + '_organization_tree\']')
    boolean parentObjectExist = TestHelper.isElementPresent(parentObject)

    if (parentObjectExist) {
        println('Parent branch exit')
        TestObject parentUnlinkObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + parentOrganizationName) + '_organization_tree_action\']')

        boolean parentUnlinkObjectExist = TestHelper.isElementPresent(parentUnlinkObject)

        if (parentUnlinkObjectExist) {
            TestHelper.clickButton(parentUnlinkObject)

            WebElement unlinkFromParentConfirmation = TestHelper.getItem('unlink_parent_organization', 0)
            unlinkFromParentConfirmation.click()
            WebUI.delay(2)

            parentObjectExist = TestHelper.isElementPresent(parentObject)
            if (parentObjectExist) {
                TestHelper.thrownException('Error during unlink parent organization')
            }
        } else {
            TestHelper.thrownException('Unlink parent action not exist')
        }
    } else {
        TestHelper.thrownException('Parent organizaion not exist in the outgoing request')
    }
}

def acceptIncommingRequest(String parentOrganizationName, String currentOrganization) {
    WebUI.delay(1)

    linkToRedCrossOrganization(parentOrganizationName, currentOrganization)

    checkAcceptIncommingRequest(currentOrganization)
}

def rejectIncommingRequest(String parentOrganizationName, String currentOrganization, String currentOrganizationUrl) {
    WebUI.navigateToUrl(TestHelper.getHomeUrl())

    TestObject manageProfilSection = TestHelper.getObjectById('//div[@id=\'manageProfileId\']')
    TestHelper.clickButton(manageProfilSection)
    TestObject logoutAction = TestHelper.getObjectById('//div[@id=\'logout-id\']')
    TestHelper.clickButton(logoutAction)
    TestHelper.loginWithAutomationTestingUser()

    WebUI.delay(2)

    WebUI.navigateToUrl(currentOrganizationUrl)

    WebUI.delay(1)

    TestHelper.goToOrganizationManageEntity()

    TestObject manageOrganizationChapters = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Chapters_manage_entity_action\']')
    TestHelper.clickButton(manageOrganizationChapters)
    TestObject currentRequestTab = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//ion-segment-button[@id=\'manage_organization_chapters_current_request\']')
    TestHelper.clickButton(currentRequestTab)

    WebUI.delay(1)

    TestObject parentObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + parentOrganizationName) + '_organization_tree\']')
    boolean parentObjectExist = TestHelper.isElementPresent(parentObject)

    if (parentObjectExist) {
        println('Parent branch exit')

        TestObject parentUnlinkObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + parentOrganizationName) + '_organization_tree_action\']')
        boolean parentUnlinkObjectExist = TestHelper.isElementPresent(parentUnlinkObject)

        if (parentUnlinkObjectExist) {
			
            TestHelper.clickButton(parentUnlinkObject)
            WebElement unlinkFromParentConfirmation = TestHelper.getItem('unlink_parent_organization', 0)
            unlinkFromParentConfirmation.click()

            WebUI.delay(2)

            parentObjectExist = TestHelper.isElementPresent(parentObject)

            if (parentObjectExist) {
                TestHelper.thrownException('Error during unlink parent organization')
            } else {
                linkToRedCrossOrganization(parentOrganizationName, currentOrganization);
                checkRejectedIncommingRequest(currentOrganization)
            }
        } else {
            TestHelper.thrownException('Unlink parent action not exist')
        }
    } else {
        TestHelper.thrownException('Parent organization not exist in the outgoing request')
    }
}

def checkAcceptIncommingRequest(String currentOrganization) {

    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage-organization-chapters-back-button\']'))
    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage-Organization-back-button\']'))
    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'organization_back_button\']'))
    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage_network_role_back_action\']'))
    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage-User-back-button\']'))
    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'profile_manage_accounts\']'))
    TestObject logoutAction = TestHelper.getObjectById('//div[@id=\'logout-id\']')

    TestHelper.clickButton(logoutAction)
    TestHelper.loginWithDiasporaUser()

    WebUI.delay(2)

    WebUI.navigateToUrl(TestHelper.getLebanesRedCross_organizationUrl())

    WebUI.delay(1)

    TestHelper.goToOrganizationManageEntity();
    TestObject manageOrganizationChapters = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Chapters_manage_entity_action\']')
    TestHelper.clickButton(manageOrganizationChapters)
    String organizationId = currentOrganization + '_incomming_request'
    TestObject organizationIdObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + organizationId) + '\']')
    boolean organizationIdObjectExist = TestHelper.isElementPresent(organizationIdObject)

    if (!(organizationIdObjectExist)) {
        TestHelper.thrownException('Current organization not exist in the incomming request')
    }
    
    String organizationId_incommingRequest = currentOrganization + '_incomming_request_action'
    TestObject outgoingRequestAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + organizationId_incommingRequest) + '\']')
    TestHelper.clickButton(outgoingRequestAction)
    TestHelper.clickButton(TestHelper.getObjectById('//a[@id=\'Approve Request_user_action\']'))

    WebUI.delay(2)

    organizationIdObjectExist = TestHelper.isElementPresent(organizationIdObject)

    if (organizationIdObjectExist) {
        TestHelper.thrownException('Error accept incomming request: current organization still exist in the incomming request')
    }
    
    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage-organization-chapters-back-button\']'))
    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage-Organization-back-button\']'))
    TestHelper.getObjectById('//div[@id=\'organization_chapters\']')

    String organizationId_tree_list = currentOrganization + '_chapter_item_list'
    TestObject organizationInTreeObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + organizationId_tree_list) + '\']')

    boolean organizationInTreeObjectExist = TestHelper.isElementPresent(organizationInTreeObject)
	
    if (organizationInTreeObjectExist) {
        println('organization exist in the tree')
    } else {
        TestHelper.thrownException('Error incomming request: current organization not exist in the organization tree')
    }
}

def checkRejectedIncommingRequest(String currentOrganization) {

    WebUI.navigateToUrl(TestHelper.getHomeUrl())

    TestObject manageProfilSection = TestHelper.getObjectById('//div[@id=\'manageProfileId\']')
    TestHelper.clickButton(manageProfilSection)
    TestObject logoutAction = TestHelper.getObjectById('//div[@id=\'logout-id\']')

    TestHelper.clickButton(logoutAction)

    TestHelper.loginWithDiasporaUser()

    WebUI.delay(2)

    WebUI.navigateToUrl(TestHelper.getLebanesRedCross_organizationUrl())

    WebUI.delay(1)

    TestHelper.goToOrganizationManageEntity()
    TestObject manageOrganizationChapters = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Chapters_manage_entity_action\']')
    TestHelper.clickButton(manageOrganizationChapters)

    String organizationId = currentOrganization + '_incomming_request'
    TestObject organizationIdObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + organizationId) + '\']')
    boolean organizationIdObjectExist = TestHelper.isElementPresent(organizationIdObject)

    if (!(organizationIdObjectExist)) {
        TestHelper.thrownException('Current organization not exist in the incomming request')
    }
    
    String organizationId_incommingRequest = currentOrganization + '_incomming_request_action'
    TestObject outgoingRequestAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + organizationId_incommingRequest) +  '\']')
    TestHelper.clickButton(outgoingRequestAction)
    TestHelper.clickButton(TestHelper.getObjectById('//a[@id=\'Reject Request_user_action\']'))

    WebUI.delay(2)

    organizationIdObjectExist = TestHelper.isElementPresent(organizationIdObject)

    if (organizationIdObjectExist) {
        TestHelper.thrownException('Error accept incomming request: current organization still exist in the incomming request')
    }
    
    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage-organization-chapters-back-button\']'))
    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage-Organization-back-button\']'))

    TestHelper.getObjectById('//div[@id=\'organization_chapters\']')

    String organizationId_tree_list = currentOrganization + '_chapter_item_list'
    TestObject organizationInTreeObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + organizationId_tree_list) +  '\']')
    boolean organizationInTreeObjectExist = TestHelper.isElementPresent(organizationInTreeObject)

    println('OrganizationId_tree_list: ' + organizationId_tree_list)

    if (!(organizationInTreeObjectExist)) {
        println('organization exist in the tree')
    } else {
        TestHelper.thrownException('Error incomming request: current organization not exist in the organization tree')
    }
}

def linkToRedCrossOrganization(String parentOrganizationName, String currentOrganization) {
	
    TestObject currentRequestTab = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//ion-segment-button[@id=\'manage_organization_chapters_current_request\']')
    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage_organization_chapters_add_parent_organization_current_empty_state\']'))
    UserRoleHelper.searchForOrganizationAndClickAction(parentOrganizationName)

    WebUI.delay(2)

    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage-organization-chapters-back-button\']'))
    TestObject manageOrganizationChapters = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Chapters_manage_entity_action\']')

    TestHelper.clickButton(manageOrganizationChapters)
    TestHelper.clickButton(currentRequestTab)

    WebUI.delay(1)

    TestObject parentObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + parentOrganizationName) +  '_organization_tree\']')

    boolean parentObjectExist = TestHelper.isElementPresent(parentObject)

    if (parentObjectExist) {
        println('Parent branch exit')
    } else {
        TestHelper.thrownException('Parent organization not exist in the outgoing request')
    }
}

