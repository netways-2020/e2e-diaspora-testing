import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement

String organizationName = 'Netways_Organization' + new Date().getTime();
String roleName = 'Founder'

WebUI.openBrowser('')

WebUI.navigateToUrl(TestHelper.getHomeUrl())

TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'loginButton\']'))

WebUI.delay(1)

TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'login-with-email-button\']'))

TestObject emailInput = TestHelper.getObjectById('//input[@id=\'email-input\']')

TestHelper.setInputValue(emailInput, 'automationTesting@gmail.com')

//WebUI.setText(findTestObject('Object Repository/Page_Ionic App/input_Enter Your Email Address_email-input'), 'automationTesting@gmail.com')
TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'login-email-submit-button\']'))

TestObject passwordInput = TestHelper.getObjectById('//input[@id=\'passwordInputId\']')

TestHelper.setPasswordValue(passwordInput, 'cvW8qx4B2o3F4VwP/kNsqA==')

//WebUI.setEncryptedText(findTestObject('Object Repository/Page_Ionic App/input_Your password must be at least 6 char_625064'), 'cvW8qx4B2o3F4VwP/kNsqA==')
TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'submit-button\']'))

TestObject manageProfilSection = TestHelper.getObjectById('//div[@id=\'manageProfileId\']')

WebUI.waitForElementVisible(manageProfilSection, 10)

TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'profile_tab_id\']'))

TestHelper.goToManageEntity()
TestObject manageNetworkRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Network Roles_manage_entity_action\']')
TestHelper.clickButton(manageNetworkRoleAction)

//int organizationsSize = TestHelper.countElementByClassName('manage_user_network_role')
//if (organizationsSize > 0) {
//    WebElement selectedOrganization = TestHelper.getItem('manage_user_network_role', 0)
//    selectedOrganization.click() 
//} else {
    TestObject emptyStateObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-network-role-empty-state\']')
    boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)

    if (emptyStateObjectPresent == true) {
        println('hasn\'t networks roles')

        TestObject emptyStateAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-network-role-empty-state-button\']')
        WebUI.click(emptyStateAction)
		createNewOrganization(organizationName, roleName)

    } else {
        println('has network roles')

        TestObject addOrganizationRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'add-new-network-role-action\']')
        WebUI.click(addOrganizationRoleAction)
		createNewOrganization(organizationName, roleName)
    }
//}

def createNewOrganization(String organizationName, String roleName) {
	UserRoleHelper.createOrganizationWithImage(organizationName, roleName)
	WebUI.delay(2);
	String organizationTarget = organizationName + '-' + roleName + '-role'
	TestObject selectedOrganization = TestHelper.getObjectById("//div[@id='"+organizationTarget+"']") 
	TestHelper.clickButton(selectedOrganization);
}

