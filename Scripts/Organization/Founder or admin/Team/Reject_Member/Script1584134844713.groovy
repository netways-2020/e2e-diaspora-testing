import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testobject.ConditionType

long date = new Date().getTime();
String registeredEmail = 'netways_user' + date + '@gmail.com'
String password = 'cvW8qx4B2o3F4VwP/kNsqA=='

String firstName = 'FirstName_' + date;
String lastName = 'LastName_' + date;
String fullName = firstName + ' ' + lastName;
String city = 'Hermel';
String roleName = 'Member';

// Login and get organization url
WebUI.callTestCase(findTestCase('Organization/Founder or admin/Authorization for organization/Login_For_Organization'), [:], FailureHandling.STOP_ON_FAILURE)
String organizationUrl = WebUI.getUrl();


// Register new user
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='organization_back_button']"))
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_network_role_back_action']"))
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-User-back-button']"))
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_manage_accounts']"))

TestObject logoutAction = TestHelper.getObjectById("//div[@id='logout-id']")
TestHelper.clickButton(logoutAction);
TestHelper.registerNewUser(registeredEmail, password, firstName, lastName, city, false);

// add organization role
joinOrganization(organizationUrl, roleName);

// Check if exist in unverified team
rejectEmployee(organizationUrl, fullName);

WebUI.delay(2);
WebUI.closeBrowser();

def joinOrganization(String organizationUrl, String roleName) {
	WebUI.navigateToUrl(organizationUrl);
	TestObject joinOrganizationAction = TestHelper.getObjectById("//div[@id='Join Organization_action']")
	boolean joinOrganizationActionExist = TestHelper.isElementPresent(joinOrganizationAction);
	
	if(joinOrganizationActionExist) {
		TestHelper.clickButton(joinOrganizationAction);
		UserRoleHelper.joinOrganization(roleName);
		WebUI.delay(3);
		WebUI.navigateToUrl(TestHelper.getHomeUrl());
		TestObject manageProfilSection = TestHelper.getObjectById("//div[@id='manageProfileId']")
		TestHelper.clickButton(manageProfilSection);
		
		TestObject logoutAction = TestHelper.getObjectById("//div[@id='logout-id']")
		TestHelper.clickButton(logoutAction);
	} else {
		TestHelper.thrownException("User already has role in this organization");
	}
}

def rejectEmployee(String organizationUrl, String memberName) {
	TestHelper.loginWithAutomationTestingUser();
	WebUI.navigateToUrl(organizationUrl);
	TestHelper.goToOrganizationManageEntity();
	TestObject manageTeamMemberAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Organization Members_manage_entity_action']")
	TestHelper.clickButton(manageTeamMemberAction);
	
	TestObject pendingMemberSearchInput = TestHelper.getObjectById("//input[@id='organization_pending_member']")
	TestHelper.setInputValue(pendingMemberSearchInput, memberName)
	WebUI.delay(2);
	
	String pendingEmployeeId = memberName + '_pending_employee_name';
	String pendingEmployeeActionId = memberName + '_pending_employee_action';
	TestObject newMember = TestHelper.getObjectById("//div[@id='"+pendingEmployeeId+"']")
	boolean newMemberExist = TestHelper.isElementPresent(newMember);
	
	if(newMemberExist) {
		TestObject pendingMemberAction = TestHelper.getObjectById("//div[@id='"+pendingEmployeeActionId+"']")
		TestHelper.clickButton(pendingMemberAction);
		
		
		// Show Current Member Profile
		TestObject viewCurrentMemberAction = TestHelper.getObjectById("//a[@id='View Profile_user_action']")
		TestHelper.clickButton(viewCurrentMemberAction);
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_backIcon_id']"))
		
		// Reject Pending Member
		TestHelper.clickButton(pendingMemberAction);
		TestObject pendingMemberRemoveRequestAction = TestHelper.getObjectById("//a[@id='Reject Request_user_action']")
		TestHelper.clickButton(pendingMemberRemoveRequestAction);
		
		WebUI.delay(2)		
		
		boolean removedMemberExist = TestHelper.isElementPresent(newMember);
		if(!removedMemberExist) {
			println('Member has been rejected successfully');
		} else {
			TestHelper.thrownException("Error during rejecting new member");
		}
	} else {
		TestHelper.thrownException("No member with this name exist");
	}
}

