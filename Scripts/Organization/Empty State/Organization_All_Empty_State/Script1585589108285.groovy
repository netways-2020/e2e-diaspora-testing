import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement

String organizationName = 'Netways_Organization' + new Date().getTime();
String roleName = 'Founder'

WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Login'), [:], FailureHandling.STOP_ON_FAILURE)
TestHelper.goToManageEntity()
TestObject manageNetworkRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Network Roles_manage_entity_action\']')
TestHelper.clickButton(manageNetworkRoleAction)

TestObject emptyStateObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-network-role-empty-state\']')
boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)

if (emptyStateObjectPresent == true) {
	println('hasn\'t networks roles')

	TestObject emptyStateAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-network-role-empty-state-button\']')
	WebUI.click(emptyStateAction)
	createNewOrganization(organizationName, roleName)

} else {
	println('has network roles')

	TestObject addOrganizationRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'add-new-network-role-action\']')	
	WebUI.click(addOrganizationRoleAction)
	createNewOrganization(organizationName, roleName)
}

def createNewOrganization(String organizationName, String roleName) {
	UserRoleHelper.createOrganization(organizationName, roleName)
	WebUI.delay(2);
	String organizationTarget = organizationName + '-' + roleName + '-role'
	TestObject selectedOrganization = TestHelper.getObjectById("//div[@id='"+organizationTarget+"']")
	TestHelper.clickButton(selectedOrganization);
	WebUI.delay(2);
	
	// Check Common Header Exist
	checkCommonHeaderElementExist();
	
	// Check General Info
	UserRoleHelper.checkOrganizationGeneralInfoFieldsEmptyState();
	
	// Check Organization Section
	checkOrganizationSectionsEmptyState();
	
	// Check Organization Section title and subtitle
	checkOrganizationSectionsTitleAndSubTitle();

	// Check SubPages Empty State
	checkSubPagesEmptyState();
	
	// Check Update Empty State
	checkUpdateEmptyState(organizationName);

	// Check Media Empty State
	checkMediaEmptyState();

	WebUI.delay(2);
	WebUI.closeBrowser();
}

def checkCommonHeaderElementExist() {
	boolean successEmptyFields = true;
	
	TestObject organizationMenuAction_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='organization_menu_action_id']")
	TestObject manage_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Manage_Organization_entity_action']")
	TestObject invite_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Invite_Organization_entity_action']")	
	
	TestObject followers_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='follower_count_id_tag_label']")
	TestObject leadership_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='leadership_id_tag_label']")

	if(!TestHelper.isElementPresent(followers_label_object)) {
		TestHelper.thrownException("Followers Label not present");
	} else if(!TestHelper.isElementPresent(leadership_label_object)) {
		TestHelper.thrownException("Leadership Label not present");
	} else if(!TestHelper.isElementPresent(organizationMenuAction_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(manage_entity_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(invite_entity_object)) {
		successEmptyFields = false;
	} 
	
	if(successEmptyFields) {
		println("Organization common header info exist");
	} else {
			TestHelper.thrownException("Error: Organization commone header info")
	}
}

def checkOrganizationSectionsEmptyState() {
	String errorMessage = "Error: Organization section empty state exist";
	
	// About Section
		TestHelper.checkObjectExit("//div[@id='organization_about']", errorMessage);
		// 	TestObject organization_about_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='organization_about']")

		
	// Affiliation
		TestHelper.checkObjectExit("//div[@id='affiliation-section']", errorMessage);
		TestHelper.checkObjectText("//div[@id='no-affiliation-empty-state-title']", "Promote business with companies & startups affiliated with you" , errorMessage);
		TestHelper.checkObjectText("//div[@id='no-affiliation-empty-state']", "Add Affiliated Companies" , errorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no-affiliation-empty-state-image']", "assets/icons/emptyState/Member_Companies.svg", errorMessage)
	
		//	TestObject organization_affiliation_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='affiliation-section']")
		//	TestObject organization_affiliation_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-affiliation-empty-state']")

	// Members and Leadership
		TestObject organization_member_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='organization_teams_members']")
		TestObject organization_leadership_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='organization_leadership_members']")
		
	// Updates
		TestHelper.checkObjectExit("//div[@id='organization_updates']", errorMessage);
		TestHelper.checkObjectText("//div[@id='no-organization-updates-empty-state-title']", "Notify the Lebanese community of important news and events" , errorMessage);
		TestHelper.checkObjectText("//div[@id='no-organization-updates-empty-state']", "Post News or Events" , errorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no-organization-updates-empty-state-image']", "assets/icons/emptyState/Post_Update.svg", errorMessage)

		//	TestObject organization_updates_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='organization_updates']")
		//	TestObject organization_updates_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-organization-updates-empty-state']")

		 
	// Chapters
		TestHelper.checkObjectExit("//div[@id='organization_chapters']", errorMessage);
		TestHelper.checkObjectText("//div[@id='no-organization-chapters-empty-state-title']", "Every chapter in an organization can indicate its parent chapter" , errorMessage);
		TestHelper.checkObjectText("//div[@id='no-organization-chapters-empty-state']", "Choose Your Parent Chapter" , errorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no-organization-chapters-empty-state-image']", "assets/icons/emptyState/Branches.svg", errorMessage)

		//	TestObject organization_chapters_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='organization_chapters']")
		//	TestObject organization_chapters_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-organization-chapters-empty-state']")

	// Committees
		TestHelper.checkObjectExit("//div[@id='organization_committee']", errorMessage);
		TestHelper.checkObjectText("//div[@id='no-organization-committee-empty-state-title']", "Create committees. Assign users and manage their roles" , errorMessage);
		TestHelper.checkObjectText("//div[@id='no-organization-committee-empty-state']", "Create a Committee" , errorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no-organization-committee-empty-state-image']", "assets/icons/emptyState/Committees.svg", errorMessage)

		//	TestObject organization_committee_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='organization_committee']")
		//	TestObject organization_committee_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-organization-committee-empty-state']")
	
	// Media
		TestHelper.checkObjectExit("//div[@id='organization_media']", errorMessage);
		TestHelper.checkObjectText("//div[@id='no-organization-media-empty-state-title']", "Show sights, achievements or projects with images and videos" , errorMessage);
		TestHelper.checkObjectText("//div[@id='no-organization-media-empty-state']", "Add Photos or Videos" , errorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no-organization-media-empty-state-image']", "assets/icons/emptyState/Media_Gallery.svg", errorMessage)

		//	TestObject organization_media_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='organization_media']")
		//	TestObject organization_media_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-organization-media-empty-state']")
 
		println("Organization section empty state exist");	
}

def checkOrganizationSectionsTitleAndSubTitle() {
	TestObject about_title_object = TestHelper.getObjectById("//div[@id='organization_about_title_id']")
	TestObject about_subTitle_object = TestHelper.getObjectById("//div[@id='organization_about_subTitle_id']")
	TestObject about_icon_object = TestHelper.getObjectById("//div[@id='organization_about_title_id_icon']")
	
	if(TestHelper.isElementPresent(about_title_object) && TestHelper.isElementPresent(about_subTitle_object)) {
		String aboutTitle = WebUI.getText(about_title_object);
		String aboutSubTitle = WebUI.getText(about_subTitle_object);
		String aboutIcon = WebUI.getText(about_icon_object);
		
		if(!aboutTitle.equals("About") || !aboutSubTitle.equals("Info, sector and contact details") || !aboutIcon.equals("assets/icons/diasporaIcon/About_Active.svg")) {
			TestHelper.thrownException("Invalid about title and subtitle")
		}
	}

	TestObject affiliation_title_object = TestHelper.getObjectById("//div[@id='organization_affiliation_title_id']")
	TestObject affiliation_subTitle_object = TestHelper.getObjectById("//div[@id='organization_affiliation_subTitle_id']")
	TestObject affiliation_icon_object = TestHelper.getObjectById("//div[@id='organization_affiliation_title_id_icon']")
	
	if(TestHelper.isElementPresent(affiliation_title_object) && TestHelper.isElementPresent(affiliation_subTitle_object)) {
		String affiliationTitle = WebUI.getText(affiliation_title_object);
		String affiliationSubTitle = WebUI.getText(affiliation_subTitle_object);
		String affiliationIcon = WebUI.getText(affiliation_icon_object);
				
		 if(!affiliationTitle.equals("Member Companies") || !affiliationSubTitle.equals("Affiliated to this org") || !affiliationIcon.equals("assets/icons/diasporaIcon/affiliation.svg")) {
			TestHelper.thrownException("Invalid affiliation title and subtitle")
		}
	}

	TestObject members_title_object = TestHelper.getObjectById("//div[@id='organization_members_title_id']")
	TestObject members_subTitle_object = TestHelper.getObjectById("//div[@id='organization_members_subTitle_id']")
	TestObject members_icon_object = TestHelper.getObjectById("//div[@id='organization_members_title_id_icon']")
	
	if(TestHelper.isElementPresent(members_title_object) && TestHelper.isElementPresent(members_subTitle_object)) {
		String membersTitle = WebUI.getText(members_title_object);
		String membersSubTitle = WebUI.getText(members_subTitle_object);
		String membersIcon = WebUI.getText(members_icon_object);
		
		if(!membersTitle.equals("Members") || !membersSubTitle.equals("Members of this organization") || !membersIcon.equals("assets/icons/diasporaIcon/People_Active.svg")) {
			TestHelper.thrownException("Invalid members title and subtitle")
		}
	}

	
	TestObject leaderships_title_object = TestHelper.getObjectById("//div[@id='organization_leadership_title_id']")
	TestObject leaderships_subTitle_object = TestHelper.getObjectById("//div[@id='organization_leadership_subTitle_id']")
	TestObject leaderships_icon_object = TestHelper.getObjectById("//div[@id='organization_leadership_title_id_icon']")
	
	if(TestHelper.isElementPresent(leaderships_title_object) && TestHelper.isElementPresent(leaderships_subTitle_object)) {
		String leadershipsTitle = WebUI.getText(leaderships_title_object);
		String leadershipsSubTitle = WebUI.getText(leaderships_subTitle_object);
		String leadershipsIcon = WebUI.getText(leaderships_icon_object);
		

		if(!leadershipsTitle.equals("Leadership") || !leadershipsSubTitle.equals("Leaders & management") || !leadershipsIcon.equals("assets/icons/diasporaIcon/vip_badge.svg")) {
			TestHelper.thrownException("Invalid leaderships title and subtitle")
		}
	}

	
	TestObject chapters_title_object = TestHelper.getObjectById("//div[@id='organization_chapters_title_id']")
	TestObject chapters_subTitle_object = TestHelper.getObjectById("//div[@id='organization_chapters_subTitle_id']")
	TestObject chapters_icon_object = TestHelper.getObjectById("//div[@id='organization_chapters_title_id_icon']")
	
	if(TestHelper.isElementPresent(chapters_title_object) && TestHelper.isElementPresent(chapters_subTitle_object)) {
		String chaptersTitle = WebUI.getText(chapters_title_object);
		String chaptersSubTitle = WebUI.getText(chapters_subTitle_object);
		String chaptersIcon = WebUI.getText(chapters_icon_object);
		
		if(!chaptersTitle.equals("Chapters") || !chaptersSubTitle.equals("Chapters subtitle") || !chaptersIcon.equals("assets/icons/diasporaIcon/branches_gray.svg")) {
			TestHelper.thrownException("Invalid chapters title and subtitle")
		}
	}

	TestObject committees_title_object = TestHelper.getObjectById("//div[@id='organization_committees_title_id']")
	TestObject committess_subTitle_object = TestHelper.getObjectById("//div[@id='organization_committees_subTitle_id']")
	TestObject committees_icon_object = TestHelper.getObjectById("//div[@id='organization_committees_title_id_icon']")
	
	if(TestHelper.isElementPresent(committees_title_object) && TestHelper.isElementPresent(committess_subTitle_object)) {
		String committeesTitle = WebUI.getText(committees_title_object);
		String committeesSubTitle = WebUI.getText(committess_subTitle_object);
		String committeesIcon = WebUI.getText(committees_icon_object);
		
		if(!committeesTitle.equals("Committees") || !committeesSubTitle.equals("Committees & members") || !committeesIcon.equals("assets/icons/diasporaIcon/committee.svg")) {
			TestHelper.thrownException("Invalid committees title and subtitle")
		}
	}

	TestObject updates_title_object = TestHelper.getObjectById("//div[@id='organization_updates_title_id']")
	TestObject updates_subTitle_object = TestHelper.getObjectById("//div[@id='organization_updates_subTitle_id']")
	TestObject updates_icon_object = TestHelper.getObjectById("//div[@id='organization_updates_title_id_icon']")
	
	if(TestHelper.isElementPresent(updates_title_object) && TestHelper.isElementPresent(updates_subTitle_object)) {
		String updatesTitle = WebUI.getText(updates_title_object);
		String updatesSubTitle = WebUI.getText(updates_subTitle_object);
		String updatesIcon = WebUI.getText(updates_icon_object);
		
		if(!updatesTitle.equals("Updates") || !updatesSubTitle.equals("Posts & discussions") || !updatesIcon.equals("assets/icons/diasporaIcon/News_Active.svg")) {
			TestHelper.thrownException("Invalid updates title and subtitle")
		}

	}

	TestObject media_title_object = TestHelper.getObjectById("//div[@id='organization_media_title_id']")
	TestObject media_subTitle_object = TestHelper.getObjectById("//div[@id='organization_media_subTitle_id']")
	TestObject media_icon_object = TestHelper.getObjectById("//div[@id='organization_media_title_id_icon']")
	
	if(TestHelper.isElementPresent(media_title_object) && TestHelper.isElementPresent(media_subTitle_object)) {
		String mediaTitle = WebUI.getText(media_title_object);
		String mediaSubTitle = WebUI.getText(media_subTitle_object);
		String mediaIcon = WebUI.getText(media_icon_object);
		
		if(!mediaTitle.equals("Media Gallery") || !mediaSubTitle.equals("Photos & videos") || !mediaIcon.equals("assets/icons/diasporaIcon/View_Gallery.svg")) {
			TestHelper.thrownException("Invalid media title and subtitle")
		}
	}
}


def checkSubPagesEmptyState() {
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Manage_Organization_entity_action']"))
		checkManageEntityIcon();
		
	// Check Manage Members Page
		String pendingMemberErrorMessage = "Error: Invalid Pending Member Empty State";
		String approvedMemberErrorMessage = "Error: Invalid Approved Member Empty State";
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Organization Members_manage_entity_action']"))
		 
		TestHelper.checkObjectText("//div[@id='no-organization-pending-members-empty-state-title']", "No Pending Requests" , pendingMemberErrorMessage);
		TestHelper.checkObjectText("//div[@id='no-organization-pending-members-empty-state-subtitle']", "There are currently no pending requests. We will notify you when a new request arrives" , pendingMemberErrorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no-organization-pending-members-empty-state-image']", "assets/icons/emptyState/Person.svg", pendingMemberErrorMessage)
	
		TestHelper.clickButton(TestHelper.getObjectById("//ion-segment-button[@id='current_employee_tab_id']"))
		
		
		TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='organization_current_member']"), 'abcdefgh123');
		WebUI.delay(2);
 
		TestHelper.checkObjectText("//div[@id='no-organization-current-members-empty-state-title']", "No Members" , approvedMemberErrorMessage);
		TestHelper.checkObjectText("//div[@id='no-organization-current-members-empty-state-subtitle']", "Approved join requests will appear here" , approvedMemberErrorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no-organization-current-members-empty-state-image']", "assets/icons/emptyState/Person.svg", approvedMemberErrorMessage)
 
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-organization-member-back-button']"))
					
	// Check Administrator Page
		String administratorListPageErrorMessage = "Error: Invalid Administrator List Empty State";
		String addAdministratorPageErrorMessage = "Error: Invalid Add Administrator Empty State";
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Page Administrators_manage_entity_action']"))
		 
		TestHelper.checkObjectText("//div[@id='manage-admin-organization-empty-state-title']", "No Page Administrators" , administratorListPageErrorMessage);
		TestHelper.checkObjectText("//div[@id='manage-admin-organization-empty-state-subtitle']", "This page doesn’t currently have any administrators. You can assign a page member or invite someone via email" , administratorListPageErrorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='manage-admin-organization-empty-state-image']", "assets/icons/emptyState/Person.svg", administratorListPageErrorMessage)
		TestHelper.checkObjectText("//div[@id='manage-admin-organization-add-new-admin-empty-state']", "Add Page Administrator" , administratorListPageErrorMessage);
		
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-admin-organization-add-new-admin-empty-state']"))
		
		TestHelper.checkObjectText("//div[@id='add_admin_organization_member_empty_state_title']", "No Members" , addAdministratorPageErrorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='add_admin_organization_member_empty_state_image']", "assets/icons/emptyState/Person.svg", addAdministratorPageErrorMessage)
		 
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='add-admin-organization-back-button']"))
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-admin-organization-back-button']"))
		
	// Manage Committees Page
		String manageCommitteeErrorMessage = "Error: Invalid Manage Committee Empty State";
		String manageCommitteeMemberErrorMessage = "Error: Invalid Manage Committee Member Empty State";
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Committees_manage_entity_action']"))
		 
		TestHelper.checkObjectText("//div[@id='no-organization-committees-empty-state-title']", "No Committees Listed" , manageCommitteeErrorMessage);
		TestHelper.checkObjectText("//div[@id='no-organization-committees-empty-state-subtitle']", "This page doesn’t currently have any listed support staff. Tap the button below to create one" , manageCommitteeErrorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no-organization-committees-empty-state-image']", "assets/icons/emptyState/List.svg", manageCommitteeErrorMessage)
		TestHelper.checkObjectText("//div[@id='no-organization-committees-empty-state-button']", "Create Committee", manageCommitteeErrorMessage)
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='no-organization-committees-empty-state-button']"))
		TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='committee-name-input']"), "testCommittee")
		WebUI.delay(2);
		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='add-committee-name-action']"))
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='testCommittee_org_committee']"))

	// Committe Member Page			
		TestHelper.checkObjectText("//div[@id='join-committee-member-empty-state-title']", "No Members" , manageCommitteeMemberErrorMessage);
		TestHelper.checkObjectText("//div[@id='join-committee-member-empty-state-subtitle']", "This committee doesn’t currently have any members. Tap the button below to add or join" , manageCommitteeMemberErrorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='join-committee-member-empty-state-image']", "assets/icons/emptyState/Person.svg", manageCommitteeMemberErrorMessage)
		TestHelper.checkObjectText("//div[@id='add-committee-member-empty-state-button']", "Add Members", manageCommitteeMemberErrorMessage)
		TestHelper.checkObjectText("//div[@id='join-committee-member-empty-state-button']", "Join Committee", manageCommitteeMemberErrorMessage)
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='add-committee-member-empty-state-button']"));
		TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='add-committee-member-input-search']"), "abcdefgh123")
		WebUI.delay(2);

		TestHelper.checkObjectText("//div[@id='add-committee-member-empty-state-title']", "No Members" , manageCommitteeMemberErrorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='add-committee-member-empty-state-image']", "assets/icons/emptyState/Person.svg", manageCommitteeMemberErrorMessage)
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='add_committee_member_back_action']"));
		
				
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_committee_members_back_action']"))
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_committee_back_action']"))

		
	// Check Manage Affiliation Page
	   String managePendingAffiliationPageErrorMessage = "Error: Invalid Manage Pending Affiliation Empty State";
	   String manageCurrentAffiliationPageErrorMessage = "Error: Invalid Manage Current Affiliation Empty State";
	   
	   TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Member Companies_manage_entity_action']"))
		
	   TestHelper.checkObjectText("//div[@id='no-pending-affiliation-request-empty-state_title']", "No Pending Requests" , managePendingAffiliationPageErrorMessage);
	   TestHelper.checkObjectText("//div[@id='no-pending-affiliation-request-empty-state_subtitle']", "There are currently no pending requests. We will notify you when a new request arrives" , managePendingAffiliationPageErrorMessage);
	   TestHelper.checkObjectImageSrc("//div[@id='no-pending-affiliation-request-empty-state_image']", "assets/icons/emptyState/List.svg", managePendingAffiliationPageErrorMessage)
	   TestHelper.checkObjectText("//div[@id='no-pending-company-affiliation-request-empty-state']", "Add Company", managePendingAffiliationPageErrorMessage)
	   
	   TestHelper.clickButton(TestHelper.getObjectById("//ion-segment-button[@id='manage_company_current_affiliation']"))
	  
	   TestHelper.checkObjectText("//div[@id='no-current-affiliation-request-empty-state_title']", "No Companies" , manageCurrentAffiliationPageErrorMessage);
	   TestHelper.checkObjectText("//div[@id='no-current-affiliation-request-empty-state_subtitle']", "Approved join requests will appear here" , manageCurrentAffiliationPageErrorMessage);
	   TestHelper.checkObjectImageSrc("//div[@id='no-current-affiliation-request-empty-state_image']", "assets/icons/emptyState/List.svg", manageCurrentAffiliationPageErrorMessage)
	   TestHelper.checkObjectText("//div[@id='no-current-company-affiliation-request-empty-state']", "Add Company", manageCurrentAffiliationPageErrorMessage)

	   TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_company_affiliation_back_action']"))
	   
	 // Check Manage Chapters
	   String manageIncommingChaptersPageErrorMessage = "Error: Invalid Manage Incomming Chapter Empty State";
	   String manageOutgoingChaptersPageErrorMessage =  "Error: Invalid Manage Sent Chapter Empty State";
	   String manageCurrentChaptersPageErrorMessage =  "Error: Invalid Manage Current Chapter Empty State";

	   TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Chapters_manage_entity_action']"))
	   
	   TestHelper.checkObjectText("//div[@id='manage_chapters_incomming_empty_state_title']", "No Incoming Requests" , manageIncommingChaptersPageErrorMessage);
	   TestHelper.checkObjectText("//div[@id='manage_chapters_incomming_empty_state_subtitle']", "There are currently no incoming requests. We will notify you when a new request arrives" , manageIncommingChaptersPageErrorMessage);
	   TestHelper.checkObjectImageSrc("//div[@id='manage_chapters_incomming_empty_state_image']", "assets/icons/emptyState/List.svg", manageIncommingChaptersPageErrorMessage)

	   TestHelper.clickButton(TestHelper.getObjectById("//ion-segment-button[@id='manage_organization_chapters_outgoing_request']"))
	   
	   TestHelper.checkObjectText("//div[@id='manage_chapters_outgoing_empty_state_title']", "No Outgoing Requests" , manageOutgoingChaptersPageErrorMessage);
	   TestHelper.checkObjectText("//div[@id='manage_chapters_outgoing_empty_state_subtitle']", "You haven’t sent any join requests" , manageOutgoingChaptersPageErrorMessage);
	   TestHelper.checkObjectImageSrc("//div[@id='manage_chapters_outgoing_empty_state_image']", "assets/icons/emptyState/List.svg", manageOutgoingChaptersPageErrorMessage)
	   TestHelper.checkObjectText("//div[@id='manage_organization_chapters_add_parent_organization_outgoing_empty_state']", "Add Parent Organization" , manageOutgoingChaptersPageErrorMessage);
			  
	   TestHelper.clickButton(TestHelper.getObjectById("//ion-segment-button[@id='manage_organization_chapters_current_request']"))
	   
	   TestHelper.checkObjectText("//div[@id='manage_chapters_current_empty_state_title']", "No Organization Chapters Listed" , manageCurrentChaptersPageErrorMessage);
	   TestHelper.checkObjectText("//div[@id='manage_chapters_current_empty_state_subtitle']", "Start by adding your parent organization. When your request is approved, the organization’s structure will appear here" , manageCurrentChaptersPageErrorMessage);
	   TestHelper.checkObjectImageSrc("//div[@id='manage_chapters_current_empty_state_image']", "assets/icons/emptyState/List.svg", manageCurrentChaptersPageErrorMessage)
	   TestHelper.checkObjectText("//div[@id='manage_organization_chapters_add_parent_organization_current_empty_state']", "Add Parent Organization" , manageCurrentChaptersPageErrorMessage);
		 
	   TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-organization-chapters-back-button']"))
	   TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-Organization-back-button']"))
	   
	   // Check follwers
	   String followerErrorMessage = "Error: Invalid Followers Page Empty State";
		   
	   WebUI.delay(1)
	   TestHelper.clickButton(TestHelper.getObjectById("//span[@id='follower_count_id_tag_label']"));
	   TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='search-organization-follower-input']"), 'abcdefgh123');
	   WebUI.delay(2);

	   TestHelper.checkObjectText("//div[@id='no_followers_title']", "No Results Found" , followerErrorMessage);
	   TestHelper.checkObjectImageSrc("//div[@id='no_followers_image']", "assets/icons/emptyState/Person.svg", followerErrorMessage)
 
	   TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back-follower-id']"));
		
}

def checkUpdateEmptyState(String organizationName) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='no-organization-updates-empty-state']"));
	long now = new Date().getTime();
	String postTitle = 'New post title add now_' + now;
	String postDescription = 'New post description add now_' + now;
		
	TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Create Post_user_action']"));
	
	UserActionsHelper.addPost(postTitle, postDescription)
	WebUI.delay(2);
	TestHelper.verifyCardExist(organizationName, postTitle, null, false);
	
	TestObject manageCardObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='organization_manage_updates']")
	boolean manageCardObjectExist = TestHelper.isElementPresent(manageCardObject);
	if(!manageCardObjectExist) {
		TestHelper.thrownException("Manage card button not exist");
	}
	
	TestHelper.clickButton(manageCardObject);
	String cartId = postTitle + '_descriptionId';
	TestHelper.clickButton(TestHelper.getObjectById("//span[@id='" + cartId + "']"));
		

	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='card_details_manage_button']"))
	WebUI.delay(1);
	TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Delete Post_user_action']"))
	WebUI.delay(1);
	
	WebElement deleteCardConfirmation = TestHelper.getItem('remove-new-card', 0)
	deleteCardConfirmation.click();
	
	WebUI.delay(1);
	
	String updateErrorMessage = "Error: Invalid Updates List Empty State";
	TestHelper.checkObjectText("//div[@id='no_updates_empty_state_title']", "Nothing to Show Here" , updateErrorMessage);
	TestHelper.checkObjectText("//div[@id='no_updates_empty_state_subtitle']", "There are no entries to display on this page" , updateErrorMessage);
	TestHelper.checkObjectImageSrc("//div[@id='no_updates_empty_state_image']", "assets/icons/emptyState/List.svg", updateErrorMessage)

	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back_button_cardListing']"))
}

def checkMediaEmptyState() {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='no-organization-media-empty-state']"));

	long now = new Date().getTime();
	String imageCaption = 'caption' + now;
	UserActionsHelper.addMedia(imageCaption);
	TestObject addPhotoAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='organization_manage_media']")
	WebUI.click(addPhotoAction)
	
	TestHelper.checkIfImageAddedSuccessfully(true, 0);
	TestHelper.removeImage();
	
	WebUI.delay(1);
	String mediaErrorMessage = "Error: Invalid Media Empty State";
	TestHelper.checkObjectText("//div[@id='no_gallery_images_empty_state_title']", "No Photo Or Video Added" , mediaErrorMessage);
	TestHelper.checkObjectImageSrc("//div[@id='no_gallery_images_empty_state_image']", "assets/icons/emptyState/List.svg", mediaErrorMessage)

	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back_button_gallery']"))
}



def checkManageEntityIcon() {
	String manage_entity_error_message = 'Invalid manage entity info';
	
	TestHelper.checkObjectText("//div[@id='Organization Information_manage_entity_action']", "Organization Information" , manage_entity_error_message);
	TestHelper.checkObjectText("//div[@id='Organization Information_manage_entity_subtitle_action']", "Logo, description, organization type…" , manage_entity_error_message);
	TestHelper.checkObjectImageSrc("//div[@id='Organization Information_manage_entity_icon_action']", "assets/icons/diasporaIcon/Basic_Information.svg", manage_entity_error_message)
	   
	TestHelper.checkObjectText("//div[@id='Organization Members_manage_entity_action']", "Organization Members" , manage_entity_error_message);
	TestHelper.checkObjectText("//div[@id='Organization Members_manage_entity_subtitle_action']", "Tap here to setup as a new company" , manage_entity_error_message);
	TestHelper.checkObjectImageSrc("//div[@id='Organization Members_manage_entity_icon_action']", "assets/icons/diasporaIcon/People.svg", manage_entity_error_message)
	 
	TestHelper.checkObjectText("//div[@id='Page Administrators_manage_entity_action']", "Page Administrators" , manage_entity_error_message);
	TestHelper.checkObjectText("//div[@id='Page Administrators_manage_entity_subtitle_action']", "Assign or remove page administrators" , manage_entity_error_message);
	TestHelper.checkObjectImageSrc("//div[@id='Page Administrators_manage_entity_icon_action']", "assets/icons/diasporaIcon/Page_Administrator.svg", manage_entity_error_message)
	 
	TestHelper.checkObjectText("//div[@id='Committees_manage_entity_action']", "Committees" , manage_entity_error_message);
	TestHelper.checkObjectText("//div[@id='Committees_manage_entity_subtitle_action']", "Create and manage committees" , manage_entity_error_message);
	TestHelper.checkObjectImageSrc("//div[@id='Committees_manage_entity_icon_action']", "assets/icons/diasporaIcon/Committees.svg", manage_entity_error_message)
 
	TestHelper.checkObjectText("//div[@id='Member Companies_manage_entity_action']", "Member Companies" , manage_entity_error_message);
	TestHelper.checkObjectText("//div[@id='Member Companies_manage_entity_subtitle_action']", "Listing requests under your org" , manage_entity_error_message);
	TestHelper.checkObjectImageSrc("//div[@id='Member Companies_manage_entity_icon_action']", "assets/icons/diasporaIcon/Member_Companies.svg", manage_entity_error_message)
 
	TestHelper.checkObjectText("//div[@id='Chapters_manage_entity_action']", "Chapters" , manage_entity_error_message);
	TestHelper.checkObjectText("//div[@id='Chapters_manage_entity_subtitle_action']", "Organization structure & branches" , manage_entity_error_message);
	TestHelper.checkObjectImageSrc("//div[@id='Chapters_manage_entity_icon_action']", "assets/icons/diasporaIcon/Chapters.svg", manage_entity_error_message)
}    
  
