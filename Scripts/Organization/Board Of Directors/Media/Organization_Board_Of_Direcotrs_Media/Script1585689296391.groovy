import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import org.junit.After

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement

long now = new Date().getTime();

String organizationName = 'Netways_Organization' + now;
String roleName = 'BoardOfDirectory'
String imageCaption = 'caption' + now;

 
WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Register_new_user'), [:], FailureHandling.STOP_ON_FAILURE)
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_tab_id']"))
TestHelper.goToManageEntity()
TestObject manageNetworkRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Network Roles_manage_entity_action\']')
TestHelper.clickButton(manageNetworkRoleAction)

TestObject emptyStateObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-network-role-empty-state\']')
boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)

if (emptyStateObjectPresent == true) {
	println('hasn\'t networks roles')

	TestObject emptyStateAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-network-role-empty-state-button\']')
	WebUI.click(emptyStateAction)
	createNewOrganization(organizationName, roleName, imageCaption)

} else {
	println('has networks roles')

	TestObject addOrganizationRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'add-new-network-role-action\']')
	WebUI.click(addOrganizationRoleAction)
	createNewOrganization(organizationName, roleName, imageCaption)
}

def createNewOrganization(String organizationName, String roleName, String imageCaption) {
	UserRoleHelper.createOrganizationAsBoardOfDirectory(organizationName, roleName)
	WebUI.delay(2);
	String organizationTarget = organizationName + '-' + roleName + '-role'
	println(organizationTarget);
	TestObject selectedOrganization = TestHelper.getObjectById("//div[@id='"+organizationTarget+"']")
	WebUI.delay(2);
	TestHelper.clickButton(selectedOrganization);
	WebUI.delay(2);

	TestHelper.getObjectById("//div[@id='organization_media']")
	TestObject emptyStateOrganizationGallery = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-organization-media-empty-state']")
	boolean emptyStateOrganizationGalleryExist = TestHelper.isElementPresent(emptyStateOrganizationGallery)
	WebUI.click(emptyStateOrganizationGallery);
	UserActionsHelper.addMedia(imageCaption);	
	
	TestObject addPhotoAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='organization_manage_media']")
	WebUI.click(addPhotoAction)
	
	checkIfImageAddedSuccessfully(true, 0); 
	removeImage();
}

 	
def removeImage() { 
	int imageCountBeforAdd = TestHelper.countElementByClassName('gallery-container');
	WebElement element = TestHelper.getItem('gallery-container', 0);
	element.click();
	
	TestHelper.checkImageLoaded("image_detail_id");
	
	TestObject imageCaption = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='image-caption-id']")
	boolean imageCaptionExist = TestHelper.isElementPresent(imageCaption);

	if(imageCaptionExist) {
		print("Image caption exist");
	} else {
		TestHelper.thrownException("Image caption not exist");
	}
	
	TestObject removeAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='galler_image_id']")
	boolean removeActionExist = TestHelper.isElementPresent(removeAction);
 
	if(removeActionExist) {
		TestHelper.clickButton(removeAction)
		UserActionsHelper.removeMedia();
		this.checkIfImageRemovedSuccessfully(imageCountBeforAdd);
	} else {
		TestHelper.thrownException("Doesn't have the privilege tot delete an image");
	}	
}


def checkIfImageAddedSuccessfully(boolean firstImageAdded, int imageCountBeforAdd) {
	int imageCount = TestHelper.countElementByClassName('gallery-container');
	 
	if(firstImageAdded) {
		if(imageCount == 1) {
			println("Image added successfully");
		} else {
			TestHelper.thrownException("Image not added successfully");
		}
	} else { 
	   if((imageCountBeforAdd + 1) == imageCount) {
		   println("Image added successfully");
	   } else {
		   TestHelper.thrownException("Image not added successfully");
	   }
	}
}


def checkIfImageRemovedSuccessfully(int imageCountBeforAdd) {
	int imageCount = TestHelper.countElementByClassName('gallery-container');
	
	if((imageCountBeforAdd - 1) == imageCount) {
	   println("Image deleted successfully");
	   WebUI.closeBrowser();
   } else {
	   TestHelper.thrownException("Image not deleted successfully");
   } 
}
 