import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable


String organizationName = 'Netways_Organization' + new Date().getTime();
String roleName = 'BoardOfDirectory'

 
WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Register_new_user'), [:], FailureHandling.STOP_ON_FAILURE)
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_tab_id']"))
TestHelper.goToManageEntity()
TestObject manageNetworkRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Network Roles_manage_entity_action\']')
TestHelper.clickButton(manageNetworkRoleAction) 

TestObject emptyStateObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-network-role-empty-state\']')
boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)

if (emptyStateObjectPresent == true) {
	println('hasn\'t networks roles')

	TestObject emptyStateAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-network-role-empty-state-button\']')
	WebUI.click(emptyStateAction)
	createNewOrganization(organizationName, roleName)

} else {
	println('has networks roles')

	TestObject addOrganizationRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'add-new-network-role-action\']')	
	WebUI.click(addOrganizationRoleAction)
	createNewOrganization(organizationName, roleName)
}

def createNewOrganization(String organizationName, String roleName) {
	UserRoleHelper.createOrganizationAsBoardOfDirectory(organizationName, roleName)
	WebUI.delay(2);
	String organizationTarget = organizationName + '-' + roleName + '-role'
	println(organizationTarget);
	TestObject selectedOrganization = TestHelper.getObjectById("//div[@id='"+organizationTarget+"']")
	WebUI.delay(2);
	TestHelper.clickButton(selectedOrganization);
	WebUI.delay(2);
	
	checkEmptyState();	
	WebUI.closeBrowser();
}

def checkEmptyState() {
	String organizationMediaEmptyStateErrorMessage = "Error: Organization media empty state exist";
	
	TestObject organizationMenuAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='organization_menu_action_id']")
	TestObject message_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Message_Organization_entity_action']")
	TestObject following_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Following_Organization_entity_action']")
	
	
	TestObject followers_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='follower_count_id_tag_label']")
	TestObject leadership_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='leadership_id_tag_label']")

	if(!TestHelper.isElementPresent(followers_label_object)) {
		TestHelper.thrownException("Followers Label not present");
	} else if(!TestHelper.isElementPresent(leadership_label_object)) {
		TestHelper.thrownException("Leadership Label not present");
	} else if(!TestHelper.isElementPresent(following_entity_object)) {
		TestHelper.thrownException("Following Action not present");
	} else if(!TestHelper.isElementPresent(message_entity_object)) {
		TestHelper.thrownException("Message Action not present");
	}  
	
	TestHelper.checkObjectExit("//div[@id='organization_media']", organizationMediaEmptyStateErrorMessage);
	TestHelper.checkObjectText("//div[@id='no-organization-media-empty-state-title']", "Show sights, achievements or projects with images and videos" , organizationMediaEmptyStateErrorMessage);
	TestHelper.checkObjectText("//div[@id='no-organization-media-empty-state']", "Add Photos or Videos" , organizationMediaEmptyStateErrorMessage);
	TestHelper.checkObjectImageSrc("//div[@id='no-organization-media-empty-state-image']", "assets/icons/emptyState/Media_Gallery.svg", organizationMediaEmptyStateErrorMessage)


	if(TestHelper.isElementPresent(organizationMenuAction)) {
		TestHelper.clickButton(organizationMenuAction);

		TestObject reportOrganizationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Report A Problem_user_action']")
		TestObject shareOrganizationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Share_user_action']")
		TestObject claimOrganizationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Claim this organization_user_action']")
		
		if(!TestHelper.isElementPresent(reportOrganizationAction) || !TestHelper.isElementPresent(shareOrganizationAction) || !TestHelper.isElementPresent(claimOrganizationAction)) {
			TestHelper.thrownException("Organization Action not present");
		} else {
			print("Check Complete")
		}
	} else {
		TestHelper.thrownException("Menu Action not present");
	}
}