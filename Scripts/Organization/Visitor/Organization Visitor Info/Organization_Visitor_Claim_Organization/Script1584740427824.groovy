import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

// Register new user and go to my profile tab
long date = new Date().getTime();
String registeredEmail = 'netways_user' + date + '@gmail.com'
String password = 'cvW8qx4B2o3F4VwP/kNsqA=='

String firstName = 'FirstName_' + date;
String lastName = 'LastName_' + date;
String fullName = firstName + ' ' + lastName;
String city = 'Hermel';
 

TestHelper.registerNewUser(registeredEmail, password, firstName, lastName, city, true);
WebUI.navigateToUrl(TestHelper.getLebanesRedCross_organizationUrl());
checkEmptyState();
WebUI.closeBrowser();
 
def checkEmptyState() {
	TestObject organizationMenuAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='organization_menu_action_id']")
 
	if(TestHelper.isElementPresent(organizationMenuAction)) {
		TestHelper.clickButton(organizationMenuAction);

		TestObject claimOrganizationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Claim this organization_user_action']")
 		
		if(!TestHelper.isElementPresent(claimOrganizationAction)) {
			TestHelper.thrownException("Organization Action not present");
		} else {
			TestHelper.clickButton(claimOrganizationAction);
			TestHelper.clickButton(TestHelper.getObjectById("//button[@id='claim-entity-action-id']"))
			print("Check Complete")
		}
	} else {
		TestHelper.thrownException("Menu Action not present");
	}
}