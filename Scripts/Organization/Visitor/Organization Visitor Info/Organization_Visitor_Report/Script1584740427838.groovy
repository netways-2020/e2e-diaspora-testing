import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

String organizationName = 'Netways_Organization' + new Date().getTime();
String roleName = 'Member';

String reportDescription = 'Report description for '+ organizationName;
String reportContact = 'Report Contact for '+ organizationName;


// Login and go to my profile tab
WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Login'), [:], FailureHandling.STOP_ON_FAILURE)
TestHelper.goToManageEntity()
TestObject manageNetworkRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Network Roles_manage_entity_action\']')
TestHelper.clickButton(manageNetworkRoleAction)

TestObject emptyStateObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-network-role-empty-state\']')
boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)

if (emptyStateObjectPresent == true) {
	println('hasn\'t networks roles')

	TestObject emptyStateAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-network-role-empty-state-button\']')
	WebUI.click(emptyStateAction)
	createNewOrganization(organizationName, roleName, reportDescription, reportContact)

} else {
	println('has networks roles')

	TestObject addOrganizationRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'add-new-network-role-action\']')	
	WebUI.click(addOrganizationRoleAction)
	createNewOrganization(organizationName, roleName,reportDescription, reportContact)
}

def createNewOrganization(String organizationName, String roleName, String reportDescription, String reportContact ) {
	UserRoleHelper.createOrganizationAsEmploye(organizationName, roleName)
	WebUI.delay(2);
	String organizationTarget = organizationName + '-' + roleName + '-role'
	println(organizationTarget);
	TestObject selectedOrganization = TestHelper.getObjectById("//div[@id='"+organizationTarget+"']")
	TestHelper.clickButton(selectedOrganization);
	WebUI.delay(2);
	
	reportOrganization(reportDescription, reportContact)
	WebUI.closeBrowser();
}

def reportOrganization(String reportDescription, String reportContact ) {
	TestObject organizationMenuAction = TestHelper.getObjectById("//div[@id='organization_menu_action_id']")
 
	if(TestHelper.isElementPresent(organizationMenuAction)) {
		TestHelper.clickButton(organizationMenuAction);

		TestObject reportOrganizationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Report A Problem_user_action']")
		
		if(!TestHelper.isElementPresent(reportOrganizationAction)) {
			TestHelper.thrownException("Organization Action not present");
		} else {
			TestHelper.clickButton(reportOrganizationAction);
			UserActionsHelper.sendReport(reportDescription, reportContact);
		}
	} else {
		TestHelper.thrownException("Menu Action not present");
	}
}


