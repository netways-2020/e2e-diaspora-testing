import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
 
String organizationName = 'Netways_Organization' + new Date().getTime();
String roleName = 'Member'

WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Register_new_user'), [:], FailureHandling.STOP_ON_FAILURE)
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_tab_id']"))
  
TestHelper.goToManageEntity()
TestObject manageNetworkRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Network Roles_manage_entity_action\']')
TestHelper.clickButton(manageNetworkRoleAction) 

TestObject emptyStateObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-network-role-empty-state\']')
boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)

if (emptyStateObjectPresent == true) {
	println('hasn\'t networks roles')

	TestObject emptyStateAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-network-role-empty-state-button\']')
	WebUI.click(emptyStateAction)
	createNewOrganization(organizationName, roleName)

} else {
	println('has networks roles')

	TestObject addOrganizationRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'add-new-network-role-action\']')	
	WebUI.click(addOrganizationRoleAction)
	createNewOrganization(organizationName, roleName)
}

def createNewOrganization(String organizationName, String roleName) {
	UserRoleHelper.createOrganizationAsEmploye(organizationName, roleName)
	WebUI.delay(2);
	String organizationTarget = organizationName + '-' + roleName + '-role'
	println(organizationTarget);
	TestObject selectedOrganization = TestHelper.getObjectById("//div[@id='"+organizationTarget+"']")
	WebUI.delay(2);
	TestHelper.clickButton(selectedOrganization);
	WebUI.delay(2);
	
	addNewNews(organizationName);	
	WebUI.closeBrowser();
}

 
def addNewNews(String organizationName) {
	TestObject organizationUpdateAction = TestHelper.getObjectById("//div[@id='no-organization-updates-empty-state']");
	if(!TestHelper.isElementPresent(organizationUpdateAction)) {
		TestHelper.thrownException("Update Empty State Not Exist");
	}
	
	TestHelper.clickButton(organizationUpdateAction);
	TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Create Post_user_action']"));
	
	long now = new Date().getTime();
	String postTitle = 'New post title add now_' + now;
	String postDescription = 'New post description add now_' + now;
	
	UserActionsHelper.addPost(postTitle, postDescription)
	WebUI.delay(2);
	TestHelper.verifyCardExist(organizationName, postTitle, 'organization', true);
	
	// Verify Card Features
	verifyCardFeature(postTitle, postDescription, organizationName);
}
 
def verifyCardFeature(String postTitle, String postDescription, String organizationName) {
	TestObject manageCardObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='organization_manage_updates']")
	boolean manageCardObjectExist = TestHelper.isElementPresent(manageCardObject);
	if(!manageCardObjectExist) {
		TestHelper.thrownException("Manage card button not exist");
	}
	
	TestHelper.clickButton(manageCardObject);
	String cartId = postTitle + '_descriptionId';
	TestHelper.clickButton(TestHelper.getObjectById("//span[@id='" + cartId + "']"));
	
	String newPostTitle =  "new " + postTitle;
	String newPostDescription =  "new " + postDescription;
	
	TestHelper.checkPostCardFeatures(postTitle, postDescription, newPostTitle, newPostDescription, organizationName);
	

	WebUI.delay(3);
	String newCartId = newPostTitle + '_descriptionId';
	TestObject deleteCardObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='" + newCartId + "']")
	boolean deleteCardObjectExist = TestHelper.isElementPresent(deleteCardObject);
	
	if(deleteCardObjectExist) {
		TestHelper.thrownException("Card not deleted still exist in the card list");
	} else {
		checkCardRemovedFromMainPage(postDescription);
	}
}

def checkCardRemovedFromMainPage(String cardDescriptionId) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back_button_cardListing']"))
	TestObject cardDescriptionObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+ cardDescriptionId  + "_id']")
	
	boolean cardDescriptionObjectExist = TestHelper.isElementPresent(cardDescriptionObject)

	if(cardDescriptionObjectExist) {
		TestHelper.thrownException("Card not deleted from main page")
	} else {
		println("Test Complete");
		WebUI.closeBrowser();
	}

}



