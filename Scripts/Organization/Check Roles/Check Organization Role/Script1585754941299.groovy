import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

String organizationName = 'Netways_Organization' + new Date().getTime();

String founderRoleName = 'Founder'
String memberRoleName = 'Member'
String boardOfDirectoryRoleName = 'BoardOfDirectory'
String committeeMemberRoleName = 'CommitteMemberRole'
String committeeName = "NewCommitte";


WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Register_new_user'), [:], FailureHandling.STOP_ON_FAILURE)
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_tab_id']"))
TestHelper.goToManageEntity()
TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'Network Roles_manage_entity_action\']'))

// Founder Role
checkNetworkRole(organizationName, founderRoleName);
checkFounderSections();

// Member Role 
changeToMemberRole(organizationName, founderRoleName, memberRoleName);
checkNonAdminEmptyState();

// Board of directory Role
changeToBoardOfDirectoryRole(organizationName, memberRoleName, boardOfDirectoryRoleName);
checkNonAdminEmptyState();

// Committe Or Board Member Role
changeToCommitteeMemberRole(organizationName, committeeMemberRoleName, committeeName);
checkNonAdminEmptyState();

WebUI.delay(2);
WebUI.closeBrowser();

// Create Organization
def checkNetworkRole(String organizationName, String founderRoleName) {
	TestObject emptyStateObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-network-role-empty-state\']')
	boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)
	
	if (emptyStateObjectPresent == true) {
		println('hasn\'t networks roles')
	
		TestObject emptyStateAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-network-role-empty-state-button\']')
		WebUI.click(emptyStateAction)
		createNewOrganization(organizationName, founderRoleName)
	
	} else {
		println('has network roles')
	
		TestObject addOrganizationRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'add-new-network-role-action\']')
		WebUI.click(addOrganizationRoleAction)
		createNewOrganization(organizationName, founderRoleName)
	}	
}
 
def createNewOrganization(String organizationName, String founderRoleName) {
	UserRoleHelper.createOrganization(organizationName, founderRoleName)
	WebUI.delay(2);
	String organizationTarget = organizationName + '-' + founderRoleName + '-role'
	TestObject selectedOrganization = TestHelper.getObjectById("//div[@id='"+organizationTarget+"']")
	TestHelper.clickButton(selectedOrganization);
	WebUI.delay(2);
}

// Check Founder Section
def checkFounderSections() {
	checkFounderCommonHeaderElementExist();
	checkFounderOrganizationSectionsEmptyState();
	checkFounderOrganizationSectionsTitleAndSubTitle();
}


// Change From Founder To Member
def changeToMemberRole(String organizationName, String founderRoleName, String memberRoleName) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='organization_back_button']"))
	
	String adminorganizationTarget = organizationName + '-options-icon'
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='"+adminorganizationTarget+"']"));
		
	TestObject editRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Edit Role_user_action']")
	TestHelper.clickButton(editRoleAction);
	
	TestObject networkRolePositionType = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='network-role-position-type']")
	TestHelper.clickButton(networkRolePositionType)
	
	TestObject networkRolePositionTypeValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Member_item']")
	TestHelper.clickButton(networkRolePositionTypeValue)
	
	 
	TestObject businessRolePositionName = new TestObject().addProperty("xpath", ConditionType.EQUALS,  "//input[@id='network-role-position-name']")
	TestHelper.setInputValue(businessRolePositionName, memberRoleName)
	
	TestObject updateBusinessRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='update_network_role']")
	TestHelper.clickButton(updateBusinessRoleAction);
	
	String memberOrganizationTarget = organizationName + '-' + memberRoleName + '-role'
	TestObject selectedOrganization = TestHelper.getObjectById("//div[@id='"+memberOrganizationTarget+"']")
	WebUI.delay(2);
	TestHelper.clickButton(selectedOrganization);
	WebUI.delay(2);
}

// Change From Member To Board Of Directory
def changeToBoardOfDirectoryRole(String organizationName, String memberRoleName, String boardOfDirectoryRoleName) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='organization_back_button']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='organization_back_button']"))
	
	String memberTarget = organizationName + '-options-icon'
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='"+memberTarget+"']"));
		
	TestObject editRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Edit Role_user_action']")
	TestHelper.clickButton(editRoleAction);
	
	TestObject networkRolePositionType = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='network-role-position-type']")
	TestHelper.clickButton(networkRolePositionType)
	
	TestObject networkRolePositionTypeValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Board of Directors_item']")
	TestHelper.clickButton(networkRolePositionTypeValue)
	
	 
	TestObject businessRolePositionName = new TestObject().addProperty("xpath", ConditionType.EQUALS,  "//input[@id='network-role-position-name']")
	TestHelper.setInputValue(businessRolePositionName, boardOfDirectoryRoleName)
	
	TestObject updateBusinessRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='update_network_role']")
	TestHelper.clickButton(updateBusinessRoleAction);
	
	String boardOfDirectoryOrganizationTarget = organizationName + '-' + boardOfDirectoryRoleName + '-role'
	TestObject selectedOrganization = TestHelper.getObjectById("//div[@id='"+boardOfDirectoryOrganizationTarget+"']")
	WebUI.delay(2);
	TestHelper.clickButton(selectedOrganization);
	WebUI.delay(2);
}

// Change From Board Of Directory To Committee Member
def changeToCommitteeMemberRole(String organizationName, String committeeMemberRoleName, String committeeName) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='organization_back_button']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='organization_back_button']"))
	
	String boardOfDirectoryMemberTarget = organizationName + '-options-icon'
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='"+boardOfDirectoryMemberTarget+"']"));
		
	TestObject editRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Edit Role_user_action']")
	TestHelper.clickButton(editRoleAction);
	
	TestObject networkRolePositionType = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='network-role-position-type']")
	TestHelper.clickButton(networkRolePositionType)
	
	TestObject networkRolePositionTypeValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Committee or Board Member_item']")
	TestHelper.clickButton(networkRolePositionTypeValue)
	
	
	TestObject committeePositionTypeButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='committee-position-type']")
	WebUI.click(committeePositionTypeButton)

	TestObject addCommitteeAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='add_committee_action']")
	WebUI.click(addCommitteeAction)
	
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='committee-name-input']"), committeeName);
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='add-committee-name-action']"))
	WebUI.delay(1);
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='NewCommitte_committee_item']"))
	 
	TestObject businessRolePositionName = new TestObject().addProperty("xpath", ConditionType.EQUALS,  "//input[@id='network-role-position-name']")
	TestHelper.setInputValue(businessRolePositionName, committeeMemberRoleName)
	
	TestObject updateBusinessRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='update_network_role']")
	TestHelper.clickButton(updateBusinessRoleAction);
	
	String committeeOrganizationTarget = organizationName + '-' + committeeMemberRoleName + ', ' +  committeeName + '-role'	
	TestObject selectedOrganization = TestHelper.getObjectById("//div[@id='"+committeeOrganizationTarget+"']")
	WebUI.delay(2);
	TestHelper.clickButton(selectedOrganization);
	WebUI.delay(2);
}

 
// Founder Sections
def checkFounderCommonHeaderElementExist() {
	boolean successEmptyFields = true;
	
	TestObject organizationMenuAction_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='organization_menu_action_id']")
	TestObject manage_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Manage_Organization_entity_action']")
	TestObject invite_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Invite_Organization_entity_action']")
	
	TestObject followers_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='follower_count_id_tag_label']")
	TestObject leadership_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='leadership_id_tag_label']")

	if(!TestHelper.isElementPresent(followers_label_object)) {
		TestHelper.thrownException("Followers Label not present");
	} else if(!TestHelper.isElementPresent(leadership_label_object)) {
		TestHelper.thrownException("Leadership Label not present");
	} else if(!TestHelper.isElementPresent(organizationMenuAction_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(manage_entity_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(invite_entity_object)) {
		successEmptyFields = false;
	}
	
	if(successEmptyFields) {
		println("Organization common header info exist");
	} else {
			TestHelper.thrownException("Error: Organization commone header info")
	}
}

def checkFounderOrganizationSectionsEmptyState() {

	boolean successEmptyFields = true;
	
	TestObject organization_about_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='organization_about']")
	
	TestObject organization_affiliation_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='affiliation-section']")
	TestObject organization_affiliation_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-affiliation-empty-state']")

	TestObject organization_member_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='organization_teams_members']")
	TestObject organization_leadership_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='organization_leadership_members']")

	TestObject organization_updates_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='organization_updates']")
	TestObject organization_updates_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-organization-updates-empty-state']")

	TestObject organization_chapters_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='organization_chapters']")
	TestObject organization_chapters_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-organization-chapters-empty-state']")

	TestObject organization_committee_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='organization_committee']")
	TestObject organization_committee_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-organization-committee-empty-state']")
	
	TestObject organization_media_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='organization_media']")
	TestObject organization_media_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-organization-media-empty-state']")
	
	
	// Check Object Exist
	if(!TestHelper.isElementPresent(organization_about_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(organization_affiliation_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(organization_affiliation_empty_state_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(organization_member_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(organization_leadership_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(organization_updates_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(organization_updates_empty_state_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(organization_chapters_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(organization_chapters_empty_state_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(organization_committee_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(organization_committee_empty_state_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(organization_media_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(organization_media_empty_state_object)) {
		successEmptyFields = false;
	}
	
	if(successEmptyFields) {
			println("Organization section empty state exist");
	} else {
			TestHelper.thrownException("Error: Organization section empty state exist")
	}
	
}

def checkFounderOrganizationSectionsTitleAndSubTitle() {
	TestObject about_title_object = TestHelper.getObjectById("//div[@id='organization_about_title_id']")
	TestObject about_subTitle_object = TestHelper.getObjectById("//div[@id='organization_about_subTitle_id']")

	if(TestHelper.isElementPresent(about_title_object) && TestHelper.isElementPresent(about_subTitle_object)) {
		String aboutTitle = WebUI.getText(about_title_object);
		String aboutSubTitle = WebUI.getText(about_subTitle_object);
	
		if(!aboutTitle.equals("About") && !aboutSubTitle.equals("Info, sector and contact details")) {
			TestHelper.thrownException("Invalid about title and subtitle")
		}
	}

	TestObject affiliation_title_object = TestHelper.getObjectById("//div[@id='organization_affiliation_title_id']")
	TestObject affiliation_subTitle_object = TestHelper.getObjectById("//div[@id='organization_affiliation_subTitle_id']")

	if(TestHelper.isElementPresent(affiliation_title_object) && TestHelper.isElementPresent(affiliation_subTitle_object)) {
		String affiliationTitle = WebUI.getText(affiliation_title_object);
		String affiliationSubTitle = WebUI.getText(affiliation_subTitle_object);

		 if(!affiliationTitle.equals("Member Companies") && !affiliationSubTitle.equals("Affiliated to this org")) {
			TestHelper.thrownException("Invalid affiliation title and subtitle")
		}
	}

	TestObject members_title_object = TestHelper.getObjectById("//div[@id='organization_members_title_id']")
	TestObject members_subTitle_object = TestHelper.getObjectById("//div[@id='organization_members_subTitle_id']")

	if(TestHelper.isElementPresent(members_title_object) && TestHelper.isElementPresent(members_subTitle_object)) {
		String membersTitle = WebUI.getText(members_title_object);
		String membersSubTitle = WebUI.getText(members_subTitle_object);

		if(!membersTitle.equals("Members") && !membersSubTitle.equals("Members of this organization")) {
			TestHelper.thrownException("Invalid members title and subtitle")
		}
	}

	
	TestObject leaderships_title_object = TestHelper.getObjectById("//div[@id='organization_leadership_title_id']")
	TestObject leaderships_subTitle_object = TestHelper.getObjectById("//div[@id='organization_leadership_subTitle_id']")

	if(TestHelper.isElementPresent(leaderships_title_object) && TestHelper.isElementPresent(leaderships_subTitle_object)) {
		String leadershipsTitle = WebUI.getText(leaderships_title_object);
		String leadershipsSubTitle = WebUI.getText(leaderships_subTitle_object);

		if(!leadershipsTitle.equals("Leadership") && !leadershipsSubTitle.equals("Leaders & management")) {
			TestHelper.thrownException("Invalid leaderships title and subtitle")
		}
	}

	
	TestObject chapters_title_object = TestHelper.getObjectById("//div[@id='organization_chapters_title_id']")
	TestObject chapters_subTitle_object = TestHelper.getObjectById("//div[@id='organization_chapters_subTitle_id']")

	if(TestHelper.isElementPresent(chapters_title_object) && TestHelper.isElementPresent(chapters_subTitle_object)) {
		String chaptersTitle = WebUI.getText(chapters_title_object);
		String chaptersSubTitle = WebUI.getText(chapters_subTitle_object);

		if(!chaptersTitle.equals("Chapters") && !chaptersSubTitle.equals("Chapters subtitle")) {
			TestHelper.thrownException("Invalid chapters title and subtitle")
		}
	}

	TestObject committees_title_object = TestHelper.getObjectById("//div[@id='organization_committees_title_id']")
	TestObject committess_subTitle_object = TestHelper.getObjectById("//div[@id='organization_committees_subTitle_id']")

	if(TestHelper.isElementPresent(committees_title_object) && TestHelper.isElementPresent(committess_subTitle_object)) {
		String committeesTitle = WebUI.getText(committees_title_object);
		String committeesSubTitle = WebUI.getText(committess_subTitle_object);

		if(!committeesTitle.equals("Committees") && !committeesSubTitle.equals("Committees & members")) {
			TestHelper.thrownException("Invalid committees title and subtitle")
		}
	}

	TestObject updates_title_object = TestHelper.getObjectById("//div[@id='organization_updates_title_id']")
	TestObject updates_subTitle_object = TestHelper.getObjectById("//div[@id='organization_updates_subTitle_id']")

	if(TestHelper.isElementPresent(updates_title_object) && TestHelper.isElementPresent(updates_subTitle_object)) {
		String updatesTitle = WebUI.getText(updates_title_object);
		String updatesSubTitle = WebUI.getText(updates_subTitle_object);

		if(!updatesTitle.equals("Updates") && !updatesSubTitle.equals("Posts & discussions")) {
			TestHelper.thrownException("Invalid updates title and subtitle")
		}

	}

	TestObject media_title_object = TestHelper.getObjectById("//div[@id='organization_media_title_id']")
	TestObject media_subTitle_object = TestHelper.getObjectById("//div[@id='organization_media_subTitle_id']")
	
	if(TestHelper.isElementPresent(media_title_object) && TestHelper.isElementPresent(media_subTitle_object)) {
		String mediaTitle = WebUI.getText(media_title_object);
		String mediaSubTitle = WebUI.getText(media_subTitle_object);

		if(!mediaTitle.equals("Media Gallery") && !mediaSubTitle.equals("Photos & videos")) {
			TestHelper.thrownException("Invalid media title and subtitle")
		}
	}
}

// Member Sections
def checkNonAdminEmptyState() {
	String organizationMediaEmptyStateErrorMessage = "Error: Organization media empty state exist";
	
	TestObject organizationMenuAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='organization_menu_action_id']")
	TestObject message_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Message_Organization_entity_action']")
	TestObject following_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Following_Organization_entity_action']")
	
	
	TestObject followers_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='follower_count_id_tag_label']")
	TestObject leadership_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='leadership_id_tag_label']")

	if(!TestHelper.isElementPresent(followers_label_object)) {
		TestHelper.thrownException("Followers Label not present");
	} else if(!TestHelper.isElementPresent(leadership_label_object)) {
		TestHelper.thrownException("Leadership Label not present");
	} else if(!TestHelper.isElementPresent(following_entity_object)) {
		TestHelper.thrownException("Following Action not present");
	} else if(!TestHelper.isElementPresent(message_entity_object)) {
		TestHelper.thrownException("Message Action not present");
	}
	
	TestHelper.checkObjectExit("//div[@id='organization_media']", organizationMediaEmptyStateErrorMessage);
	TestHelper.checkObjectText("//div[@id='no-organization-media-empty-state-title']", "Show sights, achievements or projects with images and videos" , organizationMediaEmptyStateErrorMessage);
	TestHelper.checkObjectText("//div[@id='no-organization-media-empty-state']", "Add Photos or Videos" , organizationMediaEmptyStateErrorMessage);
	TestHelper.checkObjectImageSrc("//div[@id='no-organization-media-empty-state-image']", "assets/icons/emptyState/Media_Gallery.svg", organizationMediaEmptyStateErrorMessage)


	if(TestHelper.isElementPresent(organizationMenuAction)) {
		TestHelper.clickButton(organizationMenuAction);

		TestObject reportOrganizationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Report A Problem_user_action']")
		TestObject shareOrganizationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Share_user_action']")
		TestObject claimOrganizationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Claim this organization_user_action']")
		
		if(!TestHelper.isElementPresent(reportOrganizationAction) || !TestHelper.isElementPresent(shareOrganizationAction) || !TestHelper.isElementPresent(claimOrganizationAction)) {
			TestHelper.thrownException("Organization Action not present");
		} else {
			print("Check Non Admin Complete")
		}
	} else {
		TestHelper.thrownException("Menu Action not present");
	}
}

