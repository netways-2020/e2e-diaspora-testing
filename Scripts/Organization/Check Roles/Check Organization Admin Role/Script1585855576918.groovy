import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

String organizationName = 'Netways_Organization' + new Date().getTime();

String founderRoleName = 'Founder'
String memberRoleName = 'Member'
 
// Founder User
long date = new Date().getTime();
String registeredEmail = 'netways_user' + date + '@gmail.com'
String password = 'cvW8qx4B2o3F4VwP/kNsqA==';
String firstName = 'FirstName_' + date;
String lastName = 'LastName_' + date;
String memberName = firstName + ' ' + lastName;
String city = 'Hermel';

// Member User
String founderRegisteredEmail = 'netways_founder' + date + '@gmail.com'
String founderFirstName = 'FirstName_' + date;
String founderLastName = 'LastName_' + date;


TestHelper.registerNewUser(founderRegisteredEmail, password, founderFirstName, founderLastName, city, true)
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_tab_id']"))
TestHelper.goToManageEntity()
TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'Network Roles_manage_entity_action\']'))

// Founder Role
checkNetworkRole(organizationName, founderRoleName);

// Create Profile and join organization
createProfileAndJoinOrganization(organizationName, memberRoleName, registeredEmail, password, firstName, lastName, city)
assignNewUserAsAdmin(founderRegisteredEmail, password, memberName);

loginWithCreatedUserAndCheckAdmin(organizationName, registeredEmail, password, memberRoleName)
checkFounderSections();

WebUI.delay(2);
WebUI.closeBrowser();


// Create Organization
def checkNetworkRole(String organizationName, String founderRoleName) {
	TestObject emptyStateObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-network-role-empty-state\']')
	boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)
	
	if (emptyStateObjectPresent == true) {
		println('hasn\'t networks roles')
	
		TestObject emptyStateAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-network-role-empty-state-button\']')
		WebUI.click(emptyStateAction)
		createNewOrganizationAsFounder(organizationName, founderRoleName)
	
	} else {
		println('has network roles')
	
		TestObject addOrganizationRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'add-new-network-role-action\']')
		WebUI.click(addOrganizationRoleAction)
		createNewOrganizationAsFounder(organizationName, founderRoleName)
	}
}
 
def createNewOrganizationAsFounder(String organizationName, String founderRoleName) {
	UserRoleHelper.createOrganization(organizationName, founderRoleName)
	WebUI.delay(2);
	String organizationTarget = organizationName + '-' + founderRoleName + '-role'
	TestObject selectedOrganization = TestHelper.getObjectById("//div[@id='"+organizationTarget+"']")
	TestHelper.clickButton(selectedOrganization);
	
	organizationUrl = WebUI.getUrl();
	
	WebUI.delay(2);
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='organization_back_button']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_network_role_back_action']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-User-back-button']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_manage_accounts']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Log Out']"));
}

def createProfileAndJoinOrganization(String organizationName, String employeeRoleName, String registeredEmail, String password, String firstName, String lastName, String city) {
	TestHelper.registerNewUser(registeredEmail, password, firstName, lastName, city, false)
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_tab_id']"))
	TestHelper.goToManageEntity()
	TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'Network Roles_manage_entity_action\']'))
	TestObject emptyStateObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-network-role-empty-state\']')
	boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)
	
	if (emptyStateObjectPresent == true) {
		println('hasn\'t networks roles')
	
		TestObject emptyStateAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-network-role-empty-state-button\']')
		WebUI.click(emptyStateAction)
		joinOrganization(organizationName, employeeRoleName)
	
	} else {
		println('has network roles')
	
		TestObject addOrganizationRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'add-new-network-role-action\']')
		WebUI.click(addOrganizationRoleAction)
		joinOrganization(organizationName, employeeRoleName)
	}
}

def joinOrganization(String organizationName, String employeeRoleName) {
	TestObject searchForOrganizationInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='searchForOrganizationInput']")
	TestHelper.setInputValue(searchForOrganizationInput, organizationName)

	WebUI.delay(2);
	String organizationId = organizationName + '_search_for_organization';
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='"+organizationId+"']"))
	
	UserRoleHelper.joinOrganization(employeeRoleName);
	String organizationTarget = organizationName + '-' + employeeRoleName + '-role'
	TestObject selectedOrganization = TestHelper.getObjectById("//div[@id='"+organizationTarget+"']")
	TestHelper.clickButton(selectedOrganization);
	checkNonAdminEmptyState();
	
	WebUI.delay(2);
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='organization_back_button']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='organization_back_button']"));

	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_network_role_back_action']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-User-back-button']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_manage_accounts']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Log Out']"));
}

def assignNewUserAsAdmin(String founderEmail, String password, String memberName) {
	TestHelper.loginWithSpecificEmail(founderEmail, password);
	WebUI.navigateToUrl(organizationUrl);
	TestHelper.goToOrganizationManageEntity();
	TestObject manageTeamMemberAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Organization Members_manage_entity_action']")
	TestHelper.clickButton(manageTeamMemberAction);
	
	TestObject pendingMemberSearchInput = TestHelper.getObjectById("//input[@id='organization_pending_member']")
	TestHelper.setInputValue(pendingMemberSearchInput, memberName)
	WebUI.delay(2);
	
	String pendingEmployeeId = memberName + '_pending_employee_name';
	String pendingEmployeeActionId = memberName + '_pending_employee_action';
	
	
	TestObject newMember = TestHelper.getObjectById("//div[@id='"+pendingEmployeeId+"']")
	
	boolean newMemberExist = TestHelper.isElementPresent(newMember);
	if(newMemberExist) {
		TestObject pendingMemberAction = TestHelper.getObjectById("//div[@id='"+pendingEmployeeActionId+"']")
		TestHelper.clickButton(pendingMemberAction);
		
		TestObject pendingMemberApproveRequestAction = TestHelper.getObjectById("//a[@id='Approve Request_user_action']")
		TestHelper.clickButton(pendingMemberApproveRequestAction);
		
		WebUI.delay(2);
		
		
		TestObject manageOrganizationMembersBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-organization-member-back-button']")
		TestHelper.clickButton(manageOrganizationMembersBackButton);

		assignAdminToOrganization(memberName);
		
	} else {
		TestHelper.thrownException("No employee with this name exist");
	}
}

def assignAdminToOrganization(String memberName) {
	TestObject manageAdminMemberAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Page Administrators_manage_entity_action']")
	TestHelper.clickButton(manageAdminMemberAction);
	
	// Check empty state add admin
	TestObject manage_admin_add_admin = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-admin-organization-add-new-admin-empty-state']")
	boolean checkManage_admin_admin = TestHelper.isElementPresent(manage_admin_add_admin);
	
	if(checkManage_admin_admin) {
		TestHelper.clickButton(manage_admin_add_admin)
		assignMemberAsAdmin(memberName);
	} else {
		TestObject manage_admin_add_admin_from_header = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-admin-organization-add-new-admin']")
		TestHelper.clickButton(manage_admin_add_admin_from_header)
		assignMemberAsAdmin(memberName);
	}
}

def assignMemberAsAdmin(String memberName) {
	 
// Assigne member as admin
 	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='assign-admin-search-input']"), memberName);
	WebUI.delay(2);
	
	String memberId = memberName + '_assign_admin_role_action';
	TestObject memberAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + memberId + "']")
	TestHelper.clickButton(memberAction);

	// View Profile
	TestObject viewCurrentMemberAction = TestHelper.getObjectById("//a[@id='View Profile_user_action']")
	TestHelper.clickButton(viewCurrentMemberAction);
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_backIcon_id']"))

	// Assign Admin
	TestHelper.clickButton(memberAction);
	TestObject assignAdminMemberAction = TestHelper.getObjectById("//a[@id='Assign As Administrator_user_action']")
	TestHelper.clickButton(assignAdminMemberAction);
	WebUI.delay(2);
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='add-admin-organization-back-button']"))
}

def checkAdmin(String memberName) {
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='manage-organization-admin-search-input']"), memberName);
	WebUI.delay(2);
	
	String memberId = memberName + '_admin_role';
	TestObject memberObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + memberId + "']")
	boolean checkAdminExist = TestHelper.isElementPresent(memberObject);
	
	if(checkAdminExist) {
		println("Memeber assign as admin succesfully");
	} else {
		TestHelper.thrownException("Error during assing member as admin");
	}
}

def loginWithCreatedUserAndCheckAdmin(String organizationName, String email, String password, String memberRoleName) {
	
	WebUI.closeBrowser();
	
	WebUI.delay(3);
	TestHelper.loginWithSpecificUserNameAndPassword(email, password, true)
	
	TestHelper.goToManageEntity()
	TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'Network Roles_manage_entity_action\']'))

	String organizationTarget = organizationName + '-' + memberRoleName + '-role'
	TestObject selectedOrganization = TestHelper.getObjectById("//div[@id='"+organizationTarget+"']")
	TestHelper.clickButton(selectedOrganization);
}

// Founder Sections

// Check Founder Section
def checkFounderSections() {
	checkFounderCommonHeaderElementExist();
	checkFounderOrganizationSectionsEmptyState();
	checkFounderOrganizationSectionsTitleAndSubTitle();
}

def checkFounderCommonHeaderElementExist() {
	boolean successEmptyFields = true;
	
	TestObject organizationMenuAction_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='organization_menu_action_id']")
	TestObject manage_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Manage_Organization_entity_action']")
	TestObject invite_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Invite_Organization_entity_action']")
	
	TestObject followers_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='follower_count_id_tag_label']")
	TestObject leadership_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='leadership_id_tag_label']")

	if(!TestHelper.isElementPresent(followers_label_object)) {
		TestHelper.thrownException("Followers Label not present");
	} else if(!TestHelper.isElementPresent(leadership_label_object)) {
		TestHelper.thrownException("Leadership Label not present");
	} else if(!TestHelper.isElementPresent(organizationMenuAction_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(manage_entity_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(invite_entity_object)) {
		successEmptyFields = false;
	}
	
	if(successEmptyFields) {
		println("Organization common header info exist");
	} else {
			TestHelper.thrownException("Error: Organization commone header info")
	}
}

def checkFounderOrganizationSectionsEmptyState() {

	boolean successEmptyFields = true;
	
	TestObject organization_about_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='organization_about']")
	
	TestObject organization_affiliation_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='affiliation-section']")
	TestObject organization_affiliation_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-affiliation-empty-state']")

	TestObject organization_member_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='organization_teams_members']")
	TestObject organization_leadership_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='organization_leadership_members']")

	TestObject organization_updates_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='organization_updates']")
	TestObject organization_updates_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-organization-updates-empty-state']")

	TestObject organization_chapters_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='organization_chapters']")
	TestObject organization_chapters_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-organization-chapters-empty-state']")

	TestObject organization_committee_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='organization_committee']")
	TestObject organization_committee_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-organization-committee-empty-state']")
	
	TestObject organization_media_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='organization_media']")
	TestObject organization_media_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-organization-media-empty-state']")
	
	
	// Check Object Exist
	if(!TestHelper.isElementPresent(organization_about_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(organization_affiliation_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(organization_affiliation_empty_state_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(organization_member_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(organization_leadership_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(organization_updates_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(organization_updates_empty_state_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(organization_chapters_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(organization_chapters_empty_state_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(organization_committee_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(organization_committee_empty_state_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(organization_media_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(organization_media_empty_state_object)) {
		successEmptyFields = false;
	}
	
	if(successEmptyFields) {
			println("Organization section empty state exist");
	} else {
			TestHelper.thrownException("Error: Organization section empty state exist")
	}
	
}

def checkFounderOrganizationSectionsTitleAndSubTitle() {
	TestObject about_title_object = TestHelper.getObjectById("//div[@id='organization_about_title_id']")
	TestObject about_subTitle_object = TestHelper.getObjectById("//div[@id='organization_about_subTitle_id']")

	if(TestHelper.isElementPresent(about_title_object) && TestHelper.isElementPresent(about_subTitle_object)) {
		String aboutTitle = WebUI.getText(about_title_object);
		String aboutSubTitle = WebUI.getText(about_subTitle_object);
	
		if(!aboutTitle.equals("About") && !aboutSubTitle.equals("Info, sector and contact details")) {
			TestHelper.thrownException("Invalid about title and subtitle")
		}
	}

	TestObject affiliation_title_object = TestHelper.getObjectById("//div[@id='organization_affiliation_title_id']")
	TestObject affiliation_subTitle_object = TestHelper.getObjectById("//div[@id='organization_affiliation_subTitle_id']")

	if(TestHelper.isElementPresent(affiliation_title_object) && TestHelper.isElementPresent(affiliation_subTitle_object)) {
		String affiliationTitle = WebUI.getText(affiliation_title_object);
		String affiliationSubTitle = WebUI.getText(affiliation_subTitle_object);

		 if(!affiliationTitle.equals("Member Companies") && !affiliationSubTitle.equals("Affiliated to this org")) {
			TestHelper.thrownException("Invalid affiliation title and subtitle")
		}
	}

	TestObject members_title_object = TestHelper.getObjectById("//div[@id='organization_members_title_id']")
	TestObject members_subTitle_object = TestHelper.getObjectById("//div[@id='organization_members_subTitle_id']")

	if(TestHelper.isElementPresent(members_title_object) && TestHelper.isElementPresent(members_subTitle_object)) {
		String membersTitle = WebUI.getText(members_title_object);
		String membersSubTitle = WebUI.getText(members_subTitle_object);

		if(!membersTitle.equals("Members") && !membersSubTitle.equals("Members of this organization")) {
			TestHelper.thrownException("Invalid members title and subtitle")
		}
	}

	
	TestObject leaderships_title_object = TestHelper.getObjectById("//div[@id='organization_leadership_title_id']")
	TestObject leaderships_subTitle_object = TestHelper.getObjectById("//div[@id='organization_leadership_subTitle_id']")

	if(TestHelper.isElementPresent(leaderships_title_object) && TestHelper.isElementPresent(leaderships_subTitle_object)) {
		String leadershipsTitle = WebUI.getText(leaderships_title_object);
		String leadershipsSubTitle = WebUI.getText(leaderships_subTitle_object);

		if(!leadershipsTitle.equals("Leadership") && !leadershipsSubTitle.equals("Leaders & management")) {
			TestHelper.thrownException("Invalid leaderships title and subtitle")
		}
	}

	
	TestObject chapters_title_object = TestHelper.getObjectById("//div[@id='organization_chapters_title_id']")
	TestObject chapters_subTitle_object = TestHelper.getObjectById("//div[@id='organization_chapters_subTitle_id']")

	if(TestHelper.isElementPresent(chapters_title_object) && TestHelper.isElementPresent(chapters_subTitle_object)) {
		String chaptersTitle = WebUI.getText(chapters_title_object);
		String chaptersSubTitle = WebUI.getText(chapters_subTitle_object);

		if(!chaptersTitle.equals("Chapters") && !chaptersSubTitle.equals("Chapters subtitle")) {
			TestHelper.thrownException("Invalid chapters title and subtitle")
		}
	}

	TestObject committees_title_object = TestHelper.getObjectById("//div[@id='organization_committees_title_id']")
	TestObject committess_subTitle_object = TestHelper.getObjectById("//div[@id='organization_committees_subTitle_id']")

	if(TestHelper.isElementPresent(committees_title_object) && TestHelper.isElementPresent(committess_subTitle_object)) {
		String committeesTitle = WebUI.getText(committees_title_object);
		String committeesSubTitle = WebUI.getText(committess_subTitle_object);

		if(!committeesTitle.equals("Committees") && !committeesSubTitle.equals("Committees & members")) {
			TestHelper.thrownException("Invalid committees title and subtitle")
		}
	}

	TestObject updates_title_object = TestHelper.getObjectById("//div[@id='organization_updates_title_id']")
	TestObject updates_subTitle_object = TestHelper.getObjectById("//div[@id='organization_updates_subTitle_id']")

	if(TestHelper.isElementPresent(updates_title_object) && TestHelper.isElementPresent(updates_subTitle_object)) {
		String updatesTitle = WebUI.getText(updates_title_object);
		String updatesSubTitle = WebUI.getText(updates_subTitle_object);

		if(!updatesTitle.equals("Updates") && !updatesSubTitle.equals("Posts & discussions")) {
			TestHelper.thrownException("Invalid updates title and subtitle")
		}

	}

	TestObject media_title_object = TestHelper.getObjectById("//div[@id='organization_media_title_id']")
	TestObject media_subTitle_object = TestHelper.getObjectById("//div[@id='organization_media_subTitle_id']")
	
	if(TestHelper.isElementPresent(media_title_object) && TestHelper.isElementPresent(media_subTitle_object)) {
		String mediaTitle = WebUI.getText(media_title_object);
		String mediaSubTitle = WebUI.getText(media_subTitle_object);

		if(!mediaTitle.equals("Media Gallery") && !mediaSubTitle.equals("Photos & videos")) {
			TestHelper.thrownException("Invalid media title and subtitle")
		}
	}
}

// Member Sections
def checkNonAdminEmptyState() {
	String organizationMediaEmptyStateErrorMessage = "Error: Organization media empty state exist";
	
	TestObject organizationMenuAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='organization_menu_action_id']")
	TestObject message_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Message_Organization_entity_action']")
	TestObject following_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Following_Organization_entity_action']")
	
	
	TestObject followers_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='follower_count_id_tag_label']")
	TestObject leadership_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='leadership_id_tag_label']")

	if(!TestHelper.isElementPresent(followers_label_object)) {
		TestHelper.thrownException("Followers Label not present");
	} else if(!TestHelper.isElementPresent(leadership_label_object)) {
		TestHelper.thrownException("Leadership Label not present");
	} else if(!TestHelper.isElementPresent(following_entity_object)) {
		TestHelper.thrownException("Following Action not present");
	} else if(!TestHelper.isElementPresent(message_entity_object)) {
		TestHelper.thrownException("Message Action not present");
	}
	
	TestHelper.checkObjectExit("//div[@id='organization_media']", organizationMediaEmptyStateErrorMessage);
	TestHelper.checkObjectText("//div[@id='no-organization-media-empty-state-title']", "Show sights, achievements or projects with images and videos" , organizationMediaEmptyStateErrorMessage);
	TestHelper.checkObjectText("//div[@id='no-organization-media-empty-state']", "Add Photos or Videos" , organizationMediaEmptyStateErrorMessage);
	TestHelper.checkObjectImageSrc("//div[@id='no-organization-media-empty-state-image']", "assets/icons/emptyState/Media_Gallery.svg", organizationMediaEmptyStateErrorMessage)


	if(TestHelper.isElementPresent(organizationMenuAction)) {
		TestHelper.clickButton(organizationMenuAction);

		TestObject reportOrganizationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Report A Problem_user_action']")
		TestObject shareOrganizationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Share_user_action']")
		TestObject claimOrganizationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Claim this organization_user_action']")
		
		if(!TestHelper.isElementPresent(reportOrganizationAction) || !TestHelper.isElementPresent(shareOrganizationAction) || !TestHelper.isElementPresent(claimOrganizationAction)) {
			TestHelper.thrownException("Organization Action not present");
		} else {
			print("Check Non Admin Complete")
		}
	} else {
		TestHelper.thrownException("Menu Action not present");
	}
}

