import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable


long date = new Date().getTime();
String registeredEmail = 'netways_user' + date + '@gmail.com'
String password = 'cvW8qx4B2o3F4VwP/kNsqA=='

String firstName = 'FirstName_' + date;
String lastName = 'LastName_' + date;
String city = 'Hermel';
 
 
WebUI.openBrowser('')

WebUI.navigateToUrl(TestHelper.getHomeUrl())

TestHelper.clickButton(TestHelper.getObjectById("//button[@id='registerButton']"))
WebUI.delay(1)

// Email
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='login-with-email-button']"))
	TestObject emailInput = TestHelper.getObjectById("//input[@id='email-input']")
	TestHelper.setInputValue(emailInput, registeredEmail)
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='login-email-submit-button']"))

// Password
	TestObject passwordInput = TestHelper.getObjectById("//input[@id='passwordInputId']")
	TestHelper.setPasswordValue(passwordInput, password)
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='submit-button']"))

// Profile general details
	TestObject firstNameInput = TestHelper.getObjectById("//input[@id='user-input-first-name']")
	TestHelper.setInputValue(firstNameInput, firstName)
	
	TestObject lastNameInput = TestHelper.getObjectById("//input[@id='user-input-last-name']")
	TestHelper.setInputValue(lastNameInput, lastName)

	TestObject saveUserGeneralDetails = TestHelper.getObjectById("//button[@id='user_save_general_details']")
	TestHelper.clickButton(saveUserGeneralDetails)
	
	
// Profile location details
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='user_city_of_origin']"))
	TestObject cityOfOriginSearchInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='city-search-input']")
	TestHelper.setInputValue(cityOfOriginSearchInput, city)
	TestObject cityOfOriginObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+city+"']")
	WebUI.delay(1)
	WebUI.click(cityOfOriginObject)

	
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='user_city_of_residence']"))
	TestHelper.setInputValue(cityOfOriginSearchInput, city)
	TestObject cityOfResidenceObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+city+"']")
	WebUI.delay(1)
	WebUI.click(cityOfResidenceObject)
	
	TestObject saveUserDetails = TestHelper.getObjectById("//button[@id='save_user_info']")
	TestHelper.clickButton(saveUserDetails)
	WebUI.delay(1)
	
	TestObject manageProfilSection = TestHelper.getObjectById("//div[@id='manageProfileId']")
	WebUI.waitForElementVisible(manageProfilSection, 10)
	
	

TestHelper.clickButton(TestHelper.getObjectById("//div[@id='market_tab_id']"))

