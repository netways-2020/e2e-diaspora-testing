import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import static org.junit.Assert.*

import org.junit.Test
import org.openqa.selenium.WebElement as WebElement

//get all the name and last name form data files
String[] firstNameLastNameArray = findTestData('Data Files/firstNameLastname').getAllData()

//generate random number from the lengh of the array
int randomFirstNameLastNameNumber = new Random().nextInt(firstNameLastNameArray.length)

//check if the gerated array is  0
int randomFirstNameLastName = randomFirstNameLastNameNumber == 0 ? 1 : randomFirstNameLastNameNumber

//get the random first name from the excel table
String firstname = findTestData('Data Files/firstNameLastname').getValue(1, randomFirstNameLastName)

//get the random last name  from the excel table
String lastname = findTestData('Data Files/firstNameLastname').getValue(2, randomFirstNameLastName)

//cities
//get the list of first name and last name from the excel as an array
String[] citiesArray = findTestData('Data Files/Cities').getAllData()

//generate random number from the lengh of the array for residence city
int randomResidenceCity = new Random().nextInt(200)

//generate random number from the lengh of the array for origin city
int randomOriginCity = new Random().nextInt(citiesArray.length)

//get the random first name from the exel table
//String originCity = findTestData('Data Files/Cities').getValue(1, randomOriginCity)
String originCity = findTestData('Data Files/Cities').getValue(1, randomOriginCity)

//get the random firstname from the exel table
String residenceCity = findTestData('Data Files/Cities').getValue(1,randomResidenceCity)

//generate new email
String email = ('blaybel@' + new Date().getTime()) + '.com'

//generate password
String password = 'Test@123'

//create company and organization values
String organizationName = 'Netways_Organization' + new Date().getTime();
String companyName = 'Netways' + new Date().getTime()
String roleName = "Founder"

//Cities
TestObject cityOrigineDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + originCity) + '\']')
TestObject cityResidenceDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + residenceCity) + '\']')
TestObject cityDistrictDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//span[@id=\'district_city\']')
TestObject profile_tab = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//ion-tab-button[@id='tab-button-profile']")

//home page
TestObject homeTitleProfilePage = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'home_title_' + firstname + ' ' + lastname) + '\']')
TestObject originCityInHomeDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//span[@id=\'country_lb_1_tag_label\']')
TestObject residenceCityInHomeDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//span[@id=\'country_lb_1_tag_label\']')

//manageAccount	
TestObject requestToBeFeatured = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'manage-account_Request To Be Featured\']')
TestObject ReportProblem = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'manage-account_Report A Problem\']')
TestObject AdvertiseWithUs = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'manage-account_Advertise With Us\']')

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

registerNewUser( email, password, firstname, lastname, cityOrigineDiv, cityResidenceDiv, originCity, residenceCity)
createCompany(companyName,roleName);
createOrganization(organizationName, roleName);
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_manage_accounts']"))
checkManageAccountEntity(originCity,companyName, organizationName)
checkManageAccountLink()
sendReportFromManageAccount(requestToBeFeatured)
sendReportFromManageAccount(ReportProblem)
WebUI.delay(7)
sendReportFromManageAccount(AdvertiseWithUs)
WebUI.closeBrowser()

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

def registerNewUser( email, password, firstname, lastname, cityOrigineDiv, cityResidenceDiv, originCity, residenceCity){
	
   WebUI.openBrowser('')
   
   WebUI.navigateToUrl('http://localhost:8100/auth/landing')
   
   WebUI.click(TestHelper.getObjectById("//button[@id='registerButton']"))
   
   TestHelper.clickButton(TestHelper.getObjectById("//button[@id='login-with-email-button']"))
   
   WebUI.setText(TestHelper.getObjectById('//input[@id=\'email-input\']'), email)
   
   TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'login-email-submit-button\']'))
   
   WebUI.setText(TestHelper.getObjectById('//input[@id=\'passwordInputId\']'), password)
   
   TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'submit-button\']'))
   
   WebUI.setText(TestHelper.getObjectById('//input[@id=\'user-input-first-name\']'),  firstname)
   
   WebUI.setText(TestHelper.getObjectById('//input[@id=\'user-input-last-name\']'), lastname)
   
   TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'user_save_general_details\']'))
   
   TestHelper.clickButton(TestHelper.getObjectById("//button[@id='user_city_of_origin']"))
   
   WebUI.setText(TestHelper.getObjectById("//input[@id='city-search-input']"), originCity)
   
   TestHelper.clickButton(cityOrigineDiv)
   
   TestHelper.clickButton(TestHelper.getObjectById("//button[@id='user_city_of_residence']"))
   
   WebUI.setText(TestHelper.getObjectById("//input[@id='city-search-input']"), residenceCity)
   
   TestHelper.clickButton(cityResidenceDiv)
   
   TestHelper.clickButton(TestHelper.getObjectById("//button[@id='save_user_info']"))
   
   TestObject manageProfilSection = TestHelper.getObjectById("//div[@id='manageProfileId']")
	   
	   WebUI.waitForElementVisible(manageProfilSection, 5)
   }
	
def createCompany(String companyName, String roleName) {
   TestHelper.clickButton(TestHelper.getObjectById("//ion-tab-button[@id='tab-button-profile']"))
   TestHelper.goToManageEntity();
   TestObject manageBuinessRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Business Roles_manage_entity_action\']')
   TestHelper.clickButton(manageBuinessRoleAction)
   
   TestObject emptyStateObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-business-role-empty-state\']')
	   boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)
   
	   if (emptyStateObjectPresent == true) {
		   println('hasn\'t business roles')
	   TestObject emptyStateAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-business-role-empty-state-button\']')
	   WebUI.click(emptyStateAction)
	   UserRoleHelper.createCompany(companyName, roleName)
	   
	   TestObject manageBusinessBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_business_role_back_action']")
	   WebUI.waitForElementPresent(manageBusinessBackButton, 5)
	   TestHelper.clickButton(manageBusinessBackButton);
	   
   } else {
	   println('has business roles')
	   TestObject addBusinessRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'add-new-business-role-action\']')
	   WebUI.click(addBusinessRoleAction)
	   UserRoleHelper.createCompany(companyName, roleName)
	   
	   TestObject manageBusinessBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_business_role_back_action']")
		   WebUI.waitForElementPresent(manageBusinessBackButton, 5)
		   
		   TestHelper.clickButton(manageBusinessBackButton);
	   }
   }
	
def createOrganization(String organizationName, String roleName) {
	   TestObject manageNetworkRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='Network Roles_manage_entity_action']")
   TestHelper.clickButton(manageNetworkRoleAction)
   
   TestObject emptyStateObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-network-role-empty-state']")
	   boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)
   
	   if (emptyStateObjectPresent == true) {
		   println("hasn't network roles")

	   TestObject emptyStateAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-network-role-empty-state-button']")
		   TestHelper.clickButton(emptyStateAction)
   
		   // Create organization, add get created organization url
	   UserRoleHelper.createOrganization(organizationName, roleName);
		
	   TestObject manageNetworkBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_network_role_back_action']")
	   WebUI.waitForElementPresent(manageNetworkBackButton, 5)
	   TestHelper.clickButton(manageNetworkBackButton);
	   TestHelper.click()
   
   } else {
	   println("has network roles")
	   TestObject addNetworkRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='add-new-network-role-action']")
	   TestHelper.clickButton(addNetworkRoleAction)
	   
	   // Create organization, add get created organization url
	   UserRoleHelper.createOrganization(organizationName, roleName);
	   TestObject manageNetworkBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_network_role_back_action']")
		   WebUI.waitForElementPresent(manageNetworkBackButton, 5)
		   TestHelper.clickButton(manageNetworkBackButton);
		   TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-User-back-button']"))
	   }
   }

def checkManageAccountEntity(originCity,companyName, organizationName) {
	
	TestObject manageCompanyDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'manage-account-' + companyName) + '\']')
	// check manage account company
	TestHelper.clickButton(manageCompanyDiv)
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='Company Information_manage_entity_action']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-Company-back-button']"))

	//	check manage account organization
	TestObject manageOrganizationDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'manage-account-' + organizationName) + '\']')
	TestHelper.clickButton(manageOrganizationDiv)
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='Organization Information_manage_entity_action']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-Organization-back-button']"))
	
}

def checkManageAccountLink() {
	
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='List Your Profession']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='edit-profile-back-button']"))
	
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Create Company or Startup']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='search-company-back-button']"))
	
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Add Role In a Company']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='search-company-back-button']"))
	
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Create Organization']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='search-oraganization-back-button']"))
	
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Add Role In an Organization']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='search-oraganization-back-button']"))
	
//	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Saved Posts']"))
//	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='edit-profile-back-button']")) need id
	
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Notification Settings']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_notifications_back_button']"))
	
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Change Password']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='change-password-back-button']"))
	
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='manage-account_Help']"))
	
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-account_About Us']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='about-us-back-button']"))
	
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='manage-account_Privacy Policy']"))
	
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='manage-account_Terms of Service']"))
}

def sendReportFromManageAccount(TestObject targetReport) {
	TestHelper.clickButton(targetReport)
	TestHelper.setInputValue(TestHelper.getObjectById("//textarea[@id='report-description']"), 'Description Test')
	TestHelper.setInputValue(TestHelper.getObjectById("//textarea[@id='report-contact']"), 'contact Test')
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='report-send-action']"))
}





