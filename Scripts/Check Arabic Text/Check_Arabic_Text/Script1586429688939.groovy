import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import java.util.List
import java.util.Map

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable


// Define Varibales
TestObject fistName_input, lastName_input, about_textArea;
List<String> countries = TestHelper.prepareCountries();
Map<String, List<String>> cities = TestHelper.prepareCities();

 
// New Value
long newDate =  new Date().getTime();

// Country and city
String selectedCountryOfOrigine = TestHelper.getRandomValueFromList(countries);
String selectedCountryOfResidence = TestHelper.getRandomValueFromList(countries);

List<String> citiesOfCountryOfOrigine = cities.get(selectedCountryOfOrigine)
List<String> citiesOfCountryOfResidence = cities.get(selectedCountryOfResidence)
		
String selectedCityOfOrigine =  TestHelper.getRandomValueFromList(citiesOfCountryOfOrigine);
String selectedCityOfResidence =  TestHelper.getRandomValueFromList(citiesOfCountryOfResidence);
  
// Login and go to my profile tab
long date = new Date().getTime();
String registeredEmail = 'netways_user' + date + '@gmail.com'
String password = 'cvW8qx4B2o3F4VwP/kNsqA==';

String firstName = 'FirstName_' + date;
String lastName = 'LastName_' + date;
String fullName = firstName + ' ' + lastName;
String city = 'Hermel';

TestHelper.registerNewUser(registeredEmail, password, firstName, lastName, city, true);

TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_tab_id']"))

TestHelper.goToManageEntity();
TestObject manageBasicInformationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Basic Information_manage_entity_action']")
TestHelper.clickButton(manageBasicInformationAction)


// Edit Profile and check values
prepareEditProfileFields();

changeUserInfoValue(newDate);
checkEditedProfileValue(newDate);

// Clear Edited Fields
TestHelper.goToManageEntity();
TestHelper.clickButton(manageBasicInformationAction)
clearProfileFields(firstName, lastName);
checkClearedEditedProfileValue(fullName);
WebUI.closeBrowser();

def prepareEditProfileFields() {
	// About Info
	fistName_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='first-name-id']")
	lastName_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='last-name-id']")
	about_textArea = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//textarea[@id='about-section']")	
}

// Edit Fields
def changeUserInfoValue(long newDate) {
	TestHelper.setInputValue(fistName_input, newFirstName + newDate)
	TestHelper.setInputValue(lastName_input, newLastName + newDate)
	TestHelper.setInputValue(about_textArea, newAbout + newDate)
	
	TestObject saveUserInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='save_user_info']")
	TestHelper.clickButton(saveUserInfo);
	WebUI.delay(3)
	
	TestObject manageEntity = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-User-back-button']")
	TestHelper.clickButton(manageEntity);
}

def checkEditedProfileValue(long newDate) {
	WebUI.delay(1)
	boolean successfullyEdited = true;
	
	String edit_profile_fullName = (newFirstName + newDate) + " " +  (newLastName + newDate);
	
    // Prepare IDs
		String edited_profile_name_id = 'profile_title_' + edit_profile_fullName;
		String edited_about_id = 'user_shortBio_id_' + (newAbout + newDate);
 	 		
	// Prepare Objects
	 	TestObject edited_profile_name_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + edited_profile_name_id +"']");
		TestObject edited_about_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + edited_about_id +"']");
	   	
	// Check Object Exist
	if(!TestHelper.isElementPresent(edited_profile_name_object)) {
		successfullyEdited = false;
	}  
	else if(!TestHelper.isElementPresent(edited_about_object)) {
		successfullyEdited = false;
	}  
 
	if(successfullyEdited) {
	  println("Profile Updated Successfully");
	} else {
	  TestHelper.thrownException("Profile Not Updated")
	}
}
							
def checkClearedEditedProfileValue(String fullName) {
	WebUI.delay(1)
	boolean successfullyEdited = true;
		
	// Prepare IDs
		String edited_profile_name_id = 'profile_title_' + fullName;
			  
	// Prepare Objects
		TestObject edited_profile_name_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + edited_profile_name_id +"']");
		TestObject edited_about_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no_user_shortBio_id_Add a Short Biography']")
		   
	// Check Object Exist
		if(!TestHelper.isElementPresent(edited_profile_name_object)) {
			successfullyEdited = false;
		}
		else if(!TestHelper.isElementPresent(edited_about_object)) {
			successfullyEdited = false;
		}
	 
		if(successfullyEdited) {
		  println("Profile Clear Data Successfully");
		} else {
		  TestHelper.thrownException("Profile Not Clear Data")
		}
}
	
// Reset Fields
def clearProfileFields(String firstName, String lastName) {
	TestHelper.setInputValue(fistName_input, firstName)
	TestHelper.setInputValue(lastName_input, lastName)
	TestHelper.setInputValue(about_textArea, " ");
	
	TestObject saveUserInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='save_user_info']")
	TestHelper.clickButton(saveUserInfo);
	WebUI.delay(3)
	
	TestObject manageEntity = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-User-back-button']")
	TestHelper.clickButton(manageEntity);
}

