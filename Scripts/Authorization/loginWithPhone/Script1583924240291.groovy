import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import static org.junit.Assert.*

import org.junit.Test
import org.openqa.selenium.Keys as Keys

String validPhoneNumber = 70741123

String notRegistredNumber = 70741154

String wrongPassword = 'Test@1234'

String password = 'Test@123'

TestObject loginSubmitButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//button[@id=\'login-submit-button\']')
TestObject finalLoginSubmitButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//button[@id=\'submit-button\']')
TestObject forgotPasswordBackButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'forgot-password-back-button\']')
TestObject passwordInputId = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//input[@id=\'passwordInputId\']')

WebUI.openBrowser('')

WebUI.navigateToUrl('http://localhost:8100/auth/landing')

TestHelper.clickButton(TestHelper.getObjectById("//button[@id='loginButton']"))

TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='phoneNumber-input']"), notRegistredNumber)

TestHelper.clickButton(TestHelper.getObjectById("//button[@id='login-submit-button']"))

TestHelper.isElementPresent(TestHelper.getObjectById('//div[@id=\'phone not registered\']'))

WebUI.setText(TestHelper.getObjectById("//input[@id='phoneNumber-input']"), validPhoneNumber)

TestHelper.clickButton(TestHelper.getObjectById("//button[@id='login-submit-button']"))

TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='passwordInputId']"), 'ghsdfdf')

TestHelper.clickButton(TestHelper.getObjectById("//button[@id='submit-button']"))

TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='password-incorrect-message']"))

TestHelper.clickButton(TestHelper.getObjectById("//div[@id='forgot-password-message']"))

TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='resend-sms']"))

TestHelper.clickButton(TestHelper.getObjectById("//div[@id='forgot-password-back-button']"))

WebUI.setText(TestHelper.getObjectById("//input[@id='passwordInputId']"), wrongPassword)

TestHelper.clickButton(TestHelper.getObjectById("//button[@id='submit-button']"))

WebUI.setText(TestHelper.getObjectById("//input[@id='passwordInputId']"), password)

TestHelper.clickButton(TestHelper.getObjectById("//button[@id='submit-button']"))

TestHelper.isElementPresent(TestHelper.getObjectById("//input[@id='search-input-action']"))

WebUI.closeBrowser()


