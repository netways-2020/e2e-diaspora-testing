import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys


String validEmail = "testloginuser@gmail.com"
String password = "Test@123"
String inValidEmail ="mzaiterInvalid@netways.com"


//TestObject emailInput = TestHelper.getObjectById("//input[@id='email-input']")

WebUI.openBrowser('')

WebUI.navigateToUrl('http://localhost:8100/auth/landing')

TestHelper.clickButton(TestHelper.getObjectById("//button[@id='loginButton']"))

TestHelper.clickButton(TestHelper.getObjectById("//button[@id='login-with-email-button']"))

WebUI.setText(findTestObject('Page_Ionic App/input_Enter Your Email Address_email-input'), inValidEmail)

TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'login-email-submit-button\']'))

TestHelper.isElementPresent(TestHelper.getObjectById('//span[@id=\'error-message-email-number\']'))

TestObject emailInput = TestHelper.getObjectById("//input[@id='email-input']")

WebUI.setText(emailInput, validEmail)

TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'login-email-submit-button\']'))

TestObject passwordInput = TestHelper.getObjectById('//input[@id=\'passwordInputId\']')

TestHelper.setInputValue(passwordInput, 'Test123')

TestHelper.clickButton(TestHelper.getObjectById("//button[@id='submit-button']"))

TestHelper.isElementPresent(TestHelper.getObjectById('//div[@id=\'password-incorrect-message\']'))

TestHelper.setInputValue(passwordInput, password)

TestHelper.clickButton(TestHelper.getObjectById("//button[@id='submit-button']"))

WebUI.verifyElementPresent(findTestObject('Page_Ionic App/ion-slides_AabaPostInviteManageXoxoy X Post_a4a2da'), 0)

WebUI.closeBrowser()

