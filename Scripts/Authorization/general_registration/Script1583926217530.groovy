import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import cucumber.api.java.bm.Tetapi
import internal.GlobalVariable as GlobalVariable

import org.junit.After
import org.openqa.selenium.WebElement as WebElement

//get all the name and last name form data files
String[] firstNameLastNameArray = findTestData('Data Files/firstNameLastname').getAllData()

//generate random number from the lengh of the array
int randomFirstNameLastNameNumber = new Random().nextInt(firstNameLastNameArray.length)

//check if the gerated array is  0
int randomFirstNameLastName = randomFirstNameLastNameNumber == 0 ? 1 : randomFirstNameLastNameNumber

//get the random firstname from the exel table
String firstname = findTestData('Data Files/firstNameLastname').getValue(1, randomFirstNameLastName)

//get the random lastname  from the exel table
String lastname = findTestData('Data Files/firstNameLastname').getValue(2, randomFirstNameLastName)

//cities

//get the list of firstname and lastname from the exel as an array
String[] citiesArray = findTestData('Data Files/Cities').getAllData()

//generate random number from the lengh of the array for residence city
int randomResidenceCity = new Random().nextInt(200)

//generate random number from the lengh of the array for origin city
int randomOriginCity = new Random().nextInt(citiesArray.length)

//get the random firstname from the exel table
String originCity = findTestData('Data Files/Cities').getValue(1, randomOriginCity)

//get the random firstname from the exel table
String residenceCity = findTestData('Data Files/Cities').getValue(1,randomResidenceCity)

//generate new email
String email = ('blaybel@' + new Date().getTime()) + '.com'

//generate password
String password = 'Test@123'

//create company and organization values
String organizationName = 'Netways_Organization' + new Date().getTime();
String companyName = 'Netways' + new Date().getTime()
String roleName = "Founder"

//Cities
TestObject cityOrigineDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Aaba\']')
TestObject cityResidenceDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Aaba\']')
TestObject cityDistrictDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//span[@id=\'district_tag_label\']')
TestObject profile_tab = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='profile_tab_id']")

//registration button
TestObject registerByEmailButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='registerButton']")
TestObject loginWithEmailButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='login-with-email-button']")
TestObject loginEmailSubmitButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//button[@id=\'login-email-submit-button\']')
TestObject createAccountButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//button[@id=\'passwordInputId\']')
TestObject userSaveGeneralDetails = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//button[@id=\'user_save_general_details\']')

//registration input
TestObject emailInput = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//input[@id=\'email-input\']')
TestObject passwordInputId = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//input[@id=\'passwordInputId\']')
TestObject userInputFirstName = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//input[@id=\'user-input-first-name\']')
TestObject userInputLastName = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//input[@id=\'user-input-last-name\']')

//home page 
TestObject homeTitleProfilePage = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'home_title_' + firstname + ' ' + lastname) + '\']')
TestObject originCityInHomeDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//span[@id=\'country_lb_1_tag_label\']')
TestObject residenceCityInHomeDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//span[@id=\'country_lb_2_tag_label\']')

//town wallpaper
TestObject townWallpaperDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='search-entity-page-back-button']")
TestObject titleWallpaperDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + originCity) + '\']')
TestObject townPageBackButton= new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'town_page_back_button\']')

//searchEntity page
TestObject searchEntityBackButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='search-entity-page-back-button']")

//search page back button
TestObject searchBackButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='search-page-back-button']")

//Start Texting 
registerNewUser(registerByEmailButton, loginWithEmailButton, emailInput, email, loginEmailSubmitButton, passwordInputId, password, userInputFirstName, firstname, userInputLastName, lastname, userSaveGeneralDetails, cityOrigineDiv, cityResidenceDiv, originCity, residenceCity)

//createCompany(companyName,roleName);
//createOrganization(organizationName, roleName);
//TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-User-back-button']"));
//TestHelper.clickButton(TestHelper.getObjectById("//div[@id='home_tab_id']"));
//checkHomeWallpaperWithTown(homeTitleProfilePage, firstname, profile_tab, cityOrigineDiv, townPageBackButton)
//checkMainCardAction(originCityInHomeDiv, residenceCityInHomeDiv, cityOrigineDiv, townPageBackButton)
//checkpageCardSlider('Aaba', false)
//checkpageCardSlider(companyName, false)
//checkpageCardSlider(organizationName, true)
//checkpageCardRoute('Aaba', townPageBackButton, cityOrigineDiv)
//checkNewsSection();
//checkPeopleSection();
//checkExplorRoleSection()
//checkbecomeAtopNetworkerSection() 
//checkNetworkSection(firstname)
checkExplorNetworksSection(searchBackButton)
//WebUI.closeBrowser()


//////////////////////////////////////////////////////////////////////////////////////

def registerNewUser(TestObject registerByEmailButton, TestObject loginWithEmailButton, TestObject emailInput, String email, TestObject loginEmailSubmitButton, 
	TestObject passwordInputId, String password, TestObject userInputFirstName, String firstname, TestObject userInputLastName, String lastname, 
	TestObject userSaveGeneralDetails, TestObject cityOrigineDiv, TestObject cityResidenceDiv, originCity, residenceCity) {
 
	WebUI.openBrowser('')
	
	WebUI.navigateToUrl('http://localhost:8100/auth/landing')
	
	WebUI.click(registerByEmailButton)
	
	WebUI.click(loginWithEmailButton)
	
	WebUI.setText(emailInput, email)
	
	WebUI.click(loginEmailSubmitButton)
	
	WebUI.setText(passwordInputId, password)
	
	WebUI.click(findTestObject('Page_Ionic App/button_Create Account'))
	
	WebUI.setText(userInputFirstName,  firstname)
	
	WebUI.setText(userInputLastName, lastname)
	
	WebUI.click(userSaveGeneralDetails)
	
	WebUI.click(findTestObject('Object Repository/Page_Ionic App/button_Select city of origin'))
	
	WebUI.setText(findTestObject('Object Repository/Page_Ionic App/input_Complete Registration_input input-tex_735f02'), originCity)
	
	WebUI.click(cityOrigineDiv)
	
	WebUI.click(findTestObject('Page_Ionic App/button_Select city of residence'))
	
	WebUI.setText(findTestObject('Object Repository/Page_Ionic App/input_Complete Registration_input input-tex_735f02'), residenceCity)
	
	WebUI.click(cityResidenceDiv)
	
	WebUI.click(findTestObject('Page_Ionic App/button_Complete Registration'))
	
	TestObject manageProfilSection = TestHelper.getObjectById("//div[@id='manageProfileId']")
	WebUI.waitForElementVisible(manageProfilSection, 10)
}
 
def createCompany(String companyName, String roleName) { 
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_tab_id']"))
	TestHelper.goToManageEntity();
	TestObject manageBuinessRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Business Roles_manage_entity_action\']')
	TestHelper.clickButton(manageBuinessRoleAction)
	
	TestObject emptyStateObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-business-role-empty-state\']')
	boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)

	if (emptyStateObjectPresent == true) {
		println('hasn\'t business roles') 
		TestObject emptyStateAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-business-role-empty-state-button\']')
		WebUI.click(emptyStateAction)
		UserRoleHelper.createCompany(companyName, roleName)
		
		TestObject manageBusinessBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_business_role_back_action']")
		WebUI.waitForElementPresent(manageBusinessBackButton, 5)
		TestHelper.clickButton(manageBusinessBackButton);
		
	} else {
		println('has business roles') 
		TestObject addBusinessRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'add-new-business-role-action\']')
		WebUI.click(addBusinessRoleAction)
		UserRoleHelper.createCompany(companyName, roleName)
		
		TestObject manageBusinessBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_business_role_back_action']")
		WebUI.waitForElementPresent(manageBusinessBackButton, 5)
		
		TestHelper.clickButton(manageBusinessBackButton);
	}
}
 
def createOrganization(String organizationName, String roleName) { 
	TestObject manageNetworkRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='Network Roles_manage_entity_action']")
	TestHelper.clickButton(manageNetworkRoleAction)
	
	TestObject emptyStateObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-network-role-empty-state']")
	boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)

	if (emptyStateObjectPresent == true) {
		println("hasn't network roles")
 
		TestObject emptyStateAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-network-role-empty-state-button']")
		TestHelper.clickButton(emptyStateAction)

		// Create organization, add get created organization url
		UserRoleHelper.createOrganization(organizationName, roleName);
		 
		TestObject manageNetworkBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_network_role_back_action']")
		WebUI.waitForElementPresent(manageNetworkBackButton, 5) 
		TestHelper.clickButton(manageNetworkBackButton);
	
	} else {
		println("has network roles")
		TestObject addNetworkRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='add-new-network-role-action']")
		TestHelper.clickButton(addNetworkRoleAction)
		
		// Create organization, add get created organization url
		UserRoleHelper.createOrganization(organizationName, roleName);
		TestObject manageNetworkBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_network_role_back_action']")
		WebUI.waitForElementPresent(manageNetworkBackButton, 5) 
		
		TestHelper.clickButton(manageNetworkBackButton);
	} 
}

def checkHomeWallpaperWithTown(homeTitleProfilePage, firstname, profile_tab, cityOrigineDiv, townPageBackButton) {

	WebUI.verifyTextPresent('Welcome, ' + firstname, false)
	
	String  homeWallpaper =  WebUI.executeJavaScript("return document.getElementById('home_wallpaper').getAttribute('src')", null)
	
	println(homeWallpaper)
	
	TestHelper.isElementPresent(homeTitleProfilePage)

	TestHelper.clickButton(profile_tab)
	
	TestHelper.clickButton(cityOrigineDiv)
	
	WebUI.delay(10)
	
	String  townWallpaper = WebUI.executeJavaScript("return document.getElementById('town_wallpaper').textContent;", null)
	
	println(townWallpaper)
	
	if(homeWallpaper!=townWallpaper) {
		TestHelper.thrownException("the wallpaper of home and town not matach")
	}
	TestHelper.clickButton(townPageBackButton)
}

def checkMainCardAction(originCityInHomeDiv, residenceCityInHomeDiv, cityOrigineDiv, townPageBackButton) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='home_tab_id']"))
	WebUI.verifyElementPresent(originCityInHomeDiv,0)
	WebUI.verifyElementPresent(residenceCityInHomeDiv,0)
	TestHelper.clickButton(originCityInHomeDiv)
	TestHelper.isElementPresent(cityOrigineDiv)
	TestHelper.clickButton(townPageBackButton)
}

def checkpageCardSlider(cardName, isOrganization) {
	TestObject pagecardTitleDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'page_card_' + cardName) + '\']')
	TestObject pagecardactionPostDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'page_card_action_Post_' + cardName) + '\']')
	TestObject pagecardactionShareDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'page_card_action_Share_' + cardName) + '\']')
	if (isOrganization) {
		pagecardactionShareDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'page_card_action_Manage_' + cardName) + '\']')
	}
	TestObject pagecardactionInviteDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'page_card_action_Invite_' + cardName) + '\']')
	TestHelper.isElementPresent(pagecardTitleDiv)
	TestHelper.isElementPresent(pagecardactionPostDiv)
	TestHelper.isElementPresent(pagecardactionShareDiv)
	TestHelper.isElementPresent(pagecardactionInviteDiv)
}

def checkpageCardRoute(cardName, townPageBackButton, cityOrigineDiv) {
	TestObject pagecardTitleDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'page_card_' + cardName) + '\']')
	TestHelper.clickButton(pagecardTitleDiv)
	TestHelper.isElementPresent(cityOrigineDiv)
	TestHelper.clickButton(townPageBackButton)
}

def checkNewsSection() {
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='home_section_title_News']"));
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='home_section_subtitle_From pages you follow_News']"));
	TestHelper.isElementPresent(TestHelper.getObjectById("//span[@id='home_card_title_DiasporaID Headlines']"));
	TestHelper.countElementByClassName("app-list-container")
	int newsCount = TestHelper.countElementByClassName("app-list-container")
	if(newsCount < 3 ) {
		TestHelper.thrownException("number of news not over 3")
	}
	
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='home_section_action_News']"));
	//	TestHelper.isElementPresent(TestHelper.getObjectById("//span[@id='main_cardDiasporaID Headlines']"));
	int countListingCard = TestHelper.countElementByClassName("app-list-container")
	if(countListingCard < 3 ) {
		TestHelper.thrownException("number of news not over 3")
	}
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back_button_cardListing']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//ion-content[@id='home_page']"));
}

def checkPeopleSection() {
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='home_section_title_People']"));
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='home_section_subtitle_You should follow_People']"));
	int PeopleCount = TestHelper.countElementByClassName("parent-class_People")
	if(PeopleCount < 3 ) {
		TestHelper.thrownException("number of news not over 3")
	}
	WebElement PeopleTitleDiv = TestHelper.getItem('title_People' , 1)
	String PeopleTitleName = PeopleTitleDiv.text
	TestObject followPeopleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'right_action_id_' + PeopleTitleName) + '\']')
	TestHelper.clickButton(followPeopleAction)
	WebUI.click(TestHelper.getObjectById("//div[@id='profile_tab_id']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='user_follower_action']"))
	TestHelper.clickButton(TestHelper.getObjectById("//ion-segment-button[@id='followings_tab_id']"))
	TestObject SearchFollowingInput = TestHelper.getObjectById("//input[@id='manage_following_search']")
	TestHelper.setInputValue(SearchFollowingInput, PeopleTitleName)
	TestObject followPeopleDiv= new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + PeopleTitleName + '_profile') + '\']')
	TestHelper.isElementPresent(followPeopleDiv)
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_followers_back_id']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='home_tab_id']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='home_section_action_People']"))
	int PeopleCountDirectory = TestHelper.countElementByClassName("parent-class_People")
		if(PeopleCountDirectory < 3 ) {
			TestHelper.thrownException("number of news not over 3")
	}
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='home_tab_id']"))
}

def checkExplorRoleSection(searchEntityBackButton) {
	
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='home_section_title_Explore By Role']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='home_section_subtitle_Looking for a mayor, a mukhtar, or a town ambassador to contact? Start here._Explore By Role']"))
	
	// check explor by role button
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='explore_by_role_button_Mayors']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Mayors-search-entity-filter-type']"))
	TestHelper.clickButton(searchEntityBackButton)

	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='explore_by_role_button_Community Ambassadors']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Community Ambassadors-search-entity-filter-type']"))
	TestHelper.clickButton(searchEntityBackButton)
	
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='explore_by_role_button_Mukhtars']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Mukhtars-search-entity-filter-type']"))
	TestHelper.clickButton(searchEntityBackButton)
	
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='explore_by_role_button_Elected Members']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Elected Members-search-entity-filter-type']"))
	TestHelper.clickButton(searchEntityBackButton)
	
}

def checkbecomeAtopNetworkerSection() {
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='home_section_title_Explore By Role']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='invite_Invite your friends to join DiasporaID and become a top networker in your community']"))
	int countListingInviteCard = TestHelper.countElementByClassName("banner-top-networker-networker-name")
	println(countListingInviteCard)
	if(countListingInviteCard < 3 ) {
		TestHelper.thrownException("number of networker not over 3")
	}
	WebElement InvitePeopleTitleDiv = TestHelper.getItem('banner-top-networker-networker-name' , 1)
	String InvitePeopleTitleName = InvitePeopleTitleDiv.text
	TestObject followPeopleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'right_action_id_' + InvitePeopleTitleName) + '\']')
	TestHelper.clickButton(followPeopleAction)
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_tab_id']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='user_follower_action']"))
	TestHelper.clickButton(TestHelper.getObjectById("//ion-segment-button[@id='followings_tab_id']"))
	TestObject SearchFollowingInput = TestHelper.getObjectById("//input[@id='manage_following_search']")
	TestHelper.setInputValue(SearchFollowingInput, InvitePeopleTitleName)
	TestObject followPeopleDiv= new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + InvitePeopleTitleName + '_profile') + '\']')
	TestHelper.isElementPresent(followPeopleDiv)
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_followers_back_id']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='home_tab_id']"))
	WebElement InvitePeopleNumberDiv = TestHelper.getItem('banner-top-networker-networker-invites' , 0)
	String InvitePeopleNumber = InvitePeopleNumberDiv.text
	WebUI.verifyTextPresent(InvitePeopleNumber, false)
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='invite_button']"))
}

def checkNetworkSection(firstname) {
	WebUI.scrollToElement(TestHelper.getObjectById("//div[@id='home_section_title_Networks']"), 10)
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='home_section_title_Networks']"));
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='home_section_subtitle_Communities & Organizations_Networks']"));
	int NetworkCount = TestHelper.countElementByClassName("parent-class_Networks")
	if(NetworkCount < 1 ) {
		TestHelper.thrownException("number of network not over 1 ")
	}
	WebElement networkTitleDiv = TestHelper.getItem("title_Networks" , 1)
	String networkTitleName = networkTitleDiv.text
	TestObject followNetworkAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'right_action_id_' + networkTitleName) + '\']')
	TestHelper.clickButton(followNetworkAction)
	TestObject followNetworkName = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'title_name_' + networkTitleName) + '\']')
	TestHelper.clickButton(followNetworkName)
	TestHelper.clickButton(TestHelper.getObjectById("//span[@id='follower_count_id_tag_label']"))
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='search-organization-follower-input']"), firstname)
	WebUI.delay(5)
	WebUI.verifyTextPresent(firstname, false)
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back-follower-id']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='organization_back_button']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='home_section_action_Networks']"))
	
	int networkCountDirectory = TestHelper.countElementByClassName("parent-class_Networks")
	if(networkCountDirectory < 3 ) {
		TestHelper.thrownException("number of news not over 3")
}
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='home_tab_id']"))
}

def checkExplorNetworksSection(searchEntityBackButton) {
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='home_section_title_Explore Networks']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='home_section_title_Explore Networks']"))
	
	// check explor network button
	 
	//	organization
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='explore_by_role_button_Organizations']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Organizations_networks_search_entity']"))
	TestHelper.clickButton(searchEntityBackButton)
	 
	//	HomeTowns
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='explore_by_role_button_Hometowns']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Hometowns_networks_search_entity']"))
	TestHelper.clickButton(searchEntityBackButton)
	
	//	Diplomatic Missions
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='explore_by_role_button_Diplomatic Missions']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Diplomatic Missions_networks_search_entity']"))
	TestHelper.clickButton(searchEntityBackButton)
	
	//	Federations
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='explore_by_role_button_Federations']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Organizations_networks_search_entity']"))
	TestHelper.clickButton(searchEntityBackButton)
	
	//	Chambers  
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='explore_by_role_button_Chambers']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Organizations_networks_search_entity']"))
	TestHelper.clickButton(searchEntityBackButton)
	
	//	News
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='explore_by_role_button_News']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='News_networks_search_entity']"))
	TestHelper.clickButton(searchEntityBackButton)
	
	//	Events
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='explore_by_role_button_Events']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Events_networks_search_entity']"))
	TestHelper.clickButton(searchEntityBackButton)
	
}
