import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

String registeredEmail = 'mzaiterssss@netways.com'

String invalidEmailAddress = 'testInvalid'

String password = 'Test@123'

String newPassword = 'Test@1234'

//get the random firstname from the exel table 
String firstName = "ali" + new Date().getTime();

//get the random lastname  from the exel table
String lastName ='mahdi' + new Date().getTime();

//get the list of firstname and lastname from the exel as an array
String[] citiesArray = findTestData('Data Files/Cities').getAllData()

//generate random number from the lengh of the array for residence city
int randomResidenceCity = new Random().nextInt(200)

//generate random number from the lengh of the array for origin city
int randomOriginCity = new Random().nextInt(citiesArray.length)

//get the random firstname from the exel table
String originCity = findTestData('Data Files/Cities').getValue(1, randomOriginCity)

//get the random firstname from the exel table
String residenceCity = findTestData('Data Files/Cities').getValue(2,randomResidenceCity)

//generate new email 
String email = ('blaybel@' + new Date().getTime()) + '.com'

//manage account
TestObject cityOrigineDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\''+ originCity) + '\']')
TestObject cityResidenceDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\''+ residenceCity) + '\']')
TestObject countryResidenceDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'United States of America\']')
WebUI.openBrowser('')

WebUI.navigateToUrl('http://localhost:8100/auth/landing')

WebUI.click(TestHelper.getObjectById("//button[@id='registerButton']"))

TestHelper.clickButton(TestHelper.getObjectById("//button[@id='login-with-email-button']"))

//check enter email page
WebUI.setText(TestHelper.getObjectById('//input[@id=\'email-input\']'), invalidEmailAddress)
TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'login-email-submit-button\']'))
WebUI.verifyTextPresent('Please enter a valid email address', false)
WebUI.setText(TestHelper.getObjectById('//input[@id=\'email-input\']'), registeredEmail)
TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'login-email-submit-button\']'))
TestHelper.isElementPresent(TestHelper.getObjectById('//span[@id=\'error-message-email-number\']'))
WebUI.verifyTextPresent('Log In Using ' + registeredEmail, false)
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='already-registred-message " + registeredEmail + "']"))

TestHelper.isElementPresent(TestHelper.getObjectById('//div[@id=\'forgot-password-message\']'))
TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'password-back-button\']'))

WebUI.setText(TestHelper.getObjectById('//input[@id=\'email-input\']'), email)
TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'login-email-submit-button\']'))

WebUI.setText(TestHelper.getObjectById('//input[@id=\'passwordInputId\']'), password)
TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'submit-button\']'))


//check first name last name page
TestHelper.clickButton(TestHelper.getObjectById('//input[@id=\'user-input-first-name\']'))
TestHelper.clickButton(TestHelper.getObjectById('//input[@id=\'user-input-last-name\']'))
TestHelper.clickButton(TestHelper.getObjectById('//input[@id=\'user-input-first-name\']'))
WebUI.verifyTextPresent('First Name is required', false)
WebUI.verifyTextPresent('Last Name is required', false)
WebUI.setText(TestHelper.getObjectById('//input[@id=\'user-input-first-name\']'), firstName)
WebUI.setText(TestHelper.getObjectById('//input[@id=\'user-input-last-name\']'), lastName)
TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'user_save_general_details\']'))

//choose origin and residence city
TestHelper.clickButton(TestHelper.getObjectById("//button[@id='user_city_of_origin']"))
WebUI.setText(TestHelper.getObjectById("//input[@id='city-search-input']"), originCity)
TestHelper.clickButton(cityOrigineDiv)
TestHelper.clickButton(TestHelper.getObjectById("//button[@id='user_country_of_residence']"))
WebUI.setText(TestHelper.getObjectById("//input[@id='country-search-input']"), 'america')
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='United States of America']"))
TestHelper.clickButton(TestHelper.getObjectById("//button[@id='user_city_of_residence']"))
WebUI.setText(TestHelper.getObjectById("//input[@id='city-search-input']"), residenceCity)
WebUI.click(cityResidenceDiv)
TestHelper.clickButton(TestHelper.getObjectById("//button[@id='save_user_info']"))

WebUI.delay(3)
WebUI.verifyTextPresent('Welcome, ' + firstName, false)
TestHelper.isElementPresent(TestHelper.getObjectById('//span[@id=\'country_lb_1_tag_label\']'))

// change password flow
changePassword(newPassword,password)
WebUI.closeBrowser()

def changePassword(newPassword, password) {
	TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manageProfileId\']'))
	
	TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'Change Password\']'))
	
	TestHelper.setInputValue(TestHelper.getObjectById('//input[@id=\'old-password-input\']'), password)
	
	TestHelper.setInputValue(TestHelper.getObjectById('//input[@id=\'new-password-input\']'), newPassword)
	
	TestHelper.setInputValue(TestHelper.getObjectById('//input[@id=\'confirm-password-input\']'), newPassword)
	
	WebUI.click(TestHelper.getObjectById('//button[@id=\'save-change-button\']'))
	
	TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'Log Out\']'))
	
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='loginButton']"))
	
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='login-with-email-button']"))
	
	TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'login-email-submit-button\']'))
	
	WebUI.setText(TestHelper.getObjectById('//input[@id=\'passwordInputId\']'), newPassword)
	
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='submit-button']"))
	
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_tab_id']"))
	
}

