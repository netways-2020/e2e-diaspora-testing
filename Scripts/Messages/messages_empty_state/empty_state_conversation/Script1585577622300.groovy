import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

String firstname = "ali" +  + new Date().getTime();
String lastname = "mahdi" +  + new Date().getTime();
String originCity = 'Froun'
String residenceCity = 'Froun'

String email = "mahdi@" +  + new Date().getTime() + '.com';

AuthHelpers.townRegistration(originCity, residenceCity, firstname, lastname, email)
checkEmptyConversation()
WebUI.closeBrowser()


def checkEmptyConversation() {
	TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'maincard_Message\']'))
	String errorMessage = "'Error empty conversataion page'"
	TestHelper.checkObjectText("//div[@id='conversation-empty-state-title']", "No Conversations" , errorMessage);
	TestHelper.checkObjectText("//div[@id='conversation-empty-state-subtitle']", "You haven’t received any messages yet" , errorMessage);
	TestHelper.checkObjectImageSrc("//div[@id='conversation-empty-state-icon']", "assets/icons/emptyState/Messages.svg", errorMessage)
}
