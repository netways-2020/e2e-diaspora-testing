import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement

long date = new Date().getTime();
String organizationName = 'Netways_Organization' + date;
String roleName = "Founder";

// New User 
String registeredEmail = 'netways_user' + date + '@gmail.com'
String newRegisteredEmail = 'new_netways_user' + date + '@gmail.com'
String password = 'cvW8qx4B2o3F4VwP/kNsqA=='
String firstName = 'FirstName_' + date;
String lastName = 'LastName_' + date;
String city = 'Hermel';
String fullName = firstName + ' ' + lastName;


// Login and go to my profile tab
TestHelper.registerNewUser(registeredEmail, password, firstName, lastName, city, true);
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_tab_id']"))

WebElement currentUser = TestHelper.getItem('profile-name-title', 0)
String currentUserName = currentUser.text;


TestHelper.goToManageEntity();
TestObject manageNetworkRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Network Roles_manage_entity_action']")
TestHelper.clickButton(manageNetworkRoleAction)

checkOrganization(organizationName, roleName);

String organizationMessageContent = 'Message sent to: ' + selectedOrganizationName;

// Messages
registerNewUser(newRegisteredEmail, password, firstName, lastName, city);
sendMessageForOrganization(organizationUrl, organizationMessageContent);

// Check Messages
loginWithFirstUser(fullName, currentUserName, currentUserName, selectedOrganizationName, organizationMessageContent, registeredEmail, password);
println("Test Complete");
WebUI.delay(2);
WebUI.closeBrowser();

// Check Organization
def checkOrganization (String organizationName, String roleName) {
	TestObject emptyStateObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-network-role-empty-state']")
	boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)

	if (emptyStateObjectPresent == true) {
		println("hasn't network roles")
 
		TestObject emptyStateAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-network-role-empty-state-button']")
		TestHelper.clickButton(emptyStateAction)

		// Create organization, add get created organization url
		UserRoleHelper.createOrganization(organizationName, roleName);
		 
		TestObject manageNetworkBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_network_role_back_action']")
		WebUI.waitForElementPresent(manageNetworkBackButton, 5)
		
		getOrganizationName(organizationName, roleName);
	} else {
		println("has network roles")
		TestObject addNetworkRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='add-new-network-role-action']")
		TestHelper.clickButton(addNetworkRoleAction)
		
		// Create organization, add get created organization url
		UserRoleHelper.createOrganization(organizationName, roleName);
		TestObject manageNetworkBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_network_role_back_action']")
		WebUI.waitForElementPresent(manageNetworkBackButton, 5)
		getOrganizationName(organizationName, roleName);
	}
}

def getOrganizationName(String organizationName, String roleName) {
	String networkRoleId = organizationName + '-' + roleName + '-role';
	String networkRoleIdXpath = "//div[@id='"+networkRoleId+"']"
	TestObject networkRoleObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, networkRoleIdXpath)
	TestHelper.clickButton(networkRoleObject);
	 
	WebUI.delay(6);
	organizationUrl = WebUI.getUrl();
	 
	WebElement organizationNameObject = TestHelper.getItem('organization-name-title', 0)
	selectedOrganizationName = organizationNameObject.text;
}

 
// Send messages functions
def registerNewUser(String registeredEmail, String password, String firstName, String lastName, String city) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='organization_back_button']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_network_role_back_action']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-User-back-button']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_manage_accounts']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='logout-id']"));

	TestHelper.registerNewUser(registeredEmail, password, firstName, lastName, city, false)	
}
 
def sendMessageForOrganization(String organizationUrl, String message) {
	WebUI.navigateToUrl(organizationUrl);
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Message_Organization_entity_action']"));
	sendMessage(message);
}
 
def sendMessage(String message) {
	TestObject messageInput = TestHelper.getObjectById("//textarea[@id='message']")
	TestHelper.setInputValue(messageInput, message)
	WebUI.delay(1);
	TestObject sendMessageAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='send_message_action']")
	TestHelper.clickButton(sendMessageAction)
	WebUI.delay(2);
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back_button_conversation']"))
}

def loginWithFirstUser(String senderName,  String currentUserName, String profileName, String organizationName, String organizationMessageContent, String email, String password) {
	WebUI.navigateToUrl(TestHelper.getHomeUrl());
	TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manageProfileId\']'))
	TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'logout-id\']'))

	TestHelper.loginWithSpecificEmail(email, password);	
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='message_tab_id']"))
	
	checkOrganizationMessages(organizationName, senderName, currentUserName, organizationMessageContent);	
}

// Check messages function
 
def checkOrganizationMessages(String targetName, String senderName, String currentUserName, String message) {
	changeTarget(targetName);
	checkMessageExist(senderName, message);
	checkIfMessageReaded(senderName, currentUserName, message);
}
 
def changeTarget(String targetName) {
	TestObject changeTargetAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='change-target-id']")
	WebUI.click(changeTargetAction)
	
	TestObject selectedTargetBadge = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + targetName + "_mailbox_badge']")
	boolean selectedTargetBadgePresent = TestHelper.isElementPresent(selectedTargetBadge)
	
	if(!selectedTargetBadgePresent) {
		TestHelper.thrownException("Select target badge not exist");
	}
	
	TestObject selectedTarget = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" +targetName+ "_mailbox_account']")
	WebUI.click(selectedTarget)
}

def checkMessageExist(String senderName, String message) {	
	TestObject senderObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + senderName + "_other_user']")
	TestObject messageBadgeObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + message + "_other_user_badge']")
	TestObject messageContentObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + message + "_message_content']")

	boolean senderObjectPresent = TestHelper.isElementPresent(senderObject)
	boolean messageBadgeObjectPresent = TestHelper.isElementPresent(messageBadgeObject)
	boolean messageContentObjectPresent = TestHelper.isElementPresent(messageContentObject)
	
	if (!senderObjectPresent) {
		TestHelper.thrownException("Sender name not exist");
	} 
	else if (!messageBadgeObject) {
		TestHelper.thrownException("Message badge not exist");
	}  
	else if (!messageContentObject) {
		TestHelper.thrownException("Message content not exist");
	}	
}

def checkIfMessageReaded(String senderName, String currentUserName, String message) {
	
	// read message badge count
	TestObject messageCountObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='message-count-id']")
	String messageCountString = WebUI.getText(messageCountObject);
	int messageCount = Integer.parseInt(messageCountString);
	
	if(messageCount != 1) {
		TestHelper.thrownException("Message badge number not displayed");
	}
	
	TestObject senderObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + senderName + "_other_user']")
	TestHelper.clickButton(senderObject);
	
	TestObject senderIdObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//p[@id='" + senderName + "_sender_id']")
	TestObject incommingMessageContentObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + message + "_message_id']")
	TestObject manageConversationObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_conversation_action']")
	
	
	boolean senderIdObjectPresent = TestHelper.isElementPresent(senderIdObject)
	boolean incommingMessageContentObjectPresent = TestHelper.isElementPresent(incommingMessageContentObject)
	boolean manageConversationObjectPresent = TestHelper.isElementPresent(manageConversationObject)
		
	if (!senderIdObjectPresent) {
		TestHelper.thrownException("Sender id not exist");
	} else if (!incommingMessageContentObjectPresent) {
		TestHelper.thrownException("Incomming message content not exist");
	}  else if (!manageConversationObjectPresent) {
		TestHelper.thrownException("Manage conversation not exist");
	}
	
	
	// Read message
	String newMessage = 'Logged in message to ' + currentUserName;
	sendMessage(newMessage);
	WebUI.delay(1);
	TestObject sendMessageContentObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + newMessage + "_message_content']")
	boolean sendMessageContentObjectPresent = TestHelper.isElementPresent(sendMessageContentObject)
	if (!sendMessageContentObjectPresent) {
		TestHelper.thrownException("Send message id not exist");
	}
	
	
	// Check if message readed
	TestObject messageBadgeObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + senderName + "_other_user_badge']")
	TestObject messageContentObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + newMessage + "_message_content']")
	
	println('messageBadgeObject: ' + messageBadgeObject);
	boolean messageBadgeObjectPresent = TestHelper.isElementPresent(messageBadgeObject)
	boolean messageContentObjectPresent = TestHelper.isElementPresent(messageContentObject)
	
	if (messageBadgeObjectPresent) {
		TestHelper.thrownException("Message badge still exist");
	}  else if (!messageContentObjectPresent) {
		TestHelper.thrownException("Message content not exist");
	}

	WebUI.delay(2);
	
	// check message badge count decrement
	TestObject newMessageCountObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='message-count-id']")
	boolean newMessageCountObjectExist = TestHelper.isElementPresent(newMessageCountObject)
	
	if(newMessageCountObjectExist) {
		TestHelper.thrownException("Message badge number not decrement");
	}

   
	// Delete Conversation
	TestHelper.clickButton(senderObject);
	TestHelper.clickButton(manageConversationObject);
	
	TestObject deleteConversationConfirmationObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Delete Conversation_user_action']")
	TestHelper.clickButton(deleteConversationConfirmationObject);
	
	WebElement deleteConversationConfirmation = TestHelper.getItem('remove_conversation_confirmation', 0)
	deleteConversationConfirmation.click();

	TestObject deletedMessageObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + newMessage + "_message_content']")	
	boolean deletedMessageObjectPresent = TestHelper.isElementPresent(deletedMessageObject)
	
	if(deletedMessageObjectPresent) {
		TestHelper.thrownException("Message not deleted");
	} 
}
