import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement

long date = new Date().getTime();

// New User 
String registeredEmail = 'netways_user' + date + '@gmail.com'
String password = 'cvW8qx4B2o3F4VwP/kNsqA=='
String firstName = 'FirstName_' + date;
String lastName = 'LastName_' + date;
String city = 'Hermel';
String fullName = firstName + ' ' + lastName;

String adminUser = TestHelper.adminTown_email;
String mayorUser = TestHelper.mayorTown_email;

// Login and go to my profile tab
TestHelper.loginWithSpecificUserNameAndPassword(adminUser, password, true);
 
WebElement currentUser = TestHelper.getItem('profile-name-title', 0)
String currentUserName = currentUser.text;

TestHelper.goToManageEntity();
TestObject manageNetworkRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Network Roles_manage_entity_action']")
TestHelper.clickButton(manageNetworkRoleAction)

checkTown();
 
String townMessageContent = 'Message sent to: ' + selectedTownName;

// Messages
registerNewUser(registeredEmail, password, firstName, lastName, city);
sendMessageForTown(selectedTownUrl, townMessageContent);

// Check Messages
loginWithAdminTown(fullName, currentUserName, currentUserName, selectedTownName, townMessageContent, adminUser, password);

// Check Message in ambassador user
loginWithMayorUser(fullName, currentUserName, currentUserName, selectedTownName, townMessageContent, mayorUser, password);

println("Test Complete");
WebUI.delay(2);
WebUI.closeBrowser();

// Check Town
def checkTown() {
	int townRoleSize = TestHelper.countElementByClassName('manage_user_town_role')
	
	if (townRoleSize > 0) {
		WebElement selectedTown = TestHelper.getItem('manage_user_town_role', 0)
		selectedTown.click();
		
		selectedTownUrl = WebUI.getUrl();
		WebElement townNameObject = TestHelper.getItem('town-name-title', 0)
		selectedTownName = townNameObject.text;

		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='town_page_back_button']"))
		
		TestObject manageNetworkBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_network_role_back_action']")
		WebUI.waitForElementPresent(manageNetworkBackButton, 5);
		TestHelper.clickButton(manageNetworkBackButton);
				
	} else {
		TestHelper.thrownException("User hasn't town");
	}
}
	
 
// Send messages functions
def registerNewUser(String registeredEmail, String password, String firstName, String lastName, String city) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-User-back-button']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_manage_accounts']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='logout-id']"));

	TestHelper.registerNewUser(registeredEmail, password, firstName, lastName, city, false)	
}

def sendMessageForTown(String townUrl, String message) {
	WebUI.navigateToUrl(townUrl);
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='maincard_Message']"));
	sendMessage(message);
}


def sendMessage(String message) {
	TestObject messageInput = TestHelper.getObjectById("//textarea[@id='message']")
	TestHelper.setInputValue(messageInput, message)
	WebUI.delay(1);
	TestObject sendMessageAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='send_message_action']")
	TestHelper.clickButton(sendMessageAction)
	WebUI.delay(2);
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back_button_conversation']"))
}

def loginWithAdminTown(String senderName,  String currentUserName, String profileName, String townName, String townMessageContent, 
	String adminEmail, String adminPassword) {
	WebUI.navigateToUrl(TestHelper.getHomeUrl());
	TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manageProfileId\']'))
	TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'logout-id\']'))
	TestHelper.loginWithSpecificEmail(adminEmail, adminPassword);

	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='message_tab_id']"))
	checkTownMessage(townName, senderName, currentUserName, townMessageContent);
}
		
def loginWithMayorUser(String senderName,  String currentUserName, String profileName, String townName, String townMessageContent,
	String mayorEmail, String mayorPassword) {
	TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'profile_tab_id\']'))
	TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'profile_manage_accounts\']'))
	TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'logout-id\']'))
	TestHelper.loginWithSpecificEmail(mayorEmail, mayorPassword);

	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='message_tab_id']"))
	changeTarget(townName);
	
	checkMessageExistWithoutBadge(senderName, townMessageContent, currentUserName)
}
	
	

// Check messages function
def checkTownMessage(String targetName, String senderName, String currentUserName, String message) {
	changeTarget(targetName);
	checkMessageExist(senderName, message);
	checkIfMessageReaded(senderName, currentUserName, message);
}
 
def changeTarget(String targetName) {
	TestObject changeTargetAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='change-target-id']")
	WebUI.click(changeTargetAction)
	
	TestObject selectedTargetBadge = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + targetName + "_mailbox_account']")
	boolean selectedTargetBadgePresent = TestHelper.isElementPresent(selectedTargetBadge)
	
	if(!selectedTargetBadgePresent) {
		TestHelper.thrownException("Select target not exist");
	}
	
	TestObject selectedTarget = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" +targetName+ "_mailbox_account']")
	WebUI.click(selectedTarget)
}

def checkMessageExist(String senderName, String message) {	
	TestObject senderObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + senderName + "_other_user']")
	TestObject messageBadgeObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + message + "_other_user_badge']")
	TestObject messageContentObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + message + "_message_content']")

	boolean senderObjectPresent = TestHelper.isElementPresent(senderObject)
	boolean messageBadgeObjectPresent = TestHelper.isElementPresent(messageBadgeObject)
	boolean messageContentObjectPresent = TestHelper.isElementPresent(messageContentObject)
	
	if (!senderObjectPresent) {
		TestHelper.thrownException("Sender name not exist");
	} 
	else if (!messageBadgeObject) {
		TestHelper.thrownException("Message badge not exist");
	}  
	else if (!messageContentObject) {
		TestHelper.thrownException("Message content not exist");
	}	
}

def checkMessageExistWithoutBadge(String senderName, String message, String currentUserName) {
	String newMessage = 'Logged in message to ' + currentUserName;
	
	TestObject messageContentObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + newMessage + "_message_content']") 
	boolean messageContentObjectPresent = TestHelper.isElementPresent(messageContentObject)
	
	if (!messageContentObject) {
		TestHelper.thrownException("Message content not exist");
	}
	
	TestHelper.clickButton(messageContentObject);
	
	TestObject senderIdObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//p[@id='" + senderName + "_sender_id']")
	TestObject incommingMessageContentObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + newMessage + "_message_id']")
	TestObject manageConversationObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_conversation_action']")

	boolean senderIdObjectPresent = TestHelper.isElementPresent(senderIdObject)
	boolean incommingMessageContentObjectPresent = TestHelper.isElementPresent(incommingMessageContentObject)
	boolean manageConversationObjectPresent = TestHelper.isElementPresent(manageConversationObject)
		
	if (!senderIdObjectPresent) {
		TestHelper.thrownException("Sender id not exist");
	} else if (!incommingMessageContentObjectPresent) {
		TestHelper.thrownException("Incomming message content not exist");
	}  else if (!manageConversationObjectPresent) {
		TestHelper.thrownException("Manage conversation not exist");
	}

	// Delete Conversation
	TestHelper.clickButton(manageConversationObject);
	
	TestObject deleteConversationConfirmationObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Delete Conversation_user_action']")
	TestHelper.clickButton(deleteConversationConfirmationObject);
	
	WebElement deleteConversationConfirmation = TestHelper.getItem('remove_conversation_confirmation', 0)
	deleteConversationConfirmation.click();

	WebUI.delay(2);
	println("//div[@id='" + newMessage + "_message_content']");
	
	TestObject deletedMessageObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + senderName + "_other_user']")
	boolean deletedMessageObjectPresent = TestHelper.isElementPresent(deletedMessageObject)
	
	if(deletedMessageObjectPresent) {
		TestHelper.thrownException("Message not deleted");
	}
}


def checkIfMessageReaded(String senderName, String currentUserName, String message) {
	TestObject senderObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + senderName + "_other_user']")
	TestHelper.clickButton(senderObject);
	
	TestObject senderIdObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//p[@id='" + senderName + "_sender_id']")
	TestObject incommingMessageContentObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + message + "_message_id']")
	TestObject manageConversationObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_conversation_action']")
	
	
	boolean senderIdObjectPresent = TestHelper.isElementPresent(senderIdObject)
	boolean incommingMessageContentObjectPresent = TestHelper.isElementPresent(incommingMessageContentObject)
	boolean manageConversationObjectPresent = TestHelper.isElementPresent(manageConversationObject)
		
	if (!senderIdObjectPresent) {
		TestHelper.thrownException("Sender id not exist");
	} else if (!incommingMessageContentObjectPresent) {
		TestHelper.thrownException("Incomming message content not exist");
	}  else if (!manageConversationObjectPresent) {
		TestHelper.thrownException("Manage conversation not exist");
	}
	
	// Read message
	String newMessage = 'Logged in message to ' + currentUserName;
	sendMessage(newMessage);
	WebUI.delay(1);
	TestObject sendMessageContentObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + newMessage + "_message_content']")
	boolean sendMessageContentObjectPresent = TestHelper.isElementPresent(sendMessageContentObject)
	if (!sendMessageContentObjectPresent) {
		TestHelper.thrownException("Send message id not exist");
	}
	
	
	// Check if message readed
	TestObject messageBadgeObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + senderName + "_other_user_badge']")
	TestObject messageContentObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + newMessage + "_message_content']")
	
	println('messageBadgeObject: ' + messageBadgeObject);
	boolean messageBadgeObjectPresent = TestHelper.isElementPresent(messageBadgeObject)
	boolean messageContentObjectPresent = TestHelper.isElementPresent(messageContentObject)
	
	if (messageBadgeObjectPresent) {
		TestHelper.thrownException("Message badge still exist");
	}  else if (!messageContentObjectPresent) {
		TestHelper.thrownException("Message content not exist");
	}
}
