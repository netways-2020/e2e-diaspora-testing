import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable


String email = "testcover@gmail.com"
String password = "Test@123"
String originCity = "Aaba"
TestObject cityOrigineDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + originCity) + '\']')

int randomEntityNumber = new Random().nextInt(3)
int validRandomEntityNumber = randomEntityNumber == 0 ? 1 : randomEntityNumber;

String profileEntities =  findTestData('Data Files/randomEnitities').getValue(1, validRandomEntityNumber)
String diplomaticEntities =  findTestData('Data Files/randomEnitities').getValue(2, validRandomEntityNumber)
String organizationEntities =  findTestData('Data Files/randomEnitities').getValue(3, validRandomEntityNumber)
String companyEntities =  findTestData('Data Files/randomEnitities').getValue(4, validRandomEntityNumber)
String townEntities =  findTestData('Data Files/randomEnitities').getValue(5, validRandomEntityNumber)

//Start Testing
AuthHelpers.generaLogin(email, password, originCity)
TestHelper.isElementPresent(TestHelper.getObjectById('//div[@id=\'manageProfileId\']'))
WebUI.navigateToUrl("http://localhost:8100/" + profileEntities)
checkImageEntities('User_has_image', 'profile')

WebUI.navigateToUrl("http://localhost:8100/" + organizationEntities)
checkImageEntities('Organization_has_image', 'organization')

WebUI.navigateToUrl("http://localhost:8100/" + companyEntities)
checkImageEntities('Company_has_image', 'company')

WebUI.navigateToUrl("http://localhost:8100/" + townEntities)
checkwWallpaperEntities('town_wallpaper', 'town')

WebUI.navigateToUrl("http://localhost:8100/" + diplomaticEntities)
checkImageEntities('DiplomaticMissions_has_image', 'diplomatic')


WebUI.closeBrowser()


def checkImageEntities(imageId, entitiesName) {
	TestObject imageObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//img[@id=\'' + imageId) + '\']')
	boolean imageDiv  = TestHelper.isElementPresent(imageObject)
	if (!imageDiv) {
		TestHelper.thrownException("somthing wrong in image " + entitiesName)
	}
}


def checkwWallpaperEntities(wallpaperId, entitiesName) {
	TestObject wallpaperObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//p[@id=\'' + wallpaperId) + '\']')
	WebUI.delay(5)
	String wallpaperSrc = WebUI.getText(wallpaperObject)
	if (wallpaperSrc.equals("assets/images/placeholder/placeholder.jpg")) {
		TestHelper.thrownException("somthing wrong in wallpaper " + entitiesName)
	}
}
