import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement

//get all the name and last name form data files
String[] firstNameLastNameArray = findTestData('Data Files/firstNameLastname').getAllData()

//generate random number from the lengh of the array
int randomFirstNameLastNameNumber = new Random().nextInt(firstNameLastNameArray.length)

//check if the gerated array is  0
int randomFirstNameLastName = randomFirstNameLastNameNumber == 0 ? 1 : randomFirstNameLastNameNumber

//get the random first name from the excel table
String firstname = findTestData('Data Files/firstNameLastname').getValue(1, randomFirstNameLastName)

//get the random last name  from the excel table
String lastname = findTestData('Data Files/firstNameLastname').getValue(2, randomFirstNameLastName)

//citie
//get the list of first name and last name from the excel as an array
String[] citiesArray = findTestData('Data Files/Cities').getAllData()

//generate random number from the lengh of the array for residence city
int randomResidenceCity = new Random().nextInt(200)

//generate random number from the lengh of the array for origin city
int randomOriginCity = new Random().nextInt(citiesArray.length)

//get the random first name from the exel table
//String originCity = findTestData('Data Files/Cities').getValue(1, randomOriginCity)
String originCity = findTestData('Data Files/Cities').getValue(1, randomOriginCity)

//get the random firstname from the exel table
String residenceCity = findTestData('Data Files/Cities').getValue(1,randomResidenceCity)

//generate new email
String email = ('blaybel@' + new Date().getTime()) + '.com'

//generate password
String password = 'Test@123'

//create company and organization values
String organizationName = 'Netways_Organization' + new Date().getTime();
String companyName = 'Netways' + new Date().getTime()
String roleName = "Founder"

//Cities
TestObject cityOrigineDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Aaba\']')
TestObject cityResidenceDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Aaba\']')
TestObject cityDistrictDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//span[@id=\'district_city\']')
TestObject profile_tab = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//ion-tab-button[@id='tab-button-profile']")

//home page
TestObject homeTitleProfilePage = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'home_title_' + firstname + ' ' + lastname) + '\']')
TestObject originCityInHomeDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//span[@id=\'country_lb_1_tag_label\']')
TestObject residenceCityInHomeDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//span[@id=\'country_lb_1_tag_label\']')

//town wallpaper
TestObject townPageBackButton= new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'town_page_back_button\']')

//check section
TestObject checkSectionDiv= new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'home_section_title_Business Support\']')


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

registerNewUser( email, password, firstname, lastname, cityOrigineDiv, cityResidenceDiv, originCity, residenceCity)
scrollToBottom(checkSectionDiv)
createCompany(companyName,roleName);
createOrganization(organizationName, roleName);
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-User-back-button']"));
TestHelper.clickButton(TestHelper.getObjectById("//ion-tab-button[@id='tab-button-home']"));
checkHomeWallpaperWithTown(homeTitleProfilePage, firstname, profile_tab, cityOrigineDiv, townPageBackButton)
checkMainCardAction(originCityInHomeDiv, residenceCityInHomeDiv, cityOrigineDiv, townPageBackButton)
checkpageCardSlider('Aaba', false)
checkpageCardSlider(companyName, false)
checkpageCardSlider(organizationName, true)
checkpageCardRoute('Aaba', townPageBackButton, cityOrigineDiv)
checkNewsSection();
WebUI.closeBrowser()
//////////////////////////////////////////////////////////////////////////////////////

def registerNewUser( email, password, firstname, lastname, cityOrigineDiv, cityResidenceDiv, originCity, residenceCity){
 
	WebUI.openBrowser('')
	
	WebUI.navigateToUrl('http://localhost:8100/auth/landing')
	
	WebUI.click(TestHelper.getObjectById("//button[@id='registerButton']"))
	
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='login-with-email-button']"))
	
	WebUI.setText(TestHelper.getObjectById('//input[@id=\'email-input\']'), email)
	
	TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'login-email-submit-button\']'))
	
	WebUI.setText(TestHelper.getObjectById('//input[@id=\'passwordInputId\']'), password)
	
	TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'submit-button\']'))
	
	WebUI.setText(TestHelper.getObjectById('//input[@id=\'user-input-first-name\']'),  firstname)
	
	WebUI.setText(TestHelper.getObjectById('//input[@id=\'user-input-last-name\']'), lastname)
	
	TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'user_save_general_details\']'))
	
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='user_city_of_origin']"))
	
	WebUI.setText(TestHelper.getObjectById("//input[@id='city-search-input']"), 'Aaba')
	
	TestHelper.clickButton(cityOrigineDiv)
	
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='user_city_of_residence']"))
	
	WebUI.setText(TestHelper.getObjectById("//input[@id='city-search-input']"), 'Aaba')
	
	TestHelper.clickButton(cityResidenceDiv)
	
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='save_user_info']"))
	
	TestObject manageProfilSection = TestHelper.getObjectById("//div[@id='manageProfileId']")
	
	WebUI.waitForElementVisible(manageProfilSection, 5)
}
 
def createCompany(String companyName, String roleName) {
	TestHelper.clickButton(TestHelper.getObjectById("//ion-tab-button[@id='tab-button-profile']"))
	TestHelper.goToManageEntity();
	TestObject manageBuinessRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Business Roles_manage_entity_action\']')
	TestHelper.clickButton(manageBuinessRoleAction)
	
	TestObject emptyStateObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-business-role-empty-state\']')
	boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)

	if (emptyStateObjectPresent == true) {
		println('hasn\'t business roles')
		TestObject emptyStateAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-business-role-empty-state-button\']')
		WebUI.click(emptyStateAction)
		UserRoleHelper.createCompany(companyName, roleName)
		
		TestObject manageBusinessBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_business_role_back_action']")
		WebUI.waitForElementPresent(manageBusinessBackButton, 5)
		TestHelper.clickButton(manageBusinessBackButton);
		
	} else {
		println('has business roles')
		TestObject addBusinessRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'add-new-business-role-action\']')
		WebUI.click(addBusinessRoleAction)
		UserRoleHelper.createCompany(companyName, roleName)
		
		TestObject manageBusinessBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_business_role_back_action']")
		WebUI.waitForElementPresent(manageBusinessBackButton, 5)
		
		TestHelper.clickButton(manageBusinessBackButton);
	}
}
 
def createOrganization(String organizationName, String roleName) {
	TestObject manageNetworkRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='Network Roles_manage_entity_action']")
	TestHelper.clickButton(manageNetworkRoleAction)
	
	TestObject emptyStateObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-network-role-empty-state']")
	boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)

	if (emptyStateObjectPresent == true) {
		println("hasn't network roles")
 
		TestObject emptyStateAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-network-role-empty-state-button']")
		TestHelper.clickButton(emptyStateAction)

		// Create organization, add get created organization url
		UserRoleHelper.createOrganization(organizationName, roleName);
		 
		TestObject manageNetworkBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_network_role_back_action']")
		WebUI.waitForElementPresent(manageNetworkBackButton, 5)
		TestHelper.clickButton(manageNetworkBackButton);
	
	} else {
		println("has network roles")
		TestObject addNetworkRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='add-new-network-role-action']")
		TestHelper.clickButton(addNetworkRoleAction)
		
		// Create organization, add get created organization url
		UserRoleHelper.createOrganization(organizationName, roleName);
		TestObject manageNetworkBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_network_role_back_action']")
		WebUI.waitForElementPresent(manageNetworkBackButton, 5)
		
		TestHelper.clickButton(manageNetworkBackButton);
	}
}

def checkHomeWallpaperWithTown(homeTitleProfilePage, firstname, profile_tab, cityOrigineDiv, townPageBackButton) {
	
	String  homeWallpaper =  WebUI.executeJavaScript("return document.getElementById('home_wallpaper').getAttribute('src')", null)
	
//	if(HomeTitelProfileText != 'Welcome, '+firstname) {
//		TestHelper.thrownException('the title profile not corret')
//	}
	TestHelper.clickButton(profile_tab)
	
	TestHelper.clickButton(cityOrigineDiv)
	
	WebUI.delay(10)
	
	String  townWallpaper = WebUI.executeJavaScript("return document.getElementById('town_wallpaper').textContent;", null)
	
	println(townWallpaper)
	
	if(homeWallpaper!=townWallpaper) {
		TestHelper.thrownException("the wallpaper of home and town not matach")
	}
	TestHelper.clickButton(townPageBackButton)
}

def checkMainCardAction(originCityInHomeDiv, residenceCityInHomeDiv, cityOrigineDiv, townPageBackButton) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='home_tab_id']"))
	WebUI.verifyElementPresent(originCityInHomeDiv,0)
	WebUI.verifyElementPresent(residenceCityInHomeDiv,0)
	TestHelper.clickButton(originCityInHomeDiv)
	TestHelper.isElementPresent(cityOrigineDiv)
	TestHelper.clickButton(townPageBackButton)
}

def checkpageCardSlider(cardName, isOrganization) {
	TestObject pagecardTitleDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'page_card_' + cardName) + '\']')
	TestObject pagecardactionPostDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'page_card_action_Post_' + cardName) + '\']')
	TestObject pagecardactionShareDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'page_card_action_Share_' + cardName) + '\']')
	if (isOrganization) {
		pagecardactionShareDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'page_card_action_Manage_' + cardName) + '\']')
	}
	TestObject pagecardactionInviteDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'page_card_action_Invite_' + cardName) + '\']')
	TestHelper.isElementPresent(pagecardTitleDiv)
	TestHelper.isElementPresent(pagecardactionPostDiv)
	TestHelper.isElementPresent(pagecardactionShareDiv)
	TestHelper.isElementPresent(pagecardactionInviteDiv)
}

def checkpageCardRoute(cardName, townPageBackButton, cityOrigineDiv) {
	TestObject pagecardTitleDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'page_card_' + cardName) + '\']')
	TestHelper.clickButton(pagecardTitleDiv)
	TestHelper.isElementPresent(cityOrigineDiv)
	TestHelper.clickButton(townPageBackButton)}

def checkNewsSection() {
	TestObject news_icon_object = TestHelper.getObjectById("//div[@id='home_section_title_News_icon']")
	String newsIcon = WebUI.getText(news_icon_object);
	if(!newsIcon.equals("assets/icons/diasporaIcon/News_Active.svg")) {
		TestHelper.thrownException("news title icon is wrong")
	}
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='home_section_title_News']"));
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='home_section_subtitle_From pages you follow_News']"));
	TestHelper.countElementByClassName("app-list-container")
	int newsCount = TestHelper.countElementByClassName("app-list-container")
	if(newsCount < 3 ) {
		TestHelper.thrownException("number of news not over 3")
	}
	
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='home_section_action_News']"));
	TestHelper.isElementPresent(TestHelper.getObjectById("//span[@id='main_card_DiasporaID Headlines']"));
	int countListingCard = TestHelper.countElementByClassName("app-list-container")
	if(countListingCard < 3 ) {
		TestHelper.thrownException("number of news not over 3")
	}
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back_button_cardListing']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//ion-content[@id='home_page']"));
}

def scrollToBottom(checkSectionDiv) {	
	boolean checkSection = false
	while(!checkSection) {
		WebUI.click(TestHelper.getObjectById("//button[@id='home-scroll-To-Bottom']"))
		checkSection = TestHelper.isElementPresent(checkSectionDiv)
	}
}


