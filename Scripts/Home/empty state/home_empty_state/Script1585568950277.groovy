import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

String firstname = "ali" +  + new Date().getTime();
String lastname = "mahdi" +  + new Date().getTime();
String originCity = 'Froun'
String residenceCity = 'Froun'

String email = "mahdi@" +  + new Date().getTime() + '.com';
TestObject checkSectionDiv= new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'home_section_title_Business Support\']')



AuthHelpers.generalRegistration(originCity, residenceCity, firstname, lastname, email)
scrollToBottom(checkSectionDiv)
emptyPartOfOrganization()
joinGlobalBusinessHub()
WebUI.closeBrowser()

def emptyPartOfOrganization() {
	String errorMessage = "'Error in part of  an organization sections'"
	
	TestHelper.checkObjectText("//div[@id='Part Of An Organization?']", "Part Of An Organization?" , errorMessage);
	TestHelper.checkObjectText("//div[@id='List your position in an organization that you manage or belong to']", "List your position in an organization that you manage or belong to" , errorMessage);
	
	TestHelper.checkObjectText("//div[@id='home_President or Owner_action_title_Id']", "President or Owner" , errorMessage);
	TestHelper.checkObjectText("//div[@id='home_President or Owner_action_subTitle_Id']", "List Your Organization" , errorMessage);
	TestHelper.checkObjectImageSrc("//div[@id='home_President or Owner_action_icon_Id']", "assets/icons/diasporaIcon/Organization_Leadership.svg", errorMessage)
	
	TestHelper.checkObjectText("//div[@id='home_Organization Member_action_title_Id']", "Organization Member" , errorMessage);
	TestHelper.checkObjectText("//div[@id='home_Organization Member_action_subTitle_Id']", "Add Your Role" , errorMessage);
	TestHelper.checkObjectImageSrc("//div[@id='home_Organization Member_action_icon_Id']", "assets/icons/diasporaIcon/OrganizationMember.svg", errorMessage)
}


def joinGlobalBusinessHub() {
	String errorMessage = "'Error in Join A Global Business Hubsections'"
	
	TestHelper.checkObjectText("//div[@id='Join A Global Business Hub']", "Join A Global Business Hub" , errorMessage);
	TestHelper.checkObjectText("//div[@id='List your position in an company that you manage or belong to']", "List your position in an company that you manage or belong to" , errorMessage);
	
	TestHelper.checkObjectText("//div[@id='home_Owner or Employee_action_title_Id']", "Owner or Employee" , errorMessage);
	TestHelper.checkObjectText("//div[@id='home_Owner or Employee_action_subTitle_Id']", "Add or Join Company" , errorMessage);
	TestHelper.checkObjectImageSrc("//div[@id='home_Owner or Employee_action_icon_Id']", "assets/icons/diasporaIcon/OwnerOrEmployee.svg", errorMessage)
	
	TestHelper.checkObjectText("//div[@id='home_Self Employed_action_title_Id']", "Self Employed" , errorMessage);
	TestHelper.checkObjectText("//div[@id='home_Self Employed_action_subTitle_Id']", "List Your Profession" , errorMessage);
	TestHelper.checkObjectImageSrc("//div[@id='home_Self Employed_action_icon_Id']", "assets/icons/diasporaIcon/Self_Employed.svg", errorMessage)
}

def scrollToBottom(checkSectionDiv) {
	boolean checkSection = false
	while(!checkSection) {
		WebUI.click(TestHelper.getObjectById("//button[@id='home-scroll-To-Bottom']"))
		checkSection = TestHelper.isElementPresent(checkSectionDiv)
	}
}




