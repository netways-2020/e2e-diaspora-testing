import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import org.junit.After
import org.openqa.selenium.WebElement as WebElement

String firstName = "ali" +  + new Date().getTime();
String lastName  = "mahdi" +  + new Date().getTime();
String originCity = 'Aaba'
String residenceCity = 'Aaba'
String email = "mahdi@" +  + new Date().getTime() + '.com';

//create company and organization values
String organizationName = 'Netways_Organization' + new Date().getTime();
String companyName = 'Netways' + new Date().getTime()
String roleName = "Founder"

//Cities
TestObject cityOrigineDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Aaba\']')
TestObject cityResidenceDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Aaba\']')
TestObject cityDistrictDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//span[@id=\'district_city\']')
TestObject profile_tab = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//ion-tab-button[@id='tab-button-profile']")

//home page
TestObject homeTitleProfilePage = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'home_title_' + firstName + ' ' + lastName) + '\']')
TestObject originCityInHomeDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//span[@id=\'country_lb_1_tag_label\']')
TestObject residenceCityInHomeDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//span[@id=\'country_lb_1_tag_label\']')

//town wallpaper
TestObject townPageBackButton= new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'town_page_back_button\']')

//check section
TestObject checkSectionDiv= new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'home_section_title_Business Support\']')


//Start Testing
AuthHelpers.generalRegistration(originCity, residenceCity, firstName, lastName, email)
scrollToBottom(checkSectionDiv)
checkPeopleSection();
checkNetworkSection(firstName)
checkBusinessSection(firstName)
WebUI.closeBrowser()

//Functions

def checkPeopleSection() {
	TestObject people_icon_object = TestHelper.getObjectById("//div[@id='home_section_title_People_icon']")
	String peopleIcon = WebUI.getText(people_icon_object);
	if(!peopleIcon.equals("assets/icons/diasporaIcon/People_Active.svg")) {
		TestHelper.thrownException("people title icon is wrong")
	}
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='home_section_title_People']"));
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='home_section_subtitle_You should follow_People']"));
	int PeopleCount = TestHelper.countElementByClassName("parent-class_People")
	if(PeopleCount < 3 ) {
		TestHelper.thrownException("number of people not over 3")
	}
	WebElement PeopleTitleDiv = TestHelper.getItem('People_home_title' , 1)
	String PeopleTitleName = PeopleTitleDiv.text
	TestObject followPeopleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'right_action_id_' + PeopleTitleName) + '\']')
	TestHelper.clickButton(followPeopleAction)
	WebUI.delay(5)
	TestHelper.clickButton(TestHelper.getObjectById("//ion-tab-button[@id='tab-button-profile']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Manage_User_entity_action']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Following and Followed User_manage_entity_action']"))
	TestHelper.clickButton(TestHelper.getObjectById("//ion-segment-button[@id='followings_tab_id']"))
	TestObject SearchFollowingInput = TestHelper.getObjectById("//input[@id='manage_following_search']")
	TestHelper.setInputValue(SearchFollowingInput, PeopleTitleName)
	TestObject followPeopleDiv= new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + PeopleTitleName + '_profile') + '\']')
	TestHelper.isElementPresent(followPeopleDiv)
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_followers_back_id']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-User-back-button']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='home_tab_id']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='home_section_action_People']"))
	int PeopleCountDirectory = TestHelper.countElementByClassName("parent-class_People")
	if(PeopleCountDirectory < 3 ) {
		TestHelper.thrownException("number of news not over 3")
	}
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='home_tab_id']"))
}

def checkNetworkSection(firstName) {
	TestObject network_icon_object = TestHelper.getObjectById("//div[@id='home_section_title_Networks_icon']")
	String newsIcon = WebUI.getText(network_icon_object);
	if(!newsIcon.equals("assets/icons/diasporaIcon/Networks_Active.svg")) {
		TestHelper.thrownException("network title icon is wrong")
	}
	WebUI.scrollToElement(TestHelper.getObjectById("//div[@id='home_section_title_Networks']"), 10)
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='home_section_title_Networks']"));
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='home_section_subtitle_Communities & Organizations_Networks']"));
	int NetworkCount = TestHelper.countElementByClassName("parent-class_Networks")
	if(NetworkCount < 1 ) {
		TestHelper.thrownException("number of network not over 1 ")
	}
	WebElement networkTitleDiv = TestHelper.getItem("Networks_home_title" , 1)
	String networkTitleName = networkTitleDiv.text
	TestObject followNetworkAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'right_action_id_' + networkTitleName) + '\']')
	TestHelper.clickButton(followNetworkAction)
	TestObject followNetworkName = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'title_name_' + networkTitleName) + '\']')
	TestHelper.clickButton(followNetworkName)
	TestHelper.clickButton(TestHelper.getObjectById("//span[@id='follower_count_id_tag_label']"))
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='search-organization-follower-input']"), firstName)
	WebUI.delay(5)
	WebUI.verifyTextPresent(firstName, false)
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back-follower-id']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='organization_back_button']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='home_section_action_Networks']"))

	int networkCountDirectory = TestHelper.countElementByClassName("parent-class_Networks")
	if(networkCountDirectory < 3 ) {
		TestHelper.thrownException("number of organization not over 3")
	}
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='home_tab_id']"))
}

def checkBusinessSection(firstName) {
	TestObject business_icon_object = TestHelper.getObjectById("//div[@id='home_section_title_Business_icon']")
	String businessIcon = WebUI.getText(business_icon_object);
	if(!businessIcon.equals("assets/icons/diasporaIcon/Business_Inactive.svg")) {
		TestHelper.thrownException("business title icon is wrong")
	}
	WebUI.scrollToElement(TestHelper.getObjectById("//div[@id='home_section_title_Business']"), 5)
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='home_section_title_Business']"));
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='home_section_subtitle_Companies, Startups, Freelancers_Business']"));
	int businessCount = TestHelper.countElementByClassName("parent-class_People")
	if(businessCount < 1 ) {
		TestHelper.thrownException("number of people not over 1 ")
	}
	WebElement businessTitleDiv = TestHelper.getItem("Business_home_title", 0)
	String businessTitleName = businessTitleDiv.text
	println(businessTitleName)
	TestObject followbusinessAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'right_action_id_' + businessTitleName) + '\']')
	TestHelper.clickButton(followbusinessAction)
	TestObject businessPeolpleName = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'title_name_' + businessTitleName) + '\']')
	TestHelper.clickButton(businessPeolpleName)
	TestHelper.clickButton(TestHelper.getObjectById("//span[@id='follower_count_id_tag_label']"))
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='search-company-follower-input']"), firstName)
	WebUI.delay(5)
	WebUI.verifyTextPresent(firstName, false)
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back-follower-id']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='company_back_button']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='home_section_action_Business']"))

	int networkCountDirectory = TestHelper.countElementByClassName("parent-class_Business")
	if(networkCountDirectory < 3 ) {
		TestHelper.thrownException("number of  not over 3")
	}
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='home_tab_id']"))
}

def checkbusinessSupportSection(firstName){
	TestObject business_support_icon_object = TestHelper.getObjectById("//div[@id='home_section_title_Business Support_icon']")
	String businessSupportIcon = WebUI.getText(business_support_icon_object);
	if(!businessSupportIcon.equals("assets/icons/diasporaIcon/plant-vase.svg")) {
		TestHelper.thrownException("business support title icon is wrong")
	}
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='home_section_title_Business Support']"));
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='pages-sub-section-sub-section-mini subtitle']"));
	int businessSupportCount = TestHelper.countElementByClassName("support")
	if(businessSupportCount < 1 ) {
		TestHelper.thrownException("number of business not over 1 ")
	}
	WebElement businessSupportTitleDiv = TestHelper.getItem("Support_home_title", 0)
	String businessSuportName = businessSupportTitleDiv.text
	println(businessSuportName)
	TestObject followbusinessSupportAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'right_action_id_' + businessSuportName) + '\']')
	TestHelper.clickButton(followbusinessSupportAction)
	TestObject followbusinessSupportName = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'title_name_' + businessSuportName) + '\']')
	TestHelper.clickButton(followbusinessSupportName)
	TestHelper.clickButton(TestHelper.getObjectById("//span[@id='follower_count_id_tag_label']"))
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='search-company-follower-input']"), firstName)
	WebUI.delay(5)
	WebUI.verifyTextPresent(firstName, false)
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back-follower-id']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='company_back_button']"))
}

def scrollToBottom(checkSectionDiv) {
	boolean checkSection = false
	while(!checkSection) {
		WebUI.click(TestHelper.getObjectById("//button[@id='home-scroll-To-Bottom']"))
		checkSection = TestHelper.isElementPresent(checkSectionDiv)
	}
}

