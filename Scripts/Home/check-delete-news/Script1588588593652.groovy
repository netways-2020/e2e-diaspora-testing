import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement

WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebElement profileNameObject = TestHelper.getItem('profile-name-title', 0)
String profileName = profileNameObject.text;

TestHelper.getObjectById("//div[@id='user_updates']")
  
TestObject emptyStateUserUpdates = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-updates-empty-state']")
boolean emptyStateUserUpdatesExist = TestHelper.isElementPresent(emptyStateUserUpdates)

long now = new Date().getTime();
String postTitle = 'New post title add now_' + now;
String postDescription = 'New post description add now_' + now;

if (emptyStateUserUpdatesExist) {
	println("Hasn't udpates");
	WebUI.click(emptyStateUserUpdates)
	
	UserActionsHelper.addPost(postTitle, postDescription);
	
	WebUI.navigateToUrl(TestHelper.getHomeUrl());
	WebUI.delay(2);
	TestHelper.verifyHomeCardExist(profileName, postTitle, 'profile', true);
	
	deleteCard(postTitle, postDescription, profileName);
	checkDeletedCard(postTitle);
	
	WebUI.closeBrowser();
} else {
	println("Has udpates");

	TestObject home_tab = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='home_tab_id']")
	WebUI.click(home_tab)
	
	TestObject user_action_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//img[@id='user_action_button']")
	WebUI.click(user_action_button)
	
	TestObject create_post_action_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Create Post_user_action']")
	WebUI.click(create_post_action_button)

	UserActionsHelper.addPost(postTitle, postDescription);
	WebUI.delay(3);
	WebUI.navigateToUrl(TestHelper.getHomeUrl());
	WebUI.delay(2);
		 
	TestObject userUpdates = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='user_updates']")	 
	WebUI.waitForElementPresent(userUpdates, 3);
	TestHelper.verifyHomeCardExist(profileName, postTitle, 'profile', true);
	
	deleteCard(postTitle, postDescription, profileName);
	checkDeletedCard(postTitle);
		
	WebUI.closeBrowser();
}

def deleteCard(String postTitle, String postDescription, String profileName ) {
	TestObject cardObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+ postTitle  + "_id']")
	boolean cardObjectExist = TestHelper.isElementPresent(cardObject)
	
	if(cardObjectExist) {
		TestHelper.clickButton(cardObject);		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='card_details_manage_button']"))
		WebUI.delay(1);
		TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Delete Post_user_action']"))
		WebUI.delay(1);

		WebElement deleteCardConfirmation = TestHelper.getItem('remove-new-card', 0)
		deleteCardConfirmation.click();
		WebUI.delay(2);
	} 
}
 
def checkDeletedCard(String postTitle) {
	TestObject cardTitleObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+ postTitle  + "_id']")
	boolean cardTitleObjectExist = TestHelper.isElementPresent(cardTitleObject)

	if(cardTitleObjectExist) {
		TestHelper.thrownException("Card still exist")
	} else {
		print('Card deleted Successfully')		
	}
}

