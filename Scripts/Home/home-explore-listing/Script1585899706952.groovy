import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement

String firstName = "ali" +  + new Date().getTime();
String lastName  = "mahdi" +  + new Date().getTime();
String originCity = 'Aaba'
String residenceCity = 'Aaba'
String email = "mahdi@" +  + new Date().getTime() + '.com';

//create company and organization values
String organizationName = 'Netways_Organization' + new Date().getTime();
String companyName = 'Netways' + new Date().getTime()
String roleName = "Founder"

//Cities
TestObject cityOrigineDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Aaba\']')
TestObject cityResidenceDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Aaba\']')
TestObject cityDistrictDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//span[@id=\'district_city\']')
TestObject profile_tab = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//ion-tab-button[@id='tab-button-profile']")

//home page
TestObject homeTitleProfilePage = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'home_title_' + firstName + ' ' + lastName) + '\']')
TestObject originCityInHomeDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//span[@id=\'country_lb_1_tag_label\']')
TestObject residenceCityInHomeDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//span[@id=\'country_lb_1_tag_label\']')

//town wallpaper
TestObject townPageBackButton= new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'town_page_back_button\']')

TestObject checkSectionDiv= new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'home_section_title_Business Support\']')

//check section
TestObject searchEntityBackButton= new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'search-entity-page-back-button\']')
TestObject searchBackButton= new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'search-page-back-button\']')

//Start Testing
AuthHelpers.generalRegistration(originCity, residenceCity, firstName, lastName, email)
scrollToBottom(checkSectionDiv)
checkbecomeAtopNetworkerSection()
checkExplorRoleSection(searchEntityBackButton)
checkExplorNetworksSection(searchBackButton)
CheckPartOfOrganization()
checkExploreSectorSection(searchEntityBackButton)
joinBusinessHub()
WebUI.closeBrowser()

//Function
def checkExplorRoleSection(searchEntityBackButton) {

	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='home_section_title_Explore By Role']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='home_section_subtitle_Looking for a mayor, a mukhtar, or a town ambassador to contact? Start here._Explore By Role']"))
	
	// check explor by role button
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='home_explore_button_Mayors']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Mayors-search-entity-filter-type']"))
	TestHelper.clickButton(searchEntityBackButton)
	
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='home_explore_button_Community Ambassadors']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Community Ambassadors-search-entity-filter-type']"))
	TestHelper.clickButton(searchEntityBackButton)
	
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='home_explore_button_Mukhtars']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Mukhtars-search-entity-filter-type']"))
	TestHelper.clickButton(searchEntityBackButton)
	
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='home_explore_button_Elected Members']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Elected Members-search-entity-filter-type']"))
	TestHelper.clickButton(searchEntityBackButton)

}

def checkbecomeAtopNetworkerSection() {
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='home_section_title_Explore By Role']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='invite_Invite your friends to join DiasporaID and become a top networker in your community']"))
	int countListingInviteCard = TestHelper.countElementByClassName("banner-top-networker-networker-name")
	println(countListingInviteCard)
	if(countListingInviteCard < 3 ) {
		TestHelper.thrownException("number of networker not over 3")
	}
	WebElement InvitePeopleTitleDiv = TestHelper.getItem('banner-top-networker-networker-name' , 1)
	String InvitePeopleTitleName = InvitePeopleTitleDiv.text
	TestObject followPeopleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'right_action_id_' + InvitePeopleTitleName) + '\']')
	TestHelper.clickButton(followPeopleAction)
	WebUI.delay(5)
	TestHelper.clickButton(TestHelper.getObjectById("//ion-tab-button[@id='tab-button-profile']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='user_follower_action']"))
	TestHelper.clickButton(TestHelper.getObjectById("//ion-segment-button[@id='followings_tab_id']"))
	TestObject SearchFollowingInput = TestHelper.getObjectById("//input[@id='manage_following_search']")
	TestHelper.setInputValue(SearchFollowingInput, InvitePeopleTitleName)
	TestObject followPeopleDiv= new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + InvitePeopleTitleName + '_profile') + '\']')
	TestHelper.isElementPresent(followPeopleDiv)
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_followers_back_id']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='home_tab_id']"))
	WebElement InvitePeopleNumberDiv = TestHelper.getItem('banner-top-networker-networker-invites' , 0)
	String InvitePeopleNumber = InvitePeopleNumberDiv.text
	WebUI.verifyTextPresent(InvitePeopleNumber, false)
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='invite_button']"))
}

def checkExplorNetworksSection(searchEntityBackButton) {
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='home_section_title_Explore Networks']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='home_section_title_Explore Networks']"))

	// check explor network button

	//	organization
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='home_explore_button_Organizations']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='Organizations_networks_search_entity']"))
	TestHelper.clickButton(searchEntityBackButton)

	//	HomeTowns
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='home_explore_button_Hometowns']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='Hometowns_networks_search_entity']"))
	TestHelper.clickButton(searchEntityBackButton)

	//	Diplomatic Missions
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='home_explore_button_Diplomatic Missions']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='Diplomatic Missions_networks_search_entity']"))
	TestHelper.clickButton(searchEntityBackButton)

	//	Federations
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='home_explore_button_Federations']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='Organizations_networks_search_entity']"))
	TestHelper.clickButton(searchEntityBackButton)

	//	Chambers
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='home_explore_button_Chambers']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='Organizations_networks_search_entity']"))
	TestHelper.clickButton(searchEntityBackButton)

	//	News
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='home_explore_button_News']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='News_networks_search_entity']"))
	TestHelper.clickButton(searchEntityBackButton)

	//	Events
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='home_explore_button_Events']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='Events_networks_search_entity']"))
	TestHelper.clickButton(searchEntityBackButton)
}

def CheckPartOfOrganization() {
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='Part Of An Organization?']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='List your position in an organization that you manage or belong to']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='President or Owner_action']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//input[@id='searchForOrganizationInput']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='search-oraganization-back-button']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Organization Member_action']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//input[@id='searchForOrganizationInput']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='search-oraganization-back-button']"))
}

def checkExploreSectorSection(searchEntityBackButton) {

	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='home_section_title_Explore By Sector']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='home_section_subtitle_Companies & startups by industry_Explore By Sector']"))

	// check explor by role button
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='home_explore_button_Apparel & Fashion']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Apparel & Fashion-search-entity-filter-type']"))
	TestHelper.clickButton(searchEntityBackButton)

	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='home_explore_button_Hospitality']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Hospitality-search-entity-filter-type']"))
	TestHelper.clickButton(searchEntityBackButton)

	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='home_explore_button_Real Estate']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Real Estate-search-entity-filter-type']"))
	TestHelper.clickButton(searchEntityBackButton)

	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='home_explore_button_Information Technology and Services']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Information Technology and Services-search-entity-filter-type']"))
	TestHelper.clickButton(searchEntityBackButton)
}

def joinBusinessHub() {

	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='Join A Global Business Hub']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='List your position in an company that you manage or belong to']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Owner or Employee_action']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//input[@id='searchForCompanyInput']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='search-company-back-button']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Self Employed_action']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='profession-section']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='edit-profile-back-button']"))
	}

def scrollToBottom(checkSectionDiv) {
	boolean checkSection = false
	while(!checkSection) {
		WebUI.click(TestHelper.getObjectById("//button[@id='home-scroll-To-Bottom']"))
		checkSection = TestHelper.isElementPresent(checkSectionDiv)
	}
}

