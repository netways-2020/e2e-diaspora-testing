import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import org.openqa.selenium.WebElement

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Company/Founder or admin/Authorization for company/Login_For_Company'), [:], FailureHandling.STOP_ON_FAILURE)

String companyUrl = WebUI.getUrl();
WebElement companyNameObject = TestHelper.getItem('company-name-title', 0)
String companyName = companyNameObject.text;

TestHelper.getObjectById("//div[@id='company_updates']")
  
TestObject emptyStateCompanyUpdates = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-company-updates-empty-state']")
boolean emptyStateCompanyUpdatesExist = TestHelper.isElementPresent(emptyStateCompanyUpdates)

long now = new Date().getTime();
String eventTitle = 'New Event title add now_' + now;
String eventLocation = 'New location add now_' + now;
String eventDescription = 'New Event Descirption add now_' + now;


if (emptyStateCompanyUpdatesExist) {
	println("Hasn't udpates");
	 WebUI.click(emptyStateCompanyUpdates)
	
	TestObject create_event_action_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Create Event_user_action']")
	WebUI.click(create_event_action_button)
	 
	UserActionsHelper.addEvent(eventTitle, eventDescription, eventLocation)
	checkEventCard();
	
} else {
	println("Has udpates");

	WebUI.navigateToUrl(TestHelper.getHomeUrl());
	
	WebUI.delay(2);
	TestObject home_tab = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='home_tab_id']")
	WebUI.click(home_tab)
	
	TestObject user_action_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//img[@id='user_action_button']")
	WebUI.click(user_action_button)
	
	TestObject create_event_action_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Create Event_user_action']")
	WebUI.click(create_event_action_button)

	TestObject changeTargetAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='change-target-id']")
	WebUI.click(changeTargetAction)
 
	TestObject selectedTarget = TestHelper.getObjectById("//div[@id='" +companyName+ "_target']");
	WebUI.click(selectedTarget)
 
	UserActionsHelper.addEvent(eventTitle, eventDescription, eventLocation)

	checkEventCard();
}
 
def checkEventCard() {
	TestObject directory_tab = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='directory_tab_id']")
	WebUI.waitForElementPresent(directory_tab, 5)
	
	TestHelper.clickButton(TestHelper.getObjectById("//ion-segment-button[@id='directory-networks-tab']"));	
}