import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
String firstName = "ali" +  + new Date().getTime();
String lastName  = "mahdi" +  + new Date().getTime();
String originCity = 'Aaba'
String residenceCity = 'Aaba'
String email = "mahdi@" +  + new Date().getTime() + '.com';

//organization to follow Section
TestObject titleSectionOrganizationToFollow = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'directory_section_title_Organizations To Follow\']')
TestObject subtitleOrganizationToFollow = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'directory_section_subtitle_Lebanese organizations in your community_Organizations To Follow\']')
String parentClassOrganizationToFollow = "parent-class_Organizations"
String titleListClassOrganizationToFollow = "title_directory_Organizations"
TestObject searchFollowerOrganizationInput = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//input[@id=\'search-organization-follower-input\']')
TestObject leftActionOrganizationcBackButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'organization_back_button\']')

//diplomatic to follow Section
TestObject titleSectionDiplomaticToFollow = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'directory_section_title_Diplomatic Missions\']')
TestObject subtitleDiplomaticToFollow = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'directory_section_subtitle_Lebanese embassies and consulates_Diplomatic Missions\']')
String parentClassDiplomaticToFollow = "parent-class_Diplomatic"
String titleListClassDiplomaticToFollow = "title_directory_Diplomatic"
TestObject searchFollowerDiplomaticInput = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//input[@id=\'search-diplomatic-follower-input\']')
TestObject leftActiondiplomaticBackButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'diplomatic_back_button\']')

//town to follow Section
TestObject titleSectionTownToFollow = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'directory_section_title_Towns To Follow\']')
TestObject subtitleTownToFollow = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'directory_section_subtitle_Get updates happening near your hometown_Towns To Follow\']')
String parentClassTownToFollow = "parent-class_Towns"
String titleListClassTownToFollow = "title_directory_Towns"

//searchEntity page
TestObject searchEntityBackButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='search-entity-page-back-button']")

//check section
TestObject checkSectionDiv= new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'directory_section_title_Upcoming Events\']')


//Strat testing
AuthHelpers.generalRegistration(originCity, residenceCity, firstName, lastName, email)
TestHelper.clickButton(TestHelper.getObjectById('//ion-tab-button[@id=\'tab-button-directory\']'))
TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'network-directory-tab\']'))
scrollToBottom(checkSectionDiv)
checkTitleIcon()
checkNetwokOrganizarionSection(firstName, lastName, titleSectionOrganizationToFollow, titleSectionOrganizationToFollow, subtitleOrganizationToFollow, parentClassOrganizationToFollow, titleListClassOrganizationToFollow, searchFollowerOrganizationInput, leftActionOrganizationcBackButton)
checkNetwokDiplomaticSection(firstName, lastName, titleSectionDiplomaticToFollow, titleSectionDiplomaticToFollow, subtitleDiplomaticToFollow, parentClassDiplomaticToFollow, titleListClassDiplomaticToFollow, searchFollowerDiplomaticInput, leftActiondiplomaticBackButton)
checkExplorByCategorie(searchEntityBackButton)
CheckPartOfOrganization()
checkNetworkTownToFollow(firstName, lastName, titleSectionTownToFollow, titleSectionTownToFollow, subtitleTownToFollow, parentClassTownToFollow, titleListClassTownToFollow)
checkEvents()
WebUI.closeBrowser()

//Functions
def checkNetwokOrganizarionSection(firstname, lastname, scrollTo, networkTitle, networkSubtitle, parentNetworkClass, tilteClassList, searchFollowerInput, leftActionBackButton) {
	WebUI.scrollToElement(scrollTo, 10)
	TestHelper.isElementPresent(networkTitle);
	TestHelper.isElementPresent(networkSubtitle);
	int NetworkCount = TestHelper.countElementByClassName(parentNetworkClass)
	String parentTitle = WebUI.getText(networkTitle)
	if(NetworkCount < 1 ) {
		TestHelper.thrownException("number of " + parentTitle + " not over 1" )
	}
	WebElement networkTitleDiv = TestHelper.getItem(tilteClassList , 1)
	String networkTitleName = networkTitleDiv.text
	TestObject followNetworkAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'right_action_id_directory_' + parentTitle + '_' + networkTitleName) + '\']')
	TestHelper.clickButton(followNetworkAction)
	TestObject followNetworkName = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'title_name_' + networkTitleName) + '\']')
	TestHelper.clickButton(followNetworkName)
	TestHelper.clickButton(TestHelper.getObjectById("//span[@id='follower_count_id_tag_label']"))
	TestHelper.setInputValue(searchFollowerInput, firstname)
	String fullUserName = firstname + ' ' + lastname
	TestObject followUserDiv =new TestObject().addProperty('xpath', ConditionType.EQUALS, (('//div[@id=\'' + fullUserName) + 
        '_organization_followers') + '\']')
	WebUI.delay(3)
	if(!TestHelper.isElementPresent(followUserDiv)) {
		TestHelper.thrownException("following organization not succeeded")
	}
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back-follower-id']"))
	TestHelper.clickButton(leftActionBackButton)
	println("Test " + parentTitle + " succeeded")
}

def checkNetwokDiplomaticSection(firstname, lastname, scrollTo, networkTitle, networkSubtitle, parentNetworkClass, tilteClassList, searchFollowerInput, leftActionBackButton) {
	WebUI.scrollToElement(scrollTo, 10)
	TestHelper.isElementPresent(networkTitle);
	TestHelper.isElementPresent(networkSubtitle);
	int NetworkCount = TestHelper.countElementByClassName(parentNetworkClass)
	String parentTitle = WebUI.getText(networkTitle)
	if(NetworkCount < 1 ) {
		TestHelper.thrownException("number of " + parentTitle + " not over 1" )
	}
	WebElement networkTitleDiv = TestHelper.getItem(tilteClassList , 1)
	String networkTitleName = networkTitleDiv.text
	TestObject followNetworkAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'right_action_id_directory_' + parentTitle + '_' + networkTitleName) + '\']')
	TestHelper.clickButton(followNetworkAction)
	TestObject followNetworkName = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'title_name_' + networkTitleName) + '\']')
	TestHelper.clickButton(followNetworkName)
	TestHelper.clickButton(TestHelper.getObjectById("//span[@id='follower_count_id_tag_label']"))
	TestHelper.setInputValue(searchFollowerInput, firstname)
	String fullUserName = firstname + ' ' + lastname
	TestObject followUserDiv =new TestObject().addProperty('xpath', ConditionType.EQUALS, (('//div[@id=\'' + fullUserName) +
		'_diplomatic_followers') + '\']')
	WebUI.delay(3)
	if(!TestHelper.isElementPresent(followUserDiv)) {
		TestHelper.thrownException("following organization not succeeded")
	}
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back-follower-id']"))
	TestHelper.clickButton(leftActionBackButton)
	println("Test " + parentTitle + " succeeded")
}

def checkExplorByCategorie(searchEntityBackButton) {
	
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='directory_section_title_Explore By Category']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='directory_section_subtitle_Find lebanese organizations by interest_Explore By Category']"))
	
	// check explor by role button
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='director_networks_explore_button_Academy']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Academy-search-entity-filter-type']"))
	TestHelper.clickButton(searchEntityBackButton)

	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='director_networks_explore_button_Association']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Association-search-entity-filter-type']"))
	TestHelper.clickButton(searchEntityBackButton)
	
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='director_networks_explore_button_Centre']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Centre-search-entity-filter-type']"))
	TestHelper.clickButton(searchEntityBackButton)
	
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='director_networks_explore_button_Club']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Club-search-entity-filter-type']"))
	TestHelper.clickButton(searchEntityBackButton)
	
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='director_networks_explore_button_Community']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Community-search-entity-filter-type']"))
	TestHelper.clickButton(searchEntityBackButton)
	
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='director_networks_explore_button_Forum']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Forum-search-entity-filter-type']"))
	TestHelper.clickButton(searchEntityBackButton)
	
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='director_networks_explore_button_Foundation']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Foundation-search-entity-filter-type']"))
	TestHelper.clickButton(searchEntityBackButton)
	
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='director_networks_explore_button_Institution']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Institution-search-entity-filter-type']"))
	TestHelper.clickButton(searchEntityBackButton)
	
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='director_networks_explore_button_Organization']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Organization-search-entity-filter-type']"))
	TestHelper.clickButton(searchEntityBackButton)
	
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='director_networks_explore_button_Society']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Society-search-entity-filter-type']"))
	TestHelper.clickButton(searchEntityBackButton)
	
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='director_networks_explore_button_Mentor Hub']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Mentor Hub-search-entity-filter-type']"))
	TestHelper.clickButton(searchEntityBackButton)
	
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='director_networks_explore_button_DiasporaID Hub']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='DiasporaID Hub-search-entity-filter-type']"))
	TestHelper.clickButton(searchEntityBackButton)
	
	println("test  explore by category succeeded")
	
}
	
def CheckPartOfOrganization() {
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='Part Of An Organization?']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='List your position in an organization that you manage or belong to']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='President or Owner_action']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//input[@id='searchForOrganizationInput']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='search-oraganization-back-button']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Organization Member_action']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//input[@id='searchForOrganizationInput']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='search-oraganization-back-button']"))
	println("test  part of organization succeeded")
}

def checkNetworkTownToFollow(firstname,lastname, scrollTo, networkTitle, networkSubtitle, parentNetworkClass, tilteClassList) {
	WebUI.scrollToElement(scrollTo, 10)
	TestHelper.isElementPresent(networkTitle);
	TestHelper.isElementPresent(networkSubtitle);
	int NetworkCount = TestHelper.countElementByClassName(parentNetworkClass)
	String parentTitle = WebUI.getText(networkTitle)
	if(NetworkCount < 1 ) {
		TestHelper.thrownException("number of network not over 1 ")
	}
	WebElement networkTitleDiv = TestHelper.getItem(tilteClassList , 2)
	String networkTitleName = networkTitleDiv.text
	TestObject followNetworkAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'right_action_id_directory_' + parentTitle + '_' + networkTitleName) + '\']')
	TestHelper.clickButton(followNetworkAction)
	TestObject followNetworkName = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'title_name_' + networkTitleName) + '\']')
	TestHelper.clickButton(followNetworkName)
	TestHelper.clickButton(TestHelper.getObjectById("//span[@id='follower_number_tag_label']"))
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='search-input-follower']"), firstname)
	String fullUserName = firstname + ' ' + lastname
	TestObject followUserDiv =new TestObject().addProperty('xpath', ConditionType.EQUALS, (('//div[@id=\'' + fullUserName) +
		'_town') + '\']')
	WebUI.delay(3)
		if(!TestHelper.isElementPresent(followUserDiv)) {
		TestHelper.thrownException("following organization not succeeded")
	}
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='town_follower_back_button']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='town_page_back_button']"))
	
	println("test " + parentTitle + " succeeded")
}

def checkEvents() {
	int eventNumber = TestHelper.countElementByClassName("directory-event-section")
	
	if(eventNumber < 1) {
		TestHelper.thrownException("error in event section")
	} 
}

def checkTitleIcon() {
	TestObject organization_toFollow__icon_object = TestHelper.getObjectById("//div[@id='directory_section_title_Organizations To Follow_icon']")
	String organizationToFollowIcon = WebUI.getText(organization_toFollow__icon_object);
	if(!organizationToFollowIcon.equals("assets/icons/diasporaIcon/Networks_Active.svg")) {
		TestHelper.thrownException("organization to follow title icon is wrong")
	}
	
	TestObject diplomatic_mission_icon_object = TestHelper.getObjectById("//div[@id='directory_section_title_Diplomatic Missions_icon']")
	String diplomaticMissionIcon = WebUI.getText(diplomatic_mission_icon_object);
	if(!diplomaticMissionIcon.equals("assets/icons/diasporaIcon/diplomatic_with_flag.svg")) {
		TestHelper.thrownException("diplomatic missiontitle icon is wrong")
	}
	
	TestObject towns_toFollow_icon_object = TestHelper.getObjectById("//div[@id='directory_section_title_Towns To Follow_icon']")
	String townsToFollowIcon = WebUI.getText(towns_toFollow_icon_object);
	if(!townsToFollowIcon.equals("assets/icons/diasporaIcon/town_find.svg")) {
		TestHelper.thrownException("town to follow title icon is wrong")
	}
	
	TestObject upcoming_events_icon_object = TestHelper.getObjectById("//div[@id='directory_section_title_Upcoming Events_icon']")
	String upComingIcon = WebUI.getText(upcoming_events_icon_object);
	if(!upComingIcon.equals("assets/icons/diasporaIcon/Calendar_event.svg")) {
		TestHelper.thrownException("upcoming event title icon is wrong")
	}
}

def scrollToBottom(checkSectionDiv) {
	boolean checkSection = false
	while(!checkSection) {
		WebUI.delay(1)
		WebUI.click(TestHelper.getObjectById("//button[@id='directory-networks-scroll-To-Bottom']"))
		checkSection = TestHelper.isElementPresent(checkSectionDiv)
	}
}