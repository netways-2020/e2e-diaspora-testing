import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
String firstName = "ali" +  + new Date().getTime();
String lastName = "mahdi" +  + new Date().getTime();
String originCity = 'Aaba'
String residenceCity = 'Aaba'
String email = "mahdi@" +  + new Date().getTime() + '.com';

//Lebane wine section
TestObject titleSectionLebaneseWineToFollow = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'directory_section_title_Lebanese Wines\']')
TestObject subtitleLebaneseWineToFollow = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'directory_section_subtitle_A curated list by DiasporaID_Lebanese Wines\']')
String parentClassLebaneseWineToFollow = "parent-class_Lebanese"
String titleListClassLebaneseWineToFollow = "title_directory_Lebanese"
TestObject searchFollowerLebaneseWineInput = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//input[@id=\'search-company-follower-input\']')
TestObject leftActionLebaneseWineBackButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'company_back_button\']')

//support organization
TestObject titleSectionSupportOrganizationToFollow = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'directory_section_title_Support Organizations\']')
TestObject subtitleSupportOrganizationToFollow = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'directory_section_subtitle_Lebanese business support organizations_Support Organizations\']')
String parentClassSupportOrganizationToFollow = "parent-class_Support"
String titleListClassSupportOrganizationToFollow = "title_directory_Support"
TestObject searchFollowerSupportOrganizationInput = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//input[@id=\'search-organization-follower-input\']')
TestObject leftActionSupportOrganizationBackButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'organization_back_button\']')

//companies to follow
TestObject titleSectionCompanyToFollow = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'directory_section_title_Companies To Follow\']')
TestObject subtitleComapnyToFollow = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'directory_section_subtitle_Businesses & startups that might interest you_Companies To Follow\']')
String parentClassCompanyToFollow = "parent-class_Companies"
String titleListClassCompanyToFollow = "title_directory_Companies"
TestObject searchFollowerComapnyInput = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//input[@id=\'search-company-follower-input\']')
TestObject leftActionCompanyBackButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'company_back_button\']')

//Top Lebanese Startups
TestObject titleSectionLebaneseStartupsToFollow = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'directory_section_title_Top Lebanese Startups\']')
TestObject subtitleLebaneseStartupsToFollow = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'directory_section_subtitle_A curated list by DiasporaID_Top Lebanese Startups\']')
String parentClassLebaneseStartupsToFollow = "parent-class_Top"
String titleListClassLebaneseStartupsToFollow = "title_directory_Top"
TestObject searchFollowerLebaneseStartupsInput = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//input[@id=\'search-company-follower-input\']')
TestObject leftActionLebaneseStartupsBackButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'company_back_button\']')

//check section
TestObject checkSectionDiv= new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'directory_section_title_Top Lebanese Startups\']')

//searchEntity page
TestObject searchEntityBackButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='search-entity-page-back-button']")

//Start Testing 
AuthHelpers.generalRegistration(originCity, residenceCity, firstName, lastName, email)
TestHelper.clickButton(TestHelper.getObjectById('//ion-tab-button[@id=\'tab-button-directory\']'))
TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'business-directory-tab\']'))
scrollToBottom(checkSectionDiv)
checkTitleIcon()
checkBusinessSection(firstName, lastName, titleSectionLebaneseWineToFollow, 
					 titleSectionLebaneseWineToFollow, subtitleLebaneseWineToFollow, 
					 parentClassLebaneseWineToFollow, titleListClassLebaneseWineToFollow, 
					 searchFollowerLebaneseWineInput, leftActionLebaneseWineBackButton)

checkBusinessSection(firstName, lastName, 
					 titleSectionCompanyToFollow, titleSectionCompanyToFollow, 
					 subtitleComapnyToFollow, parentClassCompanyToFollow, 
					 titleListClassCompanyToFollow, searchFollowerComapnyInput, 
					 leftActionCompanyBackButton)

checkExplorBySector(searchEntityBackButton)
joinBusinessHub()
checkSupportOrganizationSection(firstName, lastName, 
								titleSectionSupportOrganizationToFollow, titleSectionSupportOrganizationToFollow, 
								subtitleSupportOrganizationToFollow, parentClassSupportOrganizationToFollow, 
								titleListClassSupportOrganizationToFollow, searchFollowerSupportOrganizationInput, 
								leftActionSupportOrganizationBackButton)


checkBusinessSection(firstName, lastName, 
					 titleSectionLebaneseStartupsToFollow, titleSectionLebaneseStartupsToFollow, 
					 subtitleLebaneseStartupsToFollow, parentClassLebaneseStartupsToFollow, 
					 titleListClassLebaneseStartupsToFollow, searchFollowerLebaneseStartupsInput, 
					 leftActionLebaneseStartupsBackButton)

WebUI.closeBrowser()

//Function
def checkBusinessSection(firstName,lastName, scrollTo, businessTitle, businessSubtitle, parentbusinessClass, tilteClassList, searchFollowerInput, leftActionBackButton) {
	WebUI.scrollToElement(scrollTo, 10)
	TestHelper.isElementPresent(businessTitle);
	TestHelper.isElementPresent(businessSubtitle);
	int businessCount = TestHelper.countElementByClassName(parentbusinessClass)
	String parentTitle = WebUI.getText(businessTitle)
	if(businessCount < 1 ) {
		TestHelper.thrownException("number of business not over 1 ")
	}
	WebElement businessTitleDiv = TestHelper.getItem(tilteClassList, 1)
	String businessTitleName = businessTitleDiv.text
//	String peopleTitleName = 'new comp'
	println(businessTitleName)
	TestObject followBusinessAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'right_action_id_directory_' + parentTitle + '_' + businessTitleName) + '\']')
	TestHelper.clickButton(followBusinessAction)
	TestObject followPeolpleName = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'title_name_' + businessTitleName) + '\']')
	TestHelper.clickButton(followPeolpleName)
	TestHelper.clickButton(TestHelper.getObjectById("//span[@id='follower_count_id_tag_label']"))
	TestHelper.setInputValue(searchFollowerInput, firstName)
	String fullUserName = firstName + ' ' + lastName
	TestObject followUserDiv =new TestObject().addProperty('xpath', ConditionType.EQUALS, (('//div[@id=\'' + fullUserName) +
		'_company_followers') + '\']')
	WebUI.delay(3)
		if(!TestHelper.isElementPresent(followUserDiv)) {
		TestHelper.thrownException("following company not succeeded")
	}
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back-follower-id']"))
	TestHelper.clickButton(leftActionBackButton)
}

def checkSupportOrganizationSection(firstName,lastName, scrollTo, businessTitle, businessSubtitle, parentbusinessClass, tilteClassList, searchFollowerInput, leftActionBackButton) {
	WebUI.scrollToElement(scrollTo, 10)
	TestHelper.isElementPresent(businessTitle);
	TestHelper.isElementPresent(businessSubtitle);
	int businessCount = TestHelper.countElementByClassName(parentbusinessClass)
	String parentTitle = WebUI.getText(businessTitle)
	if(businessCount < 1 ) {
		TestHelper.thrownException("number of business not over 1 ")
	}
	WebElement businessTitleDiv = TestHelper.getItem(tilteClassList, 1)
	String businessTitleName = businessTitleDiv.text
//	String peopleTitleName = 'new comp'
	println(businessTitleName)
	TestObject followBusinessAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'right_action_id_directory_' + parentTitle + '_' + businessTitleName) + '\']')
	TestHelper.clickButton(followBusinessAction)
	TestObject followPeolpleName = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'title_name_' + businessTitleName) + '\']')
	TestHelper.clickButton(followPeolpleName)
	TestHelper.clickButton(TestHelper.getObjectById("//span[@id='follower_count_id_tag_label']"))
	TestHelper.setInputValue(searchFollowerInput, firstName)
	String fullUserName = firstName + ' ' + lastName
	TestObject followUserDiv =new TestObject().addProperty('xpath', ConditionType.EQUALS, (('//div[@id=\'' + fullUserName) +
		'_organization_followers') + '\']')
	WebUI.delay(3)
		if(!TestHelper.isElementPresent(followUserDiv)) {
		TestHelper.thrownException("following organization not succeeded")
	}
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back-follower-id']"))
	TestHelper.clickButton(leftActionBackButton)
}

def checkExplorBySector(searchEntityBackButton) {
	
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='directory_section_title_Explore By Sector']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='directory_section_subtitle_Lebanese companies & startups by industry_Explore By Sector']"))
	
	// check explor by role button
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='director_business_explore_button_Apparel & Fashion']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Apparel & Fashion-search-entity-filter-type']"))
	TestHelper.clickButton(searchEntityBackButton)

	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='director_business_explore_button_Consumer Goods']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Consumer Goods-search-entity-filter-type']"))
	TestHelper.clickButton(searchEntityBackButton)
	
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='director_business_explore_button_Hospitality']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Hospitality-search-entity-filter-type']"))
	TestHelper.clickButton(searchEntityBackButton)
	
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='director_business_explore_button_Real Estate']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Real Estate-search-entity-filter-type']"))
	TestHelper.clickButton(searchEntityBackButton)
	
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='director_business_explore_button_Information Technology and Services']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='Information Technology and Services-search-entity-filter-type']"))
	TestHelper.clickButton(searchEntityBackButton)
}

def joinBusinessHub() {
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='Join A Global Business Hub']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='List your position in an company that you manage or belong to']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Owner or Employee_action']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//input[@id='searchForCompanyInput']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='search-company-back-button']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Self Employed_action']"))
	TestHelper.isElementPresent(TestHelper.getObjectById("//button[@id='profession-section']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='edit-profile-back-button']"))
}

def checkTitleIcon() {
	TestObject lebanese_wine__icon_object = TestHelper.getObjectById("//div[@id='directory_section_title_Lebanese Wines_icon']")
	String lebaneWineIcon = WebUI.getText(lebanese_wine__icon_object);
	if(!lebaneWineIcon.equals("assets/icons/diasporaIcon/fav-list.svg")) {
		TestHelper.thrownException("lebanese wine  title icon is wrong")
	}
	
	TestObject company_toFollow__icon_object = TestHelper.getObjectById("//div[@id='directory_section_title_Companies To Follow_icon']")
	String toFollowIcon = WebUI.getText(company_toFollow__icon_object);
	if(!toFollowIcon.equals("assets/icons/diasporaIcon/company_32.svg")) {
		TestHelper.thrownException("company to follow title icon is wrong")
	}
	
	TestObject support_organization_icon_object = TestHelper.getObjectById("//div[@id='directory_section_title_Support Organizations_icon']")
	String supportOrganizaationIcon = WebUI.getText(support_organization_icon_object);
	if(!supportOrganizaationIcon.equals("assets/icons/diasporaIcon/plant-vase.svg")) {
		TestHelper.thrownException("support organization title icon is wrong")
	}
	
	TestObject lebanes_startUp_icon_object = TestHelper.getObjectById("//div[@id='directory_section_title_Top Lebanese Startups_icon']")
	String StartUpIcon = WebUI.getText(lebanes_startUp_icon_object);
	if(!StartUpIcon.equals("assets/icons/diasporaIcon/rocket_32.svg")) {
		TestHelper.thrownException("top lebanese start up title icon is wrong")
	}
}

def scrollToBottom(checkSectionDiv) {
	boolean checkSection = false
	while(!checkSection) {
		WebUI.delay(1)
		WebUI.click(TestHelper.getObjectById("//button[@id='directory-business-scroll-To-Bottom']"))
		checkSection = TestHelper.isElementPresent(checkSectionDiv)
	}
}


