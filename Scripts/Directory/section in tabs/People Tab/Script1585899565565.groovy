import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
import static org.junit.Assert.*
import org.junit.After as After
import org.junit.Test as Test
String firstName = "ali" +  + new Date().getTime();
String lastName  = "mahdi" +  + new Date().getTime();
String originCity = 'Aaba'
String residenceCity = 'Aaba'
String email = "mahdi@" +  + new Date().getTime() + '.com';

//People Section
TestObject titleSectionPeople = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'directory_section_title_People To Follow\']')
TestObject subtitleSectionPeople = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'directory_section_subtitle_Profiles you might be interested in_People To Follow\']')
String parentClassNamePeople = 'parent-class_People'
String titleListClassPeople = 'title_directory_People'

//Ambasador and consultant section
TestObject titleSectionAmbassadors = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'directory_section_title_Ambassadors & Consuls\']')
TestObject subtitleSectionAmbassadors = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'directory_section_subtitle_Diplomats in or near your community_Ambassadors & Consuls\']')
String parentClassNameAmbassadors = 'parent-class_Ambassadors'
String titleListClassAmbassadors = 'title_directory_Ambassadors'

//Honorary Consols
TestObject titleSectionHonorary = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'directory_section_title_Honorary Consuls\']')
TestObject subtitleSectionHonorary = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'directory_section_subtitle_Honorary consuls in or near your community_Honorary Consuls\']')
String parentClassNameHonorary = 'parent-class_Honorary'
String titleListClassHonorary = 'title_directory_Honorary'

//Featured professional
TestObject titleSectionFeatured = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'directory_section_title_Featured Professionals\']')
TestObject subtitleSectionFeatured = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'directory_section_subtitle_Lebanese excelling at what they do_Featured Professionals\']')
String parentClassNameFeatured = 'parent-class_Featured'
String titleListClassFeatured = 'title_directory_Featured'

//in your sector
TestObject titleSectionInYourSector = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'directory_section_title_In Your Sector\']')
TestObject subtitleSectionInYourSector = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'directory_section_subtitle_Lebanese worldwide working in your industry_In Your Sector\']')
String parentClassNameInYourSector = 'parent-class_In'
String titleListClassInYourSector = 'title_directory_In'

//with your Last name
TestObject titleSectionLastName = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'directory_section_title_With Your Last Name\']')
TestObject subtitleSectionLastName = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'directory_section_subtitle_Lebanese worldwide having your family name_With Your Last Name\']')
String parentClassNameLastName = 'parent-class_With'
String titleListClassLastName = 'title_directory_With'

//searchEntity page
TestObject searchEntityBackButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'search-entity-page-back-button\']')
//check section
TestObject checkSectionDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'directory_section_title_Featured Professionals\']')

//Start Testing
AuthHelpers.generalRegistration(originCity, residenceCity, firstName, lastName, email)
addProfileSector()
scrollToBottom(checkSectionDiv)
checkListSectionInPeople(titleSectionPeople, titleSectionPeople, subtitleSectionPeople, parentClassNamePeople, titleListClassPeople)
checkExplorRoleSection(searchEntityBackButton)
checkListSectionInPeople(subtitleSectionAmbassadors, titleSectionAmbassadors, subtitleSectionAmbassadors, parentClassNameAmbassadors, 
    titleListClassAmbassadors)
checkLeaderShipShips(searchEntityBackButton)
checkListSectionInPeople(titleSectionHonorary, titleSectionHonorary, subtitleSectionHonorary, parentClassNameHonorary, titleListClassHonorary)
checkProfessionalBySector(searchEntityBackButton)
checkListSectionInPeople(titleSectionInYourSector, titleSectionInYourSector, subtitleSectionInYourSector, parentClassNameInYourSector, 
    titleListClassInYourSector)
checkListSectionInPeople(titleSectionFeatured, titleSectionFeatured, subtitleSectionFeatured, parentClassNameFeatured, titleListClassFeatured)
checkWithLastNameSectionsInPeople(titleSectionLastName, subtitleSectionLastName, parentClassNameLastName, titleListClassLastName, 
    checkSectionDiv)
checkTitleIcon()
WebUI.closeBrowser()

//Functions
def checkListSectionInPeople(scrollTo, titleSection, subtitleSection, parentClassName, titleListClass) {
    WebUI.scrollToElement(scrollTo, 5)
    TestHelper.isElementPresent(titleSection)
    String parentTitle = WebUI.getText(titleSection)
    TestHelper.isElementPresent(subtitleSection)
    int itemCount = TestHelper.countElementByClassName(parentClassName)
    if (itemCount < 1) {
        TestHelper.thrownException("number of item is 0 ")
    }
    WebElement itemTitleDiv = TestHelper.getItem(titleListClass, 2)
    String itemTitleName = itemTitleDiv.text
    println(itemTitleName)
    TestObject followitemAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, ((('//div[@id=\'right_action_id_directory_' + 
        parentTitle) + '_') + itemTitleName) + '\']')
    TestHelper.clickButton(followitemAction)
    WebUI.delay(7)
    WebUI.click(TestHelper.getObjectById('//div[@id=\'profile_tab_id\']'))
    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'Manage_User_entity_action\']'))
    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'Following and Followed User_manage_entity_action\']'))
    TestHelper.clickButton(TestHelper.getObjectById('//ion-segment-button[@id=\'followings_tab_id\']'))
    TestObject SearchFollowingInput = TestHelper.getObjectById('//input[@id=\'manage_following_search\']')
    TestHelper.setInputValue(SearchFollowingInput, itemTitleName)
    TestObject followPeopleDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, (('//div[@id=\'' + itemTitleName) + 
        '_profile') + '\']')
    TestHelper.isElementPresent(followPeopleDiv)
	if(!TestHelper.isElementPresent(followPeopleDiv)){
		TestHelper.thrownException("following user not succeeded")
	}

    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage_followers_back_id\']'))

    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage-User-back-button\']'))

    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'directory_tab_id\']'))
	
	println("Test " + parentTitle + " succeeded")
}

def checkExplorRoleSection(searchEntityBackButton) {
    TestHelper.isElementPresent(TestHelper.getObjectById('//div[@id=\'directory_section_title_Explore By Role\']'))

    TestHelper.isElementPresent(TestHelper.getObjectById('//div[@id=\'directory_section_subtitle_Looking for a mayor, a mukhtar, or a town ambassador to contact? Start here._Explore By Role\']'))

    TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'director_people_explore_button_Mayors\']'))

    TestHelper.isElementPresent(TestHelper.getObjectById('//button[@id=\'Mayors-search-entity-filter-type\']'))

    TestHelper.clickButton(searchEntityBackButton)

    TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'director_people_explore_button_Community Ambassadors\']'))

    TestHelper.isElementPresent(TestHelper.getObjectById('//button[@id=\'Community Ambassadors-search-entity-filter-type\']'))

    TestHelper.clickButton(searchEntityBackButton)

    TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'director_people_explore_button_Mukhtars\']'))

    TestHelper.isElementPresent(TestHelper.getObjectById('//button[@id=\'Mukhtars-search-entity-filter-type\']'))

    TestHelper.clickButton(searchEntityBackButton)

    TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'director_people_explore_button_Elected Members\']'))

    TestHelper.isElementPresent(TestHelper.getObjectById('//button[@id=\'Elected Members-search-entity-filter-type\']'))

    TestHelper.clickButton(searchEntityBackButton)
	
	println("Test explore by role succeeded")
}

def checkLeaderShipShips(searchEntityBackButton) {
    TestHelper.isElementPresent(TestHelper.getObjectById('//div[@id=\'directory_section_title_Leadership Profiles\']'))

    TestHelper.isElementPresent(TestHelper.getObjectById('//div[@id=\'directory_section_subtitle_Browse profiles by their group on the platform_Leadership Profiles\']'))

    WebUI.delay(2)

    TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'director_people_explore_button_Diplomats & Diplomatic Staff\']'))

    TestHelper.isElementPresent(TestHelper.getObjectById('//button[@id=\'Diplomats & Diplomatic Staff-search-entity-filter-type\']'))

    TestHelper.clickButton(searchEntityBackButton)

    TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'director_people_explore_button_Organization Leadership\']'))

    TestHelper.isElementPresent(TestHelper.getObjectById('//button[@id=\'Organization Leadership-search-entity-filter-type\']'))

    TestHelper.clickButton(searchEntityBackButton)

    TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'director_people_explore_button_Company Leadership\']'))

    TestHelper.isElementPresent(TestHelper.getObjectById('//button[@id=\'Company Leadership-search-entity-filter-type\']'))

    TestHelper.clickButton(searchEntityBackButton)

    TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'director_people_explore_button_Mentors\']'))

    TestHelper.isElementPresent(TestHelper.getObjectById('//button[@id=\'Mentors-search-entity-filter-type\']'))

    TestHelper.clickButton(searchEntityBackButton)

    TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'director_people_explore_button_Advisors\']'))

    TestHelper.isElementPresent(TestHelper.getObjectById('//button[@id=\'Advisors-search-entity-filter-type\']'))

    TestHelper.clickButton(searchEntityBackButton)
	
	println("Test check leader ship succeeded")
}

def checkProfessionalBySector(searchEntityBackButton) {
    TestHelper.isElementPresent(TestHelper.getObjectById('//div[@id=\'directory_section_title_Professionals By Sector\']'))

    TestHelper.isElementPresent(TestHelper.getObjectById('//div[@id=\'directory_section_subtitle_Find lebanese professionals by industry_Professionals By Sector\']'))

    TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'director_people_explore_button_Apparel & Fashion\']'))

    TestHelper.isElementPresent(TestHelper.getObjectById('//button[@id=\'Apparel & Fashion-search-entity-filter-type\']'))

    TestHelper.clickButton(searchEntityBackButton)

    TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'director_people_explore_button_Consumer Goods\']'))

    TestHelper.isElementPresent(TestHelper.getObjectById('//button[@id=\'Consumer Goods-search-entity-filter-type\']'))

    TestHelper.clickButton(searchEntityBackButton)

    TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'director_people_explore_button_Hospitality\']'))

    TestHelper.isElementPresent(TestHelper.getObjectById('//button[@id=\'Hospitality-search-entity-filter-type\']'))

    TestHelper.clickButton(searchEntityBackButton)

    TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'director_people_explore_button_Real Estate\']'))

    TestHelper.isElementPresent(TestHelper.getObjectById('//button[@id=\'Real Estate-search-entity-filter-type\']'))

    TestHelper.clickButton(searchEntityBackButton)

    TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'director_people_explore_button_Information Technology and Services\']'))

    TestHelper.isElementPresent(TestHelper.getObjectById('//button[@id=\'Information Technology and Services-search-entity-filter-type\']'))

    TestHelper.clickButton(searchEntityBackButton)
	
	println("Test prfessional by sector succeeded")
}

def scrollToBottom(def checkSectionDiv) {
    boolean checkSection = false
    while (!(checkSection)) {
        WebUI.delay(1)

        WebUI.click(TestHelper.getObjectById('//button[@id=\'directory-people-scroll-to-bottom\']'))

        checkSection = TestHelper.isElementPresent(checkSectionDiv)
    }
}

def addProfileSector() {
    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'profile_tab_id\']'))

    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'Manage_User_entity_action\']'))

    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'Basic Information_manage_entity_action\']'))

    TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'sector-section\']'))

    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'Agriculture, Forestry and Fishing_sector\']'))

    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'Farming_sector\']'))

    TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'save_user_info\']'))

    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage-User-back-button\']'))

    WebUI.delay(5)

    TestHelper.clickButton(TestHelper.getObjectById('//ion-tab-button[@id=\'tab-button-directory\']'))
	
	println("Test add profile sector succeeded")
}

def changeLastName() {
    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'profile_tab_id\']'))

    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'Manage_User_entity_action\']'))

    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'Basic Information_manage_entity_action\']'))

    TestHelper.clickButton(TestHelper.getObjectById('//input[@id=\'last-name-id\']'))

    TestHelper.setInputValue(TestHelper.getObjectById('//input[@id=\'last-name-id\']'), 'khoury')

    TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'save_user_info\']'))

    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage-User-back-button\']'))

    TestHelper.clickButton(TestHelper.getObjectById('//ion-tab-button[@id=\'tab-button-directory\']'))
}

def checkWithLastNameSectionsInPeople(titleSectionLastName, subtitleSectionLastName, parentClassNameLastName, 
									  titleListClassLastName, checkSectionDiv) 
{
    boolean LastNameSectionInPeople = TestHelper.isElementPresent(titleSectionLastName)

    if (LastNameSectionInPeople) {
        checkListSectionInPeople(titleSectionLastName, titleSectionLastName, subtitleSectionLastName, parentClassNameLastName, 
            titleListClassLastName)
    } else {
        changeLastName()

        scrollToBottom(checkSectionDiv)
		
		WebUI.click(TestHelper.getObjectById('//button[@id=\'directory-people-scroll-to-bottom\']'))

        checkListSectionInPeople(titleSectionLastName, titleSectionLastName, subtitleSectionLastName, parentClassNameLastName, 
            titleListClassLastName)
    }
}


def checkTitleIcon() {
	TestObject people_toFollow__icon_object = TestHelper.getObjectById("//div[@id='directory_section_title_People To Follow_icon']")
	String peopleToFollowIcon = WebUI.getText(people_toFollow__icon_object);
	if(!peopleToFollowIcon.equals("assets/icons/diasporaIcon/People_Active.svg")) {
		TestHelper.thrownException("people to follow title icon is wrong")
	}
	
	TestObject ambassador_consuls_icon_object = TestHelper.getObjectById("//div[@id='directory_section_title_Ambassadors & Consuls_icon']")
	String AmbassadorConsulsIcon = WebUI.getText(ambassador_consuls_icon_object);
	if(!AmbassadorConsulsIcon.equals("assets/icons/diasporaIcon/Badge_VIP.svg")) {
		TestHelper.thrownException("ambassador and consuls title icon is wrong")
	}
	
	TestObject honorary_consuls_icon_object = TestHelper.getObjectById("//div[@id='directory_section_title_Honorary Consuls_icon']")
	String honoraryConsulsIcon = WebUI.getText(honorary_consuls_icon_object);
	if(!honoraryConsulsIcon.equals("assets/icons/diasporaIcon/Badge_VIP.svg")) {
		TestHelper.thrownException("honrary and consuls title icon is wrong")
	}
	
	TestObject featured_professional_icon_object = TestHelper.getObjectById("//div[@id='directory_section_title_Featured Professionals_icon']")
	String feautredProfessionalIcon = WebUI.getText(featured_professional_icon_object);
	if(!feautredProfessionalIcon.equals("assets/icons/diasporaIcon/Freelancer.svg")) {
		TestHelper.thrownException("featured professional title icon is wrong")
	}
	
	TestObject with_lastName_icon_object = TestHelper.getObjectById("//div[@id='directory_section_title_With Your Last Name_icon']")
	String withLastNameIcon = WebUI.getText(with_lastName_icon_object);
	if(!withLastNameIcon.equals("assets/icons/diasporaIcon/family.svg")) {
		TestHelper.thrownException("with last name title icon is wrong")
	}
}

