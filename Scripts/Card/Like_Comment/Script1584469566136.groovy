import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement

long now = new Date().getTime();
String postTitle = 'New post title add now_' + now;
String postDescription = 'New post description add now_' + now;

// New User
String registeredEmail = 'netways_user' + now + '@gmail.com'
String password = 'cvW8qx4B2o3F4VwP/kNsqA=='

String firstName = 'FirstName_' + now;
String lastName = 'LastName_' + now;
String fullName = firstName + ' ' + lastName;
String city = 'Hermel';
String roleName = 'Founder';


WebUI.callTestCase(findTestCase('Company/Founder or admin/Authorization for company/Login_For_Company'), [:], FailureHandling.STOP_ON_FAILURE)

String companyUrl = WebUI.getUrl();
WebElement companyNameObject = TestHelper.getItem('company-name-title', 0)
String companyName = companyNameObject.text;

TestHelper.getObjectById("//div[@id='company_updates']")
  
TestObject emptyStateCompanyUpdates = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-company-updates-empty-state']")
boolean emptyStateCompanyUpdatesExist = TestHelper.isElementPresent(emptyStateCompanyUpdates)

  
if (emptyStateCompanyUpdatesExist) {
	println("Hasn't udpates");
	 WebUI.click(emptyStateCompanyUpdates)
	
	TestObject create_news_action_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Create Post_user_action']")
	WebUI.click(create_news_action_button)
	 
	UserActionsHelper.addPost(postTitle, postDescription)
	TestHelper.verifyCardExist(companyName, postTitle, 'company', false);
	 
	// like post added
	String commentText = 'Comment added now';
	
	// like post added
	addNewCommentToPost(postTitle, registeredEmail, password, firstName, lastName, city, companyUrl, companyName, commentText);
	
	//
	likeNewPostAdded(postTitle, companyUrl, commentText)
} else {
	println("Has udpates");

	WebUI.navigateToUrl(TestHelper.getHomeUrl());
	
	WebUI.delay(1);

	TestObject home_tab = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='home_tab_id']")
	WebUI.click(home_tab)
	
	TestObject user_action_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//img[@id='user_action_button']")
	WebUI.click(user_action_button)
	
	TestObject create_news_action_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Create Post_user_action']")
	WebUI.click(create_news_action_button)

	TestObject changeTargetAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='change-target-id']")
	WebUI.click(changeTargetAction)
 
	TestObject selectedTarget = TestHelper.getObjectById("//div[@id='" +companyName+ "_target']")
	WebUI.click(selectedTarget)
 
	UserActionsHelper.addPost(postTitle, postDescription)
	WebUI.delay(3);
	TestObject profile_tab = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='profile_tab_id']")
	WebUI.waitForElementPresent(profile_tab, 5)
	
	WebUI.navigateToUrl(companyUrl);
	TestHelper.verifyCardExist(companyName, postTitle, 'company', false);
	
	String commentText = 'Comment added now';
	
	// like post added
	addNewCommentToPost(postTitle, registeredEmail, password, firstName, lastName, city, companyUrl, companyName, commentText);
	
	//
	likeNewPostAdded(postTitle, companyUrl, commentText)
}
 
 
def addNewCommentToPost(String postTitle, String registeredEmail, String password, String firstName, String lastName, String city, String companyUrl, String selectedCompanyName, String commentText) {
	WebUI.navigateToUrl(TestHelper.getHomeUrl());
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manageProfileId']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='logout-id']"));	
	TestHelper.registerNewUser(registeredEmail, password, firstName, lastName, city, false);
	
	WebUI.navigateToUrl(companyUrl);
	
	TestHelper.getObjectById("//div[@id='company_updates']")
	TestHelper.clickButton(TestHelper.getObjectById( "//div[@id='" + postTitle + "_id']"));

		
	TestObject cardCommentInputObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//textarea[@id='commentInput']")
	TestObject cardCommentMessageActionObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='send_message_action']")
	TestHelper.setInputValue(cardCommentInputObject, commentText);
	TestHelper.clickButton(cardCommentMessageActionObject);

	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='card_details_back_button']"));
	
	WebUI.navigateToUrl(TestHelper.getHomeUrl());
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manageProfileId']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='logout-id']"));
}

def likeNewPostAdded(String postTitle, String companyUrl, String commentText) {
	TestHelper.loginWithAutomationTestingUser();
	WebUI.navigateToUrl(companyUrl);

	TestHelper.getObjectById("//div[@id='company_updates']")
	TestHelper.clickButton(TestHelper.getObjectById( "//div[@id='" + postTitle + "_id']"));

	String numberOfLikesString =  WebUI.getText(TestHelper.getObjectById("//span[@id='card_comment_like_count']"));
	String[] numberOfLikesArray = numberOfLikesString.split(" ");
	int numberOfLikes = Integer.parseInt(numberOfLikesArray[0])

	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='"+commentText+"_card_comment_action']"))
		
	String newNumberOfLikesString =  WebUI.getText(TestHelper.getObjectById("//span[@id='card_comment_like_count']"));
	String[] newNumberOfLikesArray = newNumberOfLikesString.split(" ");
	int newNumberOfLikes = Integer.parseInt(newNumberOfLikesArray[0])

	if(newNumberOfLikes != numberOfLikes + 1) {
		TestHelper.thrownException("Comment not liked successfully");
	} else {
	
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='"+commentText+"_card_comment_action']"))
	
		String newNumberOfUnlikesString =  WebUI.getText(TestHelper.getObjectById("//span[@id='card_comment_like_count']"));
		String[] newNumberOfUnlikesArray = newNumberOfUnlikesString.split(" ");
		int newNumberOfUnlikes = Integer.parseInt(newNumberOfUnlikesArray[0])
	
		if(newNumberOfUnlikes != newNumberOfLikes - 1) {
			TestHelper.thrownException("Comment not unliked successfully");
		} else {
			print("Test Complete");
			WebUI.closeBrowser();
		}
	}
}
