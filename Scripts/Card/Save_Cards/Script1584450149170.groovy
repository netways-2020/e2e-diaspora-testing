import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement

WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebElement profileNameObject = TestHelper.getItem('profile-name-title', 0)
String profileName = profileNameObject.text;

TestHelper.getObjectById("//div[@id='user_updates']")
  
TestObject emptyStateUserUpdates = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-updates-empty-state']")
boolean emptyStateUserUpdatesExist = TestHelper.isElementPresent(emptyStateUserUpdates)

long now = new Date().getTime();
String postTitle = 'New post title add now_' + now;
String postDescription = 'New post description add now_' + now;

if (emptyStateUserUpdatesExist) {
	println("Hasn't udpates");
	WebUI.click(emptyStateUserUpdates)
	
	UserActionsHelper.addPost(postTitle, postDescription);
	TestHelper.verifyCardExist(profileName, postTitle, 'profile', true);	
	
	// Save card
	saveCard(postTitle, postDescription);
	checkIfCardInFavorideCards(profileName, postTitle)
	
} else {
	println("Has udpates");

	TestObject home_tab = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='home_tab_id']")
	WebUI.click(home_tab)
	
	TestObject user_action_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//img[@id='user_action_button']")
	WebUI.click(user_action_button)
	
	TestObject create_post_action_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Create Post_user_action']")
	WebUI.click(create_post_action_button)

	UserActionsHelper.addPost(postTitle, postDescription);
	WebUI.delay(5);
	TestObject profile_tab = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='profile_tab_id']")
	WebUI.delay(3)	
	TestHelper.clickButton(profile_tab)
 	
	TestObject userUpdates = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='user_updates']")
	 
	WebUI.waitForElementPresent(userUpdates, 3);
	TestHelper.verifyCardExist(profileName, postTitle, 'profile', true);
	
	// Save card
	saveCard(postTitle, postDescription);
	checkIfCardInFavorideCards(profileName, postTitle)
}

def saveCard(String postTitle, String postDescription) {
	TestObject manageCardObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='profile_manage_updates']")
	boolean manageCardObjectExist = TestHelper.isElementPresent(manageCardObject);
	if(!manageCardObjectExist) {
		TestHelper.thrownException("Manage card button not exist");
	}
	
	TestHelper.clickButton(manageCardObject);
	String cartId = postTitle + '_descriptionId';
	TestHelper.clickButton(TestHelper.getObjectById("//span[@id='" + cartId + "']"));
	
	TestHelper.savePostCard();
}

def checkIfCardInFavorideCards(String profileName, String postTitle) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='card_details_back_button']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back_button_cardListing']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_manage_accounts']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Saved Posts']"))
	
	TestHelper.verifyCardExist(profileName, postTitle, null, false);
			
	String cartId = postTitle + '_saved_card_id';
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='" + cartId + "']"));

	TestHelper.unSavePostCard();
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='card_details_back_button']"))
	
	TestObject unsavedCard = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + cartId + "']")	
	boolean unsavedCardExist = TestHelper.isElementPresent(unsavedCard);
	
	if(unsavedCardExist) {
		TestHelper.thrownException("Card still exist in the list");
	} else {
		println("Test Complete");
		WebUI.closeBrowser();
	}
	
}

