import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import org.eclipse.persistence.internal.jpa.parsing.jpql.antlr.JPQLParser.deleteClause_scope
import org.openqa.selenium.WebElement as WebElement

long now = new Date().getTime();
String needTitle = 'new product title ' + now;
String needDescription = 'new product description ' + now;


// New User
long date = new Date().getTime();
String registeredEmail = 'netways_user' + date + '@gmail.com'
String password = 'cvW8qx4B2o3F4VwP/kNsqA=='

String firstName = 'FirstName_' + date;
String lastName = 'LastName_' + date;
String fullName = firstName + ' ' + lastName;
String city = 'Hermel';
String roleName = 'Founder';


// Login and go to my profile tab
WebUI.callTestCase(findTestCase('Company/Founder or admin/Authorization for company/Login_For_Company'), [:], FailureHandling.STOP_ON_FAILURE)
String companyUrl = WebUI.getUrl();
WebElement companyNameObject = TestHelper.getItem('company-name-title', 0)
String selectedCompanyName = companyNameObject.text;

addProduct(needTitle, needDescription, companyUrl, selectedCompanyName);

// Check Product Created Successfully
checkNewCardIsSuccessfullyCreated(needTitle, needDescription);

// Contact Seller
//String companyUrl = 'http://localhost:8100/app/company/6fc0dbe6-67fc-4109-b05a-fd33c687bd1b';
//String selectedCompanyName = 'Netways_1584366030575';
contactSeller(needTitle, registeredEmail, password, firstName, lastName, city, companyUrl, selectedCompanyName)


def addProduct(String needTitle, String needDescription, String companyUrl, String selectedCompanyName) {
	TestHelper.goToCompanyManageEntity();
	TestObject manageMarketPlaceAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Marketplace Listings_manage_entity_action']")
	TestHelper.clickButton(manageMarketPlaceAction)
	
	TestObject emptyStateAddProduct = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='market_place_add_product_action']")
	boolean emptyStateAddProductExist = TestHelper.isElementPresent(emptyStateAddProduct)

	if (emptyStateAddProductExist) {
		TestHelper.clickButton(emptyStateAddProduct);
		UserActionsHelper.addNeedOrOffer(needTitle, needDescription)
		 
		TestObject manageMarketPlaceBackButton = TestHelper.getObjectById("//div[@id='marketPlace_back_button']")
		WebUI.waitForElementVisible(manageMarketPlaceBackButton, 10)
		 			
	} else {
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='marketPlace_back_button']"));
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-Company-back-button']"));
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='company_back_button']"));
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_business_role_back_action']"));
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-User-back-button']"));
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='home_tab_id']"));
		WebUI.delay(1);
	
		// Add new product
		TestObject user_action_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//img[@id='user_action_button']")
		WebUI.click(user_action_button)
		
		TestObject add_product_action_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='List a Product_user_action']")
		WebUI.click(add_product_action_button)
	
		TestObject changeTargetAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='change-target-id']")
		WebUI.click(changeTargetAction)
		
		TestObject selectedTarget = TestHelper.getObjectById("//div[@id='" +selectedCompanyName+ "_target']");
		WebUI.click(selectedTarget)
	
		UserActionsHelper.addNeedOrOffer(needTitle, needDescription)
		
		TestObject manageProfilSection = TestHelper.getObjectById("//div[@id='manageProfileId']")
		WebUI.waitForElementVisible(manageProfilSection, 10)
		
		WebUI.navigateToUrl(companyUrl);
		
		WebUI.delay(1);
		TestHelper.goToCompanyManageEntity();
		TestHelper.clickButton(manageMarketPlaceAction);
		TestObject manageMarketPlaceBackButton = TestHelper.getObjectById("//div[@id='marketPlace_back_button']")
		WebUI.waitForElementVisible(manageMarketPlaceBackButton, 10)		
	}
}

def checkNewCardIsSuccessfullyCreated(String needTitle, String needDescription) { 
	String newItemId = needTitle + '_product';
	TestObject productSearchInputObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='market_place_product_search_input']")
	TestHelper.setInputValue(productSearchInputObject, needTitle)
	WebUI.delay(2);
	 
	TestObject newItemObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='" + newItemId +  "']")
	boolean newItemObjectExist = TestHelper.isElementPresent(newItemObject)
	 
	TestObject leftTagObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='secondLeftTag']")
	boolean leftTagObjectExist = TestHelper.isElementPresent(leftTagObject)
	 
	if (!newItemObjectExist || !leftTagObjectExist) {
		TestHelper.thrownException("Product item details not added successfully");
	} 
}

def contactSeller(String needTitle, String registeredEmail, String password, String firstName, String lastName, String city, String companyUrl, String selectedCompanyName) {
	WebUI.navigateToUrl(TestHelper.getHomeUrl());
//	WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Login'), [:], FailureHandling.STOP_ON_FAILURE)
//	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='home_tab_id']"))
	
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manageProfileId']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='logout-id']"));

	TestHelper.registerNewUser(registeredEmail, password, firstName, lastName, city, false);

	WebUI.navigateToUrl(companyUrl);
	WebUI.delay(1);
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='company-products-section']"))
	
	String productId = needTitle + '_product';
	TestObject productIdtemObject = TestHelper.getObjectById( "//span[@id='" + productId +  "']");
	boolean productIdtemObjectExist = TestHelper.isElementPresent(productIdtemObject)
		  
	if (productIdtemObjectExist) {
		println("newItemId: " + productId);
		TestHelper.clickButton(productIdtemObject);		
		WebUI.delay(1);
//		TestHelper.clickButton(productIdtemObject);
		
		TestHelper.getObjectById("//div[@id='contact-owner-section']")
		TestHelper.clickButton(TestHelper.getObjectById( "//button[@id='" + needTitle + "_contact_seller_action']"));
		String message = firstName + ' ask for your product ' + needTitle
		String fullName = firstName + ' ' + lastName;
		
		sendMessage(message);
		checkMessage(fullName, selectedCompanyName, message);
		
	} else {
		TestHelper.thrownException("Product not exist");
	}
}
  
def sendMessage(String message) {
	TestObject messageInput = TestHelper.getObjectById("//textarea[@id='message']")
	TestHelper.setInputValue(messageInput, message)
	TestHelper.clickButton(messageInput)
	WebUI.delay(1);
	TestObject sendMessageAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='send_meesage_to_company_owner_action']")
	TestHelper.clickButton(sendMessageAction)
	WebUI.delay(3);
}

def checkMessage(String senderName, String selectedCompanyName,  String message) {
	WebUI.navigateToUrl(TestHelper.getHomeUrl());
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manageProfileId']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='logout-id']"));
	TestHelper.loginWithAutomationTestingUser();
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='message_tab_id']"))
	
	changeTarget(selectedCompanyName);
	checkMessageExist(senderName, message);
	WebUI.closeBrowser();
}

def changeTarget(String targetName) {
	TestObject changeTargetAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='change-target-id']")
	WebUI.click(changeTargetAction)
	
	TestObject selectedTargetBadge = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + targetName + "_mailbox_badge']")
	boolean selectedTargetBadgePresent = TestHelper.isElementPresent(selectedTargetBadge)
	
	if(!selectedTargetBadgePresent) {
		TestHelper.thrownException("Select target badge not exist");
	}
	
	TestObject selectedTarget = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" +targetName+ "_mailbox_account']")
	WebUI.click(selectedTarget)
}

def checkMessageExist(String senderName, String message) {
	TestObject senderObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + senderName + "_other_user']")
	TestObject messageBadgeObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + message + "_other_user_badge']")
	TestObject messageContentObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + message + "_message_content']")

	boolean senderObjectPresent = TestHelper.isElementPresent(senderObject)
	boolean messageBadgeObjectPresent = TestHelper.isElementPresent(messageBadgeObject)
	boolean messageContentObjectPresent = TestHelper.isElementPresent(messageContentObject)
	
	if (!senderObjectPresent) {
		TestHelper.thrownException("Sender name not exist");
	}
	else if (!messageBadgeObject) {
		TestHelper.thrownException("Message badge not exist");
	}
	else if (!messageContentObject) {
		TestHelper.thrownException("Message content not exist");
	}
}
