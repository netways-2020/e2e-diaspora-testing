import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement

String parentCompanyName = 'Netways' + new Date().getTime()

String roleName = 'Founder'

WebUI.callTestCase(findTestCase('Company/Founder or admin/Authorization for company/Login_For_Company'), [:], FailureHandling.STOP_ON_FAILURE)

WebElement companyNameObject = TestHelper.getItem('company-name-title', 0)

String currentCompanyName = companyNameObject.text

String currentCompanyUrl = WebUI.getUrl()

TestHelper.goToCompanyManageEntity()

TestObject manageCompanyBranches = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Branches_manage_entity_action\']')

TestHelper.clickButton(manageCompanyBranches)

// Outgoing request
addCompanyParentAndCheckOutgoingRequest(parentCompanyName, roleName)

// Current request
addCompanyParentAndCheckCurrentRequest(parentCompanyName, roleName)

// Incomming request
acceptIncommingRequest(parentCompanyName, currentCompanyName)

rejectIncommingRequest(parentCompanyName, currentCompanyName, currentCompanyUrl) 

WebUI.delay(2);
WebUI.closeBrowser();

// Create Company
// Check outgoing request
// Check if created company exist in the outgoing request
// Check Outgoing Request	
// Withdraw Request
// Create Company
// Check current request
// Check Parent Company Exist
// Unlink From Parent Company
// Logout
// Remove parent branch
// Check Parent Company Exist
// Unlink From Parent Company
// Check if current company exist in the incomming request
// Accept Incomming Request
// Logout
// Check if current company exist in the incomming request
// Accept Incomming Request
// Create Company
// Check current request
// Check Parent Company Exist

def addCompanyParentAndCheckOutgoingRequest(String parentCompanyName, String roleName) {
    TestObject outgoingRequestTab = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//ion-segment-button[@id=\'manage_company_branch_outgoing_request\']')

    TestHelper.clickButton(outgoingRequestTab)

    WebUI.delay(1)

    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage_company_branch_add_parent_company_outgoing_empty_state\']'))

    UserRoleHelper.createCompany(parentCompanyName, roleName)

    WebUI.delay(2)

    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage-company-branches-back-button\']'))

    TestObject manageCompanyBranches = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Branches_manage_entity_action\']')

    TestHelper.clickButton(manageCompanyBranches)

    TestHelper.clickButton(outgoingRequestTab)

    WebUI.delay(1)

    String companyId = parentCompanyName + '_outgoing_request'

    TestObject companyIdObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + companyId) + '\']')

    boolean companyIdObjectExist = TestHelper.isElementPresent(companyIdObject)

    if (!(companyIdObjectExist)) {
        TestHelper.thrownException('Parent company not exist in the outgoing request')
    }
    
    String companyId_outgoingRequest = parentCompanyName + '_outgoing_request_action'

    TestObject outgoingRequestAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + companyId_outgoingRequest) + '\']')

	
	TestHelper.clickButton(outgoingRequestAction)
	TestHelper.clickButton(TestHelper.getObjectById('//a[@id=\'View Profile_user_action\']'))
		WebUI.delay(2)
	
	parentCompanyUrl = WebUI.getUrl(); 
	WebElement parentCompanyBackButton = TestHelper.getItem(parentCompanyName + '_company_back_button_action', 0)	
	parentCompanyBackButton.click();
	
	WebUI.delay(1)
	
	TestHelper.clickButton(outgoingRequestAction)
    TestHelper.clickButton(TestHelper.getObjectById('//a[@id=\'Withdraw Request_user_action\']'))
	
    WebUI.delay(2)

    companyIdObjectExist = TestHelper.isElementPresent(companyIdObject)

    if (companyIdObjectExist) {
        TestHelper.thrownException('Erro withdraw request: Parent company still exist in the outgoing request')
    }
}

def addCompanyParentAndCheckCurrentRequest(String parentCompanyName, String roleName) {
    TestObject currentRequestTab = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//ion-segment-button[@id=\'manage_company_branch_current_request\']')

    TestHelper.clickButton(currentRequestTab)

    WebUI.delay(1)

    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage_company_branch_add_parent_company_current_empty_state\']'))

    UserRoleHelper.searchForCompanyAndClickAction(parentCompanyName)

    WebUI.delay(2)

    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage-company-branches-back-button\']'))

    TestObject manageCompanyBranches = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Branches_manage_entity_action\']')

    TestHelper.clickButton(manageCompanyBranches)

    TestHelper.clickButton(currentRequestTab)

    WebUI.delay(1)

    TestObject parentObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + parentCompanyName) + 
        '_company_tree\']')

    boolean parentObjectExist = TestHelper.isElementPresent(parentObject)

    if (parentObjectExist) {
        println('Parent branch exit')

        TestObject parentUnlinkObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + parentCompanyName) + 
            '_company_tree_action\']')

        boolean parentUnlinkObjectExist = TestHelper.isElementPresent(parentUnlinkObject)

        if (parentUnlinkObjectExist) {
            TestHelper.clickButton(parentUnlinkObject)

            WebElement unlinkFromParentConfirmation = TestHelper.getItem('unlink_parent_company', 0)

            unlinkFromParentConfirmation.click()

            WebUI.delay(2)

            parentObjectExist = TestHelper.isElementPresent(parentObject)

            if (parentObjectExist) {
                TestHelper.thrownException('Error during unlink parent company')
            }
        } else {
            TestHelper.thrownException('Unlink parent action not exist')
        }
    } else {
        TestHelper.thrownException('Parent company not exist in the outgoing request')
    }
}

def acceptIncommingRequest(String parentCompanyName, String currentCompany) {
    WebUI.delay(1)

    linkToParentCompany(parentCompanyName, currentCompany)

    checkAcceptIncommingRequest(currentCompany)
}

def rejectIncommingRequest(String parentCompanyName, String currentCompany, String currentCompanyUrl) {
    WebUI.navigateToUrl(TestHelper.getHomeUrl())

    TestObject manageProfilSection = TestHelper.getObjectById('//div[@id=\'manageProfileId\']')

    TestHelper.clickButton(manageProfilSection)

    TestObject logoutAction = TestHelper.getObjectById('//div[@id=\'logout-id\']')

    TestHelper.clickButton(logoutAction)

    TestHelper.loginWithAutomationTestingUser()

    WebUI.delay(2)

    WebUI.navigateToUrl(currentCompanyUrl)

    WebUI.delay(1)

    TestHelper.goToCompanyManageEntity()

    TestObject manageCompanyBranches = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Branches_manage_entity_action\']')

    TestHelper.clickButton(manageCompanyBranches)

    TestObject currentRequestTab = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//ion-segment-button[@id=\'manage_company_branch_current_request\']')

    TestHelper.clickButton(currentRequestTab)

    WebUI.delay(1)

    TestObject parentObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + parentCompanyName) + 
        '_company_tree\']')

    boolean parentObjectExist = TestHelper.isElementPresent(parentObject)

    if (parentObjectExist) {
        println('Parent branch exit')

        TestObject parentUnlinkObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + parentCompanyName) + 
            '_company_tree_action\']')

        boolean parentUnlinkObjectExist = TestHelper.isElementPresent(parentUnlinkObject)

        if (parentUnlinkObjectExist) {
            TestHelper.clickButton(parentUnlinkObject)

            WebElement unlinkFromParentConfirmation = TestHelper.getItem('unlink_parent_company', 0)

            unlinkFromParentConfirmation.click()

            WebUI.delay(2)

            parentObjectExist = TestHelper.isElementPresent(parentObject)

            if (parentObjectExist) {
                TestHelper.thrownException('Error during unlink parent company')
            } else {
                linkToParentCompany(parentCompanyName, currentCompany)

                checkRejectedIncommingRequest(currentCompany)
            }
        } else {
            TestHelper.thrownException('Unlink parent action not exist')
        }
    } else {
        TestHelper.thrownException('Parent company not exist in the outgoing request')
    }
}

def checkAcceptIncommingRequest(String currentCompany) {
    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage-company-branches-back-button\']'))

    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage-Company-back-button\']'))

    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'company_back_button\']'))

    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage_business_role_back_action\']'))

    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage-User-back-button\']'))

    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'profile_manage_accounts\']'))

    TestObject logoutAction = TestHelper.getObjectById('//div[@id=\'logout-id\']')

    TestHelper.clickButton(logoutAction)

    TestHelper.loginWithAutomationTestingUser()

    WebUI.delay(2)

    WebUI.navigateToUrl(parentCompanyUrl)

    WebUI.delay(1)

    TestHelper.goToCompanyManageEntity()

    TestObject manageCompanyBranches = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Branches_manage_entity_action\']')

    TestHelper.clickButton(manageCompanyBranches)

    String companyId = currentCompany + '_incomming_request'

    TestObject companyIdObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + companyId) + 
        '\']')

    boolean companyIdObjectExist = TestHelper.isElementPresent(companyIdObject)

    if (!(companyIdObjectExist)) {
        TestHelper.thrownException('Current company not exist in the incomming request')
    }
    
    String companyId_incommingRequest = currentCompany + '_incomming_request_action'

    TestObject outgoingRequestAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + companyId_incommingRequest) + 
        '\']')

    TestHelper.clickButton(outgoingRequestAction)

    TestHelper.clickButton(TestHelper.getObjectById('//a[@id=\'Approve Request_user_action\']'))

    WebUI.delay(2)

    companyIdObjectExist = TestHelper.isElementPresent(companyIdObject)

    if (companyIdObjectExist) {
        TestHelper.thrownException('Error accept incomming request: current company still exist in the incomming request')
    }
    
    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage-company-branches-back-button\']'))

    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage-Company-back-button\']'))

    TestHelper.getObjectById('//div[@id=\'company_branches\']')

    String companyId_tree_list = currentCompany + '_branch_item_list'

    TestObject companyInTreeObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + companyId_tree_list) + 
        '\']')

    boolean companyInTreeObjectExist = TestHelper.isElementPresent(companyInTreeObject)

    if (companyInTreeObjectExist) {
        println('company exist in the tree')
    } else {
        TestHelper.thrownException('Error incomming request: current company not exist in the company tree')
    }
}

def checkRejectedIncommingRequest(String currentCompany) {
    WebUI.navigateToUrl(TestHelper.getHomeUrl())

    TestObject manageProfilSection = TestHelper.getObjectById('//div[@id=\'manageProfileId\']')

    TestHelper.clickButton(manageProfilSection)

    TestObject logoutAction = TestHelper.getObjectById('//div[@id=\'logout-id\']')

    TestHelper.clickButton(logoutAction)

    TestHelper.loginWithDiasporaUser()

    WebUI.delay(2)

    WebUI.navigateToUrl(parentCompanyUrl)

    WebUI.delay(1)

    TestHelper.goToCompanyManageEntity()

    TestObject manageCompanyBranches = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Branches_manage_entity_action\']')

    TestHelper.clickButton(manageCompanyBranches)

    String companyId = currentCompany + '_incomming_request'

    TestObject companyIdObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + companyId) + 
        '\']')

    boolean companyIdObjectExist = TestHelper.isElementPresent(companyIdObject)

    if (!(companyIdObjectExist)) {
        TestHelper.thrownException('Current company not exist in the incomming request')
    }
    
    String companyId_incommingRequest = currentCompany + '_incomming_request_action'

    TestObject outgoingRequestAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + companyId_incommingRequest) + 
        '\']')

    TestHelper.clickButton(outgoingRequestAction)

    TestHelper.clickButton(TestHelper.getObjectById('//a[@id=\'Reject Request_user_action\']'))

    WebUI.delay(2)

    companyIdObjectExist = TestHelper.isElementPresent(companyIdObject)

    if (companyIdObjectExist) {
        TestHelper.thrownException('Error accept incomming request: current company still exist in the incomming request')
    }
    
    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage-company-branches-back-button\']'))

    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage-Company-back-button\']'))

    TestHelper.getObjectById('//div[@id=\'company_branches\']')

    String companyId_tree_list = currentCompany + '_branch_item_list'

    TestObject companyInTreeObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + companyId_tree_list) + 
        '\']')

    boolean companyInTreeObjectExist = TestHelper.isElementPresent(companyInTreeObject)

    println('companyId_tree_list: ' + companyId_tree_list)

    if (!(companyInTreeObjectExist)) {
        println('company exist in the tree')
    } else {
        TestHelper.thrownException('Error incomming request: current company not exist in the company tree')
    }
}

def linkToParentCompany(String parentCompanyName, String currentCompany) {
    TestObject currentRequestTab = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//ion-segment-button[@id=\'manage_company_branch_current_request\']')

    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage_company_branch_add_parent_company_current_empty_state\']'))

    UserRoleHelper.searchForCompanyAndClickAction(parentCompanyName)

    WebUI.delay(2)

    TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage-company-branches-back-button\']'))

    TestObject manageCompanyBranches = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Branches_manage_entity_action\']')

    TestHelper.clickButton(manageCompanyBranches)

    TestHelper.clickButton(currentRequestTab)

    WebUI.delay(1)

    TestObject parentObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + parentCompanyName) + 
        '_company_tree\']')

    boolean parentObjectExist = TestHelper.isElementPresent(parentObject)

    if (parentObjectExist) {
        println('Parent branch exit')
    } else {
        TestHelper.thrownException('Parent company not exist in the outgoing request')
    }
}

 