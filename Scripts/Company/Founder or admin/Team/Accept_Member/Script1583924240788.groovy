import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static org.junit.Assert.*

import org.junit.Test

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.testobject.ConditionType
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement

long date = new Date().getTime();
String registeredEmail = 'netways_user' + date + '@gmail.com'
String password = 'cvW8qx4B2o3F4VwP/kNsqA=='

String firstName = 'FirstName_' + date;
String lastName = 'LastName_' + date;
String fullName = firstName + ' ' + lastName;
String city = 'Hermel';
String roleName = 'Founder';

// Login and get company url
WebUI.callTestCase(findTestCase('Company/Founder or admin/Authorization for company/Login_For_Company'), [:], FailureHandling.STOP_ON_FAILURE)
String companyUrl = WebUI.getUrl();


// Register new user
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='company_back_button']"))
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_business_role_back_action']"))
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-User-back-button']"))
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_manage_accounts']"))

TestObject logoutAction = TestHelper.getObjectById("//div[@id='logout-id']")
TestHelper.clickButton(logoutAction);
TestHelper.registerNewUser(registeredEmail, password, firstName, lastName, city, false);

// add company role
joinCompany(companyUrl, roleName);

// Check if exist in unverified team
verifyEmployee(companyUrl, fullName);

// remove Employee
removeEmployee(fullName);

WebUI.delay(2);
WebUI.closeBrowser();

def joinCompany(String companyUrl, String roleName) {
	WebUI.navigateToUrl(companyUrl);
	TestObject joinCompanyAction = TestHelper.getObjectById("//div[@id='Join Company_action']")
	boolean joinCompanyActionExist = TestHelper.isElementPresent(joinCompanyAction);
	
	if(joinCompanyActionExist) {
		TestHelper.clickButton(joinCompanyAction);
		UserRoleHelper.joinCompany(roleName);
		WebUI.delay(1); 
		WebUI.navigateToUrl(TestHelper.getHomeUrl());
		TestObject manageProfilSection = TestHelper.getObjectById("//div[@id='manageProfileId']")
		TestHelper.clickButton(manageProfilSection);
		
		TestObject logoutAction = TestHelper.getObjectById("//div[@id='logout-id']")
		TestHelper.clickButton(logoutAction); 
	} else {
		TestHelper.thrownException("User already has role in this company");
	} 
}

def verifyEmployee(String companyUrl, String memberName) {
	TestHelper.loginWithAutomationTestingUser();
	WebUI.navigateToUrl(companyUrl);
	TestHelper.goToCompanyManageEntity();
	TestObject manageTeamMemberAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Team Members_manage_entity_action']")
	TestHelper.clickButton(manageTeamMemberAction);
	
	TestObject pendingMemberSearchInput = TestHelper.getObjectById("//input[@id='company_pending_member']")
	TestHelper.setInputValue(pendingMemberSearchInput, memberName)
	WebUI.delay(2);
	
	String pendingEmployeeId = memberName + '_pending_employee_name';
	String pendingEmployeeActionId = memberName + '_pending_employee_action';
	
	String currentEmployeeId = memberName + '_current_employee_name';
	
	TestObject newMember = TestHelper.getObjectById("//div[@id='"+pendingEmployeeId+"']")
	
	boolean newMemberExist = TestHelper.isElementPresent(newMember); 
	if(newMemberExist) {
		TestObject pendingMemberAction = TestHelper.getObjectById("//div[@id='"+pendingEmployeeActionId+"']")
		TestHelper.clickButton(pendingMemberAction);
		
		TestObject pendingMemberApproveRequestAction = TestHelper.getObjectById("//a[@id='Approve Request_user_action']")
		TestHelper.clickButton(pendingMemberApproveRequestAction);
				
		TestObject currentEmployeeTab = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//ion-segment-button[@id='current_employee_tab_id']")
		TestHelper.clickButton(currentEmployeeTab);

		TestObject currentMemberSearchInput = TestHelper.getObjectById("//input[@id='company_current_member']")
		TestHelper.setInputValue(currentMemberSearchInput, memberName)
		WebUI.delay(2);
		
		TestObject currentMember = TestHelper.getObjectById("//div[@id='"+currentEmployeeId+"']")
		boolean currentMemberExist = TestHelper.isElementPresent(currentMember);
		if(currentMemberExist) {
			println('User has been accepted has employee successfully');
		} else {
			TestHelper.thrownException("Error during accepting new member");
		}
	} else {
		TestHelper.thrownException("No employee with this name exist");
	}	
}

def removeEmployee(String memberName) {
	String currentEmployeeId = memberName + '_current_employee_name';
	String currentEmployeeActionId = memberName + '_current_employee_action';
	TestObject currentEmployeeAction = TestHelper.getObjectById("//div[@id='"+currentEmployeeActionId+"']")
	TestHelper.clickButton(currentEmployeeAction);
	
	//Show Current Member Profile
	TestObject viewCurrentMemberAction = TestHelper.getObjectById("//a[@id='View Profile_user_action']")
	TestHelper.clickButton(viewCurrentMemberAction);
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_backIcon_id']"))
	
	// Remove Current Member
	TestHelper.clickButton(currentEmployeeAction);
	TestObject removeCurrentMemberAction = TestHelper.getObjectById("//a[@id='Remove Member_user_action']")
	TestHelper.clickButton(removeCurrentMemberAction);
	
	WebElement removeMemberConfirmation = TestHelper.getItem('reject-current-member', 0)
	removeMemberConfirmation.click(); 
	
	WebUI.delay(2);
	
	TestObject currentMember = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='"+currentEmployeeId+"']")
	boolean currentMemberExist = TestHelper.isElementPresent(currentMember);
	if(!currentMemberExist) {
		println('User has been removed successfully');
	} else {
		TestHelper.thrownException("Error during removing new member");
	}
}

