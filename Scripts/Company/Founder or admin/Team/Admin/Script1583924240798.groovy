import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement



long date = new Date().getTime();
String registeredEmail = 'netways_user' + date + '@gmail.com'
String password = 'cvW8qx4B2o3F4VwP/kNsqA=='

String firstName = 'FirstName_' + date;
String lastName = 'LastName_' + date;
String fullName = firstName + ' ' + lastName;
String city = 'Hermel';
String roleName = 'Founder';

// Login and get company url
WebUI.callTestCase(findTestCase('Company/Founder or admin/Authorization for company/Login_For_Company'), [:], FailureHandling.STOP_ON_FAILURE)
String companyUrl =  WebUI.getUrl();


// Register new user
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='company_back_button']"))
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_business_role_back_action']"))
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-User-back-button']"))
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_manage_accounts']"))

TestObject logoutAction = TestHelper.getObjectById("//div[@id='logout-id']")
TestHelper.clickButton(logoutAction);
TestHelper.registerNewUser(registeredEmail, password, firstName, lastName, city, false);

// add company role
joinCompany(companyUrl, roleName);

// assign admin
acceptMemberAsEmployee(companyUrl, fullName);
checkAdmin(fullName)

// Remove
removeAdmin(fullName)

WebUI.delay(2);
WebUI.closeBrowser();

def joinCompany(String companyUrl, String roleName) {
	WebUI.navigateToUrl(companyUrl);
	TestObject joinCompanyAction = TestHelper.getObjectById("//div[@id='Join Company_action']")
	boolean joinCompanyActionExist = TestHelper.isElementPresent(joinCompanyAction);
	
	if(joinCompanyActionExist) {
		TestHelper.clickButton(joinCompanyAction);
		UserRoleHelper.joinCompany(roleName);
		WebUI.delay(1);
		WebUI.navigateToUrl(TestHelper.getHomeUrl());
		TestObject manageProfilSection = TestHelper.getObjectById("//div[@id='manageProfileId']")
		TestHelper.clickButton(manageProfilSection);
		
		TestObject logoutAction = TestHelper.getObjectById("//div[@id='logout-id']")
		TestHelper.clickButton(logoutAction);
	} else {
		TestHelper.thrownException("User already has role in this company");
	}
}

def acceptMemberAsEmployee(String companyUrl, String memberName) {
	TestHelper.loginWithAutomationTestingUser();
	WebUI.navigateToUrl(companyUrl);
	TestHelper.goToCompanyManageEntity();
	TestObject manageTeamMemberAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Team Members_manage_entity_action']")
	TestHelper.clickButton(manageTeamMemberAction);
	
	TestObject pendingMemberSearchInput = TestHelper.getObjectById("//input[@id='company_pending_member']")
	TestHelper.setInputValue(pendingMemberSearchInput, memberName)
	WebUI.delay(2);
	
	String pendingEmployeeId = memberName + '_pending_employee_name';
	String pendingEmployeeActionId = memberName + '_pending_employee_action';
	
	
	TestObject newMember = TestHelper.getObjectById("//div[@id='"+pendingEmployeeId+"']")
	
	boolean newMemberExist = TestHelper.isElementPresent(newMember);
	if(newMemberExist) {
		TestObject pendingMemberAction = TestHelper.getObjectById("//div[@id='"+pendingEmployeeActionId+"']")
		TestHelper.clickButton(pendingMemberAction);
		
		TestObject pendingMemberApproveRequestAction = TestHelper.getObjectById("//a[@id='Approve Request_user_action']")
		TestHelper.clickButton(pendingMemberApproveRequestAction);
		
		WebUI.delay(2);
		
		
		TestObject manageCompanyMembersBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-company-member-back-button']")
		TestHelper.clickButton(manageCompanyMembersBackButton);

		assignAdminToCompany(companyUrl, memberName);
		
	} else {
		TestHelper.thrownException("No employee with this name exist");
	}
}

def assignAdminToCompany(String companyUrl, String memberName) {
	
	TestObject manageAdminMemberAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Page Administrators_manage_entity_action']")
	TestHelper.clickButton(manageAdminMemberAction);
	
	// Check empty state add admin
	TestObject manage_admin_add_admin = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-admin-company-add-new-admin-empty-state']")
	boolean checkManage_admin_admin = TestHelper.isElementPresent(manage_admin_add_admin);
	
	if(checkManage_admin_admin) {
		TestHelper.clickButton(manage_admin_add_admin)
		assignMemberAsAdmin(memberName);
	} else {
		TestObject manage_admin_add_admin_from_header = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-admin-company-add-new-admin']")
		TestHelper.clickButton(manage_admin_add_admin_from_header)	
		assignMemberAsAdmin(memberName);
	}
}

def assignMemberAsAdmin(String memberName) {
	TestObject assignAdminTab = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//ion-segment-button[@id='add-company-assign-admin-segment']")
	TestObject inviteAdminTab = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//ion-segment-button[@id='add-company-invite-admin-segment']")
	TestHelper.clickButton(inviteAdminTab)

// Invite Admin
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='invite_company_admin_first_name']"), 'Ali');
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='invite_company_admin_last_name']"), 'Blaybel');
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='invite_company_admin_email']"), 'blaybel.2010@gmail.com');
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='invite_company_admin_action']"));
	
// Assigne member as admin
	TestHelper.clickButton(assignAdminTab);
	WebUI.delay(2);
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='assign-admin-search-input']"), memberName);
	WebUI.delay(2);
	
	String memberId = memberName + '_assign_admin_role_action';
	TestObject memberAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + memberId + "']")
	TestHelper.clickButton(memberAction);

	// View Profile	
	TestObject viewCurrentMemberAction = TestHelper.getObjectById("//a[@id='View Profile_user_action']")
	TestHelper.clickButton(viewCurrentMemberAction);
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_backIcon_id']"))

	// Assign Admin
	TestHelper.clickButton(memberAction);
	TestObject assignAdminMemberAction = TestHelper.getObjectById("//a[@id='Assign As Administrator_user_action']")
	TestHelper.clickButton(assignAdminMemberAction);
	WebUI.delay(2);
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='add-admin-company-back-button']"))
}

def checkAdmin(String memberName) {
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='manage-company-admin-search-input']"), memberName);
	WebUI.delay(2);
	
	String memberId = memberName + '_admin_role';
	TestObject memberObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + memberId + "']")
	boolean checkAdminExist = TestHelper.isElementPresent(memberObject);
	
	if(checkAdminExist) {
		println("Memeber assign as admin succesfully"); 
	} else {
		TestHelper.thrownException("Error during assing member as admin");
	}
}

def removeAdmin(String memberName) {
	String memberId = memberName + '_admin_role_action';
	TestObject memberAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + memberId + "']")
	TestHelper.clickButton(memberAction);

	// View Profile	
	TestObject viewAdminMemberAction = TestHelper.getObjectById("//a[@id='View Profile_user_action']")
	TestHelper.clickButton(viewAdminMemberAction);
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_backIcon_id']"))

	// Assign Admin
	TestHelper.clickButton(memberAction);
	TestObject removeAdminMemberAction = TestHelper.getObjectById("//a[@id='Remove Administrator_user_action']")
	TestHelper.clickButton(removeAdminMemberAction);
	
	WebElement removeAdminConfirmation = TestHelper.getItem('remove-admin-member', 0)
	removeAdminConfirmation.click();

	WebUI.delay(2);
	
	String removedMemberId = memberName + '_admin_role';
	TestObject memberObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + removedMemberId + "']")
	boolean checkAdminExist = TestHelper.isElementPresent(memberObject);
	
	if(!checkAdminExist) {
		println("Admin has been removed succesfully");
	} else {
		TestHelper.thrownException("Error during remove admin");
	}
}

 