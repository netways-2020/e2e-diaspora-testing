import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement

long now = new Date().getTime();
String needTitle = 'new service title ' + now;
String needDescription = 'new service description ' + now;
String newTitle = needTitle + '_new';
String newDescription = needTitle + '_new';
 
// Login and go to my profile tab
WebUI.callTestCase(findTestCase('Company/Founder or admin/Authorization for company/Login_For_Company'), [:], FailureHandling.STOP_ON_FAILURE)
String companyUrl = WebUI.getUrl();
WebElement companyNameObject = TestHelper.getItem('company-name-title', 0)
String selectedCompanyName = companyNameObject.text;

TestHelper.goToCompanyManageEntity();
TestObject manageMarketPlaceAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Marketplace Listings_manage_entity_action']")
TestHelper.clickButton(manageMarketPlaceAction)


TestHelper.clickButton(TestHelper.getObjectById("//ion-segment-button[@id='market_place_service_tab']"))
TestObject emptyStateAddService = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='market_place_add_service_action']")
boolean emptyStateAddServicetExist = TestHelper.isElementPresent(emptyStateAddService)

 
if (emptyStateAddServicetExist) {	
	TestHelper.clickButton(emptyStateAddService);
	UserActionsHelper.addNeedOrOffer(needTitle, needDescription)
	 
	TestObject manageMarketPlaceBackButton = TestHelper.getObjectById("//div[@id='marketPlace_back_button']")
	WebUI.waitForElementVisible(manageMarketPlaceBackButton, 10)
	
	// Check Service Created Successfully
	checkNewCardIsSuccessfullyCreated(needTitle, needDescription);
	checkServiceSectionExist(needTitle);
	editService(needTitle, newTitle, newDescription)
	removeService(newTitle)

//	checkIfServiceAddedSuccessfully(true, 0);
} else { 
	int itemsCountBeforAdd = TestHelper.countElementByClassName('service_items');

	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='marketPlace_back_button']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-Company-back-button']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='company_back_button']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_business_role_back_action']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-User-back-button']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='home_tab_id']"));
	WebUI.delay(1);

	// Add new service
	TestObject user_action_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//img[@id='user_action_button']")
	WebUI.click(user_action_button)
	
	TestObject add_service_action_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='List a Service_user_action']")
	WebUI.click(add_service_action_button)

	TestObject changeTargetAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='change-target-id']")
	WebUI.click(changeTargetAction)
	
	TestObject selectedTarget = TestHelper.getObjectById("//div[@id='" +selectedCompanyName+ "_target']");
	WebUI.click(selectedTarget)
	
	UserActionsHelper.addNeedOrOffer(needTitle, needDescription)
	
	TestObject manageProfilSection = TestHelper.getObjectById("//div[@id='manageProfileId']")
	WebUI.waitForElementVisible(manageProfilSection, 10)
	WebUI.navigateToUrl(companyUrl);
	
	WebUI.delay(1);
	TestHelper.goToCompanyManageEntity();
	TestHelper.clickButton(manageMarketPlaceAction);
	TestObject manageMarketPlaceBackButton = TestHelper.getObjectById("//div[@id='marketPlace_back_button']")
	WebUI.waitForElementVisible(manageMarketPlaceBackButton, 10)
	
	// Check Service Created Successfully
	checkNewCardIsSuccessfullyCreated(needTitle, needDescription);
	checkServiceSectionExist(needTitle);
	editService(needTitle, newTitle, newDescription)
	removeService(newTitle)

//	checkIfServiceAddedSuccessfully(false, itemsCountBeforAdd);
}



def checkNewCardIsSuccessfullyCreated(String needTitle, String needDescription) {
	TestHelper.clickButton(TestHelper.getObjectById("//ion-segment-button[@id='market_place_service_tab']"))
	
	String newItemId = needTitle + '_service'; 
	TestObject serviceSearchInputObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='market_place_service_search_input']")
	TestHelper.setInputValue(serviceSearchInputObject, needTitle)
	WebUI.delay(2);
	
	TestObject newItemObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='" + newItemId +  "']")
	boolean newItemObjectExist = TestHelper.isElementPresent(newItemObject)
	 
	TestObject leftTagObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='secondLeftTag']")
	boolean leftTagObjectExist = TestHelper.isElementPresent(leftTagObject)
	 
	if (!newItemObjectExist || !leftTagObjectExist) {
		TestHelper.thrownException("Service item details not added successfully");
	}
}

def checkIfServiceAddedSuccessfully(boolean firstItemAdded, int itemCountBeforAdd) {
	int itemCount = TestHelper.countElementByClassName('service_items');
	
	if(firstItemAdded) {
		if(itemCount == 1) {
			println("Service item  added successfully");
		} else {
			TestHelper.thrownException("Service item not added successfully");
		}
	} else {
	   if((itemCountBeforAdd + 1) == itemCount) {
		   println("Service item added successfully");
	   } else {
		   TestHelper.thrownException("Service item not added successfully");
	   }
	}
}

def checkServiceSectionExist(String needTitle) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='marketPlace_back_button']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-Company-back-button']"));
	
	TestObject companyServicesSection = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='company-services-section']")
	boolean companyServicesSectionExist = TestHelper.isElementPresent(companyServicesSection)
	
	if (companyServicesSectionExist) {
		println("Service section exist");
		String newItemId = needTitle + '_service';
		 
		TestObject newItemObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='" + newItemId +  "']")
		boolean newItemObjectExist = TestHelper.isElementPresent(newItemObject)
		 
		TestObject leftTagObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='secondLeftTag']")
		boolean leftTagObjectExist = TestHelper.isElementPresent(leftTagObject)
		 
		if (!newItemObjectExist || !leftTagObjectExist) {
			TestHelper.thrownException("Service item details not added successfully");
		} else {
			println("Added service exist");
		}
	} else {
		TestHelper.thrownException("Service section not exist")
	}
}


def editService(String needTitle, String newNeedTitle, String newNeedDescritpion) {
	// Edit Service
	String newItemId = needTitle + '_service';
	TestObject editItemObject = TestHelper.getObjectById( "//span[@id='" + newItemId +  "']");
	boolean editItemObjectItemObjectExist = TestHelper.isElementPresent(editItemObject)
		  
	if (editItemObjectItemObjectExist) {
		println("newItemId: " + newItemId);
		TestHelper.clickButton(editItemObject);
		
		TestHelper.verifyOfferCardFeature('Service');
		TestHelper.verifyOfferCard(needTitle, 0, 0, true)
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='card_details_manage_button']"));
		TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Edit Post_user_action']"));
		
		UserActionsHelper.editNeedOrOffer(newNeedTitle, newNeedDescritpion);
		
		TestObject cardDetailsBackButton = TestHelper.getObjectById("//div[@id='card_details_back_button']")
		WebUI.waitForElementVisible(cardDetailsBackButton, 10)
 		
		int companyOfferCount = TestHelper.countElementByClassName('company-offer-countries');
		int companyKeywordCount = TestHelper.countElementByClassName('company-offer-keywords');
		
		TestHelper.verifyOfferCard(newNeedTitle, companyOfferCount, companyKeywordCount, false)
		TestHelper.clickButton(cardDetailsBackButton);
		
		// Check Service Created Successfully
		WebUI.delay(3);
		TestObject newItemObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='" + newNeedTitle + '_service' +  "']")		
		boolean newItemObjectExist = TestHelper.isElementPresent(newItemObject)
		 
		TestObject leftTagObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='secondLeftTag']")
		boolean leftTagObjectExist = TestHelper.isElementPresent(leftTagObject)

		String text = WebUI.getText(newItemObject)
		if (text.equals(newNeedTitle) && leftTagObjectExist) {
			println("Product edited exist");
		} else {
			TestHelper.thrownException("Product item edited successfully");
		}
	}
}

def removeService(String newNeedTitle) {
	
	// Remove Service
	String newItemId = newNeedTitle + '_service';
	TestObject removeItemObject = TestHelper.getObjectById( "//span[@id='" + newItemId +  "']");
	boolean removeItemObjectExist = TestHelper.isElementPresent(removeItemObject)
		  
	if (removeItemObjectExist) {
		TestHelper.clickButton(removeItemObject);

		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='card_details_manage_button']"));
		TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Delete Post_user_action']"));
		
		WebElement deleteImageConfirmation = TestHelper.getItem('remove-new-card', 0)
		deleteImageConfirmation.click();

		TestObject cardDetailsBackButton = TestHelper.getObjectById("//div[@id='company_menu_action_id']")
		WebUI.waitForElementVisible(cardDetailsBackButton, 10)
		
		// Check Product Created Successfully
		TestObject newItemObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='" + newItemId +  "']")
		boolean newItemObjectExist = TestHelper.isElementPresent(newItemObject)
		 
		if (newItemObjectExist) {
			TestHelper.thrownException("Service item removed successfully");
		} else {
			println("Service deleted exist");
			WebUI.closeBrowser();
		}
	}
}
