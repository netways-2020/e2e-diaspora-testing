import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import org.eclipse.persistence.internal.jpa.parsing.jpql.antlr.JPQLParser.deleteClause_scope
import org.openqa.selenium.WebElement as WebElement

long now = new Date().getTime();
String needTitle = 'new product title ' + now;
String needDescription = 'new product description ' + now;

String newTitle = needTitle + '_new';
String newDescription = needTitle + '_new';


// Login and go to my profile tab
WebUI.callTestCase(findTestCase('Company/Founder or admin/Authorization for company/Login_For_Company'), [:], FailureHandling.STOP_ON_FAILURE)
String companyUrl = WebUI.getUrl();

WebElement companyNameObject = TestHelper.getItem('company-name-title', 0)
String selectedCompanyName = companyNameObject.text;

TestHelper.goToCompanyManageEntity();
TestObject manageMarketPlaceAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Marketplace Listings_manage_entity_action']")
TestHelper.clickButton(manageMarketPlaceAction)


TestObject emptyStateAddProduct = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='market_place_add_product_action']")
boolean emptyStateAddProductExist = TestHelper.isElementPresent(emptyStateAddProduct)

 
if (emptyStateAddProductExist) {
	TestHelper.clickButton(emptyStateAddProduct);
	UserActionsHelper.addNeedOrOffer(needTitle, needDescription)
	 
	TestObject manageMarketPlaceBackButton = TestHelper.getObjectById("//div[@id='marketPlace_back_button']")
	WebUI.waitForElementVisible(manageMarketPlaceBackButton, 10)
	
	// Check Product Created Successfully
	checkNewCardIsSuccessfullyCreated(needTitle, needDescription);
	checkProducSectionExist(needTitle)
	editProduct(needTitle, newTitle, newDescription)
	removeProduct(newTitle)
//	checkIfProductAddedSuccessfully(true, 0);
	
} else { 

//	int itemsCountBeforAdd = TestHelper.countElementByClassName('product_items');

	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='marketPlace_back_button']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-Company-back-button']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='company_back_button']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_business_role_back_action']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-User-back-button']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='home_tab_id']"));
	WebUI.delay(1);

	// Add new product
	TestObject user_action_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//img[@id='user_action_button']")
	WebUI.click(user_action_button)
	
	TestObject add_product_action_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='List a Product_user_action']")
	WebUI.click(add_product_action_button)

	TestObject changeTargetAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='change-target-id']")
	WebUI.click(changeTargetAction)
	
	TestObject selectedTarget = TestHelper.getObjectById("//div[@id='" +selectedCompanyName+ "_target']");
	WebUI.click(selectedTarget)
	
	UserActionsHelper.addNeedOrOffer(needTitle, needDescription)
	
	TestObject manageProfilSection = TestHelper.getObjectById("//div[@id='manageProfileId']")
	WebUI.waitForElementVisible(manageProfilSection, 10)
	
	WebUI.navigateToUrl(companyUrl);
	
	WebUI.delay(1);
	TestHelper.goToCompanyManageEntity();
	TestHelper.clickButton(manageMarketPlaceAction);
	TestObject manageMarketPlaceBackButton = TestHelper.getObjectById("//div[@id='marketPlace_back_button']")
	WebUI.waitForElementVisible(manageMarketPlaceBackButton, 10)
	
	// Check Product Created Successfully
	checkNewCardIsSuccessfullyCreated(needTitle, needDescription); 
	checkProducSectionExist(needTitle);
	editProduct(needTitle, newTitle, newDescription)
	removeProduct(newTitle)

//	checkIfProductAddedSuccessfully(false, itemsCountBeforAdd);
}


def checkNewCardIsSuccessfullyCreated(String needTitle, String needDescription) { 
	String newItemId = needTitle + '_product';
	TestObject productSearchInputObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='market_place_product_search_input']")
	TestHelper.setInputValue(productSearchInputObject, needTitle)
	WebUI.delay(2);
	 
	TestObject newItemObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='" + newItemId +  "']")
	boolean newItemObjectExist = TestHelper.isElementPresent(newItemObject)
	 
	TestObject leftTagObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='secondLeftTag']")
	boolean leftTagObjectExist = TestHelper.isElementPresent(leftTagObject)
	 
	if (!newItemObjectExist || !leftTagObjectExist) {
		TestHelper.thrownException("Product item details not added successfully");
	} 
}

def checkIfProductAddedSuccessfully(boolean firstItemAdded, int itemCountBeforAdd) {
	int itemCount = TestHelper.countElementByClassName('product_items');
	
	if(firstItemAdded) {
		if(itemCount == 1) {
			println("Product item added successfully");
		} else {
			TestHelper.thrownException("Product item not added successfully");
		}
	} else {
	   if((itemCountBeforAdd + 1) == itemCount) {
		   println("Product item added successfully");
	   } else {
		   TestHelper.thrownException("Product item not added successfully");
	   }
	}
}

def checkProducSectionExist(String needTitle) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='marketPlace_back_button']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-Company-back-button']"));
	
	TestObject companyProductsSection = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='company-products-section']")
	boolean companyProductsSectionExist = TestHelper.isElementPresent(companyProductsSection)
	
	if (companyProductsSectionExist) {
		println("Product section exist");
		String newItemId = needTitle + '_product';
		 
		TestObject newItemObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='" + newItemId +  "']")
		boolean newItemObjectExist = TestHelper.isElementPresent(newItemObject)
		 
		TestObject leftTagObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='secondLeftTag']")
		boolean leftTagObjectExist = TestHelper.isElementPresent(leftTagObject)
		 
		if (!newItemObjectExist || !leftTagObjectExist) {
			TestHelper.thrownException("Product item details not added successfully");
		} else {
			println("Added product exist");
		}
	} else {
		TestHelper.thrownException("Product section not exist")
	}
}

def editProduct(String needTitle, String newNeedTitle, String newNeedDescritpion) {
	// Edit Product
	String newItemId = needTitle + '_product';
	TestObject editItemObject = TestHelper.getObjectById( "//span[@id='" + newItemId +  "']");
	boolean editItemObjectItemObjectExist = TestHelper.isElementPresent(editItemObject)
	 	 
	if (editItemObjectItemObjectExist) {
		println("newItemId: " + newItemId);
		TestHelper.clickButton(editItemObject);
		
		TestHelper.verifyOfferCardFeature('Product');
		TestHelper.verifyOfferCard(needTitle, 0, 0, true)
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='card_details_manage_button']"));
		TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Edit Post_user_action']"));		
		
		UserActionsHelper.editNeedOrOffer(newNeedTitle, newNeedDescritpion);
		
		TestObject cardDetailsBackButton = TestHelper.getObjectById("//div[@id='card_details_back_button']")
		WebUI.waitForElementVisible(cardDetailsBackButton, 10)
		
		int companyOfferCount = TestHelper.countElementByClassName('company-offer-countries');
		int companyKeywordCount = TestHelper.countElementByClassName('company-offer-keywords');
		
		TestHelper.verifyOfferCard(newNeedTitle, companyOfferCount, companyKeywordCount, false)
		TestHelper.clickButton(cardDetailsBackButton);
		
		// Check Product Created Successfully
		WebUI.delay(3);
		TestObject newItemObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='" + newNeedTitle + '_product' +  "']")
		boolean newItemObjectExist = TestHelper.isElementPresent(newItemObject)
		 
		TestObject leftTagObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='secondLeftTag']")
		boolean leftTagObjectExist = TestHelper.isElementPresent(leftTagObject)
 
		String text = WebUI.getText(newItemObject)
		if (text.equals(newNeedTitle) && leftTagObjectExist) {
			println("Product edited exist");
		} else {
			TestHelper.thrownException("Product item edited successfully");
		}
	} 
}

def removeProduct(String newNeedTitle) {
	
	// Remove Product
	String newItemId = newNeedTitle + '_product';
	TestObject removeItemObject = TestHelper.getObjectById( "//span[@id='" + newItemId +  "']");
	boolean removeItemObjectExist = TestHelper.isElementPresent(removeItemObject)
		  
	if (removeItemObjectExist) {
		TestHelper.clickButton(removeItemObject);

		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='card_details_manage_button']"));
		TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Delete Post_user_action']"));
		
		WebElement deleteImageConfirmation = TestHelper.getItem('remove-new-card', 0)
		deleteImageConfirmation.click();

		TestObject cardDetailsBackButton = TestHelper.getObjectById("//div[@id='company_menu_action_id']")
		WebUI.waitForElementVisible(cardDetailsBackButton, 10)
		
		// Check Product Created Successfully
		TestObject newItemObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='" + newItemId +  "']")
		boolean newItemObjectExist = TestHelper.isElementPresent(newItemObject)
		 
		if (newItemObjectExist) {
			TestHelper.thrownException("Product item removed successfully");
		} else {
			println("Product deleted exist");
			WebUI.closeBrowser();
			
		}
	}
}