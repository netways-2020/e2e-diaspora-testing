import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement

long now = new Date().getTime();
String needTitle = 'new title ' + now;
String needDescription = 'new description ' + now;
String newTitle = needTitle + '_new';
String newDescription = needTitle + '_new';

// Login and go to my profile tab
WebUI.callTestCase(findTestCase('Company/Founder or admin/Authorization for company/Login_For_Company'), [:], FailureHandling.STOP_ON_FAILURE)
String companyUrl = WebUI.getUrl();
WebElement companyNameObject = TestHelper.getItem('company-name-title', 0)
String selectedCompanyName = companyNameObject.text;

TestHelper.goToCompanyManageEntity();
TestObject manageMarketPlaceAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Marketplace Listings_manage_entity_action']")
TestHelper.clickButton(manageMarketPlaceAction)

TestHelper.clickButton(TestHelper.getObjectById("//ion-segment-button[@id='market_place_franchise_tab']"))

TestObject emptyStateAddFranchise = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='market_place_add_franchise_action']")
boolean emptyStateAddFranchiseExist = TestHelper.isElementPresent(emptyStateAddFranchise)

 
if (emptyStateAddFranchiseExist) {	
	TestHelper.clickButton(emptyStateAddFranchise);
	UserActionsHelper.addNeedOrOffer(needTitle, needDescription)
	 
	TestObject manageMarketPlaceBackButton = TestHelper.getObjectById("//div[@id='marketPlace_back_button']")
	WebUI.waitForElementVisible(manageMarketPlaceBackButton, 10);
	
	// Check franchise Created Successfully
	checkNewCardIsSuccessfullyCreated(needTitle, needDescription);
	checkFranchiseSectionExist(needTitle);
	editFranchise(needTitle, newTitle, newDescription)
	removeFranchise(newTitle)

//	checkIfFranchiseAddedSuccessfully(true, 0);
	
} else { 
	int itemsCountBeforAdd = TestHelper.countElementByClassName('franchise_items');

	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='marketPlace_back_button']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-Company-back-button']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='company_back_button']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_business_role_back_action']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-User-back-button']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='home_tab_id']"));
	WebUI.delay(1);

	// Add new franchise
	TestObject user_action_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//img[@id='user_action_button']")
	WebUI.click(user_action_button)
	
	TestObject add_franchise_action_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='List a Franchise_user_action']")
	WebUI.click(add_franchise_action_button)

	TestObject changeTargetAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='change-target-id']")
	WebUI.click(changeTargetAction)
	
	TestObject selectedTarget = TestHelper.getObjectById("//div[@id='" +selectedCompanyName+ "_target']");
	WebUI.click(selectedTarget)
	
	UserActionsHelper.addNeedOrOffer(needTitle, needDescription)
	
	TestObject manageProfilSection = TestHelper.getObjectById("//div[@id='manageProfileId']")
	WebUI.waitForElementVisible(manageProfilSection, 10)
	WebUI.navigateToUrl(companyUrl);
	
	
	WebUI.delay(1);
	TestHelper.goToCompanyManageEntity();
	TestHelper.clickButton(manageMarketPlaceAction);
	TestObject manageMarketPlaceBackButton = TestHelper.getObjectById("//div[@id='marketPlace_back_button']")
	WebUI.waitForElementVisible(manageMarketPlaceBackButton, 10)
	
	// Check Franchise Created Successfully
	checkNewCardIsSuccessfullyCreated(needTitle, needDescription);
	checkFranchiseSectionExist(needTitle);
	editFranchise(needTitle, newTitle, newDescription)
	removeFranchise(newTitle)

//	checkIfFranchiseAddedSuccessfully(false, itemsCountBeforAdd);
}


def checkNewCardIsSuccessfullyCreated(String needTitle, String needDescription) {
	TestHelper.clickButton(TestHelper.getObjectById("//ion-segment-button[@id='market_place_franchise_tab']"))
	String newItemId = needTitle + '_franchise';
	TestObject frachiseSearchInputObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='market_place_franchise_search_input']")
	TestHelper.setInputValue(frachiseSearchInputObject, needTitle)
	WebUI.delay(2);
	
	TestObject newItemObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='" + newItemId +  "']")
	boolean newItemObjectExist = TestHelper.isElementPresent(newItemObject)
	 
	TestObject leftTagObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='secondLeftTag']")
	boolean leftTagObjectExist = TestHelper.isElementPresent(leftTagObject)
	 
	if (!newItemObjectExist || !leftTagObjectExist) {
		TestHelper.thrownException("Franchise item details not added successfully");
	}
}


def checkIfFranchiseAddedSuccessfully(boolean firstItemAdded, int itemCountBeforAdd) {
	int itemCount = TestHelper.countElementByClassName('franchise_items');
	
	if(firstItemAdded) {
		if(itemCount == 1) {
			println("Franchise item added successfully");
		} else {
			TestHelper.thrownException("Franchise item not added successfully");
		}
	} else {
	   if((itemCountBeforAdd + 1) == itemCount) {
		   println("Franchsie item added successfully");
	   } else {
		   TestHelper.thrownException("Franchise item not added successfully");
	   }
	}
}


def checkFranchiseSectionExist(String needTitle) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='marketPlace_back_button']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-Company-back-button']"));
	
	TestObject companyFranchisesSection = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='company-franchises-section']")
	boolean companyFranchisesSectionExist = TestHelper.isElementPresent(companyFranchisesSection)
	
	if (companyFranchisesSectionExist) {
		println("Franchises section exist");
		String newItemId = needTitle + '_franchise';
		 
		TestObject newItemObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='" + newItemId +  "']")
		boolean newItemObjectExist = TestHelper.isElementPresent(newItemObject)
		 
		TestObject leftTagObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='secondLeftTag']")
		boolean leftTagObjectExist = TestHelper.isElementPresent(leftTagObject)
		 
		if (!newItemObjectExist || !leftTagObjectExist) {
			TestHelper.thrownException("Franchise item details not added successfully");
		} else {
			println("Added franchise exist");
		}
	} else {
		TestHelper.thrownException("Franchise section not exist")
	}
}



def editFranchise(String needTitle, String newNeedTitle, String newNeedDescritpion) {
	// Edit Franchise
	String newItemId = needTitle + '_franchise';
	TestObject editItemObject = TestHelper.getObjectById( "//span[@id='" + newItemId +  "']");
	boolean editItemObjectItemObjectExist = TestHelper.isElementPresent(editItemObject)
		  
	if (editItemObjectItemObjectExist) {
		println("newItemId: " + newItemId);
		TestHelper.clickButton(editItemObject);
		
		TestHelper.verifyOfferCardFeature('Franchise');
		TestHelper.verifyOfferCard(needTitle, 0, 0, true)
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='card_details_manage_button']"));
		TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Edit Post_user_action']"));
		
		UserActionsHelper.editNeedOrOffer(newNeedTitle, newNeedDescritpion);
		
		TestObject cardDetailsBackButton = TestHelper.getObjectById("//div[@id='card_details_back_button']")
		WebUI.waitForElementVisible(cardDetailsBackButton, 10)
		
		int companyOfferCount = TestHelper.countElementByClassName('company-offer-countries');
		int companyKeywordCount = TestHelper.countElementByClassName('company-offer-keywords');
		
		TestHelper.verifyOfferCard(newNeedTitle, companyOfferCount, companyKeywordCount, false)
		TestHelper.clickButton(cardDetailsBackButton)
		
		// Check Franchise Created Successfully
		WebUI.delay(3);
		TestObject newItemObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='" + newNeedTitle + '_franchise' +  "']")
		boolean newItemObjectExist = TestHelper.isElementPresent(newItemObject)
		 
		TestObject leftTagObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='secondLeftTag']")
		boolean leftTagObjectExist = TestHelper.isElementPresent(leftTagObject)

		String text = WebUI.getText(newItemObject)
		if (text.equals(newNeedTitle) && leftTagObjectExist) {
			println("Product edited exist");
		} else {
			TestHelper.thrownException("Product item edited successfully");
		}
	}
}

def removeFranchise(String newNeedTitle) {
	
	// Remove Franchise
	String newItemId = newNeedTitle + '_franchise';
	TestObject removeItemObject = TestHelper.getObjectById( "//span[@id='" + newItemId +  "']");
	boolean removeItemObjectExist = TestHelper.isElementPresent(removeItemObject)
		  
	if (removeItemObjectExist) {
		TestHelper.clickButton(removeItemObject);

		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='card_details_manage_button']"));
		TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Delete Post_user_action']"));
		
		WebElement deleteImageConfirmation = TestHelper.getItem('remove-new-card', 0)
		deleteImageConfirmation.click();

		TestObject cardDetailsBackButton = TestHelper.getObjectById("//div[@id='company_menu_action_id']")
		WebUI.waitForElementVisible(cardDetailsBackButton, 10)
		
		// Check Product Created Successfully
		TestObject newItemObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='" + newItemId +  "']")
		boolean newItemObjectExist = TestHelper.isElementPresent(newItemObject)
		 
		if (newItemObjectExist) {
			TestHelper.thrownException("Franchise item removed successfully");
		} else {
			println("Franchise deleted exist");
			WebUI.closeBrowser();
		}
	}
}
