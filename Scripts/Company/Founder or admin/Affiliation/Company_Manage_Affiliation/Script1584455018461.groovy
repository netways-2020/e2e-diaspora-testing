import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import static org.junit.Assert.*

import org.junit.After
import org.junit.Test
import org.openqa.selenium.WebElement as WebElement
import org.stringtemplate.v4.compiler.STParser.compoundElement_return

String organizationName = 'Netways_Organization' + new Date().getTime();
String companyName = 'Netways' + new Date().getTime()
String roleName = "Founder"
String companyUrl, organizationUrl;
String selectedCompanyName, selectedOrganizationName;

 
// Login and go to my profile tab
WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Login'), [:], FailureHandling.STOP_ON_FAILURE)
 
TestHelper.goToManageEntity();
TestObject manageNetworkRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Network Roles_manage_entity_action']")
TestHelper.clickButton(manageNetworkRoleAction)

checkOrganization(organizationName, roleName);
checkCompany(companyName, roleName)
addAffiliation();
acceptAffiliation();
checkIfOrganzationAffiliationExistInCompanyAffiliations();
checkIfOrganizaitonExistInCompanyProfile();
removeAffilition();

// Check Organization 
def checkOrganization (String organizationName, String roleName) { 
//	int organizationSize = TestHelper.countElementByClassName('manage_user_network_role')
//	println(organizationSize)
//	if (organizationSize > 0) {
//		WebElement selectedOrganization = TestHelper.getItem('manage_user_network_role', 0)
//		selectedOrganization.click();
//		
//		WebUI.delay(6);
//		organizationUrl = WebUI.getUrl();
//		WebElement organizationNameObject = TestHelper.getItem('organization-name-title', 0)
//		selectedOrganizationName = organizationNameObject.text;
//		
//		TestObject organizationBackButton = TestHelper.getObjectById("//div[@id='organization_back_button']");
//		TestHelper.clickButton(organizationBackButton);
//		
//	} else {
		TestObject emptyStateObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-network-role-empty-state']")
		boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)
	
		if (emptyStateObjectPresent == true) {
			println("hasn't network roles")
	 
			TestObject emptyStateAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-network-role-empty-state-button']")
			TestHelper.clickButton(emptyStateAction)

			// Create organization, add get created organization url
			UserRoleHelper.createOrganization(organizationName, roleName);
			 
			TestObject manageNetworkBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_network_role_back_action']")
			WebUI.waitForElementPresent(manageNetworkBackButton, 5)
			
			getOrganizationName(organizationName, roleName);
		} else {
			println("has network roles") 
			TestObject addNetworkRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='add-new-network-role-action']")
			TestHelper.clickButton(addNetworkRoleAction)
			
			// Create organization, add get created organization url
			UserRoleHelper.createOrganization(organizationName, roleName);
			TestObject manageNetworkBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_network_role_back_action']")
			WebUI.waitForElementPresent(manageNetworkBackButton, 5)
			getOrganizationName(organizationName, roleName);
		}
//	} 
}

def getOrganizationName(String organizationName, String roleName) { 
	String networkRoleId = organizationName + '-' + roleName + '-role';
	String networkRoleIdXpath = "//div[@id='"+networkRoleId+"']"
	TestObject networkRoleObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, networkRoleIdXpath)
	TestHelper.clickButton(networkRoleObject);
	 
	WebUI.delay(6);
	TestObject organizationBackButton = TestHelper.getObjectById("//div[@id='organization_back_button']");
 	organizationUrl = WebUI.getUrl();
	 
	WebElement organizationNameObject = TestHelper.getItem('organization-name-title', 0)
	selectedOrganizationName = organizationNameObject.text;
	TestHelper.clickButton(organizationBackButton);
}

// Check Company
def checkCompany(String companyName, String roleName) {
	TestObject manageNetworkBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_network_role_back_action']")
	TestHelper.clickButton(manageNetworkBackButton);
	 
	TestObject manageBuinessRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Business Roles_manage_entity_action\']')
	TestHelper.clickButton(manageBuinessRoleAction)
//	int companiesSize = TestHelper.countElementByClassName('manage_user_business_role')
//	if (companiesSize > 0) {
//		WebElement selectedCompany = TestHelper.getItem('manage_user_business_role', 0) 
//		selectedCompany.click()  
//		companyUrl = WebUI.getUrl(); 
//		
//		WebElement companyNameObject = TestHelper.getItem('company-name-title', 0)
//		selectedCompanyName = companyNameObject.text;
//		
//	} else {
		TestObject emptyStateObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-business-role-empty-state\']')
	
		boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)
	
		if (emptyStateObjectPresent == true) {
			println('hasn\'t business roles')
	
			TestObject emptyStateAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'no-business-role-empty-state-button\']')
			WebUI.click(emptyStateAction)
			createNewCompany(companyName, roleName)
	
		} else {
			println('has business roles')
	
			TestObject addBusinessRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'add-new-business-role-action\']')
			WebUI.click(addBusinessRoleAction)
			createNewCompany(companyName, roleName)
		}
//	} 
}
 
def createNewCompany(String companyName, String roleName) {
	UserRoleHelper.createCompany(companyName, roleName)
	WebUI.delay(2);
	String companyTarget = companyName + '-' + roleName + '-role'
	TestObject selectedCompany = TestHelper.getObjectById("//div[@id='"+companyTarget+"']")
	TestHelper.clickButton(selectedCompany);
	companyUrl = WebUI.getUrl(); 
	WebElement companyNameObject = TestHelper.getItem('company-name-title', 0)
	selectedCompanyName = companyNameObject.text;
}

// Add Affiliation
def addAffiliation() {
	TestHelper.goToCompanyManageEntity();
	TestObject manageAffiliationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Member Of_manage_entity_action']")
	TestHelper.clickButton(manageAffiliationAction)
	
	TestObject requestedAffiliation = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-pending-affiliation-request-empty-state']")
	boolean requestedAffiliationPresent = TestHelper.isElementPresent(requestedAffiliation)

	if (requestedAffiliationPresent) {
		TestHelper.clickButton(requestedAffiliation);
				
		TestObject searchForOrganization = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='searchForOrganizationInput']")
		TestHelper.setInputValue(searchForOrganization, selectedOrganizationName)
		WebUI.delay(1);

		String organizationAction = selectedOrganizationName + '_filter_organization_action';
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='" + organizationAction + "']"));
		
		WebElement linkConfirmation = TestHelper.getItem('link_organization_affiliation_confirmation', 0)
		linkConfirmation.click();
 		
		WebUI.delay(4);
		WebUI.navigateToUrl(organizationUrl)
	} else {
		TestHelper.thrownException("Organization already has affiliation");
	}
}

def acceptAffiliation() { 
	TestHelper.goToOrganizationManageEntity();
	TestObject manageCompaniesMember = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Member Companies_manage_entity_action']")
	TestHelper.clickButton(manageCompaniesMember)

	WebUI.delay(2);

	String companyId = selectedCompanyName + '_pending_company_affiliation';
	TestObject companyObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + companyId + "']")
	boolean companyObjectExist = TestHelper.isElementPresent(companyObject);
	
	if(companyObjectExist) {
		String companyAction = selectedCompanyName + '_manage_pending_company_affiliation';
		TestObject companyActionObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + companyAction + "']")
		TestHelper.clickButton(companyActionObject);
	
		// Accept Company Request
		TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Approve Request_user_action']"));
		WebUI.delay(3);
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_company_affiliation_back_action']"));
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-Organization-back-button']"));
	} else {
		TestHelper.thrownException("Company not exist");
	}

}	

def checkIfOrganzationAffiliationExistInCompanyAffiliations() {
	WebUI.navigateToUrl(companyUrl);
	TestHelper.goToCompanyManageEntity();
	TestObject manageAffiliationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Member Of_manage_entity_action']")
	TestHelper.clickButton(manageAffiliationAction)

	// Check if organization exist in the current tab
	TestHelper.clickButton(TestHelper.getObjectById( "//ion-segment-button[@id='manage_network_current_affiliation']"))
	String affiliationOrganization = selectedOrganizationName + '_current_org_affiliation';
	TestObject affiliationOrganizationObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + affiliationOrganization + "']")
	boolean affiliationOrganizationObjectExist = TestHelper.isElementPresent(affiliationOrganizationObject);
	
	if(!affiliationOrganizationObjectExist) {
		TestHelper.thrownException("Affiliation organization not present in the company affiliations");
	}
	
	
	// Feature Organization
	String organizationAction = selectedOrganizationName + '_manage_current_org_affiliation';
	TestHelper.clickButton(TestHelper.getObjectById( "//div[@id='" + organizationAction + "']"))
	TestHelper.clickButton(TestHelper.getObjectById( "//a[@id='Set As Primary_user_action']"));
	WebUI.delay(2);
	
	String featuredOrganization = selectedOrganizationName + '_featured';
	TestObject featuredOrganizationObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + featuredOrganization + "']")
	boolean featuredOrganizationObjectExist = TestHelper.isElementPresent(featuredOrganizationObject);
	
	if(featuredOrganizationObjectExist) {		
		// Unfeature Organization
		TestHelper.clickButton(TestHelper.getObjectById( "//div[@id='" + organizationAction + "']"))
		TestHelper.clickButton(TestHelper.getObjectById( "//a[@id='Remove From Primary_user_action']"))
		WebUI.delay(2);
		boolean unfeaturedOrganizationObjectExist = TestHelper.isElementPresent(featuredOrganizationObject);
		
		if(unfeaturedOrganizationObjectExist) {
			TestHelper.thrownException("Orgnization still set as featured");
		}
	} else {
		TestHelper.thrownException("Orgnization not set as featured");
	}
}

def checkIfOrganizaitonExistInCompanyProfile() {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_organization_affiliation_back_action']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-Company-back-button']"))
	
	TestObject affiliationSection = TestHelper.getObjectById("//div[@id='affiliation-section']")
	boolean affiliationSectionExist = TestHelper.isElementPresent(affiliationSection);
	
	if(affiliationSectionExist) {
		String mainOrganization = selectedOrganizationName + '_main_page_org_affiliation';
		TestObject mainOrganizationObject = TestHelper.getObjectById("//div[@id='" + mainOrganization + "']")
		boolean mainOrganizationObjectExist = TestHelper.isElementPresent(mainOrganizationObject);
		
		if(!mainOrganizationObjectExist) {
			TestHelper.thrownException("Affiliation not exist in the main page");	
		}	
	} else {
		TestHelper.thrownException("Affiliation organizaiton exist in the company profile");
	}	
}

def removeAffilition() {
	TestHelper.goToCompanyManageEntity();
	TestObject manageAffiliationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Member Of_manage_entity_action']")
	TestHelper.clickButton(manageAffiliationAction)

	// Check if organization exist in the current tab
	TestHelper.clickButton(TestHelper.getObjectById( "//ion-segment-button[@id='manage_network_current_affiliation']"))

	// Remove affiliation
	String organizationAction = selectedOrganizationName + '_manage_current_org_affiliation';
	TestHelper.clickButton(TestHelper.getObjectById( "//div[@id='" + organizationAction + "']"))
	TestHelper.clickButton(TestHelper.getObjectById( "//a[@id='Remove Organization_user_action']"))
	WebElement removeConfirmation = TestHelper.getItem('removeCurrentOrganizatinoAffiliation', 0)
	removeConfirmation.click();
	
	WebUI.delay(2);

	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_organization_affiliation_back_action']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-Company-back-button']"))
	
	TestObject affiliationSection = TestHelper.getObjectById("//div[@id='affiliation-section']")
	boolean affiliationSectionExist = TestHelper.isElementPresent(affiliationSection);
	
	if(affiliationSectionExist) {
		String mainOrganization = selectedOrganizationName + '_main_page_org_affiliation';
		TestObject mainOrganizationObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + mainOrganization + "']")
		boolean mainOrganizationObjectExist = TestHelper.isElementPresent(mainOrganizationObject);
		
		if(!mainOrganizationObjectExist) {
			println("Test has been passed");
			WebUI.closeBrowser();
		} else {
			TestHelper.thrownException("Affiliation not exist in the main page");
		}
		
	} else {
		TestHelper.thrownException("Affiliation organizaiton exist in the company profile");
	}
}



