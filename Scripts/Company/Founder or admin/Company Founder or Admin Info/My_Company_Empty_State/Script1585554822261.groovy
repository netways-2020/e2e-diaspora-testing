import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

String companyName = 'Netways' + new Date().getTime()
String roleName = 'Founder'

WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Login'), [:], FailureHandling.STOP_ON_FAILURE)
TestHelper.goToManageEntity()
TestObject manageBuinessRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='Business Roles_manage_entity_action']")
TestHelper.clickButton(manageBuinessRoleAction)

TestObject emptyStateObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='no-business-role-empty-state']")
boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)

if (emptyStateObjectPresent == true) {
	println('hasn\'t business roles')

	TestObject emptyStateAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='no-business-role-empty-state-button']")
	WebUI.click(emptyStateAction)
	createNewCompany(companyName, roleName)

} else {
	println('has business roles')

	TestObject addBusinessRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='add-new-business-role-action']")
	WebUI.click(addBusinessRoleAction)
	createNewCompany(companyName, roleName)
}

def createNewCompany(String companyName, String roleName) {
	UserRoleHelper.createCompany(companyName, roleName)
	WebUI.delay(2);
	String companyTarget = companyName + '-' + roleName + '-role'
	TestObject selectedCompany = TestHelper.getObjectById("//div[@id='"+companyTarget+"']")
	TestHelper.clickButton(selectedCompany);
	WebUI.delay(2);
	
	// Check Common Header Exist
	checkCommonHeaderElementExist();
	
	// Check General Info
	UserRoleHelper.checkCompanyGeneralInfoFieldsEmptyState();
	
	// Check Company Section
	checkCompanySectionsEmptyState();
	
	
	// Check Company Section title and subtitle
	checkCompanySectionsTitleAndSubTitle();
	
	WebUI.closeBrowser();
}

def checkCommonHeaderElementExist() {
	boolean successEmptyFields = true;
	
	TestObject followers_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='follower_count_id_tag_label']")
	TestObject team_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='team_id_tag_label']")

	TestObject companyMenuAction_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='company_menu_action_id']")
	TestObject manage_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Manage_Company_entity_action']")
	TestObject invite_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Invite_Company_entity_action']")	
	
	if(!TestHelper.isElementPresent(followers_label_object)) {
		TestHelper.thrownException("Followers Label not present");
	} else if(!TestHelper.isElementPresent(team_label_object)) {
		TestHelper.thrownException("Team Label not present");
	} else if(!TestHelper.isElementPresent(companyMenuAction_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(manage_entity_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(invite_entity_object)) {
		successEmptyFields = false;
	} 
	
	if(successEmptyFields) {
		println("Company common header info exist");
	} else {
			TestHelper.thrownException("Error: Company commone header info")
	}
}

def checkCompanySectionsEmptyState() {

	boolean successEmptyFields = true;
	
	TestObject company_about_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='company_about']")
	
	TestObject company_affiliation_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='affiliation-section']")
	TestObject company_affiliation_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-affiliation-empty-state']")
		
//	TestObject company_products_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div['company-products-section']")
//	TestObject company_services_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div['company-services-section']")
//	TestObject company_franchises_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div['company-franchises-section']")
	TestObject company_marketplace_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='market-place-no-needs-section']")
	TestObject company_marketplace_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-company-needs-empty-state']")
		
//	TestObject company_brands_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div['brands-section']")
	TestObject company_branches_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='company_branches']")
	TestObject company_branches_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-company-branches-empty-state']")
	
	TestObject company_teams_members_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='company_teams_members']")
//	TestObject company_teams_members_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-company-team-member-empty-state']")
	
//	TestObject company_join_member_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div['join_company_section']")
//	TestObject company_join_member_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div['join_company_action']")	
		
	TestObject company_updates_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='company_updates']")
	TestObject company_updates_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-company-updates-empty-state']")
	
	TestObject company_media_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='company_media']")
	TestObject company_media_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-company-media-empty-state']")
	
	
	// Check Object Exist	
	if(!TestHelper.isElementPresent(company_about_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(company_affiliation_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(company_affiliation_empty_state_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(company_marketplace_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(company_marketplace_empty_state_object)) {
		successEmptyFields = false;
	} 
//	else if(!TestHelper.isElementPresent(company_brands_object)) {
//		successEmptyFields = false;
//	} 
	else if(!TestHelper.isElementPresent(company_branches_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(company_branches_empty_state_object)) {
		successEmptyFields = false;
	} 
	else if(!TestHelper.isElementPresent(company_teams_members_object)) {
		successEmptyFields = false;
	}
//	 else if(!TestHelper.isElementPresent(company_teams_members_empty_state_object)) {
//		successEmptyFields = false;
//	} 
//	  else if(!TestHelper.isElementPresent(company_join_member_object)) {
//		successEmptyFields = false;
//	} else if(!TestHelper.isElementPresent(company_join_member_empty_state_object)) {
//		successEmptyFields = false;
//	} 
	
	else if(!TestHelper.isElementPresent(company_updates_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(company_updates_empty_state_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(company_media_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(company_media_empty_state_object)) {
		successEmptyFields = false;
	}
	
	
	if(successEmptyFields) {
			println("Company section empty state exist");
	} else {
			TestHelper.thrownException("Error: Company section empty state exist")
	}
	
}

def checkCompanySectionsTitleAndSubTitle() {
	TestObject about_title_object = TestHelper.getObjectById("//div[@id='company_about_title_id']")
	TestObject about_subTitle_object = TestHelper.getObjectById("//div[@id='company_about_subTitle_id']")

	if(TestHelper.isElementPresent(about_title_object) && TestHelper.isElementPresent(about_subTitle_object)) {
		String aboutTitle = WebUI.getText(about_title_object);
		String aboutSubTitle = WebUI.getText(about_subTitle_object);
	
		if(!aboutTitle.equals("About") && !aboutSubTitle.equals("Info, sector and contact details")) {
			TestHelper.thrownException("Invalid about title and subtitle")
		}
	}

	TestObject affiliation_title_object = TestHelper.getObjectById("//div[@id='company_affiliation_title_id']")
	TestObject affiliation_subTitle_object = TestHelper.getObjectById("//div[@id='company_affiliation_subTitle_id']")

	if(TestHelper.isElementPresent(affiliation_title_object) && TestHelper.isElementPresent(affiliation_subTitle_object)) {
		String affiliationTitle = WebUI.getText(affiliation_title_object);
		String affiliationSubTitle = WebUI.getText(affiliation_subTitle_object);

		 if(!affiliationTitle.equals("Member Of") && !affiliationSubTitle.equals("Member in these entities")) {
			TestHelper.thrownException("Invalid affiliation title and subtitle")
		}
	}

	TestObject market_place_title_object = TestHelper.getObjectById("//div[@id='company_market_place_title_id']")
	TestObject market_palce_subTitle_object = TestHelper.getObjectById("//div[@id='company_market_place_subTitle_id']")

	if(TestHelper.isElementPresent(market_place_title_object) && TestHelper.isElementPresent(market_palce_subTitle_object)) {
		String marketPlaceTitle = WebUI.getText(market_place_title_object);
		String marketPlaceSubTitle = WebUI.getText(market_palce_subTitle_object);

		if(!marketPlaceTitle.equals("Marketplace Listings") && !marketPlaceSubTitle.equals("Business offerings")) {
			TestHelper.thrownException("Invalid marketplace title and subtitle")
		}
	}

	TestObject branches_title_object = TestHelper.getObjectById("//div[@id='company_branches_title_id']")
	TestObject branches_subTitle_object = TestHelper.getObjectById("//div[@id='company_branches_subTitle_id']")

	if(TestHelper.isElementPresent(branches_title_object) && TestHelper.isElementPresent(branches_subTitle_object)) {
		String branchesTitle = WebUI.getText(branches_title_object);
		String branchesSubTitle = WebUI.getText(branches_subTitle_object);

		if(!branchesTitle.equals("Branches") && !branchesSubTitle.equals("Company hierarchy")) {
			TestHelper.thrownException("Invalid branches title and subtitle")
		}
	}

	TestObject teamMembers_title_object = TestHelper.getObjectById("//div[@id='company_team_members_title_id']")
	TestObject teamMembers_subTitle_object = TestHelper.getObjectById("//div[@id='company_team_members_subTitle_id']")

	if(TestHelper.isElementPresent(teamMembers_title_object) && TestHelper.isElementPresent(teamMembers_subTitle_object)) {
		String teamMemberTitle = WebUI.getText(teamMembers_title_object);
		String teamMemberSubTitle = WebUI.getText(teamMembers_subTitle_object);

		if(!teamMemberTitle.equals("Team Members") && !teamMemberSubTitle.equals("Company leadership & staff")) {
			TestHelper.thrownException("Invalid Team title and subtitle")
		}
	}

	TestObject updates_title_object = TestHelper.getObjectById("//div[@id='company_updates_title_id']")
	TestObject updates_subTitle_object = TestHelper.getObjectById("//div[@id='company_updates_subTitle_id']")

	if(TestHelper.isElementPresent(updates_title_object) && TestHelper.isElementPresent(updates_subTitle_object)) {
		String updatesTitle = WebUI.getText(updates_title_object);
		String updatesSubTitle = WebUI.getText(updates_subTitle_object);

		if(!updatesTitle.equals("Updates") && !updatesSubTitle.equals("Posts & discussions")) {
			TestHelper.thrownException("Invalid updates title and subtitle")
		}

	}

	TestObject media_title_object = TestHelper.getObjectById("//div[@id='company_media_title_id']")
	TestObject media_subTitle_object = TestHelper.getObjectById("//div[@id='company_media_subTitle_id']")
	
	if(TestHelper.isElementPresent(media_title_object) && TestHelper.isElementPresent(media_subTitle_object)) {
		String mediaTitle = WebUI.getText(media_title_object);
		String mediaSubTitle = WebUI.getText(media_subTitle_object);

		if(!mediaTitle.equals("Media Gallery") && !mediaSubTitle.equals("Photos & videos")) {
			TestHelper.thrownException("Invalid media title and subtitle")
		}

	}
}


