import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import java.util.List
import java.util.Map

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable


// Define Varibales
TestObject companyName_input, description_textArea, companyType_button, address_input, sector_button, keywords_button, brands_button;
TestObject headquarterCountry_button, headquarterCity_button;
TestObject phoneCountryCode_button, phoneNumber_input, contactEmail_input, poBox_input;
TestObject website_input, facebook_input, linkedin_input, instagram_input, twitter_input;

List<String> countries = TestHelper.prepareCountries();
Map<String, List<String>> cities = TestHelper.prepareCities();
 
 
// New Value
long newDate =  new Date().getTime();

// Country and city
String selectedHeadquarterCountry = TestHelper.getRandomValueFromList(countries);
List<String> citiesOfHeadquarterCountry = cities.get(selectedHeadquarterCountry)
		
String selectedHeadquarterCity =  TestHelper.getRandomValueFromList(citiesOfHeadquarterCountry);
 
  
// Login and go to my organization
WebUI.callTestCase(findTestCase('Company/Founder or admin/Authorization for company/Login_For_Company'), [:], FailureHandling.STOP_ON_FAILURE)

TestHelper.goToCompanyManageEntity();
TestObject manageBasicInformationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Company Information_manage_entity_action']")
TestHelper.clickButton(manageBasicInformationAction)


// Edit company and check values
prepareEditCompanyFields();
changeCompanyInfoValue(countries, cities, newDate, selectedHeadquarterCountry, selectedHeadquarterCity);
checkEditedCompanyValue(newDate, selectedHeadquarterCountry, selectedHeadquarterCity);

// Clear Edited Fields
TestHelper.goToCompanyManageEntity();
TestHelper.clickButton(manageBasicInformationAction)
clearCompanyFields(selectedHeadquarterCountry);
CompanyHelper.checkCompanyGeneralInfoFieldsEmptyState();
WebUI.closeBrowser();


def prepareEditCompanyFields() {
	// About Info
	companyName_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='company-name-id']")
	description_textArea = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//textarea[@id='about-section']")
	companyType_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='company-type-id']")
	
	address_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='company-address-id']")

	// Place Info
	headquarterCountry_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='company-headquarter-country']")
	headquarterCity_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='company-headquarter-city']")

	sector_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='company-sector-section']")
	keywords_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='keyword-section']")
	brands_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='brand-section']")
	
	
	// Contact Info
	phoneCountryCode_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='company-phone-country-code']")
	phoneNumber_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='company-phone-number']")
	contactEmail_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='company-contact-email']")
	poBox_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='pobox-input']")
	
	// Social Media
	website_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='company-website']")
	facebook_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='company-social-media-facebook']")
	linkedin_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='company-social-media-linkedin']")
	instagram_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='company-social-media-instagram']")
	twitter_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='company-social-media-twitter']")	
}

// Edit Fields
def changeCompanyInfoValue(List<String> countries, Map<String, List<String>> cities, long newDate, String selectedHeadquarterCountry, String selectedHeadquarterCity) {
	
	// User info
		TestHelper.setInputValue(companyName_input, companyName + newDate)
		TestHelper.setInputValue(description_textArea, newAbout + newDate)
		
		TestHelper.clickButton(companyType_button);
		WebUI.delay(1);
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Startup_item']"));		
		TestHelper.setInputValue(address_input, newAddress + newDate)
		
	// Headquarter Country/City 
		WebUI.delay(2)
		TestHelper.clickButton(headquarterCountry_button)
		TestObject headquarterCountrySearchInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='country-search-input']")
		TestHelper.setInputValue(headquarterCountrySearchInput, selectedHeadquarterCountry)
		TestObject headquarterCountryObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+selectedHeadquarterCountry+"']")
		WebUI.delay(2.5)
		WebUI.click(headquarterCountryObject)
		
		TestHelper.clickButton(headquarterCity_button)
		TestObject headquarterCitySearchInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='city-search-input']")
		TestHelper.setInputValue(headquarterCitySearchInput, selectedHeadquarterCity)
		TestObject headquarterCityObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+selectedHeadquarterCity+"']")
		WebUI.delay(1)
		WebUI.click(headquarterCityObject)
 		
	// Sector
		TestHelper.clickButton(sector_button)
		TestObject artSector = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Arts_sector']")
		TestObject animationSector = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Design_sector']")
		
		WebUI.click(artSector)
		WebUI.click(animationSector)
		
	// Keyword
		TestHelper.clickButton(keywords_button)
		TestObject activitiesKeyword = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//label[@id='Activities_item']")
		TestObject adventuresKeyword = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//label[@id='Adventures_item']")
		TestObject saveSelectedKeyword = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='save_selected_item_action']")
		
		if(TestHelper.isElementPresent(activitiesKeyword)) {
			WebUI.click(activitiesKeyword)
		}
		if(TestHelper.isElementPresent(adventuresKeyword)) {
			WebUI.click(adventuresKeyword)
		}
		WebUI.click(saveSelectedKeyword)
		

	// Brand
		TestHelper.clickButton(brands_button)
		TestObject al_mouna_item_brand = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//label[@id='Al Mouna_item']")
		TestObject batchig_brand = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//label[@id='Batchig_item']")
		
		TestObject saveSelectedBrands = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='save_selected_item_action']")
		
		if(TestHelper.isElementPresent(al_mouna_item_brand)) {
			WebUI.click(al_mouna_item_brand)
		}
		if(TestHelper.isElementPresent(batchig_brand)) {
			WebUI.click(batchig_brand)
		}

		WebUI.click(saveSelectedBrands)

	
	// Contact Info
		WebUI.delay(2)
		
		TestHelper.clickButton(phoneCountryCode_button)
		TestObject phoneCountrySearchInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='country-search-input']")
		TestHelper.setInputValue(phoneCountrySearchInput, selectedHeadquarterCountry)
		TestObject phoneCountryObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+selectedHeadquarterCountry+"']")
		WebUI.delay(1)
		WebUI.click(phoneCountryObject)
		
		TestHelper.setInputValue(phoneNumber_input, '123456')
 
	// Email
		TestHelper.setInputValue(contactEmail_input, newEmail + newDate + '@gmail.com')
		TestHelper.setInputValue(poBox_input, '123456')
		
	// Social Media
		TestHelper.setInputValue(website_input, newWebsite + newDate)
		TestHelper.setInputValue(facebook_input, newFacebook + newDate)
		TestHelper.setInputValue(linkedin_input, newLinkedIn + newDate)
		TestHelper.setInputValue(instagram_input, newInstagram + newDate)
		TestHelper.setInputValue(twitter_input, newTwitter + newDate)
		
		TestObject saveCompanyInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='save_company_info']")
		TestHelper.clickButton(saveCompanyInfo);
		WebUI.delay(3)
		
		TestObject manageEntity = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-Company-back-button']")
		TestHelper.clickButton(manageEntity);
}

def checkEditedCompanyValue(long newDate, String selectedHeadquarterCountry, String selectedHeadquarterCity) {
	WebUI.delay(1)
	boolean successfullyEdited = true;
	
	String headquarterCountry = TestHelper.getCountryIso(selectedHeadquarterCountry); 	
	String countryIsoCode = TestHelper.getCountryIso(selectedHeadquarterCountry);
	String phoneCountryCode = TestHelper.getCountryCode(selectedHeadquarterCountry);
	 
	 // Prepare IDs
		// Basic Info
			String edited_company_name_id = 'company_title_' + (companyName + newDate);
			String edited_company_description_id = 'company_introduction_id_' + (newAbout + newDate);
			String edited_company_address_id = 'company_address_id_' + (newAddress + newDate);
			String edited_sector_id = 'company_sector_id_' + newSector;

		// Brands
			String al_mouna_item_brand_id = 'Al Mouna_brand';
			String batchig_brand_id = 'Batchig_brand';
			
		// Countries
			String edited_country_headquarter  = 'country_' + countryIsoCode;
			String edited_city_headquarter  = 'country_' + countryIsoCode + '_tag_label' ;
			
 		// Contact Info
			String edited_phone_id = 'company_contact_phone_id_'+ phoneCountryCode + '123456';
			String edited_email_id = 'company_contact_email_id_'+ (newEmail + newDate) + '@gmail.com';
			String edited_pobox_id = 'company_pobox_id_'+  '123456';
			
		// Social Media
			String edited_website_id  = 'company_contact_website_id_' + newWebsite  + newDate;
			String edited_facebook_id  = "@id='company_facebook_id'  and  @alt= '" + newFacebook  + newDate +"'";
			String edited_linkedin_id  = "@id='company_linkedin_id'  and  @alt= '" + newLinkedIn  + newDate +"'";
			String edited_twitter_id   = "@id='company_twitter_id'   and  @alt= '" + newTwitter   + newDate +"'";
			String edited_instagram_id = "@id='company_instagram_id' and  @alt= '" + newInstagram + newDate +"'";
			
	// Prepare Objects
		// Basic Info
			TestObject edited_company_name_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + edited_company_name_id +"']");
			TestObject edited_about_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + edited_company_description_id +"']");
			TestObject edited_address_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + edited_company_address_id +"']")
			TestObject edited_sector_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + edited_sector_id +"']")
		
		// Brands
			TestObject edited_company_brand_1 = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='" + al_mouna_item_brand_id +"']");
			TestObject edited_company_brand_2 = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='" + batchig_brand_id +"']");

		
		// Countries Info
			TestObject edited_company_country_headquarter = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//img[@id='" + edited_country_headquarter +"']");
			TestObject edited_company_city_headquarter = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='" + edited_city_headquarter +"']");
			 
			 
		// Contact Info
			TestObject edited_email_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + edited_email_id +"']")
			TestObject edited_phone_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + edited_phone_id +"']")
			TestObject edited_pobox_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + edited_pobox_id +"']")
			TestObject edited_website_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + edited_website_id +"']")
			
		// Social Media
			TestObject edited_facebook_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//img["+ edited_facebook_id +"]")
			TestObject edited_linkedin_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//img["+ edited_linkedin_id +"]")
			TestObject edited_twitter_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,  "//img["+ edited_twitter_id  +"]")
			TestObject edited_instagram_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//img["+ edited_instagram_id +"]")
			
			
	// Check Object Exist
		if(!TestHelper.isElementPresent(edited_company_name_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_about_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_address_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_sector_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_company_brand_1)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_company_brand_2)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_company_country_headquarter)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_company_city_headquarter)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_email_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_phone_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_pobox_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_website_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_facebook_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_linkedin_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_twitter_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_instagram_object)) {
			successfullyEdited = false;
		}
 
 
	if(successfullyEdited) {
	  println("Company Updated Successfully");
	} else {
	  TestHelper.thrownException("Company Not Updated")
	}
	
}
	
// Reset Fields
def clearCompanyFields(String selectedCountryOfResidence) {
	
	TestHelper.setInputValue(description_textArea, " ");
	TestHelper.setInputValue(address_input, " ");
	
	// Keywords
	WebUI.delay(1);	
	TestHelper.clickButton(keywords_button)
	TestObject activitiesKeyword = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//label[@id='Activities_item_selected']")
	TestObject adventuresKeyword = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//label[@id='Adventures_item_selected']")
	TestObject saveSelectedKeyword = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='save_selected_item_action']")	
	
	WebUI.click(adventuresKeyword)
	WebUI.delay(1);
	WebUI.click(activitiesKeyword)
	WebUI.delay(1);
	WebUI.click(saveSelectedKeyword)
	
	// Brands
	WebUI.delay(1);
	TestHelper.clickButton(brands_button)
	
	TestObject al_mouna_item_brand = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//label[@id='Al Mouna_item_selected']")
	TestObject batchig_brand = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//label[@id='Batchig_item_selected']")
	TestObject saveSelectedBrands = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='save_selected_item_action']")	
	
	WebUI.click(al_mouna_item_brand)
	WebUI.delay(1);	
	WebUI.click(batchig_brand)
	WebUI.delay(1);	
	WebUI.click(saveSelectedBrands)
	
	
	// Contact Info
	WebUI.delay(1);
	TestHelper.clickButton(phoneCountryCode_button)
	TestObject phoneCountrySearchInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='country-search-input']")
	TestHelper.setInputValue(phoneCountrySearchInput, selectedCountryOfResidence)
	TestObject phoneCountryObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+selectedCountryOfResidence+"']")
	WebUI.delay(1)
	WebUI.click(phoneCountryObject)

	
	TestHelper.setInputValue(contactEmail_input, " ");
//	TestHelper.setInputValue(poBox_input, null);
	
	// Social Media
	WebUI.delay(1);
	TestHelper.setInputValue(website_input, " ");
	WebUI.delay(1);
	TestHelper.setInputValue(facebook_input, " ");
	WebUI.delay(1);
	TestHelper.setInputValue(linkedin_input, " ");
	WebUI.delay(1);
	TestHelper.setInputValue(instagram_input, " ");
	WebUI.delay(1);
	TestHelper.setInputValue(twitter_input, " ");
		
	TestObject saveCompanyInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='save_company_info']")
	TestHelper.clickButton(saveCompanyInfo);
	WebUI.delay(3)
	
	TestObject manageEntity = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-Company-back-button']")
	TestHelper.clickButton(manageEntity);
}

