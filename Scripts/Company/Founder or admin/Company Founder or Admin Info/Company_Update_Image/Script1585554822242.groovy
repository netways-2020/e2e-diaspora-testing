import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import java.util.List
import java.util.Map

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable


WebUI.callTestCase(findTestCase('Company/Founder or admin/Authorization for company/Login_For_Company'), [:], FailureHandling.STOP_ON_FAILURE)

checkThatTheCompanyHasntImage();

TestHelper.goToCompanyManageEntity()
TestObject manageBasicInformationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Company Information_manage_entity_action']")
TestHelper.clickButton(manageBasicInformationAction)
changeCompanyImage();

checkThatTheCompanyHasImage();

def checkThatTheCompanyHasntImage() {
	TestObject companyAvatar = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//ngx-avatar[@id='Company_hasnt_image']")
	
	if(!TestHelper.isElementPresent(companyAvatar)) {
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='company_back_button']"))
		UserRoleHelper.createCompanyAndGoToTarget();
		checkThatTheCompanyHasntImage();
	}
}

def changeCompanyImage() {
	String userPath = System.getProperty("user.dir")
	String imagePath = userPath + "//Image//test_image.png"
 
	TestObject changeImageAction = TestHelper.getObjectById("//div[@id='change_image_action']");
	TestHelper.clickButton(changeImageAction);
	
	// Image
	TestObject changeProfileImageAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='image_gallery_action']")
	WebUI.uploadFile(changeProfileImageAction, imagePath)

	TestObject selectImageAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='cropper_select_image']")
	TestHelper.clickButton(selectImageAction);

	WebUI.delay(2);
	// Update profile image
	TestObject saveCompanyInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='save_company_info']")
	TestHelper.clickButton(saveCompanyInfo);
	WebUI.delay(3)
	
	TestObject manageEntity = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-Company-back-button']")
	TestHelper.clickButton(manageEntity);
}

def checkThatTheCompanyHasImage() {
	TestObject companyAvatar = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//img[@id='Company_has_image']")	
	if(!TestHelper.isElementPresent(companyAvatar)) {
		TestHelper.thrownException("Company image not updated");
	} else {
		WebUI.delay(2);
		WebUI.closeBrowser();
		println("Image has been updated successfully");
	}
}
