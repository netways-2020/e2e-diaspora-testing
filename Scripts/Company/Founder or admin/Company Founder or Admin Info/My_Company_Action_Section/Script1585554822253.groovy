import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable


WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Login_With_User_Has_Full_Information'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.navigateToUrl(TestHelper.getBlaybel_companyUrl());

checkCompanyActions();
WebUI.closeBrowser();

def checkCompanyActions() {
	TestObject about_title_object = TestHelper.getObjectById("//div[@id='company_about_title_id']")
	TestObject about_subTitle_object = TestHelper.getObjectById("//div[@id='company_about_subTitle_id']")
	TestObject about_more_object = TestHelper.getObjectById("//div[@id='company_about_more_id']")
	
	if(TestHelper.isElementPresent(about_title_object) && TestHelper.isElementPresent(about_subTitle_object)  && TestHelper.isElementPresent(about_more_object)) {
		String aboutTitle = WebUI.getText(about_title_object);
		String aboutSubTitle = WebUI.getText(about_subTitle_object);
		String aboutMoreAction = WebUI.getText(about_more_object);
	
		if(!aboutTitle.equals("About") || !aboutSubTitle.equals("Info, sector and contact details") || !aboutMoreAction.equals("Edit")) {
			TestHelper.thrownException("Invalid about title and subtitle")
		}
		
		TestHelper.clickButton(about_more_object);
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='edit-company-back-button']"));
	} else {
		TestHelper.thrownException("Invalid sections data found");
	}

	TestObject affiliation_title_object = TestHelper.getObjectById("//div[@id='company_affiliation_title_id']")
	TestObject affiliation_subTitle_object = TestHelper.getObjectById("//div[@id='company_affiliation_subTitle_id']")
	TestObject affiliation_more_object = TestHelper.getObjectById("//div[@id='company_affiliation_more_id']")
	
	if(TestHelper.isElementPresent(affiliation_title_object) && TestHelper.isElementPresent(affiliation_subTitle_object) && TestHelper.isElementPresent(affiliation_more_object)) {
		String affiliationTitle = WebUI.getText(affiliation_title_object);
		String affiliationSubTitle = WebUI.getText(affiliation_subTitle_object);
		String affiliationAction = WebUI.getText(affiliation_more_object);

		 if(!affiliationTitle.equals("Member Of") || !affiliationSubTitle.equals("Member in these entities") || !affiliationAction.equals("Edit")) {
			TestHelper.thrownException("Invalid affiliation title and subtitle")
		}
		 
		 TestHelper.clickButton(affiliation_more_object);
		 TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_organization_affiliation_back_action']"));
	} else {
		TestHelper.thrownException("Invalid sections data found");
	}


	TestObject product_title_object = TestHelper.getObjectById("//div[@id='company_product_title_id']")
	TestObject product_subTitle_object = TestHelper.getObjectById("//div[@id='company_product_subTitle_id']")
	TestObject product_more_object = TestHelper.getObjectById("//div[@id='company_product_more_id']")
	
	if(TestHelper.isElementPresent(product_title_object) && TestHelper.isElementPresent(product_subTitle_object) && TestHelper.isElementPresent(product_more_object)) {
		String productTitle = WebUI.getText(product_title_object);
		String productSubTitle = WebUI.getText(product_subTitle_object);
		String productMoreAction = WebUI.getText(product_more_object);
		
		if(!productTitle.equals("Products") || !productSubTitle.equals("Listed by this company") || !productMoreAction.equals("Edit")) {
			TestHelper.thrownException("Invalid product title and subtitle")
		}
		
		TestHelper.clickButton(product_more_object);
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='marketPlace_back_button']"));
	} else {
		TestHelper.thrownException("Invalid sections data found");
	}
	
	
	TestObject service_title_object = TestHelper.getObjectById("//div[@id='company_service_title_id']")
	TestObject service_subTitle_object = TestHelper.getObjectById("//div[@id='company_service_subTitle_id']")
	TestObject service_more_object = TestHelper.getObjectById("//div[@id='company_service_more_id']")
	
	if(TestHelper.isElementPresent(service_title_object) && TestHelper.isElementPresent(service_subTitle_object) && TestHelper.isElementPresent(service_more_object)) {
		String serviceTitle = WebUI.getText(service_title_object);
		String serviceSubTitle = WebUI.getText(service_subTitle_object);
		String serviceMoreAction = WebUI.getText(service_more_object);
		
		if(!serviceTitle.equals("Services") || !serviceSubTitle.equals("Listed by this company") || !serviceMoreAction.equals("Edit")) {
			TestHelper.thrownException("Invalid service title and subtitle")
		}
		
		TestHelper.clickButton(service_more_object);
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='marketPlace_back_button']"));
	} else {
		TestHelper.thrownException("Invalid sections data found");
	}
	
	
	TestObject franchise_title_object = TestHelper.getObjectById("//div[@id='company_franchise_title_id']")
	TestObject franchise_subTitle_object = TestHelper.getObjectById("//div[@id='company_franchise_subTitle_id']")
	TestObject franchise_more_object = TestHelper.getObjectById("//div[@id='company_franchise_more_id']")
	
	if(TestHelper.isElementPresent(franchise_title_object) && TestHelper.isElementPresent(franchise_subTitle_object) && TestHelper.isElementPresent(franchise_more_object)) {
		String franchiseTitle = WebUI.getText(franchise_title_object);
		String franchiseSubTitle = WebUI.getText(franchise_subTitle_object);
		String franchiseMoreAction = WebUI.getText(franchise_more_object);
		
		if(!franchiseTitle.equals("Franchises") || !franchiseSubTitle.equals("Listed by this company") || !franchiseMoreAction.equals("Edit")) {
			TestHelper.thrownException("Invalid franchise title and subtitle")
		}
		
		TestHelper.clickButton(franchise_more_object);
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='marketPlace_back_button']"));
	} else {
		TestHelper.thrownException("Invalid sections data found");
	}
	

	TestObject branches_title_object = TestHelper.getObjectById("//div[@id='company_branches_title_id']")
	TestObject branches_subTitle_object = TestHelper.getObjectById("//div[@id='company_branches_subTitle_id']")
	TestObject branches_more_object = TestHelper.getObjectById("//div[@id='company_branches_more_id']")
	
	if(TestHelper.isElementPresent(branches_title_object) && TestHelper.isElementPresent(branches_subTitle_object) && TestHelper.isElementPresent(branches_more_object)) {
		String branchesTitle = WebUI.getText(branches_title_object);
		String branchesSubTitle = WebUI.getText(branches_subTitle_object);
		String branchesMoreAction = WebUI.getText(branches_more_object);
		
		if(!branchesTitle.equals("Branches") || !branchesSubTitle.equals("Company hierarchy") || !branchesMoreAction.equals("Edit")) {
			TestHelper.thrownException("Invalid branches title and subtitle")
		}
		
		TestHelper.clickButton(branches_more_object);
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-company-branches-back-button']"));
	} else {
		TestHelper.thrownException("Invalid sections data found");
	}

	TestObject teamMembers_title_object = TestHelper.getObjectById("//div[@id='company_team_members_title_id']")
	TestObject teamMembers_subTitle_object = TestHelper.getObjectById("//div[@id='company_team_members_subTitle_id']")
	TestObject teamMembers_more_object = TestHelper.getObjectById("//div[@id='company_team_members_more_id']")
	
	if(TestHelper.isElementPresent(teamMembers_title_object) && TestHelper.isElementPresent(teamMembers_subTitle_object) && TestHelper.isElementPresent(teamMembers_more_object)) {
		String teamMemberTitle = WebUI.getText(teamMembers_title_object);
		String teamMemberSubTitle = WebUI.getText(teamMembers_subTitle_object);
		String teamMemberMoreAction = WebUI.getText(teamMembers_more_object);
		
		if(!teamMemberTitle.equals("Team Members") || !teamMemberSubTitle.equals("Company leadership & staff") || !teamMemberMoreAction.equals("Edit")) {
			TestHelper.thrownException("Invalid Team title and subtitle")
		}
		
		TestHelper.clickButton(teamMembers_more_object);
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-company-member-back-button']"));
	} else {
		TestHelper.thrownException("Invalid sections data found");
	}

	TestObject updates_title_object = TestHelper.getObjectById("//div[@id='company_updates_title_id']")
	TestObject updates_subTitle_object = TestHelper.getObjectById("//div[@id='company_updates_subTitle_id']")
	TestObject updates_more_object = TestHelper.getObjectById("//div[@id='company_updates_more_id']")
	
	if(TestHelper.isElementPresent(updates_title_object) && TestHelper.isElementPresent(updates_subTitle_object) && TestHelper.isElementPresent(updates_more_object)) {
		String updatesTitle = WebUI.getText(updates_title_object);
		String updatesSubTitle = WebUI.getText(updates_subTitle_object);
		String updatesMore = WebUI.getText(updates_more_object);
		
		if(!updatesTitle.equals("Updates") || !updatesSubTitle.equals("Posts & discussions") || !updatesMore.equals("Edit")) {
			TestHelper.thrownException("Invalid updates title and subtitle")
		}

		TestHelper.clickButton(updates_more_object);
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back_button_cardListing']"));
	} else {
		TestHelper.thrownException("Invalid sections data found");
	}

	TestObject media_title_object = TestHelper.getObjectById("//div[@id='company_media_title_id']")
	TestObject media_subTitle_object = TestHelper.getObjectById("//div[@id='company_media_subTitle_id']")
	TestObject media_more_object = TestHelper.getObjectById("//div[@id='company_media_more_id']")
	
	if(TestHelper.isElementPresent(media_title_object) && TestHelper.isElementPresent(media_subTitle_object) && TestHelper.isElementPresent(media_more_object)) {
		String mediaTitle = WebUI.getText(media_title_object);
		String mediaSubTitle = WebUI.getText(media_subTitle_object);
		String mediaMore = WebUI.getText(media_more_object);
		
		if(!mediaTitle.equals("Media Gallery") || !mediaSubTitle.equals("Photos & videos") || !mediaMore.equals("Edit")) {
			TestHelper.thrownException("Invalid media title and subtitle")
		}

		TestHelper.clickButton(media_more_object);
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back_button_gallery']"));
	} else {
		TestHelper.thrownException("Invalid sections data found");
	}
}


