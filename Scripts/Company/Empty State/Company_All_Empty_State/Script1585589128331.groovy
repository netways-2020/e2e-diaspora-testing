import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement

String companyName = 'Netways' + new Date().getTime()
String roleName = 'Founder'

WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Login'), [:], FailureHandling.STOP_ON_FAILURE)
TestHelper.goToManageEntity()
TestObject manageBuinessRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='Business Roles_manage_entity_action']")
TestHelper.clickButton(manageBuinessRoleAction)

TestObject emptyStateObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='no-business-role-empty-state']")
boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)

if (emptyStateObjectPresent == true) {
	println('hasn\'t business roles')

	TestObject emptyStateAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='no-business-role-empty-state-button']")
	WebUI.click(emptyStateAction)
	createNewCompany(companyName, roleName)

} else {
	println('has business roles')

	TestObject addBusinessRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='add-new-business-role-action']")
	WebUI.click(addBusinessRoleAction)
	createNewCompany(companyName, roleName)
}

def createNewCompany(String companyName, String roleName) {
	UserRoleHelper.createCompany(companyName, roleName)
	WebUI.delay(2);
	String companyTarget = companyName + '-' + roleName + '-role'
	TestObject selectedCompany = TestHelper.getObjectById("//div[@id='"+companyTarget+"']")
	TestHelper.clickButton(selectedCompany);
	WebUI.delay(2);
	
	// Check Common Header Exist
	checkCommonHeaderElementExist();
	
	// Check General Info
	UserRoleHelper.checkCompanyGeneralInfoFieldsEmptyState();
	
	// Check Company Section
	checkCompanySectionsEmptyState();
	
	
	// Check Company Section title and subtitle
	checkCompanySectionsTitleAndSubTitle();
	
	// Check SubPages Empty State
	checkSubPagesEmptyState();
	
	// Check Update Empty State
	checkUpdateEmptyState(companyName);

	// Check Media Empty State
	checkMediaEmptyState();
	
	WebUI.delay(2);
	WebUI.closeBrowser();
}

def checkCommonHeaderElementExist() {
	boolean successEmptyFields = true;
	
	TestObject followers_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='follower_count_id_tag_label']")
	TestObject team_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='team_id_tag_label']")

	TestObject companyMenuAction_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='company_menu_action_id']")
	TestObject manage_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Manage_Company_entity_action']")
	TestObject invite_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Invite_Company_entity_action']")	
	
	if(!TestHelper.isElementPresent(followers_label_object)) {
		TestHelper.thrownException("Followers Label not present");
	} else if(!TestHelper.isElementPresent(team_label_object)) {
		TestHelper.thrownException("Team Label not present");
	} else if(!TestHelper.isElementPresent(companyMenuAction_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(manage_entity_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(invite_entity_object)) {
		successEmptyFields = false;
	} 
	
	if(successEmptyFields) {
		println("Company common header info exist");
	} else {
			TestHelper.thrownException("Error: Company commone header info")
	}
}

def checkCompanySectionsEmptyState() {
	String errorMessage = "Error: Company section empty state exist";
	
	// About Section
		TestHelper.checkObjectExit("//div[@id='company_about']", errorMessage);
		//	TestObject company_about_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='company_about']")

	

	// Affiliation
		TestHelper.checkObjectExit("//div[@id='affiliation-section']", errorMessage);
		TestHelper.checkObjectText("//div[@id='no-affiliation-empty-state-title']", "Listed under a chamber, accelerator, or a business support organization?" , errorMessage);
		TestHelper.checkObjectText("//div[@id='no-affiliation-empty-state']", "Add Affiliated Organizations" , errorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no-affiliation-empty-state-image']", "assets/icons/emptyState/Member_Companies.svg", errorMessage)
	
		//	TestObject company_affiliation_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='affiliation-section']")
		//	TestObject company_affiliation_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-affiliation-empty-state']")

	
	// Market Place 	
		TestHelper.checkObjectExit("//div[@id='market-place-no-needs-section']", errorMessage);
		TestHelper.checkObjectText("//div[@id='no-company-needs-empty-state-title']", "Access global markets, Lebanese clients and franchise seekers" , errorMessage);
		TestHelper.checkObjectText("//div[@id='no-company-needs-empty-state']", "Add Business Listings" , errorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no-company-needs-empty-state-image']", "assets/icons/emptyState/Marketplace_Listings.svg", errorMessage)
 
		//	TestObject company_products_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div['company-products-section']")
		//	TestObject company_services_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div['company-services-section']")
		//	TestObject company_franchises_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div['company-franchises-section']")
		//  TestObject company_marketplace_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='market-place-no-needs-section']")
		//  TestObject company_marketplace_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-company-needs-empty-state']")
		
	// Company Branches
		TestHelper.checkObjectExit("//div[@id='company_branches']", errorMessage);
		TestHelper.checkObjectText("//div[@id='no-company-branches-empty-state-title']", "Every branch in a company can indicate its parent branch" , errorMessage);
		TestHelper.checkObjectText("//div[@id='no-company-branches-empty-state']", "Choose Your Parent Branch" , errorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no-company-branches-empty-state-image']", "assets/icons/emptyState/Branches.svg", errorMessage)
		//	TestObject company_brands_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div['brands-section']")
		//	TestObject company_branches_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='company_branches']")
		//	TestObject company_branches_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-company-branches-empty-state']")
			
	// Company Teams
		TestHelper.checkObjectExit("//div[@id='company_teams_members']", errorMessage);
		//  TestObject company_teams_members_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='company_teams_members']")
		//	TestObject company_teams_members_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-company-team-member-empty-state']")
	

	//	TestObject company_join_member_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div['join_company_section']")
	//	TestObject company_join_member_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div['join_company_action']")	

	// Updates
		TestHelper.checkObjectExit("//div[@id='company_updates']", errorMessage);
		TestHelper.checkObjectText("//div[@id='no-company-updates-empty-state-title']", "Notify followers and potential clients of important updates" , errorMessage);
		TestHelper.checkObjectText("//div[@id='no-company-updates-empty-state']", "Post News or Events" , errorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no-company-updates-empty-state-image']", "assets/icons/emptyState/Post_Update.svg", errorMessage)

		//	TestObject company_updates_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='company_updates']")
		//	TestObject company_updates_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-company-updates-empty-state']")
	
		
	// Media Gallery
		TestHelper.checkObjectExit("//div[@id='company_media']", errorMessage);
		TestHelper.checkObjectText("//div[@id='no-company-media-empty-state-title']", "Show sights, achievements or projects with images and videos" , errorMessage);
		TestHelper.checkObjectText("//div[@id='no-company-media-empty-state']", "Add Photos or Videos" , errorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no-company-media-empty-state-image']", "assets/icons/emptyState/Media_Gallery.svg", errorMessage)

		//	TestObject company_media_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='company_media']")
		//	TestObject company_media_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-company-media-empty-state']")
	
	
		println("Company section empty state exist");
			
}


def checkCompanySectionsTitleAndSubTitle() {
	TestObject about_title_object = TestHelper.getObjectById("//div[@id='company_about_title_id']")
	TestObject about_subTitle_object = TestHelper.getObjectById("//div[@id='company_about_subTitle_id']")
	TestObject about_icon_object = TestHelper.getObjectById("//div[@id='company_about_title_id_icon']")
	
	if(TestHelper.isElementPresent(about_title_object) && TestHelper.isElementPresent(about_subTitle_object)) {
		String aboutTitle = WebUI.getText(about_title_object);
		String aboutSubTitle = WebUI.getText(about_subTitle_object);
		String aboutIcon =  WebUI.getText(about_icon_object);
		if(!aboutTitle.equals("About") || !aboutSubTitle.equals("Info, sector and contact details") || !aboutIcon.equals("assets/icons/diasporaIcon/About_Active.svg")) {
			TestHelper.thrownException("Invalid about title and subtitle")
		}
	}

	TestObject affiliation_title_object = TestHelper.getObjectById("//div[@id='company_affiliation_title_id']")
	TestObject affiliation_subTitle_object = TestHelper.getObjectById("//div[@id='company_affiliation_subTitle_id']")
	TestObject affiliation_icon_object = TestHelper.getObjectById("//div[@id='company_affiliation_title_id_icon']")
	
	if(TestHelper.isElementPresent(affiliation_title_object) && TestHelper.isElementPresent(affiliation_subTitle_object)) {
		String affiliationTitle = WebUI.getText(affiliation_title_object);
		String affiliationSubTitle = WebUI.getText(affiliation_subTitle_object);
		String affiliationIcon = WebUI.getText(affiliation_icon_object);
		
		 if(!affiliationTitle.equals("Member Of") || !affiliationSubTitle.equals("Member in these entities") || !affiliationIcon.equals("assets/icons/diasporaIcon/affiliation.svg")) {
			TestHelper.thrownException("Invalid affiliation title and subtitle")
		}
	}

	TestObject market_place_title_object = TestHelper.getObjectById("//div[@id='company_market_place_title_id']")
	TestObject market_palce_subTitle_object = TestHelper.getObjectById("//div[@id='company_market_place_subTitle_id']")
	TestObject market_place_icon_object = TestHelper.getObjectById("//div[@id='company_market_place_title_id_icon']")
	
	if(TestHelper.isElementPresent(market_place_title_object) && TestHelper.isElementPresent(market_palce_subTitle_object)) {
		String marketPlaceTitle = WebUI.getText(market_place_title_object);
		String marketPlaceSubTitle = WebUI.getText(market_palce_subTitle_object);
		String marketPlaceIcon = WebUI.getText(market_place_icon_object);
		
		if(!marketPlaceTitle.equals("Marketplace Listings") || !marketPlaceSubTitle.equals("Business offerings") || !marketPlaceIcon.equals("assets/icons/diasporaIcon/marketplace.svg")) {
			TestHelper.thrownException("Invalid marketplace title and subtitle")
		}
	}

	TestObject branches_title_object = TestHelper.getObjectById("//div[@id='company_branches_title_id']")
	TestObject branches_subTitle_object = TestHelper.getObjectById("//div[@id='company_branches_subTitle_id']")
	TestObject branches_icon_object = TestHelper.getObjectById("//div[@id='company_branches_title_id_icon']")
	
	if(TestHelper.isElementPresent(branches_title_object) && TestHelper.isElementPresent(branches_subTitle_object)) {
		String branchesTitle = WebUI.getText(branches_title_object);
		String branchesSubTitle = WebUI.getText(branches_subTitle_object);
		String branchesIcon = WebUI.getText(branches_icon_object);

		if(!branchesTitle.equals("Branches") || !branchesSubTitle.equals("Company hierarchy") || !branchesIcon.equals("assets/icons/diasporaIcon/branches_gray.svg")) {
			TestHelper.thrownException("Invalid branches title and subtitle")
		}
	}

	TestObject teamMembers_title_object = TestHelper.getObjectById("//div[@id='company_team_members_title_id']")
	TestObject teamMembers_subTitle_object = TestHelper.getObjectById("//div[@id='company_team_members_subTitle_id']")
	TestObject teamMembers_icon_object = TestHelper.getObjectById("//div[@id='company_team_members_title_id_icon']")
	
	if(TestHelper.isElementPresent(teamMembers_title_object) && TestHelper.isElementPresent(teamMembers_subTitle_object)) {
		String teamMemberTitle = WebUI.getText(teamMembers_title_object);
		String teamMemberSubTitle = WebUI.getText(teamMembers_subTitle_object);
		String teamMemberIcon = WebUI.getText(teamMembers_icon_object);
		
		if(!teamMemberTitle.equals("Team Members") || !teamMemberSubTitle.equals("Company leadership & staff") || !teamMemberIcon.equals("assets/icons/diasporaIcon/People_Active.svg")) {
			TestHelper.thrownException("Invalid Team title and subtitle")
		}
	}

	TestObject updates_title_object = TestHelper.getObjectById("//div[@id='company_updates_title_id']")
	TestObject updates_subTitle_object = TestHelper.getObjectById("//div[@id='company_updates_subTitle_id']")
	TestObject updates_icon_object = TestHelper.getObjectById("//div[@id='company_updates_title_id_icon']")
	
	if(TestHelper.isElementPresent(updates_title_object) && TestHelper.isElementPresent(updates_subTitle_object)) {
		String updatesTitle = WebUI.getText(updates_title_object);
		String updatesSubTitle = WebUI.getText(updates_subTitle_object);
		String updatesIcon = WebUI.getText(updates_icon_object);
		
		if(!updatesTitle.equals("Updates") || !updatesSubTitle.equals("Posts & discussions") || !updatesIcon.equals("assets/icons/diasporaIcon/News_Active.svg")) {
			TestHelper.thrownException("Invalid updates title and subtitle")
		}

	}

	TestObject media_title_object = TestHelper.getObjectById("//div[@id='company_media_title_id']")
	TestObject media_subTitle_object = TestHelper.getObjectById("//div[@id='company_media_subTitle_id']")
	TestObject media_icon_object = TestHelper.getObjectById("//div[@id='company_media_title_id_icon']")
	
	if(TestHelper.isElementPresent(media_title_object) && TestHelper.isElementPresent(media_subTitle_object)) {
		String mediaTitle = WebUI.getText(media_title_object);
		String mediaSubTitle = WebUI.getText(media_subTitle_object);
		String mediaIcon = WebUI.getText(media_icon_object);
		
		if(!mediaTitle.equals("Media Gallery") || !mediaSubTitle.equals("Photos & videos") || !mediaIcon.equals("assets/icons/diasporaIcon/View_Gallery.svg")) {
			TestHelper.thrownException("Invalid media title and subtitle")
		}

	}
}


def checkSubPagesEmptyState() {
 		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Manage_Company_entity_action']"))
		 checkManageEntityIcon();
		 
	// Check Manage Team Members Page
		String pendingTeamErrorMessage = "Error: Invalid Pending Team Empty State";
		String approvedMemberErrorMessage = "Error: Invalid Approved Member Empty State";
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Team Members_manage_entity_action']"))
		 
		TestHelper.checkObjectText("//div[@id='no_pending_member_empty_state_title']", "No Pending Requests" , pendingTeamErrorMessage);
		TestHelper.checkObjectText("//div[@id='no_pending_member_empty_state_subtitle']", "There are currently no pending requests. We will notify you when a new request arrives" , pendingTeamErrorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no_pending_member_empty_state_image']", "assets/icons/emptyState/Person.svg", pendingTeamErrorMessage)
	
		
		TestHelper.clickButton(TestHelper.getObjectById("//ion-segment-button[@id='current_employee_tab_id']"))
		
		
		TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='company_current_member']"), 'abcdefgh123');
		WebUI.delay(2);
 
		TestHelper.checkObjectText("//div[@id='no_approved_member_empty_state_title']", "No Members" , approvedMemberErrorMessage);
		TestHelper.checkObjectText("//div[@id='no_approved_member_empty_state_subtitle']", "Approved join requests will appear here" , approvedMemberErrorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no_approved_member_empty_state_image']", "assets/icons/emptyState/Person.svg", approvedMemberErrorMessage)
 
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-company-member-back-button']"))
				
	// Check User Follower/Following Page
		String administratorListPageErrorMessage = "Error: Invalid Administrator List Empty State";
		String addAdministratorPageErrorMessage = "Error: Invalid Add Administrator Empty State";
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Page Administrators_manage_entity_action']"))
		 
		TestHelper.checkObjectText("//div[@id='no_admin_member_empty_state_title']", "No Page Administrators" , administratorListPageErrorMessage);
		TestHelper.checkObjectText("//div[@id='no_admin_member_empty_state_subtitle']", "This page doesn’t currently have any administrators. You can assign a page member or invite someone via email" , administratorListPageErrorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no_admin_member_empty_state_image']", "assets/icons/emptyState/Person.svg", administratorListPageErrorMessage)
		TestHelper.checkObjectText("//div[@id='manage-admin-company-add-new-admin-empty-state']", "Add Page Administrator" , administratorListPageErrorMessage);
		
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-admin-company-add-new-admin-empty-state']"))
		
		TestHelper.checkObjectText("//div[@id='add_admin_member_empty_state_title']", "No Members" , addAdministratorPageErrorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='add_admin_member_empty_state_image']", "assets/icons/emptyState/Person.svg", addAdministratorPageErrorMessage)
 		
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='add-admin-company-back-button']"))
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-admin-company-back-button']"))
		
	// Market Place Page
		String manageProductsErrorMessage = "Error: Invalid Manage Products Empty State";
		String manageServicesPageErrorMessage = "Error: Invalid Manage Services Empty State";
		String manageFranchisesPageErrorMessage = "Error: Invalid Manage Franchises Empty State";
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Marketplace Listings_manage_entity_action']"))
		 
		TestHelper.checkObjectText("//div[@id='no_marketPlace_product_empty_state_title']", "No Products Listed" , manageProductsErrorMessage);
		TestHelper.checkObjectText("//div[@id='no_marketPlace_product_empty_state_subtitle']", "You haven’t added any products yet under this company. Tap the button below to get started" , manageProductsErrorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no_marketPlace_product_empty_state_image']", "assets/icons/emptyState/Product.svg", manageProductsErrorMessage)
		TestHelper.checkObjectText("//div[@id='market_place_add_product_action']", "Add Product", manageProductsErrorMessage)
		
		TestHelper.clickButton(TestHelper.getObjectById("//ion-segment-button[@id='market_place_service_tab']"))
		
		TestHelper.checkObjectText("//div[@id='no_marketPlace_service_empty_state_title']", "No Services Listed" , manageServicesPageErrorMessage);
		TestHelper.checkObjectText("//div[@id='no_marketPlace_service_empty_state_subtitle']", "You haven’t added any services yet under this company. Tap the button below to get started" , manageServicesPageErrorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no_marketPlace_service_empty_state_image']", "assets/icons/emptyState/Product.svg", manageServicesPageErrorMessage)
		TestHelper.checkObjectText("//div[@id='market_place_add_service_action']", "Add Service", manageServicesPageErrorMessage)

		
		TestHelper.clickButton(TestHelper.getObjectById("//ion-segment-button[@id='market_place_franchise_tab']"))
		
		TestHelper.checkObjectText("//div[@id='no_marketPlace_franchise_empty_state_title']", "No Franchises Listed" , manageFranchisesPageErrorMessage);
		TestHelper.checkObjectText("//div[@id='no_marketPlace_franchise_empty_state_subtitle']", "You haven’t added any franchises yet under this company. Tap the button below to get started" , manageFranchisesPageErrorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no_marketPlace_franchise_empty_state_image']", "assets/icons/emptyState/Product.svg", manageFranchisesPageErrorMessage)
		TestHelper.checkObjectText("//div[@id='market_place_add_franchise_action']", "Add Franchise", manageFranchisesPageErrorMessage)
 		 
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='marketPlace_back_button']"))
		
		
	// Check Manage Affiliation Page
	   String managePendingAffiliationPageErrorMessage = "Error: Invalid Manage Pending Affiliation Empty State";
	   String manageCurrentAffiliationPageErrorMessage = "Error: Invalid Manage Current Affiliation Empty State";
	   
	   TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Member Of_manage_entity_action']"))
		
	   TestHelper.checkObjectText("//div[@id='no-pending-affiliation-request-empty-state_title']", "No Pending Requests" , managePendingAffiliationPageErrorMessage);
	   TestHelper.checkObjectText("//div[@id='no-pending-affiliation-request-empty-state_subtitle']", "Organizations that request showing your company in their affiliation section will be listed here for your approval" , managePendingAffiliationPageErrorMessage);
	   TestHelper.checkObjectImageSrc("//div[@id='no-pending-affiliation-request-empty-state_image']", "assets/icons/emptyState/List.svg", managePendingAffiliationPageErrorMessage)
	   TestHelper.checkObjectText("//div[@id='no-pending-affiliation-request-empty-state']", "Add An Affiliation", managePendingAffiliationPageErrorMessage)
	   
	   TestHelper.clickButton(TestHelper.getObjectById("//ion-segment-button[@id='manage_network_current_affiliation']"))
	  
	   TestHelper.checkObjectText("//div[@id='no-current-affiliation-request-empty-state_title']", "No Organizations" , manageCurrentAffiliationPageErrorMessage);
	   TestHelper.checkObjectText("//div[@id='no-current-affiliation-request-empty-state_subtitle']", "Approved join requests will appear here" , manageCurrentAffiliationPageErrorMessage);
	   TestHelper.checkObjectImageSrc("//div[@id='no-current-affiliation-request-empty-state_image']", "assets/icons/emptyState/List.svg", manageCurrentAffiliationPageErrorMessage)
	   TestHelper.checkObjectText("//div[@id='no-current-affiliation-request-empty-state']", "Add An Affiliation", manageCurrentAffiliationPageErrorMessage)

	   TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_organization_affiliation_back_action']"))
	   
	 // Check Manage Branches
	   String manageIncommingBranchesPageErrorMessage = "Error: Invalid Manage Incomming Branche Empty State";
	   String manageOutgoingBranchesPageErrorMessage =  "Error: Invalid Manage Sent Branche Empty State";
	   String manageCurrentBranchesPageErrorMessage =  "Error: Invalid Manage Current Branche Empty State";

	   TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Branches_manage_entity_action']"))
	   
	   TestHelper.checkObjectText("//div[@id='manage_branche_incomming_empty_state_title']", "No Incoming Requests" , manageIncommingBranchesPageErrorMessage);
	   TestHelper.checkObjectText("//div[@id='manage_branche_incomming_empty_state_subtitle']", "Requests to join your company as a branch will appear here" , manageIncommingBranchesPageErrorMessage);
	   TestHelper.checkObjectImageSrc("//div[@id='manage_branche_incomming_empty_state_image']", "assets/icons/emptyState/List.svg", manageIncommingBranchesPageErrorMessage)

	   TestHelper.clickButton(TestHelper.getObjectById("//ion-segment-button[@id='manage_company_branch_outgoing_request']"))
	   
	   TestHelper.checkObjectText("//div[@id='manage_branche_outgoing_empty_state_title']", "No Outgoing Requests" , manageOutgoingBranchesPageErrorMessage);
	   TestHelper.checkObjectText("//div[@id='manage_branche_outgoing_empty_state_subtitle']", "Outgoing requests will appear here" , manageOutgoingBranchesPageErrorMessage);
	   TestHelper.checkObjectImageSrc("//div[@id='manage_branche_outgoing_empty_state_image']", "assets/icons/emptyState/List.svg", manageOutgoingBranchesPageErrorMessage)
	   TestHelper.checkObjectText("//div[@id='manage_company_branch_add_parent_company_outgoing_empty_state']", "Add Parent Company" , manageOutgoingBranchesPageErrorMessage);
	   	   
	   TestHelper.clickButton(TestHelper.getObjectById("//ion-segment-button[@id='manage_company_branch_current_request']"))
	   
	   TestHelper.checkObjectText("//div[@id='manage_branche_current_empty_state_title']", "No Company Branches Listed" , manageCurrentBranchesPageErrorMessage);
	   TestHelper.checkObjectText("//div[@id='manage_branche_current_empty_state_subtitle']", "Start by adding your parent company. When your request is approved, the company’s structure will appear here" , manageCurrentBranchesPageErrorMessage);
	   TestHelper.checkObjectImageSrc("//div[@id='manage_branche_current_empty_state_image']", "assets/icons/emptyState/List.svg", manageCurrentBranchesPageErrorMessage)
	   TestHelper.checkObjectText("//div[@id='manage_company_branch_add_parent_company_current_empty_state']", "Add Parent Company" , manageCurrentBranchesPageErrorMessage);
  	   
	   TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-company-branches-back-button']"))
	   TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-Company-back-button']"))   
	   
	   
	   // Check follwers
	   String followerErrorMessage = "Error: Invalid Followers Page Empty State";
		   
	   WebUI.delay(1)
	   TestHelper.clickButton(TestHelper.getObjectById("//span[@id='follower_count_id_tag_label']"));
	   TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='search-company-follower-input']"), 'abcdefgh123');
	   WebUI.delay(2);

	   TestHelper.checkObjectText("//div[@id='no_followers_title']", "No Results Found" , followerErrorMessage);
	   TestHelper.checkObjectImageSrc("//div[@id='no_followers_image']", "assets/icons/emptyState/Person.svg", followerErrorMessage)
 
	   TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back-follower-id']"));
}

def checkUpdateEmptyState(String companyName) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='no-company-updates-empty-state']"));
	long now = new Date().getTime();
	String postTitle = 'New post title add now_' + now;
	String postDescription = 'New post description add now_' + now;
		
	TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Create Post_user_action']"));
	
	UserActionsHelper.addPost(postTitle, postDescription)
	WebUI.delay(2);
	TestHelper.verifyCardExist(companyName, postTitle, null, false);
	
	TestObject manageCardObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='company_manage_updates']")
	boolean manageCardObjectExist = TestHelper.isElementPresent(manageCardObject);
	if(!manageCardObjectExist) {
		TestHelper.thrownException("Manage card button not exist");
	}
	
	TestHelper.clickButton(manageCardObject);
	String cartId = postTitle + '_descriptionId';
	TestHelper.clickButton(TestHelper.getObjectById("//span[@id='" + cartId + "']"));
		

	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='card_details_manage_button']"))
	WebUI.delay(1);
	TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Delete Post_user_action']"))
	WebUI.delay(1);
	
	WebElement deleteCardConfirmation = TestHelper.getItem('remove-new-card', 0)
	deleteCardConfirmation.click();
	
	WebUI.delay(1);
	
	String updateErrorMessage = "Error: Invalid Updates List Empty State";
	TestHelper.checkObjectText("//div[@id='no_updates_empty_state_title']", "Nothing to Show Here" , updateErrorMessage);
	TestHelper.checkObjectText("//div[@id='no_updates_empty_state_subtitle']", "There are no entries to display on this page" , updateErrorMessage);
	TestHelper.checkObjectImageSrc("//div[@id='no_updates_empty_state_image']", "assets/icons/emptyState/List.svg", updateErrorMessage)

	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back_button_cardListing']"))
}

def checkMediaEmptyState() {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='no-company-media-empty-state']"));

	long now = new Date().getTime();
	String imageCaption = 'caption' + now;
	UserActionsHelper.addMedia(imageCaption);
	TestObject addPhotoAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='company_manage_media']")
	WebUI.click(addPhotoAction)
	
	TestHelper.checkIfImageAddedSuccessfully(true, 0);
	TestHelper.removeImage();
	
	WebUI.delay(1);
	String mediaErrorMessage = "Error: Invalid Media Empty State";
	TestHelper.checkObjectText("//div[@id='no_gallery_images_empty_state_title']", "No Photo Or Video Added" , mediaErrorMessage);
	TestHelper.checkObjectImageSrc("//div[@id='no_gallery_images_empty_state_image']", "assets/icons/emptyState/List.svg", mediaErrorMessage)

	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back_button_gallery']"))
}


def checkManageEntityIcon() {
	String manage_entity_error_message = 'Invalid manage entity info';
	
	TestHelper.checkObjectText("//div[@id='Company Information_manage_entity_action']", "Company Information" , manage_entity_error_message);
	TestHelper.checkObjectText("//div[@id='Company Information_manage_entity_subtitle_action']", "Logo, info, sector, keywords, brands…" , manage_entity_error_message);
	TestHelper.checkObjectImageSrc("//div[@id='Company Information_manage_entity_icon_action']", "assets/icons/diasporaIcon/Basic_Information.svg", manage_entity_error_message)
	   
	TestHelper.checkObjectText("//div[@id='Team Members_manage_entity_action']", "Team Members" , manage_entity_error_message);
	TestHelper.checkObjectText("//div[@id='Team Members_manage_entity_subtitle_action']", "Manage roles, admins & join requests" , manage_entity_error_message);
	TestHelper.checkObjectImageSrc("//div[@id='Team Members_manage_entity_icon_action']", "assets/icons/diasporaIcon/Team_Members.svg", manage_entity_error_message)
	 
	TestHelper.checkObjectText("//div[@id='Page Administrators_manage_entity_action']", "Page Administrators" , manage_entity_error_message);
	TestHelper.checkObjectText("//div[@id='Page Administrators_manage_entity_subtitle_action']", "Assign or remove page administrators" , manage_entity_error_message);
	TestHelper.checkObjectImageSrc("//div[@id='Page Administrators_manage_entity_icon_action']", "assets/icons/diasporaIcon/Page_Administrator.svg", manage_entity_error_message)
	 
	TestHelper.checkObjectText("//div[@id='Marketplace Listings_manage_entity_action']", "Marketplace Listings" , manage_entity_error_message);
	TestHelper.checkObjectText("//div[@id='Marketplace Listings_manage_entity_subtitle_action']", "Your products, services & franchises" , manage_entity_error_message);
	TestHelper.checkObjectImageSrc("//div[@id='Marketplace Listings_manage_entity_icon_action']", "assets/icons/diasporaIcon/Marketplace_Listings.svg", manage_entity_error_message)
 
	TestHelper.checkObjectText("//div[@id='Member Of_manage_entity_action']", "Member Of" , manage_entity_error_message);
	TestHelper.checkObjectText("//div[@id='Member Of_manage_entity_subtitle_action']", "Organizations you’re a member of" , manage_entity_error_message);
	TestHelper.checkObjectImageSrc("//div[@id='Member Of_manage_entity_icon_action']", "assets/icons/diasporaIcon/Member_Companies.svg", manage_entity_error_message)
 
	TestHelper.checkObjectText("//div[@id='Branches_manage_entity_action']", "Branches" , manage_entity_error_message);
	TestHelper.checkObjectText("//div[@id='Branches_manage_entity_subtitle_action']", "Company structure & branches" , manage_entity_error_message);
	TestHelper.checkObjectImageSrc("//div[@id='Branches_manage_entity_icon_action']", "assets/icons/diasporaIcon/Chapters.svg", manage_entity_error_message)
} 
 

 


