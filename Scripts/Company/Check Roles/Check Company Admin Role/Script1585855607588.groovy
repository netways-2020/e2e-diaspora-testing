import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

String companyName = 'Netways_Company' + new Date().getTime();

String founderRoleName = 'Founder'
String memberRoleName = 'Member'
 
// Founder User
long date = new Date().getTime();
String registeredEmail = 'netways_user' + date + '@gmail.com'
String password = 'cvW8qx4B2o3F4VwP/kNsqA==';
String firstName = 'FirstName_' + date;
String lastName = 'LastName_' + date;
String memberName = firstName + ' ' + lastName;
String city = 'Hermel';

// Member User
String founderRegisteredEmail = 'netways_founder' + date + '@gmail.com'
String founderFirstName = 'FirstName_' + date;
String founderLastName = 'LastName_' + date;


TestHelper.registerNewUser(founderRegisteredEmail, password, founderFirstName, founderLastName, city, true)
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_tab_id']"))
TestHelper.goToManageEntity()
TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'Business Roles_manage_entity_action\']'))

// Founder Role
checkBusinessRole(companyName, founderRoleName);

// Create Profile and join company
createProfileAndJoinCompany(companyName, memberRoleName, registeredEmail, password, firstName, lastName, city)
//joinCompany(companyName, memberRoleName);
addNewRegisterAsAdmin(founderRegisteredEmail, password, memberName);

loginWithCreatedUserAndCheckAdmin(companyName, registeredEmail, password, memberRoleName)
checkFounderSections();

WebUI.delay(2);
WebUI.closeBrowser();

// Create Company
def checkBusinessRole(String companyName, String founderRoleName) {
	TestObject emptyStateObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='no-business-role-empty-state']")
	boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)
	
	if (emptyStateObjectPresent == true) {
		println('hasn\'t business roles')
	
		TestObject emptyStateAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='no-business-role-empty-state-button']")
		WebUI.click(emptyStateAction)
		createNewCompany(companyName, founderRoleName)
	
	} else {
		println('has business roles')
	
		TestObject addBusinessRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='add-new-business-role-action']")
		WebUI.click(addBusinessRoleAction)
		createNewCompany(companyName, founderRoleName)
	}	
}

def createNewCompany(String companyName, String founderRoleName) {
	UserRoleHelper.createCompany(companyName, founderRoleName)
	WebUI.delay(2);
	String organizationTarget = companyName + '-' + founderRoleName + '-role'
	TestObject selectedOrganization = TestHelper.getObjectById("//div[@id='"+organizationTarget+"']")
	TestHelper.clickButton(selectedOrganization);
	
	companyUrl = WebUI.getUrl();
	
	WebUI.delay(2);
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='company_back_button']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_business_role_back_action']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-User-back-button']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_manage_accounts']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Log Out']"));
}

def createProfileAndJoinCompany(String companyName, String employeeRoleName, String registeredEmail, String password, String firstName, String lastName, String city) {
	TestHelper.registerNewUser(registeredEmail, password, firstName, lastName, city, false)
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_tab_id']"))
	TestHelper.goToManageEntity()
	TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'Business Roles_manage_entity_action\']'))
		
	TestObject emptyStateObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='no-business-role-empty-state']")
	boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)
	
	if (emptyStateObjectPresent == true) {
		println('hasn\'t business roles')
	
		TestObject emptyStateAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='no-business-role-empty-state-button']")
		WebUI.click(emptyStateAction)
		joinCompany(companyName, employeeRoleName)
	
	} else {
		println('has business roles')
	
		TestObject addBusinessRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='add-new-business-role-action']")
		WebUI.click(addBusinessRoleAction)
		joinCompany(companyName, employeeRoleName)
	}
}

def joinCompany(String companyName, String employeeRoleName) {
	TestObject searchForCompanyInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='searchForCompanyInput']")
	TestHelper.setInputValue(searchForCompanyInput, companyName)

	String companyId = companyName + '_search_for_company';
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='"+companyId+"']"))
	
	UserRoleHelper.joinCompany(employeeRoleName)
	String companyTarget = companyName + '-' + employeeRoleName + '-role'
	TestObject selectedCompany = TestHelper.getObjectById("//div[@id='"+companyTarget+"']")
	TestHelper.clickButton(selectedCompany);
	checkNonAdminEmptyState();
	
	WebUI.delay(2);
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='company_back_button']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='company_back_button']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_business_role_back_action']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-User-back-button']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_manage_accounts']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Log Out']"));
}

def addNewRegisterAsAdmin(String founderEmail, String password, String memberName) {
	TestHelper.loginWithSpecificEmail(founderEmail, password);
	WebUI.navigateToUrl(companyUrl);
		TestHelper.goToCompanyManageEntity();
	TestObject manageTeamMemberAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Team Members_manage_entity_action']")
	TestHelper.clickButton(manageTeamMemberAction);
	
	TestObject pendingMemberSearchInput = TestHelper.getObjectById("//input[@id='company_pending_member']")
	TestHelper.setInputValue(pendingMemberSearchInput, memberName)
	WebUI.delay(2);
	
	String pendingEmployeeId = memberName + '_pending_employee_name';
	String pendingEmployeeActionId = memberName + '_pending_employee_action';
	
	
	TestObject newMember = TestHelper.getObjectById("//div[@id='"+pendingEmployeeId+"']")
	
	boolean newMemberExist = TestHelper.isElementPresent(newMember);
	if(newMemberExist) {
		TestObject pendingMemberAction = TestHelper.getObjectById("//div[@id='"+pendingEmployeeActionId+"']")
		TestHelper.clickButton(pendingMemberAction);
		
		TestObject pendingMemberApproveRequestAction = TestHelper.getObjectById("//a[@id='Approve Request_user_action']")
		TestHelper.clickButton(pendingMemberApproveRequestAction);
		
		WebUI.delay(2);
		
		
		TestObject manageCompanyMembersBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-company-member-back-button']")
		TestHelper.clickButton(manageCompanyMembersBackButton);

		assignAdminToCompany(memberName);
		
	} else {
		TestHelper.thrownException("No employee with this name exist");
	}
}

def assignAdminToCompany(String memberName) {
	
	TestObject manageAdminMemberAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Page Administrators_manage_entity_action']")
	TestHelper.clickButton(manageAdminMemberAction);
	
	// Check empty state add admin
	TestObject manage_admin_add_admin = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-admin-company-add-new-admin-empty-state']")
	boolean checkManage_admin_admin = TestHelper.isElementPresent(manage_admin_add_admin);
	
	if(checkManage_admin_admin) {
		TestHelper.clickButton(manage_admin_add_admin)
		assignMemberAsAdmin(memberName);
	} else {
		TestObject manage_admin_add_admin_from_header = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-admin-company-add-new-admin']")
		TestHelper.clickButton(manage_admin_add_admin_from_header)	
		assignMemberAsAdmin(memberName);
	}
}

def assignMemberAsAdmin(String memberName) {
 	 
// Assigne member as admin
	WebUI.delay(2);
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='assign-admin-search-input']"), memberName);
	WebUI.delay(2);
	
	String memberId = memberName + '_assign_admin_role_action';
	TestObject memberAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + memberId + "']")
	TestHelper.clickButton(memberAction);

	// View Profile	
	TestObject viewCurrentMemberAction = TestHelper.getObjectById("//a[@id='View Profile_user_action']")
	TestHelper.clickButton(viewCurrentMemberAction);
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_backIcon_id']"))

	// Assign Admin
	TestHelper.clickButton(memberAction);
	TestObject assignAdminMemberAction = TestHelper.getObjectById("//a[@id='Assign As Administrator_user_action']")
	TestHelper.clickButton(assignAdminMemberAction);
	WebUI.delay(2);
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='add-admin-company-back-button']"))
}

def checkAdmin(String memberName) {
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='manage-company-admin-search-input']"), memberName);
	WebUI.delay(2);
	
	String memberId = memberName + '_admin_role';
	TestObject memberObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + memberId + "']")
	boolean checkAdminExist = TestHelper.isElementPresent(memberObject);
	
	if(checkAdminExist) {
		println("Memeber assign as admin succesfully");
	} else {
		TestHelper.thrownException("Error during assing member as admin");
	}
}

def loginWithCreatedUserAndCheckAdmin(String companyName, String email, String password, String memberRoleName) {
	WebUI.closeBrowser();
	
	WebUI.delay(3);
	TestHelper.loginWithSpecificUserNameAndPassword(email, password, true)
	 
	TestHelper.goToManageEntity()
	TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'Business Roles_manage_entity_action\']'))

	String companyTarget = companyName + '-' + memberRoleName + '-role'
	TestObject selectedOrganization = TestHelper.getObjectById("//div[@id='"+companyTarget+"']")
	TestHelper.clickButton(selectedOrganization);
}

// Founder Sections
def checkFounderSections() {
	checkCommonHeaderElementExist();
	checkCompanySectionsEmptyState();
	checkCompanySectionsTitleAndSubTitle();
}

def checkCommonHeaderElementExist() {
	boolean successEmptyFields = true;
	
	TestObject followers_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='follower_count_id_tag_label']")
	TestObject team_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='team_id_tag_label']")

	TestObject companyMenuAction_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='company_menu_action_id']")
	TestObject manage_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Manage_Company_entity_action']")
	TestObject invite_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Invite_Company_entity_action']")	
	
	if(!TestHelper.isElementPresent(followers_label_object)) {
		TestHelper.thrownException("Followers Label not present");
	} else if(!TestHelper.isElementPresent(team_label_object)) {
		TestHelper.thrownException("Team Label not present");
	} else if(!TestHelper.isElementPresent(companyMenuAction_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(manage_entity_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(invite_entity_object)) {
		successEmptyFields = false;
	} 
	
	if(successEmptyFields) {
		println("Company common header info exist");
	} else {
			TestHelper.thrownException("Error: Company commone header info")
	}
}

def checkCompanySectionsEmptyState() {

	boolean successEmptyFields = true;
	
	TestObject company_about_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='company_about']")
	
	TestObject company_affiliation_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='affiliation-section']")
	TestObject company_affiliation_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-affiliation-empty-state']")
		
//	TestObject company_products_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div['company-products-section']")
//	TestObject company_services_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div['company-services-section']")
//	TestObject company_franchises_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div['company-franchises-section']")
	TestObject company_marketplace_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='market-place-no-needs-section']")
	TestObject company_marketplace_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-company-needs-empty-state']")
		
//	TestObject company_brands_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div['brands-section']")
	TestObject company_branches_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='company_branches']")
	TestObject company_branches_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-company-branches-empty-state']")
	
	TestObject company_teams_members_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='company_teams_members']")
//	TestObject company_teams_members_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-company-team-member-empty-state']")
	
//	TestObject company_join_member_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div['join_company_section']")
//	TestObject company_join_member_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div['join_company_action']")	
		
	TestObject company_updates_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='company_updates']")
	TestObject company_updates_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-company-updates-empty-state']")
	
	TestObject company_media_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='company_media']")
	TestObject company_media_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-company-media-empty-state']")
	
	
	// Check Object Exist	
	if(!TestHelper.isElementPresent(company_about_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(company_affiliation_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(company_affiliation_empty_state_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(company_marketplace_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(company_marketplace_empty_state_object)) {
		successEmptyFields = false;
	} 
//	else if(!TestHelper.isElementPresent(company_brands_object)) {
//		successEmptyFields = false;
//	} 
	else if(!TestHelper.isElementPresent(company_branches_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(company_branches_empty_state_object)) {
		successEmptyFields = false;
	} 
	else if(!TestHelper.isElementPresent(company_teams_members_object)) {
		successEmptyFields = false;
	}
//	 else if(!TestHelper.isElementPresent(company_teams_members_empty_state_object)) {
//		successEmptyFields = false;
//	} 
//	  else if(!TestHelper.isElementPresent(company_join_member_object)) {
//		successEmptyFields = false;
//	} else if(!TestHelper.isElementPresent(company_join_member_empty_state_object)) {
//		successEmptyFields = false;
//	} 
	
	else if(!TestHelper.isElementPresent(company_updates_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(company_updates_empty_state_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(company_media_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(company_media_empty_state_object)) {
		successEmptyFields = false;
	}
	
	
	if(successEmptyFields) {
			println("Company section empty state exist");
	} else {
			TestHelper.thrownException("Error: Company section empty state exist")
	}
	
}

def checkCompanySectionsTitleAndSubTitle() {
	TestObject about_title_object = TestHelper.getObjectById("//div[@id='company_about_title_id']")
	TestObject about_subTitle_object = TestHelper.getObjectById("//div[@id='company_about_subTitle_id']")

	if(TestHelper.isElementPresent(about_title_object) && TestHelper.isElementPresent(about_subTitle_object)) {
		String aboutTitle = WebUI.getText(about_title_object);
		String aboutSubTitle = WebUI.getText(about_subTitle_object);
	
		if(!aboutTitle.equals("About") && !aboutSubTitle.equals("Info, sector and contact details")) {
			TestHelper.thrownException("Invalid about title and subtitle")
		}
	}

	TestObject affiliation_title_object = TestHelper.getObjectById("//div[@id='company_affiliation_title_id']")
	TestObject affiliation_subTitle_object = TestHelper.getObjectById("//div[@id='company_affiliation_subTitle_id']")

	if(TestHelper.isElementPresent(affiliation_title_object) && TestHelper.isElementPresent(affiliation_subTitle_object)) {
		String affiliationTitle = WebUI.getText(affiliation_title_object);
		String affiliationSubTitle = WebUI.getText(affiliation_subTitle_object);

		 if(!affiliationTitle.equals("Member Of") && !affiliationSubTitle.equals("Member in these entities")) {
			TestHelper.thrownException("Invalid affiliation title and subtitle")
		}
	}

	TestObject market_place_title_object = TestHelper.getObjectById("//div[@id='company_market_place_title_id']")
	TestObject market_palce_subTitle_object = TestHelper.getObjectById("//div[@id='company_market_place_subTitle_id']")

	if(TestHelper.isElementPresent(market_place_title_object) && TestHelper.isElementPresent(market_palce_subTitle_object)) {
		String marketPlaceTitle = WebUI.getText(market_place_title_object);
		String marketPlaceSubTitle = WebUI.getText(market_palce_subTitle_object);

		if(!marketPlaceTitle.equals("Marketplace Listings") && !marketPlaceSubTitle.equals("Business offerings")) {
			TestHelper.thrownException("Invalid marketplace title and subtitle")
		}
	}

	TestObject branches_title_object = TestHelper.getObjectById("//div[@id='company_branches_title_id']")
	TestObject branches_subTitle_object = TestHelper.getObjectById("//div[@id='company_branches_subTitle_id']")

	if(TestHelper.isElementPresent(branches_title_object) && TestHelper.isElementPresent(branches_subTitle_object)) {
		String branchesTitle = WebUI.getText(branches_title_object);
		String branchesSubTitle = WebUI.getText(branches_subTitle_object);

		if(!branchesTitle.equals("Branches") && !branchesSubTitle.equals("Company hierarchy")) {
			TestHelper.thrownException("Invalid branches title and subtitle")
		}
	}

	TestObject teamMembers_title_object = TestHelper.getObjectById("//div[@id='company_team_members_title_id']")
	TestObject teamMembers_subTitle_object = TestHelper.getObjectById("//div[@id='company_team_members_subTitle_id']")

	if(TestHelper.isElementPresent(teamMembers_title_object) && TestHelper.isElementPresent(teamMembers_subTitle_object)) {
		String teamMemberTitle = WebUI.getText(teamMembers_title_object);
		String teamMemberSubTitle = WebUI.getText(teamMembers_subTitle_object);

		if(!teamMemberTitle.equals("Team Members") && !teamMemberSubTitle.equals("Company leadership & staff")) {
			TestHelper.thrownException("Invalid Team title and subtitle")
		}
	}

	TestObject updates_title_object = TestHelper.getObjectById("//div[@id='company_updates_title_id']")
	TestObject updates_subTitle_object = TestHelper.getObjectById("//div[@id='company_updates_subTitle_id']")

	if(TestHelper.isElementPresent(updates_title_object) && TestHelper.isElementPresent(updates_subTitle_object)) {
		String updatesTitle = WebUI.getText(updates_title_object);
		String updatesSubTitle = WebUI.getText(updates_subTitle_object);

		if(!updatesTitle.equals("Updates") && !updatesSubTitle.equals("Posts & discussions")) {
			TestHelper.thrownException("Invalid updates title and subtitle")
		}

	}

	TestObject media_title_object = TestHelper.getObjectById("//div[@id='company_media_title_id']")
	TestObject media_subTitle_object = TestHelper.getObjectById("//div[@id='company_media_subTitle_id']")
	
	if(TestHelper.isElementPresent(media_title_object) && TestHelper.isElementPresent(media_subTitle_object)) {
		String mediaTitle = WebUI.getText(media_title_object);
		String mediaSubTitle = WebUI.getText(media_subTitle_object);

		if(!mediaTitle.equals("Media Gallery") && !mediaSubTitle.equals("Photos & videos")) {
			TestHelper.thrownException("Invalid media title and subtitle")
		}

	}
}


// Member Sections
def checkNonAdminEmptyState() {
		TestObject companyMenuAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='company_menu_action_id']")
//	TestObject follow_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Follow_Company_entity_action']")
	TestObject message_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Message_Company_entity_action']")
	TestObject following_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Following_Company_entity_action']")
	
	TestObject followers_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='follower_count_id_tag_label']")
	TestObject team_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='team_id_tag_label']")

//	TestObject company_join_member_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div['join_company_section']")
//	TestObject company_join_member_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div['join_company_action']")
	
	if(!TestHelper.isElementPresent(followers_label_object)) {
		TestHelper.thrownException("Followers Label not present");
	} else if(!TestHelper.isElementPresent(team_label_object)) {
		TestHelper.thrownException("Team Label not present");
	} else if(!TestHelper.isElementPresent(following_entity_object)) {
		TestHelper.thrownException("Following Action not present");
	} else if(!TestHelper.isElementPresent(message_entity_object)) {
		TestHelper.thrownException("Message Action not present");
	}  
//	else if(!TestHelper.isElementPresent(company_join_member_object)) {
//		TestHelper.thrownException("Join member section not present");
//	} else if(!TestHelper.isElementPresent(company_join_member_empty_state_object)) {
//		TestHelper.thrownException("Join member section empty state not present");
//	}
//	
	if(TestHelper.isElementPresent(companyMenuAction)) {
		TestHelper.clickButton(companyMenuAction);

		TestObject reportCompanyAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Report A Problem_user_action']")
		TestObject shareCompanyAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Share_user_action']")
		TestObject claimCompanyAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Claim this company_user_action']")
		
		if(!TestHelper.isElementPresent(reportCompanyAction) || !TestHelper.isElementPresent(shareCompanyAction) || !TestHelper.isElementPresent(claimCompanyAction)) {
			TestHelper.thrownException("Company Action not present");
		} else {
			print("Check Complete")
		}
	} else {
		TestHelper.thrownException("Menu Action not present");
	}
}

