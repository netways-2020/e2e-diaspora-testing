import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

String companyName = 'Netways' + new Date().getTime()
String roleName = 'Member'

// Register new user and go to my profile tab
long date = new Date().getTime();
String registeredEmail = 'netways_user' + date + '@gmail.com'
String password = 'cvW8qx4B2o3F4VwP/kNsqA=='

String firstName = 'FirstName_' + date;
String lastName = 'LastName_' + date;
String fullName = firstName + ' ' + lastName;
String city = 'Hermel';
 
TestHelper.registerNewUser(registeredEmail, password, firstName, lastName, city, true);
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_tab_id']"))
TestHelper.goToManageEntity()
TestObject manageBuinessRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='Business Roles_manage_entity_action']")
TestHelper.clickButton(manageBuinessRoleAction)

TestObject emptyStateObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='no-business-role-empty-state']")
boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)

if (emptyStateObjectPresent == true) {
	println('hasn\'t business roles')

	TestObject emptyStateAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='no-business-role-empty-state-button']")
	WebUI.click(emptyStateAction)
	createNewCompany(fullName, companyName, roleName)

} else {
	println('has business roles')

	TestObject addBusinessRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='add-new-business-role-action']")
	WebUI.click(addBusinessRoleAction)
	createNewCompany(fullName, companyName, roleName)
}

def createNewCompany(String fullName, String companyName, String roleName) {
	UserRoleHelper.createCompanyAsEmployee(companyName, roleName)
	WebUI.delay(2);
	String companyTarget = companyName + '-' + roleName + '-role'
	TestObject selectedCompany = TestHelper.getObjectById("//div[@id='"+companyTarget+"']")
	TestHelper.clickButton(selectedCompany);
	WebUI.delay(2);
	
	checkFollowers(fullName);
	
	WebUI.delay(2);
	WebUI.closeBrowser();
}

def checkFollowers(String fullName) {
	TestObject followers_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='follower_count_id_tag_label']")
	if(!TestHelper.isElementPresent(followers_label_object)) {
		TestHelper.thrownException("Followers Label not present");
	}
	
	TestHelper.clickButton(followers_label_object);
	
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='search-company-follower-input']"), fullName);
	WebUI.delay(2);
	String followerId = fullName + '_company_followers';
	TestObject followerObject = TestHelper.getObjectById("//div[@id='" + followerId + "']");
		
	if(!TestHelper.isElementPresent(followerObject)) {
		TestHelper.thrownException("Follower not exist");
	} else {
		print("Check Complete")
	}
}

