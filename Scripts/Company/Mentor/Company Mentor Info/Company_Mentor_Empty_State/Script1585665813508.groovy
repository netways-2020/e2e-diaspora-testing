import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

String companyName = 'Netways' + new Date().getTime()
String roleName = 'Mentor'
 
WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Register_new_user'), [:], FailureHandling.STOP_ON_FAILURE)
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_tab_id']"))

TestHelper.goToManageEntity()
TestObject manageBuinessRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='Business Roles_manage_entity_action']")
TestHelper.clickButton(manageBuinessRoleAction) 

TestObject emptyStateObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='no-business-role-empty-state']")
boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)

if (emptyStateObjectPresent == true) {
	println('hasn\'t  business roles')

	TestObject emptyStateAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='no-business-role-empty-state-button']")
	WebUI.click(emptyStateAction)
	createNewCompany(companyName, roleName)

} else {
	println('has business roles')

	TestObject addBusinessRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='add-new-business-role-action']")
	WebUI.click(addBusinessRoleAction)
	createNewCompany(companyName, roleName)
}

def createNewCompany(String companyName, String roleName) {
	UserRoleHelper.createCompanyAsMentor(companyName, roleName)
	WebUI.delay(2);
	String companyTarget = companyName + '-' + roleName + '-role'
	println(companyTarget);
	TestObject selectedCompany = TestHelper.getObjectById("//div[@id='"+companyTarget+"']")
	WebUI.delay(2);
	TestHelper.clickButton(selectedCompany);
	WebUI.delay(2);
	
	checkEmptyState();	
	WebUI.closeBrowser();
}

def checkEmptyState() {
	TestObject companyMenuAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='company_menu_action_id']")
//	TestObject follow_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Follow_Company_entity_action']")
	TestObject message_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Message_Company_entity_action']")
	TestObject following_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Following_Company_entity_action']")
	
	TestObject followers_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='follower_count_id_tag_label']")
	TestObject team_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='team_id_tag_label']")

//	TestObject company_join_member_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div['join_company_section']")
//	TestObject company_join_member_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div['join_company_action']")
	
	if(!TestHelper.isElementPresent(followers_label_object)) {
		TestHelper.thrownException("Followers Label not present");
	} else if(!TestHelper.isElementPresent(team_label_object)) {
		TestHelper.thrownException("Team Label not present");
	} else if(!TestHelper.isElementPresent(following_entity_object)) {
		TestHelper.thrownException("Following Action not present");
	} else if(!TestHelper.isElementPresent(message_entity_object)) {
		TestHelper.thrownException("Message Action not present");
	}  
//	else if(!TestHelper.isElementPresent(company_join_member_object)) {
//		TestHelper.thrownException("Join member section not present");
//	} else if(!TestHelper.isElementPresent(company_join_member_empty_state_object)) {
//		TestHelper.thrownException("Join member section empty state not present");
//	}
//	
	if(TestHelper.isElementPresent(companyMenuAction)) {
		TestHelper.clickButton(companyMenuAction);

		TestObject reportCompanyAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Report A Problem_user_action']")
		TestObject shareCompanyAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Share_user_action']")
		TestObject claimCompanyAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Claim this company_user_action']")
		
		if(!TestHelper.isElementPresent(reportCompanyAction) || !TestHelper.isElementPresent(shareCompanyAction) || !TestHelper.isElementPresent(claimCompanyAction)) {
			TestHelper.thrownException("Company Action not present");
		} else {
			print("Check Complete")
		}
	} else {
		TestHelper.thrownException("Menu Action not present");
	}
}