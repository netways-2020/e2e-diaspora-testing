import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable


// Register new user and go to my profile tab
long date = new Date().getTime();
String registeredEmail = 'netways_user' + date + '@gmail.com'
String password = 'cvW8qx4B2o3F4VwP/kNsqA=='

String firstName = 'FirstName_' + date;
String lastName = 'LastName_' + date;
String fullName = firstName + ' ' + lastName;
String city = 'Hermel';
 

TestHelper.registerNewUser(registeredEmail, password, firstName, lastName, city, true);
WebUI.navigateToUrl(TestHelper.getNetways_companyUrl());
checkEmptyState(fullName);

WebUI.delay(2);
WebUI.closeBrowser();

def checkEmptyState(String fullName) {
	String errorMessage = 'Invalid Join Company Action Title and SubTitle';
	TestObject companyMenuAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='company_menu_action_id']")
	TestObject follow_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Follow_Company_entity_action']")
	TestObject message_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Message_Company_entity_action']")
	 
	TestObject followers_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='follower_count_id_tag_label']")
	TestObject team_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='team_id_tag_label']")

	TestObject company_join_member_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='join_company_section']")
	TestObject company_join_member_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='join_company_action']")
	
	
	if(!TestHelper.isElementPresent(followers_label_object)) {
		TestHelper.thrownException("Followers Label not present");
	} else if(!TestHelper.isElementPresent(team_label_object)) {
		TestHelper.thrownException("Team Label not present");
	} else if(!TestHelper.isElementPresent(follow_entity_object)) {
		TestHelper.thrownException("Follow Action not present");
	} else if(!TestHelper.isElementPresent(message_entity_object)) {
		TestHelper.thrownException("Message Action not present");
	}
	else if(!TestHelper.isElementPresent(company_join_member_object)) {
		TestHelper.thrownException("Join member section not present");
	} else if(!TestHelper.isElementPresent(company_join_member_empty_state_object)) {
		TestHelper.thrownException("Join member section empty state not present");
	}
	
	TestHelper.checkObjectText("//div[@id='join_company_action']", "Member of this Company?" , errorMessage);
	TestHelper.checkObjectText("//div[@id='join_company_action_subTitle']", "Join now and indicate your role" , errorMessage);
	TestHelper.checkObjectImageSrc("//div[@id='companyPage_Join Company_action_icon_Id']", "assets/icons/diasporaIcon/OwnerOrEmployee.svg", errorMessage)

	TestHelper.checkObjectText("//div[@id='companyPage_Join Company_action_title_Id']", "Join Company" , errorMessage);
	TestHelper.checkObjectText("//div[@id='companyPage_Join Company_action_subTitle_Id']", "Tap here to get started" , errorMessage);

 
	if(TestHelper.isElementPresent(companyMenuAction)) {
		TestHelper.clickButton(companyMenuAction);

		TestObject reportCompanyAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Report A Problem_user_action']")
		TestObject shareCompanyAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Share_user_action']")
		TestObject claimCompanyAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Claim this company_user_action']")
		
		if(!TestHelper.isElementPresent(reportCompanyAction) || !TestHelper.isElementPresent(shareCompanyAction) || !TestHelper.isElementPresent(claimCompanyAction)) {
			TestHelper.thrownException("Company Action not present");
		} else {
			print("Check Complete")
		}
	} else {
		TestHelper.thrownException("Menu Action not present");
	}
}

def checkFollower(TestObject followers_label_object, String fullName) {
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='cancel_action_sheet_id']"))
	
	TestObject follow_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Follow_Company_entity_action']");
	TestHelper.clickButton(follow_entity_object);
	WebUI.delay(2);

	
	TestHelper.clickButton(followers_label_object);
	
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='search-company-follower-input']"), fullName);
	WebUI.delay(2);
	String followerId = fullName + '_company_followers';
	TestObject followerObject = TestHelper.getObjectById("//div[@id='" + followerId + "']");
		
	if(!TestHelper.isElementPresent(followerObject)) {
		TestHelper.thrownException("Follower not exist");
	} else {
		print("Check Complete")
	}
}