import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

String companyName = 'Netways' + new Date().getTime()
String roleName = 'Employee';

String reportDescription = 'Report description for '+ companyName;
String reportContact = 'Report Contact for '+ companyName;


// Login and go to my profile tab
WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Login'), [:], FailureHandling.STOP_ON_FAILURE)
TestHelper.goToManageEntity()
TestObject manageBuinessRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='Business Roles_manage_entity_action']")
TestHelper.clickButton(manageBuinessRoleAction)

TestObject emptyStateObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='no-business-role-empty-state']")
boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)

if (emptyStateObjectPresent == true) {
	println('hasn\'t business roles')

	TestObject emptyStateAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='no-business-role-empty-state-button']")
	WebUI.click(emptyStateAction)
	createNewCompany(companyName, roleName, reportDescription, reportContact)

} else {
	println('has business roles')

	TestObject addBusinessRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='add-new-business-role-action']")
	WebUI.click(addBusinessRoleAction)
	createNewCompany(companyName, roleName,reportDescription, reportContact)
}

def createNewCompany(String companyName, String roleName, String reportDescription, String reportContact ) {
	UserRoleHelper.createCompanyAsEmployee(companyName, roleName)
	WebUI.delay(2);
	String companyTarget = companyName + '-' + roleName + '-role'
	println(companyTarget);
	TestObject selectedCompany = TestHelper.getObjectById("//div[@id='"+companyTarget+"']")
	TestHelper.clickButton(selectedCompany);
	WebUI.delay(2);
	
	reportCompany(reportDescription, reportContact)
	
	WebUI.closeBrowser();
}

def reportCompany(String reportDescription, String reportContact ) {
	TestObject companyMenuAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='company_menu_action_id']")
 
		if(TestHelper.isElementPresent(companyMenuAction)) {
		TestHelper.clickButton(companyMenuAction);

		TestObject reportCompanyAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Report A Problem_user_action']")
		
		if(!TestHelper.isElementPresent(reportCompanyAction)) {
			TestHelper.thrownException("Company Action not present");
		} else {
			TestHelper.clickButton(reportCompanyAction);
			UserActionsHelper.sendReport(reportDescription, reportContact);
		}
	} else {
		TestHelper.thrownException("Menu Action not present");
	}
}


