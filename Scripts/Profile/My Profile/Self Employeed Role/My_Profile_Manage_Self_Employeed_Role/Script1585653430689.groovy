import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import cucumber.api.java.Before
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement

long date = new Date().getTime();

String registeredEmail = 'netways_user' + date + '@gmail.com'
String password = 'cvW8qx4B2o3F4VwP/kNsqA=='

String firstName = 'FirstName_' + date;
String lastName = 'LastName_' + date;
String fullName = firstName + ' ' + lastName;
String city = 'Hermel';


// Login and go to my profile tab
TestHelper.registerNewUser(registeredEmail, password, firstName, lastName, city, true)
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_tab_id']"))
String oldEntityRole = UserRoleHelper.getFeaturedRole();

TestHelper.goToManageEntity();
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Basic Information_manage_entity_action']"))


setProfession();
checkProfessionAndSetFeatured();
unFeatureSelfEmployeedRole(oldEntityRole);
removeSelfEmployeedRole();

WebUI.delay(2);
WebUI.closeBrowser();

def setProfession() {
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='profession-section']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Actuary_item']"))
	TestHelper.clickButton(TestHelper.getObjectById( "//button[@id='save_user_info']"))
	WebUI.delay(3)
}

def checkProfessionAndSetFeatured() {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Business Roles_manage_entity_action']"))
	 
	TestObject selfEmployeedObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Self Employed-Actuary-role']")
	boolean selfEmployeeRoleExist = TestHelper.isElementPresent(selfEmployeedObject);

	if(!selfEmployeeRoleExist) {
      TestHelper.thrownException("Self employeed role not exit");
	}

	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Self Employed-options-icon']"));
	TestObject setAsFeatureAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Set Role As Featured_user_action']")
	TestHelper.clickButton(setAsFeatureAction);
	 
	WebUI.delay(1.5)
	
	// Check if successfully set as feature
	TestObject manageBusinessRoleBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_business_role_back_action']")
	TestHelper.clickButton(manageBusinessRoleBackButton);
	TestObject manageEntityBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-User-back-button']")
	TestHelper.clickButton(manageEntityBackButton);
 
	UserRoleHelper.checkIfSuccessfullySetAsFeature('Self Employed');
	
	TestHelper.goToManageEntity();
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Business Roles_manage_entity_action']"));
}

   
def unFeatureSelfEmployeedRole(String oldEntityRole) {
	WebUI.delay(1.5)
	
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Self Employed-options-icon']"));
	TestObject unfeatureAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Unfeature Role_user_action']")
	TestHelper.clickButton(unfeatureAction);

	WebUI.delay(1.5)
	
	// Check if successfully set as unfeature
	TestObject manageBusinessRoleBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_business_role_back_action']")
	TestHelper.clickButton(manageBusinessRoleBackButton);
	TestObject manageEntityBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-User-back-button']")
	TestHelper.clickButton(manageEntityBackButton);
		
	UserRoleHelper.checkIfSuccessfullySetAsUnFeature(oldEntityRole);	
}

def removeSelfEmployeedRole() {
	TestHelper.goToManageEntity();	
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Basic Information_manage_entity_action']"))

	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='profession-section']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='clear_selected_value']"));
	WebUI.delay(1);

	TestHelper.clickButton(TestHelper.getObjectById( "//button[@id='save_user_info']"))
	WebUI.delay(3)


	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Business Roles_manage_entity_action']"))
	
    TestObject selfEmployeedObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Self Employed-Actuary-role']")
    boolean selfEmployeeRoleExist = TestHelper.isElementPresent(selfEmployeedObject);
	
    if(selfEmployeeRoleExist) {
		TestHelper.thrownException("Self employeed still exit");
    } else {
		TestObject manageBusinessRoleBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_business_role_back_action']")
		TestHelper.clickButton(manageBusinessRoleBackButton);
		TestObject manageEntityBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-User-back-button']")
		TestHelper.clickButton(manageEntityBackButton);
		println('Test Complete');
	}

}
 