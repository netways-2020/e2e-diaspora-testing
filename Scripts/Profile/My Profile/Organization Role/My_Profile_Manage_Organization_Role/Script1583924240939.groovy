import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement


String organizationName = 'Netways_Organization' + new Date().getTime();
String roleName = "Founder"
String newRoleName = "Employee"

 
// Login and go to my profile tab
WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Login'), [:], FailureHandling.STOP_ON_FAILURE)
String oldEntityRole = UserRoleHelper.getFeaturedRole();

TestHelper.goToManageEntity();
TestObject manageNetworkRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Network Roles_manage_entity_action']")
TestHelper.clickButton(manageNetworkRoleAction)
 

// Check if user has business Role
TestObject emptyStateObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-network-role-empty-state']")
boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)


if (emptyStateObjectPresent) {
	println("hasn't network roles")
	TestObject emptyStateAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-network-role-empty-state-button']")
	WebUI.click(emptyStateAction)	
	
	// Create organization, add organization role
	UserRoleHelper.createOrganization(organizationName, roleName)
	
 	// Edit Organization Role
	editOrganizationRole(organizationName, newRoleName);
	checkOrganizationRoleIsEditedSuccessfully(organizationName, newRoleName);
	
	// Feature-Unfeature Role
	setOrganizationRoleAsFeatureRole(organizationName)
	unFeaturOrganizationRole(organizationName, oldEntityRole)
	
	// Remove Organization Role
	removeOrganizationRole(organizationName)

} else {
	println("has network roles")
	TestObject addNetworkRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='add-new-network-role-action']")
	TestHelper.clickButton(addNetworkRoleAction)
	
	// Create organization, add organization role
	UserRoleHelper.createOrganization(organizationName, roleName)
	
 	// Edit Organization Role
	editOrganizationRole(organizationName, newRoleName);
	checkOrganizationRoleIsEditedSuccessfully(organizationName, newRoleName);
	
	// Feature-Unfeature Role
	setOrganizationRoleAsFeatureRole(organizationName)
	unFeaturOrganizationRole(organizationName, oldEntityRole)
	
	// Remove Organization Role
	removeOrganizationRole(organizationName)
}


 
// Edit Business rle
def editOrganizationRole(String organizationName, String newRoleName) {
	WebUI.delay(1.5)
	
	String manageNetworkRoleImageId = organizationName + '-options-icon'
	String manageNetworkImageXpath = "//div[@id='"+manageNetworkRoleImageId+"']"
	TestObject manageRoleImage = TestHelper.getObjectById(manageNetworkImageXpath)
	TestHelper.clickButton(manageRoleImage);
	

	// Edit Network Role
	TestObject editRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Edit Role_user_action']")
	TestHelper.clickButton(editRoleAction);
	
  
	TestObject networkRolePositionType = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='network-role-position-type']")
	TestHelper.clickButton(networkRolePositionType)
	
	TestObject networkRolePositionTypeValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Member_item']")
	TestHelper.clickButton(networkRolePositionTypeValue)
	
	 
	TestObject businessRolePositionName = new TestObject().addProperty("xpath", ConditionType.EQUALS,  "//input[@id='network-role-position-name']")
	TestHelper.setInputValue(businessRolePositionName, newRoleName)
	
	TestObject updateBusinessRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='update_network_role']")
	TestHelper.clickButton(updateBusinessRoleAction)
	
	TestObject manageDiplomaticRoleBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_network_role_back_action']")
	WebUI.waitForElementPresent(manageDiplomaticRoleBackButton, 3);
}

def checkOrganizationRoleIsEditedSuccessfully(String organizationName, String newRoleName) {
	String networkRoleId = organizationName + '-' + newRoleName + '-role';
	String networkRoleIdXpath = "//div[@id='"+networkRoleId+"']"
	TestObject networkRoleObject = TestHelper.getObjectById(networkRoleIdXpath)
	
	String editedRoleValue = WebUI.getText(networkRoleObject)
	
	boolean roleSuccessfullyEdited = TestHelper.verifyElementValue(editedRoleValue, newRoleName);
	
	if(roleSuccessfullyEdited) {
	  println("Role has been successfully Edited")
	} else {
	  println("An expected error has been occurred")
	}
}


// Feature-Unfeature role
def setOrganizationRoleAsFeatureRole(String organizationName) {
	String manageNetworkRoleImageId = organizationName + '-options-icon'
	String manageRoleImageXpath = "//div[@id='"+manageNetworkRoleImageId+"']"
	TestObject manageRoleImage = TestHelper.getObjectById(manageRoleImageXpath)
	TestHelper.clickButton(manageRoleImage);
	
	TestObject setAsFeatureAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Set Role As Featured_user_action']")
	TestHelper.clickButton(setAsFeatureAction);

	WebUI.delay(1.5)
	
	// Check if successfully set as feature
	TestObject manageNetworkRoleBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_network_role_back_action']")
	TestHelper.clickButton(manageNetworkRoleBackButton);
	
	TestObject manageEntityBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-User-back-button']")
	TestHelper.clickButton(manageEntityBackButton);
 
	UserRoleHelper.checkIfSuccessfullySetAsFeature(organizationName);
	
	TestHelper.goToManageEntity();
	TestObject manageNetworkRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Network Roles_manage_entity_action']")
	TestHelper.clickButton(manageNetworkRoleAction)
}

def unFeaturOrganizationRole(String organizationName,String oldEntityRole) {
	WebUI.delay(1.5)
	String manageNetworkRoleImageId = organizationName + '-options-icon'
	String manageRoleImageXpath = "//div[@id='"+manageNetworkRoleImageId+"']"
	TestObject manageRoleImage = TestHelper.getObjectById(manageRoleImageXpath)
	TestHelper.clickButton(manageRoleImage);
	
	TestObject unfeatureAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Unfeature Role_user_action']")
	TestHelper.clickButton(unfeatureAction);
	
	WebUI.delay(1.5)
	
	// Check if successfully set as feature
	TestObject manageNetworkRoleBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_network_role_back_action']")
	TestHelper.clickButton(manageNetworkRoleBackButton);
	
	TestObject manageEntityBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-User-back-button']")
	TestHelper.clickButton(manageEntityBackButton);
	
	UserRoleHelper.checkIfSuccessfullySetAsUnFeature(oldEntityRole);
	
	TestHelper.goToManageEntity();
	TestObject manageNetworkRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Network Roles_manage_entity_action']")
	TestHelper.clickButton(manageNetworkRoleAction)
}


// Remove business role
def removeOrganizationRole(String organizationName) {
	String manageNetworkRoleImageId = organizationName + '-options-icon'
	String manageRoleImageXpath = "//div[@id='"+manageNetworkRoleImageId+"']"
	TestObject manageRoleImage = TestHelper.getObjectById(manageRoleImageXpath)
	
	boolean foundElement = TestHelper.isElementPresent(manageRoleImage)
	if(foundElement) {
		println("Element Present")
	}
	
	TestHelper.clickButton(manageRoleImage);
	 	
	TestObject deleteRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Delete Role_user_action']")
	TestHelper.clickButton(deleteRoleAction);
		
	WebElement deleteNetworkConfirmation = TestHelper.getItem('delete_network_action', 0)
	deleteNetworkConfirmation.click();
	
	WebUI.delay(2)
	boolean foundDeletedElement = TestHelper.isElementPresent(manageRoleImage)
	
	if(!foundDeletedElement) {
	  println("Role has been successfully deleted")
	  TestObject manageNetworkRoleBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_network_role_back_action']")
		TestHelper.clickButton(manageNetworkRoleBackButton);
		
		TestObject manageEntityBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-User-back-button']")
		TestHelper.clickButton(manageEntityBackButton);
		WebUI.closeBrowser();
	} else {
		TestHelper.thrownException("Error during delete network role")
	}
	

}
	  
