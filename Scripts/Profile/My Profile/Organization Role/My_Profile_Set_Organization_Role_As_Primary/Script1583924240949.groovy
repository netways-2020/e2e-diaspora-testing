import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.helper.KeywordHelper
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.junit.After as After
import org.openqa.selenium.Keys as Keys

// Login and go to my profile tab
WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Login'), [:], FailureHandling.STOP_ON_FAILURE)

TestHelper.goToManageEntity();
TestObject manageNetworkRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Network Roles_manage_entity_action']")
TestHelper.clickButton(manageNetworkRoleAction)

// Check if user has organization Roles
TestObject emptyStateObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-network-role-empty-state']")
boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)

String organizationName = 'Netways_Organization' + new Date().getTime();
String roleName = 'Founder';

if (emptyStateObjectPresent) {
	println("hasn't network roles")
	TestObject emptyStateAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-network-role-empty-state-button']")
	WebUI.click(emptyStateAction)	
	
	// Create organization, add business role
	UserRoleHelper.createOrganization(organizationName, roleName)
	
	// Set netword role as primary role
	setOrganizationRoleAsPrimary(organizationName);
	UserRoleHelper.checkIfSuccessfullySetAsFeature(organizationName);
	WebUI.closeBrowser()

} else {
	println("has network roles")
	TestObject addNetworkRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='add-new-network-role-action']")
	TestHelper.clickButton(addNetworkRoleAction)
	
	// Create organization, add business role
	UserRoleHelper.createOrganization(organizationName, roleName)
	
	// Set netword role as primary role
	setOrganizationRoleAsPrimary(organizationName);
	UserRoleHelper.checkIfSuccessfullySetAsFeature(organizationName);
	WebUI.closeBrowser();
}


def setOrganizationRoleAsPrimary(String organizationName) {
	String businessRoleImageId = organizationName + '-feature-icon'
	String businessRoleImageXpath = "//div[@id='"+businessRoleImageId+"']"
		
	TestObject manageNetworkRoleBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_network_role_back_action']")
	WebUI.click(manageNetworkRoleBackButton)

	TestObject manageEntity = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-User-back-button']")
	TestHelper.clickButton(manageEntity);

	TestObject managePrimayEntity = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-primary-role']")
	TestHelper.clickButton(managePrimayEntity);
	 	
	TestObject setAsPrimaryRoleImage = TestHelper.getObjectById(businessRoleImageXpath)
	TestHelper.clickButton(setAsPrimaryRoleImage);
	
	WebUI.delay(1)
		
	TestObject managePrimaryRoleBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_primary_role_back_action']")
	TestHelper.clickButton(managePrimaryRoleBackButton);
}

 
