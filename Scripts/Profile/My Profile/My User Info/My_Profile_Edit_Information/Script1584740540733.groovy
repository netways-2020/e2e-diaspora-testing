import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

// Define Varibales
TestObject fistName_input, lastName_input, about_textArea, sector_button, profession_button;
TestObject countryOfOrigin_button, cityOfOrigin_button, countryOfResidence_button, cityOfResidence_button;
TestObject phoneCountryCode_button, phoneNumber_input, email_input;
TestObject facebook_input, linkedin_input, instagram_input, twitter_input;

List<String> countries = TestHelper.prepareCountries();
Map<String, List<String>> cities = TestHelper.prepareCities();
 
 
// New Value
long newDate =  new Date().getTime();

// Country and city
String selectedCountryOfOrigine = TestHelper.getRandomValueFromList(countries);
String selectedCountryOfResidence = TestHelper.getRandomValueFromList(countries);

List<String> citiesOfCountryOfOrigine = cities.get(selectedCountryOfOrigine)
List<String> citiesOfCountryOfResidence = cities.get(selectedCountryOfResidence)
		
String selectedCityOfOrigine =  TestHelper.getRandomValueFromList(citiesOfCountryOfOrigine);
String selectedCityOfResidence =  TestHelper.getRandomValueFromList(citiesOfCountryOfResidence);
 
  
// Login and go to my profile tab
long date = new Date().getTime();
String registeredEmail = 'netways_user' + date + '@gmail.com'
String password = 'cvW8qx4B2o3F4VwP/kNsqA==';

String firstName = 'FirstName_' + date;
String lastName = 'LastName_' + date;
String fullName = firstName + ' ' + lastName;
String city = 'Hermel';
String roleName = 'SupportStaffRole';
String officeName = "Blaybel Office Don't Remove It";

TestHelper.registerNewUser(registeredEmail, password, firstName, lastName, city, true);

TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_tab_id']"))

TestHelper.goToManageEntity();
TestObject manageBasicInformationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Basic Information_manage_entity_action']")
TestHelper.clickButton(manageBasicInformationAction)


// Edit Profile and check values
prepareEditProfileFields();
changeUserInfoValue(countries, cities, newDate, selectedCountryOfOrigine, selectedCountryOfResidence, selectedCityOfOrigine, selectedCityOfResidence);
checkEditedProfileValue(newDate, selectedCountryOfOrigine, selectedCountryOfResidence, selectedCityOfOrigine, selectedCityOfResidence);

// Clear Edited Fields
TestHelper.goToManageEntity();
TestHelper.clickButton(manageBasicInformationAction)
clearProfileFields(firstName, lastName, selectedCountryOfResidence);
UserRoleHelper.checkUserGeneralInfoFieldsEmptyState();
WebUI.closeBrowser();

def prepareEditProfileFields() {
	// About Info
	fistName_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='first-name-id']")
	lastName_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='last-name-id']") 
	about_textArea = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//textarea[@id='about-section']")
	
	sector_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='sector-section']")
	profession_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='profession-section']")
	
	// Place Info
	countryOfOrigin_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='user-country-of-origin']")
	cityOfOrigin_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='user-city-of-origin']")
	countryOfResidence_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='user-country-of-residence']")
	cityOfResidence_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='user-city-of-residence']")
	
	// Contact Info
	phoneCountryCode_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='user-phone-country-code']")
	phoneNumber_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='user-phone-number']")
	email_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='user-email']")
	
	// Social Media
	facebook_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='user-social-media-facebook']")
	linkedin_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='user-social-media-linkedin']")
	instagram_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='user-social-media-instagram']")
	twitter_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='user-social-media-twitter']")
	
}

// Edit Fields
def changeUserInfoValue(List<String> countries, Map<String, List<String>> cities, long newDate, String selectedCountryOfOrigine, 
						String selectedCountryOfResidence, String selectedCityOfOrigine, String selectedCityOfResidence) {
	
	// User info
		TestHelper.setInputValue(fistName_input, newFirstName + newDate)
		TestHelper.setInputValue(lastName_input, newLastName + newDate)
		TestHelper.setInputValue(about_textArea, newAbout + newDate)
		
	// Profession
		TestHelper.clickButton(profession_button)
		TestObject professionItem = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Actuary_item']")
		WebUI.click(professionItem)
		
	// Sector
		TestHelper.clickButton(sector_button)
		TestObject artSector = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Arts_sector']")
		TestObject animationSector = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Animation_sector']")
		
		WebUI.click(artSector)
		WebUI.click(animationSector)
		
	// Country/City of origin
		TestHelper.clickButton(countryOfOrigin_button)
		TestObject countryOfOriginSearchInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='country-search-input']")
		TestHelper.setInputValue(countryOfOriginSearchInput, selectedCountryOfOrigine)
		TestObject countryOfOriginObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+selectedCountryOfOrigine+"']")
		WebUI.delay(2.5)
		WebUI.click(countryOfOriginObject)
		
		TestHelper.clickButton(cityOfOrigin_button)
		TestObject cityOfOriginSearchInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='city-search-input']")
		TestHelper.setInputValue(cityOfOriginSearchInput, selectedCityOfOrigine)
		TestObject cityOfOriginObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+selectedCityOfOrigine+"']")
		WebUI.delay(1)
		WebUI.click(cityOfOriginObject)
		
	// Country/City of origin
		TestHelper.clickButton(countryOfResidence_button)
		TestObject countryOfResidenceSearchInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='country-search-input']")
		TestHelper.setInputValue(countryOfResidenceSearchInput, selectedCountryOfResidence)
		TestObject countryOfResidenceObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+selectedCountryOfResidence+"']")
		WebUI.delay(2.5)
		WebUI.click(countryOfResidenceObject)
		
		TestHelper.clickButton(cityOfResidence_button)
		TestObject cityOfResidenceSearchInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='city-search-input']")
		TestHelper.setInputValue(cityOfResidenceSearchInput, selectedCityOfResidence)
		TestObject cityOfResidenceObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+selectedCityOfResidence+"']")
		WebUI.delay(1)	
		WebUI.click(cityOfResidenceObject)
	
	// Contact Info
		WebUI.delay(2)
		
		TestHelper.clickButton(phoneCountryCode_button)
		TestObject phoneCountrySearchInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='country-search-input']")
		TestHelper.setInputValue(phoneCountrySearchInput, selectedCountryOfResidence)
		TestObject phoneCountryObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+selectedCountryOfResidence+"']")
		WebUI.delay(1)
		WebUI.click(phoneCountryObject) 
		
		TestHelper.setInputValue(phoneNumber_input, '123456')
 
	// Email
		TestHelper.setInputValue(email_input, newEmail + newDate + '@gmail.com')
		
		
	// Social Media
		TestHelper.setInputValue(facebook_input, newFacebook + newDate)
		TestHelper.setInputValue(linkedin_input, newLinkedIn + newDate)
		TestHelper.setInputValue(instagram_input, newInstagram + newDate)
		TestHelper.setInputValue(twitter_input, newTwitter + newDate)
		
		TestObject saveUserInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='save_user_info']")
		TestHelper.clickButton(saveUserInfo);
		WebUI.delay(3)
		
		TestObject manageEntity = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-User-back-button']")
		TestHelper.clickButton(manageEntity);		
}

def checkEditedProfileValue(long newDate, String selectedCountryOfOrigine, String selectedCountryOfResidence, String selectedCityOfOrigine, 
							String selectedCityOfResidence) {
	WebUI.delay(1)
	boolean successfullyEdited = true;
	
	String edit_profile_fullName = (newFirstName + newDate) + " " +  (newLastName + newDate);
	
	String countryOfOrigin = TestHelper.getCountryIso(selectedCountryOfOrigine);
	String countryOfResidence = TestHelper.getCountryIso(selectedCountryOfResidence);
	
	String countryIsoCode = TestHelper.getCountryIso(selectedCountryOfResidence);
	String phoneCountryCode = TestHelper.getCountryCode(selectedCountryOfResidence);
 	
 	// Prepare IDs	
		// Basic Info
			String edited_profile_name_id = 'profile_title_' + edit_profile_fullName; 
			String edited_about_id = 'user_shortBio_id_' + (newAbout + newDate);
			String edited_sector_id = 'user_sector_id_' + newSector;
			String edited_profession_id = 'user_profession_id_' + newProfession;
		
		// Countries
			String edited_country_of_origin  = 'country_' + countryOfOrigin + '_' + 1; 
			String edited_city_of_origin  = 'country_' + countryOfOrigin  + '_' + 1 + '_tag_label' ; 
			
			String edited_country_of_residence  = 'country_' + countryOfResidence  + '_' + 2;  
			String edited_city_of_residence  = 'country_' + countryOfResidence  + '_' + 2 + '_tag_label'  
			
		// Contact Info
			String edited_phone_id = 'user_contact_phone_id_'+ phoneCountryCode + '123456';
			String edited_email_id = 'user_contact_email_id_'+ (newEmail + newDate) + '@gmail.com';
			
		// Social Media
			String edited_facebook_id  = "@id='user_facebook_id'  and  @alt= '" + newFacebook  + newDate +"'";
			String edited_linkedin_id  = "@id='user_linkedin_id'  and  @alt= '" + newLinkedIn  + newDate +"'";
			String edited_twitter_id   = "@id='user_twitter_id'   and  @alt= '" + newTwitter   + newDate +"'";
			String edited_instagram_id = "@id='user_instagram_id' and  @alt= '" + newInstagram + newDate +"'";
			
	// Prepare Objects
		// Basic Info
			TestObject edited_profile_name_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + edited_profile_name_id +"']");
			TestObject edited_about_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + edited_about_id +"']");
			TestObject edited_sector_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + edited_sector_id +"']")
			TestObject edited_profession_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + edited_profession_id +"']")
		
		// Countries Info
			TestObject edited_profile_country_of_origin = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//img[@id='" + edited_country_of_origin +"']");
			TestObject edited_profile_city_of_origin = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='" + edited_city_of_origin +"']");
 			
			TestObject edited_profile_country_of_residence = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//img[@id='" + edited_country_of_residence +"']");
			TestObject edited_profile_city_of_residence = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='" + edited_city_of_residence +"']");
 			
		// Contact Info
			TestObject edited_email_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + edited_email_id +"']")
			TestObject edited_phone_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + edited_phone_id +"']")
			
		// Social Media
			TestObject edited_facebook_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//img["+ edited_facebook_id +"]")
			TestObject edited_linkedin_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//img["+ edited_linkedin_id +"]")
			TestObject edited_twitter_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,  "//img["+ edited_twitter_id  +"]")
			TestObject edited_instagram_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//img["+ edited_instagram_id +"]")
			
			
	// Check Object Exist
		if(!TestHelper.isElementPresent(edited_profile_name_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_profile_country_of_origin)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_profile_city_of_origin)) {
			successfullyEdited = false;
		}				
		else if(!TestHelper.isElementPresent(edited_profile_country_of_residence)) {
			successfullyEdited = false;
		} 
		else if(!TestHelper.isElementPresent(edited_profile_city_of_residence)) {
			successfullyEdited = false;
		}
		else if(!TestHelper.isElementPresent(edited_about_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_sector_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_profession_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_email_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_facebook_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_linkedin_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_twitter_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_instagram_object)) {
			successfullyEdited = false;
		}
 
 
	if(successfullyEdited) {
	  println("Profile Updated Successfully");
	} else {
      TestHelper.thrownException("Profile Not Updated")
	}
	
}
	
// Reset Fields	
def clearProfileFields(String firstName, String lastName, String selectedCountryOfResidence) {
	TestHelper.setInputValue(fistName_input, firstName)
	TestHelper.setInputValue(lastName_input, lastName)

	TestHelper.setInputValue(about_textArea, " ");
	
	WebUI.delay(1);
	TestHelper.clickButton(sector_button);
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='clear_sector_value']"));
	WebUI.delay(1);
	
	TestHelper.clickButton(profession_button);
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='clear_selected_value']"));
	WebUI.delay(1);
	
	// Contact Info
	TestHelper.clickButton(phoneCountryCode_button)
	TestObject phoneCountrySearchInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='country-search-input']")
	TestHelper.setInputValue(phoneCountrySearchInput, selectedCountryOfResidence)
	TestObject phoneCountryObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+selectedCountryOfResidence+"']")
	WebUI.delay(1)
	WebUI.click(phoneCountryObject)

	
	TestHelper.setInputValue(email_input, " ");
		
	// Social Media
	TestHelper.setInputValue(facebook_input, " ");
	TestHelper.setInputValue(linkedin_input, " ");
	TestHelper.setInputValue(instagram_input, " ");
	TestHelper.setInputValue(twitter_input, " ");
	
	TestObject saveUserInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='save_user_info']")
	TestHelper.clickButton(saveUserInfo);
	WebUI.delay(3)
	
	TestObject manageEntity = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-User-back-button']")
	TestHelper.clickButton(manageEntity);
}

