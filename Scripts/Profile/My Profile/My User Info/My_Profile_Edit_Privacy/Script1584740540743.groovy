import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import java.util.List
import java.util.Map

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable


// Define Varibales
TestObject phoneCountryCode_button, phoneNumber_input, email_input;

List<String> countries = TestHelper.prepareCountries();
 
// New Value
long newDate =  new Date().getTime();

// Country and city
String selectedPhoneCountry = TestHelper.getRandomValueFromList(countries);
  
// Login and go to my profile tab
WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Login'), [:], FailureHandling.STOP_ON_FAILURE)

TestHelper.goToManageEntity();
TestObject manageBasicInformationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Basic Information_manage_entity_action']")
TestHelper.clickButton(manageBasicInformationAction)

// Edit Profile and check values
prepareEditProfileFields();
changeUserInfoValue(countries, newDate, selectedPhoneCountry);

TestHelper.goToManageEntity();
TestHelper.clickButton(new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Privacy Settings_manage_entity_action']"))

TestObject manageEntity, email_object_privacy, phone_object_privacy;

// Prepare Privacy Settings
TestObject emailSettings = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='ContactEmail_setting']")
TestObject phoneSettings = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='ContactPhone_setting']")
TestObject savePrivacy 	 = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='save_privacy_setting']")

// EveryOne Settings
WebUI.click(emailSettings);

everyOnePrivacySetting = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Everyone_privacy_setting']")
WebUI.click(everyOnePrivacySetting)

WebUI.click(phoneSettings);
WebUI.click(everyOnePrivacySetting)

WebUI.click(savePrivacy)

manageEntity = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-User-back-button']")
WebUI.click(manageEntity);
checkPrivacySettings(false);

// Private Settings
TestHelper.goToManageEntity();
TestHelper.clickButton(new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Privacy Settings_manage_entity_action']"))


WebUI.click(emailSettings);
privatePrivacySetting = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Private_privacy_setting']")

WebUI.click(privatePrivacySetting)

WebUI.click(phoneSettings);
WebUI.click(privatePrivacySetting)

WebUI.click(savePrivacy)

manageEntity = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-User-back-button']")
WebUI.click(manageEntity);
checkPrivacySettings(true);

print("Privacy settings updated successfully")
WebUI.closeBrowser()


def prepareEditProfileFields() {
 	// Contact Info
	phoneCountryCode_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='user-phone-country-code']")
	phoneNumber_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='user-phone-number']")
	email_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='user-email']")	
}

def changeUserInfoValue(List<String> countries, long newDate, String selectedPhoneCountry) {
	
	// Contact Info	
		TestHelper.clickButton(phoneCountryCode_button)
		TestObject phoneCountrySearchInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='country-search-input']")
		TestHelper.setInputValue(phoneCountrySearchInput, selectedPhoneCountry)
		TestObject phoneCountryObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+selectedPhoneCountry+"']")
		WebUI.delay(1)
		WebUI.click(phoneCountryObject)
		
		TestHelper.setInputValue(phoneNumber_input, '123456')
 
	// Email
		TestHelper.setInputValue(email_input, newEmail + newDate + '@gmail.com')
		
		TestObject saveUserInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='save_user_info']")
		TestHelper.clickButton(saveUserInfo);
		WebUI.delay(3)		
		TestObject manageEntity = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-User-back-button']")
		TestHelper.clickButton(manageEntity);
}

def checkPrivacySettings(boolean isPrivateSetting) {
	TestObject phone_object_privacy = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='user_contact_phone_id_private']")
	TestObject email_object_privacy = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='user_contact_email_id_private']")
	TestObject profile_about_section = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='user_about']")
	
	WebUI.scrollToElement(profile_about_section, 3);

	if(isPrivateSetting) {
		if(!TestHelper.isElementPresent(phone_object_privacy) || !TestHelper.isElementPresent(email_object_privacy)) {
			println('Error')
			TestHelper.thrownException("Privacy settings not updated successfully");
		} 
	} else {
		if(TestHelper.isElementPresent(phone_object_privacy) || TestHelper.isElementPresent(email_object_privacy)) {
			println('Error hhhjhjhjhjh')
			TestHelper.thrownException("Privacy settings not updated successfully");
		}
	}
	
}
