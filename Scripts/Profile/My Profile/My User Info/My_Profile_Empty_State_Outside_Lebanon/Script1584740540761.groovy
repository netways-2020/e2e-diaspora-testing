import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Registe_new_user_outside_lebanon'), [:], FailureHandling.STOP_ON_FAILURE)
TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'profile_tab_id\']'))

// Check Common Header Exist
checkCommonHeaderElementExist()

// Check General Info
UserRoleHelper.checkUserGeneralInfoFieldsEmptyState()

// Check Profile Section
checkProfileSectionsEmptyState()

WebUI.closeBrowser() //	TestObject no_network_role_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div['no-network-empty-state']")
// Check Object Exist	
//	else if(!TestHelper.isElementPresent(no_network_role_empty_state_object)) {
//		successEmptyFields = false;
//	} 

def checkCommonHeaderElementExist() {
    boolean successEmptyFields = true

    TestObject manageAccounts_object = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='profile_manage_accounts']")

    TestObject notification_object = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='profile_notification']")

    TestObject profile_subtitle_object = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='profile-subTitle']")

    TestObject manage_primary_role_object = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='manage-primary-role']")

    TestObject manage_entity_object = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='Manage_User_entity_action']")

    TestObject share_entity_object = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='Share_User_entity_action']")

    if (!(TestHelper.isElementPresent(manageAccounts_object))) {
        successEmptyFields = false
    } else if (!(TestHelper.isElementPresent(notification_object))) {
        successEmptyFields = false
    } else if (!(TestHelper.isElementPresent(profile_subtitle_object))) {
        successEmptyFields = false
    } else if (!(TestHelper.isElementPresent(manage_primary_role_object))) {
        successEmptyFields = false
    } else if (!(TestHelper.isElementPresent(manage_entity_object))) {
        successEmptyFields = false
    } else if (!(TestHelper.isElementPresent(share_entity_object))) {
        successEmptyFields = false
    }
    
    if (successEmptyFields) {
        println('User common header info exist')
    } else {
        TestHelper.thrownException('Error: User commone header info')
    }
}

def checkProfileSectionsEmptyState() {
    boolean successEmptyFields = true

    TestObject user_about_object = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='user_about']")

    TestObject no_network_role_title_object = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='user_network_role']")

	TestObject no_network_role_empty_state_object = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='no-network-empty-state']")
	
    TestObject no_business_role_title_object = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='user_business_role']")

    TestObject no_business_role_empty_state_object = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='no-business-empty-state']")

    TestObject no_updates_title_object = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='user_updates']")

    TestObject no_updates_empty_state_object = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='no-updates-empty-state']")

    TestObject no_media_title_object = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='user_media']")

    TestObject no_media_empty_state_object = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='no-media-empty-state']")

    if (!(TestHelper.isElementPresent(user_about_object))) {
        successEmptyFields = false
    } else if (!(TestHelper.isElementPresent(no_network_role_title_object))) {
        successEmptyFields = false
    }  else if (!(TestHelper.isElementPresent(no_network_role_empty_state_object))) {
        successEmptyFields = false
    } else if (!(TestHelper.isElementPresent(no_business_role_title_object))) {
        successEmptyFields = false
    } else if (!(TestHelper.isElementPresent(no_business_role_empty_state_object))) {
        successEmptyFields = false
    } else if (!(TestHelper.isElementPresent(no_updates_title_object))) {
        successEmptyFields = false
    } else if (!(TestHelper.isElementPresent(no_updates_empty_state_object))) {
        successEmptyFields = false
    } else if (!(TestHelper.isElementPresent(no_media_title_object))) {
        successEmptyFields = false
    } else if (!(TestHelper.isElementPresent(no_media_empty_state_object))) {
        successEmptyFields = false
    }
    
    if (successEmptyFields) {
        println('User role/news/media info is not added')
    } else {
        TestHelper.thrownException('Error: User has added role/news/media info')
    }
}

