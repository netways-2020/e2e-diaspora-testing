import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

String organizationName = 'Netways_Organization_Mentor_Hub__' + new Date().getTime();
String roleName = "Founder"

WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Login_With_Super_User'), [:], FailureHandling.STOP_ON_FAILURE)
TestHelper.goToManageEntity();
TestObject manageNetworkRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Network Roles_manage_entity_action']")
TestHelper.clickButton(manageNetworkRoleAction)
 

// Check if user has business Role
TestObject emptyStateObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-network-role-empty-state']")
boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)

if (emptyStateObjectPresent) {
	println("hasn't network roles")
	TestObject emptyStateAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-network-role-empty-state-button']")
	WebUI.click(emptyStateAction)
	
	// Create organization 
	checkMentorRoleType(organizationName, roleName);
//	UserRoleHelper.createOrganizationWithMentorType(organizationName, roleName)
//	checkOrganizatinoIsCreatedSuccessfully(organizationName);
} else {
	println("has network roles")
	TestObject addNetworkRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='add-new-network-role-action']")
	TestHelper.clickButton(addNetworkRoleAction)
	
	// Create organization
	checkMentorRoleType(organizationName, roleName);
//	UserRoleHelper.createOrganizationWithMentorType(organizationName, roleName)	
//	checkOrganizatinoIsCreatedSuccessfully(organizationName);
}

def checkMentorRoleType(String organizationName, String roleName) {
	TestObject searchForOrganizationInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='searchForOrganizationInput']")
	TestHelper.setInputValue(searchForOrganizationInput, organizationName)

	TestObject createNewOrganizationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='create_new_organization_action']")
	WebUI.click(createNewOrganizationAction)

	WebUI.delay(1);
	
	// Image
	TestObject changeImageAction = TestHelper.getObjectById("//div[@id='change_image_action']");
	TestHelper.clickButton(changeImageAction);
	TestObject changeImageGalleryAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='image_gallery_action']")
	String userPath = System.getProperty("user.dir")
	String imagePath = userPath + "//Image//test_image.png"
	println(imagePath)
	WebUI.uploadFile(changeImageGalleryAction, imagePath)

	TestObject selectImageAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='cropper_select_image']")
	TestHelper.clickButton(selectImageAction);


	TestObject organizationType = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='organization-type']")
	TestHelper.clickButton(organizationType)

	TestObject organizationTypeValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Mentor Hub_item']")
	TestHelper.clickButton(organizationTypeValue)

	TestObject organizationCategory = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='organization-category']")
	TestHelper.clickButton(organizationCategory)
	WebUI.delay(1);

	TestObject organizationCategoryValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Community_item']")
	TestHelper.clickButton(organizationCategoryValue)
	WebUI.delay(1);

	TestObject create_general_info_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='create-organization-general-info']")
	TestHelper.clickButton(create_general_info_button)

	// Role Info

	TestObject positionNameInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='organization-role-position-name']")
	WebUI.setText(positionNameInput, roleName)
	
	WebUI.delay(2);
	WebUI.closeBrowser();
}

def checkOrganizatinoIsCreatedSuccessfully(String organiztionName) {
	String organizationId = organiztionName + '_1586286236041-Founder-role';
	
	TestObject organizationObject = TestHelper.getObjectById("//div[@id='" +organizationId+ "']")
	boolean cardTargetObjectExist = TestHelper.isElementPresent(organizationObject)

	if(!organizationObject) {
		TestHelper.thrownException("Organization not added successfully");
	} else {
		print('organization added succesfuly');
		TestHelper.clickButton(organizationObject);
		
		TestObject organizationAvatar = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//img[@id=\'Organization_has_image\']')
		
		if (!(TestHelper.isElementPresent(organizationAvatar))) {
			TestHelper.thrownException('Organization image not exist')
		} else {
			WebUI.closeBrowser();
		}
	}
}


