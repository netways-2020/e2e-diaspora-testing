import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement

WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Register_new_user'), [:], FailureHandling.STOP_ON_FAILURE)
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_tab_id']"))

WebElement profileNameObject = TestHelper.getItem('profile-name-title', 0)
String profileName = profileNameObject.text;
String firstName = profileName.split(" ")[0];

// Check Common Header Exist
checkCommonHeaderElementExist();

// Check General Info
UserRoleHelper.checkUserGeneralInfoFieldsEmptyState();

// Check Profile Section
checkProfileSectionsEmptyState();

// Check Profile Section Title and SubTitle

checkProfileSectionsTitleAndSubTitle(firstName);

WebUI.closeBrowser();

def checkCommonHeaderElementExist() {
	boolean successEmptyFields = true;
	
	TestObject manageAccounts_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='profile_manage_accounts']")
	TestObject notification_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='profile_notification']")
		
	TestObject profile_subtitle_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='profile-subTitle']")
	TestObject manage_primary_role_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='manage-primary-role']")
	
	TestObject manage_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Manage_User_entity_action']")
	TestObject share_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Share_User_entity_action']")
		
	
	if(!TestHelper.isElementPresent(manageAccounts_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(notification_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(profile_subtitle_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(manage_primary_role_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(manage_entity_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(share_entity_object)) {
		successEmptyFields = false;
	} 
	
	if(successEmptyFields) {
		println("User common header info exist");
	} else {
			TestHelper.thrownException("Error: User commone header info")
	}
}

def checkProfileSectionsEmptyState() {

	boolean successEmptyFields = true;
	
	TestObject user_about_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='user_about']")
	 
	TestObject no_network_role_title_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='user_network_role']")
//	TestObject no_network_role_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-network-empty-state']")
	
	TestObject no_business_role_title_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='user_business_role']")
	TestObject no_business_role_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-business-empty-state']")
	
 	TestObject no_updates_title_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='user_updates']")
	TestObject no_updates_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-updates-empty-state']")
 	
	TestObject no_media_title_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='user_media']")
	TestObject no_media_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-media-empty-state']")

// Check Object Exist		
	if(!TestHelper.isElementPresent(user_about_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(no_network_role_title_object)) {
		successEmptyFields = false;
	} 
//	else if(!TestHelper.isElementPresent(no_network_role_empty_state_object)) {
//		successEmptyFields = false;
//	} 
	else if(!TestHelper.isElementPresent(no_business_role_title_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(no_business_role_empty_state_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(no_updates_title_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(no_updates_empty_state_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(no_media_title_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(no_media_empty_state_object)) {
		successEmptyFields = false;
	}
	
	if(successEmptyFields) {
			println("User role/news/media info is not added");
	} else {
			TestHelper.thrownException("Error: User has added role/news/media info")
	}
	
}

def checkProfileSectionsTitleAndSubTitle(String firstName) {
	TestObject user_about_title_object = TestHelper.getObjectById("//div[@id='profile_about_title_id']")
	if(TestHelper.isElementPresent(user_about_title_object)) {
		String aboutTitle = WebUI.getText(user_about_title_object).trim();
 
		if(!aboutTitle.equals("About")) {
			TestHelper.thrownException("Invalid about title")
		}
	}
	
	TestObject user_about_subTitle_object = TestHelper.getObjectById("//div[@id='profile_about_subTitle_id']")
	if(TestHelper.isElementPresent(user_about_subTitle_object)) {
		String aboutSubTitle = WebUI.getText(user_about_subTitle_object).trim();
 
		if(!aboutSubTitle.equals("Info, sector and contact details")) {
			TestHelper.thrownException("Invalid about subtitle")
		}
	}
	
// Network Role
	TestObject network_role_title_object = TestHelper.getObjectById("//div[@id='profile_networks_title_id']")
	if(TestHelper.isElementPresent(network_role_title_object)) {
		String networkRoleTitle = WebUI.getText(network_role_title_object).trim();

		if(!networkRoleTitle.equals("Networks")) {
			TestHelper.thrownException("Invalid network title")
		}
	}
	
	TestObject network_role_subTitle_object = TestHelper.getObjectById("//div[@id='profile_networks_subTitle_id']")

	if(TestHelper.isElementPresent(network_role_subTitle_object)) {
		String networkRoleSubTitle = WebUI.getText(network_role_subTitle_object).trim();
		println('networkRoleSubTitle: ' + networkRoleSubTitle);
		println('networkRoleSubTitle 2: ' + (firstName + "’s communities & orgs"));
		
		if(!networkRoleSubTitle.equals(firstName + "’s communities & orgs")) {
			TestHelper.thrownException("Invalid network subtitle")
		}
	}

// Business Role
	TestObject business_role_title_object = TestHelper.getObjectById("//div[@id='profile_businesses_title_id']")
	if(TestHelper.isElementPresent(business_role_title_object)) {
		String businessRoleTitle = WebUI.getText(business_role_title_object).trim();

		if(!businessRoleTitle.equals("Business")) {
			TestHelper.thrownException("Invalid business title")
		}
	}
	
	
	TestObject business_role_subTitle_object = TestHelper.getObjectById("//div[@id='profile_businesses_subTitle_id']")
	if(TestHelper.isElementPresent(business_role_subTitle_object)) {
		String businessRoleSubTitle = WebUI.getText(business_role_subTitle_object).trim();
		if(!businessRoleSubTitle.equals("Companies, Startups, Freelancers")) {
			TestHelper.thrownException("Invalid business subtitle")
		}
	}

// Updates
	TestObject updates_title_object = TestHelper.getObjectById("//div[@id='profile_updates_title_id']")
	if(TestHelper.isElementPresent(updates_title_object)) {
		String updatesTitle = WebUI.getText(updates_title_object).trim();
 
		println('updatesTitle: ' + updatesTitle);
		if(!updatesTitle.equals("Updates")) {
			TestHelper.thrownException("Invalid updates title ")
		}

	}
	
	
	TestObject updates_subTitle_object = TestHelper.getObjectById("//div[@id='profile_updates_subTitle_id']")
	if(TestHelper.isElementPresent(updates_subTitle_object)) {
		String updatesSubTitle = WebUI.getText(updates_subTitle_object).trim();
		println('updatesSubTitle: ' + updatesSubTitle);

		if(!updatesSubTitle.equals(firstName + "’s recent posts & events")) {
			TestHelper.thrownException("Invalid Updates subtitle")
		}

	}

// Media
	TestObject media_title_object = TestHelper.getObjectById("//div[@id='profile_media_title_id']")
	if(TestHelper.isElementPresent(media_title_object)) {
		String mediaTitle = WebUI.getText(media_title_object).trim();

		if(!mediaTitle.equals("Media Gallery")) {
			TestHelper.thrownException("Invalid media title")
		}

	}
	
	TestObject media_subTitle_object = TestHelper.getObjectById("//div[@id='profile_media_subTitle_id']")		
	if(TestHelper.isElementPresent(media_subTitle_object)) {
		String mediaSubTitle = WebUI.getText(media_subTitle_object).trim();

		if(!mediaSubTitle.equals("Photos & videos")) {
			TestHelper.thrownException("Invalid media subtitle")
		}

	}	
}

