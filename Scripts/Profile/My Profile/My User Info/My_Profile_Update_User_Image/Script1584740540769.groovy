import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Register_new_user'), [:], FailureHandling.STOP_ON_FAILURE)
TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'profile_tab_id\']'))

//WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Login'), [:], FailureHandling.STOP_ON_FAILURE)


checkThatTheUserHasntImage();

TestHelper.goToManageEntity();
TestObject manageBasicInformationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Basic Information_manage_entity_action']")
TestHelper.clickButton(manageBasicInformationAction)
changeUserImage();

checkThatTheUserHasImage();

def checkThatTheUserHasntImage() {
	TestObject userAvatar = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//ngx-avatar[@id='User_hasnt_image']")
	
	if(!TestHelper.isElementPresent(userAvatar)) {
		TestHelper.thrownException("User has image");
	}
}

def changeUserImage() {
	String userPath = System.getProperty("user.dir")
	String imagePath = userPath + "//Image//test_image.png"

	TestObject changeImageAction = TestHelper.getObjectById("//div[@id='change_image_action']");
	TestHelper.clickButton(changeImageAction);
	
	// Image
	TestObject changeProfileImageAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='image_gallery_action']")
	WebUI.uploadFile(changeProfileImageAction, imagePath)

	TestObject selectImageAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='cropper_select_image']")
	TestHelper.clickButton(selectImageAction);

	WebUI.delay(2);
 	// Update profile image
	TestObject saveUserInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='save_user_info']")
	TestHelper.clickButton(saveUserInfo);
	WebUI.delay(3)
	
	TestObject manageEntity = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-User-back-button']")
	TestHelper.clickButton(manageEntity);
}

def checkThatTheUserHasImage() {
	TestObject userAvatar = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//img[@id='User_has_image']")
	WebUI.waitForElementPresent(userAvatar, 10);
	
	if(!TestHelper.isElementPresent(userAvatar)) {
		TestHelper.thrownException("Profile image not updated");
	} else {
		println("Image has been updated successfully");
		WebUI.closeBrowser();
	}
}
