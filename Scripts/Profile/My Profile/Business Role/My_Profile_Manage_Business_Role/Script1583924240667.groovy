import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import cucumber.api.java.Before
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement


String companyName = 'Netways' + new Date().getTime()
String roleName = "Founder"
String newRoleName = "Employee"

 
// Login and go to my profile tab
WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Login'), [:], FailureHandling.STOP_ON_FAILURE)
String oldEntityRole = UserRoleHelper.getFeaturedRole();

TestHelper.goToManageEntity();
TestObject manageBuinessRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Business Roles_manage_entity_action']")
TestHelper.clickButton(manageBuinessRoleAction)
 
// Check if user has business Role
TestObject emptyStateObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-business-role-empty-state']")
boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)


if (emptyStateObjectPresent == true) {
	println("hasn't business roles")
	TestObject emptyStateAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-business-role-empty-state-button']")
	WebUI.click(emptyStateAction)
	
	// Create company, add business role 
	UserRoleHelper.createCompany(companyName, roleName)
	
	// Edit business Role 
	editBusinessRole(companyName, newRoleName); 
	checkBusinessRoleIsEditedSuccessfully(companyName, newRoleName);
	
	// Feature-Unfeature Role
	setBusinessRoleAsFeatureRole(companyName)
	unFeatureBusinessRole(companyName, oldEntityRole)
	
	// Remove Business Role
	removeBusinessRole(companyName)
	
} else {
	println("has business roles")
	TestObject addBusinessRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='add-new-business-role-action']")
	WebUI.click(addBusinessRoleAction)
	
	// Create company, add business role 
	UserRoleHelper.createCompany(companyName, roleName)
	
	// Edit business Role 
	editBusinessRole(companyName, newRoleName); 
	checkBusinessRoleIsEditedSuccessfully(companyName, newRoleName);
	
	// Feature-Unfeature Role
	setBusinessRoleAsFeatureRole(companyName)
	unFeatureBusinessRole(companyName, oldEntityRole)
	
	// Remove Business Role
	removeBusinessRole(companyName)
	
}

 
// Edit Business role
def editBusinessRole(String companyName, String newRoleName) {
	WebUI.delay(1.5)
	
	String manageBusinessRoleImageId = companyName + '-options-icon'
	String manageRoleImageXpath = "//div[@id='"+manageBusinessRoleImageId+"']"	  
	TestObject manageRoleImage = TestHelper.getObjectById(manageRoleImageXpath)		
	TestHelper.clickButton(manageRoleImage);
	
	// Edit Business Role
	TestObject editRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Edit Role_user_action']")
	TestHelper.clickButton(editRoleAction);
	
	 
	TestObject businessRolePositionType = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='business-role-position-type']")
	TestHelper.clickButton(businessRolePositionType)

	
	TestObject businessRolePositionTypeValue = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Employee_item']")
	TestHelper.clickButton(businessRolePositionTypeValue)
	 
	TestObject businessRolePositionName = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='business-role-position-name']")
	TestHelper.setInputValue(businessRolePositionName, newRoleName)
 	
	TestObject updateBusinessRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='update_business_role']")
	TestHelper.clickButton(updateBusinessRoleAction) 
	
	TestObject manageBusinessRoleBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_business_role_back_action']")
	WebUI.waitForElementPresent(manageBusinessRoleBackButton, 3);
}

def checkBusinessRoleIsEditedSuccessfully(String companyName, String newRoleName) {
	String businessRoleId = companyName + '-' + newRoleName + '-role';
	String businessRoleIdXpath = "//div[@id='"+businessRoleId+"']"
	TestObject businessRoleObject = TestHelper.getObjectById(businessRoleIdXpath)
	
	String editedRoleValue = WebUI.getText(businessRoleObject)
	
	boolean roleSuccessfullyEdited = TestHelper.verifyElementValue(editedRoleValue, newRoleName);
	
	if(roleSuccessfullyEdited) {
	  println("Role has been successfully Edited")
	} else {
	  println("An expected error has been occurred")
	}
}


// Feature-Unfeature role
def setBusinessRoleAsFeatureRole(String companyName) {
	String manageBusinessRoleImageId = companyName + '-options-icon'
	String manageRoleImageXpath = "//div[@id='"+manageBusinessRoleImageId+"']"
	TestObject manageRoleImage = TestHelper.getObjectById(manageRoleImageXpath)
	TestHelper.clickButton(manageRoleImage);

	TestObject setAsFeatureAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Set Role As Featured_user_action']")
	TestHelper.clickButton(setAsFeatureAction);
	 
	WebUI.delay(1.5)
	
	// Check if successfully set as feature
	TestObject manageBusinessRoleBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_business_role_back_action']")
	TestHelper.clickButton(manageBusinessRoleBackButton);
	
	TestObject manageEntityBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-User-back-button']")
	TestHelper.clickButton(manageEntityBackButton);
 
	UserRoleHelper.checkIfSuccessfullySetAsFeature(companyName);
	
	TestHelper.goToManageEntity();
	TestObject manageBuinessRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Business Roles_manage_entity_action']")
	TestHelper.clickButton(manageBuinessRoleAction)
}

def unFeatureBusinessRole(String companyName,String oldEntityRole) {
	WebUI.delay(1.5)
	String manageBusinessRoleImageId = companyName + '-options-icon'
	String manageRoleImageXpath = "//div[@id='"+manageBusinessRoleImageId+"']"
	TestObject manageRoleImage = TestHelper.getObjectById(manageRoleImageXpath)
	TestHelper.clickButton(manageRoleImage);
	 

	TestObject unfeatureAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Unfeature Role_user_action']")
	TestHelper.clickButton(unfeatureAction);

	WebUI.delay(1.5)
	
	// Check if successfully set as feature
	TestObject manageBusinessRoleBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_business_role_back_action']")
	TestHelper.clickButton(manageBusinessRoleBackButton);
	
	TestObject manageEntityBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-User-back-button']")
	TestHelper.clickButton(manageEntityBackButton);
	UserRoleHelper.checkIfSuccessfullySetAsUnFeature(oldEntityRole);	
	
	TestHelper.goToManageEntity();
	TestObject manageBuinessRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Business Roles_manage_entity_action']")
	TestHelper.clickButton(manageBuinessRoleAction)
}


// Remove business role
def removeBusinessRole(String companyName) {
	String manageBusinessRoleImageId = companyName + '-options-icon'
	String manageRoleImageXpath = "//div[@id='"+manageBusinessRoleImageId+"']"	  
	TestObject manageRoleImage = TestHelper.getObjectById(manageRoleImageXpath)
	
	boolean foundElement = TestHelper.isElementPresent(manageRoleImage)
	if(foundElement) {
		println("Element Present")
	}
	
	TestHelper.clickButton(manageRoleImage);
	
	TestObject deleteRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Delete Role_user_action']")
	TestHelper.clickButton(deleteRoleAction);
	
	
	WebElement deleteBusinessConfirmation = TestHelper.getItem('delete_business_action', 0)
	deleteBusinessConfirmation.click();
	
	WebUI.delay(2)
	boolean foundDeletedElement = TestHelper.isElementPresent(manageRoleImage)
	
	if(!foundDeletedElement) {
	  println("Role has been successfully deleted")
	  TestObject manageBusinessRoleBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_business_role_back_action']")
	  TestHelper.clickButton(manageBusinessRoleBackButton);
	
	  TestObject manageEntityBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-User-back-button']")
	  TestHelper.clickButton(manageEntityBackButton);
	  WebUI.closeBrowser();
 	} else {
		TestHelper.thrownException("Error during delete business role")
	}
	

}
      
