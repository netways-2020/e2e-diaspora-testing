import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.helper.KeywordHelper
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.junit.After as After
import org.openqa.selenium.Keys as Keys

String companyName = 'Netways' + new Date().getTime()
String roleName = 'Founder';

// Login and go to my profile tab
WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Login'), [:], FailureHandling.STOP_ON_FAILURE)
 
TestHelper.goToManageEntity();
TestObject manageBuinessRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Business Roles_manage_entity_action']")
TestHelper.clickButton(manageBuinessRoleAction)


// Check if user has business Role
TestObject emptyStateObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-business-role-empty-state']")
boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)
 

if (emptyStateObjectPresent == true) {
	println("hasn't business roles")
	TestObject emptyStateAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-business-role-empty-state-button']")
	WebUI.click(emptyStateAction)
	
	// Create company, add business role
	UserRoleHelper.createCompany(companyName, roleName)
	
	// Set business role as primary role
	setCompanyRoleAsPrimary(companyName);
	UserRoleHelper.checkIfSuccessfullySetAsFeature(companyName);
	WebUI.closeBrowser();

} else {
	println("has business roles")
	TestObject addBusinessRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='add-new-business-role-action']")
	WebUI.click(addBusinessRoleAction)
	
	// Create company, add business role
	UserRoleHelper.createCompany(companyName, roleName)
	
	// Set business role as primary role
	setCompanyRoleAsPrimary(companyName);
	UserRoleHelper.checkIfSuccessfullySetAsFeature(companyName);
	WebUI.closeBrowser();
}


def setCompanyRoleAsPrimary(String companyName) {
	String businessRoleImageId = companyName + '-feature-icon'
	String businessRoleImageXpath = "//div[@id='"+businessRoleImageId+"']"
	
	TestObject manageBusinessRoleBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_business_role_back_action']")
	TestHelper.clickButton(manageBusinessRoleBackButton)

	TestObject manageEntity = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-User-back-button']")
	TestHelper.clickButton(manageEntity);

	TestObject managePrimayEntity = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-primary-role']")
	TestHelper.clickButton(managePrimayEntity);
 
	TestObject setAsPrimaryRoleImage = TestHelper.getObjectById(businessRoleImageXpath)
	TestHelper.clickButton(setAsPrimaryRoleImage);
	
	WebUI.delay(1)	
	
	TestObject managePrimaryRoleBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_primary_role_back_action']")
	TestHelper.clickButton(managePrimaryRoleBackButton);
}

