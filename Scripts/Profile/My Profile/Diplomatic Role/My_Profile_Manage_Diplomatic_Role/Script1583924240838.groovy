import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement


String diplomaticName = TestHelper.getEmbassyOfLebanonInAmerica_EmbassyName();
String newRoleName = 'Permanent Representative';

WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Register_new_user'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.navigateToUrl(TestHelper.getEmbassyOfLebanonInAmerica_EmbassyUrl());

TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Join Diplomatic Mission_action']"))

TestObject positionNameInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='diplomatic-position-name']")
TestHelper.setInputValue(positionNameInput, 'Ambassador')

TestHelper.clickButton(TestHelper.getObjectById("//button[@id='join-diplomatic-action']"))
WebUI.delay(3);

WebUI.navigateToUrl(TestHelper.getMyProfileUrl());

String oldEntityRole = UserRoleHelper.getFeaturedRole()

TestHelper.goToManageEntity()
TestObject manageNetworkRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Network Roles_manage_entity_action\']')
TestHelper.clickButton(manageNetworkRoleAction)


// Edit Organization Role
	editDiplomaticRole(diplomaticName, newRoleName)
	checkDiplomaticRoleIsEditedSuccessfully(diplomaticName, newRoleName)

// Feature-Unfeature Role
	setDiplomaticRoleAsFeatureRole(diplomaticName)
	unFeaturDiplomaticRole(diplomaticName, oldEntityRole)

// Remove Organization Role
	removeDiplomaticRole(diplomaticName) 
 

def editDiplomaticRole(String organizationName, String newRoleName) {
    WebUI.delay(1.5)

    String manageNetworkRoleImageId = organizationName + '-options-icon'
    String manageNetworkImageXpath = ('//div[@id=\'' + manageNetworkRoleImageId) + '\']'

    TestObject manageRoleImage = TestHelper.getObjectById(manageNetworkImageXpath)
    TestHelper.clickButton(manageRoleImage)

    TestObject editRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//a[@id=\'Edit Role_user_action\']')
    TestHelper.clickButton(editRoleAction)

	TestObject positionNameInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='diplomatic-position-name']")
	TestHelper.setInputValue(positionNameInput, newRoleName)

	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='edit-diplomatic-action']"))
	WebUI.delay(3);
	
	TestObject manageDiplomaticRoleBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_network_role_back_action']")
	WebUI.waitForElementPresent(manageDiplomaticRoleBackButton, 3);

}

def checkDiplomaticRoleIsEditedSuccessfully(String diplomaticName, String newRoleName) {
    String networkRoleId = ((diplomaticName + '-') + newRoleName) + '-role'

    String networkRoleIdXpath = ('//div[@id=\'' + networkRoleId) + '\']'

    TestObject networkRoleObject = TestHelper.getObjectById(networkRoleIdXpath)

    String editedRoleValue = WebUI.getText(networkRoleObject)

    boolean roleSuccessfullyEdited = TestHelper.verifyElementValue(editedRoleValue, newRoleName)

    if (roleSuccessfullyEdited) {
        println('Role has been successfully Edited')
    } else {
        println('An expected error has been occurred')
    }
}

def setDiplomaticRoleAsFeatureRole(String diplomaticName) {
    String manageNetworkRoleImageId = diplomaticName + '-options-icon'

    String manageRoleImageXpath = ('//div[@id=\'' + manageNetworkRoleImageId) + '\']'

    TestObject manageRoleImage = TestHelper.getObjectById(manageRoleImageXpath)

    TestHelper.clickButton(manageRoleImage)

    TestObject setAsFeatureAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//a[@id=\'Set Role As Featured_user_action\']')

    TestHelper.clickButton(setAsFeatureAction)

    WebUI.delay(1.5)

    TestObject manageNetworkRoleBackButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'manage_network_role_back_action\']')

    TestHelper.clickButton(manageNetworkRoleBackButton)

    TestObject manageEntityBackButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'manage-User-back-button\']')

    TestHelper.clickButton(manageEntityBackButton)

    UserRoleHelper.checkIfSuccessfullySetAsFeature(diplomaticName)

    TestHelper.goToManageEntity()

    TestObject manageNetworkRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Network Roles_manage_entity_action\']')

    TestHelper.clickButton(manageNetworkRoleAction)
}

def unFeaturDiplomaticRole(String diplomaticName, String oldEntityRole) {
    WebUI.delay(1.5)

    String manageNetworkRoleImageId = diplomaticName + '-options-icon'

    String manageRoleImageXpath = ('//div[@id=\'' + manageNetworkRoleImageId) + '\']'

    TestObject manageRoleImage = TestHelper.getObjectById(manageRoleImageXpath)

    TestHelper.clickButton(manageRoleImage)

    TestObject unfeatureAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//a[@id=\'Unfeature Role_user_action\']')

    TestHelper.clickButton(unfeatureAction)

    WebUI.delay(1.5)

    TestObject manageNetworkRoleBackButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'manage_network_role_back_action\']')

    TestHelper.clickButton(manageNetworkRoleBackButton)

    TestObject manageEntityBackButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'manage-User-back-button\']')

    TestHelper.clickButton(manageEntityBackButton)

    UserRoleHelper.checkIfSuccessfullySetAsUnFeature(oldEntityRole)

    TestHelper.goToManageEntity()

    TestObject manageNetworkRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Network Roles_manage_entity_action\']')

    TestHelper.clickButton(manageNetworkRoleAction)
}

def removeDiplomaticRole(String diplomaticName) {
    String manageNetworkRoleImageId = diplomaticName + '-options-icon'

    String manageRoleImageXpath = ('//div[@id=\'' + manageNetworkRoleImageId) + '\']'

    TestObject manageRoleImage = TestHelper.getObjectById(manageRoleImageXpath)

    boolean foundElement = TestHelper.isElementPresent(manageRoleImage)

    if (foundElement) {
        println('Element Present')
    }
    
    TestHelper.clickButton(manageRoleImage)

    TestObject deleteRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//a[@id=\'Delete Role_user_action\']')
    TestHelper.clickButton(deleteRoleAction)
	
	WebElement deleteNetworkConfirmation = TestHelper.getItem('delete_network_action', 0)
	deleteNetworkConfirmation.click();
	
    WebUI.delay(2)

    boolean foundDeletedElement = TestHelper.isElementPresent(manageRoleImage)

    if (!(foundDeletedElement)) {
        println('Role has been successfully deleted')

        TestObject manageNetworkRoleBackButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'manage_network_role_back_action\']')

        TestHelper.clickButton(manageNetworkRoleBackButton)

        TestObject manageEntityBackButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'manage-User-back-button\']')

        TestHelper.clickButton(manageEntityBackButton)

        WebUI.closeBrowser()
    } else {
        TestHelper.thrownException('Error during delete network role')
    }
}

