import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

String diplomaticName = TestHelper.getEmbassyOfLebanonInAmerica_EmbassyName();

WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Register_new_user'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.navigateToUrl(TestHelper.getEmbassyOfLebanonInAmerica_EmbassyUrl());

TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Join Diplomatic Mission_action']"))

TestObject positionNameInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='diplomatic-position-name']")
TestHelper.setInputValue(positionNameInput, 'Ambassador')


TestHelper.clickButton(TestHelper.getObjectById("//button[@id='join-diplomatic-action']"))
WebUI.delay(3);

WebUI.navigateToUrl(TestHelper.getMyProfileUrl());

setDiplomaticRoleAsPrimary(diplomaticName);
UserRoleHelper.checkIfSuccessfullySetAsFeature(diplomaticName);
WebUI.closeBrowser()


def setDiplomaticRoleAsPrimary(String diplomaticName) {
	String diplomaticRoleImageId = diplomaticName + '-feature-icon'
	String diplomaticRoleImageXpath = "//div[@id='"+diplomaticRoleImageId+"']"
		
	TestObject managePrimayEntity = TestHelper.getObjectById("//div[@id='manage-primary-role']")
	TestHelper.clickButton(managePrimayEntity);
		 
	TestObject setAsPrimaryRoleImage = new TestObject().addProperty("xpath", ConditionType.EQUALS, diplomaticRoleImageXpath)
	TestHelper.clickButton(setAsPrimaryRoleImage);
	
	WebUI.delay(1)
		
	TestObject managePrimaryRoleBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_primary_role_back_action']")
	TestHelper.clickButton(managePrimaryRoleBackButton);
}


