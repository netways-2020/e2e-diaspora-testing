import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable


WebUI.openBrowser('')

WebUI.navigateToUrl(TestHelper.getHomeUrl())

TestHelper.clickButton(TestHelper.getObjectById("//button[@id='loginButton']"))
WebUI.delay(1)

TestHelper.clickButton(TestHelper.getObjectById("//button[@id='login-with-email-button']"))

TestObject emailInput = TestHelper.getObjectById("//input[@id='email-input']")
TestHelper.setInputValue(emailInput, 'automationTestingFullInformation@gmail.com')

//WebUI.setText(findTestObject('Object Repository/Page_Ionic App/input_Enter Your Email Address_email-input'), 'automationTesting@gmail.com')

TestHelper.clickButton(TestHelper.getObjectById("//button[@id='login-email-submit-button']"))

TestObject passwordInput = TestHelper.getObjectById("//input[@id='passwordInputId']")
TestHelper.setPasswordValue(passwordInput, 'cvW8qx4B2o3F4VwP/kNsqA==')

//WebUI.setEncryptedText(findTestObject('Object Repository/Page_Ionic App/input_Your password must be at least 6 char_625064'), 'cvW8qx4B2o3F4VwP/kNsqA==')

TestHelper.clickButton(TestHelper.getObjectById("//button[@id='submit-button']"))

TestObject manageProfilSection = TestHelper.getObjectById("//div[@id='manageProfileId']")
WebUI.waitForElementVisible(manageProfilSection, 10)

TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_tab_id']"))

