import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable


WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Register_new_user'), [:], FailureHandling.STOP_ON_FAILURE)
followEntities();
 
// Go To Following Tab
WebUI.navigateToUrl(TestHelper.getHomeUrl())
WebUI.delay(2);
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_tab_id']"))


TestHelper.goToManageEntity();
TestObject followingAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Pages I Follow_manage_entity_action']")
TestHelper.clickButton(followingAction)
checkFollowingEntities();


def followEntities() {
// Organization
	WebUI.navigateToUrl(TestHelper.getLebanesRedCross_organizationUrl());

	TestObject organizationFollowAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Follow_Organization_entity_action']")
	boolean organizationFollowActionExist = TestHelper.isElementPresent(organizationFollowAction)
	
	if(organizationFollowActionExist) {
		TestHelper.clickButton(organizationFollowAction);
		WebUI.delay(3);
	} else {
		TestHelper.thrownException("Organization already followed");
	}
	
	
// Company
	WebUI.navigateToUrl(TestHelper.getNetways_companyUrl());
	
	TestObject companyFollowAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Follow_Company_entity_action']")
	boolean companyFollowActionExist = TestHelper.isElementPresent(companyFollowAction)
	
	if(companyFollowActionExist) {
		TestHelper.clickButton(companyFollowAction);
		WebUI.delay(3);
	}  else {
		TestHelper.thrownException("Company already followed");
	}
	
// Diplomatic 
	WebUI.navigateToUrl(TestHelper.getEmabssyOfAssab_EmbassyUrl());
	TestObject diplomaticFollowAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Follow_DiplomaticMissions_entity_action']")
	boolean diplomaticFollowActionExist = TestHelper.isElementPresent(diplomaticFollowAction)
	
	if(diplomaticFollowActionExist) {
		TestHelper.clickButton(diplomaticFollowAction);
		WebUI.delay(3);
	}  else {
		TestHelper.thrownException("Diplomatic already followed");
	}
}


def checkFollowingEntities() {
	   String organizationFollowId = TestHelper.lebanedRedCross_OrganizationName + '_unfollow';
	   TestObject organizationFollowObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+ organizationFollowId +"']")
	   boolean organizationFollowExist = TestHelper.isElementPresent(organizationFollowObject)
		
	   if(organizationFollowExist) {
		   println('follow Organization has been unfollowed successfully');
	   } else {
   	   		TestHelper.thrownException("Followed organization not exist")
	   }

	   String embassyFollowId = TestHelper.embassyOfAssab_EmbassyName + '_unfollow';
	   TestObject embassyFollowObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+ embassyFollowId +"']")
	   boolean embassyFollowObjectExist = TestHelper.isElementPresent(embassyFollowObject)
	   
	   if(embassyFollowObjectExist) {
		   println('follow embassy has been unfollowed successfully');
	   } else {
	   		TestHelper.thrownException("Followed embassy not exist")
	   }
	

	
	// Check Organization Is Followed
		TestObject organizationSearchInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='network_input_search']")
	 	TestHelper.setInputValue(organizationSearchInput, TestHelper.lebanedRedCross_OrganizationName)

 		WebUI.delay(2);

		organizationFollowExist = TestHelper.isElementPresent(organizationFollowObject)
		 
		if(organizationFollowExist) {
			println('follow Organization has been unfollowed successfully');
			TestHelper.clickButton(organizationFollowObject);
		} else {
			TestHelper.thrownException("Followed organization not exist")
		}
		
	// Check Diplomatic Is Followed
	   WebUI.delay(2);
	   TestHelper.setInputValue(organizationSearchInput, TestHelper.embassyOfAssab_EmbassyName)

	   embassyFollowObjectExist = TestHelper.isElementPresent(embassyFollowObject)
	   if(embassyFollowObjectExist) {
		   println('follow embassy has been unfollowed successfully');
		   TestHelper.clickButton(embassyFollowObject);
	   } else {
	   		TestHelper.thrownException("Followed embassy not exist")
	   }
	
	// Check Company Is Followed
		TestObject businessTab = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//ion-segment-button[@id='business_tab_id']")
		TestHelper.clickButton(businessTab);
		
		String companyFollowId = TestHelper.netways_CompanyName + '_unfollow';
		TestObject companyFollowObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+ companyFollowId +"']")
		boolean companyFollowIdExist = TestHelper.isElementPresent(companyFollowObject)
		
		if(companyFollowIdExist) {
			println('Follow company has been unfollowed successfully');
		} else {
	   		TestHelper.thrownException("Followed company not exist")
	    }
		 
	   TestObject businessSearchInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='business_input_search']")
	   TestHelper.setInputValue(businessSearchInput, TestHelper.netways_CompanyName)
   
	   companyFollowIdExist = TestHelper.isElementPresent(companyFollowObject)
		
	   if(companyFollowIdExist) {
		   println('Follow company has been unfollowed successfully');
		   TestHelper.clickButton(companyFollowObject);
	   } else {
	   		TestHelper.thrownException("Followed company not exist")
	   }
	   
	// check unfollow action successfully
	   TestObject backButtonObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_following_back_id']")
	   TestHelper.clickButton(backButtonObject);
	    
	   TestObject followingAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Pages I Follow_manage_entity_action']")
	   TestHelper.clickButton(followingAction)
	   
	   
	   // Organization 
//		TestHelper.setInputValue(organizationSearchInput, TestHelper.lebanedRedCross_OrganizationName)
		organizationFollowExist = TestHelper.isElementPresent(organizationFollowObject);
	  
		if(!organizationFollowExist) {
		   println('followed Organization has been removed successfully');
		} else {
	   		TestHelper.thrownException("Followed organization still exist")
	    }
		
		// Embassy
		embassyFollowObjectExist = TestHelper.isElementPresent(embassyFollowObject);
		
        if(!embassyFollowObjectExist) {
		    println('followed Embassy has been removed successfully');
	    } else {
	   		TestHelper.thrownException("Followed embassy still exist")
	    }
		
   
	   // Company
	   TestHelper.clickButton(businessTab);
	   
//	   TestHelper.setInputValue(businessSearchInput, TestHelper.netways_CompanyName)
	   companyFollowIdExist = TestHelper.isElementPresent(companyFollowObject);
	   
	   if(!companyFollowIdExist) {
		   println('followed compmany has been removed successfully');
		   WebUI.closeBrowser();
	   } else {
	   		TestHelper.thrownException("Followed company not exist")
	   }
}