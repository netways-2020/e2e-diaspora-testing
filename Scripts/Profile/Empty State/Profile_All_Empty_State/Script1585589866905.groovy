import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement

WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Registe_new_user_outside_lebanon'), [:], FailureHandling.STOP_ON_FAILURE)

//TestHelper.loginWithSpecificUserNameAndPassword("netways_user1585504342936@gmail.com", "cvW8qx4B2o3F4VwP/kNsqA==", true)
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_tab_id']"))

WebElement profileNameObject = TestHelper.getItem('profile-name-title', 0)
String profileName = profileNameObject.text;
String firstName = profileName.split(" ")[0];
 
// Check Common Header Exist
checkCommonHeaderElementExist();

// Check General Info
UserRoleHelper.checkUserGeneralInfoFieldsEmptyState();

// Check Profile Section
checkProfileSectionsEmptyState();

// Check Profile Section Title and SubTitle
checkProfileSectionsTitleAndSubTitle(firstName);

// Check SubPages Empty State
checkSubPagesEmptyState();

// Check Update Empty State
checkUpdateEmptyState(profileName);

// Check Media Empty State
checkMediaEmptyState();

WebUI.closeBrowser();

def checkCommonHeaderElementExist() {
	boolean successEmptyFields = true;
	
	TestObject manageAccounts_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='profile_manage_accounts']")
	TestObject notification_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='profile_notification']")
		
	TestObject profile_subtitle_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='profile-subTitle']")
	TestObject manage_primary_role_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='manage-primary-role']")
	
	TestObject manage_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Manage_User_entity_action']")
	TestObject share_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Share_User_entity_action']")
		
	
	if(!TestHelper.isElementPresent(manageAccounts_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(notification_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(profile_subtitle_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(manage_primary_role_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(manage_entity_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(share_entity_object)) {
		successEmptyFields = false;
	} 
	
	if(successEmptyFields) {
		println("User common header info exist");
	} else {
			TestHelper.thrownException("Error: User commone header info")
	}
}

def checkProfileSectionsEmptyState() {
	String errorMessage = "Error: User has added role/news/media info";
	
// About Section
	TestHelper.checkObjectExit("//div[@id='user_about']", errorMessage);
	
// Network Role	
	TestHelper.checkObjectExit("//div[@id='user_network_role']", errorMessage);
	TestHelper.checkObjectText("//div[@id='no-network-empty-state-title']", "Are you in charge of a organization? Work in one?" , errorMessage);
	TestHelper.checkObjectText("//div[@id='no-network-empty-state']", "Add Your Network Role" , errorMessage);
	TestHelper.checkObjectImageSrc("//div[@id='no-network-empty-state-image']", "assets/icons/emptyState/Network_Roles.svg", errorMessage)

// Business Role
	TestHelper.checkObjectExit("//div[@id='user_business_role']", errorMessage);
	TestHelper.checkObjectText("//div[@id='no-business-empty-state-title']", "Are you in charge of a company or startup? Work in one?" , errorMessage);
	TestHelper.checkObjectText("//div[@id='no-business-empty-state']", "Add Your Business Role" , errorMessage);
	TestHelper.checkObjectImageSrc("//div[@id='no-business-empty-state-image']", "assets/icons/emptyState/Business_Roles_Green.svg", errorMessage)
	
// Updates
	TestHelper.checkObjectExit("//div[@id='user_updates']", errorMessage);
	TestHelper.checkObjectText("//div[@id='no-updates-empty-state-title']", "Posts and events you added to your profile or communities appear here" , errorMessage);
	TestHelper.checkObjectText("//div[@id='no-updates-empty-state']", "Create Post Under Your Profile" , errorMessage);
	TestHelper.checkObjectImageSrc("//div[@id='no-updates-empty-state-image']", "assets/icons/emptyState/Post_Update.svg", errorMessage)

// Media
	TestHelper.checkObjectExit("//div[@id='user_media']", errorMessage);
	TestHelper.checkObjectText("//div[@id='no-media-empty-state-title']", "Show sights, achievements or projects with images and videos" , errorMessage);
	TestHelper.checkObjectText("//div[@id='no-media-empty-state']", "Add Photos or Videos" , errorMessage);
	TestHelper.checkObjectImageSrc("//div[@id='no-media-empty-state-image']", "assets/icons/emptyState/Media_Gallery.svg", errorMessage) 	

	WebUI.delay(2);
	println("User role/news/media info is not added");	
}

def checkProfileSectionsTitleAndSubTitle(String firstName) {
	TestObject user_about_title_object = TestHelper.getObjectById("//div[@id='profile_about_title_id']")
	TestObject user_about_subTitle_object = TestHelper.getObjectById("//div[@id='profile_about_subTitle_id']")
	TestObject user_about_icon_object = TestHelper.getObjectById("//div[@id='profile_about_title_id_icon']")
	
	if(TestHelper.isElementPresent(user_about_title_object) && TestHelper.isElementPresent(user_about_subTitle_object)) {
		String aboutTitle = WebUI.getText(user_about_title_object);
		String aboutSubTitle = WebUI.getText(user_about_subTitle_object);
		String aboutIcon = WebUI.getText(user_about_icon_object);
		
		if(!aboutTitle.equals("About") || !aboutSubTitle.equals("Info, sector and contact details") || !aboutIcon.equals("assets/icons/diasporaIcon/About_Active.svg")) {
			TestHelper.thrownException("Invalid about title and subtitle")
		}
	}
	
	TestObject network_role_title_object = TestHelper.getObjectById("//div[@id='profile_networks_title_id']")
	TestObject network_role_subTitle_object = TestHelper.getObjectById("//div[@id='profile_networks_subTitle_id']")
	TestObject network_role_icon_object = TestHelper.getObjectById("//div[@id='profile_networks_title_id_icon']")
	
	if(TestHelper.isElementPresent(network_role_title_object) && TestHelper.isElementPresent(network_role_subTitle_object)) {
		String networkRoleTitle = WebUI.getText(network_role_title_object);
		String networkRoleSubTitle = WebUI.getText(network_role_subTitle_object);
		String networkRoleIcon = WebUI.getText(network_role_icon_object);

		if(!networkRoleTitle.equals("Networks") || !networkRoleSubTitle.equals(firstName + "’s communities & orgs") || !networkRoleIcon.equals("assets/icons/diasporaIcon/Networks_Active.svg")) {
			TestHelper.thrownException("Invalid network title and subtitle")
		}
	}

	TestObject business_role_title_object = TestHelper.getObjectById("//div[@id='profile_businesses_title_id']")
	TestObject business_role_subTitle_object = TestHelper.getObjectById("//div[@id='profile_businesses_subTitle_id']")
	TestObject business_role_icon_object = TestHelper.getObjectById("//div[@id='profile_businesses_title_id_icon']")
	
	if(TestHelper.isElementPresent(business_role_title_object) && TestHelper.isElementPresent(business_role_subTitle_object)) {
		String businessRoleTitle = WebUI.getText(business_role_title_object);
		String businessRoleSubTitle = WebUI.getText(business_role_subTitle_object);
		String businessRoleIcon = WebUI.getText(business_role_icon_object);
		
		if(!businessRoleTitle.equals("Business") || !businessRoleSubTitle.equals("Companies, Startups, Freelancers") || !businessRoleIcon.equals("assets/icons/diasporaIcon/Business_Inactive.svg")) {
			TestHelper.thrownException("Invalid business title and subtitle")
		}
	}

	TestObject updates_title_object = TestHelper.getObjectById("//div[@id='profile_updates_title_id']")
	TestObject updates_subTitle_object = TestHelper.getObjectById("//div[@id='profile_updates_subTitle_id']")
	TestObject updates_icon_object = TestHelper.getObjectById("//div[@id='profile_updates_title_id_icon']")
	
	if(TestHelper.isElementPresent(updates_title_object) && TestHelper.isElementPresent(updates_subTitle_object)) {
		String updatesTitle = WebUI.getText(updates_title_object);
		String updatesSubTitle = WebUI.getText(updates_subTitle_object);
		String updatesIcon = WebUI.getText(updates_icon_object);
		
		if(!updatesTitle.equals("Updates") || !updatesSubTitle.equals(firstName + "’s recent posts & events") || !updatesIcon.equals("assets/icons/diasporaIcon/News_Active.svg")) {
			TestHelper.thrownException("Invalid updates title and subtitle")
		}

	}

	TestObject media_title_object = TestHelper.getObjectById("//div[@id='profile_media_title_id']")
	TestObject media_subTitle_object = TestHelper.getObjectById("//div[@id='profile_media_subTitle_id']")	
	TestObject media_icon_object = TestHelper.getObjectById("//div[@id='profile_media_title_id_icon']")
	
	if(TestHelper.isElementPresent(media_title_object) && TestHelper.isElementPresent(media_subTitle_object)) {
		String mediaTitle = WebUI.getText(media_title_object);
		String mediaSubTitle = WebUI.getText(media_subTitle_object);
		String mediaIcon = WebUI.getText(media_icon_object);
		
		if(!mediaTitle.equals("Media Gallery") || !mediaSubTitle.equals("Photos & videos") || !mediaIcon.equals("assets/icons/diasporaIcon/View_Gallery.svg")) {
			TestHelper.thrownException("Invalid media title and subtitle")
		}

	}	
}

def checkSubPagesEmptyState() {
	// Check Primary Role
		String primaryRoleErrorMessage = "Error: Invalid Primary Role Empty State";
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-primary-role']"))
		TestHelper.checkObjectText("//div[@id='no-primary-role-empty-state-title']", "No Roles Added yet" , primaryRoleErrorMessage);
		TestHelper.checkObjectText("//div[@id='no-primary-role-subtTitle-empty-state']", "You haven’t listed any roles yet" , primaryRoleErrorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no-primary-role-empty-state-image']", "assets/icons/emptyState/Business_Roles.svg", primaryRoleErrorMessage)
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_primary_role_back_action']"))

		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Manage_User_entity_action']"))
		checkManageEntityIcon();
		
	// Check Pages Following Page
		String follingNetworkPageErrorMessage = "Error: Invalid Network Following Empty State";
		String followingCompaniesPageErrorMessage = "Error: Invalid Companies Following Empty State";
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Pages I Follow_manage_entity_action']"))
		 
		TestHelper.checkObjectText("//div[@id='no_network_following_empty_state_title']", "No Networks" , follingNetworkPageErrorMessage);
		TestHelper.checkObjectText("//div[@id='no_network_following_empty_state_subtitle']", "You are not following any networks yet" , follingNetworkPageErrorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no_network_following_empty_state_image']", "assets/icons/emptyState/List.svg", follingNetworkPageErrorMessage)
	
		TestHelper.clickButton(TestHelper.getObjectById("//ion-segment-button[@id='business_tab_id']"))
		
		TestHelper.checkObjectText("//div[@id='no_companies_following_empty_state_title']", "No Business" , followingCompaniesPageErrorMessage);
		TestHelper.checkObjectText("//div[@id='no_companies_following_empty_state_subtitle']", "You are not following any business yet" , followingCompaniesPageErrorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no_companies_following_empty_state_image']", "assets/icons/emptyState/List.svg", followingCompaniesPageErrorMessage)

		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_following_back_id']"))
				
	// Check User Follower/Following Page 
		String followersUserPageErrorMessage = "Error: Invalid User Followers Empty State";
		String followingUserPageErrorMessage = "Error: Invalid User Following Empty State";
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Following and Followed User_manage_entity_action']"))
		 
		TestHelper.checkObjectText("//div[@id='no_user_follower_empty_state_title']", "No Followers" , followersUserPageErrorMessage);
		TestHelper.checkObjectText("//div[@id='no_user_follower_empty_state_subtitle']", "You don't have any followers yet" , followersUserPageErrorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no_user_follower_empty_state_image']", "assets/icons/emptyState/List.svg", followersUserPageErrorMessage)
	
		TestHelper.clickButton(TestHelper.getObjectById("//ion-segment-button[@id='followings_tab_id']"))
		
		TestHelper.checkObjectText("//div[@id='no_user_following_empty_state_title']", "No Following" , followingUserPageErrorMessage);
		TestHelper.checkObjectText("//div[@id='no_user_following_empty_state_subtitle']", "You’re not following anyone yet" , followingUserPageErrorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no_user_following_empty_state_image']", "assets/icons/emptyState/List.svg", followingUserPageErrorMessage)

		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_followers_back_id']"))
		
  		
	// Check Manage Network Role Page
		String manageNetworkPageErrorMessage = "Error: Invalid Manage Network Empty State";
 		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Network Roles_manage_entity_action']"))
		 
		TestHelper.checkObjectText("//div[@id='no_manage_network_empty_state_title']", "No Network Roles" , manageNetworkPageErrorMessage);
		TestHelper.checkObjectText("//div[@id='no_manage_network_empty_state_subtitle']", "You haven’t listed any network roles yet" , manageNetworkPageErrorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no_manage_network_empty_state_image']", "assets/icons/emptyState/Network_Roles.svg", manageNetworkPageErrorMessage)
		TestHelper.checkObjectText("//div[@id='no-network-role-empty-state-button']", "Add Role", manageNetworkPageErrorMessage)
		
		TestHelper.checkObjectText("//p[@id='manage_network_hint_title']", "Manage your roles", manageNetworkPageErrorMessage)
		TestHelper.checkObjectText("//p[@id='manage_network_hint_subTitle']", "You can edit, delete or set a role as your primary role in your profile. Tap the dots to get started", manageNetworkPageErrorMessage)
		 
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_network_role_back_action']"))
		
		
	// Check Manage Business Role Page
	   String manageBusinessPageErrorMessage = "Error: Invalid Manage Business Empty State";
		
	   TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Business Roles_manage_entity_action']"))
		
	   TestHelper.checkObjectText("//div[@id='no_manage_companies_empty_state_title']", "No Business Roles" , manageBusinessPageErrorMessage);
	   TestHelper.checkObjectText("//div[@id='no_manage_companies_empty_state_subtitle']", "You haven’t listed any business roles yet" , manageBusinessPageErrorMessage);
	   TestHelper.checkObjectImageSrc("//div[@id='no_manage_companies_empty_state_image']", "assets/icons/emptyState/Business_Roles.svg", manageBusinessPageErrorMessage)
	   TestHelper.checkObjectText("//div[@id='no-business-role-empty-state-button']", "Add Role", manageBusinessPageErrorMessage)
	   
	   TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_business_role_back_action']"))
	   TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-User-back-button']"))
	   
	   
}

def checkUpdateEmptyState(String profileName) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='no-updates-empty-state-title']"));
	long now = new Date().getTime();
	String postTitle = 'New post title add now_' + now;
	String postDescription = 'New post description add now_' + now;
	UserActionsHelper.addPost(postTitle, postDescription);
	TestHelper.verifyCardExist(profileName, postTitle, null, false);
	
	TestObject manageCardObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='profile_manage_updates']")
	boolean manageCardObjectExist = TestHelper.isElementPresent(manageCardObject);
	if(!manageCardObjectExist) {
		TestHelper.thrownException("Manage card button not exist");
	}
	
	TestHelper.clickButton(manageCardObject);
	String cartId = postTitle + '_descriptionId';
	TestHelper.clickButton(TestHelper.getObjectById("//span[@id='" + cartId + "']"));
		

	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='card_details_manage_button']"))
	WebUI.delay(1);
	TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Delete Post_user_action']"))
	WebUI.delay(1);
	
	WebElement deleteCardConfirmation = TestHelper.getItem('remove-new-card', 0)
	deleteCardConfirmation.click();
	
	WebUI.delay(1);
	
	String updateErrorMessage = "Error: Invalid Updates List Empty State";
	TestHelper.checkObjectText("//div[@id='no_updates_empty_state_title']", "Nothing to Show Here" , updateErrorMessage);
	TestHelper.checkObjectText("//div[@id='no_updates_empty_state_subtitle']", "There are no entries to display on this page" , updateErrorMessage);
	TestHelper.checkObjectImageSrc("//div[@id='no_updates_empty_state_image']", "assets/icons/emptyState/List.svg", updateErrorMessage)

	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back_button_cardListing']"))
}

def checkMediaEmptyState() {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='no-media-empty-state']"));

	long now = new Date().getTime();
	String imageCaption = 'caption' + now;
	UserActionsHelper.addMedia(imageCaption);
	TestObject addPhotoAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='profile_manage_media']")
	WebUI.click(addPhotoAction)
	
	TestHelper.checkIfImageAddedSuccessfully(true, 0);
	TestHelper.removeImage();
	
	WebUI.delay(1);
	String mediaErrorMessage = "Error: Invalid Media Empty State";
	TestHelper.checkObjectText("//div[@id='no_gallery_images_empty_state_title']", "No Photo Or Video Added" , mediaErrorMessage);
	TestHelper.checkObjectImageSrc("//div[@id='no_gallery_images_empty_state_image']", "assets/icons/emptyState/List.svg", mediaErrorMessage)

	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back_button_gallery']"))
}

def checkManageEntityIcon() {
	String manage_entity_error_message = 'Invalid manage entity info';
	
	TestHelper.checkObjectText("//div[@id='Basic Information_manage_entity_action']", "Basic Information" , manage_entity_error_message);
	TestHelper.checkObjectText("//div[@id='Basic Information_manage_entity_subtitle_action']", "Photo, sector, country and more…" , manage_entity_error_message);
	TestHelper.checkObjectImageSrc("//div[@id='Basic Information_manage_entity_icon_action']", "assets/icons/diasporaIcon/Basic_Information.svg", manage_entity_error_message)
 	  
	TestHelper.checkObjectText("//div[@id='Privacy Settings_manage_entity_action']", "Privacy Settings" , manage_entity_error_message);
	TestHelper.checkObjectText("//div[@id='Privacy Settings_manage_entity_subtitle_action']", "Who can see your contact details" , manage_entity_error_message);
	TestHelper.checkObjectImageSrc("//div[@id='Privacy Settings_manage_entity_icon_action']", "assets/icons/diasporaIcon/Privacy.svg", manage_entity_error_message)
 	
	TestHelper.checkObjectText("//div[@id='Pages I Follow_manage_entity_action']", "Pages I Follow" , manage_entity_error_message);
	TestHelper.checkObjectText("//div[@id='Pages I Follow_manage_entity_subtitle_action']", "Manage pages you follow" , manage_entity_error_message);
	TestHelper.checkObjectImageSrc("//div[@id='Pages I Follow_manage_entity_icon_action']", "assets/icons/diasporaIcon/following-Pages.svg", manage_entity_error_message)
 	
	TestHelper.checkObjectText("//div[@id='Following and Followed User_manage_entity_action']", "Following and Followed User" , manage_entity_error_message);
	TestHelper.checkObjectText("//div[@id='Following and Followed User_manage_entity_subtitle_action']", "Manage following and followed" , manage_entity_error_message);
	TestHelper.checkObjectImageSrc("//div[@id='Following and Followed User_manage_entity_icon_action']", "assets/icons/diasporaIcon/Accounts_I_Follow.svg", manage_entity_error_message)
 
	TestHelper.checkObjectText("//div[@id='Network Roles_manage_entity_action']", "Network Roles" , manage_entity_error_message);
	TestHelper.checkObjectText("//div[@id='Network Roles_manage_entity_subtitle_action']", "Manage your network roles" , manage_entity_error_message);
	TestHelper.checkObjectImageSrc("//div[@id='Network Roles_manage_entity_icon_action']", "assets/icons/diasporaIcon/Network_Roles.svg", manage_entity_error_message)
 
	TestHelper.checkObjectText("//div[@id='Business Roles_manage_entity_action']", "Business Roles" , manage_entity_error_message);
	TestHelper.checkObjectText("//div[@id='Business Roles_manage_entity_subtitle_action']", "Manage your business roles" , manage_entity_error_message);
	TestHelper.checkObjectImageSrc("//div[@id='Business Roles_manage_entity_icon_action']", "assets/icons/diasporaIcon/Business_Roles.svg", manage_entity_error_message)		  		 
}