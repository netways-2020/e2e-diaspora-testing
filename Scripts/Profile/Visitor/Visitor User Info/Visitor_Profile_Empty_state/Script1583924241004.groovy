import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.navigateToUrl(TestHelper.getDiaspora_profileUrl())
WebUI.delay(3);
checkProfileMenuAction();

WebUI.delay(2);
WebUI.closeBrowser();

def checkProfileMenuAction() {

	TestObject profileMenuAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='profile_menu_action_id']")
	TestObject follow_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Follow_User_entity_action']")
	TestObject message_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Message_User_entity_action']")

	if(!TestHelper.isElementPresent(follow_entity_object)) {
		TestHelper.thrownException("Follow Action not present");
	} else if(!TestHelper.isElementPresent(message_entity_object)) {
		TestHelper.thrownException("Message Action not present");
	}
	
	if(TestHelper.isElementPresent(profileMenuAction)) {
		TestHelper.clickButton(profileMenuAction);

		TestObject reportProfileAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Report A Problem_user_action']")
		TestObject shareProfileAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Share_user_action']")
		
		if(!TestHelper.isElementPresent(reportProfileAction) || !TestHelper.isElementPresent(shareProfileAction)) {
			TestHelper.thrownException("User Action not present");
		} else {
			print("Check Complete")
		} 		
	} else {
		TestHelper.thrownException("Menu Action not present");
	}
	

}