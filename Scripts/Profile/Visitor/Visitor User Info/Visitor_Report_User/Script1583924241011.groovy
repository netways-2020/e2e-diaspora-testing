import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable


long date = new Date().getTime();
String registeredEmail = 'netways_user' + date + '@gmail.com'
String password = 'cvW8qx4B2o3F4VwP/kNsqA=='

String firstName = 'FirstName_' + date;
String lastName = 'LastName_' + date;
String fullName = firstName + ' ' + lastName;
String city = 'Hermel';
String reportDescription = 'Report description for '+ fullName;
String reportContact = 'Report Contact for '+ fullName;

TestHelper.registerNewUser(registeredEmail, password, firstName, lastName, city, true);
followAutomationTestProfile();

TestHelper.loginWithAutomationTestingUser();
reportFollowedUser(fullName, reportDescription, reportContact);

WebUI.delay(2);
WebUI.closeBrowser();

def followAutomationTestProfile() {
	WebUI.navigateToUrl(TestHelper.getAutomationTesting_profileUrl())

	TestObject userFollowAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Follow_User_entity_action']")
	boolean userFollowActionExist = TestHelper.isElementPresent(userFollowAction)
	
	if(userFollowActionExist) {
		TestHelper.clickButton(userFollowAction);
		WebUI.delay(3);
	}

	WebUI.navigateToUrl(TestHelper.getHomeUrl());
	
	TestObject manageProfilSection = TestHelper.getObjectById("//div[@id='manageProfileId']")
	TestHelper.clickButton(manageProfilSection);
	
	TestObject logoutAction = TestHelper.getObjectById("//div[@id='logout-id']")
	TestHelper.clickButton(logoutAction);
}

def reportFollowedUser(String fullName, String reportDescription, String reportContact) {
	TestObject userFollowerSection = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='user_follower_section']")
	boolean userFollowerSectionExist = TestHelper.isElementPresent(userFollowerSection)
	
	if(userFollowerSectionExist) {
		TestObject editFollowersSection = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='user_follower_action']")
		TestHelper.clickButton(editFollowersSection)

		TestObject manageFollowerSearchInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='manage_followers_search']")
	 	TestHelper.setInputValue(manageFollowerSearchInput, fullName)
		WebUI.delay(4);
		
		String followedUser = fullName + '_profile';
		TestObject followedUserObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+ followedUser +"']")
		TestHelper.clickButton(followedUserObject);
		reportUser(reportDescription, reportContact);
		println('Report has been send successfully');
		WebUI.closeBrowser()		 
	} else {
		TestHelper.thrownException("User hasn't any followers yet");
	}
}


def reportUser(String reportDescription, String reportContact) {
	TestObject profileMenuAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='profile_menu_action_id']")
	TestHelper.clickButton(profileMenuAction);

	TestObject reportProfileAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Report A Problem_user_action']")
	TestHelper.clickButton(reportProfileAction);

	UserActionsHelper.sendReport(reportDescription, reportContact);
}
