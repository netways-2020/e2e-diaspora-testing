import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement

WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Login'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.navigateToUrl(TestHelper.getProfileWithFullDetailsUrl());

WebUI.delay(2);
WebElement profileNameObject = TestHelper.getItem('profile-name-title', 0)
String profileName = profileNameObject.text;
String firstName = profileName.split(" ")[0];

checkMyProfileActions(firstName);

WebUI.closeBrowser();
 
def checkMyProfileActions(String firstName) {
	TestObject user_about_title_object = TestHelper.getObjectById("//div[@id='profile_about_title_id']")
	
	if(TestHelper.isElementPresent(user_about_title_object)) {
		String aboutTitle = WebUI.getText(user_about_title_object);
		if(!aboutTitle.equals("About")) {
			TestHelper.thrownException("Invalid about title")
		}
	} else {
		TestHelper.thrownException("Invalid sections data found");
	}
	
	TestObject user_about_subTitle_object = TestHelper.getObjectById("//div[@id='profile_about_subTitle_id']")
	if(TestHelper.isElementPresent(user_about_subTitle_object)) {
		String aboutSubTitle = WebUI.getText(user_about_subTitle_object);
		
		if(!aboutSubTitle.equals("Info, sector and contact details")) {
			TestHelper.thrownException("Invalid about subtitle")
		}
	} else {
		TestHelper.thrownException("Invalid sections data found");
	}
	
	
	TestObject peoples_title_object = TestHelper.getObjectById("//div[@id='profile_people_title_id']")
	TestObject peoples_subTitle_object = TestHelper.getObjectById("//div[@id='profile_people_subTitle_id']")
	TestObject peoples_more_object = TestHelper.getObjectById("//div[@id='profile_people_more_id']")
	
	if(TestHelper.isElementPresent(peoples_title_object) && TestHelper.isElementPresent(peoples_subTitle_object) && TestHelper.isElementPresent(peoples_more_object)) {
		String peoplesTitle = WebUI.getText(peoples_title_object);
		String peoplesSubTitle = WebUI.getText(peoples_subTitle_object);
		String peoplesMoreAction = WebUI.getText(peoples_more_object);
 
		if(!peoplesTitle.equals("People") || !peoplesSubTitle.equals("Following & followed by " + firstName) || !peoplesMoreAction.equals("See All")) {
			TestHelper.thrownException("Invalid peoples title and subtitle")
		}
		
		TestHelper.clickButton(peoples_more_object);
		
		boolean countainFollowers = TestHelper.checkCards('profile_followers_card');
		TestHelper.clickButton(TestHelper.getObjectById("//ion-segment-button[@id='followings_tab_id']"));
		boolean countainFollowings = TestHelper.checkCards('profile_following_card');
		
		if(!countainFollowers && !countainFollowings) {
			TestHelper.thrownException("Follower page not conatins any followers or followings");
		}
				
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_followers_back_id']"));
	} else {
		TestHelper.thrownException("Invalid sections data found");
	}
	 
	TestObject network_role_title_object = TestHelper.getObjectById("//div[@id='profile_networks_title_id']")
	TestObject network_role_subTitle_object = TestHelper.getObjectById("//div[@id='profile_networks_subTitle_id']")
	TestObject network_role_more_object = TestHelper.getObjectById("//div[@id='profile_networks_more_id']")
	
	if(TestHelper.isElementPresent(network_role_title_object) || TestHelper.isElementPresent(network_role_subTitle_object) || TestHelper.isElementPresent(network_role_more_object)) {
		String networkRoleTitle = WebUI.getText(network_role_title_object);
		String networkRoleSubTitle = WebUI.getText(network_role_subTitle_object);
		String networkRoleMoreAction = WebUI.getText(network_role_more_object);
 
		if(!networkRoleTitle.equals("Networks") || !networkRoleSubTitle.equals(firstName + "’s communities & orgs") || !networkRoleMoreAction.equals("More")) {
			TestHelper.thrownException("Invalid network title and subtitle")
		}
		
		TestHelper.clickButton(network_role_more_object);
		
		
		if(!TestHelper.checkCards('profile_network_role_card')) {
			TestHelper.thrownException("Network page not contains any role");
		}

		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_network_role_back_action']"));
	} else {
		TestHelper.thrownException("Invalid sections data found");
	}

	TestObject business_role_title_object = TestHelper.getObjectById("//div[@id='profile_businesses_title_id']")
	TestObject business_role_subTitle_object = TestHelper.getObjectById("//div[@id='profile_businesses_subTitle_id']")
	TestObject business_role_more_object = TestHelper.getObjectById("//div[@id='profile_businesses_more_id']")
	
	if(TestHelper.isElementPresent(business_role_title_object) && TestHelper.isElementPresent(business_role_subTitle_object) && TestHelper.isElementPresent(business_role_more_object)) {
		String businessRoleTitle = WebUI.getText(business_role_title_object);
		String businessRoleSubTitle = WebUI.getText(business_role_subTitle_object);
		String businessRoleMoreAction = WebUI.getText(business_role_more_object);
		
		if(!businessRoleTitle.equals("Business") || !businessRoleSubTitle.equals("Companies, Startups, Freelancers") || !businessRoleMoreAction.equals("More")) {
			TestHelper.thrownException("Invalid business title and subtitle")
		}
		
		TestHelper.clickButton(business_role_more_object);
		
		if(!TestHelper.checkCards('profile_business_role_card')) {
			TestHelper.thrownException("Business page not contains any role");
		}
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_business_role_back_action']"));
	} else {
		TestHelper.thrownException("Invalid sections data found");
	}

	TestObject updates_title_object = TestHelper.getObjectById("//div[@id='profile_updates_title_id']")
	TestObject updates_subTitle_object = TestHelper.getObjectById("//div[@id='profile_updates_subTitle_id']")
	TestObject updates_more_object = TestHelper.getObjectById("//div[@id='profile_updates_more_id']")
	
	if(TestHelper.isElementPresent(updates_title_object) && TestHelper.isElementPresent(updates_subTitle_object) && TestHelper.isElementPresent(updates_more_object)) {
		String updatesTitle = WebUI.getText(updates_title_object);
		String updatesSubTitle = WebUI.getText(updates_subTitle_object);
		String updatesMoreAction = WebUI.getText(updates_more_object);
		
		if(!updatesTitle.equals("Updates") || !updatesSubTitle.equals(firstName + "’s recent posts & events") || !updatesMoreAction.equals("More")) {
			TestHelper.thrownException("Invalid updates title and subtitle")
		}

		TestHelper.clickButton(updates_more_object);
		
		if(!TestHelper.checkCards('listing_cards_card_item')) {
			TestHelper.thrownException("Card listing page not contains any card");
		}
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back_button_cardListing']"));
	} else {
		TestHelper.thrownException("Invalid sections data found");
	}

	TestObject media_title_object = TestHelper.getObjectById("//div[@id='profile_media_title_id']")
	TestObject media_subTitle_object = TestHelper.getObjectById("//div[@id='profile_media_subTitle_id']")	
	TestObject media_more_object = TestHelper.getObjectById("//div[@id='profile_media_more_id']")
	
	if(TestHelper.isElementPresent(media_title_object) && TestHelper.isElementPresent(media_subTitle_object) && TestHelper.isElementPresent(media_more_object)) {
		String mediaTitle = WebUI.getText(media_title_object);
		String mediaSubTitle = WebUI.getText(media_subTitle_object);
		String mediaMoreAction = WebUI.getText(media_more_object);
		
		if(!mediaTitle.equals("Media Gallery") || !mediaSubTitle.equals("Photos & videos") || !mediaMoreAction.equals("More")) {
			TestHelper.thrownException("Invalid media title and subtitle")
		}

		TestHelper.clickButton(media_more_object);
		
		if(!TestHelper.checkCards('gallery_image_card')) {
			TestHelper.thrownException("Card listing page not contains any card");
		}
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back_button_gallery']"));

	} else {
		TestHelper.thrownException("Invalid sections data found");
	}
}

