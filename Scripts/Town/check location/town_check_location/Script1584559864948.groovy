import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
String email = "adminAaba@netways.com"
String password = "Test@123"
String originCity = "Aaba"
AuthHelpers.townLogin(email, password, originCity)

int RandomtownInfoDataRow = 1

String farFromBeirutValue = findTestData('Data Files/towninfo').getValue(9, RandomtownInfoDataRow)
String aboveSeaValue = findTestData('Data Files/towninfo').getValue(10, RandomtownInfoDataRow)
String kmInArea = findTestData('Data Files/towninfo').getValue(11, RandomtownInfoDataRow)
//location and area
TestObject farFromBeirutDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'location_'+ farFromBeirutValue) + '\']')
TestObject aboveSeaValueDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'location_'+ aboveSeaValue) + '\']')
TestObject kmInAreaDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'location_'+ kmInArea) + '\']')
TestObject mapImageDiv = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='town-map']")

//location test

if(!TestHelper.isElementPresent(farFromBeirutDiv)) {
	TestHelper.thrownException(' error in far from beirut')
}else{
	println('no error in far from beirut')
}
if(!TestHelper.isElementPresent(aboveSeaValueDiv)) {
	TestHelper.thrownException(' error in above see value')
}else{
	println('no error in above see value')
}
if(!TestHelper.isElementPresent(kmInAreaDiv)) {
	TestHelper.thrownException(' error in km in area')
}else{
	println('no error km in area')
}
if(!TestHelper.isElementPresent(mapImageDiv)) {
	TestHelper.thrownException(' error in map image ')
}else{
	println('no error map image')
}

WebUI.closeBrowser()
