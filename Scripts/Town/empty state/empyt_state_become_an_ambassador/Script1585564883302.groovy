import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

String firstname = "ali" +  + new Date().getTime();
String lastname = "mahdi" +  + new Date().getTime();
String originCity = 'Froun'
String residenceCity = 'Froun'

String email = "mahdi@" +  + new Date().getTime() + '.com';
String errorMessage = "'Error in become an ambassador sections'"

AuthHelpers.townRegistration(originCity, residenceCity, firstname, lastname, email)

TestHelper.checkObjectExit("//div[@id='bocome_town_ambassador']", errorMessage);
TestHelper.checkObjectText("//div[@id='become_ambassador_title']", "Become Your Town’s Ambassador" , errorMessage);
TestHelper.checkObjectText("//div[@id='become_ambassador_subtitletitle']", "Be your community’s champion. Network with the Diaspora and assist the town in enriching your town’s page" , errorMessage);

TestHelper.checkObjectText("//div[@id='town_Apply Now_action_title_Id']", "Apply Now" , errorMessage);
TestHelper.checkObjectText("//div[@id='town_Apply Now_action_subTitle_Id']", "Tap here to get started" , errorMessage);
TestHelper.checkObjectImageSrc("//div[@id='town_Apply Now_action_icon_Id']", "assets/icons/diasporaIcon/Town_Ambassador_EmptyState.svg", errorMessage)

WebUI.closeBrowser()

