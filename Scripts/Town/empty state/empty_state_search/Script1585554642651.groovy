import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

String email = "emptyAmbassador@gmail.com"
String password = "Test@123"
String originCity = "Aouainat"

AuthHelpers.townLogin(email, password, originCity)
checkEmtyFollowerSearch()
checkEmtyAdministratorSearch()
checkEmtyMemberSearch()
WebUI.closeBrowser()

def checkEmtyFollowerSearch() {
	String errorMessage = "Error: in image or title";
	TestHelper.clickButton(TestHelper.getObjectById("//span[@id='follower_number_tag_label']"))
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='search-input-follower']"), 'hjsdfdjkdfhjdjkhdjkjhksdf')
	TestHelper.checkObjectText("//div[@id='follower_town_not_found_title']", "No Results Found" , errorMessage);
	TestHelper.checkObjectImageSrc("//div[@id='follower_town_not_found_image']", "assets/icons/emptyState/Person.svg", errorMessage)
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='town_follower_back_button']"))
}

def checkEmtyAdministratorSearch() {
	String errorMessage = "Error: in image or title";
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='edit_administrator']"))
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='search_manage_town']"), 'hjsdfdjkdfhjdjkhdjkjhksdf')
	TestHelper.checkObjectText("//div[@id='administrator_town_not_found_title']", "No Results Found" , errorMessage);
	TestHelper.checkObjectImageSrc("//div[@id='administrator_town_not_found_image']", "assets/icons/emptyState/Person.svg", errorMessage)
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='add_admin_back_button']"))
}


def checkEmtyMemberSearch() {
	String errorMessage = "Error: in image or title";
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='view_more_member']"))
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='search_town_member']"), 'hjsdfdjkdfhjdjkhdjkjhksdf')
	TestHelper.checkObjectText("//div[@id='member_town_not_found_title']", "No Results Found" , errorMessage);
	TestHelper.checkObjectImageSrc("//div[@id='member_town_not_found_image']", "assets/icons/emptyState/Person.svg", errorMessage)
}
