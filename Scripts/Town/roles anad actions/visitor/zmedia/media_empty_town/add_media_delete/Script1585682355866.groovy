import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import static org.junit.Assert.*

import org.junit.Test
import org.openqa.selenium.WebElement as WebElement

String firstname = "ali" +  + new Date().getTime();
String lastname = "mahdi" +  + new Date().getTime();
String originCity = 'Froun'
String residenceCity = 'Froun'
String email = "mahdi@" +  + new Date().getTime() + '.com';
TestObject editMediaSection = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='town_manage_media']")

//Start Testing
AuthHelpers.townRegistration(originCity, residenceCity, firstname, lastname, email)
WebUI.navigateToUrl('http://localhost:8100/app/town/a25d93b7-0304-e711-80e1-00155d003d3b')
checkTitleOfSection(editMediaSection)
//Media Test

TestHelper.getObjectById("//div[@id='town_media']")
TestObject emptyStateUserGallery = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-town-media-empty-state']")
boolean emptyStateUserGalleryExist = TestHelper.isElementPresent(emptyStateUserGallery)

long now = new Date().getTime();
String imageCaption = 'caption' + now;

if (emptyStateUserGalleryExist) {
	println("Hasn't images");
	WebUI.click(emptyStateUserGallery)
	
	UserActionsHelper.addMedia(imageCaption);
	
	TestObject addPhotoAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='town_manage_media']")
	WebUI.click(addPhotoAction)
	
	checkIfImageAddedSuccessfully(true, 0);
	removeImage();
} else {
	println("Has images");

	TestObject emptyStateAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='town_manage_media']")
	WebUI.click(emptyStateAction)
	 WebUI.delay(4)
	int imageCountBeforAdd = TestHelper.countElementByClassName('gallery-container');
	 println(imageCountBeforAdd)
	TestObject addPhotoAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='addPhotoAction']")
	WebUI.click(addPhotoAction)

	UserActionsHelper.addTownMedia(imageCaption);
	WebUI.waitForElementPresent(addPhotoAction, 10)
	checkIfImageAddedSuccessfully(false, imageCountBeforAdd);
	removeImage()
}
WebUI.closeBrowser()

def removeImage() {
	int imageCountBeforAdd = TestHelper.countElementByClassName('gallery-container');
	WebElement element = TestHelper.getItem('gallery-container', 0);
	element.click();
	
	TestHelper.checkImageLoaded("image_detail_id");
	
	TestObject imageCaption = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='image-caption-id']")
	boolean imageCaptionExist = TestHelper.isElementPresent(imageCaption);

	if(imageCaptionExist) {
		print("Image caption exist");
	} else {
		TestHelper.thrownException("Image caption not exist");
	}

	TestObject removeAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='galler_image_id']")
	boolean removeActionExist = TestHelper.isElementPresent(removeAction);
 
	if(removeActionExist) {
		TestHelper.clickButton(removeAction)
		UserActionsHelper.removeEmptYMedia();
		this.checkIfImageRemovedSuccessfully(imageCountBeforAdd);
	} else {
		TestHelper.thrownException("Doesn't have the privilege tot delete an image");
	}
}


def checkIfImageAddedSuccessfully(boolean firstImageAdded, int imageCountBeforAdd) {
	int imageCount = TestHelper.countElementByClassName('gallery-container');
	println(imageCount)
	if(firstImageAdded) {
		if(imageCount == 1) {
			println("Image added successfully");
		} else {
			TestHelper.thrownException("Image not added successfully");
		}
	} else {
	   if((imageCountBeforAdd + 1) == imageCount) {
		   println("Image added successfully");
	   } else {
		   TestHelper.thrownException("Image not added successfully");
	   }
	}
}

def checkIfImageRemovedSuccessfully(int imageCountBeforAdd) {
	int imageCount = TestHelper.countElementByClassName('gallery-container');
	
	if((imageCountBeforAdd - 1) == imageCount) {
	   println("Image deleted successfully");
	   WebUI.closeBrowser();
   } else {
	   TestHelper.thrownException("Image not deleted successfully");
   }
}

def checkTitleOfSection(editMediaSection) {
	TestObject townMedia_title_object = TestHelper.getObjectById("//div[@id='town_media_title_id']")
	TestObject townMedia_subTitle_object = TestHelper.getObjectById("//div[@id='town_media_subTitle_id']")
	boolean editMediaSectionDiv = TestHelper.isElementPresent(editMediaSection)
	if(TestHelper.isElementPresent(townMedia_title_object) && TestHelper.isElementPresent(townMedia_subTitle_object)  && !editMediaSectionDiv) {
		String mediaTitle = WebUI.getText(townMedia_title_object);
		String mediaSubTitle = WebUI.getText(townMedia_subTitle_object);
		
		if(!mediaTitle.equals("Media Gallery") || !mediaSubTitle.equals('Photos & videos')) {
			TestHelper.thrownException("Invalid media title and subtitle")
		}
	}else {
		TestHelper.thrownException("town mediaTitle_object Title Action is wrong")
	}
}