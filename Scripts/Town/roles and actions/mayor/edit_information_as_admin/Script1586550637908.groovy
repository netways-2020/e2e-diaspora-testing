import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

String email = "fullmayor@gmail.com"
String password = "Test@123"
String originCity = "Aaba"
TestObject cityOrigineDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + originCity) + '\']')

// random About Section Values used in manage about section
String randomShortDescriptionValue = 'Test Short Description' + new Date().getTime()
String randomEtyMologieValue = 'Etymology Value' + new Date().getTime()
String randomHistroyValue = 'Hustory Value' + new Date().getTime()
String randomPoBoxValue = new Date().getTime()
String randomPhoneNumberValue = new Date().getTime()
String randomEmailAddressValue = ('testEmail' + new Date().getTime()) + '@gmail.com'
String randomWebValue = ('TestWebsite' + new Date().getTime()) + '.com'
String randomFaceBookValue = ('www.TestFacebook' + new Date().getTime()) + '.com'
String randomInstagramValue = ('www.TestInstagram' + new Date().getTime()) + '.com'
String randomTwitterValue = ('www.TestTwitter' + new Date().getTime()) + '.com'

// div of about section value to scheck if the value was updated
TestObject ShortDescriptionValue = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'ser_shortBio_id_' + randomShortDescriptionValue) + '\']')
TestObject PoBoxSectionValue = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'PO_BOX_' + randomPoBoxValue) + '\']')
TestObject PhoneNumberValue = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'town_contact_phone_id_+961' +  randomPhoneNumberValue) + '\']')
TestObject EmailAddressValue = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'user_contact_email_id_' + randomEmailAddressValue) + '\']')
TestObject WebValue = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'user_contact_website_id_' + randomWebValue) + '\']')

// the image of the social media account after added
TestObject facebookIconValue = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//img[@id=\'town_facebook_id\']')
TestObject twitterIconValue = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//img[@id=\'town_twitter_id\']')
TestObject instagramIconValue = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//img[@id=\'town_instagram_id\']')

//Etymology of town
TestObject etymologyDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\''+ randomEtyMologieValue) + '\']')
TestObject editEtymologyDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'edit_etymology\']')

//Start testing

AuthHelpers.townLogin(email, password, originCity)
fillMenageTownForm(randomShortDescriptionValue, randomEtyMologieValue, randomHistroyValue, randomPhoneNumberValue, randomEmailAddressValue, randomWebValue, randomPoBoxValue, randomFaceBookValue, randomInstagramValue,  randomTwitterValue)
checkAboutSection(ShortDescriptionValue, PoBoxSectionValue, PhoneNumberValue, EmailAddressValue,  WebValue, facebookIconValue, twitterIconValue, instagramIconValue)
checkContactAction()
checkTitleOfSection()
//check Etimlogy
TestHelper.isElementPresent(etymologyDiv)
TestHelper.isElementPresent(editEtymologyDiv)

//Functions

def fillMenageTownForm(randomShortDescriptionValue, randomEtyMologieValue, randomHistroyValue, randomPhoneNumberValue, randomEmailAddressValue, randomWebValue, randomPoBoxValue, randomFaceBookValue, randomInstagramValue,  randomTwitterValue) {
TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'maincard_Manage\']'))
TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'Town Information_manage_entity_action\']'))
TestHelper.clickButton(TestHelper.getObjectById('//textarea[@id=\'about-section\']'))
TestHelper.setInputValue(TestHelper.getObjectById('//textarea[@id=\'about-section\']'), randomShortDescriptionValue)
TestHelper.clickButton(TestHelper.getObjectById('//textarea[@id=\'etomology-section\']'))
TestHelper.setInputValue(TestHelper.getObjectById('//textarea[@id=\'etomology-section\']'), randomEtyMologieValue)
TestHelper.clickButton(TestHelper.getObjectById('//textarea[@id=\'history-section\']'))
TestHelper.setInputValue(TestHelper.getObjectById('//textarea[@id=\'history-section\']'), randomHistroyValue)
TestHelper.clickButton(TestHelper.getObjectById('//input[@id=\'phone-section\']'))
TestHelper.setInputValue(TestHelper.getObjectById('//input[@id=\'phone-section\']'), randomPhoneNumberValue)
TestHelper.clickButton(TestHelper.getObjectById('//input[@id=\'email-section\']'))
TestHelper.setInputValue(TestHelper.getObjectById('//input[@id=\'email-section\']'), randomEmailAddressValue)
TestHelper.clickButton(TestHelper.getObjectById('//input[@id=\'website-section\']'))
TestHelper.setInputValue(TestHelper.getObjectById('//input[@id=\'website-section\']'), randomWebValue)
TestHelper.clickButton(TestHelper.getObjectById('//input[@id=\'pobox-section\']'))
TestHelper.setInputValue(TestHelper.getObjectById('//input[@id=\'pobox-section\']'), randomPoBoxValue)
TestHelper.clickButton(TestHelper.getObjectById('//input[@id=\'facebook-section\']'))
TestHelper.setInputValue(TestHelper.getObjectById('//input[@id=\'facebook-section\']'), randomFaceBookValue)
TestHelper.clickButton(TestHelper.getObjectById('//input[@id=\'instagram-section\']'))
TestHelper.setInputValue(TestHelper.getObjectById('//input[@id=\'instagram-section\']'), randomInstagramValue)
TestHelper.clickButton(TestHelper.getObjectById('//input[@id=\'twitter-section\']'))
TestHelper.setInputValue(TestHelper.getObjectById('//input[@id=\'twitter-section\']'), randomTwitterValue)
TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'save_Town_info\']'))
TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage-Municipality-back-button\']'))
}

def checkAboutSection(ShortDescriptionValue, PoBoxSectionValue, PhoneNumberValue, EmailAddressValue,  WebValue, facebookIconValue, twitterIconValue, instagramIconValue) {
	WebUI.verifyElementPresent(ShortDescriptionValue, 0)
	WebUI.verifyElementPresent(PoBoxSectionValue, 0)
	WebUI.verifyElementPresent(PhoneNumberValue, 0)
	WebUI.verifyElementPresent(EmailAddressValue, 0)
	WebUI.verifyElementPresent(WebValue, 0)
	WebUI.verifyElementPresent(facebookIconValue, 0)
	WebUI.verifyElementPresent(twitterIconValue, 0)
	WebUI.verifyElementPresent(instagramIconValue, 0)
}

def checkContactAction() {
	TestObject callButtonDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//button[@id=\'callButton\']')
	TestObject emailButtonDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//button[@id=\'emailButton\']')
	
	boolean callButtonAction = TestHelper.isElementPresent(callButtonDiv)
	boolean emailButtonAction = TestHelper.isElementPresent(emailButtonDiv)
	
	if(callButtonAction || emailButtonAction) {
		TestHelper.thrownException("admin has wrong access")
	}
	else {
		println("admin has the right access")
	}
}

def checkTitleOfSection() {
		TestObject town_about_title_object = TestHelper.getObjectById("//div[@id='town_about_title_id']")
		TestObject town_about_subTitle_object = TestHelper.getObjectById("//div[@id='town_about_subTitle_id']")
		TestObject town_about_object = TestHelper.getObjectById("//div[@id='edit_about_town']")
		TestObject about_icon_object = TestHelper.getObjectById("//div[@id='town_about_title_id_icon']")
		
		if(TestHelper.isElementPresent(town_about_title_object) && TestHelper.isElementPresent(town_about_subTitle_object) && TestHelper.isElementPresent(town_about_object)) {
			String aboutTitle = WebUI.getText(town_about_title_object);
			String aboutSubTitle = WebUI.getText(town_about_subTitle_object);
			String aboutIcon = WebUI.getText(about_icon_object);
			String aboutActionTitle = WebUI.getText(town_about_object);
			if(!aboutTitle.equals("About") || !aboutSubTitle.equals("Municipality & town info") || !aboutActionTitle.equals("Edit") || !aboutIcon.equals("assets/icons/diasporaIcon/About_Active.svg")) {
				TestHelper.thrownException("Invalid about title and subtitle")
			}
			
			TestHelper.clickButton(town_about_object);
			TestHelper.clickButton(TestHelper.getObjectById("//div[@id='edit-town-back-button']"));
		}else {
			TestHelper.thrownException("About Title Action is wrong")
	}
}


