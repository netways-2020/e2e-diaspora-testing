import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

String email = "fullmayor@gmail.com"
String password = "Test@123"
String originCity = "Aaba"
TestObject cityOrigineDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + originCity) + '\']')



//Start Testing
AuthHelpers.townLogin(email, password, originCity)
checkTitleOfSection()
WebUI.closeBrowser()


def checkTitleOfSection() {
	TestObject etymologie_title_object = TestHelper.getObjectById("//div[@id='town_etymology_title_id']")
	TestObject etymologie_subTitle_object = TestHelper.getObjectById("//div[@id='town_etymology_subTitle_id']")
	TestObject townEtymology_more_object = TestHelper.getObjectById("//div[@id='edit_etymology']")
	TestObject etymology_icon_object = TestHelper.getObjectById("//div[@id='town_etymology_title_id_icon']")
	
	if(TestHelper.isElementPresent(etymologie_title_object) && TestHelper.isElementPresent(etymologie_subTitle_object) && TestHelper.isElementPresent(townEtymology_more_object)) {
		String etymologyTitle = WebUI.getText(etymologie_title_object);
		String etimologySubTitle = WebUI.getText(etymologie_subTitle_object);
		String etymologyEditAction = WebUI.getText(townEtymology_more_object);
		String etimologyIcon = WebUI.getText(etymology_icon_object);
		if(!etymologyTitle.equals("Etymology") || !etimologySubTitle.equals('Town name origin') || !etymologyEditAction.equals("Edit") || !etimologyIcon.equals("assets/icons/diasporaIcon/etymology.svg")) {
			TestHelper.thrownException("Invalid etymology title and subtitle")
		}
		
		TestHelper.clickButton(townEtymology_more_object);
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='edit-town-back-button']"))
	}else {
		TestHelper.thrownException("etimology Title Action is wrong")
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	TestObject townInThisMunicipality_title_object = TestHelper.getObjectById("//div[@id='town_relatedTowns_title_id']")
	TestObject townInThisMunicipality_subTitle_object = TestHelper.getObjectById("//div[@id='town_relatedTowns_subTitle_id']")
	TestObject townInThisMunicipality_icon_object = TestHelper.getObjectById("//div[@id='town_relatedTowns_title_id_icon']")
	if(TestHelper.isElementPresent(townInThisMunicipality_title_object) && TestHelper.isElementPresent(townInThisMunicipality_subTitle_object)) {
		String townInThisMunicipality_title_objectTitle = WebUI.getText(townInThisMunicipality_title_object);
		String townInThisMunicipality_title_objectSubTitle = WebUI.getText(townInThisMunicipality_subTitle_object);
		String townInThisMunicipalityIcon = WebUI.getText(townInThisMunicipality_icon_object);
		
		if(!townInThisMunicipality_title_objectTitle.equals("Towns in this municipality") || !townInThisMunicipality_title_objectSubTitle.equals('Under this municipality')  || !townInThisMunicipalityIcon.equals("assets/icons/diasporaIcon/related_town.svg")) {
			TestHelper.thrownException("Invalid etymology title and subtitle")
		}
	}else {
		TestHelper.thrownException("town In This Municipality title_object Title Action is wrong")
	}
}

