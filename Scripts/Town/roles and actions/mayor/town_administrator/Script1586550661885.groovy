import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement

String email = "fullmayor@gmail.com"
String password = "Test@123"
String originCity = "Aaba"
TestObject cityOrigineDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + originCity) + '\']')

//Start testing
AuthHelpers.townLogin(email, password, originCity)
checkTitleOfSection()
addNewAdmin()
TestAdministratorSection()
WebUI.closeBrowser()

def checkTitleOfSection() {
	TestObject administrators_title_object = TestHelper.getObjectById("//div[@id='town_administrator_title_id']")
	TestObject administrators_subTitle_object = TestHelper.getObjectById("//div[@id='town_administrator_subTitle_id']")
	TestObject administrators_more_object = TestHelper.getObjectById("//div[@id='edit_administrator']")
	TestObject administrator_icon_object = TestHelper.getObjectById("//div[@id='town_administrator_title_id_icon']")
	if(TestHelper.isElementPresent(administrators_title_object) && TestHelper.isElementPresent(administrators_subTitle_object) && TestHelper.isElementPresent(administrators_more_object)) {
		String administratorTitle = WebUI.getText(administrators_title_object);
		String administratorSubTitle = WebUI.getText(administrators_subTitle_object);
		String administratorMoreAction = WebUI.getText(administrators_more_object);
		String administratorIcon = WebUI.getText(administrator_icon_object);
		
		if(!administratorTitle.equals("Administrators") || !administratorSubTitle.equals('Managing this page') || !administratorMoreAction.equals("Edit") || !administratorIcon.equals("assets/icons/diasporaIcon/administrators.svg")) {
			TestHelper.thrownException("Invalid administrator title and subtitle")
		}

		TestHelper.clickButton(administrators_more_object);
		TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='add_admin_action']"))
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='add_admin_back_button']"));
	}else {
		TestHelper.thrownException("administrator Title Action is wrong")
	}
}




def addNewAdmin() {
	TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'maincard_Manage\']'))
	TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'Page Administrators_manage_entity_action\']'))
	TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'add_admin_action\']'))
	WebElement newAdminNameDiv = TestHelper.getItem('town-assign-admins' , 0)
	String newAdminName = newAdminNameDiv.text
	TestObject manageAdminByNameDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'manage_' + newAdminName) +
			'\']')
	TestObject inviteByNameDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'invite_' + newAdminName) +
			'\']')
	TestHelper.clickButton(TestHelper.getObjectById('//ion-segment-button[@id=\'invite_tab_id\']'))
	TestHelper.setInputValue(TestHelper.getObjectById('//input[@id=\'invite_first_name\']'), 'Test First Name')
	TestHelper.setInputValue(TestHelper.getObjectById('//input[@id=\'invite_last_name\']'), 'Test last Name')
	TestHelper.setInputValue(TestHelper.getObjectById('//input[@id=\'invite_email\']'), 'Test Email@gmail.com')
	TestHelper.clickButton(TestHelper.getObjectById('//ion-segment-button[@id=\'assign_member_tab_id\']'))
	TestHelper.setInputValue(TestHelper.getObjectById('//input[@id=\'search_add_town_administrator\']'), newAdminName)
	TestHelper.clickButton(inviteByNameDiv)
	TestHelper.clickButton(TestHelper.getObjectById('//a[@id=\'Assign As Administrator_user_action\']'))
	TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'back_add_administrator_back_button\']'))
	TestHelper.setInputValue(TestHelper.getObjectById('//input[@id=\'search_manage_town\']'), newAdminName)
	if (TestHelper.isElementPresent(manageAdminByNameDiv)) {
		println("admin was added")
	}else{
		TestHelper.thrownException("admin not added ")
	}
	TestHelper.clickButton(manageAdminByNameDiv)
	TestHelper.clickButton(TestHelper.getObjectById('//a[@id=\'Remove Administrator_user_action\']'))
	TestObject confirmDeleteButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, './/span[text()=\'Delete\']')
	TestHelper.clickButton(confirmDeleteButton)
	WebUI.delay(5)
	if(TestHelper.isElementPresent(manageAdminByNameDiv)) {
		TestHelper.thrownException("admin was not removed")
	}else{
		println("admin was not removed")
	}
	TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'add_admin_back_button\']'))
	TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'manage-Municipality-back-button\']'))
	
}

def TestAdministratorSection() {
	WebUI.delay(3)
		WebUI.scrollToElement(TestHelper.getObjectById("//div[@id='town_administrator_title_id']"), 3)
	if(TestHelper.getObjectById("//div[@id='town_administrator_title_id']")) {
		WebElement itemTitleDiv = TestHelper.getItem('title_administrator_list' , 0)
		String itemTitleName = itemTitleDiv.text
		println(itemTitleName)
		TestObject followitemAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'right_action_id_municipality_' + itemTitleName) + '\']')
		TestHelper.clickButton(followitemAction)
		WebUI.delay(2)
		WebUI.click(TestHelper.getObjectById("//div[@id='town_page_back_button']"))
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Manage_User_entity_action']"))
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Following and Followed User_manage_entity_action']"))
		TestHelper.clickButton(TestHelper.getObjectById("//ion-segment-button[@id='followings_tab_id']"))
		TestObject SearchFollowingInput = TestHelper.getObjectById("//input[@id='manage_following_search']")
		TestHelper.setInputValue(SearchFollowingInput, itemTitleName)
		TestObject followPeopleDiv= new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + itemTitleName + '_profile') + '\']')
		TestHelper.isElementPresent(followPeopleDiv)
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_followers_back_id']"))
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-User-back-button']"))
		TestObject goToTownButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//span[@id='country_lb_2_tag_label']")
		WebUI.click(goToTownButton)
		WebUI.scrollToElement(TestHelper.getObjectById("//div[@id='town_administrator_title_id']"), 3)
		TestHelper.clickButton(followitemAction)
	}
	else{
		TestHelper.thrownException("administratotr section not found")
	}

}