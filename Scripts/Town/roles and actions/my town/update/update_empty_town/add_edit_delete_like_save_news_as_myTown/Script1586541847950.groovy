import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement

String firstname = "ali" +  + new Date().getTime();
String lastname = "mahdi" +  + new Date().getTime();
String originCity = 'Aouainat'
String residenceCity = 'Aouainat'
String email = "mahdi@" +  + new Date().getTime() + '.com';

TestObject townPageBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='town_page_back_button']")
TestObject homeTabButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='home_tab_id']")
long now = new Date().getTime();
String postTitle = 'New post title add now_' + now;
String postDescription = 'New post description add now_' + now;
TestObject emptyStateCompanyUpdates = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-town-updates-empty-state']")
boolean emptyStateCompanyUpdatesExist = TestHelper.isElementPresent(emptyStateCompanyUpdates)


//Start Testing

AuthHelpers.townRegistration(originCity, residenceCity, firstname, lastname, email)
WebElement townNameObject = TestHelper.getItem('town-name-title', 0)
String townName = townNameObject.text;
if (emptyStateCompanyUpdatesExist) {
	println("Hasn't udpates");
	 WebUI.click(emptyStateCompanyUpdates)
	
	TestObject create_news_action_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Create Post_user_action']")
	WebUI.click(create_news_action_button)
	
	UserActionsHelper.addPost(postTitle, postDescription)
	WebUI.scrollToElement(TestHelper.getObjectById("//div[@id='town_updates_title_id']"), 0)
	TestHelper.verifyCardExist(originCity, postTitle, 'town', true)
	
	// Verify Card Features
	verifyCardFeature(postTitle, postDescription, townName);

} else {
	println("Has udpates");

	TestHelper.clickButton(townPageBackButton)
	TestHelper.clickButton(homeTabButton)
	TestObject home_tab = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='home_tab_id']")
	WebUI.click(home_tab)
	
	TestObject user_action_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//img[@id='user_action_button']")
	WebUI.click(user_action_button)
	
	TestObject create_news_action_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Create Post_user_action']")
	WebUI.click(create_news_action_button)

	TestObject changeTargetAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='change-target-id']")
	WebUI.click(changeTargetAction)
 
	TestObject selectedTarget = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" +originCity+ "_target']")
	WebUI.click(selectedTarget)
 
	UserActionsHelper.addTownPost(postTitle, postDescription)
	WebUI.delay(5)
	TestObject profile_tab = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='profile_tab_id']")
	WebUI.waitForElementPresent(profile_tab, 5)
	
	TestHelper.clickButton(profile_tab)
	TestObject originCityDiv = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" +originCity+ "']")
	TestHelper.clickButton(originCityDiv)
	TestHelper.verifyCardUpdateTownExist(originCity, postTitle);
	TestObject townUpdates = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='town_updates']")
	
   WebUI.waitForElementPresent(townUpdates, 3);
   WebUI.scrollToElement(TestHelper.getObjectById("//div[@id='town_updates_title_id']"), 0)
	TestHelper.verifyCardExist(originCity, postTitle, 'town', true)
	
	// Verify Card Features
	verifyCardFeature(postTitle, postDescription, townName);

}

 

def verifyCardFeature(String postTitle, String postDescription, String townName) {
	TestObject manageCardObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='diplomatic_manage_updates']")
	boolean manageCardObjectExist = TestHelper.isElementPresent(manageCardObject);
	if(!manageCardObjectExist) {
		TestHelper.thrownException("Manage card button not exist");
	}
	
	TestHelper.clickButton(manageCardObject);
	String cartId = postTitle + '_descriptionId';
	TestHelper.clickButton(TestHelper.getObjectById("//span[@id='" + cartId + "']"));
	
	String newPostTitle =  "new " + postTitle;
	String newPostDescription =  "new " + postDescription;
	
	TestHelper.checkPostCardFeatures(postTitle, postDescription, newPostTitle, newPostDescription, townName );
	
	WebUI.delay(3);
	String newCartId = newPostTitle + '_descriptionId';
	TestObject deleteCardObject = new TestObject().addProperty("xpath",  ConditionType.EQUALS, "//span[@id='" + newCartId + "']")
	boolean deleteCardObjectExist = TestHelper.isElementPresent(deleteCardObject);
	
	if(deleteCardObjectExist) {
		TestHelper.thrownException("Card not deleted still exist in the card list");
	} else {
		checkCardRemovedFromMainPage(postDescription);
	}
}
def checkCardRemovedFromMainPage(String cardDescriptionId) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back_button_cardListing']"))
	TestObject cardDescriptionObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+ cardDescriptionId  + "_id']")
	
	boolean cardDescriptionObjectExist = TestHelper.isElementPresent(cardDescriptionObject)

	if(cardDescriptionObjectExist) {
		TestHelper.thrownException("Card not deleted from main page")
	} else {
		println("Test Complete");
	}

}
checkTitleOfSection()
WebUI.closeBrowser()

def checkTitleOfSection() {
	TestObject updates_title_object = TestHelper.getObjectById("//div[@id='town_updates_title_id']")
	TestObject updates_subTitle_object = TestHelper.getObjectById("//div[@id='town_updates_subTitle_id']")
	TestObject updates_more_object = TestHelper.getObjectById("//div[@id='diplomatic_manage_updates']")
	
	if(TestHelper.isElementPresent(updates_title_object) && TestHelper.isElementPresent(updates_subTitle_object) && TestHelper.isElementPresent(updates_more_object)) {
		String updatesTitle = WebUI.getText(updates_title_object);
		String updatesSubTitle = WebUI.getText(updates_subTitle_object);
		String updatesMoreAction = WebUI.getText(updates_more_object);
		
		if(!updatesTitle.equals("Updates") || !updatesSubTitle.equals('Posts & discussions') || !updatesMoreAction.equals("More")) {
			TestHelper.thrownException("Invalid updates title and subtitle")
		}

		TestHelper.clickButton(updates_more_object);
		WebUI.delay(3)
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back_button_cardListing']"));
	}else {
		TestHelper.thrownException("update Title Action is wrong")
	}
}