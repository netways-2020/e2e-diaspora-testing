import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

String firstname = "ali" +  + new Date().getTime();
String lastname = "mahdi" +  + new Date().getTime();
String originCity = 'Aaba'
String residenceCity = 'Aaba'

String email = "mahdi@" +  + new Date().getTime() + '.com';

AuthHelpers.townRegistration(originCity, residenceCity, firstname, lastname, email)
applyToBeAnAmbassador(originCity)
WebUI.closeBrowser()
def applyToBeAnAmbassador(originCity) {
	TestHelper.isElementPresent(TestHelper.getObjectById('//div[@id=\'maincard_Invite\']'))
	TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'Apply Now_action\']'))
	TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'manage_town_apply_button\']'))
	TestHelper.setInputValue(TestHelper.getObjectById('//input[@id=\'apply-ambassador-number\']'), '70123456')
	TestHelper.setInputValue(TestHelper.getObjectById('//textarea[@id=\'apply-ambassador-description\']'), 'test description')
	TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'submit-ambassador\']'))
	if(TestHelper.isElementPresent(TestHelper.getObjectById('//div[@id=\'maincard_Invite\']')))
		 {
		println('submit be an ambassador request done')
	}else {
		TestHelper.thrownException("submit be an ambassador request done failed")
	}
	
}