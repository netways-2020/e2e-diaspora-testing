import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement

String firstname = "ali" +  + new Date().getTime();
String lastname = "mahdi" +  + new Date().getTime();
String originCity = 'Froun'
String residenceCity = 'Froun'
String email = "mahdi@" +  + new Date().getTime() + '.com';
TestObject editAboutSection = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='edit_about_town']")

//Ambassadors
TestObject titleSectionAmbassador = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'town_ambassador_title_id\']')
String parentClassNameAmbassador = "town-parent-municipality-class-Municipality"
String titleListClassAmbassador= "title_ambassador_list"
String erroAmbassadorMessage = "no ambassador in this town"
String successAmbassadorMessage = "Test ambassador section success"

//Start testing
AuthHelpers.townRegistration(originCity, residenceCity, firstname, lastname, email)
checkTitleOfSection()
checkAmbassador(titleSectionAmbassador, titleListClassAmbassador, erroAmbassadorMessage)
WebUI.closeBrowser()

def checkAmbassador(titleSection, titleListClass, errorMessage) {
	if(TestHelper.isElementPresent(titleSection)) {
		WebUI.delay(3)
		WebUI.scrollToElement(titleSection, 3)
		TestHelper.isElementPresent(titleSection);
		WebElement itemTitleDiv = TestHelper.getItem(titleListClass , 1)
		String itemTitleName = itemTitleDiv.text
		println(itemTitleName)
		TestObject followitemAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'sendMessage_' + itemTitleName) + '\']')
		TestHelper.clickButton(followitemAction)
		WebUI.delay(2)
		TestObject titleMessageName= new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//p[@id=\'' + itemTitleName + '_sender_id') + '\']')
		TestHelper.isElementPresent(titleMessageName)
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back_button_conversation']"))
	}else{
		TestHelper.thrownException(errorMessage)
	}
}

def checkTitleOfSection() {
	TestObject ambassador_title_object = TestHelper.getObjectById("//div[@id='town_ambassador_title_id']")
	TestObject ambassador_subTitle_object = TestHelper.getObjectById("//div[@id='town_ambassador_subTitle_id']")
	TestObject ambassador_icon_object = TestHelper.getObjectById("//div[@id='town_ambassador_title_id_icon']")
	if(TestHelper.isElementPresent(ambassador_title_object) && TestHelper.isElementPresent(ambassador_subTitle_object)) {
		String ambassadorTitle = WebUI.getText(ambassador_title_object);
		String ambassadorSubititle = WebUI.getText(ambassador_subTitle_object);
		String ambassadorIcon = WebUI.getText(ambassador_icon_object);
		if(!ambassadorTitle.equals("Town Ambassadors") || !ambassadorSubititle.equals("Representing the town & its people") || !ambassadorIcon.equals("assets/icons/diasporaIcon/town_ambassadors_member.svg")) {
			TestHelper.thrownException("Invalid ambassador title and subtitle")
		}
	}else {
		TestHelper.thrownException("ambassador Title Action is wrong")
	}
}















