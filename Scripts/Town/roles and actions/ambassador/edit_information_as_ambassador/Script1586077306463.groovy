import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable


String email = "fullambassador@gmail.com"
String password = "Test@123"
String originCity = "Froun"
TestObject checkManageDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'maincard_Manage\']')
TestObject editAboutSectionDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'town_about_more_id\']')
TestObject cityOrigineDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + originCity) + '\']')
TestObject editEtymologyDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'edit_etymology\']')
TestObject editAboutSection = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='edit_about_town']")

//Start Testing
AuthHelpers.townLogin(email, password, originCity)
checkContactAction()
checkTitleOfSection()
WebUI.closeBrowser()

boolean checkManageDivAction = TestHelper.isElementPresent(checkManageDiv)
boolean editAboutSectionDivAction = TestHelper.isElementPresent(editAboutSectionDiv)
boolean editEtymologyDivAction = TestHelper.isElementPresent(editEtymologyDiv)
if(checkManageDivAction || editAboutSectionDivAction || editEtymologyDivAction) {
	TestHelper.thrownException("ambassador has wrong access")
}
else {
	println("ambassador has the right access")
}

def checkContactAction() {
	TestObject callButtonDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//button[@id=\'callButton\']')
	TestObject emailButtonDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//button[@id=\'emailButton\']')
	
	boolean callButtonAction = TestHelper.isElementPresent(callButtonDiv)
	boolean emailButtonAction = TestHelper.isElementPresent(emailButtonDiv)
	
	if(callButtonAction || emailButtonAction) {
		TestHelper.thrownException("ambassador has wrong access")
	}
	else {
		println("ambassador has the right access")
	}
}

def checkTitleOfSection() {
	TestObject town_about_title_object = TestHelper.getObjectById("//div[@id='town_about_title_id']")
	TestObject town_about_subTitle_object = TestHelper.getObjectById("//div[@id='town_about_subTitle_id']")
	if(TestHelper.isElementPresent(town_about_title_object) && TestHelper.isElementPresent(town_about_subTitle_object)) {
		String aboutTitle = WebUI.getText(town_about_title_object);
		String aboutSubTitle = WebUI.getText(town_about_subTitle_object);
		if(!aboutTitle.equals("About") || !aboutSubTitle.equals("Municipality & town info")) {
			TestHelper.thrownException("Invalid about title and subtitle")
		}
	}else {
		TestHelper.thrownException("About Title Action is wrong")
	}
	
	TestObject etymologie_title_object = TestHelper.getObjectById("//div[@id='town_etymology_title_id']")
	TestObject etymologie_subTitle_object = TestHelper.getObjectById("//div[@id='town_etymology_subTitle_id']")
	if(TestHelper.isElementPresent(etymologie_title_object) && TestHelper.isElementPresent(etymologie_subTitle_object)) {
		String etymologyTitle = WebUI.getText(etymologie_title_object);
		String etimologySubTitle = WebUI.getText(etymologie_subTitle_object);
		
		if(!etymologyTitle.equals("Etymology") || !etimologySubTitle.equals('Town name origin') ) {
			TestHelper.thrownException("Invalid etymology title and subtitle")
		}
	}else {
		TestHelper.thrownException("etimology Title Action is wrong")
	}
	
}
