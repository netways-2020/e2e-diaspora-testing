import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement

String email = "aabaambassador@gmail.com"
String password = "Test@123"
String originCity = "Aaba"
TestObject cityOrigineDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + originCity) + '\']')

//municipality and mukhtar section Section
TestObject titleSectionMunicipality = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'town_mukthars_title_id\']')
String titleListClassMunicipality = "title_municipality_list"
String erroMunicipalityMessage = "no mukhtar or municipality"
String successMunicipalityMessage = "test mukhtar or municipality section success"


//Start Testing
AuthHelpers.townLogin(email, password, originCity)
checkTitleOfSection()
checkSectionsList(titleSectionMunicipality, titleListClassMunicipality, erroMunicipalityMessage,successMunicipalityMessage, originCity)
WebUI.closeBrowser()



def checkTitleOfSection() {
	TestObject municipality_title_object = TestHelper.getObjectById("//div[@id='town_mukthars_title_id']")
	TestObject municipality_subTitle_object = TestHelper.getObjectById("//div[@id='town_mukthars_subTitle_id']")
	TestObject municipality_icon_object = TestHelper.getObjectById("//div[@id='town_mukthars_title_id_icon']")
	if(TestHelper.isElementPresent(municipality_title_object) && TestHelper.isElementPresent(municipality_subTitle_object)) {
		String municipalityTitle = WebUI.getText(municipality_title_object);
		String municipalitySubitle = WebUI.getText(municipality_subTitle_object);
		String municipalityIcon = WebUI.getText(municipality_icon_object);
		
		if(!municipalityTitle.equals("Municipality & Mukhtars") || !municipalitySubitle.equals("Mayor, mukhtars, elected members")|| !municipalityIcon.equals("assets/icons/diasporaIcon/municipality.svg")) {
			TestHelper.thrownException("Invalid municipality title and subtitle")
		}
	}else {
		TestHelper.thrownException(" Action municipality and mukhtar wrong")
	}
}



def checkSectionsList(titleSection, titleListClass, errorMessage, successMessage,  originCity) {
	if(TestHelper.isElementPresent(titleSection)) {
	WebUI.delay(3)
	WebUI.scrollToElement(titleSection, 3)
	TestHelper.isElementPresent(titleSection);
	WebElement itemTitleDiv = TestHelper.getItem(titleListClass , 1)
	String itemTitleName = itemTitleDiv.text
	println(itemTitleName)
	TestObject followitemAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'right_action_id_municipality_' + itemTitleName) + '\']')
	TestHelper.clickButton(followitemAction)
	WebUI.delay(2)
	WebUI.click(TestHelper.getObjectById("//div[@id='town_page_back_button']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Manage_User_entity_action']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Following and Followed User_manage_entity_action']"))
	TestHelper.clickButton(TestHelper.getObjectById("//ion-segment-button[@id='followings_tab_id']"))
	TestObject SearchFollowingInput = TestHelper.getObjectById("//input[@id='manage_following_search']")
	TestHelper.setInputValue(SearchFollowingInput, itemTitleName)
	TestObject followPeopleDiv= new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + itemTitleName + '_profile') + '\']')
	TestHelper.isElementPresent(followPeopleDiv)
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_followers_back_id']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-User-back-button']"))
	TestObject goToTownButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + originCity) + '\']')
	TestHelper.clickButton(goToTownButton)
	WebUI.scrollToElement(titleSection, 3)
	TestHelper.clickButton(followitemAction)
	println(successMessage)
	}else{
		TestHelper.thrownException(errorMessage)
	}
}
