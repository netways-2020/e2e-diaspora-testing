import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import static org.junit.Assert.*

import org.junit.Test
import org.openqa.selenium.WebElement as WebElement
String email = "fullambassador@gmail.com"
String password = "Test@123"
String originCity = "Froun"
TestObject cityOrigineDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + originCity) + '\']')
TestObject editCardobject = new TestObject().addProperty('xpath', ConditionType.EQUALS, ("//a[@id='Edit Comment_user_action']"))
TestObject deleteCardobject = new TestObject().addProperty('xpath', ConditionType.EQUALS, ("//a[@id='Delete Comment_user_action']"))
String randomComment =  "randomcomment" +  new Date().getTime() ;
TestObject userCommentObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, (('//div[@id=\'' + randomComment) +
	'_card_comment_action') + '\']')

//Start Testing
AuthHelpers.townLogin(email, password, originCity)
CheckCommentRole(editCardobject, deleteCardobject,randomComment,  userCommentObject)
WebUI.closeBrowser()

def  CheckCommentRole(editCardobject, deleteCardobject,randomComment,  userCommentObject) {
	TestObject deleteCommentObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, ("//span[text()='Delete']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='diplomatic_manage_updates']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='test comment _descriptionId']"))
	TestHelper.setInputValue(TestHelper.getObjectById("//textarea[@id='commentInput']"), randomComment)
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='send_message_action']"))
	TestHelper.clickButton(userCommentObject)
	TestHelper.isElementPresent(TestHelper.getObjectById("//a[@id='Edit Comment_user_action']"))
	TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Delete Comment_user_action']"))
	TestHelper.clickButton(deleteCommentObject)
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='user comment_card_comment_action']"))
	
	boolean editCardAction = TestHelper.isElementPresent(editCardobject)
	boolean deleteCardAction = TestHelper.isElementPresent(deleteCardobject)
	
	if(editCardAction || deleteCardAction) {
		TestHelper.thrownException("ambassador can't delete comment")
	}
}

