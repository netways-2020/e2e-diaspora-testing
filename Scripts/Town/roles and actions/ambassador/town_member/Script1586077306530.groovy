import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement

String email = "fullambassador@gmail.com"
String password = "Test@123"
String originCity = "Froun"
TestObject cityOrigineDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + originCity) + '\']')


//Member
TestObject titleSectionMember = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'town_members_title_id\']')
String titleListClassMember = "title_member_list"
String erroMunicipalityMember = "no member in this municipality"
String successMunicipalityMember = "Test member section success"


//Start testing
AuthHelpers.townLogin(email, password, originCity)
checkTitleOfSection()
checkSectionsList(titleSectionMember, titleListClassMember, erroMunicipalityMember, successMunicipalityMember, originCity)
WebUI.closeBrowser()

def checkTitleOfSection() {
	TestObject members_title_object = TestHelper.getObjectById("//div[@id='town_members_title_id']")
	TestObject members_subTitle_object = TestHelper.getObjectById("//div[@id='town_members_subTitle_id']")
	TestObject members_more_object = TestHelper.getObjectById("//div[@id='view_more_member']")
	TestObject member_icon_object = TestHelper.getObjectById("//div[@id='town_members_title_id_icon']")
	
	if(TestHelper.isElementPresent(members_title_object) && TestHelper.isElementPresent(members_subTitle_object) && TestHelper.isElementPresent(members_more_object)) {
		String memberTitle = WebUI.getText(members_title_object);
		String memberSubTitle = WebUI.getText(members_subTitle_object);
		String memberMoreAction = WebUI.getText(members_more_object);
		String memberIcon = WebUI.getText(member_icon_object);
		if(!memberTitle.equals("Members") || !memberSubTitle.equals('People from here') || !memberMoreAction.equals("More") || !memberIcon.equals("assets/icons/diasporaIcon/People_Active.svg")) {
			TestHelper.thrownException("Invalid members title and subtitle")
		}

		TestHelper.clickButton(members_more_object);
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back_town_member_button']"));
	}else {
		TestHelper.thrownException("member Title Action is wrong")
	}
}


def checkSectionsList(titleSection, titleListClass, errorMessage, successMessage,  originCity) {
	if(TestHelper.isElementPresent(titleSection)) {
	WebUI.delay(3)
	WebUI.scrollToElement(titleSection, 3)
	TestHelper.isElementPresent(titleSection);
	WebElement itemTitleDiv = TestHelper.getItem(titleListClass , 1)
	String itemTitleName = itemTitleDiv.text
	println(itemTitleName)
	TestObject followitemAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'right_action_id_municipality_' + itemTitleName) + '\']')
//	TestObject followitemActionIcon = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//img[@id=\'right_action_id_municipality_icon_' + itemTitleName) + '\']')
//	String iconSrc = WebUI.getAttribute(followitemActionIcon, 'src')
	TestHelper.clickButton(followitemAction)
	WebUI.delay(2)
//	if(iconSrc.equals('assets/icons/diasporaIcon/Follow.svg')) {
//		TestHelper.clickButton(followitemAction)
//	}
	WebUI.click(TestHelper.getObjectById("//div[@id='town_page_back_button']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Manage_User_entity_action']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Following and Followed User_manage_entity_action']"))
	TestHelper.clickButton(TestHelper.getObjectById("//ion-segment-button[@id='followings_tab_id']"))
	TestObject SearchFollowingInput = TestHelper.getObjectById("//input[@id='manage_following_search']")
	TestHelper.setInputValue(SearchFollowingInput, itemTitleName)
	TestObject followPeopleDiv= new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + itemTitleName + '_profile') + '\']')
	TestHelper.isElementPresent(followPeopleDiv)
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_followers_back_id']"))
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-User-back-button']"))
	TestObject goToTownButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + originCity) + '\']')
	WebUI.click(goToTownButton)
	WebUI.scrollToElement(titleSection, 3)
	TestHelper.clickButton(followitemAction)
	println(successMessage)
	}else{
		TestHelper.thrownException(errorMessage)
	}
}







