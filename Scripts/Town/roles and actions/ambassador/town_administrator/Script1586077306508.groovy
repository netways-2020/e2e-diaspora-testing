import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement

String email = "fullambassador@gmail.com"
String password = "Test@123"
String originCity = "Froun"
TestObject cityOrigineDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + originCity) + '\']')
TestObject addNewAdministratorObject = new TestObject().addProperty('xpath', ConditionType.EQUALS, ("//img[@id='add_admin_action_icon']"))

//Start testing
AuthHelpers.townLogin(email, password, originCity)
checkTitleOfSection()
checkAddAdministratorActionAccess(addNewAdministratorObject)
TestAdministratorSection()
WebUI.closeBrowser()

def checkTitleOfSection() {
		TestObject administrators_title_object = TestHelper.getObjectById("//div[@id='town_administrator_title_id']")
	TestObject administrators_subTitle_object = TestHelper.getObjectById("//div[@id='town_administrator_subTitle_id']")
	TestObject administrators_more_object = TestHelper.getObjectById("//div[@id='edit_administrator']")
	
	if(TestHelper.isElementPresent(administrators_title_object) && TestHelper.isElementPresent(administrators_subTitle_object) && TestHelper.isElementPresent(administrators_more_object)) {
		String administratorTitle = WebUI.getText(administrators_title_object);
		String administratorSubTitle = WebUI.getText(administrators_subTitle_object);
		String administratorMoreAction = WebUI.getText(administrators_more_object);
		
		if(!administratorTitle.equals("Administrators") || !administratorSubTitle.equals('Managing this page') || !administratorMoreAction.equals("More")) {
			TestHelper.thrownException("Invalid administrator title and subtitle")
		}

		TestHelper.clickButton(administrators_more_object);
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='add_admin_back_button']"));
	}else {
		TestHelper.thrownException("administrator Title Action is wrong")
	}
}

def checkAddAdministratorActionAccess(addNewAdministratorObject) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='edit_administrator']"))
	boolean addNewAdministrator = TestHelper.isElementPresent(addNewAdministratorObject)
	if(addNewAdministrator) {
		TestHelper.thrownException("ambassador cant have access To edit section admin")
	}
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='add_admin_back_button']"))
}

def TestAdministratorSection() {
	WebUI.delay(3)
		WebUI.scrollToElement(TestHelper.getObjectById("//div[@id='town_administrator_title_id']"), 3)
	if(TestHelper.getObjectById("//div[@id='town_administrator_title_id']")) {
		WebElement itemTitleDiv = TestHelper.getItem('title_administrator_list' , 0)
		String itemTitleName = itemTitleDiv.text
		println(itemTitleName)
		TestObject followitemAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'right_action_id_municipality_' + itemTitleName) + '\']')
		TestHelper.clickButton(followitemAction)
		WebUI.delay(2)
		WebUI.click(TestHelper.getObjectById("//div[@id='town_page_back_button']"))
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Manage_User_entity_action']"))
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Following and Followed User_manage_entity_action']"))
		TestHelper.clickButton(TestHelper.getObjectById("//ion-segment-button[@id='followings_tab_id']"))
		TestObject SearchFollowingInput = TestHelper.getObjectById("//input[@id='manage_following_search']")
		TestHelper.setInputValue(SearchFollowingInput, itemTitleName)
		TestObject followPeopleDiv= new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + itemTitleName + '_profile') + '\']')
		TestHelper.isElementPresent(followPeopleDiv)
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_followers_back_id']"))
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-User-back-button']"))
		TestObject goToTownButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//span[@id='country_lb_2_tag_label']")
		WebUI.click(goToTownButton)
		WebUI.scrollToElement(TestHelper.getObjectById("//div[@id='town_administrator_title_id']"), 3)
		TestHelper.clickButton(followitemAction)
	}
	else{
		TestHelper.thrownException("administratotr section not found")
	}

}