import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import org.junit.After

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable


String firstname = "ali" +  + new Date().getTime();
String lastname = "mahdi" +  + new Date().getTime();
String originCity = 'Ankoun'
String residenceCity = 'Ankoun'
String email = "mahdi@" +  + new Date().getTime() + '.com';

TestObject checkManageDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'maincard_Manage\']')


TestObject editAboutSectionDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'town_about_more_id\']')
TestObject cityOrigineDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, ('//div[@id=\'' + originCity) + '\']')
TestObject editEtymologyDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'edit_etymology\']')
TestObject callButtonDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//button[@id=\'callButton\']')
TestObject emailButtonDiv = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//button[@id=\'emailButton\']')

//Start Testing
AuthHelpers.townRegistration(originCity, residenceCity, firstname, lastname, email)
WebUI.navigateToUrl('http://localhost:8100/app/town/286193b7-0304-e711-80e1-00155d003d3b')

boolean checkManageDivAction = TestHelper.isElementPresent(checkManageDiv)
boolean editAboutSectionDivAction = TestHelper.isElementPresent(editAboutSectionDiv)
boolean editEtymologyDivAction = TestHelper.isElementPresent(editEtymologyDiv)
boolean callButtonAction = TestHelper.isElementPresent(callButtonDiv)
boolean emailButtonAction = TestHelper.isElementPresent(emailButtonDiv)
if(checkManageDivAction || editAboutSectionDivAction || editEtymologyDivAction || !callButtonAction || !emailButtonAction) {
	TestHelper.thrownException("visitor has wrong access")
}
else {
	println("visitor has the right access")
}


def checkTitleOfSection() {
	TestObject town_about_title_object = TestHelper.getObjectById("//div[@id='town_about_title_id']")
	TestObject town_about_subTitle_object = TestHelper.getObjectById("//div[@id='town_about_subTitle_id']")
	if(TestHelper.isElementPresent(town_about_title_object) && TestHelper.isElementPresent(town_about_subTitle_object)) {
		String aboutTitle = WebUI.getText(town_about_title_object);
		String aboutSubTitle = WebUI.getText(town_about_subTitle_object);
		if(!aboutTitle.equals("About") || !aboutSubTitle.equals("Municipality & town info")) {
			TestHelper.thrownException("Invalid about title and subtitle")
		}
	}else {
		TestHelper.thrownException("About Title Action is wrong")
	}
}

