import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

String firstname = "ali" +  + new Date().getTime();
String lastname = "mahdi" +  + new Date().getTime();
String originCity = 'Aaba'
String residenceCity = 'Aaba'
String email = "mahdi@" +  + new Date().getTime() + '.com';

AuthHelpers.townRegistration(originCity, residenceCity, firstname, lastname, email)
checkAction(firstname,lastname)

def checkAction(firstname,lastname) {
	TestObject townFollowerBackButton = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'town_follower_back_button\']')
	TestObject followerTownName = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + firstname + ' ' + lastname +"_town']")
	WebUI.navigateToUrl('http://localhost:8100/app/town/906e716a-d324-e711-80e2-00155d003d3b')
	TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'maincard_Follow\']'))
	TestHelper.clickButton(TestHelper.getObjectById('//span[@id=\'follower_number_tag_label\']'))
	TestHelper.setInputValue(TestHelper.getObjectById('//input[@id=\'search-input-follower\']'), firstname)
	if(TestHelper.isElementPresent(followerTownName)) {
		println("follow town is done")
	}else {
		TestHelper.thrownException("follow town failed")
	}
	TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'town_follower_back_button\']'))
	TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'maincard_Following\']'))
	TestHelper.clickButton(TestHelper.getObjectById('//span[@id=\'follower_number_tag_label\']'))
	TestHelper.setInputValue(TestHelper.getObjectById('//input[@id=\'search-input-follower\']'), firstname)
	if(!TestHelper.isElementPresent(followerTownName)) {
		println("unfollow town is done")
	}else {
		TestHelper.thrownException("follow town failed")
	}
	TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'town_follower_back_button\']'))
	TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'maincard_Follow\']'))
}