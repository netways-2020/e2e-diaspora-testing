import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import static org.junit.Assert.*

import org.junit.Test
import org.openqa.selenium.WebElement as WebElement

String firstname = "ali" +  + new Date().getTime();
String lastname = "mahdi" +  + new Date().getTime();
String originCity = 'Dfoun'
String residenceCity = 'Dfoun'
String email = "mahdi@" +  + new Date().getTime() + '.com';

TestObject imageActionObgect = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='galler_image_id']")
//Start Testing
AuthHelpers.townRegistration(originCity, residenceCity, firstname, lastname, email)
WebUI.navigateToUrl('http://localhost:8100/app/town/286193b7-0304-e711-80e1-00155d003d3b')
checkdeleteImageAccess(imageActionObgect)
WebUI.closeBrowser()

def checkdeleteImageAccess(imageActionObgect) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='town_manage_media']"))
	WebElement element = TestHelper.getItem('gallery-container', 0);
	element.click();
	boolean imageAction = TestHelper.isElementPresent(imageActionObgect)
	if(imageAction) {
		TestHelper.thrownException("this user should not has access to delete this image")
	}
}

