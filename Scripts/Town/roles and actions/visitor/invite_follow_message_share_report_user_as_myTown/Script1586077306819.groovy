import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

String firstname = "ali" +  + new Date().getTime();
String lastname = "mahdi" +  + new Date().getTime();
String originCity = 'Ankoun'
String residenceCity = 'Ankoun'
String email = "mahdi@" +  + new Date().getTime() + '.com';

AuthHelpers.townRegistration(originCity, residenceCity, firstname, lastname, email)
WebUI.navigateToUrl('http://localhost:8100/app/town/286193b7-0304-e711-80e1-00155d003d3b')
checkInvite()
checkfollow()
checkMassage()
checkShareAndReport()
WebUI.closeBrowser()

def checkInvite() {
	TestObject inviteButtonObject =  new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'maincard_Invite\']')
	boolean inviteButtonDiv = TestHelper.isElementPresent(inviteButtonObject)
	if(inviteButtonDiv) {
		   TestHelper.thrownException("visitor should not have invite action")
	}
	else {
		println("invite action not exist")
	}
}


def checkfollow() {
	 TestObject followButtonObject =  new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'maincard_Follow\']')
	 boolean followButtonDiv = TestHelper.isElementPresent(followButtonObject)
	 if(!followButtonDiv) {
			TestHelper.thrownException("visitor should  have follow action")
	 }
	 else {
		 println("follow action exist")
	 }
}


def checkMassage() {
	TestObject MessageButtonObject =  new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'maincard_Message\']')
	boolean messageButtonDiv = TestHelper.isElementPresent(MessageButtonObject)
	if(!messageButtonDiv) {
		   TestHelper.thrownException("visitor should  have send message action")
	}
	else {
		println("send message action  exist")
	}
}

def checkShareAndReport() {
	TestHelper.clickButton(TestHelper.getObjectById('//div[@id=\'town_menu_action_id\']'))
	TestHelper.isElementPresent(TestHelper.getObjectById('//a[@id=\'Share_user_action\']'))
	TestHelper.clickButton(TestHelper.getObjectById('//a[@id=\'Report A Problem_user_action\']'))
	TestHelper.setInputValue(TestHelper.getObjectById('//textarea[@id=\'report-description\']'), 'test test test')
	TestHelper.setInputValue(TestHelper.getObjectById('//textarea[@id=\'report-contact\']'), 'test contact')
	TestHelper.clickButton(TestHelper.getObjectById('//button[@id=\'report-send-action\']'))
	TestHelper.isElementPresent(TestHelper.getObjectById('//div[@id=\'town_page_back_button\']'))
}
