import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

String firstname = "ali" +  + new Date().getTime();
String lastname = "mahdi" +  + new Date().getTime();
String originCity = 'Aaba'
String residenceCity = 'Aaba'
String email = "mahdi@" +  + new Date().getTime() + '.com';

//Start Testing

AuthHelpers.townRegistration(originCity, residenceCity, firstname, lastname, email)
WebUI.navigateToUrl('http://localhost:8100/app/town/286193b7-0304-e711-80e1-00155d003d3b')
checkTitleOfSection()
WebUI.closeBrowser()

def checkTitleOfSection() {
	TestObject updates_title_object = TestHelper.getObjectById("//div[@id='town_updates_title_id']")
	TestObject updates_subTitle_object = TestHelper.getObjectById("//div[@id='town_updates_subTitle_id']")
	TestObject updates_more_object = TestHelper.getObjectById("//div[@id='diplomatic_manage_updates']")
	
	if(TestHelper.isElementPresent(updates_title_object) && TestHelper.isElementPresent(updates_subTitle_object) && TestHelper.isElementPresent(updates_more_object)) {
		String updatesTitle = WebUI.getText(updates_title_object);
		String updatesSubTitle = WebUI.getText(updates_subTitle_object);
		String updatesMoreAction = WebUI.getText(updates_more_object);
		
		if(!updatesTitle.equals("Updates") || !updatesSubTitle.equals('Posts & discussions') || !updatesMoreAction.equals("More")) {
			TestHelper.thrownException("Invalid updates title and subtitle")
		}

		TestHelper.clickButton(updates_more_object);
		WebUI.delay(3)
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back_button_cardListing']"));
	}else {
		TestHelper.thrownException("update Title Action is wrong")
	}
}