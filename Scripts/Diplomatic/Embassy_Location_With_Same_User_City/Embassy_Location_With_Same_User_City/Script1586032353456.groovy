import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

String roleName = 'Diaspora Member';

WebUI.callTestCase(findTestCase('Diplomatic/Embassy_Location_With_Same_User_City/Login_For_Belgium_Embassy'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.navigateToUrl(TestHelper.getEmbassyOfBelgium_EmbassyUrl())

TestHelper.clickButton(TestHelper.getObjectById("//div[@id='diplomaticPage_Join Diplomatic Mission_action_icon_Id']"));
UserRoleHelper.joinDiplomatic(roleName);
WebUI.delay(3);

checkEmptyState()

WebUI.delay(2)

WebUI.closeBrowser()

def checkEmptyState() {
    String embassyMediaEmptyStateErrorMessage = 'Error: Embassy media empty state exist'

    TestObject diplomaticMenuAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'diplomatic_menu_action_id\']')

    TestObject message_entity_object = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Message_DiplomaticMissions_entity_action\']')

    TestObject invite_entity_object = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Invite_DiplomaticMissions_entity_action\']')

    TestObject followers_label_object = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//span[@id=\'follower_count_id_tag_label\']')

    TestObject leadership_label_object = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//span[@id=\'diplomats_id_tag_label\']')

    if (!(TestHelper.isElementPresent(followers_label_object))) {
        TestHelper.thrownException('Followers Label not present')
    } else if (!(TestHelper.isElementPresent(leadership_label_object))) {
        TestHelper.thrownException('Leadership Label not present')
    } else if (!(TestHelper.isElementPresent(invite_entity_object))) {
        TestHelper.thrownException('Invite Action not present')
    } else if (!(TestHelper.isElementPresent(message_entity_object))) {
        TestHelper.thrownException('Message Action not present')
    }
    
    TestHelper.checkObjectExit('//div[@id=\'diplomatic_media\']', embassyMediaEmptyStateErrorMessage)

    TestHelper.checkObjectText('//div[@id=\'no-diplomatic-media-empty-state-title\']', 'Show sights, achievements or projects with images and videos', 
        embassyMediaEmptyStateErrorMessage)

    TestHelper.checkObjectText('//div[@id=\'no-diplomatic-media-empty-state\']', 'Add Photos or Videos', embassyMediaEmptyStateErrorMessage)

    TestHelper.checkObjectImageSrc('//div[@id=\'no-diplomatic-media-empty-state-image\']', 'assets/icons/emptyState/Media_Gallery.svg', 
        embassyMediaEmptyStateErrorMessage)

    if (TestHelper.isElementPresent(diplomaticMenuAction)) {
        TestHelper.clickButton(diplomaticMenuAction)

        TestObject reportOrganizationAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//a[@id=\'Report A Problem_user_action\']')

        TestObject shareOrganizationAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//a[@id=\'Share_user_action\']')

        if (!(TestHelper.isElementPresent(reportOrganizationAction)) || !(TestHelper.isElementPresent(shareOrganizationAction))) {
            TestHelper.thrownException('Diplomatic Action not present')
        } else {
            print('Check Complete')
        }
    } else {
        TestHelper.thrownException('Menu Action not present')
    }
}

