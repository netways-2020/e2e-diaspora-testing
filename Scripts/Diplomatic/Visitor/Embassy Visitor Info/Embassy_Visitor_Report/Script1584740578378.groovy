import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

String diplomaticName = TestHelper.getEmbassyOfOman_EmbassyName();
String reportDescription = 'Report description for '+ diplomaticName;
String reportContact = 'Report Contact for '+ diplomaticName;

// Register new user and go to my profile tab
long date = new Date().getTime();
String registeredEmail = 'netways_user' + date + '@gmail.com'
String password = 'cvW8qx4B2o3F4VwP/kNsqA=='

String firstName = 'FirstName_' + date;
String lastName = 'LastName_' + date;
String fullName = firstName + ' ' + lastName;
String city = 'Hermel';
 

TestHelper.registerNewUser(registeredEmail, password, firstName, lastName, city, true);
WebUI.navigateToUrl(TestHelper.getEmbassyOfOman_EmbassyUrl());
reportDiplomatic(reportDescription, reportContact)

WebUI.delay(2);
WebUI.closeBrowser();
 
 
def reportDiplomatic(String reportDescription, String reportContact ) {
	TestObject diplomaticMenuAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='diplomatic_menu_action_id']")
 
	if(TestHelper.isElementPresent(diplomaticMenuAction)) {
		TestHelper.clickButton(diplomaticMenuAction);

		TestObject reportDiplomaticAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Report A Problem_user_action']")
		
		if(!TestHelper.isElementPresent(reportDiplomaticAction)) {
			TestHelper.thrownException("Diplomatic Action not present");
		} else {
			TestHelper.clickButton(reportDiplomaticAction);
			UserActionsHelper.sendReport(reportDescription, reportContact);
		}
	} else {
		TestHelper.thrownException("Menu Action not present");
	}
}


