import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable


// Register new user and go to embassy
long date = new Date().getTime();
String registeredEmail = 'netways_user' + date + '@gmail.com'
String password = 'cvW8qx4B2o3F4VwP/kNsqA=='

String firstName = 'FirstName_' + date;
String lastName = 'LastName_' + date;
String fullName = firstName + ' ' + lastName;
String city = 'Hermel';
 

TestHelper.registerNewUser(registeredEmail, password, firstName, lastName, city, true);
WebUI.navigateToUrl(TestHelper.getEmbassyOfOman_EmbassyUrl());
checkEmptyState(fullName);

WebUI.delay(2);
WebUI.closeBrowser();

def checkEmptyState(String fullName) {
	String errorMessage = 'Invalid Join Diplomatic Action Title and SubTitle';
	
	TestObject diplomaticMenuAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='diplomatic_menu_action_id']")
	TestObject message_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Message_DiplomaticMissions_entity_action']")
	TestObject follow_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Follow_DiplomaticMissions_entity_action']")
	  
	TestObject followers_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='follower_count_id_tag_label']")
	TestObject leadership_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='diplomats_id_tag_label']")

	TestObject diplomatic_join_member_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div['join_diplomatic_section']")
	TestObject diplomatic_join_member_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div['join_diplomatic_action']")
	
	if(!TestHelper.isElementPresent(followers_label_object)) {
		TestHelper.thrownException("Followers Label not present");
	} else if(!TestHelper.isElementPresent(leadership_label_object)) {
		TestHelper.thrownException("Leadership Label not present");
	} else if(!TestHelper.isElementPresent(follow_entity_object)) {
		TestHelper.thrownException("Follow Action not present");
	} else if(!TestHelper.isElementPresent(message_entity_object)) {
		TestHelper.thrownException("Message Action not present");
	}
	else if(!TestHelper.isElementPresent(diplomatic_join_member_object)) {
		TestHelper.thrownException("Join member section not present");
	} else if(!TestHelper.isElementPresent(diplomatic_join_member_empty_state_object)) {
		TestHelper.thrownException("Join member section empty state not present");
	}
	
	TestHelper.checkObjectText("//div[@id='join_diplomatic_action']", "Member of this Embassy?" , errorMessage);
	TestHelper.checkObjectText("//div[@id='join_company_action_subTitle']", "Join now and indicate your role" , errorMessage);
	TestHelper.checkObjectImageSrc("//div[@id='diplomaticPage_Join Diplomatic Mission_action_icon_Id']", "assets/icons/diasporaIcon/Join_Diplomatic.svg", errorMessage)

	TestHelper.checkObjectText("//div[@id='diplomaticPage_Join Diplomatic Mission_action_title_Id']", "Join Diplomatic Mission" , errorMessage);
	TestHelper.checkObjectText("//div[@id='diplomaticPage_Join Diplomatic Mission_action_subTitle_Id']", "Tap here to get started" , errorMessage);

	if(TestHelper.isElementPresent(diplomaticMenuAction)) {
		TestHelper.clickButton(diplomaticMenuAction);

		TestObject reportCompanyAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Report A Problem_user_action']")
		TestObject shareCompanyAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Share_user_action']")
		
		if(!TestHelper.isElementPresent(reportCompanyAction) || !TestHelper.isElementPresent(shareCompanyAction)) {
			TestHelper.thrownException("Diplomatic Action not present");
		} else {
			checkFollower(followers_label_object, fullName);
		}
	} else {
		TestHelper.thrownException("Menu Action not present");
	}
}

def checkFollower(TestObject followers_label_object, String fullName) {
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='cancel_action_sheet_id']"))
	
	TestObject follow_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Follow_DiplomaticMissions_entity_action']");
	TestHelper.clickButton(follow_entity_object);
	WebUI.delay(2);

	
	TestHelper.clickButton(followers_label_object);
	
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='search-diplomatic-follower-input']"), fullName);
	WebUI.delay(2);
	String followerId = fullName + '_diplomatic_followers';
	TestObject followerObject = TestHelper.getObjectById("//div[@id='" + followerId + "']");
		
	if(!TestHelper.isElementPresent(followerObject)) {
		TestHelper.thrownException("Follower not exist");
	} else {
		print("Check Complete")
	}
}