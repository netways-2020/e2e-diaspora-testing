import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable


WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Register_new_user'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.navigateToUrl(TestHelper.getEmbassyOfSudan_EmbassyUrl());

// Check Embassy Actions
checkEmbassyActions();

WebUI.closeBrowser();
 
def checkEmbassyActions() {
	
	TestObject about_title_object = TestHelper.getObjectById("//div[@id='diplomatic_about_title_id']")
	TestObject about_subTitle_object = TestHelper.getObjectById("//div[@id='diplomatic_about_subTitle_id']")
	
	if(TestHelper.isElementPresent(about_title_object) && TestHelper.isElementPresent(about_subTitle_object)) {
		String aboutTitle = WebUI.getText(about_title_object);
		String aboutSubTitle = WebUI.getText(about_subTitle_object);
		
		if(!aboutTitle.equals("About") || !aboutSubTitle.equals("Information & contact details")) {
			TestHelper.thrownException("Invalid about title and subtitle")
		}		
	} else {
		TestHelper.thrownException("Invalid sections data found");
	}

 
	TestObject members_title_object = TestHelper.getObjectById("//div[@id='diplomatic_people_title_id']")
	TestObject members_subTitle_object = TestHelper.getObjectById("//div[@id='diplomatic_people_subTitle_id']")
	TestObject members_more_object = TestHelper.getObjectById("//div[@id='diplomatic_people_more_id']")
	
	if(TestHelper.isElementPresent(members_title_object) && TestHelper.isElementPresent(members_subTitle_object)  && TestHelper.isElementPresent(members_more_object)) {
		String membersTitle = WebUI.getText(members_title_object);
		String membersSubTitle = WebUI.getText(members_subTitle_object);
		String membersMore = WebUI.getText(members_more_object);
		
		if(!membersTitle.equals("People") || !membersSubTitle.equals("Diaspora members registered") || !membersMore.equals("More")) {
			TestHelper.thrownException("Invalid members title and subtitle")
		}
		
		TestHelper.clickButton(members_more_object);
		if(!TestHelper.checkCards('diplomatic_member_card')) {
			TestHelper.thrownException("Diplomatic members page not contains any card");
		}
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='view-diplomatic-members-back-button']"));
	} else {
		TestHelper.thrownException("Invalid sections data found");
	}

	
	TestObject diplomats_title_object = TestHelper.getObjectById("//div[@id='diplomatic_diplomats_title_id']")
	TestObject diplomats_subTitle_object = TestHelper.getObjectById("//div[@id='diplomatic_diplomats_subTitle_id']")
	
	if(TestHelper.isElementPresent(diplomats_title_object) && TestHelper.isElementPresent(diplomats_subTitle_object)) {
		String leadershipsTitle = WebUI.getText(diplomats_title_object);
		String leadershipsSubTitle = WebUI.getText(diplomats_subTitle_object);
		
		if(!leadershipsTitle.equals("Diplomats & Key Staff") && !leadershipsSubTitle.equals("Ambassadors & consuls")) {
			TestHelper.thrownException("Invalid diplomats title and subtitle")
		}
		
		TestObject diplomats_more_object = TestHelper.getObjectById("//div[@id='diplomatic_diplomats_more_id']")
		if(TestHelper.isElementPresent(diplomats_more_object)) {
			TestHelper.clickButton(diplomats_more_object);
			if(!TestHelper.checkCards('key_staff_member_card')) {
				TestHelper.thrownException("Key staff page not contains any card");
			}
			TestHelper.clickButton(TestHelper.getObjectById("//div[@id='view-diplomatic-key-staff-back-button']"));
		}
	} else {
		TestHelper.thrownException("Invalid sections data found");
	}
	
	
	
	TestObject updates_title_object = TestHelper.getObjectById("//div[@id='diplomatic_updates_title_id']")
	TestObject updates_subTitle_object = TestHelper.getObjectById("//div[@id='diplomatic_updates_subTitle_id']")
	TestObject updates_more_object = TestHelper.getObjectById("//div[@id='diplomatic_updates_more_id']")
	
	if(TestHelper.isElementPresent(updates_title_object) && TestHelper.isElementPresent(updates_subTitle_object) && TestHelper.isElementPresent(updates_more_object)) {
		String updatesTitle = WebUI.getText(updates_title_object);
		String updatesSubTitle = WebUI.getText(updates_subTitle_object);
		String updatesMore = WebUI.getText(updates_more_object);
		
		if(!updatesTitle.equals("Updates") || !updatesSubTitle.equals("Posts & discussions") || !updatesMore.equals("More")) {
			TestHelper.thrownException("Invalid updates title and subtitle")
		}

		TestHelper.clickButton(updates_more_object);
		if(!TestHelper.checkCards('listing_cards_card_item')) {
			TestHelper.thrownException("Card listing page not contains any card");
		}
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back_button_cardListing']"));
	} else {
		TestHelper.thrownException("Invalid sections data found");
	}

	
	TestObject ambassadorial_network_title_object = TestHelper.getObjectById("//div[@id='diplomatic_ambassadorial_network_title_id']")
	TestObject ambassadorial_network_subTitle_object = TestHelper.getObjectById("//div[@id='diplomatic_ambassadorial_network_subTitle_id']")
	TestObject ambassadorial_network_more_object = TestHelper.getObjectById("//div[@id='diplomatic_ambassadorial_network_more_id']")
	
	if(TestHelper.isElementPresent(ambassadorial_network_title_object) && TestHelper.isElementPresent(ambassadorial_network_subTitle_object) && TestHelper.isElementPresent(ambassadorial_network_more_object)) {
		String ambassadorialNetworkTitle = WebUI.getText(ambassadorial_network_title_object);
		String ambassadorialNetworkSubTitle = WebUI.getText(ambassadorial_network_subTitle_object);
		String ambassadorialNetworkMore = WebUI.getText(ambassadorial_network_more_object);
		
		if(!ambassadorialNetworkTitle.equals("Ambassadorial Network") || !ambassadorialNetworkSubTitle.equals("Helping the mission") || !ambassadorialNetworkMore.equals("More")) {
			TestHelper.thrownException("Invalid Ambassadorial Network title and subtitle")
		}
		
		TestHelper.clickButton(ambassadorial_network_more_object);
		if(!TestHelper.checkCards('ambassadorial_network_card')) {
			TestHelper.thrownException("Ambassadorial network page not contains any card");
		}
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='view-diplomatic-ambassadorial-network-back-button']"));
	} else {
		TestHelper.thrownException("Invalid sections data found");
	}

	
	TestObject supportStaff_title_object = TestHelper.getObjectById("//div[@id='diplomatic_support_staff_title_id']")
	TestObject supportStaff_subTitle_object = TestHelper.getObjectById("//div[@id='diplomatic_support_staff_subTitle_id']")
	TestObject supportStaff_more_object = TestHelper.getObjectById("//div[@id='diplomatic_support_staff_more_id']")
	
	if(TestHelper.isElementPresent(supportStaff_title_object) && TestHelper.isElementPresent(supportStaff_subTitle_object) && TestHelper.isElementPresent(supportStaff_more_object)) {
		String supportStaffTitle = WebUI.getText(supportStaff_title_object);
		String supportStaffSubTitle = WebUI.getText(supportStaff_subTitle_object);
		String supportStaffMore = WebUI.getText(supportStaff_more_object);
		
		if(!supportStaffTitle.equals("Support Staff") || !supportStaffSubTitle.equals("Other diplomatic staff") || !supportStaffMore.equals("More")) {
			TestHelper.thrownException("Invalid support staff title and subtitle")
		}
		
		TestHelper.clickButton(supportStaff_more_object);
		if(!TestHelper.checkCards('support_stuff_card')) {
			TestHelper.thrownException("Support staff page not contains any card");
		}
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_office_back_action']"));
	} else {
		TestHelper.thrownException("Invalid sections data found");
	}

	
	TestObject tradeOffice_title_object = TestHelper.getObjectById("//div[@id='diplomatic_trade_office_title_id']")
	TestObject tradeOffice_subTitle_object = TestHelper.getObjectById("//div[@id='diplomatic_trade_office_subTitle_id']")
	TestObject tradeOffice_more_object = TestHelper.getObjectById("//div[@id='diplomatic_trade_office_more_id']")
	
	if(TestHelper.isElementPresent(tradeOffice_title_object) && TestHelper.isElementPresent(tradeOffice_subTitle_object) && TestHelper.isElementPresent(tradeOffice_more_object)) {
		String tradeOfficeTitle = WebUI.getText(tradeOffice_title_object);
		String tradeOfficeSubTitle = WebUI.getText(tradeOffice_subTitle_object);
		String tradeOfficeMore = WebUI.getText(tradeOffice_more_object);
		
		if(!tradeOfficeTitle.equals("Trade Office") || !tradeOfficeSubTitle.equals("Lebanese-owned & vetted companies") || !tradeOfficeMore.equals("More")) {
			TestHelper.thrownException("Invalid trade office title and subtitle")
		}
		
		TestHelper.clickButton(tradeOffice_more_object);
		if(!TestHelper.checkCards('trade_office_card')) {
			TestHelper.thrownException("Trade office page not contains any card");
		}
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='view-diplomatic-trade-office-back-button']"));
	} else {
		TestHelper.thrownException("Invalid sections data found");
	}


	TestObject media_title_object = TestHelper.getObjectById("//div[@id='diplomatic_media_title_id']")
	TestObject media_subTitle_object = TestHelper.getObjectById("//div[@id='diplomatic_media_subTitle_id']")
	TestObject media_more_object = TestHelper.getObjectById("//div[@id='diplomatic_media_more_id']")
	
	if(TestHelper.isElementPresent(media_title_object) && TestHelper.isElementPresent(media_subTitle_object) && TestHelper.isElementPresent(media_more_object)) {
		String mediaTitle = WebUI.getText(media_title_object);
		String mediaSubTitle = WebUI.getText(media_subTitle_object);
		String mediaMore = WebUI.getText(media_more_object);
		
		if(!mediaTitle.equals("Media Gallery") || !mediaSubTitle.equals("Photos & videos") || !mediaMore.equals("More")) {
			TestHelper.thrownException("Invalid media title and subtitle")
		}
		
		TestHelper.clickButton(media_more_object);
		if(!TestHelper.checkCards('gallery_image_card')) {
			TestHelper.thrownException("Card listing page not contains any card");
		}
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back_button_gallery']"));
	} else {
		TestHelper.thrownException("Invalid sections data found");
	}
}


