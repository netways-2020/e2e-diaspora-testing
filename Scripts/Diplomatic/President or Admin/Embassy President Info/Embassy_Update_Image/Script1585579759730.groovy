import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import java.util.List as List
import java.util.Map as Map
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Diplomatic/President or Admin/Authorization for diplomatic/Login_For_Diplomatic'), [:], FailureHandling.STOP_ON_FAILURE)

//checkThatTheDiplomaticHasntImage()

TestHelper.goToDiplomaticManageEntity();
TestObject manageBasicInformationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Embassy Information_manage_entity_action']")
TestHelper.clickButton(manageBasicInformationAction)

changeDiplomaticImage()
checkThatTheDiplomaticHasImage()  

def checkThatTheDiplomaticHasntImage() {
    TestObject diplomaticAvatar = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//ngx-avatar[@id=\'DiplomaticMissions_hasnt_image\']')

    if (!(TestHelper.isElementPresent(diplomaticAvatar))) {
		TestHelper.thrownException("Diplomatic already has image")
    }
}

def changeDiplomaticImage() {
	String userPath = System.getProperty("user.dir")
	String imagePath = userPath + "//Image//test_image.png"

    TestObject changeImageAction = TestHelper.getObjectById('//div[@id=\'change_image_action\']')
    TestHelper.clickButton(changeImageAction)
	
    TestObject changeProfileImageAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//input[@id=\'image_gallery_action\']')
    WebUI.uploadFile(changeProfileImageAction, imagePath)
 
   TestObject selectImageAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'cropper_select_image\']')
    TestHelper.clickButton(selectImageAction)

	WebUI.delay(2);
    TestObject saveDiplomaticInfo = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//button[@id=\'save_diplomatic_info\']')
    TestHelper.clickButton(saveDiplomaticInfo)
    WebUI.delay(3)

    TestObject manageEntity = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'manage-DiplomaticMissions-back-button\']')
    TestHelper.clickButton(manageEntity)
}

def checkThatTheDiplomaticHasImage() {
    TestObject diplomaticAvatar = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//img[@id=\'DiplomaticMissions_has_image\']')

    if (!(TestHelper.isElementPresent(diplomaticAvatar))) {
        TestHelper.thrownException('Diplomatic image not updated')
    } else {
        println('Image has been updated successfully')
		WebUI.delay(2);
		WebUI.closeBrowser();
    }
}

