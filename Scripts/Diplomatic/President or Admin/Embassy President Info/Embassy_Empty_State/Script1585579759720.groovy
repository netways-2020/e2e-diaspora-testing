import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement


WebUI.callTestCase(findTestCase('Diplomatic/President or Admin/Authorization for diplomatic/Login_For_Diplomatic'), [:], FailureHandling.STOP_ON_FAILURE)
WebElement diplomaticNameObject = TestHelper.getItem('diplomatic-name-title', 0)
String diplomaticName = diplomaticNameObject.text;


// Check Common Header Exist
checkCommonHeaderElementExist();

// Check General Info
UserRoleHelper.checkDiplomaticGeneralInfoFieldsEmptyState();
 
// Check Diplomatic Section
checkDiplomaticSectionsEmptyState();
 
// Check Organization Section title and subtitle
checkDiplomaticTitleAndSubTitle();

// Check SubPages Empty State
checkSubPagesEmptyState();

// Check Update Empty State
checkUpdateEmptyState(diplomaticName);

// Check Media Empty State
checkMediaEmptyState();

WebUI.delay(2);
WebUI.closeBrowser();


def checkCommonHeaderElementExist() {
	boolean successEmptyFields = true;
	
	TestObject diplomaticMenuAction_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='diplomatic_menu_action_id']")
	TestObject manage_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Manage_DiplomaticMissions_entity_action']")
	TestObject invite_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Invite_DiplomaticMissions_entity_action']")	
	
	TestObject followers_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='follower_count_id_tag_label']")
	TestObject leadership_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='diplomats_id_tag_label']")

	if(!TestHelper.isElementPresent(followers_label_object)) {
		TestHelper.thrownException("Followers Label not present");
	} else if(!TestHelper.isElementPresent(leadership_label_object)) {
		TestHelper.thrownException("Leadership Label not present");
	} else if(!TestHelper.isElementPresent(diplomaticMenuAction_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(manage_entity_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(invite_entity_object)) {
		successEmptyFields = false;
	} 
	
	if(successEmptyFields) {
		println("Diplomatic common header info exist");
	} else {
			TestHelper.thrownException("Error: Diplomatic commone header info")
	}
}

def checkDiplomaticSectionsEmptyState() {

	String errorMessage = "Error: Organization section empty state exist";
	
	// About Section
		TestHelper.checkObjectExit("//div[@id='diplomatic_about']", errorMessage);
		//	TestObject diplomatic_about_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='diplomatic_about']")
		
		
	// Diplomats
		TestHelper.checkObjectExit("//div[@id='diplomatic_diplomats']", errorMessage);
		//	TestObject diplomatic_diplomats_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='diplomatic_diplomats']")

	// Updates
		TestHelper.checkObjectExit("//div[@id='diplomatic_updates']", errorMessage);
		TestHelper.checkObjectText("//div[@id='no-diplomatic-updates-empty-state-title']", "Notify the Lebanese community of important news and events" , errorMessage);
		TestHelper.checkObjectText("//div[@id='no-diplomatic-updates-empty-state']", "Post News or Events" , errorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no-diplomatic-updates-empty-state-image']", "assets/icons/emptyState/Post_Update.svg", errorMessage)
		
		//	TestObject diplomatic_updates_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='diplomatic_updates']")
		//	TestObject diplomatic_updates_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-diplomatic-updates-empty-state']")

	// Ambassadorial
		TestHelper.checkObjectExit("//div[@id='diplomatic_ambassadorial']", errorMessage);
		TestHelper.checkObjectText("//div[@id='no-diplomatic-ambassadorial-empty-state-title']", "Show and link to organizations supporting your mission" , errorMessage);
		TestHelper.checkObjectText("//div[@id='no-diplomatic-ambassadorial-empty-state']", "Add Supporting Organizations" , errorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no-diplomatic-ambassadorial-empty-state-image']", "assets/icons/diasporaIcon/Ambassadorial_Network_green.svg", errorMessage)

		//	TestObject diplomatic_ambassadorial_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='diplomatic_ambassadorial']")
		//	TestObject diplomatic_ambassadorial_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-diplomatic-ambassadorial-empty-state']")

	// Support Staff
		TestHelper.checkObjectExit("//div[@id='diplomatic_support_staff']", errorMessage);
		TestHelper.checkObjectText("//div[@id='no-diplomatic-support-staff-empty-state-title']", "Create support staff offices. Assign users and manage their roles" , errorMessage);
		TestHelper.checkObjectText("//div[@id='no-diplomatic-support-staff-empty-state']", "Create a Support Office" , errorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no-diplomatic-support-staff-empty-state-image']", "assets/icons/diasporaIcon/Support_Staff.svg", errorMessage)

		//	TestObject diplomatic_support_staff_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='diplomatic_support_staff']")
		//	TestObject diplomatic_support_staff_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-diplomatic-support-staff-empty-state']")
	
	// Trade Office
		TestHelper.checkObjectExit("//div[@id='diplomatic_trade_office']", errorMessage);
		TestHelper.checkObjectText("//div[@id='no-diplomatic-trade-office-empty-state-title']", "Promote business with Lebanese companies in your community" , errorMessage);
		TestHelper.checkObjectText("//div[@id='no-diplomatic-trade-office-empty-state']", "Add Lebanese Companies" , errorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no-diplomatic-trade-office-empty-state-image']", "assets/icons/diasporaIcon/Trade_Office.svg", errorMessage)

		//	TestObject diplomatic_trade_office_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='diplomatic_trade_office']")
		//	TestObject diplomatic_trade_office_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-diplomatic-trade-office-empty-state']")
	
	// Media
		TestHelper.checkObjectExit("//div[@id='diplomatic_media']", errorMessage);
		TestHelper.checkObjectText("//div[@id='no-diplomatic-media-empty-state-title']", "Show sights, achievements or projects with images and videos" , errorMessage);
		TestHelper.checkObjectText("//div[@id='no-diplomatic-media-empty-state']", "Add Photos or Videos" , errorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no-diplomatic-media-empty-state-image']", "assets/icons/emptyState/Media_Gallery.svg", errorMessage)

		//	TestObject diplomatic_media_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='diplomatic_media']")
		//	TestObject diplomatic_media_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-diplomatic-media-empty-state']")
	 
		println("Diplomatic section empty state exist");
}

def checkDiplomaticTitleAndSubTitle() {
	
	TestObject about_title_object = TestHelper.getObjectById("//div[@id='diplomatic_about_title_id']")
	TestObject about_subTitle_object = TestHelper.getObjectById("//div[@id='diplomatic_about_subTitle_id']")
	TestObject about_icon_object = TestHelper.getObjectById("//div[@id='diplomatic_about_title_id_icon']")
	
	if(TestHelper.isElementPresent(about_title_object) && TestHelper.isElementPresent(about_subTitle_object)) {
		String aboutTitle = WebUI.getText(about_title_object);
		String aboutSubTitle = WebUI.getText(about_subTitle_object);
		String aboutIcon = WebUI.getText(about_icon_object);
		
		if(!aboutTitle.equals("About") || !aboutSubTitle.equals("Information & contact details") || !aboutIcon.equals("assets/icons/diasporaIcon/About_Active.svg")) {
			TestHelper.thrownException("Invalid about title and subtitle")
		}
	}

 
	TestObject members_title_object = TestHelper.getObjectById("//div[@id='diplomatic_people_title_id']")
	TestObject members_subTitle_object = TestHelper.getObjectById("//div[@id='diplomatic_people_subTitle_id']")
	TestObject members_icon_object = TestHelper.getObjectById("//div[@id='diplomatic_people_title_id_icon']")
	
	if(TestHelper.isElementPresent(members_title_object) && TestHelper.isElementPresent(members_subTitle_object)) {
		String membersTitle = WebUI.getText(members_title_object);
		String membersSubTitle = WebUI.getText(members_subTitle_object);
		String membersIcon = WebUI.getText(members_icon_object);
		
		if(!membersTitle.equals("People") || !membersSubTitle.equals("Diaspora members registered") || !membersIcon.equals("assets/icons/diasporaIcon/People_Active.svg")) {
			TestHelper.thrownException("Invalid members title and subtitle")
		}
	}

	
	TestObject diplomats_title_object = TestHelper.getObjectById("//div[@id='diplomatic_diplomats_title_id']")
	TestObject diplomats_subTitle_object = TestHelper.getObjectById("//div[@id='diplomatic_diplomats_subTitle_id']")
	TestObject diplomats_icon_object = TestHelper.getObjectById("//div[@id='diplomatic_diplomats_title_id_icon']")
	
	if(TestHelper.isElementPresent(diplomats_title_object) && TestHelper.isElementPresent(diplomats_subTitle_object)) {
		String leadershipsTitle = WebUI.getText(diplomats_title_object);
		String leadershipsSubTitle = WebUI.getText(diplomats_subTitle_object);
		String leadershipsIcon = WebUI.getText(diplomats_icon_object);
		
		if(!leadershipsTitle.equals("Diplomats & Key Staff") || !leadershipsSubTitle.equals("Ambassadors & consuls") || !leadershipsIcon.equals("assets/icons/diasporaIcon/vip_badge.svg")) {
			TestHelper.thrownException("Invalid diplomats title and subtitle")
		}
	}
	
	TestObject updates_title_object = TestHelper.getObjectById("//div[@id='diplomatic_updates_title_id']")
	TestObject updates_subTitle_object = TestHelper.getObjectById("//div[@id='diplomatic_updates_subTitle_id']")
	TestObject updates_icon_object = TestHelper.getObjectById("//div[@id='diplomatic_updates_title_id_icon']")
	
	if(TestHelper.isElementPresent(updates_title_object) && TestHelper.isElementPresent(updates_subTitle_object)) {
		String updatesTitle = WebUI.getText(updates_title_object);
		String updatesSubTitle = WebUI.getText(updates_subTitle_object);
		String updatesIcon = WebUI.getText(updates_icon_object);
		
		if(!updatesTitle.equals("Updates") || !updatesSubTitle.equals("Posts & discussions") || !updatesIcon.equals("assets/icons/diasporaIcon/News_Active.svg")) {
			TestHelper.thrownException("Invalid updates title and subtitle")
		}

	}

	
	TestObject ambassadorial_network_title_object = TestHelper.getObjectById("//div[@id='diplomatic_ambassadorial_network_title_id']")
	TestObject ambassadorial_network_subTitle_object = TestHelper.getObjectById("//div[@id='diplomatic_ambassadorial_network_subTitle_id']")
	TestObject ambassadorial_network_icon_object = TestHelper.getObjectById("//div[@id='diplomatic_ambassadorial_network_title_id_icon']")
	
	if(TestHelper.isElementPresent(ambassadorial_network_title_object) && TestHelper.isElementPresent(ambassadorial_network_subTitle_object)) {
		String ambassadorialNetworkTitle = WebUI.getText(ambassadorial_network_title_object);
		String ambassadorialNetworkSubTitle = WebUI.getText(ambassadorial_network_subTitle_object);
		String ambassadorialNetworkIcon = WebUI.getText(ambassadorial_network_icon_object);
		
		if(!ambassadorialNetworkTitle.equals("Ambassadorial Network") || !ambassadorialNetworkSubTitle.equals("Helping the mission") || !ambassadorialNetworkIcon.equals("assets/icons/diasporaIcon/Ambassadorial_Network.svg")) {
			TestHelper.thrownException("Invalid Ambassadorial Network title and subtitle")
		}
	}

	TestObject supportStaff_title_object = TestHelper.getObjectById("//div[@id='diplomatic_support_staff_title_id']")
	TestObject supportStaff_subTitle_object = TestHelper.getObjectById("//div[@id='diplomatic_support_staff_subTitle_id']")
	TestObject supportStaff_icon_object = TestHelper.getObjectById("//div[@id='diplomatic_support_staff_title_id_icon']")
	
	if(TestHelper.isElementPresent(supportStaff_title_object) && TestHelper.isElementPresent(supportStaff_subTitle_object)) {
		String supportStaffTitle = WebUI.getText(supportStaff_title_object);
		String supportStaffSubTitle = WebUI.getText(supportStaff_subTitle_object);
		String supportStaffIcon = WebUI.getText(supportStaff_icon_object);
		
		if(!supportStaffTitle.equals("Support Staff") || !supportStaffSubTitle.equals("Other diplomatic staff") || !supportStaffIcon.equals("assets/icons/diasporaIcon/committee.svg")) {
			TestHelper.thrownException("Invalid support staff title and subtitle")
		}
	}

	
	TestObject tradeOffice_title_object = TestHelper.getObjectById("//div[@id='diplomatic_trade_office_title_id']")
	TestObject tradeOffice_subTitle_object = TestHelper.getObjectById("//div[@id='diplomatic_trade_office_subTitle_id']")
	TestObject tradeOffice_icon_object = TestHelper.getObjectById("//div[@id='diplomatic_trade_office_title_id_icon']")
	
	if(TestHelper.isElementPresent(tradeOffice_title_object) && TestHelper.isElementPresent(tradeOffice_subTitle_object)) {
		String tradeOfficeTitle = WebUI.getText(tradeOffice_title_object);
		String tradeOfficeSubTitle = WebUI.getText(tradeOffice_subTitle_object);
		String tradeOfficeIcon = WebUI.getText(tradeOffice_icon_object);
		
		if(!tradeOfficeTitle.equals("Trade Office") || !tradeOfficeSubTitle.equals("Lebanese-owned & vetted companies") || !tradeOfficeIcon.equals("assets/icons/diasporaIcon/affiliation.svg")) {
			TestHelper.thrownException("Invalid trade office title and subtitle")
		}
	}


	TestObject media_title_object = TestHelper.getObjectById("//div[@id='diplomatic_media_title_id']")
	TestObject media_subTitle_object = TestHelper.getObjectById("//div[@id='diplomatic_media_subTitle_id']")
	TestObject media_icon_object = TestHelper.getObjectById("//div[@id='diplomatic_media_title_id_icon']")
	
	if(TestHelper.isElementPresent(media_title_object) && TestHelper.isElementPresent(media_subTitle_object)) {
		String mediaTitle = WebUI.getText(media_title_object);
		String mediaSubTitle = WebUI.getText(media_subTitle_object);
		String mediaIcon = WebUI.getText(media_icon_object);

		if(!mediaTitle.equals("Media Gallery") || !mediaSubTitle.equals("Photos & videos") || !mediaIcon.equals("assets/icons/diasporaIcon/View_Gallery.svg")) {
			TestHelper.thrownException("Invalid media title and subtitle")
		}
	}
}

def checkSubPagesEmptyState() {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Manage_DiplomaticMissions_entity_action']"))
	checkManageEntityIcon();
	// Check Manage Members Page
		String pendingMemberErrorMessage = "Error: Invalid Pending Member Empty State";
		String approvedMemberErrorMessage = "Error: Invalid Approved Member Empty State";
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Members & Joins_manage_entity_action']"))
		 
		TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='diplomatic_pending_member_search_input']"), "abcdefgh123")
		WebUI.delay(2);
		TestHelper.checkObjectText("//div[@id='no_pending_member_empty_state_title']", "No Pending Requests" , pendingMemberErrorMessage);
		TestHelper.checkObjectText("//div[@id='no_pending_member_empty_state_subtitle']", "There are currently no pending requests. We will notify you when a new request arrives" , pendingMemberErrorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no_pending_member_empty_state_image']", "assets/icons/emptyState/Person.svg", pendingMemberErrorMessage)
	
		
		TestHelper.clickButton(TestHelper.getObjectById("//ion-segment-button[@id='diplomatic_approved_employee_tab_id']"))
		
		TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='diplomatic_current_member_search_input']"), "abcdefgh123")
		WebUI.delay(2);
		TestHelper.checkObjectText("//div[@id='no_current_member_empty_state_title']", "No Members" , pendingMemberErrorMessage);
		TestHelper.checkObjectText("//div[@id='no_current_member_empty_state_subtitle']", "Approved join requests will appear here" , pendingMemberErrorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no_current_member_empty_state_image']", "assets/icons/emptyState/Person.svg", pendingMemberErrorMessage)

		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-diplomatic-key-staff-back-button']"))
					
	// Check Administrator Page
		String administratorListPageErrorMessage = "Error: Invalid Administrator List Empty State";
		String addAdministratorPageErrorMessage = "Error: Invalid Add Administrator Empty State";
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Page Administrators_manage_entity_action']"))
		 
		TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='manage-diplomatic-admin-search-input']"), "abcdefgh123")
		WebUI.delay(2);
		
		TestHelper.checkObjectText("//div[@id='manage_admin_diplomatic_empty_state_title']", "No Page Administrators" , administratorListPageErrorMessage);
		TestHelper.checkObjectText("//div[@id='manage_admin_diplomaticr_empty_state_subtitle']", "This page doesn’t currently have any administrators. You can assign a page member or invite someone via email" , administratorListPageErrorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='manage_admin_diplomatic_empty_state_image']", "assets/icons/emptyState/Person.svg", administratorListPageErrorMessage)
		TestHelper.checkObjectText("//div[@id='manage-admin-diplomatic-add-new-admin-empty-state']", "Add Page Administrator" , administratorListPageErrorMessage);
		
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-admin-diplomatic-add-new-admin-empty-state']"))
		
		TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='assign-diplomatic-admin-search-input']"), "abcdefgh123")
		
		TestHelper.checkObjectText("//div[@id='add_admin_member_empty_state_title']", "No Members" , addAdministratorPageErrorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='add_admin_member_empty_state_image']", "assets/icons/emptyState/Person.svg", addAdministratorPageErrorMessage)
		 
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='add-admin-diplomatic-back-button']"))
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-admin-diplomatic-back-button']"))
	
	// Manage Tarde Office Page
		String manageTradeOfficeErrorMessage = "Error: Invalid Manage Trade Office Empty State";
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Trade Office_manage_entity_action']"))
		 
		TestHelper.checkObjectText("//div[@id='manage_trade_office_empty_state_title']", "No Affiliated Companies" , manageTradeOfficeErrorMessage);
		TestHelper.checkObjectText("//div[@id='manage_trade_office_empty_state_subtitle']", "Start by adding companies under your organization. They will appear here once they approve the affiliation" , manageTradeOfficeErrorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='manage_trade_office_empty_state_image']", "assets/icons/emptyState/List.svg", manageTradeOfficeErrorMessage)
		TestHelper.checkObjectText("//div[@id='manage-trade-office-add-new-company-empty-state']", "Add A Company", manageTradeOfficeErrorMessage)
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_trade_office_back_button']"))

	// Manage Ambassadorial Network Page
		String manageAmbassadorialNetworkErrorMessage = "Error: Invalid Manage Ambassadorial Network Empty State";
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Ambassadorial Network_manage_entity_action']"))
		 
		TestHelper.checkObjectText("//div[@id='manage_ambassadorial_network_empty_state_title']", "No Organizations" , manageTradeOfficeErrorMessage);
		TestHelper.checkObjectText("//div[@id='manage_ambassadorial_network_empty_state_subtitle']", "Approved join requests will appear here" , manageTradeOfficeErrorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='manage_ambassadorial_network_empty_state_image']", "assets/icons/emptyState/List.svg", manageTradeOfficeErrorMessage)
		TestHelper.checkObjectText("//div[@id='manage-ambassadorial-network-add-new-organization-empty-state']", "Add An Organization", manageTradeOfficeErrorMessage)
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_ambassadorial_network_back_button']"))
		
				
	// Manage Diplomatic Office Page
		String manageDiplomaticOfficeErrorMessage = "Error: Invalid Manage Diplomatic Office Empty State";
		String manageDiplomaticOfficeMemberErrorMessage = "Error: Invalid Manage Diplomatic Office Member Empty State";
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='Support Staff_manage_entity_action']"))
		 
		TestHelper.checkObjectText("//div[@id='manage_diplomatic_offices_empty_state_title']", "No Support Staff Listed" , manageDiplomaticOfficeErrorMessage);
		TestHelper.checkObjectText("//div[@id='manage_diplomatic_offices_empty_state_subtitle']", "This page doesn’t currently have any listed support staff. Tap the button below to create one" , manageDiplomaticOfficeErrorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='manage_diplomatic_offices_empty_state_image']", "assets/icons/emptyState/List.svg", manageDiplomaticOfficeErrorMessage)
		TestHelper.checkObjectText("//div[@id='no-diplomatic-offices-empty-state-button']", "Create Office", manageDiplomaticOfficeErrorMessage)
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='no-diplomatic-offices-empty-state-button']"))
		TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='office-name-input']"), "testOffice")
		WebUI.delay(2);
		TestHelper.clickButton(TestHelper.getObjectById("//button[@id='add-office-name-action']"))
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='testOffice_diplomatic_office']"))

	// Committe Member Page			
		TestHelper.checkObjectText("//div[@id='add_office_member_empty_state_title']", "No Members" , manageDiplomaticOfficeMemberErrorMessage);
		TestHelper.checkObjectText("//div[@id='add_office_member_empty_state_subtitle']", "This office doesn’t currently have any members. Tap the button below to add or join" , manageDiplomaticOfficeMemberErrorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='add_office_member_empty_state_image']", "assets/icons/emptyState/Person.svg", manageDiplomaticOfficeMemberErrorMessage)
		TestHelper.checkObjectText("//div[@id='add-office-member-empty-state-button']", "Add Members", manageDiplomaticOfficeMemberErrorMessage)
		TestHelper.checkObjectText("//div[@id='join-office-member-empty-state-button']", "Join Office", manageDiplomaticOfficeMemberErrorMessage)
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='add-office-member-empty-state-button']"));
		TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='add-diplomatic-office-input-search']"), "abcdefgh123")
		WebUI.delay(2);

		TestHelper.checkObjectText("//div[@id='no_staff_member_title']", "No Members" , manageDiplomaticOfficeMemberErrorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no_staff_member_image']", "assets/icons/emptyState/Person.svg", manageDiplomaticOfficeMemberErrorMessage)
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='add_diplomatic_office_back_action']"));
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-diplomatic-member-action']"));
		TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Delete Office_user_action']"));
		WebUI.delay(1);
		WebElement removeOfficeConfirmation = TestHelper.getItem('remove-office-confirmation', 0)
		removeOfficeConfirmation.click();
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_office_back_action']"))   
   	    TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-DiplomaticMissions-back-button']"))
		   
	
	 // Check follwers
	   String followerErrorMessage = "Error: Invalid Followers Page Empty State";
		   
	   WebUI.delay(1)
   	   TestHelper.clickButton(TestHelper.getObjectById("//span[@id='follower_count_id_tag_label']"));
  	   TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='search-diplomatic-follower-input']"), 'abcdefgh123');
  	   WebUI.delay(2);

	   TestHelper.checkObjectText("//div[@id='no_followers_title']", "No Results Found" , followerErrorMessage);
	   TestHelper.checkObjectImageSrc("//div[@id='no_followers_image']", "assets/icons/emptyState/Person.svg", followerErrorMessage)
 
	   TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back-follower-id']"));
		 
	   
	  // Check Diplmatic members
	   TestHelper.clickButton(TestHelper.getObjectById("//div[@id='diplomatic_people_more_id']"));
	   TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='search-diplomatic-member-input']"), 'abcdefgh123');
	   WebUI.delay(2);
	   TestHelper.checkObjectText("//div[@id='no_diplomatic_member_title']", "No Results Found" , followerErrorMessage);
	   TestHelper.checkObjectImageSrc("//div[@id='no_diplomatic_member_image']", "assets/icons/emptyState/Person.svg", followerErrorMessage)
 
	   TestHelper.clickButton(TestHelper.getObjectById("//div[@id='view-diplomatic-members-back-button']"));
	   
}



def checkUpdateEmptyState(String diplomaticName) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='no-diplomatic-updates-empty-state']"));
	long now = new Date().getTime();
	String postTitle = 'New post title add now_' + now;
	String postDescription = 'New post description add now_' + now;
		
	TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Create Post_user_action']"));
	
	UserActionsHelper.addPost(postTitle, postDescription)
	WebUI.delay(2);
	TestHelper.verifyCardExist(diplomaticName, postTitle, null, false);
	
	TestObject manageCardObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='diplomatic_manage_updates']")
	boolean manageCardObjectExist = TestHelper.isElementPresent(manageCardObject);
	if(!manageCardObjectExist) {
		TestHelper.thrownException("Manage card button not exist");
	}
	
	TestHelper.clickButton(manageCardObject);
	String cartId = postTitle + '_descriptionId';
	TestHelper.clickButton(TestHelper.getObjectById("//span[@id='" + cartId + "']"));
		

	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='card_details_manage_button']"))
	WebUI.delay(1);
	TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Delete Post_user_action']"))
	WebUI.delay(1);
	
	WebElement deleteCardConfirmation = TestHelper.getItem('remove-new-card', 0)
	deleteCardConfirmation.click();
	
	WebUI.delay(1);
	
	String updateErrorMessage = "Error: Invalid Updates List Empty State";
	TestHelper.checkObjectText("//div[@id='no_updates_empty_state_title']", "Nothing to Show Here" , updateErrorMessage);
	TestHelper.checkObjectText("//div[@id='no_updates_empty_state_subtitle']", "There are no entries to display on this page" , updateErrorMessage);
	TestHelper.checkObjectImageSrc("//div[@id='no_updates_empty_state_image']", "assets/icons/emptyState/List.svg", updateErrorMessage)

	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back_button_cardListing']"))
}

def checkMediaEmptyState() {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='no-diplomatic-media-empty-state']"));

	long now = new Date().getTime();
	String imageCaption = 'caption' + now;
	UserActionsHelper.addMedia(imageCaption);
	TestObject addPhotoAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='diplomatic_manage_media']")
	WebUI.click(addPhotoAction)
	
	TestHelper.checkIfImageAddedSuccessfully(true, 0);
	TestHelper.removeImage();
	
	WebUI.delay(1);
	String mediaErrorMessage = "Error: Invalid Media Empty State";
	TestHelper.checkObjectText("//div[@id='no_gallery_images_empty_state_title']", "No Photo Or Video Added" , mediaErrorMessage);
	TestHelper.checkObjectImageSrc("//div[@id='no_gallery_images_empty_state_image']", "assets/icons/emptyState/List.svg", mediaErrorMessage)

	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back_button_gallery']"))
}


def checkManageEntityIcon() {
	String manage_entity_error_message = 'Invalid manage entity info';
	
	TestHelper.checkObjectText("//div[@id='Embassy Information_manage_entity_action']", "Embassy Information" , manage_entity_error_message);
	TestHelper.checkObjectText("//div[@id='Embassy Information_manage_entity_subtitle_action']", "Logo, description…" , manage_entity_error_message);
	TestHelper.checkObjectImageSrc("//div[@id='Embassy Information_manage_entity_icon_action']", "assets/icons/diasporaIcon/Basic_Information.svg", manage_entity_error_message)
	   
	TestHelper.checkObjectText("//div[@id='Members & Joins_manage_entity_action']", "Members & Joins" , manage_entity_error_message);
	TestHelper.checkObjectText("//div[@id='Members & Joins_manage_entity_subtitle_action']", "Manage roles & join requests" , manage_entity_error_message);
	TestHelper.checkObjectImageSrc("//div[@id='Members & Joins_manage_entity_icon_action']", "assets/icons/diasporaIcon/Diplomats.svg", manage_entity_error_message)
	 
	TestHelper.checkObjectText("//div[@id='Support Staff_manage_entity_action']", "Support Staff" , manage_entity_error_message);
	TestHelper.checkObjectText("//div[@id='Support Staff_manage_entity_subtitle_action']", "Manage staff and offices" , manage_entity_error_message);
	TestHelper.checkObjectImageSrc("//div[@id='Support Staff_manage_entity_icon_action']", "assets/icons/diasporaIcon/Committees.svg", manage_entity_error_message)
	 
	TestHelper.checkObjectText("//div[@id='Ambassadorial Network_manage_entity_action']", "Ambassadorial Network" , manage_entity_error_message);
	TestHelper.checkObjectText("//div[@id='Ambassadorial Network_manage_entity_subtitle_action']", "Your embassy’s support network" , manage_entity_error_message);
	TestHelper.checkObjectImageSrc("//div[@id='Ambassadorial Network_manage_entity_icon_action']", "assets/icons/diasporaIcon/Ambassadorial_Network.svg", manage_entity_error_message)
 
	TestHelper.checkObjectText("//div[@id='Trade Office_manage_entity_action']", "Trade Office" , manage_entity_error_message);
	TestHelper.checkObjectText("//div[@id='Trade Office_manage_entity_subtitle_action']", "Businesses listed under your embassy" , manage_entity_error_message);
	TestHelper.checkObjectImageSrc("//div[@id='Trade Office_manage_entity_icon_action']", "assets/icons/diasporaIcon/Chapters.svg", manage_entity_error_message)
 
	TestHelper.checkObjectText("//div[@id='Page Administrators_manage_entity_action']", "Page Administrators" , manage_entity_error_message);
	TestHelper.checkObjectText("//div[@id='Page Administrators_manage_entity_subtitle_action']", "Assign or remove page administrators" , manage_entity_error_message);
	TestHelper.checkObjectImageSrc("//div[@id='Page Administrators_manage_entity_icon_action']", "assets/icons/diasporaIcon/Page_Administrator.svg", manage_entity_error_message)
} 		 
		 		 

								  
						  
				 		