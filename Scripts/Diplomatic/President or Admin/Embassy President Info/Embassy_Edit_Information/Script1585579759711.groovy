import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import java.util.List
import java.util.Map

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

// Define Varibales
TestObject embassyName_input, description_textArea;
TestObject headquarterCountry_button, headquarterCity_button, address_input;
TestObject phoneCountryCode_button, phoneNumber_input, contactEmail_input, poBox_input;
TestObject website_input, facebook_input, linkedin_input, instagram_input, twitter_input;

//List<String> countries = TestHelper.prepareCountries();
//Map<String, List<String>> cities = TestHelper.prepareCities();
 
 
// New Value
long newDate =  new Date().getTime();

// Country and city
String selectedHeadquarterCountry = TestHelper.embassyOmanCounty();
List<String> citiesOfHeadquarterCountry = TestHelper.omanCities();
		
String selectedHeadquarterCity = citiesOfHeadquarterCountry.get(0); // TestHelper.getRandomValueFromList(citiesOfHeadquarterCountry);
 
  
// Login and go to my diplomatic
WebUI.callTestCase(findTestCase('Diplomatic/President or Admin/Authorization for diplomatic/Login_For_Diplomatic'), [:], FailureHandling.STOP_ON_FAILURE)

TestHelper.goToDiplomaticManageEntity();
TestObject manageBasicInformationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Embassy Information_manage_entity_action']")
TestHelper.clickButton(manageBasicInformationAction)


// Edit Diplomatic and check values
prepareEditDiplomaticFields();
changeDiplomaticInfoValue(newDate, selectedHeadquarterCountry, selectedHeadquarterCity);
checkEditedDiplomaticValue(newDate, selectedHeadquarterCountry, selectedHeadquarterCity);

//// Clear Edited Fields
TestHelper.goToDiplomaticManageEntity();
TestHelper.clickButton(manageBasicInformationAction)
clearDiplomaticFields(selectedHeadquarterCountry);
DiplomaticHelper.checkDiplomaticGeneralInfoFieldsEmptyState();
WebUI.closeBrowser();


def prepareEditDiplomaticFields() {
	// About Info
	embassyName_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='diplomatic-name-id']")
	description_textArea = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//textarea[@id='about-section']")
	address_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='diplomatic-address-id']")
	 
	// Place Info
	headquarterCountry_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='diplomatic-headquarter-country']")
	headquarterCity_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='diplomatic-headquarter-city']")
	  
	// Contact Info
	phoneCountryCode_button = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='diplomatic-phone-country-code']")
	phoneNumber_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='diplomatic-phone-number']")
	contactEmail_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='diplomatic-contact-email']")
	poBox_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='pobox-input']")
	
	// Social Media
	website_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='diplomatic-website']")
	facebook_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='diplomatic-social-media-facebook']")
	linkedin_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='diplomatic-social-media-linkedin']")
	instagram_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='diplomatic-social-media-instagram']")
	twitter_input = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='diplomatic-social-media-twitter']")
}

// Edit Fields
def changeDiplomaticInfoValue(long newDate, String selectedHeadquarterCountry, String selectedHeadquarterCity) {
	
	// User info
		TestHelper.setInputValue(embassyName_input, diplomaticName + newDate)
		TestHelper.setInputValue(description_textArea, newAbout + newDate)
		
	 		
	// Headquarter Country/City
		WebUI.delay(2)
		TestHelper.clickButton(headquarterCountry_button)
		TestObject headquarterCountrySearchInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='country-search-input']")
		TestHelper.setInputValue(headquarterCountrySearchInput, selectedHeadquarterCountry)
		TestObject headquarterCountryObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+selectedHeadquarterCountry+"']")
		WebUI.delay(2.5)
		WebUI.click(headquarterCountryObject)
		
		TestHelper.clickButton(headquarterCity_button)
		TestObject headquarterCitySearchInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='city-search-input']")
		TestHelper.setInputValue(headquarterCitySearchInput, selectedHeadquarterCity)
		TestObject headquarterCityObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+selectedHeadquarterCity+"']")
		WebUI.delay(1)
		WebUI.click(headquarterCityObject)
		
	// Address
		TestHelper.setInputValue(address_input, newAddress + newDate)
				  
	// Contact Info
		WebUI.delay(2)
		
		TestHelper.clickButton(phoneCountryCode_button)
		TestObject phoneCountrySearchInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='country-search-input']")
		TestHelper.setInputValue(phoneCountrySearchInput, selectedHeadquarterCountry)
		TestObject phoneCountryObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+selectedHeadquarterCountry+"']")
		WebUI.delay(1)
		WebUI.click(phoneCountryObject)
		
		TestHelper.setInputValue(phoneNumber_input, '123456')
 
	// Email
		TestHelper.setInputValue(contactEmail_input, newEmail + newDate + '@gmail.com')
		TestHelper.setInputValue(website_input, newWebsite + newDate)
		TestHelper.setInputValue(poBox_input, '123456')
		
	// Social Media
		WebUI.delay(1);
		TestHelper.setInputValue(facebook_input, newFacebook + newDate)
		TestHelper.setInputValue(linkedin_input, newLinkedIn + newDate)
		TestHelper.setInputValue(instagram_input, newInstagram + newDate)
		TestHelper.setInputValue(twitter_input, newTwitter + newDate)
		
		TestObject saveDiplomaticInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='save_diplomatic_info']")
		TestHelper.clickButton(saveDiplomaticInfo);
		WebUI.delay(3)
		
		TestObject manageEntity = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-DiplomaticMissions-back-button']")
		TestHelper.clickButton(manageEntity);
}

def checkEditedDiplomaticValue(long newDate, String selectedHeadquarterCountry, String selectedHeadquarterCity) {
	WebUI.delay(1)
	boolean successfullyEdited = true;
	
	String headquarterCountry = TestHelper.getCountryIso(selectedHeadquarterCountry);
	String countryIsoCode = TestHelper.getCountryIso(selectedHeadquarterCountry);
	String phoneCountryCode = TestHelper.getCountryCode(selectedHeadquarterCountry);
	 
		
	 // Prepare IDs
		// Basic Info
			String edited_diplomatic_name_id = 'diplomatic_title_' + (diplomaticName + newDate);
			String edited_diplomatic_description_id = 'diplomatic_introduction_id_' + (newAbout + newDate);
			String edited_diplomatic_address_id = 'diplomatic_address_id_' + (newAddress + newDate);
  
		// Countries
			String edited_country_headquarter  = 'country_' + countryIsoCode;
			String edited_city_headquarter  = 'country_' + countryIsoCode + '_tag_label' ;
			
		 // Contact Info
			String edited_phone_id = 'diplomatic_contact_phone_id_'+ phoneCountryCode + '123456';
			String edited_email_id = 'diplomatic_contact_email_id_'+ (newEmail + newDate) + '@gmail.com';
			String edited_pobox_id = 'diplomatic_pobox_id_'+  '123456';
			
		// Social Media
			String edited_website_id  = 'diplomatic_contact_website_id_' + newWebsite  + newDate;
			String edited_facebook_id  = "@id='diplomatic_facebook_id'  and  @alt= '" + newFacebook  + newDate +"'";
			String edited_linkedin_id  = "@id='diplomatic_linkedin_id'  and  @alt= '" + newLinkedIn  + newDate +"'";
			String edited_twitter_id   = "@id='diplomatic_twitter_id'   and  @alt= '" + newTwitter   + newDate +"'";
			String edited_instagram_id = "@id='diplomatic_instagram_id' and  @alt= '" + newInstagram + newDate +"'";
			
	// Prepare Objects
		// Basic Info
			TestObject edited_diplomatic_name_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + edited_diplomatic_name_id +"']");
			TestObject edited_about_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + edited_diplomatic_description_id +"']");
			TestObject edited_address_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + edited_diplomatic_address_id +"']")
			
		
		// Countries Info
			TestObject edited_diplomatic_country_headquarter = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//img[@id='" + edited_country_headquarter +"']");
			TestObject edited_diplomatic_city_headquarter = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='" + edited_city_headquarter +"']");
			 
			 
		// Contact Info
			TestObject edited_email_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + edited_email_id +"']")
			TestObject edited_phone_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + edited_phone_id +"']")
			TestObject edited_pobox_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + edited_pobox_id +"']")
			TestObject edited_website_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + edited_website_id +"']")
			
		// Social Media
			TestObject edited_facebook_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//img["+ edited_facebook_id +"]")
			TestObject edited_linkedin_object = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//img["+ edited_linkedin_id +"]")
			TestObject edited_twitter_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,  "//img["+ edited_twitter_id  +"]")
			TestObject edited_instagram_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//img["+ edited_instagram_id +"]")
			
			
	// Check Object Exist
		if(!TestHelper.isElementPresent(edited_diplomatic_name_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_about_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_address_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_diplomatic_country_headquarter)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_diplomatic_city_headquarter)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_email_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_phone_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_pobox_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_website_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_facebook_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_linkedin_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_twitter_object)) {
			successfullyEdited = false;
		} else if(!TestHelper.isElementPresent(edited_instagram_object)) {
			successfullyEdited = false;
		}
 
 
	if(successfullyEdited) {
	  println("Diplomatic Updated Successfully");
	} else {
	  TestHelper.thrownException("Diplomatic Not Updated")
	}
	
}
	
// Reset Fields
def clearDiplomaticFields(String selectedCountryOfResidence) {
	
	
	TestHelper.setInputValue(embassyName_input, 'Embassy of Lebanon in Oman')
	TestHelper.setInputValue(description_textArea, " ");
	WebUI.delay(1);
	TestHelper.setInputValue(address_input, " ");
	
	
	// Contact Info
	WebUI.delay(1);
	TestHelper.clickButton(phoneCountryCode_button)
	TestObject phoneCountrySearchInput = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@id='country-search-input']")
	TestHelper.setInputValue(phoneCountrySearchInput, selectedCountryOfResidence)
	TestObject phoneCountryObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+selectedCountryOfResidence+"']")
	WebUI.delay(1)
	WebUI.click(phoneCountryObject)

	
	TestHelper.setInputValue(contactEmail_input, " ");
//	TestHelper.setInputValue(poBox_input, null);
	
	// Social Media
	WebUI.delay(1);
	TestHelper.setInputValue(website_input, " ");
	WebUI.delay(1);
	TestHelper.setInputValue(facebook_input, " ");
	WebUI.delay(1);
	TestHelper.setInputValue(linkedin_input, " ");
	WebUI.delay(1);
	TestHelper.setInputValue(instagram_input, " ");
	WebUI.delay(1);
	TestHelper.setInputValue(twitter_input, " ");
		
	TestObject saveCompanyInfo = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//button[@id='save_diplomatic_info']")
	TestHelper.clickButton(saveCompanyInfo);
	WebUI.delay(3)
	
	TestObject manageEntity = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-DiplomaticMissions-back-button']")
	TestHelper.clickButton(manageEntity);
}

