import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Login_With_Super_User'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.navigateToUrl(TestHelper.getEmbassyOfRiyadh_EmbassyUrl());

WebUI.delay(2);
checkInviteAction();

WebUI.closeBrowser();

def checkInviteAction() {	
	TestObject emptyStateTitle = TestHelper.getObjectById("//div[@id='no-diplomatic-peoples-empty-state-title']")
	String emptyStateTitleText = WebUI.getText(emptyStateTitle);
	
	if(!TestHelper.isElementPresent(emptyStateTitle)) {
		TestHelper.thrownException("Invite Not Present");
	}
	
	if(!emptyStateTitleText.trim().equals("Invite members to register and receive your updates via DiasporaID")) {
		TestHelper.thrownException("Invite Not Present");
	} 
	
	TestObject emptyStateAction = TestHelper.getObjectById("//div[@id='no-diplomatic-peoples-empty-state']")
	String emptyStateActionText = WebUI.getText(emptyStateAction);
	
	if(!TestHelper.isElementPresent(emptyStateAction)) {
		TestHelper.thrownException("Invite Not Present");
	}
	
	if(!emptyStateActionText.trim().equals("Invite Lebanese Diaspora")) {
		TestHelper.thrownException("Invite Not Present");
	}
	
	print("Check Complete");
}
