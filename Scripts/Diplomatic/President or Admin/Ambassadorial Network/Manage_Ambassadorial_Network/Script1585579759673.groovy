import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement


WebUI.callTestCase(findTestCase('Organization/Founder or admin/Authorization for organization/Login_For_Organization'), [:], FailureHandling.STOP_ON_FAILURE)

String organizationUrl = WebUI.getUrl();
WebElement organizationObject = TestHelper.getItem('organization-name-title', 0)
String organizationName = organizationObject.text;

WebUI.navigateToUrl(TestHelper.getEmbassyOfOman_EmbassyUrl());

TestHelper.goToDiplomaticManageEntity();
TestObject manageAmbassadorialNetworkAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Ambassadorial Network_manage_entity_action']")
TestHelper.clickButton(manageAmbassadorialNetworkAction)

TestObject emptyStateObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no_ambassadorial_networks']")
boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)


if (emptyStateObjectPresent) {
	println("hasn't ambassadorial network")
	TestObject emptyStateAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-ambassadorial-network-add-new-organization-empty-state']")
	WebUI.click(emptyStateAction)
	AmbassadorialNetworkActions(organizationName);
} else {
	println("has ambassadorial network")
	TestObject addNetworkRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_ambassadorial_network_add_action']")
	TestHelper.clickButton(addNetworkRoleAction)
	AmbassadorialNetworkActions(organizationName);
}

def AmbassadorialNetworkActions(String organizationName) {
	addAmbassadorialNetwork(organizationName);
	checkAmbassadorialNetworkFeature(organizationName);
	WebUI.delay(2);
	WebUI.closeBrowser();
}


def addAmbassadorialNetwork(String organizationName) {
	
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='searchForOrganizationInput']"), organizationName)
	WebUI.delay(2);
	
	String ambassadorial_network_id = organizationName + '_filter_organization_action';
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='" + ambassadorial_network_id + "']"))
	
	WebElement linkAmbassadorialNetworkConfirmation = TestHelper.getItem('link_ambassadorial_network_confirmation', 0)
	linkAmbassadorialNetworkConfirmation.click();

	WebUI.delay(2);
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='manage_ambassadorial_network_search_input']"), organizationName)
	WebUI.delay(2);
	
	String manage_ambassadorial_page_organization_id = organizationName + '_ambassadorial_network';
	TestObject ambassadorialNetworkOrganization = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + manage_ambassadorial_page_organization_id + "']")
	boolean ambassadorialNetworkOrganizationPresent = TestHelper.isElementPresent(ambassadorialNetworkOrganization)
	
	if(!ambassadorialNetworkOrganizationPresent) {
		TestHelper.thrownException("Ambassadorial network not exist in embassy Ambassadorial network list");	
	}
}

def checkAmbassadorialNetworkFeature(String organizationName) {
	String ambassadorial_network_id = organizationName + '_manage_ambassadorial_network';
	TestObject ambassadorial_network_action = TestHelper.getObjectById("//div[@id='" + ambassadorial_network_id + "']");
	TestHelper.clickButton(ambassadorial_network_action);

	//Show Current Organization
		TestObject viewAmbassadorialNetworkAction = TestHelper.getObjectById("//a[@id='View Profile_user_action']")
		TestHelper.clickButton(viewAmbassadorialNetworkAction);
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='organization_back_button']"))
	
	// Feature ambassadorial network
		TestHelper.clickButton(ambassadorial_network_action);
		TestObject setAmbassadorialNetworkAsFeatureAction = TestHelper.getObjectById("//a[@id='Set As Primary_user_action']")
		TestHelper.clickButton(setAmbassadorialNetworkAsFeatureAction);
	
		String featured_ambassadorial_id = organizationName + '_ambassadorial_network_featured';
		TestObject featuredAmbassadorialNetworkObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + featured_ambassadorial_id + "']")
		boolean featuredAmbassadorialNetworkObjectPresent = TestHelper.isElementPresent(featuredAmbassadorialNetworkObject)
		
		if(!featuredAmbassadorialNetworkObjectPresent) {
			TestHelper.thrownException("Featured ambassadorial network not exist in embassy ambassadorial network list");
		}
		
	// Unfeature ambassadorial network
		TestHelper.clickButton(ambassadorial_network_action);
		TestObject unfeatureAmbassadorialNetworkAction = TestHelper.getObjectById("//a[@id='Remove From Primary_user_action']")
		TestHelper.clickButton(unfeatureAmbassadorialNetworkAction);
	
		WebUI.delay(3);
		String unFeatured_trade_office_id = organizationName + '_ambassadorial_network_featured';
		TestObject unfeaturedAmbassadorialNetworkObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + unFeatured_trade_office_id + "']")
		boolean unfeaturedAmbassadorialNetworkObjectPresent = TestHelper.isElementPresent(unfeaturedAmbassadorialNetworkObject)
		
		if(unfeaturedAmbassadorialNetworkObjectPresent) {
			TestHelper.thrownException("Featured ambassadorial still featured in embassy ambassadorial network list");
		}
		
	// Check ambassadorial network in diplomatic page
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_ambassadorial_network_back_button']"));
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-DiplomaticMissions-back-button']"));
		
		TestHelper.getObjectById("//div[@id='diplomatic_ambassadorial']")
		String main_ambassadorial_network_id = organizationName + '_main_ambassadorial_network';
		TestObject mainAmbassadorialNetworkObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + main_ambassadorial_network_id + "']")
		boolean mainAmbassadorialNetworkObjectPresent = TestHelper.isElementPresent(mainAmbassadorialNetworkObject)
		
		if(!mainAmbassadorialNetworkObjectPresent) {
			TestHelper.thrownException("Ambassadorial network not exist in the diplomatic page");
		}
	
	// Remove Ambassadorial network
		TestHelper.goToDiplomaticManageEntity();
		TestObject manageAmbassadorialNetworkAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Ambassadorial Network_manage_entity_action']")
		TestHelper.clickButton(manageAmbassadorialNetworkAction)
		
		TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='manage_ambassadorial_network_search_input']"), organizationName)
		WebUI.delay(2);
		
		TestHelper.clickButton(ambassadorial_network_action);
		TestObject removeAmbassadorialNetworkAction = TestHelper.getObjectById("//a[@id='Remove Organization_user_action']")
		TestHelper.clickButton(removeAmbassadorialNetworkAction);
		WebUI.delay(2);
		
		WebElement removeAmbassadorialNetworkConfirmation =  TestHelper.getItem('remove_ambassadorial_network_confirmation', 0)
		removeAmbassadorialNetworkConfirmation.click();
		WebUI.delay(4);
		
		String ambassadorial_network_page_company_id = organizationName + '_ambassadorial_network';
		TestObject ambassadorialNetworkOrganization = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + ambassadorial_network_page_company_id + "']")
		boolean ambassadorialNetworkOrganizationPresent = TestHelper.isElementPresent(ambassadorialNetworkOrganization)
		
		if(ambassadorialNetworkOrganizationPresent) {
			TestHelper.thrownException("Ambassadorial network not removed from ambassadorial network list");
		}
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_ambassadorial_network_back_button']"));
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-DiplomaticMissions-back-button']"));

		TestHelper.getObjectById("//div[@id='diplomatic_ambassadorial']")
		String deleted_main_ambassadorial_network_id = organizationName + '_main_ambassadorial_network';
		TestObject deletedMainAmbassadorialNetworkObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + deleted_main_ambassadorial_network_id + "']")
		boolean deletedMainAmbassadorialNetworkObjectPresent = TestHelper.isElementPresent(deletedMainAmbassadorialNetworkObject)
		
		if(deletedMainAmbassadorialNetworkObjectPresent) {
			TestHelper.thrownException("Ambassadorial network still exist in the diplomatic page");
		} else {
			println("Test Complete");
		}
}

