import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import org.openqa.selenium.WebElement as WebElement

long currentDate =  new Date().getTime();
String diplomaticName = 'Netways_Diplomatic' + currentDate;
String profileName;
String officeName = 'Netways_Diplomatic_Office_' + currentDate;
String newOfficeName = 'New_Netways_Diplomatic_Office_' + currentDate;

String roleName = 'Founder';

// Login and go to my organization
WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebElement profileNameObject = TestHelper.getItem('profile-name-title', 0)
profileName = profileNameObject.text;

WebUI.navigateToUrl(TestHelper.getEmbassyOfOman_EmbassyUrl());

 // Diplomatic info
WebElement diplomaticNameObject = TestHelper.getItem('diplomatic-name-title', 0)
diplomaticName = diplomaticNameObject.text;
  
TestHelper.goToDiplomaticManageEntity();
TestObject manageDiplomaticOfficeAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Support Staff_manage_entity_action']")
TestHelper.clickButton(manageDiplomaticOfficeAction)

// Check if user has offices
TestObject emptyStateObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-diplomatic-offices-empty-state']")
boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)


if (emptyStateObjectPresent) {
	println("hasn't offices")
	TestObject emptyStateAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-diplomatic-offices-empty-state-button']")
	WebUI.click(emptyStateAction)
	
	// add office
	addOffice(officeName);
	
	// join office
	joinOffice(officeName, roleName, profileName)
 
	// add office member
	addOfficeMember(officeName, profileName);
	
	// rename office
	renameOffice(newOfficeName);
	
	// check office in diplomatic page
	checkOfficeInDiplomaticPage(newOfficeName);

	// delete office
	deleteOffice(newOfficeName);
	 
} else {
	println("has offices")
	TestObject addCommitteeAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='add-new-office-action']")
	TestHelper.clickButton(addCommitteeAction)
	
	// add office
	addOffice(officeName);
	
	// join office
	joinOffice(officeName, roleName, profileName)
 
	// add office member
	addOfficeMember(officeName, profileName);
	
	// rename office
	renameOffice(newOfficeName);
	
	// check office in diplomatic page
	checkOfficeInDiplomaticPage(newOfficeName);

	// delete office
	deleteOffice(newOfficeName);
}


def addOffice(String officeName) {
	TestObject officeNameInput = TestHelper.getObjectById("//input[@id='office-name-input']");
	TestHelper.setInputValue(officeNameInput, officeName);
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='add-office-name-action']"));
	WebUI.delay(2);
	
	String officeId = officeName + '_diplomatic_office';
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='" + officeId +"']"));
}

def joinOffice(String officeName, String roleName, String profileName) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='join-office-member-empty-state-button']"));
	UserRoleHelper.joinDiplomaticOffice(roleName);

	WebUI.delay(3);
//	String committeeId = committeeName + '_org_committee';
//	TestHelper.clickButton("//div[@id='" + committeeId + "']");
	
	
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='search-diplomatic-member-input']"), profileName);
	WebUI.delay(2);
	
	// View Profile
		String userAction = profileName + '_office_member_action';
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='" + userAction + "']"));
		TestHelper.clickButton(TestHelper.getObjectById("//a[@id='View Profile_user_action']"));
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_backIcon_id']"));
			
	
	// Remove member
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='" + userAction + "']"));
		TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Leave Office_user_action']"));
		WebElement deleteMemberConfirmation = TestHelper.getItem('remove-office-member-confirmation', 0)
		WebUI.delay(1);
		deleteMemberConfirmation.click();
		WebUI.delay(1);
}

def addOfficeMember(String officeName, String profileName) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-diplomatic-member-action']"));
	TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Add Members_user_action']"));
	
	
	// Invite member
	TestHelper.clickButton(TestHelper.getObjectById("//ion-segment-button[@id='join_diplomatic_office_member_tab']"));
	
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='invite_office_member_first_name']"), 'Ali');
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='invite_office_member_last_name']"), 'Blaybel');
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='invite_office_member_email']"), 'blaybel.2010@gmail.com');
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='invite_office_member_action']"));
	
	// add member to committee
	TestHelper.clickButton(TestHelper.getObjectById("//ion-segment-button[@id='add_diplomatic_office_member_tab']"));
//	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='add-diplomatic-office-input-search']"), profileName);
//	WebUI.delay(2);
//		
//	String add_member_id = profileName + '_join_office';
//	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='" + add_member_id + "']"));
//	WebUI.delay(2);	
//	
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='add_diplomatic_office_back_action']"));
//	
//	String current_member_id = profileName + '_office_member_action';
//	boolean currentMemberExist = TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='" + current_member_id + "']"))	
//	
//	if(!currentMemberExist) {
//		TestHelper.thrownException("User not added as office member");
//	}
}

def renameOffice(String newCommitteeName) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-diplomatic-member-action']"));
	TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Rename Office_user_action']"));

	TestObject committeeNameInput = TestHelper.getObjectById("//input[@id='office-name-input']");
	TestHelper.setInputValue(committeeNameInput, newCommitteeName);
	TestHelper.clickButton(TestHelper.getObjectById("//button[@id='add-office-name-action']"));
	WebUI.delay(2);

	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_diplomatic_members_back_action']"));
	
	String new_office_id = newCommitteeName + '_diplomatic_office';
	boolean newOfficeExist = TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='" + new_office_id + "']"))
	
	if(!newOfficeExist) {
		TestHelper.thrownException("Office name not update successfully");
	}	
}

def checkOfficeInDiplomaticPage(String newOfficeName) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_office_back_action']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-DiplomaticMissions-back-button']"));
	
	TestObject officeSection = TestHelper.getObjectById("//div[@id='diplomatic_support_staff']");
	
	String new_office_id = newOfficeName + '_main_diplomatic_office';
	boolean newOfficeExist = TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='" + new_office_id + "']"))
	
	if(!newOfficeExist) {
		TestHelper.thrownException("Office not exist in the organization page");
	}
	
	TestHelper.goToDiplomaticManageEntity();
	TestObject manageDiplomaticOfficeAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Support Staff_manage_entity_action']")
	TestHelper.clickButton(manageDiplomaticOfficeAction)
	
}

def deleteOffice(String newCommitteeName) {
	String new_office_id = newCommitteeName + '_diplomatic_office';
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='" + new_office_id + "']"))

	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-diplomatic-member-action']"));
	TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Delete Office_user_action']"));
	WebUI.delay(2);
	
	WebElement removeOfficeConfirmation = TestHelper.getItem('remove-office-confirmation', 0)
	removeOfficeConfirmation.click();
	
	boolean newOfficeExist = TestHelper.isElementPresent(TestHelper.getObjectById("//div[@id='" + new_office_id + "']"))
	
	if(newOfficeExist) {
		TestHelper.thrownException("Office name not update successfully");
	} else {
		print("Test Complete");
		WebUI.closeBrowser();
	}
}
