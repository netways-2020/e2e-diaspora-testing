import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement



WebUI.callTestCase(findTestCase('Company/Founder or admin/Authorization for company/Login_For_Company'), [:], FailureHandling.STOP_ON_FAILURE)

String companyUrl = WebUI.getUrl();
WebElement companyNameObject = TestHelper.getItem('company-name-title', 0)
String companyName = companyNameObject.text;

WebUI.navigateToUrl(TestHelper.getEmbassyOfOman_EmbassyUrl());

TestHelper.goToDiplomaticManageEntity();
TestObject manageTradeOfficeAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Trade Office_manage_entity_action']")
TestHelper.clickButton(manageTradeOfficeAction)

TestObject emptyStateObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no_trade_offices']")
boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)


if (emptyStateObjectPresent) {
	println("hasn't trade offices")
	TestObject emptyStateAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-trade-office-add-new-company-empty-state']")
	WebUI.click(emptyStateAction)
	TradeOfficeActions(companyName);
} else {
	println("has trade offices")
	TestObject addBusinessRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_trade_office_add_action']")
	TestHelper.clickButton(addBusinessRoleAction)
	TradeOfficeActions(companyName);
}

def TradeOfficeActions(String companyName) {
	addTradeOffice(companyName);
	checkTradeOfficeFeature(companyName);
	WebUI.delay(2);
	WebUI.closeBrowser();
}


def addTradeOffice(String companyName) {
	
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='searchForCompanyInput']"), companyName)
	WebUI.delay(2);
	
	String trade_office_id = companyName + '_filter_company_action';
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='" + trade_office_id + "']"))
	
	WebElement linkTradeOfficeConfirmation = TestHelper.getItem('link_trade_office_confirmation', 0)
	linkTradeOfficeConfirmation.click();

	WebUI.delay(2);
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='manage_trade_office_search_input']"), companyName)
	WebUI.delay(2);
	
	String trade_office_page_company_id = companyName + '_trade_office';
	TestObject tradeOfficeCompany = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + trade_office_page_company_id + "']")
	boolean tradeOfficeCompanyPresent = TestHelper.isElementPresent(tradeOfficeCompany)
	
	if(!tradeOfficeCompanyPresent) {
		TestHelper.thrownException("Trade office not exist in embassy trade offices list");	
	}
}

def checkTradeOfficeFeature(String companyName) {
	String trade_office_id = companyName + '_manage_trade_office';
	TestObject trade_office_action = TestHelper.getObjectById("//div[@id='" + trade_office_id + "']");
	TestHelper.clickButton(trade_office_action);

	//Show Current Company
		TestObject viewTradeOfficeAction = TestHelper.getObjectById("//a[@id='View Profile_user_action']")
		TestHelper.clickButton(viewTradeOfficeAction);
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='company_back_button']"))
	
	// Feature trade office
		TestHelper.clickButton(trade_office_action);
		TestObject setTradeOfficeAsFeatureAction = TestHelper.getObjectById("//a[@id='Set As Primary_user_action']")
		TestHelper.clickButton(setTradeOfficeAsFeatureAction);
	
		String featured_trade_office_id = companyName + '_trade_office_featured';
		TestObject featuredTradeOfficeObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + featured_trade_office_id + "']")
		boolean featuredTradeOfficeObjectPresent = TestHelper.isElementPresent(featuredTradeOfficeObject)
		
		if(!featuredTradeOfficeObjectPresent) {
			TestHelper.thrownException("Featured trade office not exist in embassy trade offices list");
		}
		
	// Unfeature trade office
		TestHelper.clickButton(trade_office_action);
		TestObject removeFeaturedTradeOfficeAction = TestHelper.getObjectById("//a[@id='Remove From Primary_user_action']")
		TestHelper.clickButton(removeFeaturedTradeOfficeAction);
	
		WebUI.delay(3);
		String unFeatured_trade_office_id = companyName + '_trade_office_featured';
		TestObject unfeaturedTradeOfficeObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + unFeatured_trade_office_id + "']")
		boolean unfeaturedTradeOfficeObjectPresent = TestHelper.isElementPresent(unfeaturedTradeOfficeObject)
		
		if(unfeaturedTradeOfficeObjectPresent) {
			TestHelper.thrownException("Trade office still featured in embassy trade offices list");
		}
		
	// Check Trade Office in diplomatic page
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_trade_office_back_button']"));
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-DiplomaticMissions-back-button']"));
		
		TestHelper.getObjectById("//div[@id='diplomatic_trade_office']")
		String main_trade_office_id = companyName + '_main_trade_office';
		TestObject mainTradeOfficeObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + main_trade_office_id + "']")
		boolean mainTradeOfficeObjectPresent = TestHelper.isElementPresent(mainTradeOfficeObject)
		
		if(!mainTradeOfficeObjectPresent) {
			TestHelper.thrownException("Trade office not exist in the diplomatic page");
		}
	
	// Remove Trade office
		TestHelper.goToDiplomaticManageEntity();
		TestObject manageTradeOfficeAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Trade Office_manage_entity_action']")
		TestHelper.clickButton(manageTradeOfficeAction)
		
		TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='manage_trade_office_search_input']"), companyName)
		WebUI.delay(2);
		
		TestHelper.clickButton(trade_office_action);
		TestObject removeTradeOfficeAction = TestHelper.getObjectById("//a[@id='Remove Company_user_action']")
		TestHelper.clickButton(removeTradeOfficeAction);
		WebUI.delay(2);
		
		WebElement removeTradeOfficeConfirmation =  TestHelper.getItem('remove_trade_office_confirmation', 0)
		removeTradeOfficeConfirmation.click();
		WebUI.delay(4);
		
		String trade_office_page_company_id = companyName + '_trade_office';
		TestObject tradeOfficeCompany = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + trade_office_page_company_id + "']")
		boolean tradeOfficeCompanyPresent = TestHelper.isElementPresent(tradeOfficeCompany)
		
		if(tradeOfficeCompanyPresent) {
			TestHelper.thrownException("Trade office not removed from trade offices list");
		}
		
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_trade_office_back_button']"));
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-DiplomaticMissions-back-button']"));

		TestHelper.getObjectById("//div[@id='diplomatic_trade_office']")
		String deleted_main_trade_office_id = companyName + '_main_trade_office';
		TestObject deletedMainTradeOfficeObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + deleted_main_trade_office_id + "']")
		boolean deletedMainTradeOfficeObjectPresent = TestHelper.isElementPresent(deletedMainTradeOfficeObject)
		
		if(deletedMainTradeOfficeObjectPresent) {
			TestHelper.thrownException("Trade office still exist in the diplomatic page");
		} else {
			println("Test Complete");
		}
}

