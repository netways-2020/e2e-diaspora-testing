import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testobject.ConditionType

long date = new Date().getTime();
String registeredEmail = 'netways_user' + date + '@gmail.com'
String password = 'cvW8qx4B2o3F4VwP/kNsqA=='

String firstName = 'FirstName_' + date;
String lastName = 'LastName_' + date;
String fullName = firstName + ' ' + lastName;
String city = 'Hermel';
String roleName = 'Diaspora Member';

// Login and get organization url
WebUI.callTestCase(findTestCase('Diplomatic/President or Admin/Authorization for diplomatic/Login_For_Diplomatic'), [:], FailureHandling.STOP_ON_FAILURE)
String diplomaticUrl = WebUI.getUrl();

WebUI.navigateToUrl(TestHelper.getHomeUrl());

// Register new user
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manageProfileId']"))
TestObject logoutAction = TestHelper.getObjectById("//div[@id='logout-id']")
TestHelper.clickButton(logoutAction);
TestHelper.registerNewUser(registeredEmail, password, firstName, lastName, city, false);

// add diplomatic role
joinDiplomatic(diplomaticUrl, roleName);

// Check if exist in pending member
rejectMember(diplomaticUrl, fullName);

WebUI.delay(2);
WebUI.closeBrowser();

def joinDiplomatic(String diplomaticUrl, String roleName) {
	WebUI.navigateToUrl(diplomaticUrl);
	
	TestObject joinDiplomaticAction = TestHelper.getObjectById("//div[@id='join_diplomatic_section']")
	boolean joinDiplomaticActionExist = TestHelper.isElementPresent(joinDiplomaticAction);
	
	if(joinDiplomaticActionExist) {
		TestHelper.clickButton(joinDiplomaticAction);
		UserRoleHelper.joinDiplomatic(roleName);
		WebUI.delay(2);
		WebUI.navigateToUrl(TestHelper.getHomeUrl());
		TestObject manageProfilSection = TestHelper.getObjectById("//div[@id='manageProfileId']")
		TestHelper.clickButton(manageProfilSection);
		
		TestObject logoutAction = TestHelper.getObjectById("//div[@id='logout-id']")
		TestHelper.clickButton(logoutAction);
	} else {
		TestHelper.thrownException("User already has role in this diplomatic");
	}
}

def rejectMember(String diplomaticUrl, String memberName) {
	TestHelper.loginWithAutomationTestingUser();
	WebUI.navigateToUrl(diplomaticUrl);
	TestHelper.goToDiplomaticManageEntity();
	TestObject manageKeyStaffMembersAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Members & Joins_manage_entity_action']")
	TestHelper.clickButton(manageKeyStaffMembersAction);
	
	TestObject pendingMemberSearchInput = TestHelper.getObjectById("//input[@id='diplomatic_pending_member_search_input']")
	TestHelper.setInputValue(pendingMemberSearchInput, memberName)
	WebUI.delay(2);
	
	String pendingEmployeeId = memberName + '_pending_employee_name';
	String pendingEmployeeActionId = memberName + '_pending_employee_action';
	TestObject newMember = TestHelper.getObjectById("//div[@id='"+pendingEmployeeId+"']")
	boolean newMemberExist = TestHelper.isElementPresent(newMember);
	
	if(newMemberExist) {
		TestObject pendingMemberAction = TestHelper.getObjectById("//div[@id='"+pendingEmployeeActionId+"']")
		TestHelper.clickButton(pendingMemberAction);
		
		
		// Show Current Member Profile
		TestObject viewCurrentMemberAction = TestHelper.getObjectById("//a[@id='View Profile_user_action']")
		TestHelper.clickButton(viewCurrentMemberAction);
		TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_backIcon_id']"))
		
		// Reject Pending Member
		TestHelper.clickButton(pendingMemberAction);
		TestObject pendingMemberRemoveRequestAction = TestHelper.getObjectById("//a[@id='Reject Request_user_action']")
		TestHelper.clickButton(pendingMemberRemoveRequestAction);
		
		WebUI.delay(2)		
		
		boolean removedMemberExist = TestHelper.isElementPresent(newMember);
		if(!removedMemberExist) {
			println('Member has been rejected successfully');
		} else {
			TestHelper.thrownException("Error during rejecting new member");
		}
	} else {
		TestHelper.thrownException("No member with this name exist");
	}
}

