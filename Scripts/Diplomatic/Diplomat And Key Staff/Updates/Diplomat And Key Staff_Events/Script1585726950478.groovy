import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement

long date = new Date().getTime();
String registeredEmail = 'netways_user' + date + '@gmail.com'
String password = 'cvW8qx4B2o3F4VwP/kNsqA==';

String firstName = 'FirstName_' + date;
String lastName = 'LastName_' + date;
String fullName = firstName + ' ' + lastName;
String city = 'Hermel';
String roleName = 'Diplomats';

TestHelper.registerNewUser(registeredEmail, password, firstName, lastName, city, true);
WebUI.navigateToUrl(TestHelper.getEmabssyOfAssab_EmbassyUrl());

WebElement diplomaticNameObject = TestHelper.getItem('diplomatic-name-title', 0)
String diplomaticName = diplomaticNameObject.text;

TestHelper.clickButton(TestHelper.getObjectById("//div[@id='diplomaticPage_Join Diplomatic Mission_action_icon_Id']"));
UserRoleHelper.joinDiplomaticAsDiplomatOrKeyStaff(roleName)

WebUI.delay(3);

addNewEvent(diplomaticName);
WebUI.closeBrowser();

 
 
def addNewEvent(String diplomaticName) {
TestObject emptyStateDiplomaticUpdates = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-diplomatic-updates-empty-state']")
	if(!TestHelper.isElementPresent(emptyStateDiplomaticUpdates)) {
		TestHelper.thrownException("Update Empty State Not Exist");
	}
	
	TestHelper.clickButton(emptyStateDiplomaticUpdates);
	TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Create Event_user_action']"));
	
	long now = new Date().getTime();
	String eventTitle = 'New Event title add now_' + now;
	String eventLocation = 'New location add now_' + now;
	String eventDescription = 'New Event Descirption add now_' + now;
	
	UserActionsHelper.addEvent(eventTitle, eventDescription, eventLocation)
	
	TestHelper.verifyCardExist(diplomaticName, eventTitle, 'diplomtic', true)
	
	// Verify Card Features
	verifyCardFeature(eventTitle, eventDescription, eventLocation, diplomaticName);
}


def verifyCardFeature(String eventTitle, String eventDescription, String eventLocation, String diplomaticName) {
	TestObject manageCardObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='diplomatic_manage_updates']")
	boolean manageCardObjectExist = TestHelper.isElementPresent(manageCardObject);
	if(!manageCardObjectExist) {
		TestHelper.thrownException("Manage card button not exist");
	}
	
	TestHelper.clickButton(manageCardObject);
	String cartId = eventTitle + '_descriptionId';
	TestHelper.clickButton(TestHelper.getObjectById("//span[@id='" + cartId + "']"));
	
	String newEventTitle =  "new" + eventTitle;
	String newEventDescription =  "new" + eventDescription;
	String newEventLocation =  "new" + eventLocation;
	
	TestHelper.checkEventCardFeatures(eventTitle, eventDescription, eventLocation, newEventTitle, newEventDescription, newEventLocation, diplomaticName);
	

	WebUI.delay(3);
	String newCartId = newEventTitle + '_descriptionId';
	TestObject deleteCardObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='" + newCartId + "']")
	boolean deleteCardObjectExist = TestHelper.isElementPresent(deleteCardObject);
	
	if(deleteCardObjectExist) {
		TestHelper.thrownException("Card not deleted still exist in the card list");
	} else {
		checkCardRemovedFromMainPage(eventDescription);
	}
}

def checkCardRemovedFromMainPage(String cardDescriptionId) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back_button_cardListing']"))
	TestObject cardDescriptionObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+ cardDescriptionId  + "_id']")
	
	boolean cardDescriptionObjectExist = TestHelper.isElementPresent(cardDescriptionObject)

	if(cardDescriptionObjectExist) {
		TestHelper.thrownException("Card not deleted from main page")
	} else {
		println("Test Complete");
		WebUI.closeBrowser();
	}

}






