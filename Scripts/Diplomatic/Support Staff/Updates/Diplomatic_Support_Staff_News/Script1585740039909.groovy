import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement



long date = new Date().getTime();
String registeredEmail = 'netways_user' + date + '@gmail.com'
String password = 'cvW8qx4B2o3F4VwP/kNsqA==';

String firstName = 'FirstName_' + date;
String lastName = 'LastName_' + date;
String fullName = firstName + ' ' + lastName;
String city = 'Hermel';
String roleName = 'SupportStaffRole';
String officeName = "Blaybel_Office";

TestHelper.registerNewUser(registeredEmail, password, firstName, lastName, city, true);
WebUI.navigateToUrl(TestHelper.getEmabssyOfAssab_EmbassyUrl());

WebElement diplomaticNameObject = TestHelper.getItem('diplomatic-name-title', 0)
String diplomaticName = diplomaticNameObject.text;

TestHelper.clickButton(TestHelper.getObjectById("//div[@id='diplomaticPage_Join Diplomatic Mission_action_icon_Id']"));
UserRoleHelper.joinDiplomaticAsSupportStaff(roleName, officeName);

WebUI.delay(3);

addNewNews(diplomaticName);
WebUI.closeBrowser();

 
def addNewNews(String diplomaticName) {
	TestObject emptyStateDiplomaticUpdates = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-diplomatic-updates-empty-state']")
	if(!TestHelper.isElementPresent(emptyStateDiplomaticUpdates)) {
		TestHelper.thrownException("Update Empty State Not Exist");
	}
	
	TestHelper.clickButton(emptyStateDiplomaticUpdates);
	TestHelper.clickButton(TestHelper.getObjectById("//a[@id='Create Post_user_action']"));
	
	long now = new Date().getTime();
	String postTitle = 'New post title add now_' + now;
	String postDescription = 'New post description add now_' + now;
	
	UserActionsHelper.addPost(postTitle, postDescription)
	WebUI.delay(2);
	TestHelper.verifyCardExist(diplomaticName, postTitle, 'diplomtic', true);
	
	// Verify Card Features
	verifyCardFeature(postTitle, postDescription, diplomaticName);
}
 
def verifyCardFeature(String postTitle, String postDescription, String diplomaticName) {
	TestObject manageCardObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='diplomatic_manage_updates']")
	boolean manageCardObjectExist = TestHelper.isElementPresent(manageCardObject);
	if(!manageCardObjectExist) {
		TestHelper.thrownException("Manage card button not exist");
	}
	
	TestHelper.clickButton(manageCardObject);
	String cartId = postTitle + '_descriptionId';
	TestHelper.clickButton(TestHelper.getObjectById("//span[@id='" + cartId + "']"));
	
	String newPostTitle =  "new " + postTitle;
	String newPostDescription =  "new " + postDescription;
	
	TestHelper.checkPostCardFeatures(postTitle, postDescription, newPostTitle, newPostDescription, diplomaticName);
	

	WebUI.delay(3);
//	String newCartId = newPostTitle + '_descriptionId';
//	TestObject deleteCardObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='" + newCartId + "']")
//	boolean deleteCardObjectExist = TestHelper.isElementPresent(deleteCardObject);
//	
//	if(deleteCardObjectExist) {
//		TestHelper.thrownException("Card not deleted still exist in the card list");
//	} else {
		checkCardRemovedFromMainPage(postDescription);
//	}
}

def checkCardRemovedFromMainPage(String cardDescriptionId) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='back_button_cardListing']"))
	TestObject cardDescriptionObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='"+ cardDescriptionId  + "_id']")
	
	boolean cardDescriptionObjectExist = TestHelper.isElementPresent(cardDescriptionObject)

	if(cardDescriptionObjectExist) {
		TestHelper.thrownException("Card not deleted from main page")
	} else {
		println("Test Complete");
		WebUI.closeBrowser();
	}

}



