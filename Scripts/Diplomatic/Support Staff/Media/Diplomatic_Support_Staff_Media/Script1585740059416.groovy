import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import org.junit.After

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
// Register new user and go to embassy
long date = new Date().getTime();
String registeredEmail = 'netways_user' + date + '@gmail.com'
String password = 'cvW8qx4B2o3F4VwP/kNsqA==';

String firstName = 'FirstName_' + date;
String lastName = 'LastName_' + date;
String fullName = firstName + ' ' + lastName;
String city = 'Hermel';
String roleName = 'SupportStaffRole';
String officeName = "Blaybel_Office";
String imageCaption = 'caption' + date;

TestHelper.registerNewUser(registeredEmail, password, firstName, lastName, city, true);
WebUI.navigateToUrl(TestHelper.getEmabssyOfAssab_EmbassyUrl());

TestHelper.clickButton(TestHelper.getObjectById("//div[@id='diplomaticPage_Join Diplomatic Mission_action_icon_Id']"));
UserRoleHelper.joinDiplomaticAsSupportStaff(roleName, officeName);

WebUI.delay(3);

TestHelper.getObjectById("//div[@id='diplomatic_media']")
TestObject emptyStateDiplomaticGallery = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-diplomatic-media-empty-state']")
WebUI.click(emptyStateDiplomaticGallery);
UserActionsHelper.addMedia(imageCaption);	

TestObject addPhotoAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='diplomatic_manage_media']")
WebUI.click(addPhotoAction)

checkIfImageAddedSuccessfully(true, 0); 
removeImage();

 	
def removeImage() { 
	int imageCountBeforAdd = TestHelper.countElementByClassName('gallery-container');
	WebElement element = TestHelper.getItem('gallery-container', 0);
	element.click();

	TestHelper.checkImageLoaded("image_detail_id");
	
	TestObject imageCaption = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='image-caption-id']")
	boolean imageCaptionExist = TestHelper.isElementPresent(imageCaption);

	if(imageCaptionExist) {
		print("Image caption exist");
	} else {
		TestHelper.thrownException("Image caption not exist");
	}
	
	TestObject removeAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='galler_image_id']")
	boolean removeActionExist = TestHelper.isElementPresent(removeAction);
 
	if(removeActionExist) {
		TestHelper.clickButton(removeAction)
		UserActionsHelper.removeMedia();
		this.checkIfImageRemovedSuccessfully(imageCountBeforAdd);
	} else {
		TestHelper.thrownException("Doesn't have the privilege tot delete an image");
	}	
}


def checkIfImageAddedSuccessfully(boolean firstImageAdded, int imageCountBeforAdd) {
	int imageCount = TestHelper.countElementByClassName('gallery-container');
	 
	if(firstImageAdded) {
		if(imageCount == 1) {
			println("Image added successfully");
		} else {
			TestHelper.thrownException("Image not added successfully");
		}
	} else { 
	   if((imageCountBeforAdd + 1) == imageCount) {
		   println("Image added successfully");
	   } else {
		   TestHelper.thrownException("Image not added successfully");
	   }
	}
}


def checkIfImageRemovedSuccessfully(int imageCountBeforAdd) {
	int imageCount = TestHelper.countElementByClassName('gallery-container');
	
	if((imageCountBeforAdd - 1) == imageCount) {
	   println("Image deleted successfully");
	   WebUI.closeBrowser();
   } else {
	   TestHelper.thrownException("Image not deleted successfully");
   } 
}
 