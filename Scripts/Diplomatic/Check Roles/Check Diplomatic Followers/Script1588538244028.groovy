import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

// Register new user and go to embassy
long date = new Date().getTime();
String registeredEmail = 'netways_user' + date + '@gmail.com'
String password = 'cvW8qx4B2o3F4VwP/kNsqA==';

String firstName = 'FirstName_' + date;
String lastName = 'LastName_' + date;
String fullName = firstName + ' ' + lastName;
String city = 'Hermel';


String memberRoleName = 'Diaspora Member';
 
TestHelper.registerNewUser(registeredEmail, password, firstName, lastName, city, true);
WebUI.navigateToUrl(TestHelper.getEmabssyOfAssab_EmbassyUrl());
WebUI.delay(2);

checkMemberRole(memberRoleName);
checkFollowers(fullName)

WebUI.delay(2);
WebUI.closeBrowser();


def checkMemberRole(String memberRoleName) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='diplomaticPage_Join Diplomatic Mission_action_icon_Id']"));
	UserRoleHelper.joinDiplomatic(memberRoleName);
	WebUI.delay(3);
}

def checkFollowers(String fullName) {
	TestObject followers_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='follower_count_id_tag_label']")
	if(!TestHelper.isElementPresent(followers_label_object)) {
		TestHelper.thrownException("Followers Label not present");
	}
	
	TestHelper.clickButton(followers_label_object);
	
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='search-diplomatic-follower-input']"), fullName);
	WebUI.delay(2);
	String followerId = fullName + '_diplomatic_followers';
	TestObject followerObject = TestHelper.getObjectById("//div[@id='" + followerId + "']");
		
	if(!TestHelper.isElementPresent(followerObject)) {
		TestHelper.thrownException("Follower not exist");
	} else {
		print("Check Complete")
	}
}


