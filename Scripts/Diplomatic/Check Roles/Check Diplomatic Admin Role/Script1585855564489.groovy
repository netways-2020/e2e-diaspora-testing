import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement

// Register new user and go to embassy
long date = new Date().getTime();
String registeredEmail = 'netways_user' + date + '@gmail.com'
String password = 'cvW8qx4B2o3F4VwP/kNsqA==';

String firstName = 'FirstName_' + date;
String lastName = 'LastName_' + date;
String memberName = firstName + ' ' + lastName;
String city = 'Hermel';


String memberRoleName = 'Diaspora Member';
 
TestHelper.registerNewUser(registeredEmail, password, firstName, lastName, city, true);
WebUI.navigateToUrl(TestHelper.getEmabssyOfAssab_EmbassyUrl());
WebUI.delay(2);

WebElement diplomaticNameObject = TestHelper.getItem('diplomatic-name-title', 0)
String embassyName = diplomaticNameObject.text;

// Member Role
checkMemberRole(memberRoleName);
checkNonPresidentEmptyState();
acceptMemberRole(memberName);
addMemberToEmbassyAdmin(memberName);
checkAdmin(memberName)

loginWithNewUserAndCheckAdminSection(registeredEmail, password, embassyName, memberRoleName)
checkPresidentSections();
 
WebUI.delay(2);
WebUI.closeBrowser();

def checkMemberRole(String memberRoleName) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='diplomaticPage_Join Diplomatic Mission_action_icon_Id']"));
	UserRoleHelper.joinDiplomatic(memberRoleName);
	WebUI.delay(3);

}

// Accept Member Role
def acceptMemberRole(String memberName) {
	WebUI.navigateToUrl(TestHelper.getHomeUrl());
	WebUI.delay(2);
	
	TestObject manageProfilSection = TestHelper.getObjectById("//div[@id='manageProfileId']")
	TestHelper.clickButton(manageProfilSection);
	TestObject logoutAction = TestHelper.getObjectById("//div[@id='logout-id']")
	TestHelper.clickButton(logoutAction);
	TestHelper.loginWithAutomationTestingUser();
	
	WebUI.navigateToUrl(TestHelper.getEmabssyOfAssab_EmbassyUrl());
	WebUI.delay(2);

	TestHelper.goToDiplomaticManageEntity();
	TestObject manageKeyStaffMembersAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Members & Joins_manage_entity_action']")
	TestHelper.clickButton(manageKeyStaffMembersAction);
	
	TestObject pendingMemberSearchInput = TestHelper.getObjectById("//input[@id='diplomatic_pending_member_search_input']")
	TestHelper.setInputValue(pendingMemberSearchInput, memberName)
	WebUI.delay(2);
	
	String pendingEmployeeId = memberName + '_pending_employee_name';
	String pendingEmployeeActionId = memberName + '_pending_employee_action';
	
	String currentEmployeeId = memberName + '_current_employee_name';
	
	TestObject newMember = TestHelper.getObjectById("//div[@id='"+pendingEmployeeId+"']")
	
	boolean newMemberExist = TestHelper.isElementPresent(newMember);
	if(newMemberExist) {
		TestObject pendingMemberAction = TestHelper.getObjectById("//div[@id='"+pendingEmployeeActionId+"']")
		TestHelper.clickButton(pendingMemberAction);
		
		TestObject pendingMemberApproveRequestAction = TestHelper.getObjectById("//a[@id='Approve Request_user_action']")
		TestHelper.clickButton(pendingMemberApproveRequestAction);
				
		TestObject currentEmployeeTab = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//ion-segment-button[@id='diplomatic_approved_employee_tab_id']")
		TestHelper.clickButton(currentEmployeeTab);

		TestObject currentMemberSearchInput = TestHelper.getObjectById("//input[@id='diplomatic_current_member_search_input']")
		TestHelper.setInputValue(currentMemberSearchInput, memberName)
		WebUI.delay(2);
		
		TestObject currentMember = TestHelper.getObjectById("//div[@id='"+currentEmployeeId+"']")
		boolean currentMemberExist = TestHelper.isElementPresent(currentMember);
		if(currentMemberExist) {
			println('User has been accepted has employee successfully');
		} else {
			TestHelper.thrownException("Error during accepting new member");
		}
	} else {
		TestHelper.thrownException("No employee with this name exist");
	}
}

def addMemberToEmbassyAdmin(String memberName) {
	TestObject manageOrganizationMembersBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-diplomatic-key-staff-back-button']")
	TestHelper.clickButton(manageOrganizationMembersBackButton);

	TestObject manageAdminMemberAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Page Administrators_manage_entity_action']")
	TestHelper.clickButton(manageAdminMemberAction);
	
	// Check empty state add admin
	TestObject manage_admin_add_admin = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-admin-diplomatic-add-new-admin-empty-state']")
	boolean checkManage_admin_admin = TestHelper.isElementPresent(manage_admin_add_admin);
	
	if(checkManage_admin_admin) {
		TestHelper.clickButton(manage_admin_add_admin)
		assignMemberAsAdmin(memberName);
	} else {
		TestObject manage_admin_add_admin_from_header = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage-admin-diplomatic-add-new-admin']")
		TestHelper.clickButton(manage_admin_add_admin_from_header)
		assignMemberAsAdmin(memberName);
	}
}

def assignMemberAsAdmin(String memberName) {
	WebUI.delay(1);
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='assign-diplomatic-admin-search-input']"), memberName);
	WebUI.delay(2);
	
	String memberId = memberName + '_assign_admin_role_action';
	TestObject memberAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + memberId + "']")
	TestHelper.clickButton(memberAction);

	TestObject assignAdminMemberAction = TestHelper.getObjectById("//a[@id='Assign As Administrator_user_action']")
	TestHelper.clickButton(assignAdminMemberAction);
	WebUI.delay(2);
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='add-admin-diplomatic-back-button']"))
}

def checkAdmin(String memberName) {
	
	TestHelper.setInputValue(TestHelper.getObjectById("//input[@id='manage-diplomatic-admin-search-input']"), memberName);
	WebUI.delay(2);
	
	String memberId = memberName + '_admin_role';
	TestObject memberObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='" + memberId + "']")
	boolean checkAdminExist = TestHelper.isElementPresent(memberObject);
	
	if(checkAdminExist) {
		println("Memeber assign as admin succesfully");
	} else {
		TestHelper.thrownException("Error during assing member as admin");
	}
}

def loginWithNewUserAndCheckAdminSection(String email, String password, String embassyName, String presidentRoleName) {
	WebUI.closeBrowser();
	
	WebUI.delay(3);
	TestHelper.loginWithSpecificUserNameAndPassword(email, password, true)
	
	TestHelper.goToManageEntity()
	TestObject manageNetworkRoleAction = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'Network Roles_manage_entity_action\']')
	TestHelper.clickButton(manageNetworkRoleAction)
		
	String presidentEmbassyTarget = embassyName + '-' + presidentRoleName + '-role'
	TestObject selectedPresidentRole = TestHelper.getObjectById("//div[@id='"+presidentEmbassyTarget+"']")
	TestHelper.clickButton(selectedPresidentRole);
	
	WebUI.delay(2);
}



// Check President Section
def checkPresidentSections() {
	checkCommonHeaderElementExist();
	checkDiplomaticSectionsEmptyState();
	checkDiplomaticTitleAndSubTitle();
}

def checkCommonHeaderElementExist() {
	boolean successEmptyFields = true;
	
	TestObject diplomaticMenuAction_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='diplomatic_menu_action_id']")
	TestObject manage_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Manage_DiplomaticMissions_entity_action']")
	TestObject invite_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Invite_DiplomaticMissions_entity_action']")
	
	TestObject followers_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='follower_count_id_tag_label']")
	TestObject leadership_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='diplomats_id_tag_label']")

	if(!TestHelper.isElementPresent(followers_label_object)) {
		TestHelper.thrownException("Followers Label not present");
	} else if(!TestHelper.isElementPresent(leadership_label_object)) {
		TestHelper.thrownException("Leadership Label not present");
	} else if(!TestHelper.isElementPresent(diplomaticMenuAction_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(manage_entity_object)) {
		successEmptyFields = false;
	} else if(!TestHelper.isElementPresent(invite_entity_object)) {
		successEmptyFields = false;
	}
	
	if(successEmptyFields) {
		println("Diplomatic common header info exist");
	} else {
			TestHelper.thrownException("Error: Diplomatic commone header info")
	}
}

def checkDiplomaticSectionsEmptyState() {

	String errorMessage = "Error: Organization section empty state exist";
	
	// About Section
		TestHelper.checkObjectExit("//div[@id='diplomatic_about']", errorMessage);
		//	TestObject diplomatic_about_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='diplomatic_about']")
		
		
	// Diplomats
		TestHelper.checkObjectExit("//div[@id='diplomatic_diplomats']", errorMessage);
		//	TestObject diplomatic_diplomats_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='diplomatic_diplomats']")

	// Updates
		TestHelper.checkObjectExit("//div[@id='diplomatic_updates']", errorMessage);
		TestHelper.checkObjectText("//div[@id='no-diplomatic-updates-empty-state-title']", "Notify the Lebanese community of important news and events" , errorMessage);
		TestHelper.checkObjectText("//div[@id='no-diplomatic-updates-empty-state']", "Post News or Events" , errorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no-diplomatic-updates-empty-state-image']", "assets/icons/emptyState/Post_Update.svg", errorMessage)
		
		//	TestObject diplomatic_updates_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='diplomatic_updates']")
		//	TestObject diplomatic_updates_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-diplomatic-updates-empty-state']")

	// Ambassadorial
		TestHelper.checkObjectExit("//div[@id='diplomatic_ambassadorial']", errorMessage);
		TestHelper.checkObjectText("//div[@id='no-diplomatic-ambassadorial-empty-state-title']", "Show and link to organizations supporting your mission" , errorMessage);
		TestHelper.checkObjectText("//div[@id='no-diplomatic-ambassadorial-empty-state']", "Add Supporting Organizations" , errorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no-diplomatic-ambassadorial-empty-state-image']", "assets/icons/diasporaIcon/Ambassadorial_Network_green.svg", errorMessage)

		//	TestObject diplomatic_ambassadorial_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='diplomatic_ambassadorial']")
		//	TestObject diplomatic_ambassadorial_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-diplomatic-ambassadorial-empty-state']")

	// Support Staff
//		TestHelper.checkObjectExit("//div[@id='diplomatic_support_staff']", errorMessage);
//		TestHelper.checkObjectText("//div[@id='no-diplomatic-support-staff-empty-state-title']", "Create support staff offices. Assign users and manage their roles" , errorMessage);
//		TestHelper.checkObjectText("//div[@id='no-diplomatic-support-staff-empty-state']", "Create a Support Office" , errorMessage);
//		TestHelper.checkObjectImageSrc("//div[@id='no-diplomatic-support-staff-empty-state-image']", "assets/icons/diasporaIcon/Support_Staff.svg", errorMessage)

		//	TestObject diplomatic_support_staff_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='diplomatic_support_staff']")
		//	TestObject diplomatic_support_staff_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-diplomatic-support-staff-empty-state']")
	
	// Trade Office
		TestHelper.checkObjectExit("//div[@id='diplomatic_trade_office']", errorMessage);
		TestHelper.checkObjectText("//div[@id='no-diplomatic-trade-office-empty-state-title']", "Promote business with Lebanese companies in your community" , errorMessage);
		TestHelper.checkObjectText("//div[@id='no-diplomatic-trade-office-empty-state']", "Add Lebanese Companies" , errorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no-diplomatic-trade-office-empty-state-image']", "assets/icons/diasporaIcon/Trade_Office.svg", errorMessage)

		//	TestObject diplomatic_trade_office_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='diplomatic_trade_office']")
		//	TestObject diplomatic_trade_office_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-diplomatic-trade-office-empty-state']")
	
	// Media
		TestHelper.checkObjectExit("//div[@id='diplomatic_media']", errorMessage);
		TestHelper.checkObjectText("//div[@id='no-diplomatic-media-empty-state-title']", "Show sights, achievements or projects with images and videos" , errorMessage);
		TestHelper.checkObjectText("//div[@id='no-diplomatic-media-empty-state']", "Add Photos or Videos" , errorMessage);
		TestHelper.checkObjectImageSrc("//div[@id='no-diplomatic-media-empty-state-image']", "assets/icons/emptyState/Media_Gallery.svg", errorMessage)

		//	TestObject diplomatic_media_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='diplomatic_media']")
		//	TestObject diplomatic_media_empty_state_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='no-diplomatic-media-empty-state']")
	 
		println("Diplomatic section empty state exist");
}

def checkDiplomaticTitleAndSubTitle() {
	
	TestObject about_title_object = TestHelper.getObjectById("//div[@id='diplomatic_about_title_id']")
	TestObject about_subTitle_object = TestHelper.getObjectById("//div[@id='diplomatic_about_subTitle_id']")
	TestObject about_icon_object = TestHelper.getObjectById("//div[@id='diplomatic_about_title_id_icon']")
	
	if(TestHelper.isElementPresent(about_title_object) && TestHelper.isElementPresent(about_subTitle_object)) {
		String aboutTitle = WebUI.getText(about_title_object);
		String aboutSubTitle = WebUI.getText(about_subTitle_object);
		String aboutIcon = WebUI.getText(about_icon_object);
		
		if(!aboutTitle.equals("About") || !aboutSubTitle.equals("Information & contact details") || !aboutIcon.equals("assets/icons/diasporaIcon/About_Active.svg")) {
			TestHelper.thrownException("Invalid about title and subtitle")
		}
	}

 
	TestObject members_title_object = TestHelper.getObjectById("//div[@id='diplomatic_people_title_id']")
	TestObject members_subTitle_object = TestHelper.getObjectById("//div[@id='diplomatic_people_subTitle_id']")
	TestObject members_icon_object = TestHelper.getObjectById("//div[@id='diplomatic_people_title_id_icon']")
	
	if(TestHelper.isElementPresent(members_title_object) && TestHelper.isElementPresent(members_subTitle_object)) {
		String membersTitle = WebUI.getText(members_title_object);
		String membersSubTitle = WebUI.getText(members_subTitle_object);
		String membersIcon = WebUI.getText(members_icon_object);
		
		if(!membersTitle.equals("People") || !membersSubTitle.equals("Diaspora members registered") || !membersIcon.equals("assets/icons/diasporaIcon/People_Active.svg")) {
			TestHelper.thrownException("Invalid members title and subtitle")
		}
	}

	
	TestObject diplomats_title_object = TestHelper.getObjectById("//div[@id='diplomatic_diplomats_title_id']")
	TestObject diplomats_subTitle_object = TestHelper.getObjectById("//div[@id='diplomatic_diplomats_subTitle_id']")
	TestObject diplomats_icon_object = TestHelper.getObjectById("//div[@id='diplomatic_diplomats_title_id_icon']")
	
	if(TestHelper.isElementPresent(diplomats_title_object) && TestHelper.isElementPresent(diplomats_subTitle_object)) {
		String leadershipsTitle = WebUI.getText(diplomats_title_object);
		String leadershipsSubTitle = WebUI.getText(diplomats_subTitle_object);
		String leadershipsIcon = WebUI.getText(diplomats_icon_object);
		
		if(!leadershipsTitle.equals("Diplomats & Key Staff") || !leadershipsSubTitle.equals("Ambassadors & consuls") || !leadershipsIcon.equals("assets/icons/diasporaIcon/vip_badge.svg")) {
			TestHelper.thrownException("Invalid diplomats title and subtitle")
		}
	}
	
	TestObject updates_title_object = TestHelper.getObjectById("//div[@id='diplomatic_updates_title_id']")
	TestObject updates_subTitle_object = TestHelper.getObjectById("//div[@id='diplomatic_updates_subTitle_id']")
	TestObject updates_icon_object = TestHelper.getObjectById("//div[@id='diplomatic_updates_title_id_icon']")
	
	if(TestHelper.isElementPresent(updates_title_object) && TestHelper.isElementPresent(updates_subTitle_object)) {
		String updatesTitle = WebUI.getText(updates_title_object);
		String updatesSubTitle = WebUI.getText(updates_subTitle_object);
		String updatesIcon = WebUI.getText(updates_icon_object);
		
		if(!updatesTitle.equals("Updates") || !updatesSubTitle.equals("Posts & discussions") || !updatesIcon.equals("assets/icons/diasporaIcon/News_Active.svg")) {
			TestHelper.thrownException("Invalid updates title and subtitle")
		}

	}

	
	TestObject ambassadorial_network_title_object = TestHelper.getObjectById("//div[@id='diplomatic_ambassadorial_network_title_id']")
	TestObject ambassadorial_network_subTitle_object = TestHelper.getObjectById("//div[@id='diplomatic_ambassadorial_network_subTitle_id']")
	TestObject ambassadorial_network_icon_object = TestHelper.getObjectById("//div[@id='diplomatic_ambassadorial_network_title_id_icon']")
	
	if(TestHelper.isElementPresent(ambassadorial_network_title_object) && TestHelper.isElementPresent(ambassadorial_network_subTitle_object)) {
		String ambassadorialNetworkTitle = WebUI.getText(ambassadorial_network_title_object);
		String ambassadorialNetworkSubTitle = WebUI.getText(ambassadorial_network_subTitle_object);
		String ambassadorialNetworkIcon = WebUI.getText(ambassadorial_network_icon_object);
		
		if(!ambassadorialNetworkTitle.equals("Ambassadorial Network") || !ambassadorialNetworkSubTitle.equals("Helping the mission") || !ambassadorialNetworkIcon.equals("assets/icons/diasporaIcon/Ambassadorial_Network.svg")) {
			TestHelper.thrownException("Invalid Ambassadorial Network title and subtitle")
		}
	}

	TestObject supportStaff_title_object = TestHelper.getObjectById("//div[@id='diplomatic_support_staff_title_id']")
	TestObject supportStaff_subTitle_object = TestHelper.getObjectById("//div[@id='diplomatic_support_staff_subTitle_id']")
	TestObject supportStaff_icon_object = TestHelper.getObjectById("//div[@id='diplomatic_support_staff_title_id_icon']")
	
	if(TestHelper.isElementPresent(supportStaff_title_object) && TestHelper.isElementPresent(supportStaff_subTitle_object)) {
		String supportStaffTitle = WebUI.getText(supportStaff_title_object);
		String supportStaffSubTitle = WebUI.getText(supportStaff_subTitle_object);
		String supportStaffIcon = WebUI.getText(supportStaff_icon_object);
		
		if(!supportStaffTitle.equals("Support Staff") || !supportStaffSubTitle.equals("Other diplomatic staff") || !supportStaffIcon.equals("assets/icons/diasporaIcon/committee.svg")) {
			TestHelper.thrownException("Invalid support staff title and subtitle")
		}
	}

	
	TestObject tradeOffice_title_object = TestHelper.getObjectById("//div[@id='diplomatic_trade_office_title_id']")
	TestObject tradeOffice_subTitle_object = TestHelper.getObjectById("//div[@id='diplomatic_trade_office_subTitle_id']")
	TestObject tradeOffice_icon_object = TestHelper.getObjectById("//div[@id='diplomatic_trade_office_title_id_icon']")
	
	if(TestHelper.isElementPresent(tradeOffice_title_object) && TestHelper.isElementPresent(tradeOffice_subTitle_object)) {
		String tradeOfficeTitle = WebUI.getText(tradeOffice_title_object);
		String tradeOfficeSubTitle = WebUI.getText(tradeOffice_subTitle_object);
		String tradeOfficeIcon = WebUI.getText(tradeOffice_icon_object);
		
		if(!tradeOfficeTitle.equals("Trade Office") || !tradeOfficeSubTitle.equals("Lebanese-owned & vetted companies") || !tradeOfficeIcon.equals("assets/icons/diasporaIcon/affiliation.svg")) {
			TestHelper.thrownException("Invalid trade office title and subtitle")
		}
	}


	TestObject media_title_object = TestHelper.getObjectById("//div[@id='diplomatic_media_title_id']")
	TestObject media_subTitle_object = TestHelper.getObjectById("//div[@id='diplomatic_media_subTitle_id']")
	TestObject media_icon_object = TestHelper.getObjectById("//div[@id='diplomatic_media_title_id_icon']")
	
	if(TestHelper.isElementPresent(media_title_object) && TestHelper.isElementPresent(media_subTitle_object)) {
		String mediaTitle = WebUI.getText(media_title_object);
		String mediaSubTitle = WebUI.getText(media_subTitle_object);
		String mediaIcon = WebUI.getText(media_icon_object);

		if(!mediaTitle.equals("Media Gallery") || !mediaSubTitle.equals("Photos & videos") || !mediaIcon.equals("assets/icons/diasporaIcon/View_Gallery.svg")) {
			TestHelper.thrownException("Invalid media title and subtitle")
		}
	}
}

// check Non President Section
def checkNonPresidentEmptyState() {
	String embassyMediaEmptyStateErrorMessage = "Error: Embassy media empty state exist";
	
	TestObject diplomaticMenuAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='diplomatic_menu_action_id']")
	TestObject message_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Message_DiplomaticMissions_entity_action']")
	TestObject following_entity_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//div[@id='Following_DiplomaticMissions_entity_action']")
	  
	TestObject followers_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='follower_count_id_tag_label']")
	TestObject leadership_label_object = new TestObject().addProperty("xpath", ConditionType.EQUALS,"//span[@id='diplomats_id_tag_label']")

	
	if(!TestHelper.isElementPresent(followers_label_object)) {
		TestHelper.thrownException("Followers Label not present");
	} else if(!TestHelper.isElementPresent(leadership_label_object)) {
		TestHelper.thrownException("Leadership Label not present");
	} else if(!TestHelper.isElementPresent(following_entity_object)) {
		TestHelper.thrownException("Following Action not present");
	} else if(!TestHelper.isElementPresent(message_entity_object)) {
		TestHelper.thrownException("Message Action not present");
	}
	
	TestHelper.checkObjectExit("//div[@id='diplomatic_media']", embassyMediaEmptyStateErrorMessage);
	TestHelper.checkObjectText("//div[@id='no-diplomatic-media-empty-state-title']", "Show sights, achievements or projects with images and videos" , embassyMediaEmptyStateErrorMessage);
	TestHelper.checkObjectText("//div[@id='no-diplomatic-media-empty-state']", "Add Photos or Videos" , embassyMediaEmptyStateErrorMessage);
	TestHelper.checkObjectImageSrc("//div[@id='no-diplomatic-media-empty-state-image']", "assets/icons/emptyState/Media_Gallery.svg", embassyMediaEmptyStateErrorMessage)

	if(TestHelper.isElementPresent(diplomaticMenuAction)) {
		TestHelper.clickButton(diplomaticMenuAction);

		TestObject reportOrganizationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Report A Problem_user_action']")
		TestObject shareOrganizationAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[@id='Share_user_action']")
		
		if(!TestHelper.isElementPresent(reportOrganizationAction) || !TestHelper.isElementPresent(shareOrganizationAction)) {
			TestHelper.thrownException("Diplomatic Action not present");
		} else {
			print("Check Complete")
		}
	} else {
		TestHelper.thrownException("Menu Action not present");
	}
}

