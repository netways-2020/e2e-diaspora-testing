import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Profile/My Profile/Authorization for profile/Login'), [:], FailureHandling.STOP_ON_FAILURE)
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_notification']"));
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_notifications_action']"));

WebUI.delay(2)
boolean notificationEnabled = checkIfNotificationEnabled();
boolean emailEnabled = checkIfEmailEnabled();
 
changeNotificationSettings(notificationEnabled, emailEnabled);
WebUI.delay(2)

boolean newNotificationEnabled = checkIfNotificationEnabled();
boolean newEmailEnabled = checkIfEmailEnabled();

TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_notifications_save_action']"))
WebUI.delay(2)
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_notifications_action']"));

boolean savedNotificationEnabled = checkIfNotificationEnabled();
boolean savedEmailEnabled = checkIfEmailEnabled();

checkIfNotificaitonIsSavedSuccessfully(newNotificationEnabled, newEmailEnabled, savedNotificationEnabled, savedEmailEnabled);


def checkIfNotificationEnabled() {
	TestObject assignPushAdminElement = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='Admins assigned in municipality_push_setting_enabled']")	
	boolean assignPushAdminElementExist = TestHelper.isElementPresent(assignPushAdminElement)
 
	return assignPushAdminElementExist;
}

def checkIfEmailEnabled() {
	TestObject assignEmailAdminElement = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//span[@id='Admins assigned in municipality_email_setting_enabled']")	
	boolean assignEmailAdminElementExist = TestHelper.isElementPresent(assignEmailAdminElement)	 
	return assignEmailAdminElementExist;
}

def changeNotificationSettings(boolean notificationEnabled, boolean emailEnabled) {
	if(notificationEnabled) {
		WebUI.click(TestHelper.getObjectById("//span[@id='Admins assigned in municipality_push_setting_enabled']"));
	} else {
		WebUI.click(TestHelper.getObjectById("//span[@id='Admins assigned in municipality_push_setting_disabled']"))
	}
	
	if(emailEnabled) {
		WebUI.click(TestHelper.getObjectById("//span[@id='Admins assigned in municipality_email_setting_enabled']"));
	} else {
		WebUI.click(TestHelper.getObjectById("//span[@id='Admins assigned in municipality_email_setting_disabled']"))
	}
}
	
def checkIfNotificaitonIsSavedSuccessfully(boolean newNotificationEnabled, boolean newEmailEnabled, boolean savedNotificationEnabled, boolean savedEmailEnabled) {
	if((newNotificationEnabled != savedNotificationEnabled) || (newEmailEnabled != savedEmailEnabled)) {
		TestHelper.thrownException("Notification and email settings not saved");
	} else {
		println("Test Complete");
		WebUI.closeBrowser();
	}
}
