import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement

long date = new Date().getTime();
String organizationName = 'Netways_Organization' + date;
String roleName = "Member";

// New User
String registeredEmail = 'netways_user' + date + '@gmail.com'
String newRegisteredEmail = 'new_netways_user' + date + '@gmail.com'
String password = 'cvW8qx4B2o3F4VwP/kNsqA=='
String firstName = 'FirstName_' + date;
String lastName = 'LastName_' + date;
String city = 'Hermel';
String fullName = firstName + ' ' + lastName;


TestHelper.registerNewUser(registeredEmail, password, firstName, lastName, city, true);
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_tab_id']"))

WebElement currentUser = TestHelper.getItem('profile-name-title', 0)
String currentUserName = currentUser.text;


TestHelper.goToManageEntity();
TestObject manageNetworkRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='Network Roles_manage_entity_action']")
TestHelper.clickButton(manageNetworkRoleAction)

checkOrganization(organizationName, roleName);


// Messages
registerNewUser(newRegisteredEmail, password, firstName, lastName, city);

// add company role
joinOrganization(organizationUrl, roleName);
loginWithFirstRegeisterdUser(registeredEmail, password)
checkNotifications(fullName, organizationName);


//TestHelper.loginWithSpecificUserNameAndPassword('netways_user1584481943761@gmail.com', password, true);
//checkNotifications(roleName, organizationName);

// Check Organization
def checkOrganization (String organizationName, String roleName) {
	TestObject emptyStateObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-network-role-empty-state']")
	boolean emptyStateObjectPresent = TestHelper.isElementPresent(emptyStateObject)

	if (emptyStateObjectPresent == true) {
		println("hasn't network roles")
 
		TestObject emptyStateAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='no-network-role-empty-state-button']")
		TestHelper.clickButton(emptyStateAction)

		// Create organization, add get created organization url
		UserRoleHelper.createOrganization(organizationName, roleName);
		 
		TestObject manageNetworkBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_network_role_back_action']")
		WebUI.waitForElementPresent(manageNetworkBackButton, 5)
		
		getOrganizationName(organizationName, roleName);
	} else {
		println("has network roles")
		TestObject addNetworkRoleAction = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='add-new-network-role-action']")
		TestHelper.clickButton(addNetworkRoleAction)
		
		// Create organization, add get created organization url
		UserRoleHelper.createOrganization(organizationName, roleName);
		TestObject manageNetworkBackButton = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@id='manage_network_role_back_action']")
		WebUI.waitForElementPresent(manageNetworkBackButton, 5)
		getOrganizationName(organizationName, roleName);
	}
}

def getOrganizationName(String organizationName, String roleName) {
	String networkRoleId = organizationName + '-' + roleName + '-role';
	String networkRoleIdXpath = "//div[@id='"+networkRoleId+"']"
	TestObject networkRoleObject = new TestObject().addProperty("xpath", ConditionType.EQUALS, networkRoleIdXpath)
	TestHelper.clickButton(networkRoleObject);
	 
	WebUI.delay(6);
	organizationUrl = WebUI.getUrl();
	 
	WebElement organizationNameObject = TestHelper.getItem('organization-name-title', 0)
	selectedOrganizationName = organizationNameObject.text;
}

// Regist new user functions
def registerNewUser(String registeredEmail, String password, String firstName, String lastName, String city) {
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='organization_back_button']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_network_role_back_action']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-User-back-button']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_manage_accounts']"));
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='logout-id']"));

	TestHelper.registerNewUser(registeredEmail, password, firstName, lastName, city, false)
}
 
def joinOrganization(String organizationUrl, String roleName) {
	WebUI.navigateToUrl(organizationUrl);
	TestObject joinOrganizationAction = TestHelper.getObjectById("//div[@id='Join Organization_action']")
	boolean joinOrganizationActionExist = TestHelper.isElementPresent(joinOrganizationAction);
	
	if(joinOrganizationActionExist) {
		TestHelper.clickButton(joinOrganizationAction);
		UserRoleHelper.joinOrganization(roleName);
		WebUI.delay(5);
	} else {
		TestHelper.thrownException("User already has role in this organization");
	}
}

def loginWithFirstRegeisterdUser(String email, String password) {
	WebUI.navigateToUrl(TestHelper.getHomeUrl());
	TestObject manageProfilSection = TestHelper.getObjectById("//div[@id='manageProfileId']")
	TestHelper.clickButton(manageProfilSection);
	
	TestObject logoutAction = TestHelper.getObjectById("//div[@id='logout-id']")
	TestHelper.clickButton(logoutAction);

	TestHelper.loginWithSpecificEmail(email, password);
}

def checkNotifications(String profileName, String organization) {
	
	// read notification badge count
	WebElement notification = TestHelper.getItem("notifications-count", 1);	
	String notificationCountString = notification.text;
	
	println("notificationCountString: " + notificationCountString);
	
	int notificationCount = Integer.parseInt(notificationCountString);
	
	if(notificationCount != 3) {
		TestHelper.thrownException("Message notification number not displayed");
	}

	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_notification']"));
	

	String notificationId = profileName + ' listed themself in ' + organization	 + ' as Member. This requires your approval';
	TestObject notificationObject = TestHelper.getObjectById("//div[@id='" + notificationId + "']")
	boolean notificationObjectExist = TestHelper.isElementPresent(notificationObject);
	
	if(!notificationObjectExist) {
		TestHelper.thrownException("Notificaiton not exist");
	} else {
		TestHelper.clickButton(notificationObject);
		WebUI.click(TestHelper.getObjectById("//div[@id='manage-organization-member-back-button']"));
		WebUI.click(TestHelper.getObjectById("//div[@id='notifications_back_button']"));
		
		// check message badge count decrement
		WebElement newNotification = TestHelper.getItem("notifications-count", 1);
		String newNotificationCountString = newNotification.text;
		
		println("notificationCountString: " + newNotificationCountString);
		
		int newNotificationCount = Integer.parseInt(newNotificationCountString);
 		
		if(newNotificationCount != notificationCount -1) {
			TestHelper.thrownException("Notification badge number not decrement");
		}
	 	
		println("Test Complete");
		WebUI.closeBrowser();
	}
}


