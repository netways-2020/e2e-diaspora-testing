import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static org.junit.Assert.*

import org.junit.Test

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.testobject.ConditionType
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement

long date = new Date().getTime();
String registeredEmail = 'netways_user' + date + '@gmail.com'
String password = 'cvW8qx4B2o3F4VwP/kNsqA=='

String firstName = 'FirstName_' + date;
String lastName = 'LastName_' + date;
String fullName = firstName + ' ' + lastName;
String city = 'Hermel';
String roleName = 'Founder';

// Login and get company url
WebUI.callTestCase(findTestCase('Company/Founder or admin/Authorization for company/Login_For_Company'), [:], FailureHandling.STOP_ON_FAILURE)
String companyUrl = WebUI.getUrl();
WebElement companyNameObject = TestHelper.getItem('company-name-title', 0)
String companyName = companyNameObject.text;

// Register new user
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='company_back_button']"))
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage_business_role_back_action']"))
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='manage-User-back-button']"))
TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_manage_accounts']"))

TestObject logoutAction = TestHelper.getObjectById("//div[@id='logout-id']")
TestHelper.clickButton(logoutAction);
TestHelper.registerNewUser(registeredEmail, password, firstName, lastName, city, false);

// add company role
joinCompany(companyUrl, roleName);
loginWithAutomatinTestingUser();
checkNotifications(fullName, companyName);

def joinCompany(String companyUrl, String roleName) {
	WebUI.navigateToUrl(companyUrl);
	TestObject joinCompanyAction = TestHelper.getObjectById("//div[@id='Join Company_action']")
	boolean joinCompanyActionExist = TestHelper.isElementPresent(joinCompanyAction);
	
	if(joinCompanyActionExist) {
		TestHelper.clickButton(joinCompanyAction);
		UserRoleHelper.joinCompany(roleName);
		WebUI.delay(1); 
	} else {
		TestHelper.thrownException("User already has role in this company");
	} 
}

def loginWithAutomatinTestingUser() {
	WebUI.navigateToUrl(TestHelper.getHomeUrl());
	TestObject manageProfilSection = TestHelper.getObjectById("//div[@id='manageProfileId']")
	TestHelper.clickButton(manageProfilSection);
	
	TestObject logoutAction = TestHelper.getObjectById("//div[@id='logout-id']")
	TestHelper.clickButton(logoutAction);

	TestHelper.loginWithAutomationTestingUser();	
	TestHelper.clickButton(TestHelper.getObjectById("//div[@id='profile_notification']"));
}

def checkNotifications(String profileName, String companyName) {
	String notificationId = profileName + ' listed themself in ' + companyName + ' as Founder. This needs your approval';
	TestObject notificationObject = TestHelper.getObjectById("//div[@id='" + notificationId + "']")
	boolean notificationObjectExist = TestHelper.isElementPresent(notificationObject);
	
	if(!notificationObjectExist) {
		TestHelper.thrownException("Notificaiton not exist");
	} else {
		println("Test Complete");
		WebUI.closeBrowser();
	}
}

 